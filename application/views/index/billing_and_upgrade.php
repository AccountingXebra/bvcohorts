<section id="price-plan" class="price-plan section price-promo" style="padding:80px 0 40px 0">
	<div class="ubea-container">
    	<div class="container">
			<div class="portfolio_grid">
				
				<div class="price-cols row">
                    <div class="item col-md-4 col-sm-4 col-xs-12">
                        <h3 class="heading" style="background:#28323b">Forever Free</h3>
                        <div class="content">
                            <p class="price-figure"><span class="currency">$</span><span class="number free">0</span><br><span class="price-month">per month</span></p>
                            <ul class="list-unstyled feature-list">
                                <li>3 clients</li>
                                <li>1 user</li>
                                <li>2 invoice templates</li>
                               
                            </ul>
                            <a class="btn btn-cta btn-cta-primary btn-cta-free" href="#">Sign Up</a>
                            <br><span class="price-signup-free">Forever Free.</span>
                        </div><!--//content-->
                    </div><!--//item--> 
                    
                    <div class="item col-md-4 col-sm-4 col-xs-12">
                        <h3 class="heading" style="background:#2791ff">Solo</h3>
                        <div class="content">
                            <p class="price-figure"><span class="currency">$</span><span class="number solo">15</span><br><span class="price-month">per month</span></p>
                            <ul class="list-unstyled feature-list">
                              	<li>35 clients</li>
                                <li>2 users</li>
                                <li>15 invoice templates</li>
                            </ul>
                            <a class="btn btn-cta btn-cta-primary btn-cta-solo" href="#">Try For Free</a>
                             <br><span class="price-signup-free">30 day free trial</span>
                        </div><!--//content-->
                    </div><!--//item-->  
                    
                    <div class="item col-md-4 col-sm-4 col-xs-12">
                        <h3 class="heading" style="background:#2791ff">Pro</h3>
                        <div class="content">
                            <p class="price-figure"><span class="currency">$</span><span class="number pro">30</span><br><span class="price-month">per month</span></p>
                            <ul class="list-unstyled feature-list">
                                <li>unlimited clients</li>
                                <li>unlimited users</li>
                                <li>15 invoice templates</li>
                                
                            </ul>
                            <a class="btn btn-cta btn-cta-primary btn-cta-pro" href="#">Try For Free</a>
                             <br><span class="price-signup-free">30 day free trial</span>
                        </div><!--//content-->
                    </div><!--//item-->                     
                </div> 
				<style>
				#price-plan{text-align:center;}
				.price-promo {
					background: #FBFDFE;
				}
				.item .heading-blue, .price-plan .item .heading {
					background: #2791ff;
					color: #fff;
					padding: 10px 0;
					-moz-border-radius-topright: 4px;
					-moz-border-radius-bottomright: 0;
					-moz-border-radius-bottomleft: 0;
					-moz-border-radius-topleft: 4px;
					-moz-background-clip: padding;
					-webkit-background-clip: padding-box;
					background-clip: padding-box;
					margin-bottom: 0;
					text-transform: uppercase;
					position: relative;
					border-radius: 4px 4px 0 0;
					font-family: 'Montserrat';
					font-style: normal;
					font-weight: 500 !important;
					font-size: 24px !important;
					line-height: 1;
				}
				.price-plan .item .content {
					background: #fff;
					padding: 30px;
					border: #00BFF3 1px solid;
					border-radius: 0px 0px 5px 5px;
				}
				h3.heading {
				border: #00BFF3 1px solid;
				border-radius: 5px 5px 0px 0px;
			}
				.price-figure {
					color: #35414D;
				}
				.price-plan .item .price-figure .currency {
					font-size: 24px;
					position: relative;
					top: -12px;
				}
				.price-plan .item .price-figure .number {
					font-family: 'Open Sans',sans-serif;
					font-size: 60px;
				}
				.price-month {
					margin-top: -10px;
					display: block;
				}
				.price-month, .price-signup-free {
					font-size: 12px;
				}
				.list-unstyled {
					padding-left: 0;
					list-style: none;
				}
				.price-plan .item .feature-list li {
					padding: 10px 0;
				}
				.price-plan .item .btn {
					font-weight: 700;
					font-size: 16px;
					padding-left: 45px;
					padding-right: 45px;
				}
				.btn-cta-primary, a.btn-cta-primary {
					background: #59cc51;
					border: 2px solid #59cc51;
					color: #fff;
					text-transform: uppercase;
					font-size: 15px;
				}
				.btn, a.btn {
					padding: 8px 16px;
				}
				.btn-cta-free:hover {
					color: #fff !important;
				}
				.btn-cta-solo:hover{
					color: #fff !important;
				}
				.btn-cta-pro:hover{
					color: #fff !important;
				}
				
				@media (min-width: 320px) and (max-width: 640px){
					.setting {
						margin-right: 0px;
						margin-left: 0px;
					}
					.item.col-md-4.col-sm-4.col-xs-12 {
						margin-bottom: 20px;
					}
					#ubea-footer .container-fluid>div {
						padding-top: 20px;
						background: #000;
					}
					.part1, .part2, .part3 {
						height: auto;
						border-right-style: inset;
						border-right-color: #333;
						border-right-width: 1px;
					}
				}
				</style>
            </div>
        </div>
	</div>
</div>