<div class="ubea-section" id="portfolio_sec">
    <div class="text_2">
    	<img src="<?php echo base_url(); ?>/public/images/benefits_title.png" align="middle" alt="title" />
		<!--<div class="easy_title">Benefits of <div class="easy easy_title_1">easy<div class="easy easy_title_2">invoices</div></div></div>-->
    </div>
</div>
<div class="ubea-section" id="ubea-portfolio" data-section="portfolio">
	<div class="ubea-container">
    	<div class="container">
			<div class="portfolio_grid">
				<div class="row">
					<div class="col-md-3" id="portfolio_icon">
                   <!-- <a href="<?php echo base_url(); ?>public/images/img-1.png" class="ubea-card-item image-popup grid_border" title="Increase Profitibility">
							<figure>
								<div class="overlay"><i class="ti-plus"></i></div>
								<img src="<?php echo base_url(); ?>public/images/img-1.png" alt="Image" class="img-responsive">
							</figure>
						</a>-->
						<img src="<?php echo base_url(); ?>public/images/img-1.png" alt="Image" class="img-responsive">
                        <div class="grid_title montserrat_bold"> Increase Profitability</div>
                        <div class="grid_text montserrat_light">File GSTR1, GSTR2 and GSTR3 returns</div>
					</div>
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-2.png" alt="Image" class="img-responsive">
                        <div class="grid_title montserrat_bold">Client / Vendor Access</div>
                        <div class="grid_text montserrat_light">Provide select access to your clients and vendors</div>
					</div>
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-3.png" alt="Image" class="img-responsive">
                        <div class="grid_title montserrat_bold">Credit Period</div>
                        <div class="grid_text montserrat_light">Calculates and presents client wise credit history</div>
					</div>

					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-4.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Recurring Invoice</div>
                        <div class="grid_text montserrat_light">Integrated system to automatically send out retainer invoices</div>
					</div>
                </div>
                <div class="row">
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-5.png" alt="Image" class="img-responsive">
                        <div class="grid_title montserrat_bold">Financial Analysis</div>
                        <div class="grid_text montserrat_light">Detailed financial analysis to monitor your growth rate</div>
					</div>
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-6.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Customize invoices</div>
                        <div class="grid_text montserrat_light">Customize the invoice with your logo</div>
					</div>
                	<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-7.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">1 click process</div>
                        <div class="grid_text montserrat_light">Convert the estimate into your desired invoice with just a click</div>
					</div>
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-8.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Reminders</div>
                        <div class="grid_text montserrat_light">Set reminders and send emails for late payments</div>
					</div>
                </div>
                <div class="row">
                    <div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-9.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Multiple Currencies</div>
                        <div class="grid_text montserrat_light">Bill your clients in any currency</div>
					</div>
					<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-10.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Add Credit Notes</div>
                        <div class="grid_text montserrat_light">Adjust your discounts and credit notes</div>
					</div>
                    <div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-11.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Secure Invoicing</div>
                        <div class="grid_text montserrat_light">All your data is 256-bit encrypted</div>
					</div>
                	<div class="col-md-3" id="portfolio_icon">
						<img src="<?php echo base_url(); ?>public/images/img-12.png" alt="Image" class="img-responsive">
						<div class="grid_title montserrat_bold">Manage Paperwork</div>
                        <div class="grid_text montserrat_light">Manage your clients and related paperwork of legal agreements and Work Orders</div>
					</div>
				</div>
            </div>
        </div>
	</div>
</div>