<!--
<div class="container">
    <div class="slider_text">
               <h2>Create Profesional looking invoices within seconds and carry out financial analysis</h2>
    </div>
</div>
<div class="slider">
    <div id="ubea-hero" data-section="home">
		<div class="flexslider">
			<ul class="slides">
                <li style="background-image: url(<?php echo base_url(); ?>public/images/image_1.png);"></li>
                <li style="background-image: url(<?php echo base_url(); ?>public/images/image_2.png);"></li>
                <li style="background-image: url(<?php echo base_url(); ?>public/images/image_3.png);"></li>
                <li style="background-image: url(<?php echo base_url(); ?>public/images/img4.png);"></li>
		  	</ul>
	  	</div>
	</div>
</div> 
<div class="row">
    <div class="col-md-12">
    	<img src="<?php echo base_url(); ?>public/images/img4.png" alt="Image" class="homepage_slider">
    </div>
</div> -->


<div class="w3-content w3-display-container">
    <img class="mySlides" src="<?php echo base_url(); ?>public/images/image_1.png" style="width:100%" alt="img-1">
    <img class="mySlides" src="<?php echo base_url(); ?>public/images/image_2.png" style="width:100%" alt="img-2">
    <img class="mySlides" src="<?php echo base_url(); ?>public/images/image_3.png" style="width:100%" alt="img-3">
    <img class="mySlides" src="<?php echo base_url(); ?>public/images/img4.png" style="width:100%" alt="img-4">

	<button class="w3-btn w3button w3-display-left" onclick="plusDivs(-1)" style="font-size:50px;color:#FFF">&#10094;</button>
	<button class="w3-btn w3button w3-display-right" onclick="plusDivs(1)" style="font-size:50px;color:#FFF">&#10095;</button>
</div>

<script type="text/javascript">

var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>