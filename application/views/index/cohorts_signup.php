<?php include('header.php'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Sign up to the Xebra Cohorts community to discuss your ideas, solve business problems, get deals and price offs or generate business for partnering.">
<style type="text/css">
	.forgot_email label.error{
		padding-top: 12px;
	}
	.modal-loader {
		position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(./public/images/loading-ani.gif) center no-repeat;
	}
    .dataTables_length {
		margin-left: 500px;
	}
    .eazy{
      font-weight:900%;
    }
    /*.dot {
		height: 100px;
		width: 100px;
		background-color: #FF7D9A;
		border-radius: 50%;
		display: inline-block;
    }*/
    input#signup_submit{
		width:60% !important;
		padding-right:60px !important;
		background: #bc343a !important;
		color:white !important;
		border:none !important;
	} 
    input#login_submit:hover{
		background: #bc343a !important;
		color:white !important;
		border:none !important;
    }
    .logo_style_2{
		height: 74px !important;
		margin:5px 0;
    }
	.login_page{
		width:32% !important;
	}
	.welcom-be{
		font-size:32px;
		font-weight:100 !important;
		color:#fff !important;
		text-align:left;
		line-height:1.2 !important;
	}
	
	.welcom-title{
		font-size:32px;
		font-weight:600;
		color:#fff !important;
	}
	.welcom-content{
		padding:20px 0 10px 0 !important;
	}
	body{
		width:100% !important;
		/*background-color:#bc343a !important;*/ 
		/*background: linear-gradient(-180deg, #503cb4 2%, #503cb4 100%) !important;*/
		background: linear-gradient(-180deg, #e65343 2%, #d2433f 100%) !important;
	}
	@media screen and (-webkit-min-device-pixel-ratio:0){
		::i-block-chrome,body{ 
			background:#d2433f !important;
		}
		::i-block-chrome,.person-info{
			padding-top:8px !important;
		}
	}
	
	.comm-pic{
		border-top-left-radius:5px;
		border-bottom-left-radius:5px;
	}
	
	.soft-comment{
		background-color:#fff !important;
		height:170px;
		margin-left:-25px;
		border-top-right-radius:5px;
		border-bottom-right-radius:5px;
	}
	
	.person-comment{
		padding: 3% 5px 0 5px;
		text-align: center;
		font-size: 12.5px;
		font-weight: 500;
		color:#000;
		margin-bottom:10px !important;
	}
	
	.person-info{
		padding: 5px 5px 0 5px;
		text-align: center;
		font-size: 12.5px;
		font-weight: 500;
		color:#000;
	}
	
	.high{
		border:1px solid red !important;
		border-radius:5px !important;
	}
	
	.sign_up .login_form_textbox label.error {
		margin: -18px 0 0 0 !important;
	}
	
	#signup_submit:hover{
		background: #bc343a !important;
		color:white !important;
	}
	
	#signup_submit:active{
		background: #bc343a !important;
		color:white !important;
	}
	
	.pad-10{
		padding-top:10px;
	}
	@media only screen and (max-width: 1280px) {
		.sign_up .login_page{ width:35% !important; }
		.person-info{ padding: 12px 5px 0 5px; }
	}				
	@media only screen and (max-width: 600px) {
		.ubea-nav-toggle{ display:none; }
		.Welcome-image {
			padding: 3% 4% 2% 5%;
			margin: 18% -7%;
			width:100%;
		}
		.login_page .modal-content{
			box-shadow: none;
			width: 330px !important;
			margin: 20px -80px 0 -80px;
		}
		#email{ width:107.5%; }
		.pswd{ border: 1px solid #ababab; border-radius: 4px; width: 110%; }
		.pswd1{ border: 1px solid #ababab; border-radius: 4px; width: 112%; }
		.remme{ display:flex !important;  margin:0 0 0 -13px;}
		.forpsw{ margin:-2px 0px 0 25px; }
		input#login_submit{ padding-right:90px !important; width:63%; line-height:25px; }
		.welcom-content{ text-align:center; }
		.sign_up{ margin:7% -22px; }
		.soft-comment {
			height: 190px;
			margin-left: -35px;
			width: 330px;
			border-radius:5px;
		}
		.firstin{ margin:0 0 0 17px; }
		.comm-pic{ margin: 0 0 0 30px; border-radius: 10px; }
	}
</style>
	<span class="dot"></span>
    <div class="container " style="margin: 20px 0; width: 100%;">
	<div class="col-md-12">
	<div class="sign_up container" style="margin: 7% 0; width: 100%;">
        <div class="col-md-12">
			<div class="modal-loader" style="display: none;"></div>
			<!--<div class="col-md-1" style="padding-top:0% !important;"></div>-->
			<div class="col-md-7" style="padding-top:2% !important;">
				<div class="welcom-container">
					<div class="welcom-content">
						<span class="welcom-be">BE A</span>
						<h1 class="pad-10 welcom-title">SMART ENTREPRENEUR</h1>
					</div>
				</div>
				<img class="geen" height="150" src="<?php echo base_url();?>asset/css/img/icons/Circle-1.png" style="margin-left:-6% !important; position:absolute; text-align:center; margin-top:10px;" alt="off-cir" title="off-cir">
				<div class="row">
					<div class="col-md-3">
						<img height="170" width="150" src="<?php echo base_url();?>public/images/naman_gupta.jpg" class="gifblank comm-pic" alt="com-blank" title="com-blank">
					</div>
					<div class="col-md-7 soft-comment" style="border-left:1px solid #ccc; background-color:#fff; padding:10px;">
						<p class="person-comment">“A dynamic platform showcasing deep-rooted and resilient innovations is the key to mass growth and sustainability. Bharat Vaani has done it by building this strong community. Bharat Vaani is a revolution in its space.”</p>
						<p class="person-info"><strong>Naman Gupta</strong></br>Founder - CodeEffort</p>
					</div>
				</div>
				<!--<div class="row" style="background-color:#fff; margin:2% 0 0 0; width:80.5%; border-radius:5px; border-bottom-right-radius:5px; border-top:1px solid #eef2fe; padding: 8px 0;">
					<div class="col-md-12"><span style="font-size: 12px; color: #000;">"Wherever you see a successful business, someone once made a courageous decision." Peter Drucker</span></div>
				</div>	
				<img class="geen" width="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-2.png" style="margin-left:12% !important; position:absolute; text-align:center; margin-top:0px;" alt="off-cir">-->
			</div>
            <div class="row">
			<div class="col-md-8 login_page" style="margin-left: 0; padding:0 10px 0 60px;">
				<img class="geen" height="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-1.png" style="margin-left:-12% !important; position:absolute; text-align:center; margin-top:15px;" alt="off-cir" title="off-cir">
				<div class="modal-content" style="box-shadow: none;">
					<div style="text-align:center; margin-top:10px;"  >
						<img width="281" height="74" src="<?php echo base_url(); ?>asset/images/cohorts/bv.png" alt="Bharat Vaani" title="Bharat Vaani" class="logo_style_2"/>
						<!--label class="logo_line_2" style="font-size:15px; font-family: 'Droid Sans', Arial, sans-serif; margin-top:5px !important;">No credit card required. Cancel Anytime.</label-->
					</div>
                    <div class="modal-body" style="margin-top:-10px !important;">
						<form action="" id="signup_frm" name="signup_frm" class="" method="post" accept-charset="utf-8" novalidate="true" style="margin:0px 15px 0px 15px !important;">
						<div class="row">
                           		<div class="col-lg-12 error_cls">
									<div id="error_popup" name="error_popup">
                            		</div>
                                </div>
                                <!--div class="col-lg-12 login_form_textbox">
                                    <div class="input-group name-re">
                                      <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                      <input class="form-control" name="name" id="name" type="text" placeholder="NAME">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div-->
								
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group email-re">
                                      <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                      <input class="form-control Web_email" name="email" id="email" type="text" placeholder="EMAIL" value="<?php if(isset($_GET['ne'])){echo $_GET['ne'];}?>">
                                    </div>
                                    <div class="req_star_textbx sign-star firstin"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group pswd pass-wrng" style="border:1px solid #ababab; border-radius:4px;">
                                          <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-2x"></i></span>
                                          <input style="width:82%; border:none !important;" class="form-control password_strength show_password" name="password_2" id="password_2" type="password" placeholder="PASSWORD" onblur="alphanumeric(document.signup_frm.password_2)">
                                    </div>
									<div class="hide-show">
										<span style="position:absolute; left:82%; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy" title="small-eazy"></img></span>
									</div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								  <div class="col-lg-12 login_form_textbox" style="margin-top:-10px; margin-bottom:-12px !important;">
									<label style="font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
                                  </div>
								<div class="col-lg-12"><!--span id="pwdMeter" class="neutral"></span--></div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group pswd1 con-re" style="border:1px solid #ababab; border-radius:4px;">
                                          <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-refresh fa-lg" style="font-size:15px;"></i></span>
                                          <input style="width:82%; border:none !important;" class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM PASSWORD">
                                    </div>
									<div class="hide-show-c">
										<span style="position:absolute; left:82%; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy" title="small-eazy"></img></span>
									</div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								

                                <div class="col-lg-12 form-group login_btn_1">
                                    <input onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#bc343a'" onMouseOver="this.style.border='1px solid #bc343a',this.style.color='#fff'" type="submit" class="btn login_form_btn sign-xebra" value="SECURE SIGNUP" name="signup_submit" id="signup_submit"><i class="fa fa-lock fa-2x" style="color:#fff !important; margin-left:-25px;"></i>
                                </div>
                                
                             </div>
					</form>
				</div>
				<div class="modal-footer" style="background-color:#f0f0f5; padding:20px 10px; border-radius:5px;">
					<div class="col-lg-12">
						<div class="login_signup_link" style="font-size:13px;">Already have an account?  <a href="<?php echo base_url(); ?>login" style="color:#bc343a;">  <b>Log In</b></a></div>
						<!-- onclick="open_login()" -->
						<!--div><a style="text-decoration:none; font-family: 'Roboto', sans-serif;" class="" href="#" onclick="open_co_otp()">Forgot Password?</a></div-->
					</div>
				</div>
			</div>
			<img class="geen" width="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-2.png" style="margin-left:25% !important; position:absolute; text-align:center; margin-top:-1px;" alt="off-cir" title="off-cir">
		</div>
			</div>
		</div>
	</div>  
	</div>
	</div>
    <div class="footer" style="margin-top:4%; border-top:1px solid #C0C0C0;">
		<div class="row" style="text-align:center; padding-top:10px;">
			<div style="margin-right:-15px; font-size:13px;">
				<p style="color:white;"><a style="color:white;" href="https://bharatvaani.in/terms-and-conditions" target="_blank"> Terms & Conditions </a>  |  <a style="color:white;" href="https://bharatvaani.in/privacy-policy" target="_blank"> Privacy Policy </a>  |  <a style="color:white;" href="https://bharatvaani.in/faqs" target="_blank"> FAQs </a></p>
			</div>
			<div style="margin-right:-10px; margin-top:5px;">
				<label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y');?> Bharat Vaani. All Rights Reserved</label>
			</div>
		</div>
	</div>
	<script>
	$('.hide-show-c').show();
		$('.hide-show-c span').addClass('show')
		$('.hide-show-c span').click(function(){
		if( $(this).hasClass('show') ) {
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy" title="small-eazy"></img>');
			$('#confirm_password').attr('type','text');
			$(this).removeClass('show');
		} else {
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy" title="small-eazy"></img>');
			$('#confirm_password').attr('type','password');
			$(this).addClass('show');
		}
		});	
	</script>	
    <script>
    $( document ).ready(function(){
		
	var em = '<?php 
	if(@$web_email){
		echo $web_email;
    }else{
		echo "";
    }?>'
	if(em){
        open_signup();
		$('.Web_email').val(em);

    }
    .welcom-content{
        padding:20px 0 10px 0 !important;
    }
    body{
        width:100% !important;
        /*background-color:#bc343a !important;*/ 
        /*background: linear-gradient(-180deg, #503cb4 2%, #503cb4 100%) !important;*/
        background: linear-gradient(-180deg, #e65343 2%, #d2433f 100%) !important;
    }
    @media screen and (-webkit-min-device-pixel-ratio:0){
        ::i-block-chrome,body{ 
            background:#d2433f !important;
        }
        ::i-block-chrome,.person-info{
            padding-top:8px !important;
        }
    }

    .comm-pic{
        border-top-left-radius:5px;
        border-bottom-left-radius:5px;
    }

    .soft-comment{
        background-color:#fff !important;
        height:170px;
        margin-left:-25px;
        border-top-right-radius:5px;
        border-bottom-right-radius:5px;
    }

    .person-comment{
        padding: 3% 5px 0 5px;
        text-align: center;
        font-size: 12.5px;
        font-weight: 500;
        color:#000;
        margin-bottom:10px !important;
    }

    .person-info{
        padding: 28px 5px 0 5px;
        text-align: center;
        font-size: 12.5px;
        font-weight: 500;
        color:#000;
    }

    .high{
        border:1px solid red !important;
        border-radius:5px !important;
    }

    .sign_up .login_form_textbox label.error {
        margin: -18px 0 0 0 !important;
    }

    #signup_submit:hover{
        background: #bc343a !important;
        color:white !important;
    }

    #signup_submit:active{
        background: #bc343a !important;
        color:white !important;
    }

    .pad-10{
        padding-top:10px;
    }
    @media only screen and (max-width: 1280px) {
        .sign_up .login_page{ width:35% !important; }
        .person-info{ padding: 12px 5px 0 5px; }
    }				
    @media only screen and (max-width: 600px) {
        .ubea-nav-toggle{ display:none; }
        .Welcome-image {
            padding: 3% 4% 2% 5%;
            margin: 18% -7%;
            width:100%;
        }
        .login_page .modal-content{
            box-shadow: none;
            width: 330px !important;
            margin: 20px -80px 0 -80px;
        }
        #email{ width:107.5%; }
        .pswd{ border: 1px solid #ababab; border-radius: 4px; width: 110%; }
        .pswd1{ border: 1px solid #ababab; border-radius: 4px; width: 112%; }
        .remme{ display:flex !important;  margin:0 0 0 -13px;}
        .forpsw{ margin:-2px 0px 0 25px; }
        input#login_submit{ padding-right:90px !important; width:63%; line-height:25px; }
        .welcom-content{ text-align:center; }
        .sign_up{ margin:7% -22px; }
        .soft-comment {
            height: 190px;
            margin-left: -35px;
            width: 330px;
            border-radius:5px;
        }
        .firstin{ margin:0 0 0 17px; }
        .comm-pic{ margin: 0 0 0 30px; border-radius: 10px; }
    }
</style>
<span class="dot"></span>
<div class="container " style="margin: 20px 0; width: 100%;">
    <div class="col-md-12">
        <div class="sign_up container" style="margin: 7% 0; width: 100%;">
            <div class="col-md-12">
                <div class="modal-loader" style="display: none;"></div>
                <!--<div class="col-md-1" style="padding-top:0% !important;"></div>-->
                <div class="col-md-7" style="padding-top:2% !important;">
                    <div class="welcom-container">
                        <div class="welcom-content">
                            <span class="welcom-be">BE A</span>
                            <h1 class="pad-10 welcom-title">SMART ENTREPRENEUR</h1>
                        </div>
                    </div>
                    <img class="geen" height="150" src="<?php echo base_url(); ?>asset/css/img/icons/Circle-1.png" style="margin-left:-6% !important; position:absolute; text-align:center; margin-top:10px;" alt="off-cir">
                    <div class="row">
                        <div class="col-md-3">
                            <img height="170" width="150" src="<?php echo base_url(); ?>public/images/naman_gupta.jpg" class="gifblank comm-pic" alt="com-blank">
                        </div>
                        <div class="col-md-7 soft-comment" style="border-left:1px solid #ccc;">
                            <p class="person-comment">“A dynamic platform showcasing deep-rooted and resilient innovations is the key to mass growth and sustainability. Bharat Vaani has done it by building this strong community. Bharat Vaani is a revolution in its space.”</p>
                            <p class="person-info"><strong>Naman Gupta</strong></br>Founder - CodeEffort</p>
                        </div>
                    </div>
                    <!--<div class="row" style="background-color:#fff; margin:2% 0 0 0; width:80.5%; border-radius:5px; border-bottom-right-radius:5px; border-top:1px solid #eef2fe; padding: 8px 0;">
                            <div class="col-md-12"><span style="font-size: 12px; color: #000;">"Wherever you see a successful business, someone once made a courageous decision." Peter Drucker</span></div>
                    </div>	
                    <img class="geen" width="170" src="<?php echo base_url(); ?>asset/css/img/icons/Circle-2.png" style="margin-left:12% !important; position:absolute; text-align:center; margin-top:0px;" alt="off-cir">-->
                </div>
                <div class="row">
                    <div class="col-md-8 login_page" style="margin-left: 0; padding:0 10px 0 60px;">
                        <img class="geen" height="170" src="<?php echo base_url(); ?>asset/css/img/icons/Circle-1.png" style="margin-left:-11% !important; position:absolute; text-align:center; margin-top:15px;" alt="off-cir">
                        <div class="modal-content" style="box-shadow: none;">
                            <div style="text-align:center; margin-top:10px;"  >
                                <img width="281" height="74" src="<?php echo base_url(); ?>asset/images/cohorts/bv.png" alt="xebra-logo" class="logo_style_2"/>
                                <!--label class="logo_line_2" style="font-size:15px; font-family: 'Droid Sans', Arial, sans-serif; margin-top:5px !important;">No credit card required. Cancel Anytime.</label-->
                            </div>
                            <div class="modal-body" style="margin-top:-10px !important;">
                                <form action="" id="signup_frm" name="signup_frm" class="" method="post" accept-charset="utf-8" novalidate="true" style="margin:0px 15px 0px 15px !important;">
                                    <div class="row">
                                        <div class="col-lg-12 error_cls">
                                            <div id="error_popup" name="error_popup">
                                            </div>
                                        </div>
                                        <!--div class="col-lg-12 login_form_textbox">
                                            <div class="input-group name-re">
                                              <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                              <input class="form-control" name="name" id="name" type="text" placeholder="NAME">
                                            </div>
                                            <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                        </div-->

                                        <div class="col-lg-12 login_form_textbox">
                                            <div class="input-group email-re">
                                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                                <input class="form-control Web_email" name="email" id="email" type="text" placeholder="EMAIL" value="<?php
                                                if (isset($_GET['ne'])) {
                                                    echo $_GET['ne'];
                                                }
                                                ?>">
                                            </div>
                                            <div class="req_star_textbx sign-star firstin"><font style="color:red">*</font></div>
                                        </div>
                                        <div class="col-lg-12 login_form_textbox">
                                            <div class="input-group pswd pass-wrng" style="border:1px solid #ababab; border-radius:4px;">
                                                <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-2x"></i></span>
                                                <input style="width:82%; border:none !important;" class="form-control password_strength show_password" name="password_2" id="password_2" type="password" placeholder="PASSWORD" onblur="alphanumeric(document.signup_frm.password_2)">
                                            </div>
                                            <div class="hide-show">
                                                <span style="position:absolute; left:253px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
                                            </div>
                                            <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                        </div>
                                        <div class="col-lg-12 login_form_textbox" style="margin-top:-10px; margin-bottom:-12px !important;">
                                            <label style="font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
                                        </div>
                                        <div class="col-lg-12"><!--span id="pwdMeter" class="neutral"></span--></div>
                                        <div class="col-lg-12 login_form_textbox">
                                            <div class="input-group pswd1 con-re" style="border:1px solid #ababab; border-radius:4px;">
                                                <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-refresh fa-lg" style="font-size:15px;"></i></span>
                                                <input style="width:82%; border:none !important;" class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM PASSWORD">
                                            </div>
                                            <div class="hide-show-c">
                                                <span style="position:absolute; left:253px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
                                            </div>
                                            <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                        </div>


                                        <div class="col-lg-12 form-group login_btn_1">
                                            <input onmouseout="this.style.background - color = '#bc343a', this.style.border = '1px solid #bc343a', this.style.color = '#bc343a'" onMouseOver="this.style.border = '1px solid #bc343a', this.style.color = '#fff'" type="submit" class="btn login_form_btn sign-xebra" value="SECURE SIGNUP" name="signup_submit" id="signup_submit"><i class="fa fa-lock fa-2x" style="color:#fff !important; margin-left:-25px;"></i>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer" style="background-color:#f0f0f5; padding:20px 10px; border-radius:5px;">
                                <div class="col-lg-12">
                                    <div class="login_signup_link" style="font-size:13px;">Already have an account?  <a href="<?php echo base_url(); ?>login" style="color:#bc343a;">  <b>Log In</b></a></div>
                                    <!-- onclick="open_login()" -->
                                    <!--div><a style="text-decoration:none; font-family: 'Roboto', sans-serif;" class="" href="#" onclick="open_co_otp()">Forgot Password?</a></div-->
                                </div>
                            </div>
                        </div>
                        <img class="geen" width="170" src="<?php echo base_url(); ?>asset/css/img/icons/Circle-2.png" style="margin-left:25% !important; position:absolute; text-align:center; margin-top:-1px;" alt="off-cir">
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="footer" style="margin-top:4%; border-top:1px solid #C0C0C0;">
    <div class="row" style="text-align:center; padding-top:10px;">
        <div style="margin-right:-15px; font-size:13px;">
            <p style="color:white;"><a style="color:white;" href="https://bharatvaani.in/terms-and-conditions" target="_blank"> Terms & Conditions </a>  |  <a style="color:white;" href="https://bharatvaani.in/privacy-policy" target="_blank"> Privacy Policy </a>  |  <a style="color:white;" href="https://bharatvaani.in/faqs" target="_blank"> FAQs </a></p>
        </div>
        <div style="margin-right:-10px; margin-top:5px;">
            <label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y'); ?> Bharat Vaani. All Rights Reserved</label>
        </div>
    </div>
</div>
<script>
    $('.hide-show-c').show();
    $('.hide-show-c span').addClass('show')
    $('.hide-show-c span').click(function () {
        if ($(this).hasClass('show')) {
            $(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
            $('#confirm_password').attr('type', 'text');
            $(this).removeClass('show');
        } else {
            $(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
            $('#confirm_password').attr('type', 'password');
            $(this).addClass('show');
        }
    });
</script>	
<script>
    $(document).ready(function () {

        var em = '<?php
                                                       if (@$web_email) {
                                                           echo $web_email;
                                                       } else {
                                                           echo "";
                                                       }
                                                ?>'
        if (em) {
            open_signup();
            $('.Web_email').val(em);
        }
        var signup = '<?php echo $open_signup; ?>';
        if (signup) {
            open_signup();
        }
        var otp = '<?php echo $open_otp; ?>';
        if (otp) {
            open_otp();
        }
    });
</script>

<?php include('footer.php'); ?>