<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <title>Xebra</title>
	  <style TYPE="text/css" MEDIA="screen, print">
		@import url(<?php echo base_url(); ?>asset/materialize/css/materialize.css);
        /*@import url(<?php echo base_url(); ?>asset/css/style.css);*/
		
         
		@media only screen and (min-width: 993px){
			.container {
				width: 80%;
			}
        }
	 </style>
     <style type="text/css">
        #main{
         padding-left: 0px !important;
        }
		 
		@media print {
			
		}
		 
		@page {
            size: auto; 
            margin: 0mm;  
        }
		
        /*.container{
            background: url(Temp-2-1.jpg) no-repeat center;
            background-size: cover;
            min-height: 1000px;
        }
        #fifth-template p{
            margin:0;
            line-height: 1em;
            font-size: 15px;
        }
        .invoice-dets{
            width: 280px;
            height:150px;
        }
        .invoice-dets > p{
            font-size: 14px !important;
        }
        #inv .pl-5{
            font-size: 50px !important;
            font-weight: 400;
            letter-spacing: 0.1em;
        }
        .sidebar{
            height:1000px; 
            position: relative;
        }
        .to{
            position:absolute;
            bottom:0;
        }
        #fifth-template #tem-fifth-table {
            margin-top: 250px !important;
			width:80% !important;
			margin-left:5% !important;
        }
        #fifth-template #tem-fifth-table td,th{
            max-width: 280px;
            min-width: 80px;
            padding: 13px 10px 13px 0;
        }
        #fifth-template #tem-fifth-table tr{
            border-bottom: 1px solid black;
        }
        #payment{
            line-height: 0.5em;
            font-size: 13px;
        }
        #gst{
            width:200px;
			background-color:#ffcc00 !important;
        }
        #gst p{
            margin-bottom: 0px !important;
            padding-bottom: 0px !important ;
            padding-right: 4px;
            
        }
        #total{
            width:250px;
            border-top:1px black solid;
            font-size: 18px;
        }*/
		
		.text-right{
			text-align:right !important;
		}

    </style>
	<script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
		}
		$('.par-spa rightss cemterfd').html(USD+" ");

		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
			$('#to_words_inv').text('ZERO');
		}else{
			$('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
		});
	</script>
		</head>
		<body>
    <div class="container" style="width:837px;">
		<?php
			//$USD=$currencycode[0]->currencycode; 
        ?>
        <div class="row">
            <div class="col s12 m12 l6 sidebar">
                <p class="" style="font-size:40px; margin-bottom:0px !important; margin-left:10px;" id="inv">
						<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
                           <h5>ESTIMATE</h5>
						<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
                           <h5>PROFORMA INVOICE</h5>
                        <?php }else { $inv="Sales Invoice";?>
                           <h5><b>SALES INVOICE<b/></h5>
						<?php } ?>
				</p>
                <div class="col s12 m12 l6 float-left pl-4 text-left invoice-det font-weight-bold">
                    <p><?=strtoupper($inv)?> NO.:</p>
                    <p><?=strtoupper($inv)?> DATE:</p>
					<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
						<p>ESTIMATE / P.O. NO:</p>
					<?php } ?>
					<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
						<p>ESTIMATE / P.O. DATE:</p>
					<?php } ?>
                    <p>GSTIN:</p>
                    <p>PAN:</p>
					<p>PLACE OF SUPPLY:</p>
                </div>
                <div class="col s12 m12 l5 float-right text-right invoice-det-value font-weight-bold pr-3">
                        <p><?=  @$billing_doc[0]->inv_invoice_no_view; ?></p>
                        <p><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></p>
                        <?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<p><?=  @$billing_doc[0]->inv_po_no; ?></p>
						<?php } ?>
						<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<p><?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
						<?php } ?>
                        <p><?= @$gstno_cli[0]->gst_no; ?></p>
                        <p><?= @$cust_pan[0]->cust_pancard; ?></p>
                        <p><?=$place?></p>
                </div>
            </div>
			<div class="col s12 m12 l6" style="text-align:right;">
				<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                          if($custom_inv[0]->hdr_logo!=0){
                         ?>
                        <img src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="logo">
						<!--img height="40" width="200" src="<?php //echo base_url(); ?>public/images/xebra-logo.png" class="cmp-logo" alt="company_logo" style="margin-right:-41px !important;"-->
                     <?php } ?>
			</div>
		</div>
		<div class="row">
		
			<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                      
                      </style>
            <div class="col s12 m12 l12">
                <table id="tem-fifth-table">
                    <tr class="text-center" style="border-bottom:none;">
                        <th class="text-left"><b>ITEM DESCRIPTION</b></th>
                        <th><b>QUANTITY</b></th>
						<th><b>PRICE</b></th>
						<th><b>DISCOUNT</b></th>
                        <th><b>TOTAL</b></th>
                    </tr>
                    <?php foreach ($billing_doc as $invkey => $invlist) {  ?>
                    <tr class="text-center">
                        <td class="text-left"><span class="font-weight-bold d-block heading">
							<?php //$invkey+1;
							$temp=$sub_tbl.'_service_type'; ?>
							<?php if($invlist->$temp==3){
                            echo "Equalisation Levy";
                            }else if($invlist->$temp==4){
                            echo "Late Fee";
                            }else if($invlist->$temp==2){
                            echo "Expense Voucher";
                            }else{
                            foreach($my_services as $service){
                            if($service->service_id==$invlist->service_id){
                            echo $service->service_name;
                            }
                            }
                            }?>
						</span>
						<span class="desc">
							<?php  
                                    $particulars=$sub_tbl.'_particulars'; 
                                    $hsn=$sub_tbl.'_hsn_sac_no'; 
                                    $quantity=$sub_tbl.'_quantity'; 
                                    $rate=$sub_tbl.'_rate'; 
                                    $discount=$sub_tbl.'_discount'; 
                                    $taxable_amt=$sub_tbl.'_taxable_amt'; 
                                    $igst=$sub_tbl.'_igst'; 
                                    $igst_amt=$sub_tbl.'_igst_amt'; 
                                    $cgst=$sub_tbl.'_cgst'; 
                                    $cgst_amt=$sub_tbl.'_cgst_amt'; 
                                    $sgst=$sub_tbl.'_sgst'; 
                                    $sgst_amt=$sub_tbl.'_sgst_amt'; 
                                    $cess=$sub_tbl.'_cess'; 
                                    $cess_amt=$sub_tbl.'_cess_amt'; 

                                     $other=$sub_tbl.'_other'; 
                                    $other_amt=$sub_tbl.'_other_amt'; 
                                    $amount=$sub_tbl.'_amount'; 
                                    ?>
                                    <?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?>
						</span></td>
						<td><p><?=$invlist->$quantity;  ?></p></td>
                        <td><p><?=round($invlist->$rate);  ?></p></td>
                        <td><p><?=round($invlist->$discount);  ?></p></td>
                        <td><p><?=$USD?> <? echo $invlist->$quantity * $invlist->$rate - $invlist->$discount ?></p></td>
                    </tr>
					<?php } ?>
                </table>
			</div>
		</div>
        <div class="row pt-4">
			<div class="col s12 m12 l4">
				<div class="to pb-5">
                    <p class="m-1 font-weight-bold" style="color:#000 !important;"><b>INVOICE TO:</b></p>
                    <label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1"><?=  @ucwords($billing_doc[0]->cust_name); ?>,</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.',</br> '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></label><br>
                </div>
			</div>
			<div class="col s12 m12 l4" id="payment">
				<a href="#" class="btn purple white-text btn-lg my-2" style="background-color:#dddee2">MAKE PAYMENT</a></br>
				<p class="bnk-de" id="bank-name">BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
                <p class="bnk-de" id="ac-number">A/C NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
                <p class="bnk-de" id="branch">BRANCH: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
                <p class="bnk-de" id="ifsc">IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
				<p class="bnk-de" id="ifsc">SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
			</div>
            <div class="col s12 m12 l4 text-right pr-0">
				<p class="font-weight-bold h6 text-right pr-3"><b>SUB TOTAL:</b> <?= round(@$billing_doc[0]->inv_rev_total); ?></p>
                <div class="pr-3 float-right text-right" id="gst">
					<?php if($tax=="true") { ?>
				<p class="">CGST(9%): <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
				<p class="">SGST(9%): <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
				<?php } else { ?>
				<p class="">IGST(18%): <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
				<p class="pr-3"><span class="pr-5">IGST(18%)</span><?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
				<?php } ?>
				</div>
                <div class="clearfix"></div>
				<div class="font-weight-bold float-right text-left pl-3 pt-2" id="total">
					<h6 class="h4"><b>TOTAL:</b> <?=$USD?> <?=round($billing_doc[0]->inv_grant_total);  ?></b></h6>
				</div>
            </div>
       </div>
        
        <div class="footer pb-3">
            <p>TERMS & CONDITIONS:</p> 
			<p><?= @$billing_doc[0]->inv_terms; ?></p>
        </div>
    </div>
	</body>
	<script type="text/javascript">
			window.print();
		</script>