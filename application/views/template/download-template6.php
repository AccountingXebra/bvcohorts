<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
         padding-left: 0px !important;
         }
      </style>
      <style type="text/css" >
           
		@media only screen and (min-width: 993px){
			.container {
				width: 100%;  
				opacity: 999999;
				z-index: 99999;
			}
        }
		
		/* NEW CSS BY WCID-366 */
		p.white-color{
			color:#fff;
		}
		
		label.white-color{
			color:#fff;
		}
		
		.big-w{
			font-size:11px;
			color:#000;
		}
		.small-w{
			font-size:10px;
			color:#000;
		}
		
		.small-sw{
			font-size:9px;
			color:#000;
		}
		
		.inv-title{
			font-size:18px !important;
		}
		
		.eoe_div{
			width:50px;
		}
		
		/* Table CSS */
		
		/*#default_1 tr:nth-child(odd){
			background:#000;
		}
		
		#default_1 tr:nth-child(even){
			background:#eef2fe;
		}*/
		
		#default_1 td{
			border:none !important;
			border-top:1px solid #595959 !important;
		}
		
		#default_1 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_1 .amttable{
			text-align:center;
		}
		
		#default_2 td{
			border:none !important;
		}
		
		#default_2 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_2 .amttable{
			text-align:center;
		}
		
		#default_3 td{
			border:none !important;
			border-bottom:1px solid #ccc !important;
		}
		
		#default_3 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_3 .amttable{
			text-align:center;
		}
		
		.total_box{
			background-color:#000;
			color:#fff;
			border-radius:5px !important;
			height:auto !important;
			padding:15px 10px !important;
		}
		
		#inv_customisemodal footer {
			margin-top: 50px;
			border-bottom: 23px solid #000;
			padding:15px 10% 10px 16%;
		}
		
		.se_tem_tr{
			background-color:#000 !important;
			color:#fff;
			font-size:13px;
		}
		
		.ls_tem_tr{
			background-color:#abdea5 !important;
			color:#fff;
			font-size:13px;
			height:80px;
		}
		
		/*.inv_de_table {
			background-color:#004d1a !important;
			font-size:13px;
			height:50px;
		}*/
		
		.term-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#009933 !important;
		}
		
		hr.bu_pay{
			height:1px !important;
			border:0;
			background-color:#ccc !important;
		}
		/*END*/
		
        tbody.info-tabletd td {
         padding: 5px 5px; 
         font-size: 12px;
        }
        body{
         font-family: "Roboto", sans-serif !important;
         font-size: 15px;
        }
         .tabs-class th {
			padding: 5px 10px;
			border: 1px solid #e0e2e8;
			color:#fff;
			text-align:left;
			font-size:11px !important;
         }
         .tabs-class td {
			padding: 5px 10px;
			border: 1px solid #e0e2e8 !important;
         }
         .amttable {
			taxt-align:center; 
         }
         .tabs-class {
			font-size: 12px;
			width:100%;
			/*border: 1px solid #bbb !important;*/
         }
         p{
			margin: 3px 0;
         }
         .one-eaigh {
			text-align: right;
         }
         .three-eaigh {
			text-align: right; 
         }
         .zero {
			text-align: right;  
         }
         .yello-clr{
			background: #fffdd7;
         }
			.inword {
			font-size:13px !important;
			text-align:left;
		}
     .paybtn{
		color: #fff !important;
        padding: 8px;
        background: #7864e9;
        border-radius: 1px;
        /*border: 1px solid #7864e9;*/
        margin: 10px 0;
     }

     @media print {
          html, body {
              width: 210mm;
              height: 297mm;        
          }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
          }
      }
       .cmp-logo {
          max-width:200px !important;
          max-height: 50px !important;
        }
        tbody.info-tabletd td {
             padding: 5px 5px;
             font-size: 12px;
         }
    table.infotable {
      width: 100%;
    } 
    
    /* Font-color & Font-size of content */
    .txt_dwld{
      color : <?=$custom_inv[0]->font_color?>;
      /*font-size : <?//=$custom_inv[0]->font_size?> !important;*/
    font-size:13px; 
    }
    /* Font-color & font-size of Footer */
    .dwn_ftr{
      color : <?=$custom_inv[0]->ftr_font_color?>;
      /*font-size : <?//=$custom_inv[0]->ftr_font_size?> !important;*/
    font-size:13px;
    }
    
    /* Margin to Template */
    #view-page{
      margin : 0in <?=$custom_inv[0]->inv_margins?>in <?=$custom_inv[0]->inv_margins?>in <?=$custom_inv[0]->inv_margins?>in;
    }
  
  .txt_pnt{
    font-size:13px;
    text-align:center;
  }
  
  #up_info td {
    padding-top: 1px !important;
    padding-bottom: 1px !important;
  }

  table.no-spacing {
    border-spacing:0; /* Removes the cell spacing via CSS */
    border-collapse: collapse;  /* Optional - if you don't want to have double border where cells touch */
  }
  
  .paybtn{
	color: #fff;
	padding: 5px 8%;
	background: #7864e9;
	border-radius: 5px;
	border: 1px solid #7864e9;
	margin: 6px -3px;
  }
  
  .al-right{
	text-align:right;
  }
		
		.big-inv{
			color:#000;
			margin-top:0px !important;
			font-size:15px;
		}
		
		.inv-to{
			font-size:15px;
			color:#000;
		}
		
		.mod-pay{
			color:#000;
			font-size:12px;
		}
		
		.foot{
			background-color:#7864e9;
			margin:-4% 0% 0 0%;
			padding:1px 1%;
		}
		
		.foot_lbl{
			font-size:13px;
			color:#fff;
		}
		
		.xebra{
			color:#fff;
			margin:3% -2%;
		}
		
		.add{
			margin:10px 0 0 0;
		}
		
		.add label{
			font-size:13px;
			color:#fff;
		}
		
		.pan{
			margin:1.5% 0%;
		}
		
		.term{
			color:#fff;
			font-size:13px;
			padding: 0 0 0 10px;
		}
		
		.terms-container{
			margin: 0 0 0 -5px;
		}
		
		.term-head{
			padding-left:15px !important;
		}
  
    </style>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/js/index.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	var USD='<?php print $currencycode[0]->currencycode; ?>';
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		$('#to_words_currency').html(USD+".");
		$('.par-spa rightss cemterfd').html(USD+".");

	var final_grt=$('#tot_amount').text();
    var words=toWords(parseFloat(final_grt));
    if(words==''){
          $('#to_words_inv').text('ZERO');
      }else{
           $('#to_words_inv').text(words.toUpperCase()+' ONLY');
      }
	});
	</script>
	</head>
	<body class="layout-semi-dark bg-theme-gray sales-invoice easy-tab1 my-easy-1" id="view-page"  bgcolor="<?=$custom_inv[0]->tmp_back_color?>">
      <!-- START MAIN -->
      <div id="main page">
      <div class="wrapper ">
      <!-- START CONTENT -->
      <section id="content">
		<div class="container">
			<?php $USD=$currencycode[0]->currencycode; ?>
			<table border="0" id="" class="infotable" style="margin-top:10px !important;">
				<tr>
					<td width="30%">
						<?php $tax="true";
						if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
							$tax="false"; 
						}else{
							$tax="true";
						}

						$inv_document_type='invoice';
						$sub_tbl='';
						if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
							$inv_document_type="estimate-invoice";$sub_tbl='estl';
						}else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
							$inv_document_type="proforma-invoice";$sub_tbl='prol';
						}else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
							$inv_document_type="invoice";$sub_tbl='invl';
						}
						if($custom_inv[0]->hdr_logo!=0){
						?>
						<img width="110" height="65" src="<?php //echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" alt="comp-logo"/>
						<?php } ?>
					</td>
					<td width="15%">
						<div style="text-align:right; padding:2px 0;">
							<p class="big-w footer_p"><b>Phone</b></p>
							<p class="small-w footer_p"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></p>
						</div>
					</td>
					<td width="5%">
						<div>
							<img style="margin-top:0px;" width="30" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/phone-b.png" alt="phone">
						</div>
					</td>
					<td width="15%">
						<div style="text-align:right; padding:2px 0;">
							<p class="big-w footer_p"><b>Email</b></p>
							<p class="small-w footer_p"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></p>
						</div>
					</td>
					<td width="5%">
						<div>
							<img style="margin-top:0px;" width="30" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/email-b.png" alt="email">
						</div>
					</td>
					<td width="25%">
						<div style="text-align:right; padding:2px 0;">
							<p class="big-w footer_p"><b>Address</b></p>
							<p class="small-w footer_p"><?php echo wordwrap($billing_doc[0]->inv_address,35,"<br>\n"); ?></p>
						</div>
					</td>
					<td width="5%">
						<div>
							<img style="margin-top:0px;" width="30" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/add-b.png" alt="add">
						</div>
					</td>
				</tr>
			</table>
			<table id="inv_details_table" cellspacing="0" width="100%">
				<tbody class="info-tabletd">
					<tr class="inv_de_table" style="background-color:#298048 !important;">
						<td style="font-size:12px;" width="20%">
							<?php if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
								<p class="white-color"><b>ESTIMATE</b></p>
							<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
								<p class="white-color"><b>PROFORMA INVOICE</b></p>
							<?php }else { $inv="Sales Invoice";?>
								<p class="white-color"><b>SALES INVOICE</b></p>
							<?php } ?>
						</td>
						<td width="28%">
							<p class="white-color big-w eoe_div temp_text"><b> E & OE </b></p>
							<p class="white-color big-w temp_text"><b>BILLED TO:</b></p>
							<p class="white-color big-w temp_text"><b><?=  @ucwords($billing_doc[0]->cust_name); ?>,</b></p>
							<p class="white-color small-w temp_text"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.', '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></p>
							<p class="white-color small-w temp_text"><?php $sstate = ($billing_doc[0]->sstate)?', '.$billing_doc[0]->sstate:'';?></p>
							<p class="white-color small-w temp_text"><?php $scountry = ($billing_doc[0]->scountry)?' - '.$billing_doc[0]->scountry:''; ?></p>
						</td>
						<td width="24%">
							<p class="white-color big-w temp_text"><b><?//=strtoupper($inv)?>INVOICE NO: </b><label style="padding-top:5px;" class="white-color small-w temp_text"><?=  @$billing_doc[0]->	inv_invoice_no_view; ?></label></p>
							<p class="big-w temp_text white-color"><b>DATE OF ISSUE: </b><label style="padding-top:5px;" class="small-w temp_text white-color"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></label></p>
						</td>
						<td width="28%">
							<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<p class="white-color pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b><label style="padding-top:5px;" class="pu_no small-w temp_text white-color"><?=  @$billing_doc[0]->inv_po_no; ?></label></p>
							<?php } ?>
							<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<p class="pu_o_date big-w temp_text white-color"><b>ESTIMATE/P.O. DATE: </b><label style="padding-top:5px;" class="pu_o_date small-w temp_text white-color"><?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></label></p>
							<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
			<style>
				<?php 
					if(round($billing_doc[0]->inv_discount_total)==0){?>
                    .zerodiscount{
						display: none;
                     }
					<?php } ?>
                    <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                   <?php } ?>
                   <?php 
					if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                    }
                   <?php } ?>
                   <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                   <?php } ?>
                   <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                   <?php } ?>
                   <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                   <?php } ?>
			</style>
			<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 10px 0px 10px !important; margin:-2% 0 0 0;">
				<table id="default_1" class="tabs-class no-spacing" cellspacing="0">
					<thead>
						<tr class="ls_tem_tr" id="">
							<th class="" style="width:180px !important; padding-left:6px !important;">
								<p class="big-w temp_text">PARTICULARS</p>
							</th>
                            <th class="amttable">
								<p class="big-w temp_text">SAC</p>
                            </th>
                            <th class="amttable">
								<p class="big-w temp_text">QNTY</p>
							</th>
                            <th class="amttable">
								<p class="big-w temp_text" style="">RATE</br>(<?=$USD?>)</p>
							</th>
                            <th class="amttable">
								<p class="big-w temp_text" style="">TAXABLE</br>(<?=$USD?>) </p> 
                            </th>
                            <th class="amttable">
								<p class="big-w temp_text">CGST</br>(<?=$USD?>)(%)</p>
                            </th>
                            <th class="amttable">
								<p class="big-w temp_text">SGST</br>(<?=$USD?>)(%)</p>
                            </th>
                            <th class="amttable">
								<p class="big-w temp_text">AMOUNT</br>(<?=$USD?>)</p>
                            </th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
					<tr>
						<td class="" style="width:180px !important;">
							<p class="big-w temp_text"><b><?php //$invkey+1;
							$temp=$sub_tbl.'_service_type'; ?>
							<?php if($invlist->$temp==3){
								echo "Equalisation Levy";
							}else if($invlist->$temp==4){
								echo "Late Fee";
							}else if($invlist->$temp==2){
								echo "Expense Voucher";
							}else{
								foreach($my_services as $service){
								if($service->service_id==$invlist->service_id){
									echo $service->service_name;
								}
								}
							}?></b></p>
							<?php  
							$particulars=$sub_tbl.'_particulars'; 
							$hsn=$sub_tbl.'_hsn_sac_no'; 
							$quantity=$sub_tbl.'_quantity'; 
							$rate=$sub_tbl.'_rate'; 
							$discount=$sub_tbl.'_discount'; 
							$taxable_amt=$sub_tbl.'_taxable_amt'; 
							$igst=$sub_tbl.'_igst'; 
							$igst_amt=$sub_tbl.'_igst_amt'; 
							$cgst=$sub_tbl.'_cgst'; 
							$cgst_amt=$sub_tbl.'_cgst_amt'; 
							$sgst=$sub_tbl.'_sgst'; 
							$sgst_amt=$sub_tbl.'_sgst_amt'; 
							$cess=$sub_tbl.'_cess'; 
							$cess_amt=$sub_tbl.'_cess_amt'; 
							$other=$sub_tbl.'_other'; 
							$other_amt=$sub_tbl.'_other_amt'; 
							$amount=$sub_tbl.'_amount'; 
							?>
							<p class="small-w temp_text"><?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?></p>
						</td>
						<td class="amttable">
							<p class="small-w temp_text"><?=$invlist->$hsn;  ?></p>
						</td>
						<td class="amttable">
							<p class="big-w temp_text"><?=$invlist->$quantity;  ?></p>
						</td>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$rate);  ?></p>
						</td>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$taxable_amt);  ?></p> 
						</td>
						<?php if($tax=="true") { ?>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$cgst_amt);  ?></br>(<?=$invlist->$cgst.'%';  ?>)</p>
						</td>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$sgst_amt);  ?></br>(<?=$invlist->$sgst.'%';  ?>)</p>
						</td>
						<?php } else { ?>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$igst_amt);  ?></br>(<?=$invlist->$igst.'%';  ?>)</p>
						</td>
						<?php } ?>
						<td class="amttable">
							<p class="big-w temp_text"><?=round($invlist->$amount); ?></p>
						</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<hr class="bu_pay">
			<table id="up_info" class="infotable" style="margin-top:1%;">
				<tbody class="info-tabletd">
					<tr>
						<td width="20%" style="margin:-20px 0 10px 0;">
							<div>
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>TERMS OF PAYMENT</b></p>
								<p class="small-w temp_text"><?=@$cust_pan[0]->cust_credit_period;?></p>
							</div>
						</td>
						<td width="20%" style="margin:-20px 0 10px 0;">
							<div>
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN</b></p>
								<p class="small-w temp_text"><?= @$gstno_cli[0]->gst_no; ?></p>
							</div>
						</td>
						<td width="20%" style="margin:-20px 0 10px 0;">
							<div>
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY</b></p>
								<p class="small-w temp_text"><?=$place?></p>
							</div>
						</td>
						<td colspan="2" width="40%">
							<table>
								<tbody>
									<tr>
										<td width="20%">
											<p style="" class="big-w temp_text"><b>SUBTOTAL</b></p>
										</td>
										<td width="20%">
											<p style="text-align:right;" class="big-w temp_text"><?=$USD?>&nbsp; <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_taxable_total)?></p>
										</td>
									</tr>
									<tr>
										<td width="20%">
											<p class="big-w temp_text"><b>DISCOUNT</b></p>
										</td>
										<td width="20%">
											<p style="text-align:right;" class="big-w temp_text"><?=$USD?> <?php echo $billing_doc[0]->inv_discount_total ?></p>
										</td>
									</tr>
									<tr class="zerocess">
										<td width="20%">
											<p class="big-w temp_text"><b>CESS</b></p>
										</td>
										<td width="20%">
											<p style="text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?></p>
										</td>
									</tr>
									<tr class="zeroother">
										<td width="20%">
											<p class="big-w temp_text"><b>OTHER</b></p>
										</td>
										<td width="20%">
											<p style="text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?></p>
										</td>
									</tr>
									<tr>
										<td>
											<p style="margin-bottom:3px; color:#000;" class="big-w temp_text"><b>GRAND TOTAL</b></p>
										</td>
										<td>
											<p style="margin-bottom:10px; color:#000; text-align:right;" class="big-w temp_text"><?=$USD?>&nbsp; <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></p>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<p style="color:#000;" class="small-w temp_text">IN WORDS: <?=$USD ?>&nbsp; <?php echo strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)); ?></p>
										</td>
									</tr>
								</tbody>
							</table>
							<hr>
						</td>
					</tr>
				</tbody>
			</table>
			
			<table id="up_info" class="infotable" style="margin-top:1%;">
				<tbody class="info-tabletd">
					<tr>
						<td colspan="3">  
							<div class="bank_info">
								<p class="big-w temp_text">BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
								<p class="big-w temp_text">ACCOUNT NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
								<p class="big-w temp_text">BRANCH NAME: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
								<?php if($USD=="INR"){?>
									<p class="big-w temp_text">IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
								<?php } else { ?>
									<p class="big-w temp_text">SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
								<?php } ?>
							</div>
						</td>
						<td width="25%">
							<div>
								<button class="make-payment temp_text" class="btn" style="padding:5px 20px; border-radius:5px; background-color:#009933;"><a class="mk_pay" href="<?=@$payment_url?>" style="color:#fff; font-size:12px; text-decoration:none;">MAKE PAYMENT</a></button>
							</div>
						</td>
					</tr>
				</tbody>				
			</table>
			
			<table id="up_info" class="infotable" style="margin-top:1%;">
				<tbody class="info-tabletd">
					<tr>
						<td width="30%">
							<div>
								<p class="big-w temp_text cinno">CIN: <?= $company[0]->bus_cin_no; ?></p>
								<p class="big-w temp_text pan-1">PAN: <?= $company[0]->bus_pancard; ?></p>
								<p class="big-w temp_text gst_1">GSTIN: <?php echo $gstno[0]->gst_no; ?></p>
							</div>
						</td>
						<td width="30%">
							
						</td>
						<td colspan="2" width="40%">
							<?php if($custom_inv[0]->inv_signature){?>
								<div style="text-align:center; margin-right:10px; margin-top:10px;">
									<p class="big-w" style="padding-bottom:5px !important;">FOR <?= strtoupper($company[0]->bus_company_name) ?></p>
									<label> &nbsp </label><br>
									<?php if($billing_doc[0]->inv_signature !=""){?>
										<img width="180" height="50" src="" style="margin:0px;" class="signature_img" alt="signature">
									<?php } ?>
									<hr class="bu_pay">
									<p class="big-w"><label class="big-w"><?php echo strtoupper($billing_doc[0]->inv_sign_name) ?></label><label class="big-w"><b><?php //echo strtoupper($billing_doc[0]->inv_sign_designation) ?></b></label></p>
								</div>
							<?php }?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
      </section>
      </div>
      </div>
      <!-- END MAIN -->
	</body>	
	<?php $tbrow = count($billing_doc);?>
	<?php if($tbrow<2){?>
		<footer style="margin:20% -2% 0 -2%;">
	<?php } ?>
	<?php if($tbrow>=3){?>
		<footer style="margin:3% -2% 0 -2%;">
	<?php } ?>
		<table id="up_info" class="infotable" style="margin-top:2%;">
				<tbody class="info-tabletd">
					<tr>
						<td colspan="4" width="100%">
							<p style="color:#000;" class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
							<div id="cond" style="margin:0 0 0 -65px;">
								<ul style="color:#000;" class="term-list" style="margin-left:-70px;">
									<?= @$billing_doc[0]->inv_terms; ?>
								</ul>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="up_info" class="no-spacing infotable" style="margin-top:1%;" cellspacing="0">
				<tbody class="info-tabletd">
					<tr style="background-color:#009933;">
						<td></td>
						<td width="25%">
							<p style="text-align:center; color:#fff;"><img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook.png" alt="fb"></p>
							<p style="margin-top:-10px; text-align:center; color:#fff;" class="small-w footer_p facebk"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></p>
						</td>
						<td width="25%">
							<p style="text-align:center; color:#fff;"><img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter.png" alt="twit"></p>
							<p style="margin-top:-10px; text-align:center; color:#fff;" class="small-w footer_p twitter"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></p>
						</td>
						<td width="30%">
							<p style="text-align:center; color:#fff;"><img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn.png" alt="link"></p>
							<p style="margin-top:-10px; text-align:center; color:#fff;" class="small-w footer_p link_id">
							<?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></p>
						</td>
						<td width="20%">
							<p style="text-align:center; color:#fff;"><img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/web.png" alt="link"></p>
							<p style="text-align:center; color:#fff;" class="small-w footer_p link_id"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></p>
						</td>
					</tr>
				</tbody>				
			</table>
	</footer>
	<script type="text/javascript">
	$(document).ready(function() {
	//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
	var USD='<?php print $currencycode[0]->currencycode; ?>';
      $('#tr_rate').html(USD);
      $('#tr_discount').html(USD);
      $('#tr_tax').html(USD);
      if(USD=="INR"){
        $('#to_words_currency').html("RUPEES.");
      }else{
      $('#to_words_currency').html(USD+".");
	  }
      $('.par-spa rightss cemterfd').html(USD+".");
	});
	</script>
</html>
