<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
         padding-left: 0px !important;
         }
      </style>
      <STYLE TYPE="text/css" >
           
	  @media only screen and (min-width: 993px){
		.container {
			width: 95%;  
			margin: 20px 35px !important;
			opacity: 999999;
			z-index: 99999;
		}
         }
        .container{
            background: url(Temp-2-1.jpg) no-repeat center;
            background-size: cover;
            min-height: 1000px;
        }
        #fifth-template p{
            margin:0;
            line-height: 1em;
            font-size: 15px;
        }
        .invoice-dets{
            width: 280px;
            height:150px;
        }
        .invoice-dets > p{
            font-size: 14px !important;
        }
        #inv .pl-5{
            font-size: 50px !important;
            font-weight: 400;
            letter-spacing: 0.1em;
        }
        .sidebar{
            height:1000px; 
            position: relative;
        }
        .to{
            position:absolute;
            bottom:0;
        }
        #fifth-template #tem-fifth-table {
            margin-top: 250px !important;
			width:80% !important;
			margin-left:5% !important;
        }
        #fifth-template #tem-fifth-table td,th{
            max-width: 280px;
            min-width: 80px;
            padding: 13px 10px 13px 0;
        }
        #fifth-template #tem-fifth-table tr{
            border-bottom: 1px solid black;
        }
        #payment{
            line-height: 0.5em;
            font-size: 13px;
        }
        #gst{
            width:200px;
			background-color:#ffcc00 !important;
        }
        #gst p{
            margin-bottom: 0px !important;
            padding-bottom: 0px !important ;
            padding-right: 4px;
            
        }
        #total{
            width:250px;
            border-top:1px black solid;
            font-size: 18px;
        }*/
		
		.text-right{
			text-align:right !important;
		}

		table.infotable {
			width: 100%;
		} 
    </style>
		</head>
		<body>
		<div class="container" style="width:90%;">
        <div class="row">
			<table class="infotable" id="">
				<tr>
					<td colspan="2">
						<p class="" style="font-size:40px; margin-bottom:0px !important; margin-left:10px;" id="inv">I N V O I C E</p>
						<div class="col s12 m12 l6 float-left pl-4 text-left invoice-det font-weight-bold">
							<p>INVOICE NO.: <span>89077853</span></p>
							<p>INVOICE DATE: <span>07, April,2018</span></p>
							<p>P.O. NUMBER: <span>#13245</span></p>
							<p>P.O. DATE: <span>25 October,2018</span></p>
							<p>GSTIN: <span>112233255</span></p>
							<p>PAN: <span>898789JH89</span></p>
							<p>CIN: <span>8909898</span></p>
						</div>
					</td>
					<td colspan="2">
						LOGO HERE
					</td>
				</tr>
			</table>
		</div>
		<div class="row">
            <div class="col s12 m12 l12">
                <table id="tem-fifth-table" style="width:100%;">
                    <tr class="" style="border-bottom:none;">
                        <th class="text-left"><b>SL NO.</b></th>
						<th class="text-left"><b>ITEM DESCRIPTION</b></th>
                        <th><b>PRICE</b></th>
                        <th><b>QNTY</b></th>
                        <th><b>TOTAL</b></th>
                    </tr>
                    <tr class="text-center">
                        <td>1</td>
						<td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>2</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>3</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>4</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>5</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                </table>
			</div>
		</div>
        <div class="row" style="margin-top:5%;">
			<table class="infotable" id="">
				<tbody>
					<tr>
						<td colspan="2">
							<div class="">
								<p class="m-1 font-weight-bold"><b>INVOICE TO:</b></p>
								<label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Suhas Sawant</label><br>
								<label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Accounts Dept.</label><br>
								<label style="color:#000 !important; font-size:13px !important;" class="m-1">Windchimes Communications</label><br>
								<label style="color:#000 !important; font-size:13px !important;" class="m-1">Bandra East</label><br>
								<label style="color:#000 !important; font-size:13px !important;" class="m-1">Mumbai-400051</label><br>
							</div>
						</td>
						<td>
							<div style="padding-top:5%;">
							<a href="#" class="btn purple white-text btn-lg my-2" style="background-color:#dddee2">MAKE PAYMENT</a></br>
							<p id="ac-number"><b>A/C NUMBER:</b> 01992823738</p>
							<p id="bank-name"><b>BANK NAME:</b> ICICI Bank</p>
							<p id="branch"><b>BRANCH:</b> Bandra East</p>
							<p id="ifsc"><b>IFSC CODE:</b> ICCI202928</p>
							</div>
						</td>
						<td>
							<div style="text-align:right;">
							<p class="font-weight-bold h6 text-right pr-3"><b>SUB TOTAL:</b> 1,250,000</p>
							<p><b>+SGST(9%):</b> 112,500</p>
							<p><b>+CGST(9%):</b> 112,500</p>
							<h6 class="h4"><b>TOTAL:</b> 1,475,000</h6>
						</td>
					</tr>
				</tbody>
			</table>
       </div>
        
        <div class="footer pb-3">
            <p style="font-size:14px;"><b>TERMS & CONDITIONS:</b></p> 
			<p>Payment should be made within 30 days of the invoice date and either made via bank transfer or cheque</p>
        </div>
    </div>
	</body>