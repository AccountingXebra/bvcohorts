	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <title>Xebra</title>
	  <style TYPE="text/css" MEDIA="screen, print">
		@import url(<?php echo base_url(); ?>asset/materialize/css/materialize.css);
        /*@import url(<?php echo base_url(); ?>asset/css/style.css);*/
		
         
		@media only screen and (min-width: 993px){
			.container {
				width: 80%;
			}
        }
	 </style>
     <style type="text/css">
        body{
			margin: 10px 10px 10px 25px !important;
        }
		
		#main{
         padding-left: 20px !important;
        }
		 
		@media print {
			padding-left: 20px !important;
		}
		 
		@page {
            size: auto; 
            margin: 0mm;  
        }
				#third-template p{
					font-size:13px !important;
				}
				.header1{
					height:140px;
				}
				.header2{
					height:220px;
					padding-top:15px;
				}
				.invoice{
					line-height: 1em;
					font-size: 13px;
				}
				.invoice-details{
					line-height: 1.4em;
					font-size: 13px;
				}
				.dp{
					color:#000;
				}
				#table-first tr, th, td{
					height: 78px;
					border: 0.8px solid black;
					font-size: 13px;
					line-height: 1em;
				}
				#table-first{
					width: 100%;
					padding-bottom: 50px;
					margin:5px 0 !important;
				}
				.header1, .header2{
					background-color: #262930 !important;
				}
				
				#table-first tbody tr{
					background-color:#fff !important;
				}
				
				#table-first td{
					max-width: 450px;
					padding-top:20px;
					padding-bottom: 20px;
					padding-right: 8px;
				}
				.desc{
					font-size: 13px;
				}
				#payment{
					line-height: 0.5em;
					font-size: 13px;
				}
				#sign{
					border-bottom: 1px black solid;
					height: 36px;
					width:213px;
					
				}
				#gst{
					width:270px;
					border-bottom: 1px black solid;
					background-color: #fa3b59;
					padding:10px;
					margin-left:50%;
				}
				#gst p{
					margin-bottom: 0px !important;
					padding-bottom: 0px !important ;
					padding-right: 4px;
					
				}
				#total{
					width:270px;
					border-top:1px black solid;
					font-size: 18px;
					margin-left:50%;
				}
				.footer{
					margin-top: 15px;
					border-top: 1px solid #000;
				}
				.footer p{
					background-color: #ebf0ec;
					font-size: 13px;
					margin-bottom: 0px;
				}
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;
					line-height: 1em;
					font-size: 15px;
				}
				#social, #company{
					height:43px;
				}
				.btn, footer{
					background-color: #ebf0ec;
				}
				
				h4, .m-1{
					color:#000;
				}
				
				.paybtn{
					color: #595959;
					padding: 0 8%;
					background: #0059b3;
					border-radius: 5px;
					border: 1px solid #0059b3;
					margin: 6px -3px;
				 }
				 
				 .row{
					 margin-bottom:0px !important;
				 }
			</style>
			<script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
		}
		$('.par-spa rightss cemterfd').html(USD+" ");

		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
			$('#to_words_inv').text('ZERO');
		}else{
			$('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
		});
 </script>
			</head>
		<body>
		<div class="container">
		<?php
			$USD=$currencycode[0]->currencycode; 
		?>
        <div class="row pt-3 pb-4 header1 text-white" style="border-bottom: 7px solid #000; margin-top:4%;">
			<div class="col l2" style="padding:5% 0;">
				<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                          if($custom_inv[0]->hdr_logo!=0){?>
							<img style="margin-top:-15px;" width="150" height="55" src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="company_logo"/>
				<?php } ?>
			</div>	
            <div class="col l4">
				<h6 class="pl-2"><?php echo strtoupper($company[0]->bus_company_name); ?></h6>
				<label style="color:#595959; font-size:13px;"><?php echo wordwrap($billing_doc[0]->inv_address,50,"<br>\n"); ?></label></br>
				<label class="up_lbl"><b>TEL. NO.&nbsp&nbsp&nbsp: </b></label><label class="txt_ftr"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
				<label class="up_lbl"><b>EMAIL ID&nbsp&nbsp: </b></label><label class="txt_ftr"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
			</div>
			<div class="col l6 text-right" style="float:right !important;">
                <?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
					<h4 class="pr-2" style="font-weight:800; margin-top:2.1rem; color:#595959;">ESTIMATE</h4>
				<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
					<h4 class="pr-2" style="font-weight:800; margin-top:2.1rem; color:#595959;">PROFORMA INVOICE</h4>
				<?php }else { $inv="Sales Invoice";?>
					<h4 class="pr-2" style="font-weight:800; margin-top:2.1rem; color:#595959;">SALES INVOICE</h4>
				<?php } ?>
            </div>
        </div>
		<hr>
        <div class="row pt-3 header2 text-white" style="margin-top:-20px;">
            <div class="col l6 pl-3 invoice">
                <h6 class="big-inv"><b>E&OE </b></h6>
				<p class="dp" style="font-size:15px;">INVOICE TO:</p>
                <p class="font-weight-bold m-1"></p>
                <!--p class="font-weight-bold m-1"><b>Accounts Dept.</b></p-->
                <p class="m-1" style="line-height:20px;"><b><?=  @ucwords($billing_doc[0]->cust_name); ?>,</b></br><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.','.$billing_doc[0]->state_name.','.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,38,"<br>\n"); ?></p>
				<p class="m-1"><span class="dp">GSTIN: </span><?= @$gstno_cli[0]->gst_no; ?></p>
				<p class="m-1"><span class="dp">PAN: </span><?= @$cust_pan[0]->cust_pancard; ?></p>
            </div>
            <div class="col l6 text-left pr-3 invoice-details text-right" style="float:right !important;">
                <p class="m-1"><span class="dp"><?=strtoupper($inv)?> TO: </span> <?=  @$billing_doc[0]->inv_invoice_no_view; ?></p>
                <p class="m-1"><span class="dp"><?=strtoupper($inv)?> DATE: </span> <?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></p>
				<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
                <p class="m-1"><span class="dp">ESTIMATE / P.O. NO:</span> <?=  @$billing_doc[0]->inv_po_no; ?></p>
				<?php } ?>
				<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
                <p class="m-1"><span class="dp">ESTIMATE / P.O. DATE:</span> <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
				<?php } ?>
				<p class="m-1"><span class="dp">PLACE OF SUPPLY: </span> <?=$place?></p>
				<p class="up_lbl">TERMS OF PAYMENT: </label><label class="txt_pnt"><?=@$cust_pan[0]->cust_credit_period;?></p>
            </div>
        </div>
        <div class="row">
					<style>
					<?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                      
                      </style>
        <table id="table-first">
            <thead>
			<tr class="text-center" style="background-color: #ebf0ec;">
                <th class="text-left">ITEM DESCRIPTION</th>
                <th>SAC NO.</th>
                <th>QNTY</th>
                <th>RATE</th>
                <th>DISCOUNT</th>
                <th>TOTAL</th>
            </tr>
			</thead>
			<tbody>
			<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
            <tr class="text-center">
                <td class="text-left"><span class="font-weight-bold d-block heading">
				<b><?php //$invkey+1; 
						$temp=$sub_tbl.'_service_type'; ?>
						<?php if($invlist->$temp==3){
                        echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
                        echo "Late Fee";
                        }else if($invlist->$temp==2){
                        echo "Expense Voucher";
                        }else{
                        foreach($my_services as $service){
                        if($service->service_id==$invlist->service_id){
                        echo $service->service_name;
                        }
                        }
                        }?></b>
				</span></br>
				<span class="desc"><?php  
                        $particulars=$sub_tbl.'_particulars'; 
                        $hsn=$sub_tbl.'_hsn_sac_no'; 
                        $quantity=$sub_tbl.'_quantity'; 
                        $rate=$sub_tbl.'_rate'; 
                        $discount=$sub_tbl.'_discount'; 
                        $taxable_amt=$sub_tbl.'_taxable_amt'; 
                        $igst=$sub_tbl.'_igst'; 
                        $igst_amt=$sub_tbl.'_igst_amt'; 
                        $cgst=$sub_tbl.'_cgst'; 
                        $cgst_amt=$sub_tbl.'_cgst_amt'; 
                        $sgst=$sub_tbl.'_sgst'; 
                        $sgst_amt=$sub_tbl.'_sgst_amt'; 
                        $cess=$sub_tbl.'_cess'; 
                        $cess_amt=$sub_tbl.'_cess_amt'; 

                        $other=$sub_tbl.'_other'; 
                        $other_amt=$sub_tbl.'_other_amt'; 
                        $amount=$sub_tbl.'_amount'; 
                        ?>
						<?php echo wordwrap($invlist->$particulars,60,"<br>\n"); ?></span>
				</td>
                <td><?=$invlist->$hsn;  ?></td>
                <td><?=$invlist->$quantity;  ?></td>
                <td><?=round($invlist->$rate);  ?></td>
                <td><?=round($invlist->$discount);  ?></td>
                <td><? echo ($invlist->$rate)-($invlist->$discount) ?></td>
            </tr>
			<?php } ?>
            <!--tr class="text-center">
                <td class="">1</td>
                <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit. Modi </span></td>
                <td>236545</td>
                <td>23</td>
                <td>200,000</td>
                <td>25%</td>
                <td>150,000</td>
            </tr-->
			</tbody>
        </table>
        </div>
        <div>
        <div class="row">
			<div class="col l12">
			<label style="color:#000;"><b>CIN:</b></label><label style="color:#000;"> <?= $company[0]->bus_cin_no; ?></label>&nbsp;
			<label style="color:#000;"><b>PAN NO.:</b></label><label style="color:#000;"> <?= $company[0]->bus_pancard; ?></label>&nbsp;
			<label style="color:#000;"><b>GST:</b></label><label style="color:#000;"> <?= $gstno[0]->gst_no; ?></label>
			</div>
		</div>
		<div class="row">
			<div class="col l6" id="payment">
				<p style="margin-top:25px;"></p>
                <p id="ac-number"><b>A/C NUMBER:</b> <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
                <p id="bank-name"><b>BANK NAME:</b> <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
                <p id="branch"><b>BRANCH:</b> <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
				<?php if($USD=="INR"){?>
					<p id="ifsc"><b>IFSC CODE:</b> <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
				<?php } else { ?>
					<p id="ifsc"><b>SWIFT CODE:</b> <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
				<?php } ?>
            </div>
            <div class="col l6" style="padding-left:25%; font-size:13px !important;">
                <p style="float:right;" class="float-right font-weight-bold h4"><b>SUB TOTAL:</b> <?= $USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></p>
                <div class="pl-2 float-right text-left" id="gst">
					<?php if($tax=="true") { ?>
                    <p><b>+SGST(9%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
                    <p><b>+CGST(9%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
					<?php } else { ?>
					<p><b>+IGST(18%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
					<?php } ?>
					<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
					<p><b>+CESS:</b> <?=$USD?> <?= round(@$billing_doc[0]->inv_cess_total); ?></p>
					<?php } ?>
					<?php if(round($billing_doc[0]->inv_other_total)>0){?>
					<p><b>+OTHER:</b> <?=$USD?> <?= round(@$billing_doc[0]->inv_other_total); ?></p>
					<?php } ?>
                </div>
                <div class="clearfix"></div>
                <div class="font-weight-bold float-right text-left pl-3" id="total">
                    <p style="font-size:23px !important;"><b>TOTAL: <?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></b></p>
                </div>
			</div>
		</div>
        <div class="row">
            <div class="" style="text-align:right; margin-right:20px;">
				<label class="up_lbl" style="color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label>
				<p class="txt_pnt" style="padding-top:1px;"><?//= @$billing_doc[0]->inv_signature; ?></p>
				<label class="txt_pnt" style="color:#000 !important;"><?= $billing_doc[0]->inv_sign_name; ?></label> <label style="color:#000 !important;" class="txt_pnt"><?//= $billing_doc[0]->inv_sign_designation; ?></label>
			</div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col s7 m7 l7">
					<p class="term-c"><b>TERMS & CONDITIONS</b> <br/>
						<span class=""><?= @$billing_doc[0]->inv_terms; ?></span>
					</p>
				</div>
				<div class="col s5 m5 15" style="padding:50px 0;">
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>FACEBOOK&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label>
					</div>
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>WEBSITE&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label>
					</div>
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>TWITTER&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label>
					</div>
					<label style="font-size:13px; color:#000 !important;" class="term"><b>LINKEDIN&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
				</div>
            </div>
        </div>
        <!--<footer class="container">
            
            <div class="row pb-5 pt-2">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>
        
        </footer>-->
        </div>
	</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script type="text/javascript">
		window.print();
	</script>