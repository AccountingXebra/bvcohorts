<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous"/>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
         padding-left: 0px !important;
         }
      </style>
      <style type="text/css" >
           
	  @media only screen and (min-width: 993px){
		.container {
			width: 95%;  
			margin: 20px 10px 20px 35px !important;
			opacity: 999999;
			z-index: 99999;
		}
         }
        #sixth-template p{
            font-size: 13px !important;
        }
		
		.text-left{
			text-align:left !important;
		}
		
        #head{
            height:145px;
        }
        #head-details{
            border-left: 3px black solid;
        }
        .comp1{
            width:92px;
            height:51px;
			font-size:13px !important;
        }
        .comp{
            width:92px;
        }
        li{
            margin:14px 0px !important;
        }
        .det-1{
            width:480px;
            background-color: #f0eeef;   
        }
        .det-2{
            width:384px;
            background-color: #f0eeef;
        }
        #six-tem-table{
            margin-top:0px ;
        }
        #six-tem-table td{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:91px;
			border:1px solid #ccc;
        }
		
		#six-tem-table th{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:30px;
			font-size:12px;
        }
		
        #six-tem-table td{
            padding:10px 5px;
			font-size:12px !important;
        }
        
        #six-tem-table tr:nth-child(even){
            background-color: #f0eeef
        }
        #summary{
            background: url(1140X100.png) no-repeat center;
            background-size: cover;
			background-color: #b3b3b3 !important;
            min-height:185px;
            max-height: 185px;
        }
        .footer{
            background-color: #f0eeef;
			font-size:13px !important;
			line-height:10px;
        }
		
		.bk-color-white{
			background-color:#fff !important;
		}
		
		table.infotable {
			width: 100%;
		} 
		
		body{
			font-family: 'Roboto', sans-serif !important;
		}
		
		p{
			font-size:11px !important;
			margin:2px 0 !important;
		}
		
		.paybtn{
			color: #fff !important;
			padding: 2px 8px;
			background: #ccc;
			border-radius: 5px;
			/*border: 1px solid #7864e9;*/
			margin: 10px 0;
		}
    </style>
	  <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
	  <script type="text/javascript" src="<?php echo base_url();?>public/js/index.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	var USD='<?php print $currencycode[0]->currencycode; ?>';
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		$('#to_words_currency').html(USD+".");
		$('.par-spa rightss cemterfd').html(USD+".");

	var final_grt=$('#tot_amount').text();
    var words=toWords(parseFloat(final_grt));
    if(words==''){
          $('#to_words_inv').text('ZERO');
      }else{
           $('#to_words_inv').text(words.toUpperCase()+' ONLY');
      }
	});
	</script>
</head>
<body>
    <div class="container" style="width:95%;">
		<?php $USD=$currencycode[0]->currencycode; ?>
		<table class="infotable">
			<tbody>
				<tr>
					<td style="width:15%">
						<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                        if($custom_inv[0]->hdr_logo!=0){?>
							<img height="40" width="60" src="<?php //echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="comp-logo">
						<?php } ?>
					</td>
					<td style="width:30%">
						<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate"; ?>
						<h5 class="black-text" style="margin-bottom: 10px; margin-left: 0px;">ESTIMATE</h5>
						<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Inv"; ?>
						<h5 class="black-text" style="margin-bottom: 10px; margin-left: 0px;">PROFORMA INVOICE</h5>
						<?php }else { $inv="Sales Inv"; ?>
						<h5 class="black-text" style="margin-bottom: 10px; margin-left: 0px;"><b>SALES INVOICE</b></h5>
						<?php } ?>
						<h6 style="margin:-10px 0;" class="big-inv"><b>E&OE </b></h6>
						<p class=""><i class="fa fa-map-marker mr-3" aria-hidden="true"></i><?=  @ucwords($billing_doc[0]->cust_name); ?>,</p>
						<p class="ml-4"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.',</br> '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,30,"<br>\n"); ?></p>
						<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<p style="font-size:11px; padding-left:0px;"><b>ESTIMATE / </br>P.O. NO::</b> <?=  @$billing_doc[0]->inv_po_no; ?></p>
						<?php } ?>
						<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<p style="font-size:11px; padding-left:0px;"><b><b>ESTIMATE / </br>P.O. DATE:</b></b> <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
						<?php } ?>
						<p style="font-size:11px; padding-left:0px;"><b>PLACE OF SUPPLY:</b> <?=$place?></p>
						<p style="font-size:11px; padding-left:0px; margin-top:-8px;"><b>TERMS OF PAYMENT:</b> </label><label class="txt_pnt"><?=@$cust_pan[0]->cust_credit_period;?></p>
					</td>
					<td colspan="2" style="padding-top:20px;">
						<div class="text-center d-inline-block comp1" style="float:left;">
							<!--i class="glyphicon glyphicon-calendar" aria-hidden="true"></i-->
							<img height="15" width="15" src="<?php //echo base_url(); ?>asset/images/calendar.png" class="cmp-logo" alt="logo">
							</img>
							<p><?=strtoupper($inv)?> </br>DATE:</p>
							<span class="list-inline-item py-0 comp"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></span>
						</div>
						<div class="text-center d-inline-block comp1" style="float:left;">
							<img height="20" width="20" src="<?php //echo base_url(); ?>asset/images/hash.png" class="cmp-logo" alt="logo"/>
							<p><?=strtoupper($inv)?> </br>NO:</p>
							<span class="list-inline-item py-0 comp"><?=  @$billing_doc[0]->inv_invoice_no_view; ?></span>
						</div>
						<div class="text-center d-inline-block comp1" style="float:left;">
							<img height="20" width="20" src="<?php //echo base_url(); ?>asset/images/hash.png" class="cmp-logo" alt="logo"/>
							<p>PAN<br>&nbsp;</p>
							<span class="list-inline-item py-0 comp"><?= @$cust_pan[0]->cust_pancard; ?></span>
						</div>
						<div class="text-center d-inline-block comp1" style="float:left;">
							<img height="20" width="20" src="<?php //echo base_url(); ?>asset/images/gst.png" class="cmp-logo" alt="logo"/>
							<p>GSTIN</br>&nbsp;</p>
							<span class="list-inline-item py-0 comp"><?= @$gstno_cli[0]->gst_no; ?></span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
        <div class="row" style="margin-top:2.5%;">
		<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                     
                      </style>
        <table id="six-tem-table" style="width:100%;">
            <thead>
				<tr>
					<th style="width:50%;" class="text-left py-2 pl-5">ITEM DESCRIPTION</th>
					<th style="width:13%;" class="py-2">QNTY</th>
					<th style="width:12%;" class="py-2">PRICE</th>
					<th style="width:12%;" class="py-2">DISCOUNT</th>
					<th style="width:13%;" class="py-2 pr-5">TOTAL</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
				<tr style="background-color:#ccc !important;">
					<td class="text-left pl-5"><p class="item-name h6 mb-0"><b>
					<?php $invkey+1;
                        $temp=$sub_tbl.'_service_type'; ?>
                        <?php if($invlist->$temp==3){
							echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
							echo "Late Fee";
						}else if($invlist->$temp==2){
							echo "Expense Voucher";
                        }else{
							foreach($my_services as $service){
								if($service->service_id==$invlist->service_id){
									echo $service->service_name;
                                }
                            }
                        }?></b></p>
					<p class="item-desc">
						<?php  
                            $particulars=$sub_tbl.'_particulars'; 
                            $hsn=$sub_tbl.'_hsn_sac_no'; 
                            $quantity=$sub_tbl.'_quantity'; 
                            $rate=$sub_tbl.'_rate'; 
                            $discount=$sub_tbl.'_discount'; 
                            $taxable_amt=$sub_tbl.'_taxable_amt'; 
                            $igst=$sub_tbl.'_igst'; 
                            $igst_amt=$sub_tbl.'_igst_amt'; 
                            $cgst=$sub_tbl.'_cgst'; 
                            $cgst_amt=$sub_tbl.'_cgst_amt'; 
                            $sgst=$sub_tbl.'_sgst'; 
                            $sgst_amt=$sub_tbl.'_sgst_amt'; 
                            $cess=$sub_tbl.'_cess'; 
                            $cess_amt=$sub_tbl.'_cess_amt'; 
                            $other=$sub_tbl.'_other'; 
                            $other_amt=$sub_tbl.'_other_amt'; 
                            $amount=$sub_tbl.'_amount'; 
                            ?>
                            <?php echo wordwrap($invlist->$particulars,65,"<br>\n"); ?>
					</p></td>
					<td><?=$invlist->$quantity;  ?></td>
					<td><?=$USD?> <?=round($invlist->$rate);  ?></td>
					<td><?=$USD?> <?=round($invlist->$discount);  ?></td>
					<td class="pr-5"><?=$USD?> <? echo $invlist->$quantity * $invlist->$rate - $invlist->$discount ?></td>
				</tr>
				<?php } ?>
			</tbody>
        </table>
		
		<table class="infotable" style="width:100%;">
			<tbody>
				<tr>
					<td colspan="2">
						<p style="background-color:#ccc; padding:15px 5px; margin-bottom:10px;" class="h6 pl-5 float-left"><b>SUMMARY</b></p>
						<p style="background-color:#fff !important; margin:0px -110px 0px -22px !important; height:120px; padding:15px 25px;">
							<label style="color:#000; font-size:13px !important;"><b>TOTAL DUE:</b></label><label style="font-size:13px !important; color:#000;"> <?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></label></br>
							<label style="color:#000;"><b>IN WORDS:</b></label><label style="color:#000;"> <?=$USD?> <?php echo wordwrap(strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)),40,"<br>\n"); ?></label></br>
						</p>
					</td>
					<td colspan="2">
						<div style="text-align:right; background-color:#ccc; padding:2px 40px 2px 10px; margin:-11.5% 0 0 -4px; width:100%;">
							<p class="h6"><b>SUB TOTAL:</b><span><?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></span></p>
							<?php if($tax=="true") { ?>
							<p class="font-weight-bold"><b>+ SGST:</b><span>+ <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></span></p>
							<p class="font-weight-bold"><b>+ CGST:</b><span>+ <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></span></p>
							<?php } else { ?>
							<p class="font-weight-bold"><b>+ IGST:</b><span>+ <?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></span></p>
							<?php } ?>
							<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
							<p class="font-weight-bold"><b>+ CESS:</b><span>+ <?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?></span></p>
							<?php } ?>
							<?php if(round($billing_doc[0]->inv_other_total)>0){?>
							<p class="font-weight-bold"><b>+ OTHER:</b><span>+ <?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?></span></p>
							<?php } ?>
							<p class="h5 pt-1"><b>GRAND TOTAL:</b><span style="font-size:14px !important"><?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></span></p>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<p id="bank-name"><span class="font-weight-bold">CIN: </span> <?= $company[0]->bus_cin_no; ?></p>
						<p id="branch-name"><span class="font-weight-bold">PAN NO.: </span> <?= $company[0]->bus_pancard; ?></p>
                        <p id="branch"><span class="font-weight-bold">GST: </span> <?= $gstno[0]->gst_no; ?></p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
					<td>
						<p id="bank-name"><span class="font-weight-bold">BANK NAME: </span> <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
						<p id="branch-name"><span class="font-weight-bold">BRANCH NAME: </span> <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
                        <p id="branch"><span class="font-weight-bold">A/C NUMBER: </span> <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
						<?php if($USD=="INR"){?>
							<p id="ifsc"><span class="font-weight-bold">IFSC CODE: </span><?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
						<?php } else { ?>
							<p id="swift"><span class="font-weight-bold">SWIFT CODE: </span><?= @$billing_doc[0]->inv_bank_swift_code;?></p>
						<?php } ?>
                        <button class="paybtn"><a class="btn btn-welcome mk_pay" href="<?=@$payment_url?>" style="text-decoration:none;">MAKE PAYMENT</a></button>
					</td>
					<td colspan="2">
						<div id="sign" style="float:right !important; margin-top:0.5%; margin-right:10%;">
							<?php if($custom_inv[0]->inv_signature){?>
								<div class="" style="text-align:right">
									<label class="up_lbl" style="font-size:10.5px !important; color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
									<p class="txt_pnt" style="padding-top:20px;"><?//= @$billing_doc[0]->inv_signature; ?></p><br>
									<label class="txt_pnt" style="color:#000 !important; font-size:11px !important;"><?= $billing_doc[0]->inv_sign_name; ?></label> <label></label> <label class="txt_pnt" style="font-size:11px !important; color:#000 !important;"><?//= $billing_doc[0]->inv_sign_designation; ?></label>
								</div>
							<?php } ?>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td colspan="4">
					<div class="row footer px-5 mt-3 pt-3">
						<p style="font-size:13px !important; padding-top:5px;" class="medium-text"><b>TERMS & CONDITIONS</b></p>
						<p style="font-size:12px !important"><label class="term"><?= @$billing_doc[0]->inv_terms; ?></label></p>
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="add">
						<div style="width:96%;">
							<p style="margin-bottom:0px !important;" class="small-text"><?php if($custom_inv[0]->company_address){?><label class="dwn_ftr"><?php echo wordwrap($billing_doc[0]->inv_address,50,"<br>\n"); ?><?php } ?>
							</p>
							<label style="font-size:12px !important;" class="small-text">TEL. NO.&nbsp&nbsp&nbsp: </label><label style="font-size:12px !important;" class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
							<label style="font-size:12px !important;" class="small-text">EMAIL ID&nbsp&nbsp: </label><label style="font-size:12px !important;" class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
						</div>
					</td>
					<td class="pan">
						<div style="width:99%; margin-top:0%;">
						<label style="font-size:12px !important;" class="term"><strong>FACEBOOK&nbsp:</strong></label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label></br>    
						<label style="font-size:12px !important;" class="term"><strong>WEBSITE&nbsp;&nbsp;&nbsp;&nbsp&nbsp;: </strong> </label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label></br>
						<label style="font-size:12px !important;" class="term"><strong>TWITTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> </label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label></br>
						<label style="font-size:12px !important;" class="term"><strong>LINKEDIN&nbsp;&nbsp;&nbsp;&nbsp;: </strong></label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
        </div>
    </div>
</body>
</html>