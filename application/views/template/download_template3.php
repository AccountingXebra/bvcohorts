<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
         padding-left: 0px !important;
         }
      </style>
      <STYLE TYPE="text/css" >
           
	  @media only screen and (min-width: 993px){
		.container {
			width: 95%;  
			margin: 10px 15px 10px 35px !important;
			opacity: 999999;
			z-index: 99999;
		}
     }
				#third-template p{
					font-size:12px !important;
				}
				.header1{
					height:100px;
				}
				.header2{
					height:160px;
					padding-top:15px;
				}
				.invoice{
					line-height: 1em;
					font-size: 12px;
				}
				.invoice-details{
					line-height: 1.4em;
					font-size: 12px;
				}
				.dp{
					color:#fa3b59;
					font-size:12px !important;
				}
				#table-first tr th{
					padding:15px 0 !important;
					/*border-bottom: 0.8px solid black;*/
					font-size: 12px;
					line-height: 1em;
					text-align:center;
				}
				#table-first{
					width: 100%;
					padding-bottom: 50px;
				}
				.header1, .header2{
					background-color: #262930;
				}
				
				#table-first tbody tr{
					background-color:#fff !important;
				}
				
				#table-first td{
					max-width: 450px;
					padding-top:20px;
					padding-bottom: 20px;
					padding-right: 8px;
				}
				.desc{
					font-size: 12px;
				}
				#payment{
					line-height: 0.5em;
					font-size: 12px;
				}
				#sign{
					border-bottom: 1px black solid;
					height: 36px;
					width:213px;
					
				}
				#gst{
					width:250px;
					border-bottom: 1px black solid;
					background-color: #fa3b59;
					padding:10px;
				}
				#gst p{
					margin-bottom: 0px !important;
					padding-bottom: 0px !important ;
					padding-right: 4px;
					
				}
				#total{
					width:250px;
					border-top:1px black solid;
					font-size: 16px;
				}
				.footer p{
					/*background-color: #ebf0ec;*/
					font-size: 12px;
					margin-bottom: 0px;
				}
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;
					line-height: 1em;
					font-size: 13px;
				}
				#social, #company{
					height:43px;
				}
				footer{
					/* background-color: #ebf0ec; */
				}
				
				h5, .m-1{
					color:#fff;
				}
				
				.m-1{
					font-size:12px !important;
				}
				
				.paybtn{
					color: #fff !important;
					padding: 2% 8%;
					background: #fa3b59;
					border-radius: 5px;
					border: 1px solid #fa3b59;
					margin: 10px -3px;
				 }
				 
				table.infotable {
					width: 100%;
					display: table;
					border-collapse: collapse;
				}

				body{
					font-family: 'Roboto', sans-serif !important;
				}
				
				p{
					font-size:12px !important;
					margin:10px 0;
				}
				
				.htht{
					background: #fa3b59 !important;
					height: 3px !important;
					margin: -1px -1px;
					border: 2px solid #fa3b59;
				}
			</style>
			<script type="text/javascript">
				$(document).ready(function() {
				//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
		   
				var USD='<?php print $currencycode[0]->currencycode; ?>';
			
				$('#tr_rate').html(USD);
				$('#tr_discount').html(USD);
				$('#tr_tax').html(USD);
				if(USD=="INR"){
					$('#to_words_currency').html("RUPEES ");
				}else{
					$('#to_words_currency').html(USD+".");
				}
				$('.par-spa rightss cemterfd').html(USD+" ");

				var final_grt=$('#tot_amount').text();
				var words=toWords(parseFloat(final_grt));
				if(words==''){
					$('#to_words_inv').text('ZERO');
				}else{
					$('#to_words_inv').text(words.toUpperCase()+' ONLY');
				}
				});
		 </script>
			</head>
		<body style="margin:-10px !important; padding:0 3px !important;">
		<div class="container">
		<?php
			$USD=$currencycode[0]->currencycode; 
		?>
				<table class="infotable">
				<tbody>
				<tr style="background-color:#000;">
					<td>
						<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                        if($custom_inv[0]->hdr_logo!=0){?>
							<img style="margin-top:-15px;" width="150" height="55" src="<?php //echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="company_logo"/>
						<?php } ?>
					</td>
					<td colspan="2">
						<h5 style="margin-bottom:-3px;" class="pl-2"><?php echo strtoupper($company[0]->bus_company_name); ?></h5>
						<label style="color:#fff; font-size:13px; padding-left:2px;"><?php echo wordwrap($billing_doc[0]->inv_address,40,"<br>\n"); ?></label></br>
						<label style="color:#fff; font-size:13px; padding-left:2px;">TEL. NO.&nbsp&nbsp&nbsp: </label><label style="color:#fff; font-size:13px;"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
						<label style="color:#fff; font-size:13px; padding-left:2px;">EMAIL ID&nbsp&nbsp: </label><label style="color:#fff; font-size:13px;"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
					</td>
					<td>
						<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
							<h3 class="pr-2" style="text-align:right; font-weight:800; color:#fa3b59">ESTIMATE</h3>
						<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
							<h3 class="pr-2" style="text-align:right; font-weight:800; color:#fa3b59">PROFORMA INVOICE</h3>
						<?php }else { $inv="Sales Invoice";?>
							<h3 class="pr-2" style="text-align:right; font-weight:800; color:#fa3b59">SALES INVOICE</h3>
						<?php } ?>
					</td>
				</tr>
				<tr><td colspan="4"><hr class="htht"></hr></td></tr>
				<tr style="background-color:#000; border:none; padding:0 5px !important;">
					<td colspan="2">
						<h6 style="color:#fff !important; margin-bottom:-20px !important;" class="big-inv"><b>E&OE </b></h6>
						<p class="dp" style="font-size:15px;">INVOICE TO:</p>
						<!--p class="font-weight-bold m-1"></b></p-->
						<!--p class="font-weight-bold m-1"><b>Accounts Dept.</b></p-->
						<p class="m-1"><b><?=  @ucwords($billing_doc[0]->cust_name); ?></b>,<br><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.','.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></p>
						<span class="m-1"><span class="dp">GSTIN: </span><?= @$gstno_cli[0]->gst_no; ?></span></br>
						<span class="m-1"><span class="dp">PAN: </span><?= @$cust_pan[0]->cust_pancard; ?></span>
					</td>
					<td colspan="2">
						<div style="text-align:right;">
						<p class="m-1"><span class="dp"><?=strtoupper($inv)?> NO: </span> <?=  @$billing_doc[0]->inv_invoice_no_view; ?></p>
						<p class="m-1"><span class="dp"><?=strtoupper($inv)?> DATE: </span> <?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></p>
						<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
						<p class="m-1"><span class="dp">ESTIMATE / P.O. NO:</span> <?=  @$billing_doc[0]->inv_po_no; ?></p>
						<?php } ?>
						<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
						<p class="m-1"><span class="dp">ESTIMATE / P.O. DATE:</span> <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
						<?php } ?>
						<p class="m-1"><span class="dp">PLACE OF SUPPLY: </span> <?=$place?></p>
						<p class="m-1"><span class="dp">TERMS OF PAYMENT: </span><?=@$cust_pan[0]->cust_credit_period;?></p>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
        <div class="row" style="margin-bottom:-35px !important;">
		
			<style>
					<?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                      
                      </style>
        <table id="table-first">
            <thead>
			<tr class="text-center" style="background-color: #ebf0ec; border-bottom:none;">
                <th class="text-left" style="width:45%;text-align:left !important;">ITEM DESCRIPTION</th>
                <th style="width:10%;">SAC NO.</th>
                <th style="width:10%;">QNTY</th>
                <th style="width:10%;">RATE</th>
                <th style="width:10%;">DISCOUNT</th>
                <th style="width:15%;">TOTAL</th>
            </tr>
			</thead>
			<tbody>
			<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
            <tr class="text-center">
                <td class="text-left"><span class="font-weight-bold d-block heading">
				<b><?php //$invkey+1; 
						$temp=$sub_tbl.'_service_type'; ?>
						<?php if($invlist->$temp==3){
                        echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
                        echo "Late Fee";
                        }else if($invlist->$temp==2){
                        echo "Expense Voucher";
                        }else{
                        foreach($my_services as $service){
                        if($service->service_id==$invlist->service_id){
                        echo $service->service_name;
                        }
                        }
                        }?></b>
				</span></br>
				<span class="desc"><?php  
                        $particulars=$sub_tbl.'_particulars'; 
                        $hsn=$sub_tbl.'_hsn_sac_no'; 
                        $quantity=$sub_tbl.'_quantity'; 
                        $rate=$sub_tbl.'_rate'; 
                        $discount=$sub_tbl.'_discount'; 
                        $taxable_amt=$sub_tbl.'_taxable_amt'; 
                        $igst=$sub_tbl.'_igst'; 
                        $igst_amt=$sub_tbl.'_igst_amt'; 
                        $cgst=$sub_tbl.'_cgst'; 
                        $cgst_amt=$sub_tbl.'_cgst_amt'; 
                        $sgst=$sub_tbl.'_sgst'; 
                        $sgst_amt=$sub_tbl.'_sgst_amt'; 
                        $cess=$sub_tbl.'_cess'; 
                        $cess_amt=$sub_tbl.'_cess_amt'; 

                        $other=$sub_tbl.'_other'; 
                        $other_amt=$sub_tbl.'_other_amt'; 
                        $amount=$sub_tbl.'_amount'; 
                        ?>
						<?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?></span>
				</td>
                <td><?=$invlist->$hsn;  ?></td>
                <td><?=$invlist->$quantity;  ?></td>
                <td><?=round($invlist->$rate);  ?></td>
                <td><?=round($invlist->$discount);  ?></td>
                <td><? echo ($invlist->$rate)-($invlist->$discount) ?></td>
            </tr>
			<?php } ?>
			</tbody>
        </table>
        </div>
        <div style="margin-top:-30px !important;">
			<table class="infotable">
				<tbody>
					<tr>
						<td colspan="4">
						<label style="color:#000; font-size:12px !important;"><b>CIN:</b></label><label style="color:#000;font-size:12px !important;"> <?= $company[0]->bus_cin_no; ?></label>&nbsp;
						<label style="color:#000; font-size:12px !important;"><b>PAN NO.:</b></label><label style="color:#000; font-size:12px !important;"> <?= $company[0]->bus_pancard; ?></label>&nbsp;
						<label style="color:#000; font-size:12px !important;"><b>GST:</b></label><label style="color:#000; font-size:12px !important;"> <?= $gstno[0]->gst_no; ?></label>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="payment">
				<button class="paybtn"><a class="btn btn-welcome mk_pay" href="<?=@$payment_url?>" style="text-decoration:none; color:#fff;">MAKE PAYMENT</a></button>
				<p style="margin-top:25px;"></p>
                <p id="ac-number"><b>A/C NUMBER:</b> <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
                <p id="bank-name"><b>BANK NAME:</b> <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
                <p id="branch"><b>BRANCH:</b> <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
                <?php if($USD=="INR"){?>
					<p id="ifsc"><b>IFSC CODE:</b> <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
				<?php } else { ?>
					<p id="ifsc"><b>SWIFT CODE:</b> <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
				<?php } ?>
				</div>	
						</td>
						<td colspan="2">
							<div style="text-align:right">
								<p class="font-weight-bold h4"><b>SUB TOTAL:</b><?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></p>
								<?php if($tax=="true") { ?>
								<p><b>+SGST(9%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
								<p><b>+CGST(9%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
								<?php } else { ?>
								<p><b>+IGST(18%):</b> <?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
								<?php } ?>
								<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
								<p><b>+CESS:</b> <?=$USD?> <?= round(@$billing_doc[0]->inv_cess_total); ?></p>
								<?php } ?>
								<?php if(round($billing_doc[0]->inv_other_total)>0){?>
								<p><b>+OTHER:</b> <?=$USD?> <?= round(@$billing_doc[0]->inv_other_total); ?></p>
								<?php } ?>
								<!--div style="text-align:right" class="font-weight-bold float-right text-left pl-3" id="total"-->
									<p style="font-size:18px !important;"><b>TOTAL: <?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></b></p>
								<!--/div-->
							</div>
						</td>
					</tr>
					<tr>
					<td colspan="4">
					<div class="" style="text-align:right; margin-right:5px;">
					<label class="up_lbl" style="color:#000 !important; font-size:14px !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
					<p class="txt_pnt" style="padding-top:10px;"><?//= @$billing_doc[0]->inv_signature; ?></p><br></br>
					<label class="txt_pnt" style="color:#000 !important; font-size:14px !important;"><?= $billing_doc[0]->inv_sign_name; ?></label> <label style="font-size:14px !important; color:#000 !important;" class="txt_pnt"><?//= $billing_doc[0]->inv_sign_designation; ?></label>
					</div>
					</td>
					</tr>
					<tr>
					<td colspan="3" class="add">
						<div style="width:94%;">
							<p style="font-size:12px !important;"><b>TERMS & CONDITIONS:</b></br> 
							<?= @$billing_doc[0]->inv_terms; ?>
							</p>
						</div>
					</td>
					<td class="pan">
						<?php if($billing_doc[0]->inv_terms==""){?>
							<div style="width:100%;">
						<?php } else{?>
							<div style="width:100%; margin-top:-15%;">
						<?php } ?>
						<label style="font-size:12px !important;" class="term"><strong>FACEBOOK&nbsp: </strong></label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label></br>    
						<label style="font-size:12px !important;" class="term"><strong>WEBSITE&nbsp;&nbsp;&nbsp;&nbsp&nbsp;: </strong> </label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label></br>
						<label style="font-size:12px !important;" class="term"><strong>TWITTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> </label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label></br>
						<label style="font-size:12px !important;" class="term"><strong>LINKEDIN&nbsp;&nbsp;&nbsp;&nbsp;: </strong></label><label style="font-size:12px !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
        <!--<footer class="container">
            
            <div class="row pb-5 pt-2">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>
        
        </footer>-->
        </div>
	</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>