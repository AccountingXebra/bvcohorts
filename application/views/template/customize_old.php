<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<style>
	#inv_customisemodal p{
		font-size:13px;
	}
	* {
		margin: 0;
		padding: 0;
		outline: none;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}

	.ps-container.ps-active-y > .ps-scrollbar-y-rail {
		display: none !important;
	}

	.tabs {
		margin: 0;
		padding: 0;
		list-style: none;
		overflow: hidden;
		border-bottom: 1px solid #999;
	}

	.tabs li {
		float: left;
		border: 1px solid #999;
		border-bottom: none;
		background: #e0e0e0;
	}

	.tabs li a {
		display: block;
		padding: 10px 20px;
		font-size: 16px;
		color: #000;
		text-decoration: none;
	}

	.tabs li a:hover {
		background: #ccc;
	}

	.tabs li.active,.tabs li.active a:hover {
		font-weight: bold;
		background: #fff;
		border:none;
	}

.tab_container {
  border: 1px #999;
  border-top: none;
  background: #fff;
  width:364px;
  margin-left:10px;
}

.tab_content {
  padding: 20px;
  font-size: 16px;
}

.tabs {
	 margin-left:10px;
	 border-bottom:none;
	 height:40px;
}

/*#paper_size, #orientation ,#hdr_logo{
  position: absolute;
  opacity:1;
  content:none;
  pointer-events: none;
}*/

/*[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after*/
#hdr_logo{
    width: 14px;
    height: 14px;
    margin: 0 0 0 0;
    border: 2px solid #B0B7CA;
}

.caret{
	border-right:0px;
	border-left:0px;
}

#inv_tbl, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

#tab-table_1 th{
	background-color:#7864e9 !important;
}

p{
	font-size:13px;
}

#inv_tbl #total_amt{
	background-color:#ffffe6;
}

	#b_name, #br_name, #ifsc, #ac_no{
		height:1rem;
		/*margin-top:15px;*/
	}
	
#inv_tbl .head_details{
	text-align:center;
}

[type="checkbox"] + label {
    padding-left: 26px;
    height: 25px;
    line-height: 17px;
}


/*.ps-container > .ps-scrollbar-x-rail > .ps-scrollbar-x {

	display:none;
}*/

.modal input.select-dropdown {
    margin: -10px 0 0 -21px !important;
}

.up_lbl1, .pan-no, .gst-no, .cin-no, .txt_pnt{
	color:#000000;
}

.advance-pay1 {
    width: calc(100% - 0px) !important;
}

	#tab-table .amttable{
		text-align:center;
	}
	
	th.dis1 p {
		padding: 0 0px 0 15px;
	}
	
	p{
		font-size:11px;
	}
	
	.zebra-logo{
		margin-top:20px;
	}
	
	.tem-name{
		text-align:center;
		font-size:13px;
	}
	
	.template_div{
		margin-bottom:4%;
	}
	
	.template_div.active{
		/*border:1px solid #000;*/
	}

	.zoom:hover {
		transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
		text-align:center;
	}
	
	.al-right{
			text-align:right;
		}
		
		.big-inv{
			color:#000;
			margin-top:0px !important;
		}
		
		.inv-to{
			font-size:15px;
			color:#000;
		}
		
		.mod-pay{
			color:#000;
			font-size:14px;
			line-height:15px;
		}
		
		.foot{
			background-color:#7864e9;
			margin:-4% 0 0 0;
			padding:5px 0;
		}
		
		.foot_lbl{
			font-size:13px;
			color:#fff;
		}
		
		.xebra{
			color:#fff;
			margin:3% -2%;
		}
		
		.add{
			margin-top:10px;
			float:left;
		}
		
		.add label{
			font-size:13px;
			color:#fff;
		}
		
		.pan{
			margin-top:-10%;
			float:right !important;
		}
		
		.term{
			color:#fff;
			font-size:13px;
		}
		
		.up_lbl, .txt_pnt{
			color:#000;
			font-size:13px;
		}
		
		.paybtn{
		 	color: #fff;
		    padding: 5px 8%;
		    background: #0059b3;
		    border-radius: 5px;
		    border: 1px solid #0059b3;
		    margin: 6px -3px;
		 }
		 
		 hr.bottom-hr {
		  border-color: grey;
		  background: none;
		  padding: 4px 0 0 39px !important;
		  border-top: none;
		  border-left: none;
		  margin: 0 0 10px 3px;
		  border-right: none;
		  float: initial;
		  width: 95%;
		}
		
		.txt_ftr{
			font-size:13px;
			color:#000;
		}
		
		#layout_showbtn{
			height: 38px !important;
			width: 100% !important;
			line-height: 20px !important;
			margin: 1px 0 12px 0 !important;
		}
		
		.note-cust{
			padding-bottom:10px;
		}
		
		.note-cust p{
			font-size:13px !important;
			color:#595959 !important;
		}
		
		.empty-r{
			height:25px;
		}
		
		.company-name{
			font-size:15px;
			color:#595959;
			margin:0 0 -10px 10px;
		}
		
		.term-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#fff !important;
		}
		
		.term1-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#595959 !important;
		}
		
		
</style>
</head>  

	<div class="row">
		<div class="col s12 m12 l4">
			<div class="masonry" style="margin-left:-18px;">
				<ul class="tabs">
					<li><a id="cust_all" href="#tab1">Template</a></li>
					<li><a id="cust_tab" href="#tab2">Content</a></li>
					<li><a id="foot_tab" href="#tab3">Footer</a></li>
				</ul>
				<div class="tab_container">
					<div id="tab1" class="tab_content"><!-- TAB 1 (Template) -->
						<div class="row">
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
									<button class="btn-flat theme-primary-btn theme-btn theme-btn-large" id="layout_showbtn">SELECT LAYOUT</button>
								</div>
								<div class="col s12 m12 l12">
								<input type="checkbox" id="set_invoice_no" name="set_invoice_no" value="1">
								<label for="set_invoice_no" style="font-size:13px; margin-bottom:12px;">Set Invoice prefix</label>
								</div>
								<div class="col s12 m12 l4 inv_inp">
									<input style="border:1px solid #c2d6d6; border-radius:6px; padding-left:10px;" id="inv_no_pre" type="text" maxlength="6" size="6" class="form-control" name="invoice" placeholder="">
								</div>
								<div class="col s12 m12 l8 inv_inp" style="padding-top:15px !important;">
									<p style="font-size:13px !important; color:#000000 !important;">
										<label style="color:#000000 !important;">For Example:</label> <label style=" padding:4px !important; border:0.5px solid black !important; border-style:dotted !important; color:#000000 !important;">INV</label><label style="color:#000000 !important;">/0001-2019-20</label>	
									</p>
									<!--<input style="border:1px solid #c2d6d6; border-radius:6px; padding-left:10px;" id="inv_no_2" type="text" class="form-control" name="invoice" placeholder="">-->
								</div>
								
							</div>
							<div class="row border-bottom" style="margin-left:-5px;">									
								<div class="form-check col s12 m12 l4">
									<input type="radio" class="form-check-input" id="paper_size" name="paper" value="A4" checked="checked">
									<label class="form-check-label" for="paper_size">A4</label>
								</div>

								<div class="form-check col s12 m12 l4" style="margin-bottom:20px;">
									<input type="radio" class="form-check-input" id="letter" name="paper" value="letter">
									<label class="form-check-label" for="letter">Letter</label>
								</div>
							</div>
							<!--<div class="row">  
								<div class=" col s12 m12 l12 logo" style="margin-bottom:20px;">
									<label for="orientation" style="font-size:13px;">Orientation</label><br>
									<div class="col s12 m12 l4" style="margin-top:5px;">
										<input type="radio" id="orientation" name="orientation" value="portrait"><span style="font-size:14px;">Portrait</span></input>
									</div>
									<div class="col s12 m12 l4" style="margin-top:5px;">
										<input type="radio" id="orientation" name="orientation" value="landscape"><span style="font-size:14px;">Landscape</span></input>
									</div>
								</div>
							</div>-->
							<!--div class="row border-bottom">
								<div class="col s12 m12 l12">
									<label for="margins" style="font-size:13px;">Margins (in inches)</label><br>
									<input type="text" id="margins" name="margins" value="" style="width:20%; height:20px; border:1px solid #ccc; !important;">
									<label style="font-size:13px;">Left/right/bottom<label></input>
								</div>
							</div-->
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
								<label for="tmp_back_color" style="font-size:11px;">BACKGROUND COLOR</label><br>		
								<div class="col s12 m12 l3" style="margin-top:0px; height:32px; border:1px solid #c2d6d6;">
									<input style="margin-left:-12px; width:74px; height:30px;" id="tmp_hex_color" type="color" name="tmp_hex_color" value="#ffffff" onchange="changeColortmp();"/>
								</div>	
								<div class="col s12 m12 l5" style="margin-top:0px; margin-bottom:10px !important;">	<!-- class="full-bg" -->
									<input style="width:69px; margin:0px 0px 0px -12px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="tmp_back_color"  name="tmp_back_color" value="#ffffff" type="text"></input>
								</div>
								</div>
							</div>		
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
									<input type="checkbox" id="set_for_all_tmp" name="set_for_all_tmp" value="1">
									<label for="set_for_all_tmp" style="font-size:13px; margin-bottom:20px;">Set For All Invoices</label>
								</div>
							</div>		
							<div class="row border-bottom note-cust">	
								<p style="margin-top:-10px;">These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
							</div>
							<div class="row">
								   <button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="layout_btn">Save</button>
								   <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button" onclick="">CANCEL</button>
							</div>
						</div>
					</div><!-- TAB 1 CLOSE -->
					
					<div class="row">
							<div id="tab2" class="tab_content">	
								<div class="row">
									<label class="pull-left" style="font-size:14px;"><b>Header Logo</b></label>
								</div>
								<div class="row border-bottom">
									<input type="checkbox" id="hdr_logo" name="hdr_logo" value="1"></input>
									<label for="hdr_logo" style="font-size:13px; margin-bottom:10px;">Logo</label>
									<input type="hidden" id="template_val" name="template_val" value=""></input>
								</div>
								<!--<div class="row">
									<div class="col s12 m12 l12" style="margin-bottom:20px;">
										<div class="col s12 m12 l4">
											<label style="color: #000;">LegalContract.jpg</label><br>
											<span style="font-size:13px;">Image size 20px 20px</span>
										</div>	  
										<div class="col s12 m12 l4">
											<a href="#"><img src="<?php echo base_url(); ?>asset/css/img/icons/delete.png"></a>
										</div>
									<!--</div>
								</div>-->
								<div class="row">
									<label class="" style="font-size:14px;"><b>Document Info</b></p>
								</div>
								<div class="row border-bottom">
									<div class="col s12 m12 l6" style="margin-left:-10px !important;">
										<label for="font_color" style="font-size:13px;">FONT COLOR</label>
										<div class="col s12 m12 l6" style="margin-bottom:10px;"><input type="text" id="font_color" value="#000000" type="text" style="width:69px; margin:0px 0px 0px -12px !important; height:30px !important; border:1px solid #c2d6d6;"></input></div>
										<div class="col s12 m12 l6" style="margin-top:0px; height:32px; border:1px solid #c2d6d6;"><input id="hex_color" style="margin-left:-12px; width:68px; height:30px;" type="color" name="favcolor" value="#000000" onchange="changeColor()"></div>
									</div>
									<div class="col s12 m12 l6" style="margin-bottom:20px;">
										<label for="font_size" style="font-size:13px;">FONT SIZE</label><br>
										<div class="col s12 m12 l12" style="border:1px solid #c2d6d6 !important; padding-left:18px !important; height:33px !important;">
											<select id="font_size" name="font_size">
												<option value="8px">8px</option>
												<option value="9px">9px</option>
												<option value="10px">10px</option>
												<option value="11px">11px</option>
												<option value="12px">12px</option>
												<option value="13px" selected>13px</option>
												<option value="14px">14px</option>
												<option value="15px">15px</option>
												<option value="16px">16px</option>
												<option value="17px">17px</option>
												<option value="18px">18px</option>
												<option value="19px">19px</option>
												<option value="20px">20px</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row border-bottom" style="margin-left:-12px !important;">
										<!--<div class="col s12 m12 l6">
											<input type="checkbox" id="shipping_address" name="shipping_address" value="1">
											<label for="shipping_address" style="font-size:9px;"><b>SHIPPING ADDRESS</b></label>
										</div>
										<div class="col s12 m12 l6">
											<input type="checkbox" id="late_fees" name="late_fees" value="1">
											<label for="late_fees" style="font-size:11px;">LATE FEES</label>
										</div>-->
									<div class="col s12 m12 l6">
										<input type="checkbox" id="bank_details" name="bank_details" value="1">
										<label for="bank_details" style="font-size:11px;">BANK DETAILS</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="pan_no" name="pan_no" value="1">
										<label for="pan_no" style="font-size:11px;">PAN NO</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="gst" name="gst" value="1">
										<label for="gst" style="font-size:11px;">GST</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="payment_btn" name="payment_btn" value="1">
										<label for="payment_btn" style="font-size:11px;">PAYMENT BUTTON</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="cin_no" name="cin_no" value="1">
										<label for="cin_no" style="font-size:11px;">CIN NO</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="purchase_order_date" name="purchase_order_date" value="1">
										<label for="purchase_order_date" style="font-size:11px;">PURCHASE ORDER DATE OF ISSUE</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="purchase_order" name="purchase_order" value="1">
										<label for="purchase_order" style="font-size:11px;">PURCHASE ORDER</label>
									</div>
									<div class="col s12 m12 l6">	
									<input type="checkbox" id="terms_condition" name="terms_condition" value="1">
									<label for="terms_condition" style="font-size:11px;">TERMS & CONDITIONS</label>	 
									</div>
								</div>
								<div class="row" style="margin-bottom:5px !important;">
									<label style="font-size:14px;"><b>Other info</b></label>
									<div class="logo" style="margin-top:5px;">
										<input type="checkbox" id="signature" name="signature" value="1">
										<label for="signature" style="font-size:13px;">Show Signature Box<a style="margin-top:-7px;" class="note tooltipped info-tooltipped" data-position="top" data-delay="50" data-html="true" data-tooltip="Upload signature in personal profile module"></a></label>
									</div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:-18px !important;">
										<img width="180" height="40" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php }else{ ?>
									<div class="col s12 m12 l12" style="margin-left:-18px !important;">
										<img width="180" height="40" src="<?php echo base_url(); ?>asset/images/signature.png" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
									</div>
									<div class="row" style="margin-bottom:5px !important;"><!-- class="full-bg" -->
									<div class="col s12 m12 l6" style="margin-left:-15px !important;">
									   <input style="border:1px solid #c2d6d6; border-radius:4px; padding-left:10px;" type="text" name="client_name" id="client_name" value="<?php echo $reg_data[0]->reg_username; ?>" type="text" placeholder="CLIENT NAME" disabled> </input>
									</div>
									<div class="col s12 m12 l6">
									   <input style="border:1px solid #c2d6d6; border-radius:4px; padding-left:10px;" type="text" name="client_designation"  id="client_designation" value="<?php echo $reg_data[0]->reg_designation; ?>" type="text" placeholder="DESIGNATION" disabled> </input>
									</div>
									</div>
								<div class="row border-bottom">
									<div class="logo">
										<input type="checkbox" id="set_for_all_cont" name="set_for_all_cont" value="1">
										<label for="set_for_all_cont" style="font-size:13px; margin-bottom:20px;">Set For All Invoices</label>
									</div>
								</div>
								<div class="row border-bottom note-cust">	
									<p style="margin-top:-10px;">These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
								</div>
								<div class="row">
									<div class="col s12 m12 l12">
										<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="content_btn">Save</button>
										<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button"  onclick="#">CANCEL</button>
									</div>
								</div>
							</div>
						</div>
					<!-- TAB 3 -->
					<div id="tab3" class="tab_content">  
						<div class="row">
									<div class="row">
										<label class="pull-left" style="font-size:14px;">Document Info</label><br>
										<div class="logo" style="margin-top:5px;">
											<input type="checkbox" id="eoe" name="eoe" value="1">
											<label for="eoe" style="font-size:13px;">E&OE - Errors and Omission Expected</label>
										</div>
									</div>
									<div class="row"> <!-- class="full-bg" -->
										<div class="col s12 m12 l6">
											<label for="ftr_font_color" style="font-size:11px; margin-left:-12px !important;">FONT COLOR</label><br>
											<div class="col s12 m12 l6" style="margin-left:-12px; height:32px; border:1px solid #c2d6d6;"><input style="margin-left:-12px; width:68px; height:30px;" id="ftr_hex_color" type="color" name="ftr_hex_color" value="#000000" onchange="changeColorftr()"></div>
											<div class="col s12 m12 l6" style="margin-left:-12px !important;"><input style="width:69px; margin:0px 0px 0px 0px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="ftr_font_color" value="#000000" type="text"></input></div>
										</div>
										<div class="col s12 m12 l6">
											<label for="ftr_font_size" style="font-size:11px;">FONT SIZE</label>
											<div class="col s12 m12 l12" style="border:1px solid #c2d6d6 !important; padding-left:18px !important; height:33px !important;">
												<select id="ftr_font_size" name="ftr_font_size" style="margin-left:5px !important;">
													<option value="8px">8px</option>
													<option value="9px">9px</option>
													<option value="10px">10px</option>
													<option value="11px">11px</option>
													<option value="12px" selected>12px</option>
													<option value="13px">13px</option>
													<option value="14px">14px</option>
													<option value="15px">15px</option>
													<option value="16px">16px</option>
													<option value="17px">17px</option>
													<option value="18px">18px</option>
													<option value="19px">19px</option>
													<option value="20px">20px</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row"><!-- class="full-bg" -->
											<label for="ftr_back_color" style="font-size:11px;">BACKGROUND COLOR</label><br>
											<div class="col s12 m12 l6" style=" height:32px; border:1px solid #c2d6d6; width:70px !important;"><input style="margin-left:-12px; width:68px; height:30px;" id="back_hex_color" type="color" name="back_hex_color" value="#000000" onchange="changeColorback()"></div>
											<div class="col s12 m12 l3" style="margin-left:-16px;"><input style="width:69px; margin:0px 0px 0px 0px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="ftr_back_color" value="#000000" type="text"> </input></div>
									</div>
									<div class="row" style="margin-left:-10px !important;">
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_address" name="company_address" value="1">
												<label for="company_address" style="font-size:11px;">SHOW COMPANY ADDRESS</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_email" name="company_email" value="1">
												<label for="company_email" style="font-size:11px;">SHOW COMPANY EMAIL ID</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_contact" name="company_contact" value="1">
												<label for="company_contact" style="font-size:11px;">SHOW COMPANY CONTACT NO</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_website" name="company_website" value="1">
												<label for="company_website" style="font-size:11px;">SHOW COMPANY WEBSITE</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="fb_link" name="fb_link" value="1">
												<label for="fb_link" style="font-size:11px;">FACEBOOK LINK</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="twitter_link" name="twitter_link" value="1">
												<label for="twitter_link" style="font-size:11px;">TWITTER LINK</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="linked_link" name="linked_link" value="1">
												<label for="linked_link" style="font-size:11px;">LINKEDIN LINK</label>	
											</div>
									</div>
									<div class="logo border" style="border-top:1px solid #eef2fe; padding:15px 0;">
										<input type="checkbox" id="set_for_all_ftr" name="set_for_all_ftr" value="1">
										<label for="set_for_all_ftr" style="font-size:13px;">Set For All Invoices</label>
									</div>
									<div class="row border-bottom note-cust" style="border-top:1px solid #eef2fe; padding-top:10px;">	
										<p>These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
									</div>
									<div class="row">
										<div class="col s12 m12 l12">
											<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="footer_btn">Save</button>
											<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button"  onclick="#">CANCEL</button>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- col 4 close -->
			<a style="padding-left:25px; margin-top:15px;" class="modal-close close-pop" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<div id="template-gallary" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;">
			
				<!--div class="row border-bottom zebra-logo">
					<div class="col s12 m12 l12" style="text-align:right"><img src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo"></div>
				</div-->
				<div class="row zebra-logo">
					<div class="col s12 m12 l6 zoom template_div active default" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/default-template.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Default Template</span></div>
					</div>
					<div class="col s12 m12 l6 zoom template_div first_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/Template-4.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 1</span></div>
					</div>
					<!--div class="col s12 m12 l4 zoom template_div second_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-1.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 2</span></div>
					</div-->
					<div class="col s12 m12 l6 zoom template_div third_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/Template-6.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 2</span></div>
					</div>
					<!--div class="col s12 m12 l4 zoom template_div fourth_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-3.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 4</span></div>
					</div>
					<div class="col s12 m12 l4 zoom template_div fifth_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-5.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 5</span></div>
					</div-->
					<div class="col s12 m12 l6 zoom template_div sixth_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/Template-2.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 3</span></div>
					</div>
				</div>
			</div>
			
			<!-- Default Template Start -->
			<div id="default-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<div id="inv_tem" class="box-wrapper bg-white shadow border-radius-6" style="padding:0 25px;">
				<div class="row border-bottom" style="margin-top:10px;">
					<div class="col s12 m12 l6 temp_text" style="margin:10px 0px 10px 0px; font-size:20px; color:#0059b3;"><b><span class="temp_text">SALES INVOICE</b></span></div>
					<div class="col s12 m12 l5" style="text-align:right; margin:0 0 0 35px;">
						<img id="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="50" width="200" alt="eazyinvoices-logo">
					</div>
					<div class="col s1 m1 l1" style="text-align:right; margin:0 0 0 -35px;">
						<a position="top" style="padding-left:5px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
					</div>
				</div>
				<div class="row">
				<div class="col s6 m6 l6">
					<label id="eoe_div" class="big-inv up_lbl temp_text"><b> E&OE </b></label></br>
					<label class="up_lbl temp_text"><b>SALES INVOICE NO: </b></label><label class="txt_pnt temp_text">INV1/2019-20</label></br>
					<label class="up_lbl temp_text"><b>SALES INVOICE DATE: </b></label><label class="txt_pnt temp_text">15-08-2019</label></br>
					<label class="pu_no up_lbl temp_text"><b>ESTIMATE / P.O. NO: </b></label><label class="pu_no txt_pnt temp_text">10/2019-20</label></br>
					<label class="pu_o_date up_lbl temp_text"><b>ESTIMATE / P.O. DATE: </b></label><label class="pu_o_date txt_pnt temp_text">10-05-2019</label></br>
					<label class="up_lbl temp_text"><b>TERMS OF PAYMENT: </b></label><label class="txt_pnt temp_text">15</label>
				</div>
				<div class="col s6 m6 l6 al-right">
					<label class="inv-to temp_text"><b>INVOICE TO:</b></label><br>
					<label class="txt_pnt temp_text">NEW COMPANY LLP,</label><br>
					<label class="txt_pnt temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD,</label></br>
					<label class="txt_pnt temp_text">MALAD-WEST, MUMBAI, MAHARASHTRA</label><br>
					<label class="txt_pnt temp_text">INDIA-400061</label>
				</div>
				<div class="row"></div>
				<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 10px 0px 10px !important;">
                        <table id="tab-table_1" class="tabs-class">
                           <tbody>
                              <tr class="bg-row tabss" id="over-boder">
                                 <th class="tab1 amttable" style="width:150px !important; text-align:center;">
                                     <p class="temp_text">ITEMS</p>
                                 </th>
                                 <th class="tab1 amttable" style="width:180px !important; text-align:center;">
                                    <p class="temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="tab2 amttable" style="width:50px !important; text-align:center;">
                                    <p class="temp_text">SAC</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="temp_text">QNTY</p>
                                 </th>
                                 <th class="rate-th amttable">
                                    <p class="temp_text" style="padding:0px 0px 0px 5px !important;">RATE </br> (&#x20b9;)</p>
                                 </th>
                                 <!--<th class="dis amttable zerodiscount">
                                    <p style="margin:0px !important;">Discount</p>
                                 </th>-->
                                  <th class="dis1 amttable">
                                    <p class="temp_text" style="margin:0px !important; padding-left:0px !important; text-align:center;">TAXABLE </br>AMT(&#x20b9;) </p> 
                                 </th>
                                 <th class="amttable zerocgst">
                                    <p class="temp_text" style="padding:0px 0px 0px 7px !important;">CGST</br>AMT(&#x20b9;)</p>
                                 </th>
                                 <th class="sgstn amttable zerosgst">
                                    <p class="temp_text" style="padding:0px 0px 0px 7px !important; width:55px !important;">SGST</br>AMT (&#x20b9;)</p>
                                 </th>
                                 <!--<th class="sgstn amttable zeroigst">
                                    <p>IGST Amt (%)</p>
                                    <!--<span class="par-spa rightss">(INR)</span>-->
                                 <!--</th>-->
                                 <th class="cescc amttable zerocess" style="width:40px !important;">
                                    <p class="temp_text" style="padding:0px 0px 0px 7px !important; width:40px !important;">CESS</br>AMT(&#x20b9;)</p>
                                 </th>
                                 <th class="othertx amttable zeroother" style="width:40px !important;">
                                    <p class="temp_text" style="padding:0px 0px 0px 7px !important;">OTHER</br>AMT(&#x20b9;)</p>
                                 </th>
                                 <th class="amt-th amttable" style="padding:0px 0px -1px 10px !important; width:100px !important;">
                                    <p class="temp_text">AMOUNT</p>
                                 </th>
                              </tr>
                              <tr class="boder-tr empty-r">
                                 <td><p class="txt_pnt temp_text" style="text-align:center;"></p></td>
                                 <td><p class="txt_pnt temp_text" style="text-align:center;"></p></td>
                                 <td class="txt_pnt temp_text"></td>
                                 <td class="td-class txt_pnt temp_text"><p></p></td>
                                 <td class="three-eaigh txt_pnt temp_text"><p style="padding-left:0px !important;"></p></td>
                                 <!--<td class="zero txt_pnt zerodiscount"><p>0</p></td >-->
                                 <td class="zero txt_pnt temp_text"><p style="text-align:center; padding-left:0px !important;"></p></td>
                                 <td class="one-eaigh txt_pnt zerocgst temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt zerosgst temp_text"><p></p></td>
                                 <!--<td class="one-eaigh txt_pnt zeroigst"><p>20</p><p>5%</p></td>-->
                                 <td class="one-eaigh txt_pnt zerocess temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt zeroother temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt temp_text" style="text-align:center; padding-right:20px !important;"><p></p></td>
                              </tr>
							  
							  <tr class="boder-tr empty-r">
                                 <td><p class="txt_pnt temp_text" style="text-align:center;"></p></td>
                                 <td><p class="txt_pnt temp_text" style="text-align:center;"></p></td>
                                 <td class="txt_pnt temp_text"></td>
                                 <td class="td-class txt_pnt temp_text"><p></p></td>
                                 <td class="three-eaigh txt_pnt temp_text"><p style="padding-left:0px !important;"></p></td>
                                 <!--<td class="zero txt_pnt zerodiscount"><p>0</p></td >-->
                                 <td class="zero txt_pnt temp_text"><p style="text-align:center; padding-left:0px !important;"></p></td>
                                 <td class="one-eaigh txt_pnt zerocgst temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt zerosgst temp_text"><p></p></td>
                                 <!--<td class="one-eaigh txt_pnt zeroigst"><p>20</p><p>5%</p></td>-->
                                 <td class="one-eaigh txt_pnt zerocess temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt zeroother temp_text"><p></p></td>
                                 <td class="one-eaigh txt_pnt temp_text" style="text-align:center; padding-right:20px !important;"><p></p></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
				
					<div class="row" style="margin-top:6%;">
					<div class="col s6 m6 l7">
						<label id="pan" class="up_lbl pan-1 temp_text"><b>PAN NO: </b></label><label class="txt_pnt pan-1 temp_text">AZXSDXXXR</label>
						<p style="margin:-5px 0;">&nbsp;</p>
						<label id="gst_no" class="up_lbl gst_1 temp_text"><b>GSTIN: </b></label><label class="txt_pnt gst_1 temp_text">27AZXSDXXXRXX1</label>
						<p style="margin:-5px 0;">&nbsp;</p>
						<label class="up_lbl temp_text"><b>PLACE OF SUPPLY: </b></label><label class="txt_pnt temp_text">MAHARASHTRA</label>
						<p style="margin:-5px 0;">&nbsp;</p>
						<button id="make-payment" class="paybtn temp_text">MAKE PAYMENT</button>
					</div>
					<div class="col s6 m6 l5">
						<div style="background-color:#7864e9 !important; height:40px; width:100%; padding:5px 0 0 0px; text-align:center;">
							<label class="temp_text" style="font-size:20px !important; color:#fff !important;"><b>TOTAL: </b></label><label  class="temp_text" style="font-size:20px !important; padding-left:10px; color:#fff !important; font-weight:500;"><b>&#x20b9;  </b></label>
						</div>
						<label style="font-size:14px !important; color:#7864e9 !important;" class="temp_text"><b>IN WORDS: </b></label><span class="txt_pnt" id="to_words_currency"><label class="txt_pnt temp_text" id="to_words_inv" style="font-size:14px !important; color:#7864e9 !important; font-weight:500;"><b>RUPEES ONLY</b></label>
					</div>
					</div>
					<div class="col s7 m7 l7"></div>
					<div class="col s5 m5 l5" style="text-align:right;" id="sign_box">
						<div class="col s12 m12 l12" style="text-align:center;">
							<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
						</div>			
						<div class="col s12 m12 l12" style="margin-right:-10px !important;">
							<div class="col s2 m2 l1" style="margin-left:14px;"></div>
							<?php if($reg_data[0]->reg_degital_signature!=""){?>
							<div class="col s12 m12 l12" style="margin-left:-30px !important;">
								<img width="180" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
							</div>
							<?php } ?>
						</div>
						<div class="col s12 m12 l12" style="text-align:center; padding-left:50px;">
							<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
						</div>
					</div>
			<div class="row" style="margin-top:3%;"><div class="col s12 m12 l12"></div></div>
			<hr class="bottom-hr sec-hr head-hr">
			<div class="row" style="margin: 0 -10px 20px -10px;">
				<div class="col s12 m12 l12">
					<p class="company-name temp_text"><b>COMPANY NAME</b></p></br>
				</div>
				
				<div class="col s12 m12 l12 company-details">
					<div class="col s4 m4 l4">
						<label class="addr txt_ftr temp_text"> 45, 4TH FLOOR, ABC BUILDING,</label></br>	
						<label class="addr txt_ftr temp_text"> BK MARG, BANDRA-EAST</label></br>	
						<label class="addr txt_ftr temp_text"> MUMBAI-400051, MAHARASHTRA, INDIA</label></br>	
						<label class="telno up_lbl temp_text">TEL. NO.&nbsp&nbsp&nbsp: </label><label class="telno txt_ftr temp_text">022-2XXX8 878</label></br>
						<label class="email_id up_lbl temp_text">EMAIL ID&nbsp&nbsp: </label><label class="email_id txt_ftr temp_text">emailid@email.com</label>
					</div>
					<div class="col s4 m4 l4 bank_info">
						<label class="up_lbl temp_text">BANK NAME<span style="padding-left:49px !important;"></span>: </label><label class="txt_pnt temp_text">MBSP BANK</label></br>
						<label class="up_lbl temp_text">ACCOUNT NUMBER<span style="padding-left:7px !important;"></span>: </label><label class="txt_pnt temp_text">1000XXX5253</label></br>
						<label class="up_lbl temp_text">BRANCH NAME<span style="padding-left:31px !important;"></span>: </label><label class="txt_pnt temp_text">Mumbai Main Road</label></br>
						<label class="up_lbl temp_text">IFSC CODE<span style="padding-left:60px !important;"></span>: </label><label class="temp_text txt_pnt">MBSP000111</label></br>
						<!--label class="up_lbl temp_text">SWIFT CODE<span style="padding-left:49px !important;"></span>:</label><label class="temp_text txt_pnt"> MBSPNBBOBU</label-->
					</div>
					<div class="col s4 m4 l4">
						<label class="cinno up_lbl temp_text">CIN<span style="padding-left:30px !important;"></span>: </label><label class="temp_text cinno txt_pnt">U64200MH2008XXXI23456</label></br>
						<label class="up_lbl temp_text">PAN NO<span style="padding-left:5px !important;"></span>: </label><label class="temp_text txt_pnt">AAAXXX131F</label></br>
						<label class="temp_text up_lbl">GSTIN<span style="padding-left:14px !important;"></span>: </label><label class="temp_text txt_pnt">27AAAXX131F1ZH</label>
					</div>
				</div>
				</div>
				<footer style="margin: 45px -10px 20px -10px; padding-left:0px !important;">
				  <div class='row foot footer_box'>
				  <div class="col s7 m7 17 add" id="cond">
					<div class="col s12 m12 l12">
						<label class="term footer_p"><b>TERMS &amp; CONDITIONS</b></label><br>
						<ul class="term-list" style="margin-left:18px;">
							<li><p style="color:#fff;" class="footer_p">Payments matters are subject to Mumbai jurisdiction only</p></li>
							<li><p style="color:#fff;" class="footer_p">any discrepancy regarding this bill must be notified within 5 days from the date of submission of this bill</p></li>
							<li><p style="color:#fff;" class="footer_p">24% P.A. interest will be charged on Overdue Unpaid Amount</p></li>
						</ul>
					</div> 
				  </div>
				  </div>
				  <div class="col s5 m5 15 pan" style="margin-top:-13%;">
					<label class="webst term footer_p"><b>WEBSITE<span style="padding-left:16px !important;"></span>: </b></label><label class="webst term footer_p">www.website.com</label></br>
					<label class="facebk term footer_p"><b>FACEBOOK<span style="padding-left:5px !important;"></span>: </b></label><label class="facebk term footer_p">www.facebook.com/abc</label></br>    
					<label class="twitter term footer_p"><b>TWITTER<span style="padding-left:15px !important;"></span>: </b></label><label class="twitter term footer_p">www.twitter.com/abc</label></br>
					<label class="term link_id footer_p"><b>LINKEDIN<span style="padding-left:12px !important;"></span>: </b></label><label class="term link_id footer_p">www.linkedin.com/abc</label>
				  </div>
				  </div>
			   </footer>
					
					<!--div class="row" style="margin-bottom:-10px !important;">
					<div class="col s12 m12 l12" style="margin-bottom:-10px !important;">
						<div class='row' id="bank_de" style="border:1px solid black;">
						<div class="col s4 m4 16">
							<label class="up_lbl1"><b>BANK NAME</b><span style="padding-left:21.5px !important;"></span>: </label><label class="txt_pnt">ICICI BANK</label>
							<br>
							<label class="up_lbl1"><b>BRANCH NAME</b>&nbsp: </label><label class="txt_pnt">Mumbai Main Road</label>
						</div>
						<div class="col s4 m4 15">					
							<label class="up_lbl1"><b>IFSC CODE</b><span style="padding-left:60px !important;"></span>: </label><label class="txt_pnt">ICIC000111</label>
							<br>
							<label class="up_lbl1"><b>ACCOUNT NUMBER</b>&nbsp&nbsp: </label><label class="txt_pnt">10001115253</label>
						</div>
						<div class="col s4 m4 11" hidden>
							<label class="up_lbl1"><b>SWIFT CODE</b>&nbsp: </label><label class="txt_pnt">ICICINBBOBU</label>
							<br>
						</div>
						</div>
					</div>
					</div>
					<div class="row" style="margin-top:-20px !important; padding-right:10px !important;">
					<div class="col s7 m7 l7">
						<div class="col s4 m4 l4" style="text-align:left;" id="make-payment">
							<button style="height: 41px !important; margin-left:0px !important;" class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5" id="make_payment_btn">MAKE PAYMENT</button>
						</div>
					</div>
					<div class="col s5 m5 l5" style="text-align:right;" id="sign_box">
						<div class="col s12 m12 l12" style="text-align:center;">
							<p style="font-size:11px; padding-bottom:5px !important; padding-left:40px !important;">FOR <?//= strtoupper($company[0]->bus_company_name) ?></p>
						</div>			
						<div class="col s12 m12 l12" style="margin-right:-10px !important;">
							<div class="col s2 m2 l1" style="margin-left:14px;"></div>
							<?php //if($reg_data[0]->reg_degital_signature!=""){?>
							<div class="col s12 m12 l12" style="margin-left:-30px !important;">
								<img width="180" height="40" src="<?php //echo base_url(); ?>public/upload/degital_signature/<?php //echo $reg_data[0]->reg_id; ?>/<?php //echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
							</div>
							<!--<div class="col s7 m7 l7" style="text-align:left;">
								<p style="color: #000;"><?php //echo $reg_data[0]->reg_degital_signature; ?></p>
								<p style="color: #000; font-size:11px !important;">Image size 40px X 40px</p>
							</div>-->
							<?php //}else{ ?>
							<!--div class="col s12 m12 l12" style="margin-left:-30px !important;">
								<img width="180" height="40" src="<?php //echo base_url(); ?>asset/images/signature.png" style="margin:0px;" class="signature_img" alt="signature">
							</div>
							<!--<div class="col s7 m7 l7" style="text-align:left;">
								<p style="color: #000;">signature.png</p>
								<p style="color: #000; font-size:11px !important;">Image size 40px X 40px</p>
							</div>-->
							<?php //} ?>
						<!--/div>
						<div class="col s12 m12 l12" style="text-align:center; padding-left:50px;">
							<p style="font-size:13px;"><label class="up_lbl1"><?php //echo strtoupper($reg_data[0]->reg_username) ?></label>      - <label class="up_lbl1"><?php //echo strtoupper($reg_data[0]->reg_designation) ?></label></p>
						</div>
					</div>
				</div>
				<div class="row" style="padding:0px 10px 10px 10px !important; border-bottom:1px solid #000000 !important;">
					<div class="col s12 m12 l12" id="cond">
						<label class="up_lbl1"><b> TERMS & CONDITIONS</b></label><br>
						<p class="temp_text">Payments matters are subject to Mumbai jurisdiction only</p>
						<p class="temp_text">any discrepancy regarding this bill must be notified within 5 days from the date of submission of this bill</p>
						<p class="temp_text">24% P.A. interest will be charged on Overdue Unpaid Amount</p>
					</div>
				</div>
				<div class="row" id="footer_box">
					<div class="col s12 m12 l6">
						<div class="col s12 m12 l12" id="facebk">
							<label class="up_lbl1"><b>FACEBOOK</b> : </label><label class="up_lbl1">www.facebook.com/pp?1</label>
						</div>
						<div class="col s12 m12 l12" id="webst">
							<label class="up_lbl1"><b>WEBSITE</b><label style="padding-right:15px !important;"></label>: </label><label class="up_lbl1">www.website.com</label>
						</div>
						<div class="col s12 m12 l12" id="twitter">
							<label class="up_lbl1"><b>TWITTER</b><label style="padding-right:15px !important;"></label>: </label><label class="up_lbl1">www.twitter.com/pp?1</label>
						</div>
						<div class="col s12 m12 l12" id="link_id">
							<label class="up_lbl1"><b>LINKEDIN</b><label style="padding-right:12px !important;"></label>: </label><label class="up_lbl1">www.linkedin.com/pp?123</label>
						</div>
					</div>
					<div class="col s12 m12 l6">
						<div class="col s12 m12 l12" id="addr">
							<p class="footer_p" id="address"><?//= strtoupper($company[0]->bus_company_name) ?>,<br>Near bapat marg, Andheri,<br>Mumbai- 400013</p>
						</div>	
						<div class="col s12 m12 l12" id="telno">
							<label class="up_lbl1"><b>TEL. NO.</b><label style="padding-right:13px !important;"></label>: </label><label class="up_lbl1">022-25778 878</label>
						</div>
						<div class="col s12 m12 l12" id="email_id">
							<label class="up_lbl1"><b>EMAIL ID</b><label style="padding-right:10px !important;"></label>: </label><label class="up_lbl1">emailid.@email.com</label>
						</div>
					</div>
				</div-->
				</div>
			</div><!-- Default Template End -->
		
		<!-- First Template Start -->
		<div id="first-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px; border:2px solid #595959; padding:0 22px !important;" hidden>
			<a style="float:right; margin:5px -22px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>			
			<head>
			<style>
				.big-text{
					font-size: 20px;
				}
				.small-text{
					font-size: 13px;
				}   
				.medium-text {
					font-size: 13px !important;
				}
				
				.teal.lighten-2{
					background-color:#000 !important;
				}
				
				.breadcrumbs-title, .breadcrumbs, .breadcrumbs-title, .small-text, .medium-text{
					color:#fff;
				}
				
				#first-tem-table .lime{
					margin-bottom:10px;
				}
				
				#first-tem-table tr th td{
					background-color:#f2f2f2 !important;
					border:none !important;
				}
				
				#first-tem-table .unit-pr{
					background-color:#b3b3b3 !important;
				}
				
				#first-tem-table .pr-last{
					background-color:#4CAF50 !important;
				}
				
				#total-last{
					color:#000 !important;
					font-size:13px !important;
				}
				
				#first-template p{
					font-size:13px;
				}
				
				.term-c, .bank-transfer{
					font-size:15px !important;
				}
				
				.term-c span, .bank-transfer span{
					font-size:13px;
					color:#000; 
				}
				
				.gray{
					background-color:#ccc !important;
				}
			</style>
			</head>
			<body class="container">
			<div class="card-panel teal lighten-2">
            <div class="row">
                <div class="col l4">
                    <img class="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo">
					<h6 class="breadcrumbs-title temp_text">COMPANY NAME</h6>
                </div>
                <div class="col l4">
                    <p style="color:#fff !important;"> 
						<span style="color:#fff; font-size:13px;" class="cinno breadcrumbs-title temp_text">CIN NO.:</span>
						<span style="color:#fff !important; font-size:13px;" class="cinno breadcrumbs temp_text">U64200MH2008XXXI23456</span></br>
						<span style="color:#fff !important; font-size:13px;" class="breadcrumbs-title temp_text">PAN NO.:</span>
						<span style="color:#fff !important; font-size:13px;" class="breadcrumbs temp_text">AAAXXX131F</span> <br/>
						<span style="color:#fff !important; font-size:13px;" class="breadcrumbs-title temp_text">GSTIN:</span>
						<span style="color:#fff !important; font-size:13px;" class="breadcrumbs temp_text">27AAAXX131F1ZH</span> <br/>
					</p>
                </div>
                <div class="col l4">
                    <p style="color:#fff !important; font-size:13px;" class="temp_text addr">45, 4TH FLOOR, ABC BUILDING,<br/> 
                    BK MARG, BANDRA-EAST<br/>
                    MUMBAI-400051, MAHARASHTRA, INDIA</P>
					<p style="color:#fff !important; font-size:13px;" class="temp_text telno">TEL. NO.: 022-2XXX8 878</p>
					<p style="color:#fff !important; font-size:13px;" class="temp_text email_id">EMAIL ID: emailid@email.com</p>	
                </div>
            </div>
            <hr/>
            <div class="row" style="margin-bottom:0px !important;">
                <div class="col l4 right">
					</br>
                    <h4 class="breadcrumbs-title temp_text">SALES INVOICE</h4>
                    <!--p class="breadcrumbs">N o . S B - 9 5 8 3 1 6 4</p-->
                </div>
            </div>
            <div class="row">
                <div class="col l4">
					<h6 style="color:#fff !important;" id="eoe_div" class="eoe_div temp_text"><b>E&OE </b></h6>		
					<p> <span style="color:#fff !important;" class="temp_text"> INVOICE TO </span> <br/>
                        <span style="color:#fff !important;" class="temp_text">NEW COMPANY LLP,</span><br/>
                        <span style="color:#fff !important;" class="temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD, <br/> MALAD-WEST, MUMBAI, MAHARASHTRA INDIA-400061</span></p>
                    <P style="color:#fff !important; font-size:13px;" class="temp_text gst_1">GSTIN : 27AZXSDXXXRXX1 </p> 
					<P style="color:#fff !important; font-size:13px;" class="temp_text pan-1">PAN : AZXSDXXXR</span></p>
                    <P style="color:#fff !important; font-size:13px;" class="temp_text">PLACE OF SUPPLY : MAHARASHTRA</p>
                    <P style="color:#fff !important; font-size:13px;" class="temp_text">TERMS OF PAYMENT: 15</p>
                </div>
                <div class="col l7 right card-panel green white-text" style="padding:10px 0;">
                    <p class="col l3"><span class="temp_text">INVOICE NO.</span> <br/> <span class="temp_text">INV1/2019-20</span></p>
                    <p class="col l3"><span class="temp_text">INVOICE DATE</span> <br/> <span class="temp_text">15-08-2019</span></p>
                    <p class="pu_no col l3"><span class="temp_text">P.O. NUMBER</span> <br/> <span class="temp_text">10/2019-20</span></p>
                    <p class="pu_o_date col l3"><span class="temp_text">P.O. DATE</span> <br/> <span class="temp_text">10-05-2019</span></p>
                </div>
            </div>
        </div>
        <div>
            <table id="first-tem-table" class="centered">
                <thead class="gray">
                    <tr>
                    <th class="temp_text" style="text-align: left;">ITEM DETAILS</th>
                    <th class="temp_text">UNIT PRICE</th>
                    <th class="temp_text">QNTY</th>
					<th class="temp_text">DISCOUNT</th>
                    <th class="temp_text">PRICE</th>
                    </tr>
                </thead>
                <tr>
                    <td style="text-align: left;">
                        <p class="temp_text"><b></b><br/> <span class="" ></span></p>
                    </td>
                    <td class="temp_text unit-pr"></td>
                    <td class="temp_text"></td>
					<td class="temp_text"></td>
                    <td class="temp_text pr-last"></td>
                </tr>
                
                <tr>
                    <td style="text-align: left;">
                        <p class="temp_text"><b></b> <br/> <span class="" ></span></p>
                    </td>
                    <td class="temp_text unit-pr"></td>
                    <td class="temp_text"></td>
					<td class="temp_text"></td>
                    <td class="temp_text pr-last"></td>
                </tr>
            </table>
            <div class="row" style="margin-top:10px;">
                <div class="col l2 right" style="text-align: right;">
                    <p style="font-size:13px;" class="temp_text">INR 0</p>
                    <p style="font-size:13px;" class="total-last temp_text">
                        INR 0 <br/> INR 0 <br/>
                    </p>
                </div>
                <div class="col l2 right">
                    <p style="font-size:13px;" class="temp_text">SUB TOTAL</p>
                    <p style="font-size:13px;" class="temp_text total-last">
                        + SGST (9%) <br/> + CGST (9%)</br>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col l6">
					<button class="make-payment temp_text" class="btn green white-text" style="font-size: 15px !important; padding:5px 20px; border-radius:5px;" type = "submit"> MAKE PAYMENT </button>
				</div>
				<div class="col l6 right">
					<p class="right card-panel green temp_text" style="font-size: 18px;">GRAND TOTAL: INR  </p>
                </div>
            </div>
			<div class="row">
				<div class="col l8 right">
					<p class="right temp_text" style="font-size: 15px"><b>IN WORDS: RUPEES ONLY</b></p>
                </div>
			</div>
            <br/>
            <div class="row">
                <div class="col l4 bank_info">
                    <p class="bank-transfer temp_text"><b>BANK TRANSFER</b><br/>
                        <span class="temp_text" style="color:#000;">
                            Bank Name<span style="padding-left:14px !important;"></span>: MBSP BANK <br/>
                            A/C Number<span style="padding-left:9px !important;"></span>: 1000XXX5253<br/>
							Branch<span style="padding-left:42px !important;"></span>: Mumbai Main Road <br/>
                            IFSC Code<span style="padding-left:20px !important;"></span>: MBSP000111
                        </span>
                    </p>
                </div>
                <div class="col l4" align="center">
                    <br/><br/>
                </div>
				
                <div class="col l4 sign_box" style="text-align: center;">
                    <p class="temp_text" style="text-align: center;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
					<?php if($reg_data[0]->reg_degital_signature!=""){?>
						<img width="250" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
					<?php } ?></br>
					<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col 16 add cond">
					<label style="color:#000;" class="term footer_p"><b>TERMS &amp; CONDITIONS</b></label><br>
					<ul class="term1-list" style="margin-left:18px;">
						<li><p style="color:#000;" class="footer_p">Payments matters are subject to Mumbai jurisdiction only</p></li>
						<li><p style="color:#000;" class="footer_p">any discrepancy regarding this bill must be notified within 5 days from the date </br>of submission of this bill</p></li>
						<li><p style="color:#000;" class="footer_p">24% P.A. interest will be charged on Overdue Unpaid Amount</p></li>
					</ul>
				</div>
				<div class="col 16" style="padding-top:15px;">
					<label style="font-size:13px; color:#000;" class="facebk footer_p"><b>FACEBOOK<span style="padding-left:4px !important;"></span>: </b></label><label style="color:#000; font-size:13px;" class="facebk footer_p">www.facebook.com/abc</label></br>    
					<label style="color:#000; font-size:13px;" class="webst footer_p"><b>WEBSITE<span style="padding-left:14px !important;"></span>: </b></label><label style="font-size:13px; color:#000;" class="webst footer_p">www.website.com</label></br>
					<label style="color:#000; font-size:13px;" class="twitter footer_p"><b>TWITTER<span style="padding-left:12.5px !important;"></span>: </b></label><label style="font-size:13px; color:#000;" class="twitter footer_p">www.twitter.com/abc</label></br>
					<label style="color:#000; font-size:13px;" class="link_id footer_p"><b>LINKEDIN<span style="padding-left:10px !important;"></span>: </b></label><label style="font-size:13px; color:#000;" class="link_id footer_p">www.linkedin.com/abc</label>
				</div>
            </div>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
            </div-->
			</div>
		</div>
		<!-- First Template END -->
		
		<!-- Second Template END -->
		<div id="second-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<head>
			<style>
				.white-text{
					color:#fff !important;
				}
				
				.small-text{
					color:#000 !important;
					font-size: 13px;
				}   
				.medium-text {
					color:#000 !important;
					font-size: 15px;
				}
				.top-hr{
					margin: 15px 0px 0px -70px;
					height: 8px;
					background-color:#9c27b0;
				}
				
				.purple{
					font-size:13px !important;
				}
				
				#second-template p{
					font-size:13px;
					color:#000 !important;
				}
				
				.cust-name{
					font-size:15px;
				}
				
				.cust-details{
					font-size:13px !important;
				}
				
				.inv-de{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#fff !important;
				}
				
				.inv-info{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#000 !important;
					font-size:13px !important;
				}
				
				#sec-table thead  th{
					background-color:#7979d2 !important; 
					padding:20px !important;
				}
				
				.purple{
					background-color:#7979d2 !important; 
				}
				
				.tot-inr{
					padding:20px !important;
				}
				
				.total-word{
					font-size:15px !important;
				}
			</style>
			</head>
			<body class="container">
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l4">
					<img src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo">
				</div>
				<div class="col l8"><hr class="top-hr"></hr></div>
			</div>	
			<div class="row">
            <div class="col l6" style="margin-left:10px;">
                <p style="margin-bottom:-20px !important; margin-top: 20px;">
                    <span class="purple white-text" style="padding-left: 2px;padding-right: 2px;">INVOICE</span><span class="to"> TO <span><br/>
                    <span class="cust-name"><b>Suhas Sawant</b><span> <br/>
                    <span class="cust-com medium-text">Manager, Windchimes Communications Pvt. Ltd.</span>
                </p></br>
                <br/>
                <p class="cust-details small-text">
                    <b>PHONE:</b> +91 9819184721, +91 2226470454   <br/>
                    <b>EMAIL</b> suhas@windchimes.co.in   <br/>
                    <b>WEBSITE</b> www.windchimes.co.in   <br/>
                    <b>ADDRESS:</b> 402, Capri Building, A.K. Marg, Near Bandra Court, <br>    Bandra East, Mumbai - 400051. <br/>
                    <b>GSTIN:</b> 345678 <br/>
                    <b>PAN:</b> 901234 <br/>
                    <b>PLACE OF SUPPLY:</b> DADAR 
                </p>
            </div>
            <div class="col l4 right">
                <h3 class="black-text" style="margin-bottom: 10px; margin-left: 8px;"> INVOICE </h3>
                <div class="col white-text">
                    <p>
                        <span class="purple inv-de">INVOICE DATE</span>  <br/></br>
                        <span class="purple inv-de">P.O. NO.</span>      <br/><br/>
                        <span class="purple inv-de">P.O. DATE</span>     <br/><br/>
                        <span class="purple inv-de">GSTIN</span>         
                    </p>
                    <p style="margin-top:25px;">
                        <span class="inv-de black">DUE DATE</span>
                    </p>
                </div>
                <div class="col">
                    <p>
                        <span class="inv-info">: March 10,2018</span> <br/><br/>
                        <span class="inv-info">: 123456789 </span>    <br/><br/>
                        <span class="inv-info">: March 10, 2018</span>  <br/><br/>
                        <span class="inv-info">: 1234567890</span>      
                    </p>
                    <p style="margin-top:25px;">
                        <span class="inv-info">: April 10, 2018</span>
                    </p>
                </div>
                <div class="col"></div>
            </div>
			</div>
			<div class="row">
            <table id="sec-table" class="centered striped">
                <thead class="white-text">
                    <tr>
                        <th style="text-align: left; padding-left:5%; ">ITEM NAME & DESCRIPTION</th>
                        <th>SAC</th>
                        <th>QUANTITY</th>
                        <th>RATE</th>
                        <th>DISCOUNT</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">Web</span> Design <br/>
                            <span class="small-text">This is a sample letter that has been placed <br/> 
                                to demonstrate the typing format</span>
                        </p>
                    </td>
                    <td>342</td>
                    <td>3</td>
                    <td>INR 653.00</td>
                    <td>15%</td>
                    <td>INR 1959.00</td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">Typography</span> Idea <br/>
                            <span class="small-text">This is a sample letter that has been placed <br/> 
                                to demonstrate the typing format</span>
                        </p>
                    </td>
                    <td>658</td>
                    <td>4</td>
                    <td>INR 125.00</td>
                    <td>30%</td>
                    <td>INR 500</td>
                </tr>
            </table>
            <hr class="purple" style="height: 2px;"/>
			</div>
			<div class="row">
            <div class="col l2 right" style="margin-left: 5px;">
                    <span style="color:#7979d2 !important;">INR 4429.00 </span><br/>
                    <p class="medium-text">
                        INR 664.35 <br/> INR 664.35 <br/>
                        INR 250    <br/> INR 200  <br/>
                    </p>
                </div>
                <div class="col l2 right" style="text-align: right; margin-right: 5px;">
                    <span style="color:#7979d2 !important; ">SUB TOTAL</span> <br/>
                    <p class="medium-text">
                        +SGST(09%) <br/> +CGST(09%) <br/> 
                        +CESS <br/> +OTHERS <br/>
                    </P>
                </div>
			</div>
			<div class="row">
				<div class="col right" style="margin-right: 70px; margin-top: -25px; padding:10px;">
                    <p class="right tot-inr card-panel purple white-text" style="color: #fff !important;">TOTAL DUE: <span class="white-text tot-inr" style="margin-left: 25px; color:#fff !important;">INR 5757.7</span></p> <br/>
					<P class="right total-word">FIVE THOUSAND SEVEN HUNDRED AND FIFTY SEVEN POINT SEVEN</P>
                </div>
            </div>
			<div class="row">
                <div class="col l4">
                    <p><span class="purple white-text" style="padding: 8px; margin-bottom:10px;"> Payment </span> </br></br>
                        <span class="small-text grey-text" style="margin-top:10px !important; font-size:13px !important;">
                            <b>PAYMENT METHOD:</b> Bank Transfer <br/>
                            <b>A/C NUMBER:</b> 23568974123568 <br/>
                            <b>BANK NAME:</b> HDFC Bank   <br/>
                            <b>IFSC CODE:</b> ICIC0012354 </br>
                        </span>
                    </p>
                    <button class = "btn purple white-text" style="margin-top:10px; font-size: 20px;" type = "submit"> MAKE PAYMENT </button>
                </div>
            <div class="col l4 right" style="margin-right: 70px;">
                <br/><br/>
                    <p style="text-align: right; font-size:13px !important;">Mr. Amit Shah <br/>  <span class="small-text">Account Manager</span> </p>
                </div>
            </div>
			<div class="row" style="border-top:1px solid #000 !important; margin-top:20px;">
                <br/>
                <p class="medium-text"><b>TERMS & CONDITIONS</b> <br/>
                    <span style="font-size:13px !important;" class="small-text">Payment Should be made within 30 days by cheque, <i>Paypal</i> or bank transfer.</span>
                </p>
            </div>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
                <p class="col l4">
                    <span class="medium-text">
                        Address:
                    </span> <br/>
                    ABC Finance Private Limited. <br/>
                    110/112, B Wing, Kalpataru Enclave, S.V. Road,  <br/>
                    Santacruz - west    <br/>
                    TEL NO. :9769976482 <br/>
                    EMAIL ID :suhas@windchimes.co.in <br/>
                    
                </p>
            </div-->
		</body>
		</div>
		<!-- Second Template END -->
		
		<!-- Third Template Start -->
		<div id="third-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<head>
			<style>
				#third-template p{
					font-size:13px;
				}
				.header1{
					height:170px;
				}
				.header2{
					height:160px;
					padding-top:15px;
				}
				.invoice{
					line-height: 1em;
					font-size: 13px;
				}
				.invoice-details{
					line-height: 1.4em;
					font-size: 13px;
				}
				.dp{
					color:#fa3b59
				}
				#table-first tr{
					height: 78px;
					border-bottom: 0.8px solid black;
					font-size: 13px;
					line-height: 1em;
				}
				#table-first{
					width: 100%;
					padding-bottom: 50px;
				}
				.header1, .header2{
					background-color: #262930;
				}
				
				#table-first tbody tr{
					background-color:#fff !important;
				}
				
				#table-first td{
					max-width: 450px;
					padding-top:20px;
					padding-bottom: 20px;
					padding-right: 8px;
				}
				.desc{
					font-size: 13px;
				}
				#payment{
					line-height: 0.5em;
					font-size: 13px;
				}
				#sign{
					border-bottom: 1px black solid;
					height: 36px;
					width:213px;
					
				}
				#gst{
					width:250px;
					border-bottom: 1px black solid;
					background-color: #fa3b59;
					padding:10px;
				}
				#gst p{
					margin-bottom: 0px !important;
					padding-bottom: 0px !important ;
					padding-right: 4px;
					
				}
				#total{
					width:250px;
					border-top:1px black solid;
					font-size: 18px;
				}
				.footer{
					margin-top: 50px;
					border-bottom: 10px solid #50e3c2;
				}
				.footer p{
					background-color: #ebf0ec;
					font-size: 13px;
					margin-bottom: 0px;
				}
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;
					line-height: 1em;
					font-size: 15px;
				}
				#social, #company{
					height:43px;
				}
				.btn, footer{
					background-color: #ebf0ec;
				}
				
				h5, h4, .m-1{
					color:#fff;
				}
				
				.paybtn{
					color: #fff;
					padding: 0 8%;
					background: #0059b3;
					border-radius: 5px;
					border: 1px solid #0059b3;
					margin: 6px -3px;
				 }
			</style>
			</head>
		<body>
		<div class="container inv_tem" style="width:790px !important">
        <div class="row pt-3 pb-4 header1 text-white" style="border-bottom: #fb395b 7px solid;">
            <div class="col s12 m12 l2" style="padding:5% 0;">
				<img class="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="130" alt="eazyinvoices-logo">
			</div>
			<div class="col s12 m12 l4" style="margin-top:-20px;">
                <h5 class="pl-2 temp_text">COMAPNY NAME</h5>
				<p class="temp_text addr" style="color:#fff; padding-left:10px;">P402, Capri Building, A.K. Marg, </br>Near Bandra Court, <br> Bandra East, Mumbai - 400051. </p>
				<p style="color:#fff !important; font-size:13px; padding-left:10px;" class="temp_text telno">TEL. NO.: 022-25778 878</p>
				<p style="color:#fff !important; font-size:13px; padding-left:10px;" class="temp_text email_id">EMAIL ID: emailid.@email.com</p>	
            </div>
            <div class="col s12 m12 l6 text-right">
                <h4 class="pr-2 temp_text" style="padding:9% 0; font-weight:800; color:#fa3b59">SALES INVOICE</h4>
            </div>
        </div>
        <div class="row pt-3 header2 text-white" style="margin-top:-20px;">
            <div class="col s12 m12 l6 pl-3 invoice">
                <h6 style="margin-top:0px !important; color:#fff !important;" id="eoe_div" class="eoe_div temp_text"><b>E&OE </b></h6>		
				<p class="dp temp_text" style="font-size:15px;">INVOICE TO:</p>
                <p class="font-weight-bold m-1 temp_text"><b>Suhas Sawant</b></p>
                <p class="font-weight-bold m-1 temp_text"><b>Accounts Dept.</b></p>
                <p class="temp_text m-1">Windchimes Communications</p>
                <p class="m-1 temp_text">Bandra East</p>
                <p class="m-1 temp_text">Mumbai-400051</p>
				<p class="m-1 gst_1 temp_text"><span class="dp temp_text">GSTIN: </span>982879292989</p>
				<p class="m-1 pan-1 temp_text"><span class="dp temp_text">PAN: </span>EDFRT345T</p>
            </div>
            <div class="col s12 m12 l6 text-left pr-3 invoice-details text-right">
                <p class="m-1 temp_text"><span class="dp temp_text">INVOICE NO: </span> INV001</p>
                <p class="m-1 temp_text"><span class="dp temp_text">INVOICE DATE: </span>07 Apr, 2018</p>
                <p class="m-1 pu_no temp_text"><span class="dp temp_text">P.O. NUMBER: </span>#00198</p>
                <p class="m-1 pu_o_date temp_text"><span class="dp temp_text">P.O. DATE: </span>25 Oct, 2018</p>
                <p class="m-1 temp_text"><span class="dp temp_text">PLACE OF SUPPLY: </span>Delhi</p>
				<p class="m-1 temp_text"><span class="dp temp_text">TERMS OF PAYMENT: </span>30</p>
            </div>
        </div>
        <div class="row">
        <table id="table-first">
            <thead>
			<tr class="text-center" style="background-color: #ebf0ec; border-bottom:none;">
                <th class="text-left temp_text">ITEM DESCRIPTION</th>
                <th class="temp_text">SAC NO.</th>
                <th class="temp_text">QNTY</th>
                <th class="temp_text">RATE</th>
                <th class="temp_text">DISCOUNT</th>
                <th class="temp_text">TOTAL</th>
            </tr>
			</thead>
			<tbody>
            <tr class="text-center">
                <td class="text-left"><span class="font-weight-bold d-block heading temp_text">Website Designing</span><span class="desc temp_text">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit. Modi </span></td>
                <td class="temp_text">236545</td>
                <td class="temp_text">23</td>
                <td class="temp_text">200,000</td>
                <td class="temp_text">25%</td>
                <td class="temp_text">150,000</td>
            </tr>
            <tr class="text-center">
                <td class="text-left"><span class="temp_text font-weight-bold d-block heading">Website Designing</span><span class="desc temp_text">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit. Modi </span></td>
                <td class="temp_text">236545</td>
                <td class="temp_text">23</td>
                <td class="temp_text">200,000</td>
                <td class="temp_text">25%</td>
                <td class="temp_text">150,000</td>
            </tr>
            <tr class="text-center">
                <td class="text-left"><span class="temp_text font-weight-bold d-block heading">Website Designing</span><span class="desc temp_text">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit. Modi </span></td>
                <td class="temp_text">236545</td>
                <td class="temp_text">23</td>
                <td class="temp_text">200,000</td>
                <td class="temp_text">25%</td>
                <td class="temp_text">150,000</td>
            </tr>
			</tbody>
        </table>
        </div>
        <div>
		<div class="row">
            <div class="col s12 m12 l12" id="payment">
                <p class="temp_text"><b>CIN:</b> U64200MH2008PTC123456  <b>PAN NO.:</b> AAABB1122D  <b>GST:</b> 27AAABB1122D1ZJ<p>
			</div>
		</div>
        <div class="row">
            <div class="col s12 m12 l6" id="payment">
				<a style="margin:15px 0;" href="#" class="temp_text paybtn btn btn-lg my-2 make-payment">Make Payment</a>
                <div class="bank_info"><p class="temp_text" id="ac-number">A/C Number: 01992823738</p></br></br>
                <p id="bank-name" class="temp_text">Bank Name: ICICI Bank</p></br></br>
                <p id="branch" class="temp_text">Branch: Bandra East</p></br></br>
                <p id="ifsc" class="temp_text">IFSC Code: ICCI202928</p></br></br>
				</div>
            </div>
            <div class="col s12 m12 l6 text-right">
                <p class="font-weight-bold h4 temp_text"><b>SUB TOTAL:</b>INR 1,250,000</p>
                <div class="pl-2 float-right text-left" id="gst">
                    <p class="temp_text"><b>+SGST(9%):</b>INR 112,500</p>
                    <p class="temp_text"><b>+CGST(9%):</b>INR 112,500</p>
                </div>
                <div class="clearfix"></div>
                <div class="font-weight-bold float-right text-left pl-3" id="total">
                    <p class="temp_text" style="font-size:23px !important;"><b>TOTAL: INR 1,475,000</b></p>
                </div>
            </div>
        </div>
        <div class="row">
            <!--div id="sign" class="ml-auto"></div-->
            <div class="sign_box text-right pr-5 pt-2">
				<p class="temp_text">FOR COMAPNY NAME</p><br>
				<p class="temp_text">Amit Shah - Manager</p>
			</div>
        </div>
		</hr>
        <div class="row">
            <div class="col s7 m7 17 add cond">
					<label style="color:#000;" class="term footer_p"><b>TERMS &amp; CONDITIONS</b></label><br>
					<p class="footer_p">Payments matters are subject to Mumbai jurisdiction only</p>
					<p class="footer_p">any discrepancy regarding this bill must be notified within 5 days from the date of submission.</p>
					<p class="footer_p">24% P.A. interest will be charged on Overdue Unpaid Amount</p>
				</div>
				<div class="col s5 m5 15" style="padding-top:8px;">
					<label class="facebk term footer_p"><b>FACEBOOK&nbsp&nbsp: </b></label><label class="facebk term footer_p">www.facebook.com/pp?1</label></br>    
					<label class="webst term footer_p"><b>WEBSITE&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label class="webst term footer_p">www.website.com</label></br>
					<label class="twitter term footer_p"><b>TWITTER&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label class="twitter term footer_p">www.twitter.com/pp?1</label></br>
					<label class="term link_id footer_p"><b>LINKEDIN&nbsp&nbsp&nbsp&nbsp: </b></label><label class="term link_id footer_p">www.linkedin.com/pp?123</label>
				</div>
        </div>
        <!--<footer class="container">
            
            <div class="row pb-5 pt-2">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>
        
        </footer>-->
        </div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		</body>
		</div>
		<!-- Third Template END -->
		
		<!-- Fourth Template Start -->
			
			<div id="fourth-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<style>
				#fourth-template p{
					margin-bottom: 0px;
					font-size: 13px;
				}
				.address{
					line-height: 1em;
				}
				#invoice{
					background: url(ColourBand.png) no-repeat center;
					background-size: cover;
					font-size: 24px;
					letter-spacing: 0.4em;
					font-weight: 7px;
				}
				#logo{
					margin-bottom: 80px;
				}
				.mx-auto{
					width:100% !important;
				}
				.mx-auto th{
					height:20px;
					background-color: rgb(117, 117, 117);
					color: white;
					padding: 13px 10px;
					font-weight: 500;
					font-size: 0.8em;
				}
				.mx-auto td{
					padding:13px 10px;
					border-bottom: 1px solid black;
					font-weight: 500;
					font-size: 0.8em;
				}
				#total-cost{
					padding-right:98px;
				}
				#total-cost p{
					padding:13px 0px;
					border-bottom: 1px solid black;
				}
				#item-desc-head{
					background: url(ColourBand.png) no-repeat center;
					background-size:cover;
					width:300px;
				}
				#sign{
					width:80%;
					text-align:right !important;
					margin-top:15% !important;
				}
				
				footer{
					margin-top: 50px;
					border-bottom: 10px solid #50e3c2;
				}
				.footer p{
					background-color: #dddee2;
					font-size: 13px;
					margin-bottom: 0px;
				}
				/*
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;;
				}*/
				
				.first-head{
					background-color:#dddee2 !important;
					color: #000 !important;
				}
				
				#item-desc-head{
					background-color:#ff6666 !important;
				}
				
				.bnk-de{
					font-size:13px !important;
				}
				
				.pr-3, .pr-5, .mb-0, .pb-2{
					font-size:13px !important;
				}
				
				#companydetails ,.inv_tail{
					text-align:right !important;
				}
			</style>
		</head>
		<body>
		<div class="container" style="width:790px">
		<div class="row">
			<div class="col s12 m12 l7">
				<p class="" id="logo">LOGO</p>
			</div>	
			<div class="col s12 m12 l5">
				<div id="companydetails" class="mb-5">
					<p class="address">405, Capri Building, A K Marg</p>
					<p class="address">Near Bandra Court, Bandra East,</p>
					<p class="address">Mumbai - 400051</p>
					<p class="py-1"><i class="fa fa-phone" aria-hidden="true"></i> +91 22 2647 0475</p>
					<p class="pb-1"><i class="fa fa-envelope" aria-hidden="true"></i> help@eazyinvoice.co.in</p>
					<p class="pb-1"><i class="fas fa-adjust"></i>www.eazyinvoice.co.in</p>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col s12 m12 l7">
				<p class="font-weight-bold">To,</p>
                <p class="h4 mb-0"><b>SUHAS SAWANT</b></p>
                <p class="font-weight-bold"><b>Manager, Windchimes Communication Pvt. Ltd.</b></p>
                <p>A 02, Capri Building, A K Marg, </br>Near Bandra Court, Bandra East, Mumbai - 400051</p>
                <p>GSTIN: 9002929890</p>
                <p>PAN: 0390101HJS89</p>
                <p>PLACE OF SUPPLY: Maharashtra</p>
			</div>
			<div class="col s12 m12 l5 inv_tail">
				<div class="text-light p-1 pl-3 mb-4" id="invoice">
					<b>Invoice</b>
				</div>
				<div id="invoice-details" class="pl-3">
					<p><b>INVOICE NO.</b></p>
					<p class="h3" style="font-size:18px !important;"><b>#23698720</b></p>
				<div class="d-inline-block" style="text-align:left !important; margin-left:48%;">
					<p><b>INVOICE DATE</b><span style="margin-left:5px;"></span>: 30 April 2018</p>
					<p><b>ISSUE DATE</b><span style="margin-left:17px;"></span>: 2 April 2018</p>
					<p><b>P.O. NUMBER</b><span style="margin-left:10px;"></span>: 0598297392</p>
					<p><b>P.O. DATE</b><span style="margin-left:28px;"></span>: 25 April 2018</p>
					<p><b>GSTIN</b><span style="margin-left:46px;"></span>: 7292829291</p>
					<p><b>PAN</b><span style="margin-left:56px;"></span>: 8923929JH82H</p>
					<p><b>CIN</b><span style="margin-left:59px;"></span>: 823929</p>
				</div>
				</div>
		  </div>
		</div>
		<div class="row">
        <div class="col s12 m12 l12">
        <table class="mx-auto">
            <tr class="bg-light text-center">
                <th class="bg-light text-dark first-head" style="background-color:#dddee2">NO.</th>
                <th class="text-left" id="item-desc-head">ITEM DESCRIPTION</th>
				<th style="background-color:#00b300 !important">UNIT PRICE</th>
                <th class="text-dark" style="background-color:#dddee2 !important;">QNTY</th>
                <th style="background-color:#0080ff !important;">TOTAL</th>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
				<td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td class="">1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td class="">1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
        </table>
		</div>
		</div>
		<div class="row">
            <div class="col s12 m12 l6 pl-5" id="">
                
            </div>
            <div class="col s12 m12 l6 text-right" id="total-cost">
                <p class="pr-3"><span class="pr-5 text-left">SUB TOTAL:</span>INR 16,900</p>
                <p class="pr-3"><span class="pr-5">TAX: GST(15%)</span>INR 22,780</p>
                <p class="pr-3"><span class="pr-5">GRAND TOTAL:</span>INR 18,228.0</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l6 pl-5" id="payment">
                <a href="#" class="btn purple white-text btn-lg mb-2 mt-2">MAKE PAYMENT</a></br></br>
                <p class="bnk-de" id="ac-number">A/C NUMBER: 01992823738</p></br></br>
                <p class="bnk-de" id="bank-name">BANK NAME: ICICI Bank</p></br></br>
                <p class="bnk-de" id="branch">BRANCH: Bandra East</p></br></br>
                <p class="bnk-de" id="ifsc">IFSC CODE: ICCI202928</p></br></br>
            </div>
            <div class="col s12 m12 l6 pr-3 pt-2 d-flex justify-content-end">
                <div id="sign">
                <p class="h5 mb-0">Smako Atson</p>
                <p class="text-secondary h6 mb-0">Creative head</p>
                <p class="h1 pb-2">SIGN</p>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>TERMS & CONDITIONS:</p> 
			<p>Payment should be made within 30 days of the invoice date and either made via bank transfer or cheque</p>
        </div>
        <footer class="container mt-0">
            <!--<div class="row py-4 bg-secondary">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>-->
        </footer>
		</div>
		</body>
		</div>
		
		<!-- Fourth Template END -->
		
		
		<!-- Fifth Template Start -->
		<div id="fifth-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
		<head>	
		<style>
        /*.container{
            background: url(Temp-2-1.jpg) no-repeat center;
            background-size: cover;
            min-height: 1000px;
        }
        #fifth-template p{
            margin:0;
            line-height: 1em;
            font-size: 15px;
        }
        .invoice-dets{
            width: 280px;
            height:150px;
        }
        .invoice-dets > p{
            font-size: 14px !important;
        }
        #inv .pl-5{
            font-size: 50px !important;
            font-weight: 400;
            letter-spacing: 0.1em;
        }
        .sidebar{
            height:1000px; 
            position: relative;
        }
        .to{
            position:absolute;
            bottom:0;
        }
        #fifth-template #tem-fifth-table {
            margin-top: 250px !important;
			width:80% !important;
			margin-left:5% !important;
        }
        #fifth-template #tem-fifth-table td,th{
            max-width: 280px;
            min-width: 80px;
            padding: 13px 10px 13px 0;
        }
        #fifth-template #tem-fifth-table tr{
            border-bottom: 1px solid black;
        }
        #payment{
            line-height: 0.5em;
            font-size: 13px;
        }
        #gst{
            width:200px;
			background-color:#ffcc00 !important;
        }
        #gst p{
            margin-bottom: 0px !important;
            padding-bottom: 0px !important ;
            padding-right: 4px;
            
        }
        #total{
            width:250px;
            border-top:1px black solid;
            font-size: 18px;
        }*/

    </style>
		</head>
		<body>
    <div class="container" style="width:837px;">
        <div class="row">
            <div class="col s12 m12 l4 sidebar">
                <p class="pl-5" style="font-size:40px !important; margin-bottom:0px !important; margin-left:-4px;" id="inv">I N V O I C E</p>
                <div class="col s12 m12 l6 float-left pl-4 text-left invoice-det font-weight-bold">
                    <p>INVOICE NO.</p>
                    <p>INVOICE DATE</p>
                    <p>P.O. NUMBER</p>
                    <p>P.O. DATE</p>
                    <p>GSTIN</p>
                    <p>PAN</p>
					<p>CIN</p>
                </div>
                <div class="col s12 m12 l6 float-right text-right invoice-det-value font-weight-bold pr-3">
                        <p>89077853</p>
                        <p>07, April,2018</p>
                        <p>#13245</p>
                        <p>25 October,2018</p>
                        <p>112233255</p>
                        <p>898789JH89</p>
                        <p>8909898</p>
                </div>
                <!--div class="to pb-5">
                    <p class="m-1 font-weight-bold">INVOICE TO:</p>
                    <p class="font-weight-bold m-1">Suhas Sawant</p>
                    <p class="font-weight-bold m-1">Accounts Dept.</p>
                    <p class="m-1">Windchimes Communications</p>
                    <p class="m-1">Bandra East</p>
                    <p class="m-1">Mumbai-400051</p>
                </div-->
            </div>
			<div class="col s12 m12 l8" style="text-align:right;">
				LOGO HERE
			</div>
		</div>
		<div class="row">
            <div class="col s12 m12 l12">
                <table id="tem-fifth-table">
                    <tr class="text-center" style="border-bottom:none;">
                        <th class="text-left"><b>SL NO.</b></th>
						<th class="text-left"><b>ITEM DESCRIPTION</b></th>
                        <th><b>PRICE</b></th>
                        <th><b>QNTY</b></th>
                        <th><b>TOTAL</b></th>
                    </tr>
                    <tr class="text-center">
                        <td>1</td>
						<td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>2</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>3</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>4</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>5</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                </table>
			</div>
		</div>
        <div class="row pt-4">
			<div class="col s12 m12 l4">
				<div class="to pb-5">
                    <p class="m-1 font-weight-bold" style="color:#000 !important;"><b>INVOICE TO:</b></p>
                    <label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Suhas Sawant</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Accounts Dept.</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Windchimes Communications</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Bandra East</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Mumbai-400051</label><br>
                </div>
			</div>
			<div class="col s12 m12 l4" id="payment">
				<a href="#" class="btn purple white-text btn-lg my-2" style="background-color:#dddee2">MAKE PAYMENT</a></br></br></br></br>
				<p id="ac-number"><b>A/C NUMBER:</b> 01992823738</p></br></br>
				<p id="bank-name"><b>BANK NAME:</b> ICICI Bank</p></br></br>
				<p id="branch"><b>BRANCH:</b> Bandra East</p></br></br>
				<p id="ifsc"><b>IFSC CODE:</b> ICCI202928</p></br></br>
			</div>
            <div class="col s12 m12 l4 text-right pr-0">
				<p class="font-weight-bold h6 text-right pr-3"><b>SUB TOTAL:</b> 1,250,000</p>
                <div class="pr-3 float-right text-right" id="gst">
					<p><b>+SGST(9%):</b> 112,500</p>
                    <p><b>+CGST(9%):</b> 112,500</p>
				</div>
                <div class="clearfix"></div>
				<div class="font-weight-bold float-right text-left pl-3 pt-2" id="total">
					<h6 class="h4"><b>TOTAL:</b> 1,475,000</h6>
				</div>
            </div>
       </div>
        
        <div class="footer pb-3">
            <p style="font-size:14px;"><b>TERMS & CONDITIONS:</b></p> 
			<p>Payment should be made within 30 days of the invoice date and either made via bank transfer or cheque</p>
        </div>
    </div>
	</body>
		</div>
		<!-- Fifth Template END -->
		
		<div id="sixth-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
		<style>
        #sixth-template p{
            font-size: 13px;
        }
		
		.text-left{
			text-align:left !important;
		}
		
        #head{
            height:145px;
        }
        #head-details{
            border-left: 3px black solid;
        }
        .comp1{
            width:92px;
            height:51px;
			font-size:13px !important;
        }
        .comp{
            width:92px;
        }
        .det-1{
            width:480px;
            background-color: #f0eeef;   
        }
        .det-2{
            width:384px;
            background-color: #f0eeef;
        }
        #six-tem-table{
            margin-top:0px ;
        }
        #six-tem-table td{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:91px;
        }
		
		#six-tem-table th{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:30px;
        }
		
        #six-tem-table td{
            padding:13px 5px;
        }
        
        #six-tem-table tr:nth-child(even){
            background-color: #f0eeef
        }
        #summary{
            background: url(1140X100.png) no-repeat center;
            background-size: cover;
			background-color: #b3b3b3 !important;
            width:790px;
            min-height:100px;
            max-height: 100px;
        }
        .footer{
            background-color: #f0eeef;
        }
		
		.bk-color-white{
			background-color:#fff !important;
		}
    </style>
</head>
<body>
    <div class="container" style="width:790px !important">
        <div class="row pt-4">
            <div class="col s12 m12 l4 d-flex" id="head">
                <div class="col s4 m4 l4 justify-content-start" style="padding: 60px 0px;">
                    <img class="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="80" alt="eazyinvoices-logo"></img>
                </div>
                <div class="col s8 m8 l8 pl-2 justify-content-end" id="head-details">
					<h6 style="margin-top:0px !important; color:#fff !important;" id="eoe_div" class="eoe_div temp_text"><b>E&OE </b></h6>			
					<h6 class="h3 temp_text"><b>INVOICE</b></h6>
                    <p class="temp_text"><i class="fa fa-map-marker mr-3" aria-hidden="true"></i>402, Capri Building,</p>
                    <p class="ml-4 temp_text">AK Marg, Near Bandra Court,</p>
                    <p class="ml-4 temp_text">Bandra East,</p>
                    <p class="ml-4 temp_text">Mumbai - 400051</p>
					<p class="pu_no ml-4 temp_text">P.O. NO.: 98980</p>
                    <p style="margin-left:5px;" class="temp_text pu_o_date pan-1">P.O. DATE: 15/03/2018</p>
                </div>
            </div>
            <div class="col s12 m12 l8 pr-0">
                <div class="text-right">
                <div class="col s3 m3 l3 text-center d-inline-block comp1">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <p class="temp_text">INVOICE DATE:</p>
                </div>
                <div class="col s2 m2 l3 text-center d-inline-block comp1">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p class="temp_text">INVOICE NO:</p>
                </div>
				<div class="col s3 m3 l3 text-center d-inline-block comp1 pan-1">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p class="temp_text">PAN:</p>
                </div>
                <div class="col s2 m2 l3 text-center d-inline-block comp1 gst_1">
                    <i class="fas fa-newspaper"></i>
                    <p class="temp_text">GSTIN:</p>
                </div>
                
                <div>
					<div class="col s3 m3 l3 text-center d-inline-block comp1">
						<span class="temp_text list-inline-item py-0 comp">13 March 2018</span>
					</div>
					<div class="col s2 m2 l3 text-center d-inline-block comp1">
						<span class="temp_text list-inline-item py-0 comp">INV001</span>
					</div>
					<div class="col s2 m2 l3 text-center d-inline-block comp1 pan-1">
						<span class="temp_text list-inline-item py-0 comp"> EDFRT3456T </span>
					</div>
					<div class="col s3 m3 l3 text-center d-inline-block comp1 gst_1">
						<span class="temp_text list-inline-item py-0 comp">27RFGTY3456TSZS</span>
					</div>
                </div>
                </div>
                <!--div class="text-right">
                <div class="clearfix"></div>
                <div class="text-center d-inline-block comp">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <p>PAN:</p>
                </div>
                <div class="text-center d-inline-block comp">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p>CIN:</p>
                </div>
                <div class="text-center d-inline-block comp">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p>Placed Supply:</p>
                </div>
                <div class="text-center d-inline-block comp">
                    <i class="fas fa-newspaper"></i>
                    <p>Terms of Supply:</p>
                </div>
                <div>
                    <ul class="list-inline text-center float-right det-2">
                        <li class="list-inline-item py-0 comp">2383928HHJ93</li>
                        <li class="list-inline-item py-0 comp">123932389</li>
                        <li class="list-inline-item py-0 comp">82827</li>
                        <li class="list-inline-item py-0 comp">Terms</li>
                    </ul>
                </div>
                </div-->
            </div>
        </div>
        <div class="row" style="margin-top:10%;">
        <table id="six-tem-table">
            <thead>
				<tr>
					<th class="text-left py-2 pl-5 pr-5 temp_text">ITEM DESCRIPTION</th>
					<th class="py-2 pr-5 temp_text">UNIT PRICE</th>
					<th class="py-2 pr-5 temp_text">QNTY</th>
					<th class="py-2 pr-5 temp_text">DISCOUNT</th>
					<th class="py-2 pr-5 temp_text">TOTAL</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-left pl-5"><p class="item-name h6 mb-0 temp_text"><b>Logo Design</b></p><p class="item-desc temp_text">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit.</p></td>
					<td class="temp_text">&#x20b9; 300</td>
					<td class="temp_text">1</td>
					<td class="temp_text">1000</td>
					<td class="temp_text pr-5">&#x20b9; 300</td>
				</tr>
				<tr class="bk-color-white">
					<td class="bk-color-white text-left pl-5"><p class="temp_text item-name h6 mb-0"><b>Logo Design</b></p><p class="temp_text item-desc">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit.</p></td>
					<td class="temp_text bk-color-white">&#x20b9; 300</td>
					<td class="temp_text bk-color-white">1</td>
					<td class="temp_text">1000</td>
					<td class="bk-color-white temp_text pr-5">&#x20b9; 300</td>
				</tr>
				<tr>
					<td class="text-left pl-5"><p class="temp_text item-name h6 mb-0"><b>Logo Design</b></p><p class="item-desc temp_text">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit.</p></td>
					<td class="temp_text">&#x20b9; 300</td>
					<td class="temp_text">1</td>
					<td class="temp_text">1000</td>
					<td class="temp_text pr-5">&#x20b9; 300</td>
				</tr>
				<tr class="bk-color-white">
					<td class="bk-color-white text-left pl-5"><p class="item-name h6 mb-0 temp_text"><b>Logo Design</b></p><p class="temp_text item-desc">Lorem ipsum dolor sit amet </br>consectetur adipisicing elit.</p></td>
					<td class="temp_text bk-color-white">&#x20b9; 300</td>
					<td class=" temp_text bk-color-white">1</td>
					<td class="temp_text">1000</td>
					<td class="temp_text bk-color-white pr-5">&#x20b9; 300</td>
				</tr>
			</tbody>
        </table>
        <div class="row d-flex m-0" id="summary" style="border-top:1px solid #000 !important; padding:10px 10px !important;">
            <div class="col s12 m12 l4">
                <p style="margin-bottom:10px;" class="temp_text h6 pl-5 float-left"><b>SUMMARY</b></p>
				<p style="background-color:#fff !important; margin:0px -110px 0px -22px !important; height:60px; padding:7px 32px;">
					<label class="temp_text" style="color:#000;"><b>TOTAL DUE:</b></label><label class="temp_text" style="color:#000;">&#x20b9; 7200.00</label></br>
					<label class="temp_text" style="color:#000;"><b>IN WORDS:</b></label><label class="temp_text" style="color:#000;"><b>&#x20b9; Seven Thousand Two Hundred Only</b></label></br>
				</p>
            </div>
            <div class="col s12 m12 l8 pr-5">
            <div class="col s12 m12 l4"></div>
			<div class="col s12 m12 l4">
                <div class="d-inline-block text-right">
                    <p class="h6 temp_text"><b>SUB TOTAL</b></p>
                    <p class="font-weight-bold temp_text"><b>+ SGST</b></p>
                    <p class="font-weight-bold temp_text"><b>+ CGST</b></p>
                    <p class="h5 pt-1 temp_text"><b>GRAND TOTAL</b></p>
                </div>
			</div>
            <div class="col s12 m12 l4 text-right">
				<p class="temp_text h6">&#x20b9; 6000</p>
                <p class="font-weight-bold temp_text">+ &#x20b9; 600</p>
                <p class="font-weight-bold temp_text">+ &#x20b9; 600</p>
                <p class="h5 pt-1 temp_text">&#x20b9; 7200</p>
            </div>
            </div>
        </div>
        </div>
        <div class="row mt-5" style="padding-left:35px; width:790px;">
                <div class="col s12 m12 l4" id="payment">
                    <div>
						<p class="temp_text cinno" style="padding: 10px 0;"><span class="font-weight-bold temp_text">CIN: </span> U64200MH2008PTC123456</p></br></br>
                        <p class="pan-1 temp_text" style="padding: 10px 0;"><span class="font-weight-bold temp_text">PAN NO.:</span> AAABB1122D</p></br></br>
                        <p class="gst_1 temp_text" style="padding: 10px 0;"><span class="font-weight-bold temp_text">GST: </span>AAABB1122DSZS</p></br></br>
					</div>
                </div>
				<div class="col s12 m12 l4" id="payment">
                        <div class="bank_info"><p class="temp_text" id="ac-number"><span class="font-weight-bold temp_text">PAYMENT METHOD: </span> Bank Transfer</p></br></br>
                        <p class="temp_text" id="bank-name"><span class="font-weight-bold">BANK NAME: </span> ICICI Bank</p></br></br>
                        <p class="temp_text" id="branch"><span class="font-weight-bold">A/C NUMBER: </span> 32333533222</p></br></br>
                        <p class="temp_text" id="ifsc"><span class="font-weight-bold">IFSC CODE: </span>ICCI202928</p></br></br>
                        </div>
						<a href="#" class="temp_text make-payment btn purple btn-lg mt-3" style="background-color:#f0eeef; color:#fff;">MAKE PAYMENT</a></br></br>
                </div>
                
                <div class="col s12 m12 l4 text-center pt-3">
                    <div class="sign float-right sign_box">
                    <p class="temp_text h4 mb-0 display-4">FOR COMPANY NAME</p></br>
                    <p class="temp_text h5">John Doe - Project Manager</p>
                    </div>
                </div>
        </div>
        <div class="row px-5 mt-3 pt-3 footer_box" style="height:70px;">
            <p class="footer_p cond font-weight-bold"><b>TERMS & CONDITIONS</b></p>
            <p class="footer_p cond">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto ipsa impedit fuga expedita omnis neque facilis reiciendis quia sit esse, doloribus magni quaerat fugiat ducimus molestiae, modi inventore harum maxime! Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut esse, doloribus cumque minima impedit culpa provident magni maxime accusantium.</p>
        </div>
		<div class="row">
            <div class="col s7 m7 17 add cond">
				<p style="color:#fff !important; font-size:13px; padding-left:10px;" class="pl-2 temp_text">COMAPNY NAME</p>
				<p class="temp_text addr" style="color:#fff; padding-left:10px;">P402, Capri Building, A.K. Marg, </br>Near Bandra Court, <br> Bandra East, Mumbai - 400051. </p>
				<p style="color:#fff !important; font-size:13px; padding-left:10px;" class="temp_text telno">TEL. NO.: 022-25778 878</p>
				<p style="color:#fff !important; font-size:13px; padding-left:10px;" class="temp_text email_id">EMAIL ID: emailid.@email.com</p>	
			</div>
			<div class="col s5 m5 15" style="padding-top:8px;">
				<label class="facebk term footer_p"><b>FACEBOOK&nbsp&nbsp: </b></label><label class="facebk term footer_p">www.facebook.com/pp?1</label></br>    
				<label class="webst term footer_p"><b>WEBSITE&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label class="webst term footer_p">www.website.com</label></br>
				<label class="twitter term footer_p"><b>TWITTER&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label class="twitter term footer_p">www.twitter.com/pp?1</label></br>
				<label class="term link_id footer_p"><b>LINKEDIN&nbsp&nbsp&nbsp&nbsp: </b></label><label class="term link_id footer_p">www.linkedin.com/pp?123</label>
			</div>
        </div>
    </div>
</body>
		</div>
		<!-- END -->
	</div>
	
	</div>
	
	<script type="text/javascript">
		//After click on template full page will display
		$("#layout_showbtn").click(function(){
			$("#template-gallary").show();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(0);
		});
		
		
		$(".default").click(function(){
			$("#template-gallary").hide();
			$("#default-template").show();
			$("#first-template").hide();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(0);
		});
		
		$(".first_template").click(function(){
			$("#template-gallary").hide();
			$("#default-template").hide();
			$("#first-template").show();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(1);
		});
		
		$(".second_template").click(function(){
			$("#template-gallary").hide();
			$("#second-template").show();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(2);
		});
		
		$(".third_template").click(function(){
			$("#template-gallary").hide();
			$("#third-template").show();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(3);
		});
		
		$(".fourth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").show();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(4);
		});
		
		$(".fifth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").show();
			$("#sixth-template").hide();
			$("#template_val").val(5);
		});
		
		$(".sixth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").show();
			$("#template_val").val(6);
		});
		
		$(".close-temp").click(function(){
			$("#template-gallary").show();
			$("#default-template").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			
		});
	
		$("#custom-foot").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="3"){
				$("#foot_tab").click();
			}
		});
		
		$("#inv_custmise_fields, #est_custmise_fields, #pro_custmise_fields, #cust_fields").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="2"){
				$("#cust_tab").click();
			}
		});
		
		$("#custom-all, #est_customise_btn").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="1"){
				$("#cust_all").click();
			}
		});
		
		$("#est_ftr_customise").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="3"){
				$("#foot_tab").click();
			}
		});
	</script>
	
	<script>		
	$(document).ready(function($) {
	
	$("#font_size").change(function() {
		var f_size = $("#font_size").val();
		$('.temp_text').css("font-size", f_size);	
	});	
	
	$("#ftr_font_size").change(function() {
		var footer_size = $("#ftr_font_size").val();
		$('.footer_p').css("font-size", footer_size);	
	});
	
	$(".inv_inp").hide();
	$("#set_invoice_no").change(function() {
		if(this.checked) {
			$(".inv_inp").show();
		}else{
			$(".inv_inp").hide();
		}
	});
	
	//Logo Image hide and show
	$("#customize_logo_img").hide();
	$(".customize_logo_img").hide();
	$("#hdr_logo").change(function() {
		if(this.checked) {
			$("#customize_logo_img").show().focus();
			$("#customize_logo_img").css("border", "1px solid #ffcc66");
			$(".customize_logo_img").show().focus();
			$(".customize_logo_img").css("border", "1px solid #ffcc66");
		}else{
			$("#customize_logo_img").hide();
			$(".customize_logo_img").hide();
		}
	});
	
	$("#eoe_div").hide();
	$(".eoe_div").hide();
	$("#eoe").change(function(){
		if(this.checked) {
			$("#eoe_div").show().focus();
			$("#eoe_div").css("border", "1px solid #ffcc66");
			$(".eoe_div").show();
			$(".eoe_div").css("border", "1px solid #ffcc66");
		}else{
			$("#eoe_div").hide();
			$(".eoe_div").hide();
		}
	});
	
	//Bank Deatils Hide and show
	$(".bank_info").hide();
	$("#bank_de").hide();
	$("#bank_de1").hide();
	$("#bank_details").change(function() {
		if(this.checked) {
			$(".bank_info").show().focus();
			$(".bank_info").css("border", "1px solid #ffcc66");
			$("#bank_de").show();
			$("#bank_de1").show();
			$("#bank_de").css("border", "1px solid #ffcc66");
			$("#bank_de1").css("border", "1px solid #ffcc66");
		}else{
			$(".bank_info").hide();
			$("#bank_de1").hide();
			$("#bank_de").hide();
		}
	});
	
	//Make Payment button hide and show
	$("#make-payment").hide();
	$(".make-payment").hide();
	$("#payment_btn").change(function() {
		if(this.checked){
			$("#make-payment").show().focus();
			$("#make-payment").css("border", "2px solid #ffcc66");
			$(".make-payment").show().focus();
			$(".make-payment").css("border", "2px solid #ffcc66");
		}else{
			$("#make-payment").hide();
			$(".make-payment").hide();
		}
	});
	
	//Pan No Hide and show.
	$("#pan").hide();
	$(".pan-1").hide();
	$("#pan_no").change(function() {
		if(this.checked){
			$("#pan").show().focus();
			$("#pan").css("border", "1px solid #ffcc66");
			$(".pan-1").show().focus();
			$(".pan-1").css("border", "1px solid #ffcc66");
		}else{
			$(".pan-1").hide();
		}
	});
	
	//CIN field hide and show
	$(".cinno").hide();
	$("#cin_no").change(function() {
		if(this.checked){
			$(".cinno").show().focus();
			$(".cinno").css("border", "1px solid #ffcc66");
		}else{
			$(".cinno").hide();
		}
	});
	
	//Terms_Condition
	$("#cond").hide();
	$(".cond").hide();
	$("#terms_condition").change(function() {
		if(this.checked){
			$("#cond").show().focus();
			$("#cond").css("border", "1px solid #ffcc66");
			$(".cond").show().focus();
			$(".cond").css("border", "1px solid #ffcc66");
		}else{
			$("#cond").hide();
			$(".cond").hide();
		}
	});
	
	//GST
	$("#gst_no").hide();
	$(".gst_1").hide();
	$("#gst").change(function() {
		if(this.checked){
			$("#gst_no").show().focus();
			$("#gst_no").css("border", "1px solid #ffcc66");
			$(".gst_1").show().focus();
			$(".gst_1").css("border", "1px solid #ffcc66");
		}else{
			$("#gst_no").hide();
			$(".gst_1").hide();
		}
	});
	
	//Purchase Order date of issue
	$(".pu_o_date").hide();
	$("#purchase_order_date").change(function() {
		if(this.checked){
			$(".pu_o_date").show().focus();
			$(".pu_o_date").css("border", "1px solid #ffcc66");
		}else{
			$(".pu_o_date").hide();
		}
	});
	
	//Purchase Order date of issue
	$(".pu_no").hide();
	$("#purchase_order").change(function() {
		if(this.checked){
			$(".pu_no").show().focus();
			$(".pu_no").css("border", "1px solid #ffcc66");
		}else{
			$(".pu_no").hide();
		}
	});
	
	//Company Address
	$(".addr").hide();
	$("#company_address").change(function() {
		if(this.checked){
			$(".addr").show().focus();
			$(".addr").css("border", "1px solid #ffcc66");
		}else{
			$(".addr").hide();
		}
	});

	//Company EmailId
	$(".email_id").hide();
	$("#company_email").change(function() {
		if(this.checked){
			$(".email_id").show().focus();
			$(".email_id").css("border", "1px solid #ffcc66");
		}else{
			$(".email_id").hide();
		}
	});

	//Company Contact No
	$(".telno").hide();
	$("#company_contact").change(function() {
		if(this.checked){
			$(".telno").show().focus();
			$(".telno").css("border", "1px solid #ffcc66");
		}else{
			$(".telno").hide();
		}
	});

	//Company Website
	$(".webst").hide();
	$("#company_website").change(function() {
		if(this.checked){
			$(".webst").show().focus();
			$(".webst").css("border", "1px solid #ffcc66");
		}else{
			$(".webst").hide();
		}
	});

	//Company facebook
	$(".facebk").hide();
	$("#fb_link").change(function() {
		if(this.checked){
			$(".facebk").show().focus();
			$(".facebk").css("border", "1px solid #ffcc66");
		}else{
			$(".facebk").hide();
		}
	});
	
	//Company twitter
	$(".twitter").hide();
	$("#twitter_link").change(function() {
		if(this.checked){
			$(".twitter").show().focus();
			$(".twitter").css("border", "1px solid #ffcc66");
		}else{
			$(".twitter").hide();
		}
	});

	//Company twitter
	$(".link_id").hide();
	$("#linked_link").change(function() {
		if(this.checked){
			$(".link_id").show().focus();
			$(".link_id").css("border", "1px solid #ffcc66");
		}else{
			$(".link_id").hide();
		}
	});

	//Signature Box
	$("#sign_box").hide();
	$(".sign_box").hide();
	$("#signature").change(function() {
		if(this.checked){
			$("#sign_box").show().focus();
			$("#sign_box").css("border", "1px solid #ffcc66");
			$(".sign_box").show().focus();
			$(".sign_box").css("border", "1px solid #ffcc66");
		}else{
			$("#sign_box").hide();
			$(".sign_box").hide();
		}
	});
	
  $('.tab_content').hide();
  $('.tab_content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab_content').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });
  
  ////////////////////////////////// data fetching for customisation///////////////////////////////////////////////////////////
  var prefix=$('#prefix_sales').data('prefix');
   renderData(prefix);
  
 
  
  $('#layout_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	if($("#paper_size").is(":checked")){
	var paper_size=$("#paper_size").val();
    }
    if($("#letter").is(":checked")){
	var paper_size=$("#letter").val();
    }
	//var orientation=$("#orientation").val();
	var margins=$("#margins").val();
	var tmp_back_color=$("#tmp_back_color").val();
	var layout=$("#template_val").val();
	
	var checked=$("#set_for_all_tmp").is(":checked");

	if(checked){
		$("#set_for_all_tmp").val(1);
	}else{
		$("#set_for_all_tmp").val(0);
	}
	var set_for_all_tmp=$("#set_for_all_tmp").val();

	
	var checkedPref=$("#set_invoice_no").is(":checked");
	if(checkedPref){
		var set_inv=$("#inv_no_pre").val();
		var set_invoice_no=1;
	}else{
		var set_inv=pref;
		var set_invoice_no=0;
	}
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{

		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_template_customisation',
							data:{"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "set_invoice_no":set_invoice_no,
								  "inv_no_pre":set_inv,
								   "inv_pref":set_inv,
								  "paper_size":paper_size,
								//  "inv_orientation":orientation,
                                  "inv_margins":margins,
                                  "tmp_back_color":tmp_back_color,
                                  "inv_layout":layout,
                  				  "set_for_all_tmp":set_for_all_tmp

								  
							
							},
							cache: false,
			                success:function(result)
							{
								if(result['customisation'][0].set_invoice_no==1){
                                 $("#set_invoice_no").prop("checked",true);
                                 $("#inv_no_pre").val(result['customisation'][0].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'][0].inv_no_pre);
                                  //$("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                  if(result['customisation'][0].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
                                
                                 
								}else{
									 $("#set_invoice_no").prop("checked",false);
                                 $("#inv_no_pre").val(result['customisation'][0].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'][0].inv_no_pre);
                                  //$("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                  if(result['customisation'][0].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
								}

								if(result['customisation'][0].paper_size=='A4'){
                                 $("#paper_size").prop("checked",true);
								}
								if(result['customisation'][0].paper_size=='letter'){
                                 $("#letter").prop("checked",true);
								}							

								$("#margins").val(result['customisation'][0].inv_margins);
								$("#tmp_back_color").val(result['customisation'][0].tmp_back_color);
								$("#tmp_hex_color").val(result['customisation'][0].tmp_back_color);
								
                                if(result['customisation'][0].inv_layout==1){
                                 $("#first-template").show();
								 $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==2){
                                  $("#second-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==3){
                                  $("#third-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==4){
                                  $("#fourth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==5){
                                  $("#fifth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==6){
                                  $("#sixth-template").show();
								  $("#template-gallary").hide();
								}else{
                                  $("#default-template").show();
								  $("#template-gallary").hide();
								}

									


								if(result['customisation'][0].set_for_all_tmp==1){
                                 $("#set_for_all_tmp").prop("checked",true);
								}
								Materialize.toast('Custom options saved successfully', 2000,'red rounded');

							},
						});
	}
});

  $('#content_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	var hdr_logo_checked=$("#hdr_logo").is(":checked");
	if(hdr_logo_checked){
		$("#hdr_logo").val(1);
	}else{
		$("#hdr_logo").val(0);
	}
	var hdr_logo=$("#hdr_logo").val();
	var font_color=$("#font_color").val();
	var font_size=$("#font_size").val();
	/*var shipping_address_checked=$("#shipping_address").is(":checked");
	if(shipping_address_checked){
		$("#shipping_address").val(1);
	}else{
		$("#shipping_address").val(0);
	}
	var shipping_address=$("#shipping_address").val();*/
	/*var late_fees_checked=$("#late_fees").is(":checked");
	if(late_fees_checked){
		$("#late_fees").val(1);
	}else{
		$("#late_fees").val(0);
	}
	var late_fees=$("#late_fees").val();*/
	
	var bank_details_checked=$("#bank_details").is(":checked");
	if(bank_details_checked){
		$("#bank_details").val(1);
	}else{
		$("#bank_details").val(0);
	}
	var bank_details=$("#bank_details").val();
	var pan_no_checked=$("#pan_no").is(":checked");
	if(pan_no_checked){
		$("#pan_no").val(1);
	}else{
		$("#pan_no").val(0);
	}
	var pan_no=$("#pan_no").val();
	var gst_checked=$("#gst").is(":checked");
	if(gst_checked){
		$("#gst").val(1);
	}else{
		$("#gst").val(0);
	}
	var gst=$("#gst").val();
	var payment_btn_checked=$("#payment_btn").is(":checked");
	if(payment_btn_checked){
		$("#payment_btn").val(1);
	}else{
		$("#payment_btn").val(0);
	}
	var payment_btn=$("#payment_btn").val();
	var cin_no_checked=$("#cin_no").is(":checked");
	if(cin_no_checked){
		$("#cin_no").val(1);
	}else{
		$("#cin_no").val(0);
	}
	var cin_no=$("#cin_no").val();
	var purchase_order_date_checked=$("#purchase_order_date").is(":checked");
	if(purchase_order_date_checked){
		$("#purchase_order_date").val(1);
	}else{
		$("#purchase_order_date").val(0);
	}
	var purchase_order_date=$("#purchase_order_date").val();
	var purchase_order_checked=$("#purchase_order").is(":checked");
	if(purchase_order_checked){
		$("#purchase_order").val(1);
	}else{
		$("#purchase_order").val(0);
	}
	var purchase_order=$("#purchase_order").val();
	var terms_condition_checked=$("#terms_condition").is(":checked");
	if(terms_condition_checked){
		$("#terms_condition").val(1);
	}else{
		$("#terms_condition").val(0);
	}
     var terms_condition=$("#terms_condition").val();
	var signature_checked=$("#signature").is(":checked");
	if(signature_checked){
		$("#signature").val(1);
	}else{
		$("#signature").val(0);
	}
	var signature=$("#signature").val();
	var client_name=$("#client_name").val();
	var client_designation=$("#client_designation").val();
	
	
	var checked=$("#set_for_all_cont").is(":checked");

	if(checked){
		$("#set_for_all_cont").val(1);
	}else{
		$("#set_for_all_cont").val(0);
	}
	var set_for_all_cont=$("#set_for_all_cont").val();
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{
		
		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_content_customisation',
							data:{"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "hdr_logo":hdr_logo,
								  "font_color":font_color,
                                  "font_size":font_size,
                                 // "shipping_address":shipping_address,
                                 // "late_fees":late_fees,
                  				  "bank_details":bank_details,
                  				  "pan_no":pan_no,
                                  "gst":gst,
                                  "payment_btn":payment_btn,
                                  "cin_no":cin_no,
                  				  "purchase_order_date":purchase_order_date,
                  				  "purchase_order":purchase_order,
                  				  "terms_condition":terms_condition,
                                  "inv_signature":signature,
                                  "client_name":client_name,
                                  "client_designation":client_designation,
                  				  "set_for_all_cont":set_for_all_cont

								  
							
							},
							cache: false,
			                success:function(result)
							{
								
								if(result['customisation'][0].hdr_logo==1){
                                 $("#hdr_logo").prop("checked",true);
								}
								$("#font_color").val(result['customisation'][0].font_color);
								$("#hex_color").val(result['customisation'][0].font_color);
								$("#font_size").val(result['customisation'][0].font_size);
								

								/*if(result['customisation'][0].shipping_address==1){
                                 $("#shipping_address").prop("checked",true);
								}

								if(result['customisation'][0].late_fees==1){
                                 $("#late_fees").prop("checked",true);
								}*/
								if(result['customisation'][0].bank_details==1){
                                 $("#bank_details").prop("checked",true);

                                 $("#"+prefix+"_bank_details").show();

								}else{
									$("#bank_details").prop("checked",false);
									 $("#"+prefix+"_bank_details").hide();
								}

								if(result['customisation'][0].pan_no==1){
                                 $("#pan_no").prop("checked",true);
                                 $("#"+prefix+"_pan_no").show();
								}
								if(result['customisation'][0].gst==1){
                                 $("#gst").prop("checked",true);
                                 $("#"+prefix+"_gst").show();
								}
								if(result['customisation'][0].payment_btn==1){
                                 $("#payment_btn").prop("checked",true);
								}
								if(result['customisation'][0].cin_no==1){
                                 $("#cin_no").prop("checked",true);
                                 $("#"+prefix+"_cin_no").show();
								}
								if(result['customisation'][0].purchase_order_date==1){
                                 $("#purchase_order_date").prop("checked",true);
                                 $("#"+prefix+"_reference").show();
								}
								if(result['customisation'][0].purchase_order==1){
                                 $("#purchase_order").prop("checked",true);
                                 $("#"+prefix+"_reference_ref_date").show();
								}
								if(result['customisation'][0].terms_condition==1){
                                 $("#terms_condition").prop("checked",true);
                                  $("#"+prefix+"terms_condition").show();
								}
								if(result['customisation'][0].inv_signature==1){
                                 $("#signature").prop("checked",true);
                                  $("#"+prefix+"_signature_box").show();
                                  $("#"+prefix+"_sign_name").val(result['customisation'][0].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'][0].client_designation);
                                  
								}else{
                                   $("#signature").prop("checked",false);
                                  $("#"+prefix+"_signature_box").hide();
                                  $("#"+prefix+"_sign_name").val(result['customisation'][0].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'][0].client_designation);

								}

                                 $("#client_name").val(result['customisation'][0].client_name);							
						
                                 $("#client_designation").val(result['customisation'][0].client_designation);
								
                                 if(result['customisation'][0].set_for_all_cont==1){
                                 $("#set_for_all_cont").prop("checked",true);
								}


								Materialize.toast('Custom options saved successfully', 2000,'red rounded');



							},
						});
	}
});


 $('#footer_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	var eoe_checked=$("#eoe").is(":checked");
	if(eoe_checked){
		$("#eoe").val(1);
	}else{
		$("#eoe").val(0);
	}
	var eoe=$("#eoe").val();
	var ftr_font_color=$("#ftr_font_color").val();
	var ftr_font_size=$("#ftr_font_size").val();
	var ftr_back_color=$("#ftr_back_color").val();
	var company_address_checked=$("#company_address").is(":checked");
	if(company_address_checked){
		$("#company_address").val(1);
	}else{
		$("#company_address").val(0);
	}
	var company_address=$("#company_address").val();

	var company_email_checked=$("#company_email").is(":checked");
	if(company_email_checked){
		$("#company_email").val(1);
	}else{
		$("#company_email").val(0);
	}
	var company_email=$("#company_email").val();
	var company_contact_checked=$("#company_contact").is(":checked");
	if(company_contact_checked){
		$("#company_contact").val(1);
	}else{
		$("#company_contact").val(0);
	}
	var company_contact=$("#company_contact").val();

	var company_website_checked=$("#company_website").is(":checked");
	if(company_website_checked){
		$("#company_website").val(1);
	}else{
		$("#company_website").val(0);
	}
	var company_website=$("#company_website").val();
	var fb_link_checked=$("#fb_link").is(":checked");
	if(fb_link_checked){
		$("#fb_link").val(1);
	}else{
		$("#fb_link").val(0);
	}
	var fb_link=$("#fb_link").val();
	var twitter_link_checked=$("#twitter_link").is(":checked");
	if(twitter_link_checked){
		$("#twitter_link").val(1);
	}else{
		$("#twitter_link").val(0);
	}
	var twitter_link=$("#twitter_link").val();
	var linked_link_checked=$("#linked_link").is(":checked");
	if(linked_link_checked){
		$("#linked_link").val(1);
	}else{
		$("#linked_link").val(0);
	}
	var linked_link=$("#linked_link").val();
	
	var checked=$("#set_for_all_ftr").is(":checked");

	if(checked){
		$("#set_for_all_ftr").val(1);
	}else{
		$("#set_for_all_ftr").val(0);
	}
	var set_for_all_ftr=$("#set_for_all_ftr").val();
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{
		
		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_footer_customisation',
							data:{"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "eoe_ftr":eoe,
								  "ftr_font_color":ftr_font_color,
                                  "ftr_font_size":ftr_font_size,
                                  "ftr_back_color":ftr_back_color,
                                  "company_address":company_address,
                  				  "company_email":company_email,
                  				  "company_contact":company_contact,
                                  "company_website":company_website,
                                  "fb_link":fb_link,
                                  "twitter_link":twitter_link,
                  				  "linked_link":linked_link,
                  				  "set_for_all_ftr":set_for_all_ftr							  
							
							},
							cache: false,
			                success:function(result)
							{
								

								if(result['customisation'][0].eoe_ftr==1){
                                 $("#eoe").prop("checked",true);
								}
								$("#ftr_font_color").val(result['customisation'][0].ftr_font_color);
								$("#ftr_font_size").val(result['customisation'][0].ftr_font_size);
								$("#ftr_back_color").val(result['customisation'][0].ftr_back_color);

								if(result['customisation'][0].company_address==1){
                                 $("#company_address").prop("checked",true);
                                 $("#"+prefix+"_ftr_address").show();
								}
								if(result['customisation'][0].company_email==1){
                                 $("#company_email").prop("checked",true);
                                 $("#"+prefix+"_ftr_email").show();
								}

								if(result['customisation'][0].company_website==1){
                                 $("#company_website").prop("checked",true);
                                 $("#"+prefix+"_ftr_website").show();
								}

								if(result['customisation'].company_contact==1){
                                 $("#company_contact").prop("checked",true);

                                 $("#"+prefix+"_ftr_contact").show();
                                 
								}

								if(result['customisation'][0].fb_link==1){
                                 $("#fb_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_fb").show();
								}

								if(result['customisation'][0].twitter_link==1){
                                 $("#twitter_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_tw").show();
								}

								if(result['customisation'][0].linked_link==1){
                                 $("#linked_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_linkedin").show();
								}
								
                                 if(result['customisation'][0].set_for_all_ftr==1){
                                 $("#set_for_all_ftr").prop("checked",true);
								}


								Materialize.toast('Custom options saved successfully', 2000,'red rounded');

							},
						});
	}
});


$("#"+prefix+"_customer_id").on("change",function(){

	renderData(prefix);
});
  
 
  //////////////////////////////////// data fetching for customisation////////////////////////////////////////////////////////
 
});



function renderData(prefix){
	
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
     	var customer_id=$('#'+prefix+'_customer_id').val();


	$.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_customisation',
							data:{"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  
							},
							cache: false,
			                success:function(result)
							{
                               if(result['customisation'].set_invoice_no==1){
                                 $("#set_invoice_no").prop("checked",true);
                                 $("#inv_no_pre").val(result['customisation'].inv_no_pre);
                                
                                 // $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                 if(result['customisation'].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                   $("#inv_letter").val(result['customisation'].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 	$("#inv_letter").val(pref);
                                 }
                                
                                 
								}else{
									 $("#set_invoice_no").prop("checked",false);
                                 $("#inv_no_pre").val(result['customisation'].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'].inv_no_pre);
                                 if(result['customisation'].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
                                  
								}

								if(result['customisation'].paper_size=='A4'){
                                 $("#paper_size").prop("checked",true);
								}
								if(result['customisation'].paper_size=='letter'){
                                 $("#letter").prop("checked",true);
								}


                                if(result['customisation'].inv_layout==1){
                                 $("#first-template").show();
								 $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==2){
                                  $("#second-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==3){
                                  $("#third-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==4){
                                  $("#fourth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==5){
                                  $("#fifth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==6){
                                  $("#sixth-template").show();
								  $("#template-gallary").hide();
								}else{
                                  $("#default-template").show();
								  $("#template-gallary").hide();
								}


								
								$("#margins").val(result['customisation'].inv_margins);
								$("#tmp_back_color").val(result['customisation'].tmp_back_color);
								$("#tmp_hex_color").val(result['customisation'].tmp_back_color);
                                  

								

								

								if(result['customisation'].set_for_all_tmp==1){
                                 $("#set_for_all_tmp").prop("checked",true);
								}
                                 

								if(result['customisation'].hdr_logo==1){
                                 $("#hdr_logo").prop("checked",true);
								 $("#hdr_logo").trigger('change');
								}
								$("#font_color").val(result['customisation'].font_color);
								$("#hex_color").val(result['customisation'].font_color);
								$("#font_size").val(result['customisation'].font_size);
								

								
								if(result['customisation'].bank_details==1){
                                 $("#bank_details").prop("checked",true);
                                 $("#"+prefix+"_bank_details").show();
								 $("#bank_details").trigger('change');
								}

								if(result['customisation'].pan_no==1){
                                 $("#pan_no").prop("checked",true);
                                  $("#"+prefix+"_pan_no").show();
								  $("#pan_no").trigger('change');
								  
								}
								if(result['customisation'].gst==1){
                                 $("#gst").prop("checked",true);
                                 $("#"+prefix+"_gst").show();
								 $("#gst").trigger('change');
								}
								if(result['customisation'].payment_btn==1){
                                 $("#payment_btn").prop("checked",true);
								 $("#payment_btn").trigger('change');
								}
								if(result['customisation'].cin_no==1){
                                 $("#cin_no").prop("checked",true);
                                 $("#"+prefix+"_cin_no").show();
								 $("#cin_no").trigger('change');
								}
								if(result['customisation'].purchase_order_date==1){
                                 $("#purchase_order_date").prop("checked",true);
                                  $("#"+prefix+"_reference_ref_date").show();
                                  $("#purchase_order_date").trigger('change');
								}
								if(result['customisation'].purchase_order==1){
                                 $("#purchase_order").prop("checked",true);
                                  $("#"+prefix+"_reference").show();
								  $("#purchase_order").trigger('change');
								}
								if(result['customisation'].terms_condition==1){
                                 $("#terms_condition").prop("checked",true);
                                  $("#"+prefix+"terms_condition").show();
								  $("#terms_condition").trigger('change');
								}
								if(result['customisation'].inv_signature==1){
                                 $("#signature").prop("checked",true);
                                  $("#"+prefix+"_signature_box").show();
                                  $("#"+prefix+"_sign_name").val(result['customisation'].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'].client_designation);
								  $("#signature").trigger('change');
								}								
                                  $("#client_name").val(result['customisation'].client_name);							
						
                                 $("#client_designation").val(result['customisation'].client_designation);
								
                                 if(result['customisation'].set_for_all_cont==1){
                                 $("#set_for_all_cont").prop("checked",true);
								}

								if(result['customisation'].eoe_ftr==1){
                                 $("#eoe").prop("checked",true);
								 $("#eoe").trigger('change');
								}
								$("#ftr_font_color").val(result['customisation'].ftr_font_color);
								$("#ftr_font_size").val(result['customisation'].ftr_font_size);
								$("#ftr_back_color").val(result['customisation'].ftr_back_color);

								if(result['customisation'].company_address==1){
                                 $("#company_address").prop("checked",true);
                                 $("#"+prefix+"_ftr_address").show();
								 $("#company_address").trigger('change');
								}
								if(result['customisation'].company_email==1){
                                 $("#company_email").prop("checked",true);
                                 $("#"+prefix+"_ftr_email").show();
								 $("#company_email").trigger('change');
								}

								if(result['customisation'].company_website==1){
                                 $("#company_website").prop("checked",true);
                                 $("#"+prefix+"_ftr_website").show();
								 $("#company_website").trigger('change');
                                 
								}

								if(result['customisation'].company_contact==1){
                                 $("#company_contact").prop("checked",true);

                                 $("#"+prefix+"_ftr_contact").show();
                                 $("#company_contact").trigger('change');
								}

								if(result['customisation'].fb_link==1){
                                 $("#fb_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_fb").show();
								 $("#fb_link").trigger('change');
								}

								if(result['customisation'].twitter_link==1){
                                 $("#twitter_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_tw").show();
								 $("#twitter_link").trigger('change');
								}

								if(result['customisation'].linked_link==1){
                                 $("#linked_link").prop("checked",true);
                                  $("#"+prefix+"_ftr_linkedin").show();
								  $("#linked_link").trigger('change');
								}
								
                                 if(result['customisation'].set_for_all_ftr==1){
                                 $("#set_for_all_ftr").prop("checked",true);
								}


								//Materialize.toast('Your custom options saved successfully', 3000,'red rounded');

							},
						});
	
	
	
}

function changeColor(){
	
	
	  var hex=$('#hex_color').val();
	  $('#font_color').val(hex);
	  //document.getElementByClassName("temp_text").style.color = hex;
	  $('.temp_text').css("color", hex);
}

function changeColorftr(){
	
	
	  var hex=$('#ftr_hex_color').val();
	  $('#ftr_font_color').val(hex);
	  $('.footer_p').css("color", hex);
	  $('#address').css("color", hex);
}
function changeColorback(){
	
	
	  var hex=$('#back_hex_color').val();
	  $('#ftr_back_color').val(hex);
	  //to Set Background color.
	  $('#footer_box').css("background-color", hex);
	  $('.footer_box').css("background-color", hex);
}
var tmp_hex_color;
var defaultColor = "#ffffff";
function changeColortmp(){	
	  var hex=$('#tmp_hex_color').val();
	  //tmp_hex_color.value = defaultColor;
	  $('#tmp_back_color').val(hex);
	  $('#inv_tem').css("background-color", hex);
	  $('.inv_tem').css("background-color", hex);
	  //$('#templa').css("background-color", hex);
}


</script>