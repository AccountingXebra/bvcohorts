<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
			padding-left: 0px !important;
         }
      </style>
      
	  <style type="text/css" >
           
	  @media only screen and (min-width: 993px){
		.container {
			width: 95%;  
			margin: 10px 20px !important;
			opacity: 999999;
			z-index: 99999;
		}
        }
				.big-text{
					font-size: 20px;
				}
				.small-text{
					font-size: 12px;
				}   
				.medium-text {
					font-size: 12px;
				}
				
				.teal.lighten-2{
					background-color:#000 !important;
				}
				
				.breadcrumbs-title, .breadcrumbs, .breadcrumbs-title, .small-text, .medium-text{
					color:#fff !important;
				}
				
				#first-tem-table{
					width:100% !important;
				}
				
				#first-tem-table .lime{
					margin-bottom:10px;
					background-color: #b3b3b3 !important;
				}
				
				#first-tem-table.lime tr, th, td{
					border:none !important;
					font-size:12px !important;
				}
				
				#first-tem-table .unit-pr{
					background-color:#b3b3b3 !important;
				}
				
				#first-tem-table .pr-last{
					background-color:#4CAF50 !important;
				}
				
				#total-last{
					color:#000 !important;
					font-size:12px !important;
				}
				
				#first-template p{
					font-size:12px !important;
				}
				
				.term-c, .bank-transfer{
					font-size:12px !important;
				}
				
				.term-c span, .bank-transfer span{
					font-size:12px !important;
					color:#000 !important; 
				}
				
				.grand.card-panel{
					padding:12px 50px !important;
				}
				
				table.infotable {
					width: 100%;
				}

				body{
					font-family: 'Roboto', sans-serif !important;
				}
				
				.paybtn{
					color: #fff !important;
					padding: 8px;
					background: #4CAF50 !important;
					border-radius: 1px;
					/*border: 1px solid #7864e9;*/
				}
				
				p{
					font-size:12px !important;
				}
				
				.green {
					background-color: #4CAF50 !important;
				}
				
				.grand.card-panel {
					padding: 12px 12px !important;
				}
				</style>
			</head>
			<body class="container">
			<div class="card-panel teal lighten-2">
            <div class="row">
				<table class="infotable" id="top-label-table">
					<tr>
					<td colspan="2">
						<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                        if($custom_inv[0]->hdr_logo!=0){?>
							<img width="150" height="55" src="<?php //echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="company_logo"/>
						<?php } ?>
							
						<h5 style="margin-top:-10px;" class="breadcrumbs-title"><?php if($custom_inv[0]->company_address){?><label class="company-name"><?php echo strtoupper($company[0]->bus_company_name); ?><?php } ?></h5>
						<!--P class="breadcrumbs">GLOBAL SOCIAL MEDIA</P-->
					</td>
                    <td>
						<p style="margin-top:-10px !important;">
						<span class="breadcrumbs-title medium-text">PAN NO:</span>
                        <span class="breadcrumbs small-text"><?= $company[0]->bus_pancard; ?></span> <br/>
						<span class="breadcrumbs-title medium-text">GSTIN:</span>
                        <span class="breadcrumbs small-text"><?= $gstno[0]->gst_no; ?></span><br/>
                        <span class="breadcrumbs-title medium-text">CIN NO:</span>
                        <span class="breadcrumbs small-text"><?= $company[0]->bus_cin_no; ?></span>
						</p>
					</td>
					<td>
						<p style="margin-bottom:0px !important;" class="small-text"><?php if($custom_inv[0]->company_address){?><label class="dwn_ftr"><?php echo wordwrap($billing_doc[0]->inv_address,30,"<br>\n"); ?><?php } ?>
						</p>
						<label class="small-text">TEL. NO.&nbsp&nbsp&nbsp: </label><label class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
						<label class="small-text">EMAIL ID&nbsp&nbsp: </label><label class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
					</td>
					</tr>
					<?php
						$USD=$currencycode[0]->currencycode; 
						$sym=$currencycode[0]->symbol;
                    ?>
					<tr>	
						<td colspan="2">
							<h6 style="color:#fff; margin-bottom:-6px;" class="big-inv"><b>E&OE </b></h6>
							<p style="margin-top:-8px;"> <span class="medium-text"><b>INVOICE TO:</b></span> <br/>
							<span style="color:#fff !important;"><?=  @ucwords($billing_doc[0]->cust_name); ?>,</span><br/>
							<span class="small-text"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.' - '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,28,"<br>\n"); ?></span></p>
							<!--p class="medium-text">+91 98 191 847 21 <br/> mail@graphichuts.com</p-->
							<p class="medium-text" style="font-size:12px !important;">GSTIN: <?= @$gstno_cli[0]->gst_no; ?><br>PAN: <?= $cust_pan[0]->cust_pancard;?></br>PACE OF SUPPLY: <?=$place?></br>TERMS OF PAYMENT: <?=@$cust_pan[0]->cust_credit_period;?></p>
						</td>
						<td colspan="2">
							<?php $tax="true";
							if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
								$tax="false"; 
							}else{
								$tax="true";
							}

							$inv_document_type='invoice';
							$sub_tbl='';
							if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
								$inv_document_type="estimate-invoice";$sub_tbl='estl';
							}else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
								$inv_document_type="proforma-invoice";$sub_tbl='prol';
							}else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
								$inv_document_type="invoice";$sub_tbl='invl';
							}
							?>
							
							<div class="white-text" style="padding-left:0%; text-align:right;">
							<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
								<h4 class="breadcrumbs-title" >E S T I M A T E</h4>
							<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Inv";?>
								<h4 class="breadcrumbs-title" >P R O F O R M A &nbsp; I N V O I C E </h4>
							<?php }else { $inv="Sales Inv  ";?>
							<h4 class="breadcrumbs-title" >S A L E S &nbsp; I N V O I C E </h4>
							<?php } ?>
							</div>
							<div class="green white-text" style="padding-left:1%; height:75px;">
							<p style="float:left; margin-right:8px; padding-top:13.5px;"><span class="medium-text"><?=strtoupper($inv)?> </br> NO.</span> <br/> <span class="medium-text"><?=  @$billing_doc[0]->inv_invoice_no_view; ?></span></p>
							<p style="float:left; margin-right:8px;"><span class="medium-text"><?=strtoupper($inv)?></br> DATE</span> <br/> <span class="medium-text"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></span></p>
							<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<p style="float:left; margin-right:14px;"><span class="medium-text">ESTIMATE/ P.O.</br> NO:</span> <br/> <span class="medium-text"><?= @$billing_doc[0]->inv_po_no; ?></span></p><?php } ?>
							<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<p style="float:left; margin-right:5px;"><span class="medium-text">ESTIMATE/ P.O.</br>DATE:</span> <br/> <span class="medium-text"><?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_po_date))):''; ?></span></p>
							</div></br></br><?php } ?>
						</td>
					</tr>
				</table>
            </div>
			</div>
			<div>
			
			<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                      </style>
			
            <table id="first-tem-table" class="centered">
                <thead class="lime">
                    <tr>
                    <th style="width:50%; text-align: left;">ITEM DETAILS</th>
                    <th style="width:15%;">UNIT PRICE</th>
                    <th style="width:10%;">QTY</th>
					<th style="width:10%;">DISCOUNT</th>
                    <th style="width:15%;">PRICE</th>
                    </tr>
                </thead>
				<tbody style="background-color:#e6e6e6 !important;">
				<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
                <tr>
                    <td style="text-align: left;">
                        <p><b><?php //$invkey+1;
                              $temp=$sub_tbl.'_service_type'; ?>
                              <?php if($invlist->$temp==3){
								echo "Equalisation Levy";
                              }else if($invlist->$temp==4){
								echo "Late Fee";
                              }else if($invlist->$temp==2){
								echo "Expense Voucher";
                              }else{
                              foreach($my_services as $service){
								if($service->service_id==$invlist->service_id){
									echo $service->service_name;
                                }
                              }
                              }?></b><br/> 
						<span class=""><?php  
							$particulars=$sub_tbl.'_particulars'; 
                            $hsn=$sub_tbl.'_hsn_sac_no'; 
                            $quantity=$sub_tbl.'_quantity'; 
                            $rate=$sub_tbl.'_rate'; 
                            $discount=$sub_tbl.'_discount'; 
                            $taxable_amt=$sub_tbl.'_taxable_amt'; 
                            $igst=$sub_tbl.'_igst'; 
                            $igst_amt=$sub_tbl.'_igst_amt'; 
                            $cgst=$sub_tbl.'_cgst'; 
                            $cgst_amt=$sub_tbl.'_cgst_amt'; 
                            $sgst=$sub_tbl.'_sgst'; 
                            $sgst_amt=$sub_tbl.'_sgst_amt'; 
                            $cess=$sub_tbl.'_cess'; 
                            $cess_amt=$sub_tbl.'_cess_amt'; 
                            $other=$sub_tbl.'_other'; 
                            $other_amt=$sub_tbl.'_other_amt'; 
                            $amount=$sub_tbl.'_amount'; 
                            ?>
                            <?php echo wordwrap($invlist->$particulars,60,"<br>\n"); ?></span>
                    </td>
                    <td class="unit-pr"><?=round($invlist->$rate);  ?></td>
                    <td><?=$invlist->$quantity;  ?></td>
					<td><?=round($invlist->$discount);  ?></td>
					<td class="pr-last"> <?=$USD?> <? echo $invlist->$rate * $invlist->$quantity - $invlist->$discount ?></td>
                </tr>
				<?php } ?>
                <!--tr>
                    <td style="text-align: left;">
                        <p><b>Print Design</b> <br/> <span class="" >Web page design for all party/buyer of<br/>  corporate office.</span></p>
                    </td>
                    <td class="unit-pr"> 915 </td>
                    <td> 03 </td>
                    <td class="pr-last"> ₹ 20000.00 </td>
                </tr-->
				</tbody>
            </table>
			<table class="infotable" style="margin-top:-10px !important;">
				<tr>
					<td colspan="3"></td>
					<td>
						<div style="text-align:right;">
							<p><b>SUB TOTAL:</b><span><?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></span></p>
							<?php if($tax=="true") { ?>
							<p><b>+SGST(09%):</b> <span><?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></span></p> 
							<p><b>+CGST(09%):</b> <span><?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></span></p> 
							<?php } else { ?>
							<p><b>+IGST(18%):</b> <span><?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></span></p> 
							<?php } ?>
							<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
							<p><b>+CESS:</b> <span><?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?></span></p>
							<?php } ?>
							<?php if(round($billing_doc[0]->inv_other_total)>0){?>
							<p><b>+OTHERS:</b> <span><?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?></span></p>
							<?php } ?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="margin-top:-20px !important;">
							<button class="paybtn"><a class="btn btn-welcome mk_pay" href="<?=@$payment_url?>" style="text-decoration:none;">MAKE PAYMENT</a></button>
						</div>
					</td>
					<td>
						<div>
							<p class="grand card-panel green white-text" style="color:#fff !important; font-size: 16px; text-align:center;">GRAND TOTAL: <?=$USD?> <?=round($billing_doc[0]->inv_grant_total);  ?> </p>
							<p class="right" style="font-size: 18px"><b>IN WORDS: <?php echo strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)); ?></b></p>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<p class="bank-transfer"><b>BANK TRANSFER</b><br/>
                        <span class="grey-text">
                            BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?> <br/>
                            BRANCH: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?> <br/>
                            A/C NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?> <br/>
                            <?php if($USD=="INR"){?>
								IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?>
							<?php } else { ?>
								SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?>
							<?php } ?>
                        </span>
						</p>
					</td>
					<td>
						<div style="text-align:right;">
							<?php if($custom_inv[0]->inv_signature){?>
							<label class="up_lbl" style="font-size:13px; color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label></br>
							<p class="txt_pnt" style="padding-top:20px;"><?//= @$billing_doc[0]->inv_signature; ?></p></br></br></br>
							<label style="font-size:13px; color:#000;" class="txt_pnt"><?= $billing_doc[0]->inv_sign_name; ?></label> <label></label> <label style="font-size:13px; color:#000;" class="txt_pnt"><?//=$billing_doc[0]->inv_sign_designation; ?></label>
							<?php } ?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="add">
						<div style="width:96%;">
							<label class="term terms-container">TERMS & CONDITIONS </br> <?= @$billing_doc[0]->inv_terms; ?></label>
						</div>
					</td>
					<td class="pan">
						<?php if($billing_doc[0]->inv_terms==""){?>
							<div style="width:99%;">
						<?php } else{?>
							<div style="width:99%; margin-top:-8%;">
						<?php } ?>
						<label class="term"><strong>FACEBOOK&nbsp:</strong></label><label class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label></br>    
						<label class="term"><strong>WEBSITE&nbsp;&nbsp;&nbsp;&nbsp&nbsp;: </strong> </label><label class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label></br>
						<label class="term"><strong>TWITTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> </label><label class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label></br>
						<label class="term"><strong>LINKEDIN&nbsp;&nbsp;&nbsp;&nbsp;: </strong></label><label class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
						</div>
					</td>
				</tr>
			</table>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
            </div-->
		</div>
	</body>