<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
      <title>Xebra</title>
	  <style TYPE="text/css" MEDIA="screen, print">
		@import url(<?php echo base_url(); ?>asset/materialize/css/materialize.css);
        /*@import url(<?php echo base_url(); ?>asset/css/style.css);*/
		 
		@media only screen and (min-width: 993px){
			.container {
				width: 80%;
			}
        }
	 </style>
     <style type="text/css">
        body{
			padding-left: 20px !important;
		}
		
		#main{
         padding-left: 10px !important;
        }
		 
		@media print {
			
		}
		 
		@page {
            size: auto; 
            margin: 0mm;  
        }
        #sixth-template p{
            font-size: 12px !important;
        }
		
		.text-left{
			text-align:left !important;
		}
		
        #head{
            height:145px;
        }
        #head-details{
            border-left: 3px black solid;
        }
        .comp1{
            width:92px;
            height:51px;
			font-size:12px !important;
        }
        .comp{
            width:92px;
        }
        li{
            margin:14px 0px !important;
        }
        .det-1{
            width:480px;
            background-color: #f0eeef;   
        }
        .det-2{
            width:384px;
            background-color: #f0eeef;
        }
        #six-tem-table{
            margin-top:0px ;
        }
        #six-tem-table td{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:91px;
        }
		
		#six-tem-table th{
            min-width:100px;
            text-align: center;
            line-height: 1.1em;
            height:30px;
        }
		
        #six-tem-table td{
            padding:13px 5px;
        }
        
        #six-tem-table tr:nth-child(even){
            background-color: #f0eeef
        }
        #summary{
            background: url(1140X100.png) no-repeat center;
            background-size: cover;
			background-color: #b3b3b3 !important;
            min-height:220px;
            max-height: 220px;
        }
        .footer{
            background-color: #f0eeef;
			line-height:15px !important;
        }
		
		.bk-color-white{
			background-color:#fff !important;
		}
    </style>
	
	<script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
		}
		$('.par-spa rightss cemterfd').html(USD+" ");

		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
			$('#to_words_inv').text('ZERO');
		}else{
			$('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
		});
 </script>
</head>
<body>
    <div class="container" style="margin:5% 0 0 5%;">
		<?php
			$USD=$currencycode[0]->currencycode; 
		?>
        <div class="row pt-4">
            <div class="col l4 d-flex" id="head">
                <div class="col l4 justify-content-start" style="padding: 60px 0px;">
                    <?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                        if($custom_inv[0]->hdr_logo!=0){?>
							<img height="40" width="60" src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="comp-logo">
						<?php } ?>
                </div>
                <div class="col l8 pl-2 justify-content-end" id="head-details">
                    <?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
					<h5 class="black-text" style="margin-bottom: 10px; margin-left: 8px;">ESTIMATE</h5>
					<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Inv";?>
					<h5 class="black-text" style="margin-bottom: 10px; margin-left: 8px;">PROFORMA INVOICE</h5>
					<?php }else { $inv="Sales Inv";?>
                    <h5 class="black-text" style="margin-bottom: 10px; margin-left: 8px;"><b>SALES INVOICE</b></h5>
					<?php } ?>
                    
                    <h6 style="margin-bottom:2px;" class="big-inv"><b>E&OE </b></h6>
					<p style="margin:-2px 0 0 0;" class=""><i class="fa fa-map-marker mr-3" aria-hidden="true"></i><?php echo wordwrap($billing_doc[0]->cust_name,25,"</br>\n"); ?>,</p>
                    <p style="margin:-2px 0 0 0;" class="ml-4"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.' '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,30,"</br>\n"); ?></p>
					<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
						<p style="font-size:13px; padding-left:5px;"><b>ESTIMATE</br>/P.O. NO:</b> <?=  @$billing_doc[0]->inv_po_no; ?></p>
					<?php } ?>
					<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
						<p style="font-size:13px; padding-left:5px;"><b>ESTIMATE</br>/P.O. DATE:</b> <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
					<?php } ?>
                    <p style="font-size:13px; padding-left:5px; margin-top:0px;"><b>PLACE OF SUPPLY:</b> <?=$place?></p>
					<p style="font-size:13px; padding-left:5px; margin-top:-8px;">TERMS OF PAYMENT: </label><label style="color:#000;"><?=@$cust_pan[0]->cust_credit_period;?></p>
                </div>
            </div>
            <div class="col l8 pr-0">
                <div class="text-right">
                <div class="col l3 text-center d-inline-block comp1">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <p><?=strtoupper($inv)?> </br>DATE:</p>
                </div>
                <div class="col l3 text-center d-inline-block comp1">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p><?=strtoupper($inv)?> </br>NO:</p>
                </div>
				<div class="col l3 text-center d-inline-block comp1">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                    <p>PAN</p>
                </div>
				<div class="col l3 text-center d-inline-block comp1">
                    <img height="14" width="22" src="<?php echo base_url(); ?>asset/images/gst.png" class="cmp-logo" alt="logo"/>
                    <p>GSTIN</p>
                </div>
				<!-- fa-calendar fa-newspaper-->
                <div style="margin-top:5px;">
					<div class="col l2 text-center d-inline-block comp1">
						</br><span class="list-inline-item py-0 comp"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></span>
					</div>
					<div class="col l3 text-center d-inline-block comp1">
						</br><span class="list-inline-item py-0 comp"><?=  @$billing_doc[0]->inv_invoice_no_view; ?></span>
					</div>
					<div class="col l3 text-center d-inline-block comp1">
						</br><span class="list-inline-item py-0 comp"><?= @$cust_pan[0]->cust_pancard; ?></span>
					</div>
					<div class="col l4 text-center d-inline-block comp1">
						</br><span class="list-inline-item py-0 comp"><?= @$gstno_cli[0]->gst_no; ?></span>
					</div>
                </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:15%;">
		<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                     
                      </style>
        <table id="six-tem-table">
            <thead>
				<tr>
					<th style="width:50%;" class="text-left py-2 pl-5">ITEM DESCRIPTION</th>
					<th style="width:10%;" class="py-2">QNTY</th>
					<th style="width:13%;" class="py-2">PRICE</th>
					<th style="width:13%;" class="py-2">DISCOUNT</th>
					<th style="width:14%;" class="py-2 pr-5">TOTAL</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
				<tr style="background-color:#ccc !important;">
					<td class="text-left pl-5"><p class="item-name h6 mb-0"><b>
					<?php //$invkey+1;
                        $temp=$sub_tbl.'_service_type'; ?>
                        <?php if($invlist->$temp==3){
							echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
							echo "Late Fee";
						}else if($invlist->$temp==2){
							echo "Expense Voucher";
                        }else{
							foreach($my_services as $service){
								if($service->service_id==$invlist->service_id){
									echo $service->service_name;
                                }
                            }
                        }?></b></p>
					<p class="item-desc">
						<?php  
                            $particulars=$sub_tbl.'_particulars'; 
                            $hsn=$sub_tbl.'_hsn_sac_no'; 
                            $quantity=$sub_tbl.'_quantity'; 
                            $rate=$sub_tbl.'_rate'; 
                            $discount=$sub_tbl.'_discount'; 
                            $taxable_amt=$sub_tbl.'_taxable_amt'; 
                            $igst=$sub_tbl.'_igst'; 
                            $igst_amt=$sub_tbl.'_igst_amt'; 
                            $cgst=$sub_tbl.'_cgst'; 
                            $cgst_amt=$sub_tbl.'_cgst_amt'; 
                            $sgst=$sub_tbl.'_sgst'; 
                            $sgst_amt=$sub_tbl.'_sgst_amt'; 
                            $cess=$sub_tbl.'_cess'; 
                            $cess_amt=$sub_tbl.'_cess_amt'; 
                            $other=$sub_tbl.'_other'; 
                            $other_amt=$sub_tbl.'_other_amt'; 
                            $amount=$sub_tbl.'_amount'; 
                            ?>
                            <?php echo wordwrap($invlist->$particulars,70,"<br>\n"); ?>
					</p></td>
					<td><?=$invlist->$quantity;  ?></td>
					<td><?=round($invlist->$rate);  ?></td>
					<td><?=$USD?> <?=round($invlist->$discount);  ?></td>
					<td class="pr-5"><?=$USD?> <? echo $invlist->$quantity * $invlist->$rate - $invlist->$discount ?></td>
				</tr>
				<?php } ?>
			</tbody>
        </table>
        <div class="row d-flex m-0" id="summary" style="border-top:1px solid #000 !important; padding:0px 10px !important;">
            <div class="col l4">
                <p style="margin-bottom:10px;" class="h6 pl-5 float-left"><b>SUMMARY</b></p>
				<p style="background-color:#fff !important; margin:0px -110px 0px -22px !important; height:130px; padding:15px 50px;">
					<label style="color:#000; font-size:14px;"><b>TOTAL DUE:</b></label><label style="color:#000; font-size:13px;"> <?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></label></br>
					<label style="color:#000; font-size:14px;"><b>IN WORDS:</b></label><label style="color:#000; font-size:13px;"><b>  <?=$USD?> <?php echo wordwrap(strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)),35,"<br>\n"); ?></b></label></br>
				</p>
            </div>
            <div class="col l8 pr-5" style="padding-left:75px;">
            <div class="col l4"></div>
			<div class="col l4">
                <div class="d-inline-block text-right">
                    <p class="h6"><b>SUB TOTAL</b></p>
					<?php if($tax=="true") { ?>
                    <p style="margin:2px 0;" class="font-weight-bold"><b>+ CGST</b></p>
                    <p style="margin:2px 0;" class="font-weight-bold"><b>+ SGST</b></p>
					<?php } else { ?>
					<p style="margin:2px 0;" class="font-weight-bold"><b>+ IGST</b></p>
					<?php } ?>
					<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
					<p style="margin:2px 0;" class="font-weight-bold"><b>+ CESS</b></p>
					<?php } ?>
					<?php if(round($billing_doc[0]->inv_other_total)>0){?>
					<p style="margin:2px 0;" class="font-weight-bold"><b>+ OTHER</b></p>
					<?php } ?>
                    <p style="margin:2px 0;" class="h5 pt-1"><b> GRAND TOTAL</b></p>
                </div>
			</div>
            <div class="col l4 text-right">
				<p class="h6"><?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></p>
				<?php if($tax=="true") { ?>
                <p style="margin:2px 0;" class="font-weight-bold">+ <?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
                <p style="margin:2px 0;" class="font-weight-bold">+ <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
				<?php } else { ?>
				<p style="margin:2px 0;" class="font-weight-bold">+ <?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
				<?php } ?>
				<?php if(round($billing_doc[0]->inv_cess_total)>0){?>
				<p style="margin:2px 0;" class="font-weight-bold">+ <?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?></p>
				<?php } ?>
				<?php if(round($billing_doc[0]->inv_other_total)>0){?>
				<p style="margin:2px 0;" class="font-weight-bold">+ <?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?></p>
				<?php } ?>
                <p style="margin:2px 0;" class="h5 pt-1"><?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></p>
            </div>
            </div>
        </div>
        </div>
        <div class="row mt-5" style="padding-left:0px; margin-top:-38px;">
                <div class="col l4">
					<p style="margin:2px 0;" id="bank-name"><span class="font-weight-bold">CIN: </span> <?= $company[0]->bus_cin_no; ?></p>
					<p style="margin:2px 0;" id="bank-name"><span class="font-weight-bold">PAN NO.: </span> <?= $company[0]->bus_pancard; ?></p>
					<p style="margin:2px 0;" id="bank-name"><span class="font-weight-bold">GST: </span> <?= $gstno[0]->gst_no; ?></p>
				</div>
				<div class="col l4" id="payment">
                        <p style="margin:-2px 0;" id="bank-name"><span class="font-weight-bold">BANK NAME: </span> <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
						<p style="margin:-2px 0;" id="branch-name"><span class="font-weight-bold">BRANCH NAME: </span> <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
                        <p style="margin:-2px 0;" id="branch"><span class="font-weight-bold">A/C NUMBER: </span> <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
                        <?php if($USD=="INR"){?>
							<p style="margin:-2px 0;" id="ifsc"><span class="font-weight-bold">IFSC CODE: </span><?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
						<?php } else { ?>
							<p style="margin:-2px 0;" id="swift"><span class="font-weight-bold">SWIFT CODE: </span><?= @$billing_doc[0]->inv_bank_swift_code;?></p>
						<?php } ?>
                        <!--a href="#" class="btn purple white-text btn-lg mt-3" style="background-color:#f0eeef">MAKE PAYMENT</a></br></br-->
                </div>
                
                <div class="col l4 text-center pt-3" style="float:right !important;">
                    <div id="sign" style="float:right !important; margin-top:2%;">
                    <?php if($custom_inv[0]->inv_signature){?>
					<div class="" style="text-align:right">
						<label class="up_lbl" style="color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
						<p class="txt_pnt" style="padding-top:20px;"><?//= @$billing_doc[0]->inv_signature; ?></p><br>
						<label class="txt_pnt" style="color:#000 !important;"><?= $billing_doc[0]->inv_sign_name; ?></label> <label class="txt_pnt" style="color:#000 !important;"><?//= $billing_doc[0]->inv_sign_designation; ?></label>
					</div>
				<?php } ?>
                    </div>
                </div>
        </div>
        <div class="row footer px-5 mt-3" style=" margin-top:1%;">
            <p class="medium-text"><b>TERMS & CONDITIONS</b></p>
            <p style="line-height:2px;"><label class="term"><?= @$billing_doc[0]->inv_terms; ?></label></p>
        </div>
		<div class="row" style="margin-top:-20px;">
			<div class="col l6">
				<p style="margin:0 0 0 0px;" class="small-text"><?php echo wordwrap($billing_doc[0]->inv_address,40,"<br>\n"); ?></P>
				<label style="font-size:13px; color:#000 !important;" class="small-text"><b>TEL. NO.&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
				<label style="font-size:13px; color:#000 !important;" class="small-text"><b>EMAIL ID&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
			</div>
			<div class="col l6">
				<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>FACEBOOK&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label>
				</div>
				<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>WEBSITE&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label>
				</div>
				<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>TWITTER&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label>
				</div>
					<label style="font-size:13px; color:#000 !important;" class="term"><b>LINKEDIN&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
			</div>
		</div>
    </div>
</body>
<script type="text/javascript">
	window.print();
</script>