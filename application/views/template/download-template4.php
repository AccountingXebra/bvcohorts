	<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">

      <title>Xebra</title>
      <!-- <meta name="msapplication-TileColor" content="#00bcd4"> -->
      <style type="text/css">
         #main{
         padding-left: 0px !important;
         }
      </style>
      <STYLE TYPE="text/css" >
           
	  @media only screen and (min-width: 993px){
		.container {
			width: 95%;  
			margin: 20px 35px !important;
			opacity: 999999;
			z-index: 99999;
		}
         }
				#fourth-template p{
					margin-bottom: 0px;
					font-size: 13px;
				}
				.address{
					line-height: 1em;
				}
				#invoice{
					background: url(ColourBand.png) no-repeat center;
					background-size: cover;
					font-size: 24px;
					letter-spacing: 0.4em;
					font-weight: 7px;
				}
				#logo{
					margin-bottom: 80px;
				}
				.mx-auto{
					width:100% !important;
				}
				.mx-auto th{
					height:20px;
					background-color: rgb(117, 117, 117);
					color: white;
					padding: 13px 10px;
					font-weight: 500;
					font-size: 0.8em;
				}
				.mx-auto td{
					padding:13px 10px;
					border-bottom: 1px solid black;
					font-weight: 500;
					font-size: 0.8em;
				}
				#total-cost{
					padding-right:15px;
				}
				#total-cost p{
					padding:13px 0px;
					border-bottom: 1px solid black;
				}
				#item-desc-head{
					background: url(ColourBand.png) no-repeat center;
					background-size:cover;
					width:300px;
				}
				#sign{
					width:100%;
					text-align:right !important;
					margin-top:15% !important;
				}
				
				footer{
					margin-top: 50px;
					border-bottom: 20px black solid;
				}
				.footer p{
					background-color: #dddee2;
					font-size: 13px;
					margin-bottom: 0px;
				}
				/*
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;;
				}*/
				
				.first-head{
					background-color:#dddee2 !important;
					color: #000 !important;
				}
				
				#item-desc-head{
					background-color:#ff6666 !important;
				}
				
				.bnk-de{
					font-size:13px !important;
				}
				
				.pr-3, .pr-5, .mb-0, .pb-2{
					font-size:13px !important;
				}
				
				#companydetails ,.inv_tail{
					text-align:right !important;
				}
				
				table.infotable {
					width: 100%;
				} 
			</style>
			<script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
		}
		$('.par-spa rightss cemterfd').html(USD+" ");

		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
			$('#to_words_inv').text('ZERO');
		}else{
			$('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
   });
 </script>
		</head>
		<body>
		<div class="container" style="width:90%;">
			<?php
				//$USD=$currencycode[0]->currencycode; 
            ?>
			<table class="infotable" id="top-label-table">
				<tbody>
					<tr>
						<td colspan="3">
							<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                          if($custom_inv[0]->hdr_logo!=0){
                         ?>
                        <!--img src="<?php //echo base_url(); ?>public/upload/company_logos/<?//= $company[0]->bus_id; ?>/<?//= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="logo">-->
						<img height="40" width="200" src="<?php echo base_url(); ?>public/images/xebra-logo.png" class="cmp-logo" alt="company_logo" style="margin-right:-41px !important;">
                     <?php } ?>
						</td>
						<td>
							<div id="companydetails" class="mb-5">
								<h5><?php echo strtoupper($company[0]->bus_company_name); ?></h5>
								<p class="txt_ftr"> <?php echo wordwrap($billing_doc[0]->inv_address,20,"<br>\n"); ?></p>	
								<p class="py-1"><i class="fa fa-phone" aria-hidden="true"></i><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></p>
								<p class="pb-1"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></p>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<p class="font-weight-bold">INVOICE TO,</p>
							<p class="h4 mb-0"><b><?=  @ucwords($billing_doc[0]->cust_name); ?>,</b></p>
							<p><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.',</BR> '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></p>
							<p>GSTIN: <?= @$gstno_cli[0]->gst_no; ?></p>
							<p>PAN: <?= @$cust_pan[0]->cust_pancard; ?></p>
							<p>PLACE OF SUPPLY: <?=$place?></p>
						</td>
						<td colspan="2">
							<div id="invoice" style="margin-left:45%;">
								<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
									<h5>ESTIMATE</h5>
								<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
									<h5>PROFORMA INVOICE</h5>
								<?php }else { $inv="Sales Invoice";?>
									<h5><b>SALES INVOICE<b/></h5>
								<?php } ?>
							</div>
							<div id="invoice-details" class="pl-3" style="margin-left:45%;">
							<div style="text-align:left !important; margin-left:25%;">
								<p><b><?=strtoupper($inv)?> NO.:</b></p>
								<p class="h3" style="font-size:18px !important;"><b> <?=  @$billing_doc[0]->inv_invoice_no_view; ?></b></p>
							</div>
							<div class="d-inline-block" style="text-align:left !important; margin-left:0%;">
								<p><b><?=strtoupper($inv)?> DATE:</b><span style="margin-left:5px;"></span> <?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></p>
								<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
								<p><b>ESTIMATE / P.O. NO:</b><span style="margin-left:10px;"></span> <?=  @$billing_doc[0]->inv_po_no; ?></p>
								<?php } ?>
								<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
								<p><b>ESTIMATE / P.O. DATE:</b><span style="margin-left:0px;"></span> <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
								<?php } ?>
								<p><b>GSTIN</b>: <?= @$gstno_cli[0]->gst_no; ?></p>
								<p><b>PAN</b>: <?= @$cust_pan[0]->cust_pancard; ?></p>
								<p><b>PLACE OF SUPPLY:</b> <?=$place?></p>
							</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		
		<div class="row">
		<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                      
                      </style>
        <div class="col s12 m12 l12">
        <table class="mx-auto">
            <tr class="bg-light text-center">
                <th class="text-left" id="item-desc-head">ITEM DESCRIPTION</th>
				<th class="text-dark" style="background-color:#dddee2 !important;">QUANTITY</th>
				<th style="background-color:#00b300 !important">UNIT PRICE</th>
				<th class="bg-light text-dark first-head" style="background-color:#dddee2">DISCOUNT</th>
                <th style="background-color:#0080ff !important;">TOTAL</th>
            </tr>
            <?php foreach ($billing_doc as $invkey => $invlist) {  ?>
            <tr class="text-center">
                <td class="text-left">
					<p class="txt_pnt"><b><?php //$invkey+1;
						$temp=$sub_tbl.'_service_type'; ?>
                        <?php if($invlist->$temp==3){
                        echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
                        echo "Late Fee";
                        }else if($invlist->$temp==2){
                        echo "Expense Voucher";
                        }else{
                        foreach($my_services as $service){
                        if($service->service_id==$invlist->service_id){
                        echo $service->service_name;
                        }
                        }
                        }?></b>
					</p>
					<?php  
					$particulars=$sub_tbl.'_particulars'; 
                    $hsn=$sub_tbl.'_hsn_sac_no'; 
                    $quantity=$sub_tbl.'_quantity'; 
                    $rate=$sub_tbl.'_rate'; 
                    $discount=$sub_tbl.'_discount'; 
                    $taxable_amt=$sub_tbl.'_taxable_amt'; 
                    $igst=$sub_tbl.'_igst'; 
                    $igst_amt=$sub_tbl.'_igst_amt'; 
                    $cgst=$sub_tbl.'_cgst'; 
                    $cgst_amt=$sub_tbl.'_cgst_amt'; 
                    $sgst=$sub_tbl.'_sgst'; 
                    $sgst_amt=$sub_tbl.'_sgst_amt'; 
                    $cess=$sub_tbl.'_cess'; 
                    $cess_amt=$sub_tbl.'_cess_amt'; 
					$other=$sub_tbl.'_other'; 
                    $other_amt=$sub_tbl.'_other_amt'; 
                    $amount=$sub_tbl.'_amount'; 
                    ?>
                    <p><?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?></p>
				</td>
				<td><?=$invlist->$quantity;  ?></td>
                <td>₹ <?=round($invlist->$rate);  ?></td>
				<td>₹ <?=round($invlist->$discount);  ?></td>
                <td>₹ <? echo $invlist->$rate * $invlist->$quantity - $invlist->$discount ?></td>
            </tr>
			<?php } ?>
        </table>
		</div>
		</div>
		<div class="row">
            <div class="col s12 m12 l6 pl-5" id="">
                
            </div>
        </div>
        <div class="row">
			<table class="infotable" id="top-label-table">
				<tbody>
					<tr>
						<td colspan="2">
							
						</td>
						<td colspan="2">
							<div id="total-cost" style="text-align:right !important;">
								<p class="pr-3"><span class="pr-5 text-left">SUB TOTAL:</span> <?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?></p>
                <?php if($tax=="true") { ?>
				<p class="pr-3"><span class="pr-5">CGST(9%)</span><?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
				<p class="pr-3"><span class="pr-5">SGST(9%)</span><?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
				<?php } else { ?>
				<p class="pr-3"><span class="pr-5">IGST(18%)</span><?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
				<?php } ?>
                <p class="pr-3"><span class="pr-5">GRAND TOTAL:</span><?=$USD?> <?=round($billing_doc[0]->inv_grant_total);  ?></p>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="pl-5" id="payment" style="text-align:left !important;">
								<p class="bnk-de" id="bank-name">BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
                <p class="bnk-de" id="ac-number">A/C NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
                <p class="bnk-de" id="branch">BRANCH: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
                <p class="bnk-de" id="ifsc">IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
				<p class="bnk-de" id="ifsc">SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
							</div>
						</td>
						<td colspan="2">
							<div id="sign" style="text-align:right !important;">
								<div class="" style="text-align:right">
						<label class="up_lbl" style="color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
						<p class="txt_pnt" style="padding-top:25px;"><?= @$billing_doc[0]->inv_signature; ?></p><br>
						<label style="color:#000 !important;" class="txt_pnt"><?= $billing_doc[0]->inv_sign_name; ?></label> <label style="color:#000 !important;" class="txt_pnt"><?= $billing_doc[0]->inv_sign_designation; ?></label>
					</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
        </div>
        <div class="footer">
            <p>TERMS & CONDITIONS:</p> 
			<p><?= @$billing_doc[0]->inv_terms; ?></p>
        </div>
        <footer class="container mt-0">
            <!--<div class="row py-4 bg-secondary">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>-->
        </footer>
		</div>
		</body>
		</div