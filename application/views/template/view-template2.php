<?php $this->load->view('template/header'); ?>
	<style>
				.white-text{
					color:#fff !important;
				}
				
				.small-text{
					color:#000 !important;
					font-size: 13px;
				}   
				.medium-text {
					color:#000 !important;
					font-size: 15px;
				}
				.top-hr{
					margin: 15px 0px 0px -100px;
					height: 9px;
					border:none !important;
					background: -webkit-gradient(linear, 0 0, 100% 0, from(#f8eafa), to(#9c27b0), color-stop(100%, #9c27b0));
				}
				
				.purple{
					font-size:13px !important;
				}
				
				#second-template p{
					font-size:13px;
					color:#000 !important;
				}
				
				.cust-name{
					font-size:15px;
				}
				
				.cust-details{
					font-size:13px !important;
				}
				
				.inv-de{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#fff !important;
				}
				
				.inv-info{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#000 !important;
					font-size:13px !important;
				}
				
				#sec-table thead  th{
					background-color:#7979d2 !important; 
					padding:20px !important;
				}
				
				.purple{
					background-color:#7979d2 !important; 
				}
				
				.tot-inr{
					padding:20px !important;
				}
				
				.total-word{
					font-size:15px !important;
				}
			</style>
			<script type="text/javascript">
				$(document).ready(function() {
				//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
		   
				var USD='<?php print $currencycode[0]->currencycode; ?>';
			
				$('#tr_rate').html(USD);
				$('#tr_discount').html(USD);
				$('#tr_tax').html(USD);
				if(USD=="INR"){
					$('#to_words_currency').html("RUPEES ");
				}else{
					$('#to_words_currency').html(USD+".");
				}
				$('.par-spa rightss cemterfd').html(USD+" ");

				var final_grt=$('#tot_amount').text();
				var words=toWords(parseFloat(final_grt));
				if(words==''){
					$('#to_words_inv').text('ZERO');
				}else{
					$('#to_words_inv').text(words.toUpperCase()+' ONLY');
				}
		   });
		 </script>
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar'); ?>
        <section id="content">
			<div class="container" style="margin-top:2% !important;">
			<div class="plain-page-header">
               <div class=" pl-2">
                  <div class="row pl-2">
                     <div class="col l6 s12" style="margin-left:-14px !important;">
                        <a class="go-back underline" href="<?php echo base_url(); ?>sales/billing-documents">Back to My Billing Documents</a> 
                     </div>
                     <div class="col l6 s12 m6">
                        <span id="prefix_sales" data-prefix="inv"></span>
                     </div>
				 </div>
               </div>
            </div>
			<?php
				$USD=$currencycode[0]->currencycode; 
            ?>
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l4">
					<!--img src="<?php //echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo"-->
					<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                          if($custom_inv[0]->hdr_logo!=0){
                         ?>
                        <img height="40" width="200" src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="logo">
						<!--img height="40" width="200" src="<?php //echo base_url(); ?>public/images/xebra-logo.png" class="cmp-logo" alt="company_logo" style="margin-right:-41px !important;"-->
                     <?php } ?>
				</div>
				<div class="col l8"><hr class="top-hr"></hr></div>
			</div>	
			<div class="row">
            <div class="col l6" style="margin-left:10px;">
                <p style="margin-bottom:-20px !important; margin-top: 20px;">
                    <span class="purple white-text" style="padding-left: 2px;padding-right: 2px;">INVOICE</span><span class="to"> TO <span><br/>
                    <span class="cust-name"><b><?=  @ucwords($billing_doc[0]->cust_name); ?>,</b><span> <br/>
                    <!--span class="cust-com medium-text">Manager, Windchimes Communications Pvt. Ltd.</span-->
                </p></br>
                <br/>
                <p class="cust-details small-text">
                    <!--b>PHONE:</b> +91 9819184721, +91 2226470454   <br/>
                    <b>EMAIL</b> suhas@windchimes.co.in   <br/>
                    <b>WEBSITE</b> www.windchimes.co.in   <br/-->
                    <b>ADDRESS:</b> <?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.', '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?><br/>
                    <b>GSTIN:</b> <?= @$gstno_cli[0]->gst_no; ?><br/>
                    <b>PAN:</b> <?= @$cust_pan[0]->cust_pancard; ?><br/>
                    <b>PLACE OF SUPPLY:</b> <?=$place?>
                </p>
            </div>
            <div class="col l4 right">
                <?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
					<h6 class="black-text" style="margin-bottom: 10px; margin-left: 8px;">ESTIMATE</h6>
                <?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
					<h6 class="black-text" style="margin-bottom: 10px; margin-left: 8px;">PROFORMA INVOICE</h6>
                <?php }else { $inv="Sales Invoice";?>
                    <h6 class="black-text" style="margin-bottom: 10px; margin-left: 8px;"><b>SALES INVOICE<b/></h6>
                <?php } ?>
                <div class="col white-text">
					<p class="purple inv-de"><?=strtoupper($inv)?> NO.</p>
					<p class="purple inv-de"><?=strtoupper($inv)?> DATE</p>
					<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
                    <p class="purple inv-de">ESTIMATE / P.O. NO<p>
					<?php } ?>
					<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
						<p class="purple inv-de">ESTIMATE / P.O. DATE</p>
					<?php } ?>
					<p class="purple inv-de">TERMS OF PAYMENT:</p>
                </div>
                <div class="col">
                    <p>
                        <span class="inv-info">: <?=  @$billing_doc[0]->inv_invoice_no_view; ?></span> <br/><br/>
                        <span class="inv-info">: <?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></span><br/><br/>
						<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<span class="inv-info">: <?=  @$billing_doc[0]->inv_po_no; ?></span><br/><br/>
						<?php } ?>
						<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<span class="inv-info">: <?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></label></span></br></br>      
						<?php } ?>
						<span class="inv-info">: <?=@$cust_pan[0]->cust_credit_period;?></span> 
                    </p>
                    <!--p style="margin-top:15px;">
                        <span class="inv-info">: April 10, 2018</span>
                    </p-->
                </div>
                <div class="col"></div>
            </div>
			</div>
			<div class="row">
			<style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                     
                      </style>
            <table id="sec-table" class="centered striped">
                <thead class="white-text">
                    <tr>
                        <th style="text-align: left; padding-left:5%; ">ITEM NAME & DESCRIPTION</th>
                        <th>SAC</th>
                        <th>QUANTITY</th>
                        <th>RATE</th>
                        <th>DISCOUNT</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
				<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
                <tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">
						<?php //$invkey+1;
                        $temp=$sub_tbl.'_service_type'; ?>
                        <?php if($invlist->$temp==3){
							echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
							echo "Late Fee";
						}else if($invlist->$temp==2){
							echo "Expense Voucher";
                        }else{
							foreach($my_services as $service){
								if($service->service_id==$invlist->service_id){
									echo $service->service_name;
                                }
                            }
                        }?> </span><br/>
                        <span class="small-text">
							<?php  
                            $particulars=$sub_tbl.'_particulars'; 
                            $hsn=$sub_tbl.'_hsn_sac_no'; 
                            $quantity=$sub_tbl.'_quantity'; 
                            $rate=$sub_tbl.'_rate'; 
                            $discount=$sub_tbl.'_discount'; 
                            $taxable_amt=$sub_tbl.'_taxable_amt'; 
                            $igst=$sub_tbl.'_igst'; 
                            $igst_amt=$sub_tbl.'_igst_amt'; 
                            $cgst=$sub_tbl.'_cgst'; 
                            $cgst_amt=$sub_tbl.'_cgst_amt'; 
                            $sgst=$sub_tbl.'_sgst'; 
                            $sgst_amt=$sub_tbl.'_sgst_amt'; 
                            $cess=$sub_tbl.'_cess'; 
                            $cess_amt=$sub_tbl.'_cess_amt'; 
                            $other=$sub_tbl.'_other'; 
                            $other_amt=$sub_tbl.'_other_amt'; 
                            $amount=$sub_tbl.'_amount'; 
                            ?>
                            <?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?>
						</span>
                        </p>
                    </td>
                    <td><?=$invlist->$hsn;  ?></td>
                    <td><?=$invlist->$quantity;  ?></td>
                    <td><?=$USD?> <?=round($invlist->$rate);  ?></td>
                    <td><?=round($invlist->$discount);  ?></td>
                    <td><?=$USD?> <? echo $invlist->$quantity * $invlist->$rate - $invlist->$discount ?></td>
                </tr>
				<?php } ?>
                <!--tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">Typography</span> Idea <br/>
                            <span class="small-text">This is a sample letter that has been placed <br/> 
                                to demonstrate the typing format</span>
                        </p>
                    </td>
                    <td>658</td>
                    <td>4</td>
                    <td>INR 125.00</td>
                    <td>30%</td>
                    <td>INR 500</td>
                </tr-->
            </table>
            <hr class="purple" style="height: 2px;"/>
			</div>
			<div class="row">
            <div class="col l2 right" style="margin-left: 5px; margin-top:10px;">
                    <span style="color:#7979d2 !important;"><?=$USD?> <?=round($billing_doc[0]->inv_taxable_total);  ?> </span><br/>
                    <p class="medium-text">
                        <?php if($tax=="true") { ?>
						<?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?> <br/> <?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?> <br/>
						<?php } else { ?>
						<?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?> <br/>
						<?php } ?>
                        <?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?> <br/> <?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?> <br/>
                    </p>
                </div>
                <div class="col l2 right" style="text-align: right; margin-right: 5px; margin-top:10px;">
                    <span style="color:#7979d2 !important; ">SUB TOTAL</span> <br/>
                    <p class="medium-text">
                        <?php if($tax=="true") { ?>+SGST(09%) <br/> +CGST(09%) <br/> 
						<?php } else { ?>
						+IGST(18%)
						<?php } ?>
                        +CESS <br/> +OTHERS <br/>
                    </P>
                </div>
			</div>
			<div class="row">
				<div class="col right" style="margin-right: 45px; margin-top: -25px; padding:10px;">
                    <p class="right tot-inr card-panel purple white-text" style="color: #fff !important;">TOTAL DUE: <span class="white-text tot-inr" style="margin-left: 25px; color:#fff !important;"><?=$USD?> <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></span></p> <br/>
					<P class="right total-word"><?php //echo strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)); ?></P>
                </div>
            </div>
			<div class="row">
                <div class="col l4">
                    <p><span class="purple white-text" style="padding: 8px; margin-bottom:10px;"> PAYMENT </span> </br></br>
                        <span class="small-text grey-text" style="margin-top:10px !important; font-size:13px !important;">
                            <b>BANK NAME:</b> <?= ucwords(@$billing_doc[0]->cbank_name); ?><br/>
							<b>A/C NUMBER:</b> <?= @$billing_doc[0]->inv_bank_account_no; ?> <br/>
							<b>BRANCH NAME:</b> <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?> <br/>
                            <b>IFSC CODE:</b> <?= @$billing_doc[0]->inv_bank_ifcs_code; ?> </br>
							<b>SWIFT CODE:</b> <?= @$billing_doc[0]->inv_bank_swift_code; ?> </br>
                        </span>
                    </p>
                    <!--button class = "btn purple white-text" style="margin-top:10px; font-size: 20px;" type = "submit"> MAKE PAYMENT </button-->
                </div>
            <div class="col l4 right" style="margin-right: 70px;">
                <br/><br/>
                <?php if($custom_inv[0]->inv_signature){?>
					<div class="" style="text-align:right">
						<label class="up_lbl" style="color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
						<p class="txt_pnt" style="padding-top:20px;"><?= @$billing_doc[0]->inv_signature; ?></p><br>
						<label class="txt_pnt" style="color:#000 !important;"><?= $billing_doc[0]->inv_sign_name; ?></label> <label>-</label> <label class="txt_pnt" style="color:#000 !important;"><?= $billing_doc[0]->inv_sign_designation; ?></label>
					</div>
				<?php } ?>
                </div>
            </div>
			<div class="row" style="border-top:1px solid #000 !important; margin-top:20px;">
                <br/>
                <p class="medium-text"><b>TERMS & CONDITIONS</b></p>
                <p><label class="term"><?= @$billing_doc[0]->inv_terms; ?></label></p>
            </div>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
                <p class="col l4">
                    <span class="medium-text">
                        Address:
                    </span> <br/>
                    ABC Finance Private Limited. <br/>
                    110/112, B Wing, Kalpataru Enclave, S.V. Road,  <br/>
                    Santacruz - west    <br/>
                    TEL NO. :9769976482 <br/>
                    EMAIL ID :suhas@windchimes.co.in <br/>
                    
                </p>
            </div-->
    </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
</div>

<?php $this->load->view('template/footer'); ?>