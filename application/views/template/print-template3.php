<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <title>Xebra</title>
      <style type="text/css">
        #main{
         padding-left: 0px !important;
        }
		 
		@media print {
			.bg_color{
				background:#000 !important; 
			}
			/*footer {
				position: fixed;
				bottom: 0;
			}*/
		}
		 
		@page {
            size: auto; 
            margin: 0mm;  
        }
      </style>
      <STYLE TYPE="text/css" MEDIA="screen, print">
	  
         @import url(<?php echo base_url(); ?>asset/materialize/css/materialize.css);
         /*@import url(<?php echo base_url(); ?>asset/css/style.css);*/
         
		 @media only screen and (min-width: 993px){
			.container {
				width: 80%;
			}
         }
		 
		 /* NEW CSS BY WCID-366 */
		p.white-color{
			color:#fff;
		}
		
		label.white-color{
			color:#fff;
		}
		
		.big-w{
			font-size:13px;
			color:#000;
		}
		.small-w{
			font-size:12px;
			color:#000;
		}
		
		.small-sw{
			font-size:11px;
			color:#000;
		}
		
		.inv-title{
			font-size:18px !important;
		}
		
		.eoe_div{
			width:50px;
		}
		
		/* Table CSS */
		#default_1 td{
			border:none !important;
			border-top:1px solid #595959 !important;
		}
		
		#default_1 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_1 .amttable{
			text-align:center;
		}
		
		#default_2 td{
			border:none !important;
		}
		
		#default_2 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_2 .amttable{
			text-align:center;
		}
		
		#default_3 td{
			border:none !important;
			border-bottom:1px solid #ccc !important;
		}
		
		#default_3 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_3 .amttable{
			text-align:center;
		}
		
		.total_box{
			background-color:#000;
			color:#fff;
			border-radius:5px;
			height:auto !important;
			padding:15px 10px !important;
		}
		
		#inv_customisemodal footer {
			margin-top: 50px;
			border-bottom: 23px solid #000;
			padding:15px 10% 10px 16%;
		}
		
		.se_tem_tr{
			background-color:#000 !important;
			color:#fff;
			font-size:13px;
		}
		
		.ls_tem_tr{
			background-color:#8cd98c !important;
			color:#fff;
			font-size:13px;
		}
		
		.term-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#000 !important;
		}
		/*END*/
         tbody.info-tabletd td {
         padding: 5px 5px; 
         font-size: 12px;
         }
         body{
         font-family: "Roboto", sans-serif !important;
         font-size: 15px;
         }
         .tabs-class th {
			padding: 5px 10px;
			color:#000;
			border: 1px solid #595959;
         }
         .tabs-class td {
			padding: 5px 10px;
			border: 1px solid #595959;
         }
         .amttable {
			background: #0059b3;
         }
         .tabs-class {
         font-size: 12px;
         }
         p{
         margin: 3px 0;
         }
         .one-eaigh {
         text-align: right;
         }
         .three-eaigh {
         text-align: right; 
         }
         .zero {
         text-align: right;  
         }
         .yello-clr{
         background: #fffdd7;
         }
		 .inword {
			 font-size:14px !important;
			 text-align:left;
		 }
		 .paybtn{
		 	color: #fff;
		    padding: 5px 8%;
		    background: #0059b3;
		    border-radius: 5px;
		    border: 1px solid #0059b3;
		    margin: 6px -3px;
		 }
		.breakAfter {
			page-break-after: always
		}
		@media only screen and (min-width: 601px){
		 	.container {
    			width: 95%;
			}
		}
		@media print {
	        html, body {
	            width: 280mm;
	            height: 400mm;        
	        }
		
		 @page{
			 margin-left:-20px;
			 margin-right:-20px;
		 }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
	        }
	    }
        tbody.info-tabletd td {
             padding: 5px 5px;
             font-size: 12px;
         }
		/* Font-color & Font-size of content */
		.txt_pnt{
			text-align:center;
			color:black !important;
			font-size:13px !important;
			/*color : <?=$custom_inv[0]->font_color?>;
			font-size : <?=$custom_inv[0]->font_size?> !important; */
		}
		/* Font-color & font-size of Footer */
		.txt_ftr{
			color:black !important;
			font-size:13px !important;
			/*color : <?=$custom_inv[0]->ftr_font_color?> !important;
			font-size : <?=$custom_inv[0]->ftr_font_size?> !important; */
		}
		
		/* Margin to Template */
		#view-page{
			margin : 0in <?=$custom_inv[0]->inv_margins?>in <?=$custom_inv[0]->inv_margins?>in <?=$custom_inv[0]->inv_margins?>in;
		}
		
		#tab-table td, th {
			border: 1px solid #595959;
		}
		
		#tab-table .amttable{
		  text-align:center;
		}
		.up_lbl{
			font-size:13px;
			color:black !important;
		}
		
		hr.bottom-hr {
		  border-color: grey;
		  background: none;
		  padding: 4px 0 0 57px !important;
		  border-top: none;
		  border-left: none;
		  margin: 0 0 10px -15px;
		  border-right: none;
		  float: initial;
		  width: 99%;
		}
		
		
		.al-right{
			text-align:right;
		}
		
		.big-inv{
			color:#000;
			margin-top:0px !important;
		}
		
		.inv-to{
			font-size:15px;
			color:#000;
		}
		
		.mod-pay{
			color:#000;
			font-size:15px;
			line-height:35px;
		}
		
		.foot{
			background-color:#7864e9;
			margin:0% 4% 0 4%;
			padding:5px 4%;
			border:1px solid #000;
		}
		
		.foot_lbl{
			font-size:13px;
			color:#fff;
		}
		
		.xebra{
			color:#fff;
			margin:3% -2%;
		}
		
		.add{
			margin-top:10px;
		}
		
		.add label{
			font-size:13px;
			color:#fff;
		}
		
		.pan{
			margin:2.5% 0%;
		}
		
		.term{
			color:#000;
			
		}
      </STYLE>
      <script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
        }
		$('.par-spa rightss cemterfd').html(USD+" ");

      
		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
          $('#to_words_inv').text('ZERO');
		}else{
           $('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
		});
	   </script>
   </head>
   <body class="layout-semi-dark bg-theme-gray sales-invoice easy-tab1 my-easy-1" id="view-page" bgcolor="<?=$custom_inv[0]->tmp_back_color?>">
      <!-- START MAIN -->
      <div id="main page">
      <!-- START WRAPPER -->
      <div class="wrapper ">
      <!-- END LEFT SIDEBAR NAV-->
      <!-- START CONTENT -->
      <section id="content">
         <div class="container">
            <div class="plain-page-header">
               <div class=" pl-2">
                  <div class="col l12 s12">
                  </div>
                  <!--   <div class="col l12 s12 m12 del-gry">
                     <a href="#"><img src="<?php echo base_url(); ?>asset/css/img/icons/delete-grey.png"></a>
                     </div> -->
               </div>
            </div>
             <?php
				$USD=$currencycode[0]->currencycode; 
             ?>
            <div class="page-content pl-2">
               <div class="container">
					<div class="row" style="margin:45px 0 0 0; padding:0 0 10px 0;">
						<div class="col s4 m4 l4" style="text-align:left; padding:0px;">
							<?php $tax="true";
							if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
								$tax="false"; 
							}else{
								$tax="true";
							}

							$inv_document_type='invoice';
							$sub_tbl='';
							if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
								$inv_document_type="estimate-invoice";$sub_tbl='estl';
							}else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
								$inv_document_type="proforma-invoice";$sub_tbl='prol';
							}else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
								$inv_document_type="invoice";$sub_tbl='invl';
							}
							if($custom_inv[0]->hdr_logo!=0){?>
								<img src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="company_logo" height="55" width="150"/>
							<?php } ?>
						</div>
						<div class="col s8 m8 l8 bg_color" style="height:61px;">
						
						</div>
					</div>
					<div class="row" style="margin-top:-10px;">
						<div class="col s4 m4 l4">
							<div class="col s6 m6 l6" style="padding:6.5% 0; width:50%; border-bottom:3px solid #000;">
								<p class="big-w temp_text"><b><?//=strtoupper($inv)?>INVOICE NO: </b></p>
								<p style="padding-top:5px;" class="small-w temp_text"><?=  @$billing_doc[0]->inv_invoice_no_view; ?></p>
							</div>
							<div class="col s6 m6 l6" style="padding:6.5% 0; width:50%; border-bottom:3px solid #000;">
								<p class="big-w temp_text"><b>DATE OF ISSUE: </b></p>
								<p style="padding-top:5px;" class="small-w temp_text"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></p>
							</div>
						</div>
						<div class="col s8 m8 l8 bg_color" style="border-bottom:3px solid #000;">
							<div class="col s12 m12 l12 temp_text" style="text-align:center; padding:1.8% 0 4.5% 5px; color:#595959;">
								<?php  if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
								   <h5>ESTIMATE</h5>
								<?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Invoice";?>
								   <h5>PROFORMA INVOICE</h5>
								<?php }else { $inv="Sales Invoice";?>
								   <h5><b>SALES INVOICE</b></h5>
								<?php } ?>
							</div>
						</div>
				    </div>
					<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0;">
					<div class="col s4 m4 l4" style="">
						<div class="col s12 m12 l12" style="text-align:left; padding:10px 0;">
							<p class="small-w footer_p"><?php echo wordwrap($billing_doc[0]->inv_address,35,"<br>\n"); ?></p>
							<p class="small-w footer_p"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></p>
							<p class="small-w footer_p"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></p>
							<p style="color:#000;" class="small-w footer_p"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></p>
						</div>
					</div>
					<div class="col s4 m4 l4" style="padding:0 0;">
						<p class="big-w eoe_div temp_text"><b> E & OE </b></p>
						<p class="big-w temp_text"><b>BILLED TO:</b></p>
						<p class="big-w temp_text"><b><?=  @ucwords($billing_doc[0]->cust_name); ?>,</b></p>
						<p class="small-sw temp_text"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.', '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></p>
						<p class="small-sw temp_text"><?php $sstate = ($billing_doc[0]->sstate)?', '.$billing_doc[0]->sstate:'';?></p>
						<p class="small-sw temp_text"><?php $scountry = ($billing_doc[0]->scountry)?' - '.$billing_doc[0]->scountry:''; ?></p>
					</div>
					<div class="col s4 m4 l4" style="padding:0 0;">
						<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
							<p class="pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b></p>
							<p style="padding-top:5px;" class="pu_no small-w temp_text"><?=  @$billing_doc[0]->inv_po_no; ?></p>
						<?php } ?>
						</br>
						<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
							<p class="pu_o_date big-w temp_text"><b>ESTIMATE/P.O. DATE: </b></p>
							<p style="padding-top:5px;" class="pu_o_date small-w temp_text"><?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></p>
						<?php } ?>
					</div>
					</div>
                  <style>
					<?php 
						if(round($billing_doc[0]->inv_discount_total)==0){?>
						.zerodiscount{
                        display: none;
						}
                   <?php }?>
                   <?php 
						if(round($billing_doc[0]->inv_cgst_total)==0){?>
						.zerocgst{
                        display: none;
						}
				   <?php } ?>
                   <?php 
						if(round($billing_doc[0]->inv_sgst_total)==0){?>
						.zerosgst{
						display: none;
						}
				   <?php } ?>
                   <?php 
						if(round($billing_doc[0]->inv_igst_total)==0){?>
						.zeroigst{
						display: none;
						}
                   <?php } ?>
                   <?php 
						if(round($billing_doc[0]->inv_cess_total)==0){?>
						.zerocess{
							display: none;
						}
				   <?php } ?>
                   <?php 
						if(round($billing_doc[0]->inv_other_total)==0){?>
						.zeroother{
						display: none;
						}
				   <?php } ?>     
				</style>
                <div class="">
					 <div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 10px 0px 10px !important;">
						<div class="row">
						<table id="default_1" class="">
                           <thead>
                              <tr class="" id="">
                                 <th class="" style="width:242px !important; padding-left:6px !important; background-color:#7864e9;">
                                    <p class="big-w temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">SAC</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">QNTY</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text" style="">RATE</br>(<?=$USD?>)</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text" style="">TAXABLE</br>(<?=$USD?>) </p> 
                                 </th>
                                 <!--<?php //if($tax=="true") { ?>
								 <th class="amttable">
                                    <p class="big-w temp_text">CGST</br>(<?//=$USD?>)(%)</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">SGST</br>(<?//=$USD?>)(%)</p>
                                 </th>
								 <?php //} else { ?>
								 <th class="amttable">
                                    <p class="big-w temp_text">IGST</br>(<?//=$USD?>)(%)</p>
                                 </th>
								 <?php //} ?>-->
                                 <th class="amttable">
                                    <p class="big-w temp_text">AMOUNT</br>(<?=$USD?>)</p>
                                 </th>
                              </tr>
							</thead>
							<tbody>
								<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
								<tr style="background-color:#eef2fe;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"><b><?php //$invkey+1;
										$temp=$sub_tbl.'_service_type'; ?>
										<?php if($invlist->$temp==3){
                                          echo "Equalisation Levy";
										}else if($invlist->$temp==4){
                                           echo "Late Fee";
										}else if($invlist->$temp==2){
                                           echo "Expense Voucher";
										}else{
										foreach($my_services as $service){
											if($service->service_id==$invlist->service_id){
												echo $service->service_name;
											}
										}
										}?></b></p>
										<?php  
										$particulars=$sub_tbl.'_particulars'; 
										$hsn=$sub_tbl.'_hsn_sac_no'; 
										$quantity=$sub_tbl.'_quantity'; 
										$rate=$sub_tbl.'_rate'; 
										$discount=$sub_tbl.'_discount'; 
										$taxable_amt=$sub_tbl.'_taxable_amt'; 
										$igst=$sub_tbl.'_igst'; 
										$igst_amt=$sub_tbl.'_igst_amt'; 
										$cgst=$sub_tbl.'_cgst'; 
										$cgst_amt=$sub_tbl.'_cgst_amt'; 
										$sgst=$sub_tbl.'_sgst'; 
										$sgst_amt=$sub_tbl.'_sgst_amt'; 
										$cess=$sub_tbl.'_cess'; 
										$cess_amt=$sub_tbl.'_cess_amt'; 
										$other=$sub_tbl.'_other'; 
										$other_amt=$sub_tbl.'_other_amt'; 
										$amount=$sub_tbl.'_amount'; 
										?>
										<p class="small-w temp_text"><?php echo wordwrap($invlist->$particulars,50,"<br>\n"); ?></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"><?=$invlist->$hsn;  ?></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"><?=$invlist->$quantity;  ?></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"><?=round($invlist->$rate);  ?></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"><?=round($invlist->$taxable_amt);  ?></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"><?=round($invlist->$amount); ?></p>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
                    </div>
					<div class="row" style="padding-top:3.5%; border-top:1px solid #595959; margin:0 -35px;">
						<div class="col s12 m12 l12">
							<div class="col s7 m7 l7">
								<div style="margin-bottom:10px;">
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>TERMS OF PAYMENT:</b> <label class="small-w temp_text"><?=@$cust_pan[0]->cust_credit_period;?></label></p>
								</div>
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY:</b> <label class="small-w temp_text"><?=$place?></label></p>
								</div>
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN:</b> <label class="small-w temp_text"><?= @$gstno_cli[0]->gst_no; ?></label></p>
								</div>
							</div>
							<div class="col s5 m5 l5">
								<div class="col s12 m12 l12">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>SUBTOTAL</b></p><p style="font-size:15px; float:right; text-align:right;" class="big-w temp_text"><?=$USD?>&nbsp; <?=round($billing_doc[0]->inv_grant_total);  ?></p>
								</div>
								<hr>
								<?php if($tax=="true") { ?>
								 <div class="col s12 m12 l12">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>CGST (9%)</b></p><p style="font-size:15px; float:right; text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_cgst_total);  ?></p>
								 </div>
								 <hr>
                                 <div class="col s12 m12 l12">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>SGST (9%)</b></p><p style="font-size:15px; float:right; text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?></p>
								 </div>
								 <hr>
								 <?php } else { ?>
								 <div class="col s12 m12 l12">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>IGST (18%)</b></p><p style="font-size:15px; float:right; text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?></p>
								 </div>
								 <hr>
								 <?php } ?>
								<div class="col s12 m12 l12">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>DISCOUNT</b></p><p style="font-size:15px; float:right; text-align:right;" class="big-w temp_text"><?=$USD?> <?php echo $billing_doc[0]->inv_discount_total ?></p>
								</div>
								<hr>
								<div class="col s12 m12 l12 zerocess">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>CESS</b></p><p style="float:right; font-size:15px; text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?></p>
								</div>
								<hr class="zerocess">
								<div class="col s12 m12 l12 zeroother">
									<p style="font-size:15px; float:left;" class="big-w temp_text"><b>OTHER</b></p><p style="float:right; font-size:15px; text-align:right;" class="big-w temp_text"><?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?></p>
								</div>
								<hr class="zeroother">
								<div class="col s12 m12 l12">
									<p style="font-size:16px; float:left;" class="big-w temp_text"><b>GRAND TOTAL</b></p><p style="float:right; font-size:16px; text-align:right;" class="big-w temp_text"><?=$USD?>&nbsp; <?=$this->Common_model->moneyFormatIndia($billing_doc[0]->inv_grant_total)?></p>
								</div>
								<!--p style="color:#000; float:left;" class="small-w temp_text">IN WORDS: <?//=$USD ?>&nbsp; <?php //echo strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)); ?></p-->
							</div>
						</div>
					</div>
					<div class="row" style="padding-top:1%; margin:0 -35px;">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<div class="bank_info">
								<p class="big-w temp_text">BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?></p>
								<p class="big-w temp_text">ACCOUNT NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?></p>
								<p class="big-w temp_text">BRANCH NAME: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?></p>
								<?php if($USD=="INR"){?>
									<p class="big-w temp_text">IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?></p>
								<?php } else { ?>
									<p class="big-w temp_text">SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?></p>
								<?php } ?>
								</div>
							</div>
							<div class="col s3 m3 l3">
								<p class="big-w temp_text cinno">CIN: <?= $company[0]->bus_cin_no; ?></p>
								<p class="big-w temp_text pan-1">PAN: <?= $company[0]->bus_pancard; ?></p>
								<p class="big-w temp_text gst_1">GSTIN: <?php echo $gstno[0]->gst_no; ?></p>
							</div>
							<div class="col s5 m5 l5">
								
							</div>
						</div>
					</div>
					
					<div class="row" style="margin: 4% -35px 0 -35px;">
						<div class="col s12 m12 l12">
							<div class="col s7 m7 l7" style="margin-left:-10px;">
								<p class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
								<div class="col s12 m12 112" id="cond" style="margin:0 0 0 0;">
								<div class="col s12 m12 l12">
									<ul class="term-list" style="margin-left:-20px;">
										<?= @$billing_doc[0]->inv_terms; ?>
									</ul>
								</div> 
								</div>
							</div>
							<div class="col s5 m5 l5" style="text-align:right; margin-top:10px;" id="sign_box">
								<div class="col s12 m12 l12" style="text-align:center;">
									<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
								</div>			
								<div class="col s12 m12 l12" style="margin-right:-10px !important;">
									<div class="col s2 m2 l1" style="margin:50px 0 0 14px;"></div>
									<?php if($billing_doc[0]->inv_signature !=""){?>
									<div class="col s12 m12 l12" style="margin-left:-30px !important;">
										<img width="180" height="50" src="" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; padding:10px 0 0 50px; border-top:1px solid #ccc;">
									<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($billing_doc[0]->inv_sign_name) ?></b></label><label class="up_lbl1 temp_text"><b><?php //echo strtoupper($billing_doc[0]->inv_sign_designation) ?></b></label></p>
								</div>
							</div>
						</div>
					</div>
                   </div>
                   </div>
                  </div>
               </div>
      </section>
      </div>
      </div>
      <!-- END MAIN -->
   </body>
   
   <footer style="margin:15px 0px 15px 0px; background-color:#000; border:1px solid #000;">
   <div class="row">
		<div class="col s3 m3 l3" style="width:20%;"></div>
		<div class="col s3 m3 l3" style="padding: 2% 0 0 0; text-align:center;">
			<img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook-b.png" alt="fb">
			<p style="color:#000;" class="small-w footer_p facebk"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></p>
		</div>
		<div class="col s3 m3 l3" style="padding:2% 0 0 0; text-align:center;">
			<img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter-2.png" alt="twit">
			<p style="color:#000;" class="small-w footer_p twitter"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></p>
		</div>
		<div class="col s3 m3 l3" style="padding:2% 0 0 0; text-align:center;">
			<img style="margin-top:0px;" width="50" height="30" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn-2.png" alt="link">
			<p style="color:#000;" class="small-w footer_p link_id"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></p>
		</div>
	</div>
	</footer>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/js/index.js"></script>
</html>
<div class="breakAfter"></div>
<script type="text/javascript">
   window.print();
</script>