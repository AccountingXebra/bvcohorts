	  <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="msapplication-tap-highlight" content="no">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <title>Xebra</title>
	  <style TYPE="text/css" MEDIA="screen, print">
		@import url(<?php echo base_url(); ?>asset/materialize/css/materialize.css);
        /*@import url(<?php echo base_url(); ?>asset/css/style.css);*/
		
         
		@media only screen and (min-width: 993px){
			.container {
				width: 100%;
			}
			
			@page {
				size: auto; 
			}
        }
		</style>
      <style type="text/css">
		body{
			margin: 2% 7% !important;
		}
		
        #main{
         padding-left: 0px !important;
        }
		 
		.row{
			margin-bottom:0px !important;
		}
		 
		@page {
            size: auto; 
            margin: 0mm;  
        }
				.big-text{
					font-size: 20px;
				}
				.small-text{
					font-size: 11.5px !important;
				}   
				.medium-text {
					font-size: 11.5px !important;
				}
				
				.teal.lighten-2{
					background-color:#000 !important;
				}
				
				.breadcrumbs-title, .breadcrumbs, .breadcrumbs-title, .small-text, .medium-text{
					color:#595959 !important;
					font-size:12px;
					padding-top:2px;
				}
				
				#first-tem-table .lime{
					margin-bottom:10px;
				}
				
				#first-tem-table .lime tr, th, td{
					background-color:#f2f2f2 !important;
					border:none !important;
					font-size:13px !important;
				}
				
				#first-tem-table .unit-pr{
					background-color:#b3b3b3 !important;
				}
				
				#first-tem-table .pr-last{
					background-color:#4CAF50 !important;
				}
				
				#total-last{
					color:#000 !important;
					font-size:12px !important;
				}
				
				#first-template p{
					font-size:12px !important;
				}
				
				.term-c, .bank-transfer{
					font-size:12px !important;
				}
				
				.term-c span, .bank-transfer span{
					font-size:12px !important;
					color:#000 !important; 
				}
				
				.card-panel{
					padding:0px 15px !important;
				}
				
				.grand.card-panel{
					padding:12px 50px !important;
				}
				
			</style>
			</head>
			<script type="text/javascript">
		$(document).ready(function() {
		//$('.txt_pnt').css("color", "<?=$custom_inv[0]->font_color?>");
   
		var USD='<?php print $currencycode[0]->currencycode; ?>';
    
		$('#tr_rate').html(USD);
		$('#tr_discount').html(USD);
		$('#tr_tax').html(USD);
		if(USD=="INR"){
			$('#to_words_currency').html("RUPEES ");
		}else{
			$('#to_words_currency').html(USD+".");
        }
		$('.par-spa rightss cemterfd').html(USD+" ");

      
		var final_grt=$('#tot_amount').text();
		var words=toWords(parseFloat(final_grt));
		if(words==''){
          $('#to_words_inv').text('ZERO');
		}else{
           $('#to_words_inv').text(words.toUpperCase()+' ONLY');
		}
		});
	   </script>
			<body class="container">
			<div class="card-panel teal lighten-2">
            <div class="row" style="margin-top:30px;">
				<?php
					$USD=$currencycode[0]->currencycode; 
				?>
                <div class="col l4" style="padding:25px 0;">
                    <?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                        if($custom_inv[0]->hdr_logo!=0){?>
							<img style="margin-top:-15px;" width="180" height="55" src="<?php echo base_url(); ?>public/upload/company_logos/<?= $company[0]->bus_id; ?>/<?= $company[0]->bus_company_logo; ?>" class="cmp-logo" alt="company_logo"/>
						<?php } ?>
						<h5 style="margin-top:-2px; text-align:center;" class="breadcrumbs-title"><?php echo strtoupper($company[0]->bus_company_name); ?></h5>
                    <!--P class="breadcrumbs">GLOBAL SOCIAL MEDIA</P-->
                </div>
                <div class="col l4">
                    <p> <span class="breadcrumbs-title medium-text">PAN NO. </span>
                        <span class="breadcrumbs small-text"><?= $company[0]->bus_pancard; ?></span> <br/>
						<span class="breadcrumbs-title medium-text">GSTIN</span>
                        <span class="breadcrumbs small-text"><?= @$gstno_cli[0]->gst_no; ?></span> <br/>
                        <span class="breadcrumbs-title medium-text">CIN NO. </span>
                        <span class="breadcrumbs small-text"><?= $company[0]->bus_cin_no; ?></span></p>
                </div>
                <div class="col l4">
                    <p style="margin-bottom:0px;" class="small-text"><?php echo wordwrap($billing_doc[0]->inv_address,40,"<br>\n"); ?></P>
					<label class="small-text"><b>TEL. NO.&nbsp&nbsp&nbsp: </b></label><label class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_phone_no,26,"\n"); ?></label></br>
					<label class="small-text"><b>EMAIL ID&nbsp&nbsp: </b></label><label class="small-text"><?php echo smart_wordwrap($billing_doc[0]->inv_email_id,26,"\n"); ?></label>
                </div>
            </div>
            <hr/>
            <div class="row" style="margin-top:-15px;">
                <div class="col l4 right">
					<?php if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ $inv="Estimate";?>
                           <h5 class="breadcrumbs-title">E S T I M A T E</h5>
                       <?php }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){ $inv="Proforma Inv";?>
                           <h5 class="breadcrumbs-title">P R O F O R M A &nbsp; I N V O I C E</h5>
                        <?php }else { $inv="Sales Inv";?>
                           <h5 class="breadcrumbs-title">S A L E S &nbsp; I N V O I C E</h5>
                       <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col l3">
					<h6 style="margin-bottom:-10px;" class="big-inv"><b>E&OE </b></h6>
                    <p> <span class="medium-text" style="font-size:15px;"><b> INVOICE TO </b></span> <br/>
                        <span style="color:#595959;"> <?php echo wordwrap($billing_doc[0]->cust_name,38,"<br>\n"); ?>, </span><br/>
                        <span class="small-text"><?php echo wordwrap($billing_doc[0]->inv_billing_address.', '.$billing_doc[0]->name.', '.$billing_doc[0]->state_name.', '.$billing_doc[0]->country_name.'-'.$billing_doc[0]->inv_billing_zipcode,35,"<br>\n"); ?></span></p>
                    <!--P class="medium-text">+91 98 191 847 21 <br/> mail@graphichuts.com</p-->
                    <P class="medium-text" style="margin-top:-15px !important; font-size:12px !important;">
						<label class="up_lbl">GSTIN: <?= @$gstno_cli[0]->gst_no; ?></label></br>
						<label class="up_lbl">PAN: <?= @$cust_pan[0]->cust_pancard; ?></label></br>
						<label class="up_lbl">PACE OF SUPPLY: <?=$place?></label><br>
						<label class="up_lbl">TERMS OF PAYMENT:</label><label class="txt_pnt"><?=@$cust_pan[0]->cust_credit_period;?></label>
					</p> 
                    
                </div>
                <div class="col l8 right card-panel green white-text">
                    <p class="col l3"><span class="medium-text"><?=strtoupper($inv)?></br> NO.</span> <br/> <span class="medium-text"><?=  @$billing_doc[0]->inv_invoice_no_view; ?></span></p>
                    <p class="col l3"><span class="medium-text"><?=strtoupper($inv)?></br> DATE</span> <br/> <span class="medium-text"><?=  ($billing_doc[0]->inv_invoice_date != '' && $billing_doc[0]->inv_invoice_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y",  strtotime($billing_doc[0]->inv_invoice_date))):''; ?></span></p>
					<?php if($custom_inv[0]->purchase_order && $billing_doc[0]->inv_po_no!=""){?>
                    <p class="col l3"><span class="medium-text">ESTIMATE / </br>P.O. NO</span> <br/> <span class="medium-text"><?=  @$billing_doc[0]->inv_po_no; ?></span><?php } ?></p>
					<?php if($custom_inv[0]->purchase_order_date && $billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00'){?>
                    <p class="col l3"><span class="medium-text">ESTIMATE / </br>P.O. DATE</span> <br/> <span class="medium-text"><?=  ($billing_doc[0]->inv_po_date != '' && $billing_doc[0]->inv_po_date != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($billing_doc[0]->inv_po_date))):''; ?></span><?php } ?></p>
                </div>
            </div>
        </div>
        <div>
					<?php $tax="true";
                         if(($billing_doc[0]->inv_igst_total!=0.00 || $billing_doc[0]->inv_igst_total!='') && ($billing_doc[0]->inv_cgst_total=='' || $billing_doc[0]->inv_cgst_total==0.00) && ($billing_doc[0]->inv_sgst_total=='' || $billing_doc[0]->inv_sgst_total==0.00) ) {
                           $tax="false"; 
                         }else{
                           $tax="true";
                         }

                        $inv_document_type='invoice';
                        $sub_tbl='';
                        if($billing_doc[0]->inv_document_type=="Estimate Invoice"){ 
                          $inv_document_type="estimate-invoice";$sub_tbl='estl';
                        }else if($billing_doc[0]->inv_document_type=="Proforma Invoice"){
                            $inv_document_type="proforma-invoice";$sub_tbl='prol';
                        }else if($billing_doc[0]->inv_document_type=="Sales Invoice"){
                           $inv_document_type="invoice";$sub_tbl='invl';
                        }
                          if($custom_inv[0]->hdr_logo!=0){
                         ?>
                     <?php } ?>
					 
					 <style>
                       <?php 
                       if(round($billing_doc[0]->inv_discount_total)==0){?>
                      .zerodiscount{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cgst_total)==0){?>
                      .zerocgst{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_sgst_total)==0){?>
                      .zerosgst{
                        display: none;
                      }
                      <?php }
                      ?>

                      <?php 
                      if(round($billing_doc[0]->inv_igst_total)==0){?>
                      .zeroigst{
                        display: none;
                      }
                      <?php }
                      ?>
                      <?php 
                      if(round($billing_doc[0]->inv_cess_total)==0){?>
                      .zerocess{
                        display: none;
                      }
                      <?php }
                      ?>
                       <?php 
                      if(round($billing_doc[0]->inv_other_total)==0){?>
                      .zeroother{
                        display: none;
                      }
                      <?php }
                      ?>
                     
                      </style>
		
            <table id="first-tem-table" class="centered">
                <thead class="lime">
                    <tr>
                    <th style="text-align: left;">ITEM DETAILS</th>
                    <th>UNIT PRICE</th>
                    <th>QTY</th>
					<th>DISCOUNT</th>
                    <th>PRICE</th>
                    </tr>
                </thead>
				<?php foreach ($billing_doc as $invkey => $invlist) {  ?>
                <tr>
                    <td style="text-align: left;">
                        <p><b>
						<?php //$invkey+1;
                        $temp=$sub_tbl.'_service_type'; ?>
                        <?php if($invlist->$temp==3){
                        echo "Equalisation Levy";
                        }else if($invlist->$temp==4){
                        echo "Late Fee";
                        }else if($invlist->$temp==2){
                        echo "Expense Voucher";
                        }else{
                        foreach($my_services as $service){
                        if($service->service_id==$invlist->service_id){
                        echo $service->service_name;
                        }
                        }
                        }?></b><br/> 
						<span class="">
						<?php  
                        $particulars=$sub_tbl.'_particulars'; 
                        $hsn=$sub_tbl.'_hsn_sac_no'; 
                        $quantity=$sub_tbl.'_quantity'; 
                        $rate=$sub_tbl.'_rate'; 
                        $discount=$sub_tbl.'_discount'; 
                        $taxable_amt=$sub_tbl.'_taxable_amt'; 
                        $igst=$sub_tbl.'_igst'; 
                        $igst_amt=$sub_tbl.'_igst_amt'; 
                        $cgst=$sub_tbl.'_cgst'; 
                        $cgst_amt=$sub_tbl.'_cgst_amt'; 
                        $sgst=$sub_tbl.'_sgst'; 
                        $sgst_amt=$sub_tbl.'_sgst_amt'; 
                        $cess=$sub_tbl.'_cess'; 
                        $cess_amt=$sub_tbl.'_cess_amt'; 
                        $other=$sub_tbl.'_other'; 
                        $other_amt=$sub_tbl.'_other_amt'; 
                        $amount=$sub_tbl.'_amount'; 
                        ?>
                        <?php echo wordwrap($invlist->$particulars,60,"<br>\n"); ?>
						</span>
						</p>
                    </td>
                    <td class="unit-pr"><?=round($invlist->$rate);  ?></td>
                    <td><?=$invlist->$quantity;  ?></td>
					<td><?=round($invlist->$discount);  ?></td>
                    <td class="pr-last"> <?=$USD?> <? echo $invlist->$rate * $invlist->$quantity - $invlist->$discount ?></td>
                </tr>
				<?php } ?>
                
                <!--tr>
                    <td style="text-align: left;">
                        <p><b>Print Design</b> <br/> <span class="" >Web page design for all party/buyer of<br/>  corporate office.</span></p>
                    </td>
                    <td class="unit-pr"> 915 </td>
                    <td> 03 </td>
                    <td class="pr-last"> ₹ 20000.00 </td>
                </tr-->
            </table>
            <div class="row">
                <div class="col l2 right" style="">
                    <p><?=$USD?> <?= round($billing_doc[0]->inv_taxable_total) ?></p>
                    <p class="total-last right" style="margin-top:-5px !important;">
                        <?php if($tax=="true") { ?><?=$USD?> <?=round($billing_doc[0]->inv_cgst_total); ?> <br/><?=$USD?> <?=round($billing_doc[0]->inv_sgst_total);  ?><?php } else { ?> <?=$USD?> <?=round($billing_doc[0]->inv_igst_total);  ?> <?php } ?><br/>
                        <?php if(round($billing_doc[0]->inv_cess_total)>0){?><?=$USD?> <?=round($billing_doc[0]->inv_cess_total);  ?><?php } ?> <?php if(round($billing_doc[0]->inv_other_total)>0){?><br/> <?=$USD?> <?=round($billing_doc[0]->inv_other_total);  ?><br/><?php } ?>
                    </p>
                </div>
                <div class="col l2 right">
                    <p>SUB TOTAL</p>
                    <p class="total-last" style="margin-top:-5px !important;">
                        <?php if($tax=="true") { ?>+SGST(09%) <br/> +CGST(09%)<?php } else { ?> +IGST(18%) <?php } ?><br/> 
                        <?php if(round($billing_doc[0]->inv_cess_total)>0){?>+CESS <?php } ?><?php if(round($billing_doc[0]->inv_other_total)>0){?><br/> +OTHERS<?php } ?>
                    </P>
                </div>
            </div>
            <div class="row" style="margin-bottom:0px;">
                <div class="col l3" style="margin-top:10px;">
					<!--button class = "btn green white-text" style="font-size: 15px !important;" type="submit"> MAKE PAYMENT </button-->
				</div>
				<div class="col l9 right">
                <p class="right grand card-panel green white-text" style="font-size: 15px; color:#595959 !important;">GRAND TOTAL <?=$USD?> <?=round($billing_doc[0]->inv_grant_total);  ?> </p> <br/></br></br>
                <P class="right" style="font-size: 15px; margin:0 0 !important;"><b>IN WORDS: </b>
				<?php echo strtoupper($this->Common_model->number_words(@$billing_doc[0]->inv_grant_total,$USD)); ?></P>
                </div>
            </div>
            <br/>
            <div class="row" style="border-bottom:1px solid #000;">
                <div class="col l8">
                    <p class="bank-transfer"><b>BANK TRANSFER</b><br/>
                        <span class="grey-text">
                            BANK NAME: <?= ucwords(@$billing_doc[0]->cbank_name); ?> <br/>
                            BRANCH: <?= ucwords(@$billing_doc[0]->cbank_branch_name); ?> <br/>
                            A/C NUMBER: <?= @$billing_doc[0]->inv_bank_account_no; ?> <br/>
                            <?php if($USD=="INR"){?>
								IFSC CODE: <?= @$billing_doc[0]->inv_bank_ifcs_code; ?>
							<?php } else { ?>
								SWIFT CODE: <?= @$billing_doc[0]->inv_bank_swift_code; ?>
							<?php } ?>
                        </span>
                    </p>
                </div>
                <div class="col l4 right">
                    <?php if($custom_inv[0]->inv_signature){?>
						<label class="up_lbl" style="font-size:13px; color:#000 !important;">FOR  <?= strtoupper($company[0]->bus_company_name) ?></label><br>
						<p class="txt_pnt" style=""><?//= @$billing_doc[0]->inv_signature; ?></p><br>
						<label style="font-size:13px; color:#000;" class="txt_pnt"><?= $billing_doc[0]->inv_sign_name; ?></label> <label style="font-size:13px; color:#000;" class="txt_pnt"><?//=$billing_doc[0]->inv_sign_designation; ?></label>
					<?php } ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col s7 m7 l7">
					<p class="term-c"><b>TERMS & CONDITIONS</b> <br/>
						<span class=""><?= @$billing_doc[0]->inv_terms; ?></span>
					</p>
				</div>
				<div class="col s5 m5 15" style="padding:50px 0;">
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>FACEBOOK&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_facebook,50,"\n"); ?></label>
					</div>
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>WEBSITE&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_website,26,"\n"); ?></label>
					</div>
					<div style="padding-bottom:10px;">
					<label style="font-size:13px; color:#000 !important;" class="term"><b>TWITTER&nbsp&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_twitter,26,"\n"); ?></label>
					</div>
					<label style="font-size:13px; color:#000 !important;" class="term"><b>LINKEDIN&nbsp&nbsp&nbsp&nbsp: </b></label><label style="font-size:13px; color:#000 !important;" class="term"><?php echo smart_wordwrap($billing_doc[0]->inv_blog,26,"\n"); ?></label>
				</div>
            </div>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
            </div-->
		</div>
	</body>
	<script type="text/javascript">
		window.print();
	</script>