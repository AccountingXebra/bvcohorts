<?php
	$this->load->view("/template/header");
?>
<section class="content-header">
    <h1>
       Send Gift   
    </h1>
    <?php
        $this->load->view("/template/bread_crumb");
    ?>
</section>
<section class="content">
    <div class="row">
         <div class="col-md-12">
             <div id="flash_msg" class="">
                <span id="msg_contant"></span>
            </div>
         </div>   
    </div>
	<div class="row">
	   <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                   <h3 class="box-title">Send Gift</h3>
                </div>

                <div class="box-body">
                    <div class="col-md-8">
                        
                    
                    <?php echo form_open_multipart('',array('name'=>"sendGiftFrm",'id'=>"sendGiftFrm" ,"class" => "form-horizontal")); ?>
                        <div class="form-group">
                            <?php echo form_error('vName'); ?>
                            <?php echo form_label('Name :','vName',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'vName', 'name' => 'vName' , 'class'=>'form-control','value'=>@$UserorderData['vName'] , 'disabled' => 'true')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('vEmail'); ?>
                            <?php echo form_label('Email :','vEmail',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'vEmail', 'name' => 'vEmail' , 'class'=>'form-control','value'=>@$UserorderData['vEmail'] , 'disabled' => 'true')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('vContact'); ?>
                            <?php echo form_label('Contact :','vContact',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'vContact', 'name' => 'vContact' , 'class'=>'form-control','value'=>@$UserorderData['vContact'] , 'disabled' => 'true')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('vAddress'); ?>
                            <?php echo form_label('Name :','vAddress',array("class"=>"col-md-3 control-label"));?>
                             <div class="col-md-8 ">
                                <?php echo form_textarea(array('id' => 'vAddress', 'name' => 'vAddress' , 'class'=>'form-control', 'value'=>@$UserorderData['vAddress'] , 'disabled' => 'true' , 'rows'=>'3')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('vPostCode'); ?>
                            <?php echo form_label('PostCode :','vPostCode',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'vPostCode', 'name' => 'vPostCode' , 'class'=>'form-control','value'=>@$UserorderData['vPostCode'] , 'disabled' => 'true')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('vCountry'); ?>
                            <?php echo form_label('Country :','vCountry',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'vCountry', 'name' => 'vCountry' , 'class'=>'form-control','value'=>@$UserorderData['vCountry'] , 'disabled' => 'true')); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo form_error('txGiftLink'); ?>
                            <?php echo form_label('Gift Link :','txGiftLink',array("class"=>"col-md-3 control-label"));?>
                            <div class="col-md-8 " >
                                <?php echo form_input(array('id' => 'txGiftLink', 'name' => 'txGiftLink' , 'class'=>'form-control','value'=>@$UserorderData['txGiftLink'])); ?>
                            </div>
                        </div>
                       

                        <div class="form-group">
                            <div class="col-md-11">
                                <button class="btn bg-purple btn-flat pull-right" type="submit" >Send</button>
                            <a href="<?=base_url()?>thermometersubscription/" class="btn btn-default btn-flat pull-right">Cancel</a>
                            </div>
                            
                        </div>    

                    <?php echo form_close(); ?>
                    </div>
                </div><!-- /.box-body -->
            </div>
          
        </div>
	</div>
</section>
<script src="<?=public_path()?>js/page/thermometersubscription.js" type="text/javascript"></script>
<script type="text/javascript">
    

</script>
<?php
	$this->load->view("/template/footer");
?>


