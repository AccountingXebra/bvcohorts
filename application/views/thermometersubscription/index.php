<?php
	$this->load->view("/template/header");
?>
<section class="content-header">
    <h1>
        Subscription List
    </h1>
    <?php
        $this->load->view("/template/bread_crumb");
    ?>
</section>
<section class="content">
    <div class="row">
         <div class="col-md-12">
             <div id="flash_msg" class="">
                <span id="msg_contant"></span>
            </div>
         </div>   
    </div>
	<div class="row">
	   <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                   <h3 class="box-title">Subscription List</h3>
                </div>
                <div class="box-body table-responsive">
                    <table id="adminTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User</th>                                 
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Address</th>
                                <th>PostCode</th>
                                <th>Country</th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($userOrderData)) { foreach ($userOrderData as $key => $value) { ?>
                            <tr class="<?php echo ($value['tGiftStatus'] == 1)? "giftSend" : "";  ?>">
                                <td><?= $value['user']; ?></td>
                                <td><?= $value['vName']; ?></td>
                                <td><?= $value['vEmail']; ?></td>
                                <td><?= $value['vContact']; ?></td>
                                <td><?= $value['vAddress']; ?></td>
                                <td><?= $value['vPostCode']; ?></td>
                                <td><?= $value['vCountry']; ?></td>
                                <td><?php if($value['tGiftStatus'] == 0) { ?> <a class="btn bg-blue" href="<?=base_url()?>thermometersubscription/sendGift/<?= $value['iUserAddressId']; ?>" ><i class="fa fa-gift"></i></a> <?php } ?></td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
          
        </div>
	</div>

</section>
<script src="<?=public_path()?>js/page/thermometersubscription.js" type="text/javascript"></script>
<?php
	$this->load->view("/template/footer");
?>
