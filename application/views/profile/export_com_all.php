<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('List of Companies');
$this->excel->getActiveSheet()->setCellValue('A1', 'List of Companies');
$this->excel->getActiveSheet()->mergeCells('A1:D1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'COMPANY NAME');
$this->excel->getActiveSheet()->setCellValue('B3', 'TYPE');
$this->excel->getActiveSheet()->setCellValue('C3', 'SIZE');
$this->excel->getActiveSheet()->setCellValue('D3', 'REVENUE');
$this->excel->getActiveSheet()->setCellValue('E3', 'INCORPORATION DATE');
$this->excel->getActiveSheet()->setCellValue('F3', 'CLIENTS');
$this->excel->getActiveSheet()->setCellValue('G3', 'PANCARD');
$this->excel->getActiveSheet()->setCellValue('H3', 'CIN NO');
$this->excel->getActiveSheet()->setCellValue('I3', 'FY STARTDATE');
$this->excel->getActiveSheet()->setCellValue('J3', 'FY ENDDATE');
$this->excel->getActiveSheet()->setCellValue('K3', 'EMAIL');
$this->excel->getActiveSheet()->setCellValue('L3', 'CONTACT');
$this->excel->getActiveSheet()->setCellValue('M3', 'COUNTRY');
$this->excel->getActiveSheet()->setCellValue('N3', 'STATE');
$this->excel->getActiveSheet()->setCellValue('O3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('P3', 'ZIPCODE');
$this->excel->getActiveSheet()->getStyle("A3:P3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['bus_company_name']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $value['company_type']);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['bus_company_size']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['bus_company_revenue']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['bus_incorporation_date']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['bus_loc']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['bus_pancard']);
	$this->excel->getActiveSheet()->setCellValue('H'.$count, $value['bus_cin_no']);
	$this->excel->getActiveSheet()->setCellValue('I'.$count, $value['bus_fy_startdate']);
	$this->excel->getActiveSheet()->setCellValue('J'.$count, $value['bus_fy_enddate']);
	$this->excel->getActiveSheet()->setCellValue('K'.$count, $value['bus_company_email']);
	$this->excel->getActiveSheet()->setCellValue('L'.$count, $value['bus_company_contact']);
	$this->excel->getActiveSheet()->setCellValue('M'.$count, $value['country_name']);
	$this->excel->getActiveSheet()->setCellValue('N'.$count, $value['state_name']);
	$this->excel->getActiveSheet()->setCellValue('O'.$count, $value['bcity']);
	$this->excel->getActiveSheet()->setCellValue('P'.$count, $value['bus_billing_zipcode']);
	$count++;
}

$filename='List of Companies '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
	ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>