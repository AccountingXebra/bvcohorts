<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('List of Users');
$this->excel->getActiveSheet()->setCellValue('A1', 'Personal Profile Information');
$this->excel->getActiveSheet()->mergeCells('A1:D1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'NAME');
$this->excel->getActiveSheet()->setCellValue('B3', 'EMAIL');
$this->excel->getActiveSheet()->setCellValue('C3', 'MOBILE');
$this->excel->getActiveSheet()->setCellValue('D3', 'DESIGNATION');
$this->excel->getActiveSheet()->setCellValue('E3', 'ACCESS');

$this->excel->getActiveSheet()->getStyle("A3:E3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['reg_username']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $value['reg_email']);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['reg_mobile']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['reg_designation']);
	if($value['reg_admin_type'] == '-1'){
	$this->excel->getActiveSheet()->setCellValue('E'.$count, "Admin");	
	}else{
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['access']);
	}
	$count++;
}

$filename='List of Users '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
	ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>