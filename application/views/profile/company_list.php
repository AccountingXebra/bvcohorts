 <?php $this->load->view('template/header.php');?>

<style type="text/css">
  .dataTables_scrollBody{
			overflow:hidden !important;
			height:100% !important;
		}

		.dataTables_scrollHead{
			margin-bottom:-24px !important;
		}

		table.dataTable thead .sorting {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_asc {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_desc {
		  background-position: 110px 15px !important;
		}
  .select-dropdown{
    max-height: 350px !important;
  }
  .icon-img {
        margin: 0 31px 0 2px;
  }

 .dataTables_length {
  
    margin-left: 500px;
}

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;
  
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12PX;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 150px !important;
}
/*----------END SEARCH DROPDOWN CSS--------*/

#instamojo_modal .modal-header{
	padding:0px !important;
}

#instamojo_modal .modal-header h4 {
    color:#000066;
	width: 95% !important;
}

#instamojo_modal p{
	margin: 7px 0px 11px 0px !important;
	color:#000066;
	font-size:14px !important;
}

#instamojo_modal #insta_note{
	margin: -20px 0px 11px 0px !important;
	color:black;
}

#instamojo_modal label{
	color:black;
	font-size:15px;
}

#api_key:focus{
	background-color:red;
}

#insta_close{
	background-color:white;
	color:#c2c2a3;
}
	
	#company-profile_length .dropdown-content {
		min-width: 96px;
		margin-top:40% !important;
	}
	
	#company-profile_paginate{
		display:none !important;
	}
	
	#company_profile_bulk + label{
		display:none !important;
	}
	
	#filled-in-box1 + label{
		display:none !important;
	}
	
	#company-profile_length{
		display:none !important;
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:95px;
		margin-top:5px;
		margin-left:52%;
	}
	
	#company-profile_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	.company_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}
	
	#company-profile_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}
	
	.sticky {
		position: fixed;
		top: 70px;
		width: 75.6%;
		z-index:999;
		background: white;
		color: black;
		
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}
	
	.pay-dis[disabled]{
		background-color:#ffffff !important;	
		border:1px solid #e3e7f2 !important;
	}
	
	a.filter-search.btn-search.btn.active {
		width:32% !important;
	}
	
	.table-type1 thead th {
		padding:0 2px !important;
	}
	
	td.profil-img {
		text-align:left !important;
	}
	
	#logout_for_new_bus_modal .modal-header{
		padding:10px !important;
	}
	
	.ex{
		margin:-28px 0 0 48px !important;
	}
	
</style>
<div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content" class="bg-cp company-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">My Company Profile <small class="grey-text">(<?php echo count($company_profiles); ?> Total)</small></h5>

                  <ol class="breadcrumbs">
                    <!-- <li><a class="cur">My Profile</a>
                    </li> -->
                    <li class="active">My Company Profile</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6"><!--<?php echo base_url(); ?>profile/add-company-profile-->
                  <a id="add_cmpy" class="btn btn-theme btn-large right modal-trigger modal-close" data-target="#">ADD NEW COMPANY PROFILE</a>
                </div>
              </div>
            </div>
          </div>

          <div id="bulk-action-wrapper" style="display:none">
            <div class="container">
              <div class="row">
                <div class="col l5 s12 m12">
                  
                  <!--a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk04'>Bulk Actions <i class="arrow-icon"></i></a-->
                  <!--ul id='dropdown_bulk04' class='dropdown-content'>
                    <li><a id="email_multiple_cmp_profiles"><i class="dropdwon-icon icon email"></i>Email</a></li>
                      <li><a id="download_multiple_cmp_profiles" data-multi_download="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;" alt="export">Export</a></li>
                      <li><a id="print_multiple_cmp_profiles"><i class="material-icons">print</i>Print</a></li>
                      <li><a  id="deactive_multiple_cmp_profiles" data-multi_profiles="0" ><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li>  
                  </ul-->
                  <a class="filter-search btn-search btn" style="display:none">
                    <input type="text" name="search" id="search_cprofile" class="search-hide-show"  />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>

                 <!--  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown04' class='dropdown-content'>
                      <li><a href="<?php echo base_url(); ?>profile/company_download"><i class="dropdwon-icon icon download"></i> Download</a></li>
                      <li><a  id="deactive_multiple_cmp_profiles" data-multi_profiles="0" ><i class="dropdwon-icon icon deactivate"></i> Deactivate</a></li>
                    </ul>
                    <a class="filter-search btn-search btn"><i class="material-icons ser">search</i></a> -->
                </div>


               <div class="col l7 s12 m12" style="display:none">
                  <div class="action-btn-wapper right">
                    <div class="bulk location-drop">
						  <select class="js-example-basic-single" name="country" id="allcountry">
                           <option value="">SELECT COUNTRY</option>
                           <option value="">ALL</option>
                             <?php if($countries != '') {
                                foreach($countries as $country) { ?>
                                  <option class="icon-location" hidden value="<?php echo $country->country_id; ?>"><img src="http://localhost/invoice/asset/css/img/icons/locate.png" width="20px" alt="locate" /> <?php echo $country->country_name; ?></option>
                                <?php }  } ?>
                          </select>
                          <select class="js-example-basic-single" name="state" id="allstate">
                      <option value="">SELECT STATE</option>
                      <option value="">ALL</option>

                    </select>
                    </div>
                   
                  </div> 
                </div>

                <!-- <div class="col l2 s12 m12">
                  <div class="action-btn-wapper right">
                      <select class="js-example-basic-single" name="state" id="state">
                      <option value="">Select State</option>
                    </select>
                  </div> 
                </div> -->


              </div>
            </div>
          </div>


	   <div class="container">
		<div class="row">
		  <div class="col l12 s12 m12">
			<!--<form>-->
			  <table id="company-profile" class="responsive-table display table-type1" border="0" cellspacing="0">
				<thead id="fixedHeader">
				  <tr style="height:50px;">
					<!--<th style="width:10px; padding:0 0 50px 0 !important;">
					  <input type="checkbox" id="company_profile_bulk" name="company_profile_bulk" class="filled-in purple" />
					  <label for="company_profile_bulk"></label>
					</th>-->
					<th style="width:10%;"></th>
					<th style="width:25%;" class="comp-sort-name">Name</th>
					<th style="width:15%;"> Type of company</th>
					<th style="width:10%;">gst number</th>
					<th style="width:10%;" class="comp-bank-sort">bank details</th>
					<th style="width:10%;">payment gateway</th>
					<th style="width:5%;">Action</th>
				  </tr>
				</thead>
				<tbody class="scrollbody">
			   
				</tbody>
			  </table>
			<!--</form>-->
		  </div>
		</div>
	  </div>             	 
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
 <!-- END MAIN -->
		<!-- ----- Start Logout For New Company ----------- -->
		  <!--<div id="logout_for_new_bus_modal" class="modal fade" aria-hidden="true">
			 <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png">
			 <div class="modal-content">
				<div class="modal-header">
				   <h4>Sign Out to add new company</h4>
				   
				</div>
			 </div>
			 <div class="modal-body confirma">
				<p class="ml-20">Please Sign Up to add new company, Are you sure you want to Sign Out</p>
			 </div>
			 <div class="modal-content">
				<div class="modal-footer">
				   <div class="row">
					  <!--   <div class="col l4 s12 m12"></div> text-right-->
					  <!--<div class="col l12 s12 m12 cancel-deactiv ">
						 <a class="close modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button" data-dismiss="modal" >CANCEL</a>
						 <a class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close logout_my_account_bus centertext">CONFIRM</a>
					  </div>
				   </div>
				</div>
			 </div>
		  </div>-->
 
<div id="send_email_company_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
    <?php $this->load->view('profile/company_details_email'); ?>
</div>

<div id="instamojo_modal" class="modal modal-md ps-active-y" style="height:auto !important; margin-top:-60px; max-width:700px !important;">
    <div class="modal-dialog">
      <div class="modal-content" style="margin:10px 1px !important;">
        <div class="modal-header" style="padding-left:15px !important;">
          <a style="margin-right:-5px; margin-bottom:-30px;" class="modal-close close-pop" style="">
		  <img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
          <h4 class="modal-title">Instamojo Payment Integration</h4>
        </div>
        <div class="modal-body" style="margin:-5px 0 0 0 !important; padding:0 15px; width:100% !important;">
		<div class="col s12 m12 l12" style="text-align:center; margin:10px 10px !important;">
			<a href='https://www.instamojo.com/?referrer=client&partner_id=c995c1673ee8400d8df23fa941bd7858' target="_blank" class="btn-flat theme-primary-btn theme-btn theme-btn-large" type="button" style="background-color:#000066 !important; height:36px !important; line-height:36px !important;">SIGN UP ON INSTAMOJO</a>
		</div>
		<div class="row" style="margin-top:5px; margin-bottom:0px !important;">
			<label style="color:#000066;"><i> Instructions:</i></label>
			<i><ol style="margin-left:-24px; margin-top:-5px !important;">
				<li style="color:#000066; font-size:13px;"><p>Sign up on <a style="color:#000066;" href="http://instamojo.com" ><b>http://instamojo.com</b></a> if you don't already have an account here.</p></li>
				<li style="color:#000066; font-size:13px;"><p>After registration, enter the <b> Private API Key </b> and <b> Private Authorization Token </b>that you will find in your profile.</p></li>
				<li style="color:#000066; font-size:13px;"><p>Post submission, your Instamojo payment gateway will be linked with <b>Xebra</b> making it possible for you to integrate it in your invoices and make it easier for your clients to pay you directly.</p></li>
			</ol></i>
		</div>
		<div class="row" style="text-align:center; margin-bottom:0px!important;">
			<img class="" height="160" width="500" src="<?php echo base_url(); ?>asset/images/insta_view.png" alt="insta-view">
		</div>
		<form id='insta_form' style="margin-top:10px; margin-bottom:20px;">
          <div class="row">
			<div class="col s12 m12 l12" style="text-align:center; margin-top:0px; font-size:13px;"><span style="color:#000066;"><b>OR</b></span></div>
			<div class="col s12 m12 l12" style="text-align:center; margin-top:5px; font-size:13px;">
				<!--span style="color:#000066;"><b>IF YOU ALREADY HAVE AN ACCOUNT</b></span-->
				<a class="btn-flat theme-primary-btn theme-btn theme-btn-large" type="button" style="background-color:#000066 !important; height:36px !important; line-height:36px !important; pointer-events:none;">IF YOU ALREADY HAVE AN ACCOUNT</a>
			</div>
			<div class="col s12 m12 l12" style="margin-top:5px; margin-left:-48px; width:657px;">
				<div class="col s12 m12 l6 input-field">
                      <label class="full-bg-label" style='margin:10px 0 !important; font-size:13px;'>ENTER YOUR PRIVATE API KEY</label>
                      <input style='background-color:white !important; height: 2rem !important; margin-bottom:10px !important; width: calc(100% - 20px) !important;' name="api_key" id="api_key" value='' class="full-bg adjust-width border-right" type="text">
                </div>
				<div class="col s12 m12 l6 input-field">
                      <label class="full-bg-label" style='margin:8px 0 !important; font-size:13px; width:330px !important; '>ENTER YOUR PRIVATE AUTHENTICATION TOKEN</label>
                      <input style='background-color:white !important; height: 2rem !important; margin-bottom:10px !important; width: calc(100% - -45px) !important;' name="auth_token" id="auth_token" value='' class="full-bg adjust-width border-right" type="text">
                </div>
				<input type='text' id='bus_id' name='bus_id' value='' hidden></input>
			</div>
		  </div>
		  <p id="insta_note"><i>NOTE : Instamojo is a third party site and we recommend that you read and understand their Terms and Conditions and Privacy Policy</i></p>
		  <div class="row" style="float:right; margin-top:10px;">
			<button type="button" id="insta_close" class="btn btn-flat theme-primary-btn theme-btn theme-btn-large modal-close" data-dismiss="modal" style="color:black !important;">CANCEL</button>
			<input id='insta_submit' class="btn btn-flat theme-primary-btn theme-btn theme-btn-large" type="submit" style='margin-right:5px;' value="SAVE" />
		  </div>
      <a id="success_modal" href="#instamojo_success_modal" class="cur customize_cfields modal-trigger modal-close" hidden></a>
		</form>
		</div>
      </div>
      
    </div>
</div>

<div id="instamojo_success_modal" class="modal modal-md ps-active-y" style="height:50% !important; margin-top:0px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:0px;">
          <a style="margin-right:-5px; margin-top:3px;" class="modal-close close-pop" style="">
		  <img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
          <h4 class="modal-title" style="width:65% !important; color:#000066;">Instamojo Payment Integration</h4>
        </div>
        <div class="modal-body" style="margin:8px 0 0 0 !important;">
			<div class="row" style="text-align:center;">
				<img style="margin-top:20px;" class="" height="50" width="50" src="<?php echo base_url(); ?>asset/images/correct.png" alt="correct">
				<p style="font-size:20px; color:#000066; margin-bottom:-16px !important; margin-top:16px !important;">Congratulations</p>
				<p style="font-size:13px; color:#000066;">Your InstaMojo account is now integrated with Xebra</p>
			</div>
		</div>
	  </div>
	</div>
</div>		

 <script type="text/javascript">
    $(document).ready(function() {
		
		$("#add_cmpy").on("click",function(){
        $.ajax({
        url:base_url+'profile/check_for_Bus',
        type:"POST",
        datatype: 'json',
        data:{ },
        success:function(res){
          

          if(res == 'exist')
          {
            $("#logout_for_new_bus_modal").show(); 
            return false; 
          }
          else if(res == 'notexist')
          {
            window.location.href="<?php echo base_url();?>profile/add_company_profile"; 
          }
          else
          {
            Materialize.toast('Something went wrong. Try again', 2000,'green rounded');
          }

        },
      });
		});
		
		$(".log_close").on("click",function(){
			$("#logout_for_new_bus_modal").hide();
		});
		
		$(".model-cancel").on("click",function(){
			$("#logout_for_new_bus_modal").hide();
		});
		
		
      $('.js-example-basic-single').select2();
  }); 
</script>
	 <?php $this->load->view('template/footer.php');?>