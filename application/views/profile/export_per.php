<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('User Information');
$this->excel->getActiveSheet()->setCellValue('A1', 'Personal Profile Information');
$this->excel->getActiveSheet()->mergeCells('A1:E1');
$this->excel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);


$this->excel->getActiveSheet()->setCellValue('A3', 'Name');
$this->excel->getActiveSheet()->setCellValue('A4', @$exp_data[0]->reg_username);
$this->excel->getActiveSheet()->setCellValue('B3', 'Email');
$this->excel->getActiveSheet()->setCellValue('B4', @$exp_data[0]->reg_email);
$this->excel->getActiveSheet()->setCellValue('C3', 'Mobile');
$this->excel->getActiveSheet()->setCellValue('C4', @$exp_data[0]->reg_mobile);
$this->excel->getActiveSheet()->setCellValue('D3', 'Designation');
$this->excel->getActiveSheet()->setCellValue('D4', @$exp_data[0]->reg_designation);
$this->excel->getActiveSheet()->setCellValue('E3', 'Access');
if(@$exp_data[0]->reg_admin_type== -1) {
$this->excel->getActiveSheet()->setCellValue('E4', 'Admin');
}
else{
$this->excel->getActiveSheet()->setCellValue('E4', @$access_data[0]->access);
}

$filename='User Information '.date("d-m-Y").'.xlsx'; //save our workbook as this file name

$filename='Personal Profile - '.@$exp_data[0]->reg_username.'.xlsx'; //save our workbook as this file name

ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>