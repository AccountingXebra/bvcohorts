<?php $this->load->view('template/header.php');?>

<style type="text/css">
	.com-nature + label.error {
		margin: 0px 0 0 -10px !important;
	}

	.url label.error {
		margin-top: -10px !important;
	}

	.url label.error.active {
		margin-top: 0px !important;
	}

	.tooltip {
		position: relative;
		display: inline-block;
		margin-bottom:-6px !important;
	}

	.tooltip .tooltiptext {
	  visibility: hidden;
	  width: 150px;
	  background-color: #7864e9;
	  color: #fff;
	  text-align: center;
	  font-size:13px !important;
	  border-radius: 6px;
	  padding: 5px 0;
	  margin: 20px 0 -20px 0;
	  /* Position the tooltip */
	  position: absolute;
	  z-index: 1;
	}

	.tooltip:hover .tooltiptext {
	  visibility: visible;
	}

	.uploader-placeholder{
		height:90px !important;
	}

  .border-split-form .select-wrapper{
        padding: 7px 0 2px 0 !important;
            top: 0px !important;
  }
  .li-autocomplete{
  padding: 2px 0px 2px 20px !important;

  box-shadow: 0 0 1px 0 #bbb !important;

  background-color: #fff !important;
  }
  .ul-autocomplete{
    z-index: 99999 !important;

  }
  .select-wrapper label.error:not(.active) {
     margin: -30px 0 0 -11px;
  }
/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container {
  width: 100% !important;
  margin-top: -2px !important;
}
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 35px;
  color: #666;
  font-weight: 400;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 53px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
  border-top-left-radius: 0;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
   /* border: none;*/
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93.5%;
    max-width: 100%;
    background: #fff;
    border-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
.select2-container--open .select2-dropdown--below {
  margin-top: -15px;
/*  border-top: 1px solid #aaa;
  border-radius: 5px !important;*/
}
ul#select2-currency-results {
  width: 97%;
}

/*.select2-container--open .select2-dropdown--above{
    border-top: 1px solid #aaa;
    border-radius: 5px !important;
}*/

/*----------END SEARCH DROPDOWN CSS--------*/
/*------------- START Aniket CSS-----------------------*/

#select2-currency_format-container {
    font-size: 14px;
    color: #000;
    font-weight: 500;
}
/*------------- END Aniket CSS-----------------------*/
/*---Dropdown error message format---*/
.select-wrapper + label.error{
 margin: 18px 0 0 -10px;
}

.dec_pt.select-wrapper + label.error{
 margin: 1px 0 0 -10px;
}


select.error + label.error:not(.active){

 margin: -20px 0 0 -10px;
}
select + label.error.active{
  margin-left: -10px;
}
/*---End dropdown error message format---*/
.bodrpinkright {
      border-left: 3px solid #ff7c9b !important;
}
#pincode-error{
	margin-bottom:2px !important;
}

#pincode-error{
	top:-8px !important;
}

#select2-currency-container{
    font-size: 13px;
    line-height: 35px;
    color: #666 !important;
    font-weight: 400;
}

#select2-timezone-container{
    font-size: 13px;
    line-height: 35px;
    color: #666 !important;
    font-weight: 400;

#add_cmp_gst_frm{

  max-height: 80% !important;
}

.footer_btn{
	height:35px !important;
	line-height: 35px;
}

	.dropdown-content.select-dropdown {
		height:15vh !important;
		overflow-y: scroll !important;
	}

	.country-dropdown span.caret {
		margin: 28px 15px 0 0 !important;
	}

	.branch-add .select-dropdown {
		height:15vh !important;
		overflow-y: scroll !important;
	}

	.country-dropdown .select-dropdown {
		/*height:15vh !important;*/
		overflow-y: scroll !important;
	}
	/*.select2-container--default .select2-selection--single transparent*/
	#add_cmp_gst_frm .select2-container {
		background-color: #f8f9fd !important;
	}

	#add_company_profile .select2-container .select2-selection--single {
		margin: 10px 0 0 0 !important;
		padding:0 0 0 0 !important;
	}

	#add_company_profile .select2-container--default .select2-selection--single .select2-selection__rendered {
		line-height: 28px !important;
	}
	#add_company_profile .select2-container--default .select2-selection--single .select2-selection__arrow {
		top:5px !important;

	}

</style>
    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

          <!-- START CONTENT -->
          <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12">
                    <a style="margin-left:10% !important;" class="go-back underline" href="<?php echo base_url();?>profile/company_profile">Back to My Company Profiles</a>
                  </div>
                  <div class="col l6 s12 m6">
                  </div>
                </div>
              </div>

        <form class="create-company-form border-split-form" name="add_company_profile" id="add_company_profile" method="post" action="<?php echo base_url(); ?>profile/insert_company_profile" enctype="multipart/form-data">
          <section id="content">
            <div class="container">
              <a id="click_me" href="#welcome_inv" class="modal-trigger" hidden><span>CLICK MEM</span></a>
              <div class="page-content">
				<div class="row" style="margin-bottom: 20px !important;">
                  <div class="col s12 m12 l3"></div>
                  <div class="col s12 m12 l6">
                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
                      <div class="box-header com_pro" style="border-bottom:1px solid #EEF2FE; margin-bottom:20px !important;">
                        <h3 class="box-title">Create Company Profile</h3>
                      </div>
                      <div class="box-body">
                        <div class="row">
							<div class="col s12 m12 l12">
							<div class="input-field col s12 m12 l3" style="margin-left:-8px !important;">
								<label id="entergstin" for="search_gstin" class="full-bg-label" style="margin-top:4px !important;">Enter GSTIN</label><a style="margin:-3px -4px 0 0;" class="info-ref tooltipped info-tooltipped" data-position="bottom" data-html="true" data-delay="50" data-tooltip="It's a state-wise, PAN-based, 15-digit Goods and </br>Services Taxpayer Identification Number (GSTIN)"></a>
							</div>
							<div class="input-field col s12 m12 l6" style="margin-right:-15px;">
								<input id="search_gstin" name="search_gstin" class="full-bg adjust-width border-right" type="text" placeholder="ENTER YOUR CLIENT'S GSTIN" style="padding: 0px 0px 0px 10px !important; margin-bottom:10px !important; border:1px solid #EEF2FE !important; border-radius:5px !important;">
							</div>
							<div  id="verified_mark" hidden="hidden" class="input-field col s12 m12 l1">
								<img width="30" height="30" src="<?php echo base_url(); ?>public/images/icic-right.png" alt="correct" class="correct-gstin" style="margin:9px 0px 0px -25px;" />
							</div>
							<div class="input-field col s12 m12 l2">
								<button style="border-radius:10px; border:1px solid #50f0AE !important; color:#ffffff; background-color:#50f0AE !important; margin:5px 0px 0px -15px !important;" type="button" class="btn btn-default" id="gstingo">GO</button>
							</div>
						  </div>
						  <div class="col s12 m12 l12">
							<label style="font-weight:bold; margin-left:8% !important;" id="entergstin" for="search_gstin" class="full-bg-label">We will verify the GSTIN and autofill the cells (WOOHOO!!)</label>
						  <div id=gstMsg></div>
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
                <div class="row">
                  <div class="col s12 m12 l3"></div>
                  <div class="col s12 m12 l6">
                    <div class="box-wrapper bg-white shadow border-radius-6">
                      <div class="box-body">
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l9">
                              <div class="row">
                                <div class="input-field">
                                  <label for="company_name" class="full-bg-label">Company Name<span class="required_field">* </span></label>
                                  <input id="company_name" name="bus_company_name" class="full-bg adjust-width" type="text"  style="height:60px !important;">
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m12 l3">
                              <div class="row">
                                <div class="input-field tooltip">
                                  <label class="up_pic" style="font-size:11px; color:#696969; margin-top:-8px !important; margin-left:30px;">Upload Logo</label>
								  <div class="uploader-placeholder">
                                    <input type="file" class="hide-file" id="company_logo" name="bus_company_logo">
                                  </div>
								  <span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                         <input id="subscription_id" name="subscription_id" type="hidden" value="<?php echo $subscription_id; ?>">
                        <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field">
                                <label for="billaddress" class="full-bg-label">BILLING ADDRESS<span class="required_field">* </span></label>
                                <input id="billing_address" name="bus_billing_address" class="full-bg adjust-width border-top-none" type="text" value="">
                              </div>
                            </div>
                          </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set " id="countrysec">
                              <div class="input-field">
								<label for="country" class="full-bg-label select-label">Country<span class="required_field"> </span></label>
                                <select class="js-example-basic-single country-dropdown check-label" name="bus_billing_country" id="country">
									<option value="">COUNTRY *</option>
                                    <?php foreach ($countries as $country) { ?>
                                    <option value="<?php echo $country->country_id; ?>"><?php echo strtoupper($country->country_name); ?></option>
                                     <?php } ?>
                                  </select>
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-right-none" id="statesec">
                              <div class="input-field">
                                <label for="state" class="full-bg-label select-label state_label">State<span class="required_field"></span></label>
                                  <select class="js-example-basic-single" name="bus_billing_state" id="state">
                                     <option value="">STATE *</option>
                                  </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set border-field" id="citysec">
                              <div class="input-field">
                                <label for="city" class="full-bg-label select-label">City<span class="required_field"></span></label>
                                  <select class="js-example-basic-single" name="bus_billing_city" id="city">
                                   <option value="">CITY *</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-field border-right-none">
                              <div class="row">
                                <div class="input-field">
                                    <label for="zipcode" id="pin_code" class="full-bg-label">PIN Code<span class="required_field"> *</span></label>
                                    <input id="pincode" name="bus_billing_zipcode" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                          <!-- <div class="row">
                            <div class="col s12 m12 l12">

                              <div class="col s12 m12 l6">
                                <div class="row">
                                  <div class="input-field border-field">
                                    <label for="shipping" class="full-bg-label">SHIPPING ADDRESS<span class="optionalss"></span></label>
                                    <input id="shipping_address" name="bus_shipping_address" class="full-bg adjust-width shipp-add" type="text">
                                  </div>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 border-botom-field border-field">
                                <div class="drop-down invoice-no">
                                  <div class="col s12 chkbox2">
                                    <input type="checkbox" id="same-bill" name="same-bill">
                                    <label class="checkboxx2" for="same-bill"><span class="check-boxs">Same as billing address</span></label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set border-botom-field" id="shipcountrysec">
                                <div class="input-field shipping_values" >
                                  <label for="shipping_country" class="full-bg-label select-label">COUNTRY</label>
                                  <select class="js-example-basic-single" name="bus_shipping_country" id="shipping_country">
                                     <option value="">COUNTRY</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                                     <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set border-right-none border-botom-field" id="shipstatesec">
                                <div class="input-field shipping_values">
                                  <label for="shipping_state" class="full-bg-label select-label">STATE</label>
                                    <select class="js-example-basic-single" name="bus_shipping_state" id="shipping_state">
                                     <option value="">STATE</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set" id="shipcitysec">
                                  <div class="input-field shipping_values">
                                    <label for="shipping_city" class="full-bg-label select-label">City</label>
                                        <select class="js-example-basic-single" name="bus_shipping_city" id="shipping_city">
                                           <option value="">STATE</option>
                                        </select>
                                  </div>
                              </div>

                              <div class="col s12 m12 l6 input-set border-right-none">
                                <div class="row">
                                  <div class="input-field">
                                      <label for="pincode" class="full-bg-label">PIN CODE</label>
                                      <input id="shipping_pincode" name="bus_shipping_zipcode" class="full-bg adjust-width" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div> -->
                      </div>
                    </div>

                    <div class="">
                      <h4 class="box-inner-title">
                        Statutory Info
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white bg-img-red shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                      <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip=" PAN  is to be quoted during all the financial transactions of the <br/>Company as well as in the invoices and other registrations" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                    <label for="country" class="full-bg-label">PAN NUMBER<span class="required_field"></span></label>
                                     <input id="pan_num" name="bus_pancard" class="full-bg adjust-width border-top-none" type="text" title="Please enter 10 digits for a valid PAN number">
                                  </div>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                     <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip="Corporate Identity Numbet is a unique identification number assigned by Registrars of Companies (ROC)<br/>functioning in various states under Ministry of Corporate Affairs" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                      <label for="zipcode" class="full-bg-label">CIN NUMBER</label>
                                      <input id="cin" name="bus_cin_no" class="full-bg adjust-width border-top-none" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

					         	<div class="active-show">
                              <div class="col s12 m12 l12 input-set">
                                <div class="row">
                                  <div class="input-field legal">
                                    <p class="gstin" title="Please enter 15 digits for a valid GSTIN number">GSTIN<span class="required_field">*</span> <small class="gray-box two-i tot_gst">0</small></p>
                                    <a href="#add_cmp_gst_frm" class="modal-trigger"><input type="hidden" id="tot_gst_nu" value="0"><img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="pluse-icon" alt="plus"></a>
                                  </div>
                                </div>
                                <div class="row" id="company_gst_array_list">

                                </div>
                              </div>
                              <div class="row">
                                <div class="col l12 s12 m12 text-center">
                                  <a onclick="showGstMore()" id="showgstboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                              </div>
                              </div>

                          <?php /*?><div class="row">
                            <div class="col l12 s12 m12">
                              <div class="input-field emty-field-row border-botom-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">GSTIN NUMBER
                                      <a href="#add_gst_frm" class="modal-trigger right">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><?php */?>
                        </div>
                      </div>

                      <h4 class="box-inner-title">Bank Details<span class="required_field">*</span>
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Enter Bank Details  <small class="badge-cover tot_bank">0</small>
                                      <a href="#addbranch" class="right modal-trigger">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            <div  class="input-field" id="company_bank_list">
                            </div>
                            </div>

                          </div>

                            <div class="row">
                              <div class="col l12 s12 m12 text-center">
                                <a onclick="showMoredata('bankmore','showbankboxs')" id="showbankboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>

                                <!-- <a class="showmore-row cursor"><i data-id="company_bank_list" class="material-icons">keyboard_arrow_down</i></a> -->
                              </div>
                            </div>
                        </div>
                      </div>

					  <h4 class="box-inner-title">Debit & Credit Card details</h4>

                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Debit & Credit Card details <small class="badge-cover tot_bank">0</small>
                                      <a href="#add-card" class="right modal-trigger">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            <div  class="input-field" id="company_card_list">
                            </div>
                            </div>
                          </div>
                          <div class="row">
							<div class="col l12 s12 m12 text-center">
								<a onclick="showMoredata('bankmore','showbankboxs')" id="showbankboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
							</div>
						  </div>
                        </div>
                      </div>

					  <a data-html="true" style="text-align:center;" class="info-ref tooltipped info-tooltipped" data-position="top" data-delay="50" data-tooltip="<p>1. You can integrate with our third-party payment gateway to add 'Make Payment' </br> in your invoice so that your clients can pay you directly with a click. </p> <p style='text-align:left;'>2. You will incur a fee of 2% + Rs 3 for every transaction carried out.</p>"></a>
                      <div style="margin-top:10px !important;" class="box-wrapper bg-white lable-badge shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col l12 m12 s12">
                                  <div class="long-lable">
                                  <a href='#instamojo_modal' data-toggle='modal' class='modal-trigger modal-close' style="color: #fff;"> Integrate payment Gateway <i class="material-icons right">chevron_right</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="api_key" id ="api_key">
                      <input type="hidden" name="auth_token" id ="auth_token">

                      <h4 style="margin-top:5%;" class="box-inner-title afetr-bg-title">Customization
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                    <label for="fy_start_date" class="full-bg-label green-txt">Financial Year Start Date<span class="required_field">*</span></label>
                                    <input id="fy_start_date" name="bus_fy_startdate" class="icon-calendar-green rangedatepicker_customize full-bg adjust-width border-top-none" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                    <label for="fy_end_date" class="full-bg-label red-txt">Financial Year End Date<span class="required_field">*</span></label>
                                    <input id="fy_end_date" name="bus_fy_enddate" class="icon-calendar-red rangedatepicker_customize full-bg adjust-width border-top-none" type="text" autocomplete="off" readonly>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set" id="currencysec">
                                <div class="input-field label-active">
                                  <label for="currency" class="full-bg-label select-label">Select Currency<span class="required_field">*</span></label>
                                  <select class="js-example-basic-single country-dropdown check-label" name="bus_currency_format" id="currency">
                                  <option value="">SELECT CURRENCY<span class="required_field">*</span></option>
                                  <?php foreach ($currency as $type) { ?>
                                    <option value="<?php echo $type->currency_id; ?>"><?php echo strtoupper($type->currency." (".$type->currencycode.")"); ?></option>
                                    <?php } ?>
                                 </select>
                                </div>

								<!--<div class="col s12 m12 l6 input-set border-field" id="citysec">
                              <div class="input-field">
                                <label for="city" class="full-bg-label select-label">City<span class="required_field"></span></label>
                                  <select class="js-example-basic-single" name="bus_billing_city" id="city">
                                   <option value="">CITY *</option>
                                  </select>
                              </div>
                            </div>-->


                              </div>
							    <div class="col s12 m12 l6 input-set">
                                <div class="input-field">
                                  <label for="date_format" class="full-bg-label select-label">Select Date Format<span class="required_field">*</span></label>
								    <select id="date_format" name="bus_dateformat" class="js-example-basic-single country-dropdown check-label">
                                    <option value="">SELECT DATE FORMAT<span class="required_field">*</span></option>
                                    <option value="d/m/Y">dd/mm/yyyy</option>
									<option value="m-d-Y">mm-dd-yyyy</option>
                                    <option value="Y-m-d">yyyy-mm-dd</option>
							        </select>
                                </div>
                              </div>

                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
						        <div class="col s12 m12 l6 border-field input-set" id="timezonesec">
                                <div class="input-field">
                                <label for="select_currency" class="full-bg-label select-label">Select TimeZone<span class="required_field">*</span></label>
                                 <select class="js-example-basic-single" name="bus_timezone" id="timezone">
                                  <option value="">SELECT TIMEZONE<span class="required_field">*</span></option>
              										<option value="GMT-12:00">GMT -12:00</option>
                                  <option value="GMT-11:00">GMT -11:00</option>
                                  <option value="GMT-10:00">GMT -10:00</option>
                                  <option value="GMT-09:00">GMT -09:00</option>
                                  <option value="GMT-08:00">GMT -08:00</option>
                                  <option value="GMT-07:00">GMT -07:00</option>
                                  <option value="GMT-06:00">GMT -06:00</option>
                                  <option value="GMT-05:00">GMT -05:00</option>
                                  <option value="GMT-04:00">GMT -04:00</option>
                                  <option value="GMT-03:30">GMT -03:30</option>
                                  <option value="GMT-03:00">GMT -03:00</option>
                                  <option value="GMT-02:00">GMT -02:00</option>
                                  <option value="GMT-01:00">GMT -01:00</option>
                                  <option value="GMT+00:00">GMT +00:00</option>
                                  <option value="GMT+01:00">GMT +01:00</option>
                                  <option value="GMT+02:00">GMT +02:00</option>
                                  <option value="GMT+03:00">GMT +03:00</option>
                                  <option value="GMT+03:30">GMT +03:30</option>
                                  <option value="GMT+04:00">GMT +04:00</option>
                                  <option value="GMT+04:30">GMT +04:30</option>
                                  <option value="GMT+05:00">GMT +05:00</option>
                                  <option value="GMT+05:30">GMT +05:30</option>
                                  <option value="GMT+05:45">GMT +05:45</option>
                                  <option value="GMT+06:00">GMT +06:00</option>
                                  <option value="GMT+06:30">GMT +06:30</option>
                                  <option value="GMT+07:00">GMT +07:00</option>
                                  <option value="GMT+08:00">GMT +08:00</option>
                                  <option value="GMT+09:00">GMT +09:00</option>
                                  <option value="GMT+10:00">GMT +10:00</option>
                                  <option value="GMT+11:00">GMT +11:00</option>
                                  <option value="GMT+12:00">GMT +12:00</option>
                                  <option value="GMT+13:00">GMT +13:00</option>
              									  </select>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 border-field input-set">
                                <div class="row border-bottom"><!--style="margin-top:18px !important; width:98%;"-->
								  <div class="col s12 m12 l12" style="margin-top:18px !important; width:98%;">
									  <div class="input-field">
										<!--label for="decimal_points" style="margin-left:-10px !important; margin-top:-24px !important; font-size:11px !important;" class="full-bg-label">Set Decimal Points</label-->
										<select class="js-example-basic-single country-dropdown check-label dec_pt" name="bus_decimal_point" id="decimal_points">
										<option value="">SELECT DECIMAL POINT<span class="required_field">*</span></option>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										</select>
										<!--<input style="margin-left:-11px !important; padding-right:40px !important;" id="decimal_points" name="bus_decimal_point" class="full-bg adjust-width" type="text" value="0">-->
									  </div>
								  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>



                    <h4 class="box-inner-title">Company Information</h4>
                      <div class="box-wrapper bg-white bg-img-green-after shadow border-radius-6">
                        <div class="box-body">

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set" id="companytypesec">
                                <div class="input-field">
                                 <label for="company_type" class="full-bg-label select-label">Type of Company<span class="required_field"> *</span></label>
                                 <select class="js-example-basic-single" name="bus_company_type" id="company_type">
                                  <option value="">TYPE OF COMPANY *</option>
                                  <?php foreach ($company_type as $type) { ?>
                                      <option value="<?php echo $type->id; ?>"><?php echo strtoupper($type->company_type); ?></option>
                                   <?php } ?>
                                 </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set border-right-none">
                                <div class="row">
                                  <div class="input-field">
                                      <label for="incorporation" class="full-bg-label">Date of incorporation<span class="required_field">*</span></label>
                                      <input id="incorporation" name="bus_incorporation_date" class="full-bg icon-calendar-gray adjust-width border-top-none bdatepicker bdatepicker_inco" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

						  <div class="row">
                            <div class="col s12 m12 l12">

							  <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label for="opening_cash_balance" class="full-bg-label">Opening Cash Balance <span class="required_field">*</span></label>
                                    <input id="opening_cash_balance" name="opening_cash_balance" value="" class="full-bg adjust-width border-bottom-none numeric_number" type="text" autocomplete="off" >
                                  </div>
                                </div>
                              </div>

							  <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label style="padding-left:20px !important;" for="cash_balance_date" class="full-bg-label">Date of Opening Cash Bal<span class="required_field">*</span></label>
									<input id="" name="cash_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" value="" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>

						  <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label for="opening_petty_balance" class="full-bg-label">Opening Petty Cash Balance <span class="required_field">*</span></label>
                                    <input id="opening_petty_balance" name="opening_petty_balance" value="" class="full-bg adjust-width border-bottom-none numeric_number" type="text" autocomplete="off" >
                                  </div>
                                </div>
                              </div>

							  <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label style="padding-left:20px !important;" for="petty_balance_date" class="full-bg-label">Opening Date Petty Cash Bal<span style="margin-left:0px;" class="required_field">*</span></label>
									<input id="petty_balance_date" name="petty_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" value="" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 l12 m12">
                              <div class="col s12 m12 l6 input-set border-field border-bottom">
                                <div class="row">
                                  <div class="input-field">
                                    <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip="Fill this to get better insights in the Business Analytics module" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                      <label for="total_employee" class="full-bg-label">No. Of Employees</label>
                                       <input id="total_employee" name="bus_company_size" class="full-bg adjust-width border-bottom-none" min="0" step="1" type="number">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set border-field border-right-none border-bottom" id="revenuesec">
                                <div class="input-field" style="margin-bottom:2px !important;">
                                  <label for="revenue" class="full-bg-label select-label">REVENUE </label>
                                   <select class="js-example-basic-single" name="bus_company_revenue" id="revenue">
                                    <option value="">REVENUE</option>
                                      <option value="5">&lt; 5 CRORES</option>
                                      <option value="5-20">5-20 CRORES</option>
                                      <option value="20-50">20-50 CRORES</option>
                                      <option value="50-100">50-100 CRORES</option>
                                      <option value="100+">100 CRORES+</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l12 input-set label-active" id="companytypesec">
                                <div class="input-field" style="height:65px !important; padding-top:7px !important; margin-top:15px !important;">
                                 <!--<label for="nature_of_bus" class="full-bg-label select-label">Nature of Business<span class="required_field">*</span></label>-->
                                  <select class="js-example-basic-single com-nature" name="nature_of_bus" id="nature_of_bus" style="margin-top:10px !important;">
                                  <option value="">NATURE OF BUSINESS*</option>

                                  <option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
                                  <option value="Advertising">ADVERTISING</option>
                                  <option value="Animation Studio">ANIMATION STUDIO</option>
                                  <option value="Architecture">ARCHITECTURE</option>
                                  <option value="Arts & Crafts">ARTS & CRAFTS</option>
                                  <option value="Audit & Tax">AUDIT & TAX</option>
                                  <option value="Brand Consulting">BRAND CONSULTING</option>
                                  <option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
                                  <option value="Consultant">CONSULTANT</option>
                                  <option value="Content Studio">CONTENT STUDIO</option>
                                  <option value="Cyber Security">CYBER SECURITY</option>
                                  <option value="Data Analytics">DATA ANALYTICS</option>
                                  <option value="Digital Influencer">DIGITAL UNFLUENCER</option>
                                  <option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
                                  <option value="Direct Marketing">DIRECT MARKETING</option>
                                  <option value="Entertainment">ENTERTAINMENT</option>
                                  <option value="Event Planning">EVENT PLANNING</option>
                                  <option value="Florist">FLORIST</option>
                                  <option value="Foreign Exchange">FOREIGN EXCHANGE</option>
                                  <option value="Financial and Banking">FINANCIAL & BANKING</option>
                                  <option value="Gaming Studio">GAMING STUDIO</option>
                                  <option value="DESIGN & UI/UX">DESIGN & UI / UX</option>
                                  <option value="Hardware Servicing">HARDWARE SERVICING</option>
                                  <option value="Industry Bodies">INDUSTRY BODIES</option>
                                  <option value="Insurance">INSURANCE</option>
                                  <option value="Interior Designing ">INTERIOR DESIGNING</option>
                                  <option value="Legal Firm">LEGAL FIRM</option>
                                  <option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
                                  <option value="Mobile Services">MOBILE SERVICES</option>
                                  <option value="Music">MUSIC</option>
                                  <option value="Non-Profit">NON-PROFIT</option>
                                  <option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
                                  <option value="Photography">PHOTOGRAPHY</option>
                                  <option value="Printing">PRINTING</option>
                                  <option value="Production Studio">PRODUCTION STUDIO</option>
                                  <option value="PR / Image Management">PR/ IMAGE MANAGEMENT</option>
                                  <option value="Publishing">PUBLISHING</option>
                                  <option value="Real Estate">REAL ESTATE</option>
                                  <option value="Recording Studio">RECORDING STUDIO</option>
                                  <option value="Research">RESEARCH</option>
                                  <option value="Sales Promotion">SALES PROMOTION</option>
                                  <option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
                                  <option value="Stock & Shares">STOCK & SHARES</option>
                                  <option value="Technology (AI, AR, VR)">TECHNOLOGY(AI, AR, VR)</option>
                                  <option value="Tours & Travel">TOURS & TRAVEL</option>
                                  <option value="Training & Coaching">TRAINING & COACHING</option>
                                  <option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
                                  <option value="Therapists">THERAPISTS</option>
								  								<option value="Utilities">UTILITIES</option>
                                  <option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
                                  <option value="Web Development">WEB DEVELOPMENT</option>
                                </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                      <!-- <label for="incorporation" class="full-bg-label">Date of incorporation</label>
                                      <input id="incorporation" name="bus_incorporation_date" class="full-bg icon-calendar-gray adjust-width border-top-none bdatepicker_inco" type="text" value="<?php //echo ($row->bus_incorporation_date != '' && $row->bus_incorporation_date != '0000-00-00')?date("d/m/Y",  strtotime($row->bus_incorporation_date)):''; ?>"> -->
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row ">
                            <div class="col s12 l12 m12 input-set border-field"  >
                              <div class="input-field emty-field-row border-field ">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Areas of Expertise <small class="badge-cover tot_service">0</small>

                                      <a class="right modal-trigger" type="button" href="#add-new-service">
                                        <img src="<?php echo base_url();?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>

                                </div>

                              </div>

                            </div>

                          </div>
               <div  id="company_keywords_array_list">

              </div>
               <div class="row">
                                <div class="col l12 s12 m12 text-center" >
                                  <a class="showmore-row cursor" id="cmp_keyboard"><i data-id="company_keywords_array_list" class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                          </div>
          <div class="active-show">
                              <!--<div class="col s12 m12 l12 input-set border-field">
                                <div class="row">
                                  <div class="input-field legal">
                                    <p class="gstin">Add Branch Office <small class="gray-box two-i tot_branch">0</small></p>
                                    <a href="#new-branch" class="modal-trigger"><img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="pluse-icon"></a>
                                  </div>
                                </div>
                                <div class="row" id="company_branch_array_list">

                                </div>
                              </div>
                              <div class="row">
                               <div class="col l12 s12 m12 text-center">
                                <a onclick="showMoredata('branchmore','showbranchboxs')" id="showbranchboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                              </div> -->
                             <!--  <div class="row">
                                <div class="col l12 s12 m12 text-center">
                                  <a onclick="showGstMore()" id="showgstboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                              </div> -->
                              </div>


                          <?php /*?><div class="row">
                            <div class="col s12 l12 m12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Add Branch <!-- <small class="badge-cover">03</small> -->
                                      <a class="right modal-trigger" type="button" href="#new-branch">
                                        <img src="<?php echo base_url(); ?>/asset/css/img/icons/pluse.png" class="">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div><?php */?>

                        </div>
                      </div>



                      <h4 class="box-inner-title">Web Presence
                        <!--<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix web"></i>
                                    <label for="website" class="full-bg-label">Add Website URL</label>
                                    <input id="website_url" name="bus_website_url" class="full-bg adjust-width border-top-none autofill_url" type="text">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix client"></i>
                                    <label for="website" class="full-bg-label">Add Client List URL</label>
                                    <input id="list_of_clients" name="bus_loc" class="full-bg adjust-width border-top-none autofill_url" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l12 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix study"></i>
                                    <label for="website" class="full-bg-label">Add Case Studies URL</label>
                                    <input id="case_study" name="bus_case_study" class="full-bg adjust-width border-top-none autofill_url" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <h4 class="box-inner-title">Social Media
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix facebook"></i>
                                    <label for="website" class="full-bg-label">Add Facebook URL</label>
                                    <input id="facebook" name="bus_facebook" class="full-bg adjust-width border-top-none autofacebook_url" type="text">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix twitter"></i>
                                    <label for="website" class="full-bg-label">Add Twitter URL</label>
                                    <input id="twitter" name="bus_twitter" class="full-bg adjust-width border-top-none autotwitter_url" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix linkedin"></i>
                                    <label for="website" class="full-bg-label">Add Linkedin URL</label>
                                    <input id="linkedin" name="bus_linkedin" class="full-bg adjust-width border-top-none autolinkedin_url" type="text">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix instagram"></i>
                                    <label for="website" class="full-bg-label">Add Instagram URL</label>
                                    <input id="instagram" name="bus_instagram" class="full-bg adjust-width border-top-none autoinstagram_url" type="text">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix youtube"></i>
                                    <label for="website" class="full-bg-label">Add Youtube URL</label>
                                    <input id="youtube" name="bus_youtube" class="full-bg adjust-width border-top-none autoyoutube_url" type="text">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix whatsapp"></i>
                                    <label for="website" class="full-bg-label">Add Whatsapp Number</label>
                                    <input id="googleplus" name="bus_googleplus" class="full-bg adjust-width border-top-none autogoogleplus_url" type="number">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

					  <h4 class="box-inner-title">Document Locker <span style="font-size:13px !important; color:#FA7C9B !important;">(Only PDF & PNG Format. Upto 25MB)</span> <a class="sac info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Upload company documents like MoA, Incorporation Certificate and set reminders for their renewal."></a></h4>
                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row docrow">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs  ">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Legal Document (MoA)</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload your legal & tax documents
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2">
                                  <div class="clold">
                                     <input type="file" name="Legaldocuments[]" id="Legaldocuments" multiple class="docup">
                                  </div>
                                </div>
                              </div>
                              <div id="Legaldocumentscover"></div>

							  <div id="legel_doc" style="width:970px; margin-left:-200px; margin-right:-100px;">
							  </div>
                              <div class="row">
                                <div class="col s12 m12 l2">
                                  <div class="col s12 m12 l2">
                                    <div class="line"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs ">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Company Documents (Certificate of Incorporation)</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload your company documents
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2">
                                  <div class="clold">
                                    <input type="file" name="purchaseorderdoc[]" id="purchaseorderdoc" class="docup" multiple accept="application/pdf">
                                  </div>
									</div>
                                <div class="col s12 m12 l12">
                                    <div id="purchaseorderdoccover"></div>
                                </div>
								<div id="pur_order" style="width:970px; margin-left:-200px; margin-right:-100px;">

								</div>
                                <div class="col s12 m12 l12">
                                  <div class="line"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Upload Multiple Other Documents (GST, Pancard)</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload other documents(Rent agreement, Contracts, etc.)
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2" id="other_doc_file">
                                      <div class="clold"  id="other_doc0">
                                        <input type="file" name="otherdoc[]" id="otherdoc0" class="docup otherdoc" data-valId="0" multiple>
                                      </div>
                                    </div>
                              </div>
                              <div id="otherdoccover">

                              </div>
                            </div>
                          </div>
                          <div class="row" hidden>
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Registration Document</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload MOAs, MOUs, etc.
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2" id="memorandum_doc_file">
                                  <div class="clold" id="memorandum_doc0">
                                    <input type="file" id="memorandum0" name="memorandum[]" class="docup memorandum" data-valId="0" multiple="" accept="application/pdf">
                                  </div>
                                </div>
                              </div>
                              <div id="memorandumcover">

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                    <!--<div class="step1 footer-btns">
                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="text-center"><i class="material-icons know-more-form" onclick="showStpe('.step2','.step1')">keyboard_arrow_down</i></div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="form-botom-divider"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s12 m12 l6 right">
                          <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right ml-5"  type="button" onclick="location.href = '<?php //echo base_url();?>profile/company_profile';">Cancel</button>
                          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                        </div>
                      </div>
                    </div>-->
                  </div>
                  <div class="col s12 m12 l3"></div>
                </div>
              </div>
            </div>
			<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
		  <div class="row"><div class="col s12 m12 l12"><p></p></div></div>
		  <div class="row"><div class="col s12 m12 l12"><p></p></div></div>
          </section>
          <div class="footer-btns last-sec" style="margin-top:-60px !important;">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <div class="col s12 m3 l4"></div>
                <div class="col s12 m12 l4">
                  <i class="material-icons know-more-form" onclick="showStpe('.step1','.step2')">keyboard_arrow_up</i>
                </div>
                <div class="col s12 m12 l4">
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>profile/company_profile';">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!-- END CONTENT -->

        </div>

        <!-- END WRAPPER -->

      </div>
 <div id="add_cmp_gst_frm" class="modal add_gst_modal modal-md" >
  <style>
	#add_cmp_gst_frm .select2-container--default .select2-selection--single {
		background-color: transparent !important;
	}

	#add_cmp_gst_frm .select2-container--default .select2-selection--single .select2-selection__rendered {
		font-size: 14px !important;
		line-height: 35px;
		color: #000 !important;
		font-weight: 500 !important;
		padding-top:10px;
	}
  </style>
  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
  <div class="modalheader">
    <h4>ADD NEW GSTIN</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>
  <div class="modalbody">
    <form class="addgstin" name="add_gst_from" id="add_gst_from" method="post">

      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="It's a state-wise, PAN-based, 15-digit Goods and Services Taxpayer Identification Number (GSTIN)"></a>
                <label class="full-bg-label active">GSTIN</label>
                <input type="hidden" name="gst_array[]" value="1" />
                <input name="gstin1" id="gstin1" class="full-bg adjust-width gstin statecode_gst" type="text" title="Enter GSTIN in a valid format">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">

                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="It is a destination where the services are consumed and hence determines whther IGST or CGST/SGST tax will be levied"></a>
                <label class="full-bg-label active">PLACE OF SUPPLY</label>
                <input id="location1" name="location1" class="full-bg adjust-width js-typeahead" type="text" autocomplete="off">
                <input id="statecode1" name="statecode1" class="full-bg adjust-width js-typeahead" type="hidden">
                <div id="suggesstion-box_location1"></div>
              </div>
            </div>


          <!--<div class="col l2 s12 m2 fieldset">
            <a class="add-remove-btn add-icon addgstrow"><i class="material-icons add-bloue">add</i></a>
          </div>-->


            </div>
             <!--div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set border-field" id="citysec">
                              <div class="input-field" style="margin-left:-22px !important; width:245px !important;">
                               <label for="email1" id="email_id" class="full-bg-label">Email<span class="required_field"></span></label>
                                    <input id="email1" name="bus_email_id1" class="full-bg adjust-width" type="text">
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-field border-right-none">
                              <div class="row">
                                <div class="input-field">
                                    <label for="contact1" id="contact_no" class="full-bg-label">Contact No.<span class="required_field"></span></label>
                                    <input id="contact1" name="bus_billing_contact1" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div-->


                  <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field" style="width:490px !important; margin-left:-10px !important;">
                                <label for="billing_address1" class="full-bg-label">BILLING ADDRESS<span class="required_field">* </span></label>
                                <input id="billing_address1" name="bus_billing_address1" class="full-bg adjust-width border-top-none billing_add" type="text" value="">
                              </div>
                            </div>
                          </div>

                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set  border-field border-bottom">
                               <div class="row" style="border-left:none !important;">
                              <div class="input-field" style="width:210px !important; margin-left:-12px !important; height:70px !important;">
                               <label for="country1" class="full-bg-label select-label">Country<span class="required_field"> </span></label>
                                  <select class="js-example-basic-single country-dropdown check-label add_gst_con" name="bus_billing_country1" id="country1">
                                    <option value=""></option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                                     <?php } ?>
                                  </select>
                              </div>
                            </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-right-none" id="statesec" style="margin-top:-10px !important;">
                               <div class="row" style="margin-left:-12px !important; border-left:none !important;">
                              <div class="input-field" style="width:215px !important; margin-left:15px !important; margin-top:10px !important;">
                                <label for="state" class="full-bg-label select-label state_label">State<span class="required_field"></span></label>
                                  <select class="js-example-basic-single country-dropdown check-label add_gst_sta" name="bus_billing_state1" id="state1">
                                     <option value=""></option>
                                  </select>
                              </div>
                            </div>
                            </div>
                          </div>
                        </div>
                      <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set border-bottom" style="margin-top:-10px !important;">
                               <div class="row" style="margin-left:-12px !important; border-left:none !important;">
                              <div class="input-field" style="width:207px !important; margin-left:1px !important; margin-top:10px !important; height:74px !important;">
                                <label for="city" class="full-bg-label select-label state_label">city<span class="required_field"></span></label>
                                  <select class="js-example-basic-single country-dropdown check-label add_gst_ci" name="bus_billing_city1" id="city1">
                                     <option value=""></option>
                                  </select>
                              </div>
                            </div>
                            </div>


                            <div class="col s12 m12 l6 input-set border-field border-right-none">
                              <div class="row" style="margin-left:-10px !important;">
                                <div class="input-field">
                                    <label for="zipcode" id="pin_code" class="full-bg-label">PIN Code<span class="required_field"></span></label>
                                    <input id="pincode1" name="bus_billing_zipcode1" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set border-field" id="citysec">
                              <div class="input-field">
                               <label for="email" id="email_id" class="full-bg-label">Email<span class="required_field"></span></label>
                                    <input id="email1" name="bus_email_id1" class="full-bg adjust-width" type="text">
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-field border-right-none">
                              <div class="row">
                                <div class="input-field">
                                    <label for="contact" id="contact_no" class="full-bg-label">Contact No.<span class="required_field"></span></label>
                                    <input id="contact1" name="bus_billing_contact1" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>-->

        </div>
       <!--  <div class="col l2 s2 m2 fieldset" style="display: none;" >
          <div class="col l12 s12 m12 fieldset">
            <a class="add-remove-btn"><i class="material-icons  rem-red">remove</i></a>
          </div>
        </div> -->




      </div>



       <div id="rep-element">
      </div>
      <!-- <div class="row">
        <div class="col l10 s10 m10 fieldset empty-add-set"></div>
        <div class="col l2 s2 m2 fieldset">
          <div class="col l12 s12 m12 fieldset">
            <a class="add-remove-btn add-icon addgstrow"><i class="material-icons add-bloue">add</i></a>
          </div>
        </div>
      </div> -->
      <div class="row">
        <!--<div class="col l12 m12 s12 state-sugestion">
          * Please select state name from sugested list
        </div>-->
        <div class="col l6 m6 s12 fieldset">

        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
	<div id="remove_company_gst_data" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Deactivate GSTIN Information </h4>

      <input type="hidden" id="gst_no" name="gst_no" value="" />

	 	<input type="hidden" id="location" name="location" value="" />
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this GSTIN?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_company_gst_data">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_branch_data" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Remove Branch Information </h4>

      <input type="hidden" id="remove_branch" name="remove_branch" value="" />

	 	<input type="hidden" id="remove_city" name="remove_city" value="" />
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to remove this Branch Information?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_company_branch_data">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_bank_modal" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Deactivate Bank Information </h4>

      <input type="hidden" id="remove_ac_number" name="remove_ac_number" value="" />
      <input type="hidden" id="remove_cbank_name" name="remove_cbank_name" value="" />
      <input type="hidden" id="remove_cbank_branch_name" name="remove_cbank_branch_name" value="" />
      <input type="hidden" id="remove_account_type" name="remove_account_type" value="" />
      <input type="hidden" id="remove_ifsc_code" name="remove_ifsc_code" value="" />
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this bank information?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_bank_details">DEACTIVATE</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_card_modal" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Deactivate CARD Information </h4>


      <input type="hidden" id="remove_bank_name" name="remove_bank_name" value="" />
      <input type="hidden" id="remove_card_type" name="remove_card_type" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this card information?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_card_details">DEACTIVATE</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="add-new-service" class="modal modal-md" style="max-height: 100%;">
  <div class="modalheader">
    <h4>Add Areas of Expertise</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_new_service_keywords" name="add_new_service_keywords" method="post">
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 1</label>
                <input name="keyword_1" id="keyword_1" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 2</label>
                <input id="keyword_2" name="keyword_2" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 3</label>
                <input name="keyword_3" id="keyword_3" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 4</label>
                <input id="keyword_4" name="keyword_4" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 5</label>
                <input name="keyword_5" id="keyword_5" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 6</label>
                <input id="keyword_6" name="keyword_6" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 7</label>
                <input name="keyword_7" id="keyword_7" class="full-bg adjust-width gstin" type="text">

              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 8</label>
                <input id="keyword_8" name="keyword_8" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 9</label>
                <input name="keyword_9" id="keyword_9" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 10</label>
                <input id="keyword_10" name="keyword_10" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>

      </div>
      <div id="rep-element">
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<!--<div id="new-branch" class="modal modal-md">
  <div class="modalheader">
    <h4>Add new Branch Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_branch_info" name="add_branch_info">
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="For eg. North, Central, Mid West, East etc." data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>
                <label class="full-bg-label">ENTER REGION</label>
                <input name="branch_name" id="branch_name" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Regional office city name." data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>
                <label class="full-bg-label">CITY</label>
                <input id="branch_city" name="branch_city" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
       </div>
      <div id="rep-element">
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>-->
<!-- <div id="
" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Service Info</h4>

      <input type="hidden" id="remove_cmp_service_id" name="remove_cmp_service_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this Service Info?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea remove_cmp_servic">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div> -->
<div id="remove_cmp_service" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Service Info</h4>

      <input type="hidden" id="remove_cmp_service_id" name="remove_cmp_service_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to remove this Service Info?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea remove_sess_service remove_cmp_servic">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="instamojo_modal" class="modal modal-md ps-active-y" style="height:90% !important; margin-top:-40px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <a style="margin-right:-5px; margin-top:5px;" class="modal-close close-pop" style="">
      <img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
          <h4 class="modal-title">Instamojo Payment Integration</h4>
        </div>
        <div class="modal-body" style="margin:0px 0 0 0 !important;">
    <div class="row" style="margin-top:8px;">
      <label style="color:#000066;"><i> Instruction :</i></label>
      <i><ol style="margin-left:-24px;">
        <li style="color:#000066; font-size:13px;"><p>Sign up on <a style="color:#000066;" href="http://instamojo.com" ><b>http://instamojo.com</b></a> if you don't already have an account here.</p></li>
        <li style="color:#000066; font-size:13px;"><p>After registration, enter the <b> Private API Key </b> and <b> Private Authorization Token </b>that you will find in your profile.</p></li>
        <li style="color:#000066; font-size:13px;"><p>Post submission, your Instamojo payment gateway will be linked with <b>Xebra</b> making it possible for you to integrate it in your invoices and make it easier for your clients to pay you directly.</p></li>
      </ol></i>
    </div>
    <div class="row" style="text-align:center;">
      <img class="" height="200" width="400" src="<?php echo base_url(); ?>asset/images/insta_view.png" alt="insta-view">
    </div>
    <form id='insta_form_add' style="margin-top:10px; margin-bottom:20px;">
          <div class="row">
      <div class="col s12 m12 l12" style="text-align:center;">
        <a href='https://www.instamojo.com/?referrer=client&partner_id=c995c1673ee8400d8df23fa941bd7858' target="_blank" class="btn-flat theme-primary-btn theme-btn theme-btn-large" type="button" style="background-color:#000066 !important;">SIGN UP ON INSTAMOJO</a>
      </div>
      <div class="col s12 m12 l12" style="text-align:center; margin-top:5px; font-size:13px;"><span style="color:#000066;"><b>OR</b></span></div>
      <div class="col s12 m12 l12" style="text-align:center; margin-top:5px; font-size:13px;"><span style="color:#000066;"><b>IF YOU ALREADY HAVE AN ACCOUNT</b></span></div>
      <div class="col s12 m12 l12" style="margin-top:5px; margin-left:-34px; width:511px;">
        <div class="input-field">
                      <label class="full-bg-label" style='margin:10px 0 !important; font-size:13px;'>ENTER PRIVATE API KEY</label>
                      <input style='height: 2rem !important;' name="api_key_add" id="api_key_add" value="" class="full-bg adjust-width border-right" type="text">
                </div>
        <div class="input-field">
                      <label class="full-bg-label" style='margin:10px 0 !important; font-size:13px;'>ENTER PRIVATE AUTHENTICATION TOKEN</label>
                      <input style='height: 2rem !important;' name="auth_token_add" id="auth_token_add" value="" class="full-bg adjust-width border-right" type="text">
                </div>
        <input type='text' id='bus_id' name='bus_id' value='' hidden></input>
      </div>
      </div>
     <!--  <a id="custmise_fields" href="#instamojo_success_modal" class="cur customize_cfields modal-trigger modal-close">fdd</a> -->
      <p id="insta_note"><i>NOTE : Instamojo is a third party site and we recommend that you read and understand their Terms and Conditions and Privacy Policy</i></p>
      <div class="row" style="float:right; margin-top:10px;">
      <button type="button" id="insta_close" class="btn btn-flat theme-primary-btn theme-btn theme-btn-large modal-close" data-dismiss="modal">CANCEL</button>
      <input id='insta_submit' class="btn btn-flat theme-primary-btn theme-btn theme-btn-large" type="submit" style='margin-right:5px;' value="SAVE" />
      </div>
    </form>
    </div>
      </div>

    </div>
</div>

<div id="welcome_inv" class="modal modal-md" style="top:8%; left:10%; max-height: 100%; max-width:385px !important;">
	<style>
	/* Slider START */
	#welcome_inv .mySlides {display: none}
	img {vertical-align: middle;}

	/* Slideshow container */
	#welcome_inv .slideshow-container{
	  max-width: 1000px;
	  position: relative;
	  margin: auto;
	}

	/* Next & previous buttons */
	#welcome_inv .prev, .next {
	  cursor: pointer;
	  position: absolute;
	  top: 50%;
	  width: auto;
	  padding: 16px;
	  margin-top: -22px;
	  color: white;
	  font-weight: bold;
	  font-size: 18px;
	  transition: 0.6s ease;
	  border-radius: 0 3px 3px 0;
	  user-select: none;
	}

	/* Position the "next button" to the right */
	#welcome_inv #open {
	  right: 0;
	  top:103%;
	  border-radius: 3px 0 0 3px;
	  color:#000;
	}

	#welcome_inv #close {
	  right: 0;
	  top:114%;
	  border-radius: 3px 0 0 3px;
	  color:#000;
	}

	/* On hover, add a black background color with a little bit see-through
	#welcome_inv .prev:hover, .next:hover {
	  background-color: rgba(0,0,0,0.8);
	}*/

	/* Caption text */
	#welcome_inv .text {
	  color: #f2f2f2;
	  font-size: 15px;
	  padding: 8px 12px;
	  position: absolute;
	  bottom: 8px;
	  width: 100%;
	  text-align: center;
	}

	/* Number text (1/3 etc) */
	#welcome_inv .numbertext {
	  color: #f2f2f2;
	  font-size: 12px;
	  padding: 8px 12px;
	  position: absolute;
	  top: 0;
	}

	/* The dots/bullets/indicators */
	#welcome_inv .dot {
	  cursor: pointer;
	  height: 15px;
	  width: 15px;
	  margin: 0 2px;
	  border: 1px solid #bbb;
	  border-radius: 50%;
	  display: inline-block;
	  transition: background-color 0.6s ease;
	}

	#welcome_inv .active, .dot:hover {
	  background-color: #717171;
	}

	/* Fading animation */
	#welcome_inv .fade {
	  -webkit-animation-name: fade;
	  -webkit-animation-duration: 1.5s;
	  animation-name: fade;
	  animation-duration: 1.5s;
	}

	@-webkit-keyframes fade {
	  from {opacity: .4}
	  to {opacity: 1}
	}

	@keyframes fade {
	  from {opacity: .4}
	  to {opacity: 1}
	}

	/* On smaller screens, decrease text size */
	@media only screen and (max-width: 300px) {
	  .prev, .next,.text {font-size: 11px}
	}
	/* Slider END */
	#welcome_inv .welcom-container{
		background-color:#fff;
		border-radius:8px;
		height:465px;
		margin-top:0px !important;
	}

	#welcome_inv .welcom-title{
		font-size:20px;
		color:#7864e9;
		text-align:center !important;
	}

	#welcome_inv .bottom-row{
		padding:12px 40px;
		text-align:justify;
	}

	#welcome_inv .bottom-text{
		font-size:16px;
		color:#7864e9;
	}
	#welcome_inv .container{
		margin:8% 0;
	}
	</style>
	<div class="modalbody">
		<form class="addgstin" id="edit_new_cards_ext" name="edit_new_cards_ext" method="post">
			<div class="welcom-container">
					<!-- Carousal Start -->
					<div class="slideshow-container">
					<div class="mySlides fade">
						<div class="row">
						<div class="gif-img" style="float:left; padding: 4px 0 0 4px;">
							<img width="90" height="40" src="<?php echo base_url(); ?>public/images/top1.png" class="gifblank" alt="nxt"></a>
						</div>
						<div class="gif-img" style="float:right; padding: 4px 4px 0 0;">
							<img width="90" height="40" src="<?php echo base_url(); ?>public/images/top2.png" class="gifblank" alt="nxt"></a>
						</div>
						</div>
						<div class="welcom-content">
							<p class="welcom-title" style="margin-top:0px !important;">Welcome <?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></p>
						</div>
						<div class="Welcome-image">
							<img width="200" height="200" src="<?php echo base_url(); ?>asset/css/img/icons/blank-stage/ei-blank1.gif" class="gifblank" alt="ei-blank">
						</div>
						<div class="bottom-row">
							<p class="bottom-text"><strong>We understand that each business is different and we would like to offer you a customised product that can work for your business better</strong></p>
						</div>
					</div>

					<div class="mySlides fade">
						<div class="row">
						<div class="gif-img" style="float:left; padding: 4px 0 0 4px;">
							<img width="90" height="40" src="<?php echo base_url(); ?>public/images/top1.png" class="gifblank" alt="nxt"></a>
						</div>
						<div class="gif-img" style="float:right; padding: 4px 4px 0 0;">
							<img width="90" height="40" src="<?php echo base_url(); ?>public/images/top2.png" class="gifblank" alt="nxt"></a>
						</div>
						</div>
						<div class="Welcome-image">
							<img width="280" height="200" src="<?php echo base_url(); ?>public/images/Second-icon.png" class="gifblank" alt="ei-blank">
						</div>
						<div class="bottom-row" style="margin-top:-27px;">
							<p class="bottom-text" style="padding-bottom:10px;"><strong>Select My Company Profile from the top right dropdown to fill in your company and business details</strong></p>
							<p style="margin-top:-8px;" class="bottom-text"><strong>Then click on My Personal Profile to give out selective access of Xebra to your team</strong></p>
						</div>
					</div>

					<div class="mySlides fade">
						<div class="row">
						<div class="gif-img" style="float:left; padding: 4px 0 0 4px;">
							<img width="90" height="40" src="<?php echo base_url(); ?>public/images/top1.png" class="gifblank" alt="nxt"></a>
						</div>
						<div class="gif-img" style="float:right; padding: 4px 4px 0 0;">
							<img width="90" height="50" src="<?php echo base_url(); ?>public/images/top2.png" class="gifblank" alt="nxt"></a>
						</div>
						</div>
						<div class="Welcome-image" style="margin:0px 0 0 0;">
							<img width="320" height="265" src="<?php echo base_url(); ?>public/images/third-icon.png" class="gifblank" alt="ei-blank">
						</div>
						<div class="bottom-row" style="padding: 0 0px 0 17%; margin: -15px 0 14% 0;">
							<p class="bottom-text"><strong>Click on Billing Documents in My Sales module to prepare your first invoice</strong></p>
						</div>
					</div>
					<!--a class="prev" onclick="plusSlides(-1)">&#10094; &#10095;</a-->
					<a id="open" class="next nxt1" onclick="plusSlides(1)"><img width="70" height="30" src="<?php echo base_url(); ?>public/images/next1.png" class="gifblank" alt="nxt"></a>
					<a id="close" style="display:none;" class="next cls1" onclick="plusSlides(1)"><img width="70" height="30" src="<?php echo base_url(); ?>public/images/close1.png" class="gifblank" alt="nxt"></a>
					</div>
					<div style="text-align:center; padding-bottom:20px;">
					  <span class="dot" onclick="currentSlide(1)"></span>
 					  <span class="dot" onclick="currentSlide(2)"></span>
					  <span class="dot last_dot" onclick="currentSlide(3)"></span>
					</div>
					<!-- Carousal Ends -->
					</div>
			</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function () {
			$('#click_me').trigger('click');
		}, 500);
	});
</script>

<script>
	$("#click_me").on("click", function(e){
		e.preventDefault();
		$("#welcome_inv").modal('open');
		$(".modal-overlay").show();
	});

	$("#close").click(function(){
		$('#welcome_inv').hide();
		//window.location.href = "<?php echo base_url(); ?>profile/add-company-profile";
		setTimeout(function () {
			$(".modal-overlay").hide();
		}, 500);
		$('html, body').css('overflowY', 'auto'); 
		//$('#welcome_inv').modal('hide');
	});

	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	function currentSlide(n) {
		showSlides(slideIndex = n);
	}

	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("dot");
		if (n == slides.length){
			$("#close").show();
			$("#open").hide();
			//document.getElementById(close).style.display = 'block';
			//document.getElementById(open).style.display = 'none';
		}else{
			$("#close").hide();
			$("#open").show();
		}
		if (n > slides.length) {slideIndex = 1}
		if (n < 1) {slideIndex = slides.length}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace("active", "");
		}
		slides[slideIndex-1].style.display = "block";
		dots[slideIndex-1].className += " active";
	}

	</script>
<script type="text/javascript">
  $(document).ready(function() {
	$("#company_logo").change(function(event){
		$('.up_pic').hide();
	});
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
  $('.js-example-basic-single').select2();
  $('.select2-selection__rendered').each(function () {
      $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
  });

  if($("#tot_gst_nu").val()<=0){
$("#billing_address").keyup(function(){
  var billing_address1=$("#billing_address").val();
  $("#billing_address1").val(billing_address1);
    $("#billing_address1").parents('.input-field').addClass('label-active');
})

$("#pincode").keyup(function(){
  var pincode1=$("#pincode").val();
  $("#pincode1").val(pincode1);
    $("#pincode1").parents('.input-field').addClass('label-active');
})

$("#country").change(function(){
  var country_id=$(this).val();
 // $("select#country1 option:selected").val(country_id);
  $('#country1 option[value='+country_id+']').prop("selected", "selected").change();
  //$('#bank_country option[value='+country_id+']').prop("selected", "selected").change();
  $('#addbranch #bank_country option[value='+country_id+']').attr('selected','selected').change();
  $('#bank_country').material_select();

  if(country_id==101){
	  var curyr = new Date(new Date().getFullYear(), 3, 1);
	  $("#fy_start_date").datepicker("setDate", new Date(curyr));
	  //$("#fy_end_date")
  }

})

$("#state").change(function(){
  var state1=$(this).val();

  $("select#state1").val(state1).prop("selected", "selected").change();

})
$("#city").change(function(){
  var city1=$(this).val();

  $("select#city1").val(city1).prop("selected", "selected").change();

})

  }



  $("select").change(function () {
  if($(this).val()!=''){
    $(this).valid();
    $(this).closest('.input-field').find('.error').remove();
  }
  });

$('#gstingo').on("click",function(){
  $("#gstMsg").html("");
  $("#verified_mark").hide();
  var gstNo= $('#search_gstin').val();
    var gstExpr =/^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;

  if((gstNo.match(gstExpr)))
    {

$.ajax({
              type: "POST",
              dataType: 'json',

              url: base_url +'index/get_gst_details',
              data:{
                    "gst_id":gstNo,



              },
              cache: false,
                      success:function(response)
              {

                      //var dataGst= $.parseJSON(response);
                      if(response[0].error!=true){
                       $("#verified_mark").show();
                      $("#cust_name").val(response[0].data.lgnm);
                      $("#pincode").val(response[0].data.pradr.addr.pncd);
                       $("#pincode").keyup();
					   $("#pincode").focus().click();
                      $("#gstin1").val(response[0].data.gstin);
                      $("#gstin1").keyup();
                      var pan =response[0].data.gstin ;
                       $("#pan_no").val(pan.substring(2,12));
                     var address=response[0].data.pradr.addr.bno+" "+response[0].data.pradr.addr.flno+" "+response[0].data.pradr.addr.bnm+" "+response[0].data.pradr.addr.st+" "+response[0].data.pradr.addr.loc;

                     $("#billing_address").val(address);
                      $("#billing_address").keyup();
					  $("#billing_address").focus().click();
                      $('#country option[value='+response[0].data.info.country_id+']').attr("selected", 'selected').change();
                      setTimeout(function(){ $('select[name^="cust_billing_state"] option[value='+response[0].data.info.state_id+']').attr("selected", 'selected').change(); }, 3000);

                      setTimeout(function(){ $('select[name^="cust_billing_city"] option[value='+response[0].data.info.state_id+']').attr("selected", 'selected').change(); }, 4000);
                      }else{
                        $("#verified_mark").hide();
                        $("#gstMsg").append('<span style="color:red">'+response[0].data.error.message+'</span>').delay(3000).fadeOut();
                          return false;
                      }

              }


          });
        }
        else if(gstNo==""){
          $("#verified_mark").hide();
           $("#gstMsg").append('<span style="color:red">Enter GSTIN in a valid format</span>').delay(1000).fadeOut();
        return false;
        }
      else
        {
        $("#verified_mark").hide();
        $("#gstMsg").append('<span style="color:red">Enter GSTIN in a valid format</span>').delay(1000).fadeOut();
        return false;
        }

   })

  });
</script>
      <!-- END MAIN -->

	 <?php $this->load->view('template/footer.php');?>
