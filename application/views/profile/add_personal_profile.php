 <?php $this->load->view('template/header.php');?>
    <!-- START MAIN -->
<style type="text/css">
  .select-wrapper label.error:not(.active) {
     margin: -30px 0 0 -11px;
  }
  /*----------START SEARCH DROPDOWN CSS--------*/
.select2-container {
  width: 100% !important;
}
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 35px;
  color: #666;
  font-weight: 400;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 53px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
  border-top-left-radius: 0;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
   /* border: none;*/
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93.5%;
    max-width: 100%;
    background: #fff;
    border-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
.select2-container--open .select2-dropdown--below {
  margin-top: -15px;
/*  border-top: 1px solid #aaa;
  border-radius: 5px !important;*/
}
ul#select2-currency-results {
  width: 97%;
}
/*---Dropdown error message format---*/
.select-wrapper + label.error{
 margin: 18px 0 0 -10px;

}
select.error + label.error:not(.active){

 margin: -20px 0 0 -10px;
}
select + label.error.active{
  margin-left: -10px;
}

.toolattr{
	position: absolute;
    top: 5px;
    right: 7px;
}

.list_access{
	position: absolute;
    top: -2px;
    right: 3px;
}

#tab-table td, th {
	border: 1px solid #C0C0C0;
}

#reg_admin_type-error{
	margin: 0px 0 0 -10px !important;
}

#reg_username[type=text]{
	height:4rem !important;
}

#reg_designation[type=text]{
	height:4rem !important;
}

.uploader-placeholder, .uploader-placeholder-sign{
	height:90px !important;
}

	.tooltip {
		position: relative;
		display: inline-block;
		margin-bottom:-6px !important;
	}

	.tooltip .tooltiptext {
	  visibility: hidden;
	  width: 150px;
	  background-color: #7864e9;
	  color: #fff;
	  text-align: center;
	  font-size:13px !important;
	  border-radius: 6px;
	  padding: 5px 0;
	  margin: 20px 0 -20px 0;
	  /* Position the tooltip */
	  position: absolute;
	  z-index: 1;
	}

	.tooltip:hover .tooltiptext {
	  visibility: visible;
	}
/*---End dropdown error message format---*/

/*----------END SEARCH DROPDOWN CSS--------*/
/*------------- START Aniket CSS-----------------------*/


/*------------- END Aniket CSS-----------------------*/
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">

         <!-- START LEFT SIDEBAR NAV-->
         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <div class="container">
            <div class="plain-page-header">
              <div class="row">
                <div class="col l6 s12 m6">
                  <a class="go-back underline" href="<?php echo base_url();?>profile/personal_profile">Back to My Personal Profile</a>
                </div>
                <div class="col l6 s12 m6">
                </div>
              </div>
            </div>
            <div class="page-content">
              <div class="row">
                <div class="col s12 m12 l3"></div>
                <div class="col s12 m12 l6">
                  <form class="create-company-form border-split-form" id="add_personal_profile" name="add_personal_profile" method="post" action="<?php echo base_url();?>profile/insert_personal_profile" enctype="multipart/form-data">
                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
                      <div class="box-header">
                        <h3 class="box-title">Create Personal Profile</h3>
                      </div>
                      <div class="box-body">

                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l9">
                              <div class="row">
                                <div class="input-field">
                                  <label for="company_name" class="full-bg-label">First & Last Name<span class="required_field"> *</span></label>
                                  <input id="reg_username" name="reg_username" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m12 l3">
                              <div class="row">
                                <div class="input-field tooltip">
                                  <div class="uploader-placeholder" style="background-image: url('http://localhost/invoice/asset/css/img/icons/placeholder.png');">
                                    <label class="up_pic" style="font-size:11px; color:#696969; margin:-10px 15px 0 15px !important;">Upload your photo</label>
									<input type="file" class="hide-file" id="reg_profile_image" name="reg_profile_image">
                                    <input type="hidden" id="image_info" name="image_info" value="" />
                                  </div>
								  <span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l9">
                              <div class="row">
                                <div class="input-field">
                                  <label for="company_name" class="full-bg-label">Designation<!--<span class="required_field"> *</span>--></label>
                                  <input id="reg_designation" name="reg_designation" class="full-bg adjust-width" type="text">
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m12 l3">
                              <div class="row">
                                <div class="input-field tooltip">
								  <label class="up_sign" style="font-size:11px; color:#696969; margin-top:-10px !important; margin-left:1px;">Upload Digital Signature</label>
                                  <div class="uploader-placeholder-sign" style="background-image: url('http://localhost/invoice/asset/css/img/icons/placeholder.png')">
                                    <input type="file" class="hide-file-signature" id="reg_degital_signature" name="reg_degital_signature">
                                    <input type="hidden" id="image_signature_info" name="image_signature_info" value="" />
                                  </div>
								  <span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6">
                              <div class="row">
                                <div class="input-field" style="border-right:1px solid #eef2fe;">
                                  <label for="contact_email" class="full-bg-label">Email ID<span class="required_field"> *</span></label>
                                  <input id="reg_email" name="reg_email" class="full-bg adjust-width " type="text">
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set ">
                              <div class="row">
                                <div class="input-field">
                                    <label for="contact_mobile" class="full-bg-label">Mobile<span class="required_field"> *</span></label>
                                    <input id="reg_mobile" name="reg_mobile" class="full-bg adjust-width numeric_number" type="text" maxlength="10" title="Please enter 10 digits for a valid Mobile number">
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>



                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set">
                              <div class="row passcheck">
                                <div class="input-field">
                                    <label for="contact_mobile" class="full-bg-label">Password<span class="required_field"> *</span></label>
                                    <input id="reg_password" name="reg_password" class="full-bg adjust-width border-top-none show_password password_strength" type="password" onblur="alphanumeric(document.add_personal_profile.reg_password)">
                                    <span id="invalid_password_alpha"  class="error pass_2" style="display:none;font-size:12px;">Please Enter Alphanumeric Values and Special Character for Password</span>
                                </div>
								<div class="toolattr">
									<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Minimum 8 Characters, 1 in Uppercase, 1 Number & 1 Special Character"></a>
								</div>
                                <div class="passattr">
                                  <span id="pwdMeter" class="neutral"></span>
                                  <input type="checkbox" id="filled-in2" class="show_password_chkbox" />
                                  <label for="filled-in2" class="indic">View</label>
                                </div>
                              </div>
                            </div>
                             <div class="col s12 m12 l6 input-set">
                              <div class="row passcheck">
                                <div class="input-field">
                                  <label for="country" class="full-bg-label">Confirm Password<span class="required_field"> *</span></label>
                                  <input id="contact_confirm_password" name="contact_confirm_password" class="full-bg adjust-width border-top-none show_password_2" type="password"  onblur="alphanumeric(document.add_personal_profile.reg_password)">
                                </div>
                                <div class="passattr">
                                  <span id="" class="neutral"></span>
                                  <input type="checkbox" id="confirmpassword" name="confirmpassword" class="show_password_chkbox_2" />
                                  <label for="confirmpassword" class="indic">View</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                              <div class="row">
                          <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-none" style="border:1px solid #EEF2FE !important;">
                              <div class="input-field">
                <div class="list_access">
                  <a style="font-size:14px; color:#696969;" id="view_list" href="#view_access_list" class="cur customize_cfields modal-trigger modal-close">(View access list)</a>
                </div>
                <label for="country" class="full-bg-label select-label">Access<span class="required_field"> *</span></label>
                              <select class="js-example-basic-single" name="reg_admin_type" id="reg_admin_type">
                                  <option value="">ACCESS *</option>
                                   <?php foreach($access_list as $access){ ?>
                                  <option value="<?php echo $access->id; ?>" ><?php echo $access->access; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                            </div>

              <!--div class="col s12 m12 l6 input-set border-none" id="client_select" style="border:1px solid #EEF2FE !important;" disabled>
                              <div class="input-field">
                <label for="client-name" class="full-bg-label select-label">Select Client<span class="required_field"> *</span></label>
                <select class="js-example-basic-single" name="client-name" id="client-name">
                                  <option value="">SELECT CLIENT *</option>
                  <option value="01"> Client name1 </option>
                                </select>
                              </div>
                            </div-->
              </div>
            </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 m12 l12 mt-2">
                        <div class="form-botom-divider"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 m12 l6 right">
                       <input type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right" value="Save">
                       <button  class="btn-flat theme-flat-btn theme-btn theme-btn-large right mr-5" type="reset" onclick="location.href = '<?php echo base_url();?>profile/personal_profile';">Cancel</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="col s12 m12 l3"></div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>



        <!-- END WRAPPER -->

      </div>
      <script type="text/javascript">
        $(document).ready(function() {
          $('.js-example-basic-single').select2();
          $('.select2-selection__rendered').each(function () {
              $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
          })
        $("select").change(function () {
        if($(this).val()!=''){
          $(this).valid();
          $(this).closest('.input-field').find('.error').remove();
        }
        });

		/*var a = $('#reg_degital_signature').val();
		alert(a);

		if(a!=""){
			alert('ok');
		}else if(a==""){
			alert("Not ok");
		}*/

		$("#reg_profile_image").click(function(event){
			$('.up_pic').hide();
		});

		$("#reg_degital_signature").click(function(event){
			$('.up_sign').hide();
		});

		$("#reg_admin_type").change(function () {
			if($(this).val() !="5"){
				$("#client-name").attr('disabled','disabled');
			}else{
				$("#client-name").attr('disabled',false);
			}
		});
      });
      </script>
      <!-- END MAIN -->
<?php $this->load->view('template/footer.php');?>
