<!-- <html>
<head>
	<style type="text/css">
		table tr{
			margin-bottom: 20px;
		}
		#Statutory_info{
			margin-top:10px;
		}
	</style>
</head>
<body>
<div class='col-md-12' style='width: 60%;margin: 0 auto;background: rgba(245, 245, 245, 0.35);padding:10px;'>
<p style='text-align:center;'><img src='<?=base_url()?>asset/images/logo.png' alt='Team Invoice Management'></p>
<h2 style='text-align:center;'>Hey TR</h2>
<p style='text-align:center;'> Attached with this email you will find a spreadsheet with the details of your Invoice. </p>
<table align="center" cellpadding="0">
<tr>
<th colspan="2" align="left">
<h2>Basic Information</h2></th>
</tr>
<tr>
<th align="left">
Company Name:
</th>
<td style="padding:4px;margin:3px;">
<?php echo $company_profile[0]->bus_company_name; ?>
</td>
</tr>
<tr>
<th align="left">
Billing Address:
</th>
<td style="padding:4px;margin:3px;">
<?php $city = @$company_profile[0]->name.', ';$zip = @$company_profile[0]->bus_billing_zipcode;$zip=' '.@$zip; ?>
<p><?php echo ($company_profile[0]->bus_billing_address)?$company_profile[0]->bus_billing_address.', '.@$city.@$company_profile[0]->state_name.@$zip:''.@$company_profile[0]->name; ?></p>
</td>
</tr>
<tr>
<th align="left">
Shipping Address:
</th>
<td style="padding:4px;margin:3px;">
Navarangpura,Ahmedabad,Gujarat,India 1111111xdddddddddddddddddddddddc
</td>
</tr>
<tr>
<th colspan="2" align="left">
<h2 id="Statutory_info">Statutory Info</h2></th>
</tr>
<tr>
<th align="left">
PAN Number:
</th>
<td style="padding:4px;margin:3px;">
ABCDE1234A
</td>
</tr>
<tr>
<th colspan="2" align="left">
<h3 style="text-align:left;padding:5px 0px;margin:5px 0px;" style="margin-bottom: 0px;margin-top: 5px;">GSTIN Number</h3></th>
</tr>
<tr>
<th align="left">
GST No.
</th>
<td style="padding:4px;margin:3px;">
GST08526888
</td>
</tr>

<tr>
<th align="left">
Place of Supply
</th>
<td style="padding:4px;margin:3px;">
UNITED STATE
</td>
</tr>
</table> 

<hr style='border: 1px solid rgba(204, 204, 201, 0.43);'><p style='text-align: center;font-weight: bold;color: gray;'>Follow us</p><p style='text-align: center;'><a href='https://www.facebook.com'><img src='<?=base_url()?>asset/images/facebook_mail.png'  ></a>&nbsp;&nbsp;<a href='https://twitter.com/'><img src='<?=base_url()?>asset/images/twitter_mail.png'  ></a>&nbsp;&nbsp;<a href='https://www.linkedin.com/'><img src='<?=base_url()?>asset/images/linkedin_mail_2.png'></a><p style='text-align:center;color: gray;'>Sent from EazyInvoice &copy; Copyright 2018</p></div></body></html> -->
<style>
	@media print {
		.pagebreak { page-break-before: always; } /* page-break-after works, as well */
	}
    /*table.tableizer-table {
		font-size: 12px;

		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;

	}
	.tableizer-table th {

		font-weight: bold;
		text-align: left;

	}
	.bank_info{
		margin-left:5px;
		list-style:disc inside;

	}
	.branch_info{

	}
	h1{
		color: #7965EA;
	}
	th{

		color: #666;
	}
	h3{
		padding: 5px 0px;
		margin: 5px 0px;
		color: #000 !important;
	}*/
</style>
<style media="print">
 @page {
  size: auto;
  margin: 0;
       }
</style>
<div class="row">
    <div class='col-md-12' style='width: 90%;margin: 0 auto;background: rgba(245, 245, 245, 0.35);padding:10px;'>
       
        <?php if(@$company_profile[0]->bus_company_logo != '') {?>

        <p style='text-align:center;'><img width="100" height="50" src='<?=base_url()?>public/upload/company_logos/<?php echo $company_profile[0]->bus_id; ?>/<?php echo $company_profile[0]->bus_company_logo;?>' height="100px" width="100px" alt='Company Logo'></p>
        <?php }else { ?>
          <p style='text-align:center;'></p>
          <?php } ?>

        <table style="font-size: 14px;font-family: Arial, Helvetica, sans-serif;" class="tableizer-table" align="center" cellpadding="0" border="0">
            <thead>

                <tr class="tableizer-firstrow">
                    <th style="text-align: left;color: #666;font-weight:bold;"></th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td style="padding:4px;margin:3px;">
                        <h2>Hey <?= ucfirst($this->session->userdata['user_session']['ei_username']); ?></h2></td>
                </tr>
                <tr>
                    <td style="padding:4px;margin:3px;" colspan="2">
                        <p> Please find the details about company bellow. </p>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <h1 style="color: #7965EA;text-align: left;">Profile Information</h1></th>
                </tr>
                <tr>
                    <th colspan="2">
                        <h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Info</h3></th>
                </tr>
                <tr>
                    <th style="text-align: left;color: #666;font-weight:bold;">Name</th>
                    <td style="padding:4px;margin:3px;">
                        <?php echo @$register_data[0]->reg_username; ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;color: #666;font-weight:bold;">Email ID</th>
                    <td style="padding:4px;margin:3px;">
                        <?php echo @$register_data[0]->reg_email; ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;color: #666;font-weight:bold;">Mobile</th>
                    <td style="padding:4px;margin:3px;">
                        <?php echo @$register_data[0]->reg_mobile; ?>
                    </td>
                </tr>
				<tr>
                    <th style="text-align: left;color: #666;font-weight:bold;">Designation</th>
                    <td style="padding:4px;margin:3px;">
                        <?php echo @$register_data[0]->reg_designation; ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;color: #666;font-weight:bold;">Access</th>
                    <td style="padding:4px;margin:3px;">
                        <?php if(@$register_data[0]->reg_admin_type != '-1'){ echo @$access_data[0]->access; } else{ echo "Admin"; } ?>
                    </td>
                </tr>

                <!-- <tr>
                    <td style="padding:4px;margin:3px;" colspan="2">
                        <p> For any queries write to us at <a href="mailto:admin@windchimes.co.in" style="color: blue;"><u>admin@windchimes.co.in</u></a> </p>
                    </td>
                </tr>
                <tr>
                    <td style="padding:4px;margin:3px;" colspan="2">
                        <p> Here's to some happy invoicing! </p>
                    </td>
                </tr>
                <tr>
                    <td style="padding:4px;margin:3px;" colspan="2">
                        <p> Team Invoice Management </p>
                    </td>
                </tr> -->

            </tbody>
        </table>
        <hr style='border: 1px solid rgba(204, 204, 201, 0.43);'>
        <p style='text-align: center;font-weight: bold;color: gray;'>Follow us</p>
        <p style='text-align: center;'><a href='https://www.facebook.com'><img src='<?=base_url()?>asset/images/facebook_mail.png' alt="facebook_mail" ></a>&nbsp;&nbsp;<a href='https://twitter.com/'><img src='<?=base_url()?>asset/images/twitter_mail.png' alt="twitter_mail" ></a>&nbsp;&nbsp;<a href='https://www.linkedin.com/'><img src='<?=base_url()?>asset/images/linkedin_mail_2.png'alt="linkedin_mail"></a></p>
        <p style='text-align:center;color: gray;'>Sent from Xebra &copy; Copyright <?php echo date('Y');?></p>
    </div>
</div>
<div class="pagebreak"> </div>
</body>

</html>
<script type="text/javascript">
    window.print();
</script>