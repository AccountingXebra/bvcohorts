<style type="text/css">
	@page {
        size: auto; 
        margin: 0mm;  
    }
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="4"><h2 style="margin: 0">Company Profile</h2></th></tr>
	<tr>
	 <th>NAME</th>
	 <th>TYPE OF COMPANY</th>
	 <th>GST NUMBER</th>
	 <th>PANCARD</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($result as $key => $value) { ?>
	<tr>
		<td><?=$value['bus_company_name']?></td>
		<td><?=$value['company_type']?></td>
		<td><?=$value['gst_no']?></td>
		<td><?=$value['bus_pancard']?></td>
	</tr>	
<?php } ?>

</tbody>
</table>
<script type="text/javascript">
	window.print();
</script>