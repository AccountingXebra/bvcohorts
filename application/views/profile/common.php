<div id="deactive_profile" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate Profile</h4>

      <input type="hidden" id="profile_id" name="profile_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to deactivate this profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_user">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="deactive_profile_2" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate Profile</h4>

      <input type="hidden" id="personal_profile_id_2" name="personal_profile_id_2" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to deactivate this profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_user_2">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="deactive_company_modal" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="delete">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate Company Profile</h4>

      <input type="hidden" id="company_profile" name="company_profile" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to deactivate this Company profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_company_status">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="active_company_profile" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Activate Company Profile</h4>

      <input type="hidden" id="activate_cmp_profile_id" name="activate_cmp_profile_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to activate this Company profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_cmp_profile">Activate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="active_profile_2" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Activate Profile</h4>

      <input type="hidden" id="activate_personal_profile_id_2" name="activate_personal_profile_id_2" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to activate this profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_user_2">Activate</button>

        </div>

      </div>

    </div>

  </div>

</div>


<div id="active_profile" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Activate Profile</h4>

      <input type="hidden" id="activate_profile_id" name="activate_profile_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to activate this profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_user">Activate</button>

        </div>

      </div>

    </div>

  </div>

</div>



<div id="select_business" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Welcome To MyEasyInvoices!</h4>

    </div></div>

            <div class="modal-body change-model">

              <p> You can begin customizing by selecting the type of business you are into</p>

          </div>

          

      <div class="modal-body change-model">

     	 <div class="row pass-row renter">

            <form class="col s12" name="select_businesstype" id="select_businesstype" method="post">

                <p>

                  <input name="radio_type_user" type="radio" id="service" value="1" />

                  <label for="service">Service</label>

                </p>

                <p>

                  <input name="radio_type_user" type="radio" id="manufacturing" value="2"/>

                  <label for="manufacturing">Manufacturing</label>

                </p>

                <p>

                  <input name="radio_type_user" type="radio" id="retail" value="3" />

                  <label for="retail">Retail/Trading/Franchising</label>

                </p>

            </form>

  		</div>

		</div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close update_businesstype">Save And Countinue</button>

        </div>

      </div>

    </div>

  </div>



 

</div>



 <div id="businesstype_errormsg" class="modal error">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content success-msg">

      <p>Select any Business</p>

      <a class="modal-close close-pop pass-suc modal-trigger" href="#select_business" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div>

 </div>


<div id="pass" class="modal Password-change">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Change Password</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>
    
    <form class="col s12" name="change_password_frm" id="change_password_frm" method="post">

        <div class="modal-body change-model">
        
              <div class="row pass-row">
        
                <div class="input-field">
        
                  <label for="old_password" class="Password-chan">Enter Old Password</label>
        
                  <input id="old_password" name="old_password" class="enter" type="password">
        			
                </div>
        
              </div>
    
        </div>

     	<div class="modal-body change-model">
    
        <div class="row pass-row renter">
    
          <div class="input-field">
    
            <label for="new_password" class="Password-chan">Enter New Password</label>
    
            <input id="new_password" name="new_password" class="enter password_strength show_password" type="password" onblur="alphanumeric_password_change(document.change_password_frm.new_password)">
    
    		<span id="invalid_password_alpha"  class="error pass_2" style="display:none;font-size:12px;">Please Enter Alphanumeric Values and Special Character for Password</span>
          </div>
    
        </div>



        <div class="row pass-row renter">
    
          <div class="input-field">
    
            <label for="confirm_password" class="Password-chan">Confirm New Password</label>
    
            <input id="confirm_password" name="confirm_password" class="enter" type="password">
    
          </div>
    
        </div>
    
      </div>
      
      <div class="modal-body change-model">
    
        <div class="row pass-row renter">
    
          <p>
    
			<input type="checkbox" id="filled-in_new_pass" class="purple show_password_chkbox" />

            <label for="filled-in_new_pass">Show Password</label>

    
          </p>
    
        </div>



        <div class="row pass-row renter">
    
          <div class="input-field">
    
            <span id="pwdMeter" class="neutral"></span>
    
          </div>
    
        </div>
    
      </div>

   		<div class="modal-content">

            <div class="modal-footer">
        
              <div class="row">
        
                <div class="col l4 s12 m12"></div>
        
                <div class="col l12 s12 m12 cancel-deactiv text-center">
        
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
        <!-- #pass-succ -->
       			  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Change Password</button>
                </div>
        
              </div>
        
            </div>

  </div>

	</form>
</div>



<div id="pass-succ" class="modal Password-change">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>OTP Confirmation</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body change-model">

      <div class="row pass-row otp">

        <p>An OTP has successfully send to you registered Address, Enter it below to Change Password</p>

      </div>

    </div>


        <form class="col s12" name="otp_password_frm" id="otp_password_frm" method="post">
        
            <div class="modal-body change-model">
        
              <div class="row pass-row renter">
        
                <div class="input-field">
        
                  <label for="password_otp" class="Password-chan">ENTER 6 DIGIT OTP</label>
        
                  <input id="password_otp" name="password_otp" class="enter" type="text">
        
                </div>
        
              </div>
        
        
        
              <div class="row pass-row renter">
        
                
        
              </div>
        
            </div>
        
            <div class="modal-content">
        
            <div class="modal-footer">
        
              <div class="row">
        
                <div class="col l4 s12 m12"></div>
        
                <div class="col l12 s12 m12 cancel-deactiv text-center">
        
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
        
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Change Password</button>
        
                </div>
        
              </div>
        
            </div>
        
          </div>
        </form>
</div>


<!-- <div id="add_gst_frm" class="modal add_gst_modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png">

  <div class="modal-content">

    <div class="modal-header">

      <h4>GSTIN Information</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png"></a>

    </div></div>

    <div class="modal-body change-model">

      <div class="row pass-row otp">

        <p>Insert your Branch and GST Information</p>

      </div>

    </div>
        <form class="col s12" name="add_gst_from" id="add_gst_from" method="post">
        
            <div class="modal-body change-model">
        
              <div class="row pass-row renter">
        
                <div class="input-field">
        
                  <label for="branch_name" class="branch_name">Branch Name</label>
        
                  <input id="branch_name" name="branch_name" class="enter" type="text">
        
                </div>
        
              </div>
              
      		  <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_no" class="gst_no">GST NO</label>
            
                      <input id="gst_no" name="gst_no" class="enter" type="text">
            
             		 </div>
        
              </div>
        
        	 <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_perc" class="gst_perc">GST %</label>
            
                      <input id="gst_perc" name="gst_perc" class="enter" type="text">
            
             		 </div>
        
              </div>
              
              <div class="row pass-row renter">
              </div>
        
            </div>
        
            <div class="modal-content">
        
            <div class="modal-footer">
        
              <div class="row">
        
                <div class="col l4 s12 m12"></div>
        
                <div class="col l8 s12 m12 cancel-deactiv">
        
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
        
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>
        
                </div>
        
              </div>
        
            </div>
        
          </div>
        </form>
</div> -->

<div id="add_gst_frm" class="modal add_gst_modal modal-md">
  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
  <div class="modalheader">
    <h4>Add new GSTIN</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>
  <div class="modalbody">
    <form class="addgstin" name="add_gst_from" id="add_gst_from" method="post">
      <div class="row gstrow">
        <div class="col l10 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>
                <label class="full-bg-label active">GSTIN</label>
                <input type="hidden" name="gst_array[]" value="1" />
                <input name="gstin1" id="gstin1" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>
                <label class="full-bg-label active">PLACE OF SUPPLY</label>
                <input id="location1" name="location1" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l2 s2 m2 fieldset">
          <div class="col l12 s12 m12 fieldset">
            <a class="add-remove-btn"><i class="material-icons  rem-red">remove</i></a>
          </div>
        </div>
      </div>
      <div id="rep-element">
      </div>

      <div class="row">
        <div class="col l10 s10 m10 fieldset empty-add-set"></div>
        <div class="col l2 s2 m2 fieldset">
          <div class="col l12 s12 m12 fieldset">
            <a class="add-remove-btn add-icon addgstrow"><i class="material-icons add-bloue">add</i></a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
          <a href="#" class="findgstlink">Find GSTIN (India)</a>
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="add_gst_frm_2" class="modal add_gst_modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>GSTIN Information</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body change-model">

      <div class="row pass-row otp">

        <p>Insert your Branch and GST Information</p>

      </div>

    </div>
        <form class="col s12" name="add_gst_from_2" id="add_gst_from_2" method="post">
        
            <div class="modal-body change-model">
        
              <div class="row pass-row renter">
        
                <div class="input-field">
        
        		<input type="hidden" name="company_id_gst_2" id="company_id_gst_2" value=""/>
                <!--<input type="hidden" name="user_id_gst_2" id="user_id_gst_2" value=""/>-->
                  <label for="branch_name_gst_2" class="branch_name">Branch Name</label>
        
                  <input id="branch_name_gst_2" name="branch_name_gst_2" class="enter" type="text">
        
                </div>
        
              </div>
              
      		  <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_no_gst_2" class="gst_no">GST NO</label>
            
                      <input id="gst_no_gst_2" name="gst_no_gst_2" class="enter" type="text">
            
             		 </div>
        
              </div>
        
        	 <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_perc_gst_2" class="gst_perc">GST %</label>
            
                      <input id="gst_perc_gst_2" name="gst_perc_gst_2" class="enter" type="text">
            
             		 </div>
        
              </div>
              
              <div class="row pass-row renter">
              </div>
        
            </div>
        
            <div class="modal-content">
        
            <div class="modal-footer">
        
              <div class="row">
        
                <div class="col l4 s12 m12"></div>
        
                <div class="col l8 s12 m12 cancel-deactiv">
        
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
        
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>
        
                </div>
        
              </div>
        
            </div>
        
          </div>
        </form>
</div>

<div id="edit_gst_frm" class="modal edit_gst_modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>GSTIN Information</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body change-model">

      <div class="row pass-row otp">

        <p>Update your Branch and GST Information</p>

      </div>

    </div>
        <form class="col s12" name="edit_gst_from" id="edit_gst_from" method="post">
        
            <div class="modal-body change-model">
        
              <div class="row pass-row renter">
        
                <div class="input-field">
                
                <input type="hidden"  name="id_edit_gst" id="id_edit_gst" value=""/>
                	<input type="hidden"  name="company_id" id="company_id" value=""/>
                    <input type="hidden"  name="user_id" id="user_id" value=""/>
        
                  <label for="branch_name" class="branch_name">Branch Name</label>
        
                  <input id="branch_name_edit_gst" name="branch_name_edit_gst" class="enter" type="text">
        
                </div>
        
              </div>
              
      		  <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_no" class="gst_no">GST NO</label>
            
                      <input id="gst_no_edit_gst" name="gst_no_edit_gst" class="enter" type="text">
            
             		 </div>
        
              </div>
        
        	 <div class="row pass-row renter">
            
                    <div class="input-field">
            
                      <label for="gst_perc" class="gst_perc">GST %</label>
            
                      <input id="gst_perc_edit_gst" name="gst_perc_edit_gst" class="enter" type="text">
            
             		 </div>
        
              </div>
              
              <div class="row pass-row renter">
              </div>
        
            </div>
        
            <div class="modal-content">
        
            <div class="modal-footer">
        
              <div class="row">
        
                <div class="col l4 s12 m12"></div>
        
                <div class="col l8 s12 m12 cancel-deactiv">
        
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
        
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>
        
                </div>
        
              </div>
        
            </div>
        
          </div>
        </form>
</div>


 <div id="change_pass_error" class="modal error">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content error-msg">

      <p>Your Password did not matched !! Please Try Again..</p>

      <a class="modal-close close-pop pass-suc modal-trigger" href="#pass" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div>

 </div>
 

<div id="otp_error" class="modal error">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content error-msg">

      <p>You might have entered wrong OTP.Try Again!</p>

      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div>

 </div>
 

<div id="pass-succful" class="modal success">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content success-msg">

      <p>Password Changed Successfully</p>

      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div>

 </div>

<div id="deactive_customer_model" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate Customer</h4>

      <input type="hidden" id="deactive_customer_id" name="deactive_customer_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to deactivate this Customer?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_customer_profile">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>
  
<div id="active_customer_model" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Activate Customer</h4>

      <input type="hidden" id="activate_customer_id" name="activate_customer_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to activate this Customer?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close activate_customer_profile">Activate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_cust_legal_doc" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove Legal Document</h4>

      <input type="hidden" id="cust_legal_doc_id" name="cust_legal_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this legal document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_legal_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_cust_po_doc" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove Purchase Order Document</h4>

      <input type="hidden" id="cust_po_doc_id" name="cust_po_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this Purchase Order document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_po_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_cust_other_doc" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove Other Document</h4>

      <input type="hidden" id="cust_other_doc_id" name="cust_other_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_other_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>


<div id="addbranch" class="modal modal-md">
  <div class="modalheader">
    <h4>Add New Bank Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="bank_details" name="bank_details" method="post">
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
          <div class="col l6 s12 m6 fieldset">
            <div class="input-field">
              <label for="ac_number" class="full-bg-label">Enter Bank Account Number</label>
              <input name="ac_number" id="ac_number" class="full-bg adjust-width gstin" type="text">
            </div>
          </div>
          <div class="col l6 s12 m6 fieldset">
            <div class="input-field">
              <label class="full-bg-label" for="bank_name">Enter Bank Name</label>
              <input id="bank_name" name="bank_name" class="full-bg adjust-width" type="text">
            </div>
          </div>
        </div>
      </div>

      <div class="row gstrow">
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field ">
              <label for="acc_type" class="full-bg-label select-label">Select Account Type</label>
              <select id="acc_type" class="country-dropdown check-label" name="acc_type">
                <option value=""></option>
                <option value="1">Current</option>
                <option value="2">Savings</option>
              </select>
            </div>
          </div>
        </div>
          
        <div class="col l6 s12 m6 fieldset">
          <div class="input-field">
            <label class="full-bg-label" for="ifsc_code">Enter IFSC Code</label>
            <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">
          </div>
        </div>
      </div>
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="input-field">
              <label for="bank_branch_name" class="full-bg-label">Enter Branch Name</label>
              <input id="bank_branch_name" name="bank_branch_name" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>


<div id="edit_bank_branch" class="modal modal-md">
  <div class="modalheader">
    <h4>Edit Bank Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="edit_bank_details" name="edit_bank_details" method="post">
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
          <div class="col l6 s12 m6 fieldset">
            <div class="input-field">
             <input name="edit_cmpid_bankinfo" id="edit_cmpid_bankinfo" type="hidden">
			 <input name="edit_cbank_id" id="edit_cbank_id" type="hidden">
              <label for="ac_number" class="full-bg-label">Enter Bank Account Number</label>
              <input name="edit_ac_number" id="edit_ac_number" class="full-bg adjust-width gstin" type="text">
            </div>
          </div>
          <div class="col l6 s12 m6 fieldset">
            <div class="input-field">
              <label class="full-bg-label" for="bank_name">Enter Bank Name</label>
              <input id="edit_bank_name" name="edit_bank_name" class="full-bg adjust-width" type="text">
            </div>
          </div>
        </div>
      </div>

      <div class="row gstrow">
        <div class="col l12 s12 m12">
          <div class="col l6 s12 m6">
            <div class="input-field">
              <label for="edit_acc_type" class="full-bg-label select-label">Select Account Type</label>
              <select id="edit_acc_type" class="country-dropdown check-label" name="edit_acc_type">
                <option value=""></option>
                <option value="1">Current</option>
                <option value="2">Savings</option>
              </select>
            </div>
          </div>
          <div class="col l6 s12 m6">
            <div class="input-field">
              <label class="full-bg-label" for="ifsc_code">Enter IFSC Code</label>
              <input id="edit_ifsc_code" name="edit_ifsc_code" class="full-bg adjust-width" type="text">
            </div>
          </div>
           <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="input-field">
              <label for="bank_branch_name" class="full-bg-label">Enter Branch Name</label>
              <input id="edit_bank_branch_name" name="edit_bank_branch_name" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
            </div>
        </div>
      </div>
        </div>
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="modal1" class="modal">
  <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
  <div class="modal-content">
    <div class="modal-header">
      <h4>Deactivate Account</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
    </div></div>
    <div class="modal-body">
      <p>Are you sure you want to deactivate your account?</p>
    </div>
    <div class="modal-content">
    <div class="modal-footer">
      <div class="row">
        <div class="col l4 s12 m12"></div>
        <div class="col l12 s12 m12 cancel-deactiv text-center">
          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>
          <a href="#" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_account">Deactivate</a>
        </div>
      </div>
    </div>
  </div>
</div>

 <div id="deactivate-notification" class="modal success">
  <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png">
    <div class="modal-content success-msg">
      <p>Account Deactivated Successfully</p>
      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
    </div>
 </div>
 
 
<div id="view_add_modal-popup" class="modal">
  <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
  <div class="modal-content">
    <div class="modal-header">
      <h4>Confirmation</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
    </div></div>
    <div class="modal-body confirma">
      <p>Link has been to your primary contact person's<br>
          email id to access customer portal</p>
    </div>
    <div class="modal-content">
    <div class="modal-footer">
      <div class="row">
        <div class="col l5 s12 m12"></div>
        <div class="col l12 s12 m12 cancel-deactiv text-center">
          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
          <a href="#" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="view_modal-popup" class="modal">
  <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
  <div class="modal-content">
    <div class="modal-header">
      <h4>Confirmation</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
    </div></div>
    <div class="modal-body confirma">
      <p>Are you sure you want to remove the Link of your primary contact person's<br>
          email id to access customer portal?</p>
    </div>
    <div class="modal-content">
    <div class="modal-footer">
      <div class="row">
        <div class="col l5 s12 m12"></div>
        <div class="col l12 s12 m12 cancel-deactiv text-center">
          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
          <a href="#" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="add_customer_modal" class="modal modal-md">
  <div class="modalheader">
    <h4>Add New Customer</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_new_customer_form" name="add_new_customer_form" method="post">
    <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="input-field">
              <label for="new_cust_company_name" class="full-bg-label">Company Name</label>
              <input id="new_cust_company_name" name="new_cust_company_name" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
            </div>
        </div>
      </div>
      
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="input-field">
              <label for="new_cust_billing_address" class="full-bg-label">Billing Address</label>
              <input id="new_cust_billing_address" name="new_cust_billing_address" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
            </div>
        </div>
      </div>
      
     <div class="row gstrow">
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field ">
              <label for="new_cust_country" class="full-bg-label select-label">Country</label>
              <select id="new_cust_country" class="country-dropdown check-label" name="new_cust_country">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
          
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field ">
              <label for="new_cust_state" class="full-bg-label select-label">State</label>
              <select id="new_cust_state" class="country-dropdown check-label" name="new_cust_state">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="row gstrow">
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field ">
              <label for="new_cust_city" class="full-bg-label select-label">City</label>
              <select id="new_cust_city" class="country-dropdown check-label" name="new_cust_city">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
          
        <div class="col l6 s12 m6 fieldset">
          <div class="input-field">
            <label class="full-bg-label" for="ifsc_code">Pincode</label>
            <input id="new_cust_pincode" name="new_cust_pincode" class="full-bg adjust-width" type="text">
          </div>
        </div>
      </div>
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
          <div class="col l6 s12 m6 fieldset">
            <div class="input-field">
              <label for="ac_number" class="full-bg-label">Shipping Address</label>
              <input name="new_cust_ship_address" id="new_cust_ship_address" class="full-bg adjust-width gstin" type="text">
            </div>
          </div>
          <div class="col l6 s12 m6 fieldset">
            <div class="col s12 chkbox2">
                <input type="checkbox" name="same-bill-customer" id="same-bill-popup">
                <label class="checkboxx2 greentheme-checkbox" for="same-bill-popup"><span class="check-boxs">Same as billing address</span></label>
              </div>
          </div>
        </div>
      </div>
      <div class="row gstrow">
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field shipping_values">
              <label for="new_cust_ship_country" class="full-bg-label select-label">Shipping Country</label>
              <select id="new_cust_ship_country" class="country-dropdown check-label" name="new_cust_ship_country">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
          
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field shipping_values">
              <label for="new_cust_ship_state" class="full-bg-label select-label">Shipping State</label>
              <select id="new_cust_ship_state" class="country-dropdown check-label" name="new_cust_ship_state">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row gstrow">
        <div class="col l6 s12 m6 input-set">
          <div class="row">
            <div class="input-field shipping_values">
              <label for="new_cust_ship_city" class="full-bg-label select-label">City</label>
              <select id="new_cust_ship_city" class="country-dropdown check-label" name="new_cust_ship_city">
                <option value=""></option>
              </select>
            </div>
          </div>
        </div>
          
        <div class="col l6 s12 m6 fieldset">
          <div class="input-field">
            <label class="full-bg-label" for="ifsc_code">Shipping Pincode</label>
            <input id="new_cust_ship_pincode" name="new_cust_ship_pincode" class="full-bg adjust-width" type="text">
          </div>
        </div>
      </div>
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
            <div class="input-field">
              <label for="bank_branch_name" class="full-bg-label">PAN NUMBER</label>
              <input id="new_cust_pan_no" name="new_cust_pan_no" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>