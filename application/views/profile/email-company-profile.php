<!-- <html>
<head>
	<style type="text/css">
		table tr{
			margin-bottom: 20px;
		}
		#Statutory_info{
			margin-top:10px;
		}
	</style>
</head>
<body>
<div class='col-md-12' style='width: 60%;margin: 0 auto;background: rgba(245, 245, 245, 0.35);padding:10px;'>
<p style='text-align:center;'><img src='<?=base_url()?>asset/images/logo.png' alt='Team Invoice Management'></p>
<h2 style='text-align:center;'>Hey TR</h2>
<p style='text-align:center;'> Attached with this email you will find a spreadsheet with the details of your Invoice. </p>
<table align="center" cellpadding="0">
<tr>
<th colspan="2" align="left">
<h2>Basic Information</h2></th>
</tr>
<tr>
<th align="left">
Company Name:
</th>
<td style="padding:4px;margin:3px;">
<?php echo $company_profile[0]->bus_company_name; ?>
</td>
</tr>
<tr>
<th align="left">
Billing Address:
</th>
<td style="padding:4px;margin:3px;">
<?php $city = @$company_profile[0]->name.', ';$zip = @$company_profile[0]->bus_billing_zipcode;$zip=' '.@$zip; ?>
<p><?php echo ($company_profile[0]->bus_billing_address)?$company_profile[0]->bus_billing_address.', '.@$city.@$company_profile[0]->state_name.@$zip:''.@$company_profile[0]->name; ?></p>
</td>
</tr>
<tr>
<th align="left">
Shipping Address:
</th>
<td style="padding:4px;margin:3px;">
Navarangpura,Ahmedabad,Gujarat,India 1111111xdddddddddddddddddddddddc
</td>
</tr>
<tr>
<th colspan="2" align="left">
<h2 id="Statutory_info">Statutory Info</h2></th>
</tr>
<tr>
<th align="left">
PAN Number:
</th>
<td style="padding:4px;margin:3px;">
ABCDE1234A
</td>
</tr>
<tr>
<th colspan="2" align="left">
<h3 style="text-align:left;padding:5px 0px;margin:5px 0px;" style="margin-bottom: 0px;margin-top: 5px;">GSTIN Number</h3></th>
</tr>
<tr>
<th align="left">
GST No.
</th>
<td style="padding:4px;margin:3px;">
GST08526888
</td>
</tr>

<tr>
<th align="left">
Place of Supply
</th>
<td style="padding:4px;margin:3px;">
UNITED STATE
</td>
</tr>
</table> 





<hr style='border: 1px solid rgba(204, 204, 201, 0.43);'><p style='text-align: center;font-weight: bold;color: gray;'>Follow us</p><p style='text-align: center;'><a href='https://www.facebook.com'><img src='<?=base_url()?>asset/images/facebook_mail.png'  ></a>&nbsp;&nbsp;<a href='https://twitter.com/'><img src='<?=base_url()?>asset/images/twitter_mail.png'  ></a>&nbsp;&nbsp;<a href='https://www.linkedin.com/'><img src='<?=base_url()?>asset/images/linkedin_mail_2.png'></a><p style='text-align:center;color: gray;'>Sent from EazyInvoice &copy; Copyright 2018</p></div></body></html> -->
<style>
	/*table.tableizer-table {
		font-size: 12px;
		
		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;
		
	}
	.tableizer-table th {
		
		
		font-weight: bold;
		text-align: left;
		
	}
	.bank_info{
		margin-left:5px;
		list-style:disc inside;

	}
	.branch_info{
		
	}
	h1{
		color: #7965EA;
	}
	th{
		
		color: #B0B7CA;
	}
	h3{
		padding: 5px 0px;
		margin: 5px 0px;
		color: #000 !important;
	}*/
</style>
<div class="row">
<div class='col-md-12' style='width: 60%;margin: 0 auto;background: rgba(245, 245, 245, 0.35);padding:10px;'>
<?php if(@$company_profile[0]->bus_company_logo != '') {?>

        <p style='text-align:center;'><img src='<?=base_url()?>public/upload/company_logos/<?php echo $company_profile[0]->bus_id; ?>/<?php echo $company_profile[0]->bus_company_logo;?>' height="100px" width="100px" alt='Company Logo'></p>
        <?php }else { ?>
          <p style='text-align:center;'></p>
          <?php } ?>



<table style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;" class="tableizer-table" align="center" cellpadding="0" border="0">
<thead>

	<tr class="tableizer-firstrow"><th style="text-align: left;color: #B0B7CA;font-weight:bold;"></th></tr></thead><tbody>

 <tr><td style="padding:4px;margin:3px;" ><h2 >Hey <?= ucfirst($this->session->userdata['user_session']['ei_username']); ?></h2></td></tr>
 <tr><td style="padding:4px;margin:3px;" colspan="2"><p> Attached with this email you will find a spreadsheet with the details of your Invoice. </p></td></tr>
 <tr><th colspan="2"><h1 style="color: #7965EA;text-align: left;">Company Information</h1></th></tr>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Basic Info</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Company Name</th><td style="padding:4px;margin:3px;"><?php echo $company_profile[0]->bus_company_name; ?></td>
</tr>
 <tr>
 <th style="text-align: left;color: #B0B7CA;font-weight:bold;">Blling Address</th>
 <td style="padding:4px;margin:3px;"><?php $city = @$company_profile[0]->name.', ';$zip = @$company_profile[0]->bus_billing_zipcode;$zip=' '.@$zip; ?>
<p><?php echo @$company_profile[0]->bus_billing_address.','.@$company_profile[0]->bcity.','.@$company_profile[0]->bstate.','.@$company_profile[0]->bcountry.','.@$company_profile[0]->bus_billing_zipcode; ?></p></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Shipping Address</th>
 <td style="padding:4px;margin:3px;"><?php echo @$company_profile[0]->bus_shipping_address.','.@$company_profile[0]->scity.','.@$company_profile[0]->sstate.','.@$company_profile[0]->scountry.','.@$company_profile[0]->bus_shipping_zipcode; ?></td></tr>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Statutory Info</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">PAN Number</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_pancard; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">CIN Number</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_cin_no; ?></td></tr>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">GSTIN NUMBER</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">GST No.</th><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Place of Supply</th></tr>
 <?php foreach ($gst_details as $key => $value) { ?>
 <tr><td style="padding:4px;margin:3px;"><li><?= @$value->gst_no; ?></li></td><td style="padding:4px;margin:3px;"><?= @$value->place; ?></td></tr>
 <?php } ?>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Type of Company</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Type of Company</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->company_type; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Date of Incorporation</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_incorporation_date; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Revenue</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_company_revenue; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">No. of Employees</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_company_size; ?></td></tr>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Service Offered</h3></th></tr>
 <?php $keycount=1; 
 foreach (explode('|@|',@$company_profile[0]->bus_services_keywords) as $key => $kvalue) {
 if($keycount%2!=0){ ?>
<tr> <td style="padding:4px;margin:3px;"><li><?= @$kvalue; ?></li></td>
<?php } ?>
<?php if($keycount%2==0){ ?>
	<td style="padding:4px;margin:3px;"><li><?= @$kvalue; ?></li></td></tr>
<?php } $keycount++; } ?>
<tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Branch Details</h3></th></tr>
<tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Branch</th><th style="text-align: left;color: #B0B7CA;font-weight:bold;">City</th></tr>

<?php foreach ($branch_info as $key => $value) { ?>
 <tr class="branch_info">
 	
 <td style="padding:4px;margin:3px;"><li><?= @$value->cbranch_name; ?></li></td>
 <td style="padding:4px;margin:3px;"><?= @$value->cbranch_city; ?></td></tr>
 
<?php } ?> 	
<tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Bank Details</h3></th></tr>
<?php foreach ($bank_details as $key => $value) { ?>
 <tr class="bank_info"><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Bank Name</th><td style="padding:4px;margin:3px;"><?= @$value->cbank_name; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Account No.</th><td style="padding:4px;margin:3px;"><?= @$value->cbank_account_no; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Ifsc Code</th><td style="padding:4px;margin:3px;"><?= @$value->cbank_ifsc_code; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Branch Name</th><td style="padding:4px;margin:3px;"><?= @$value->cbank_branch_name; ?></td></tr>
 
<?php } ?> 
<tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Web Presence</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Website Url</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_website_url; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">List of Clients</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_loc; ?></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">case Studies</th><td style="padding:4px;margin:3px;"><?= @$company_profile[0]->bus_case_study; ?></td></tr>
 <tr><th colspan="2"><h3 style="text-align:left;padding:5px 0px;margin:5px 0px;">Social Media</h3></th></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Facebook Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_facebook; ?></a></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Twitter Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_twitter; ?></a></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Linkedin Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_linkedin; ?></a></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Youtube Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_youtube; ?></a></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">instagram Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_instagram; ?></a></td></tr>
 <tr><th style="text-align: left;color: #B0B7CA;font-weight:bold;">Google+ Url</th><td style="padding:4px;margin:3px;"><a href=""><?= @$company_profile[0]->bus_googleplus; ?></a></td></tr>
 <tr ><td style="padding:4px;margin:3px;" colspan="2"><p> For any queries write to us at contactus@xebra.in </p></td></tr>
 <tr><td style="padding:4px;margin:3px;" colspan="2"><p> Here's to some happy invoicing! </p></td></tr>
 <tr><td style="padding:4px;margin:3px;" colspan="2"><p> Team Invoice Management </p></td></tr>

</tbody></table>
<hr style='border: 1px solid rgba(204, 204, 201, 0.43);'><p style='text-align: center;font-weight: bold;color: gray;'>Follow us</p><p style='text-align: center;'><a href='https://www.facebook.com'><img src='<?=base_url()?>asset/images/facebook_mail.png' alt="facebookmail"></a>&nbsp;&nbsp;<a href='https://twitter.com/'><img src='<?=base_url()?>asset/images/twitter_mail.png' alt="twitter-mail"></a>&nbsp;&nbsp;<a href='https://www.linkedin.com/'><img src='<?=base_url()?>asset/images/linkedin_mail_2.png' alt="linkedin-mail"></a></p><p style='text-align:center;color: gray;'>Sent from Xebra &copy; Copyright 2019</p></div>
</div></body></html>