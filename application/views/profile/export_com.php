<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('Company Information');
$this->excel->getActiveSheet()->setCellValue('A1', 'Company Information');
$this->excel->getActiveSheet()->mergeCells('A1:D1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A2', 'COMPANY NAME');
$this->excel->getActiveSheet()->setCellValue('A3', @$company_profile[0]->bus_company_name);
$this->excel->getActiveSheet()->setCellValue('B2', 'BILLING ADDRESS');
$this->excel->getActiveSheet()->setCellValue('B3', @$company_profile[0]->bus_billing_address.','.@$company_profile[0]->bcity.','.@$company_profile[0]->bstate.','.@$company_profile[0]->bcountry.','.@$company_profile[0]->bus_billing_zipcode);
/*$this->excel->getActiveSheet()->setCellValue('A5', 'SHIPPING ADDRESS');
$this->excel->getActiveSheet()->setCellValue('B5', @$company_profile[0]->bus_shipping_address.','.@$company_profile[0]->scity.','.@$company_profile[0]->sstate.','.@$company_profile[0]->scountry.','.@$company_profile[0]->bus_shipping_zipcode);*/

$this->excel->getActiveSheet()->setCellValue('C2', 'PAN NUMBER');
$this->excel->getActiveSheet()->setCellValue('C3', @$company_profile[0]->bus_pancard);
$this->excel->getActiveSheet()->setCellValue('D2', 'CIN NUMBER');
$this->excel->getActiveSheet()->setCellValue('D3', @$company_profile[0]->bus_cin_no);

$this->excel->getActiveSheet()->setCellValue('E2', 'GST NO');
$this->excel->getActiveSheet()->setCellValue('F2', 'PLACE OF SUPPLY');
$gstcount=3;
foreach ($gst_details as $key => $value) {
$this->excel->getActiveSheet()->setCellValue('E'.$gstcount, $value->gst_no);
$this->excel->getActiveSheet()->setCellValue('F'.$gstcount, $value->place);
$gstcount++;
}

$this->excel->getActiveSheet()->setCellValue('G2', 'TYPE OF COMPANY');
$this->excel->getActiveSheet()->setCellValue('G3', @$company_profile[0]->company_type);

$this->excel->getActiveSheet()->setCellValue('H2', 'DATE OF INCORPORATION');
$this->excel->getActiveSheet()->setCellValue('H3', @$company_profile[0]->bus_incorporation_date);
$this->excel->getActiveSheet()->setCellValue('I2', 'REVENUE');
$this->excel->getActiveSheet()->setCellValue('I3', @$company_profile[0]->bus_company_revenue);
$this->excel->getActiveSheet()->setCellValue('J2', 'NO. OF EMPLOYEES');
$this->excel->getActiveSheet()->setCellValue('J3', @$company_profile[0]->bus_company_size);

$this->excel->getActiveSheet()->setCellValue('K2', 'Service Offered');
$this->excel->getActiveSheet()->getStyle('K2')->getFont()->setBold(true);
$lastcount=3; $keycount=1;
foreach (explode('|@|',@$company_profile[0]->bus_services_keywords) as $key => $value) {
if($keycount%2!=0){
$this->excel->getActiveSheet()->setCellValue('K'.$lastcount, $value);	
}
if($keycount%2==0){
$this->excel->getActiveSheet()->setCellValue('K'.$lastcount, $value);	
$lastcount++;
}
$keycount++;
}

/*$this->excel->getActiveSheet()->setCellValue('A'.$lastcount, 'Branch Details');
$this->excel->getActiveSheet()->mergeCells('A'.$lastcount.':D'.$lastcount);
$this->excel->getActiveSheet()->getStyle('A'.$lastcount.':D'.$lastcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('A'.$lastcount)->getFont()->setBold(true);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('A'.$lastcount, 'BRANCH NAME');
$this->excel->getActiveSheet()->getStyle('A'.$lastcount)->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B'.$lastcount, 'CITY');
$this->excel->getActiveSheet()->getStyle('B'.$lastcount)->getFont()->setBold(true);
$lastcount++;
foreach ($branch_info as $key => $value) {
$this->excel->getActiveSheet()->setCellValue('A'.$lastcount, $value->cbranch_name);
$this->excel->getActiveSheet()->setCellValue('B'.$lastcount, $value->cbranch_city);
$lastcount++;
}*/

$this->excel->getActiveSheet()->setCellValue('L2', 'Bank Details');
$this->excel->getActiveSheet()->mergeCells('L2:M2');
$this->excel->getActiveSheet()->getStyle('L2:M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('L2')->getFont()->setBold(true);
$bank_count=3;
foreach ($bank_details as $key => $value) {
$this->excel->getActiveSheet()->setCellValue('L'.$bank_count, $value->cbank_name);
$this->excel->getActiveSheet()->getStyle('L'.$bank_count)->getFont()->setBold(true);
$this->excel->getActiveSheet()->mergeCells('L'.$bank_count.':M'.$bank_count);
$inc_count = $bank_count+1;
$this->excel->getActiveSheet()->setCellValue('L'.$inc_count, 'ACCOUNT NO');
$this->excel->getActiveSheet()->setCellValue('M'.$inc_count, $value->cbank_account_no);
$inc_count = $inc_count+1;
$this->excel->getActiveSheet()->setCellValue('L'.$inc_count, 'IFSC CODE');
$this->excel->getActiveSheet()->setCellValue('M'.$inc_count, $value->cbank_ifsc_code);
$inc_count = $inc_count+1;
$this->excel->getActiveSheet()->setCellValue('L'.$inc_count, 'BRANCH NAME');
$this->excel->getActiveSheet()->setCellValue('M'.$inc_count, $value->cbank_branch_name);
$inc_count = $inc_count+1;
$this->excel->getActiveSheet()->setCellValue('L'.$inc_count, 'OPENING BANK BALANCE');
$this->excel->getActiveSheet()->setCellValue('M'.$inc_count, $value->opening_bank_balance);
$inc_count = $inc_count+1;
$this->excel->getActiveSheet()->setCellValue('L'.$inc_count, 'OPENING BANK BALANCE DATE');
$this->excel->getActiveSheet()->setCellValue('M'.$inc_count, date('d-m-Y',strtotime($value->opening_balance_date)));
$bank_count = $inc_count+2;
}

/*$this->excel->getActiveSheet()->setCellValue('N2', 'Opening Bank Balance');
$this->excel->getActiveSheet()->setCellValue('N3', $company_profile[0]->opening_bank_balance);
$this->excel->getActiveSheet()->setCellValue('O2', 'Opening Bank Balance Date');
$this->excel->getActiveSheet()->setCellValue('O3', date('d-m-Y',strtotime($company_profile[0]->opening_balance_date)));*/
$this->excel->getActiveSheet()->setCellValue('N2', 'Opening Petty Cash Balance');
$this->excel->getActiveSheet()->setCellValue('N3', $company_profile[0]->opening_petty_balance);
$this->excel->getActiveSheet()->setCellValue('O2', 'Opening Petty Cash Balance Date');
$this->excel->getActiveSheet()->setCellValue('O3', date('d-m-Y',strtotime($company_profile[0]->petty_balance_date)));
$this->excel->getActiveSheet()->setCellValue('P2', 'Opening Cash Balance');
$this->excel->getActiveSheet()->setCellValue('P3', $company_profile[0]->opening_cash_balance);
$this->excel->getActiveSheet()->setCellValue('Q2', 'Opening Cash Balance Date');
$this->excel->getActiveSheet()->setCellValue('Q3', date('d-m-Y',strtotime($company_profile[0]->cash_balance_date)));
$this->excel->getActiveSheet()->setCellValue('R2', 'Nature of Business');
$this->excel->getActiveSheet()->setCellValue('R3', $company_profile[0]->nature_of_bus);
$this->excel->getActiveSheet()->setCellValue('S2', 'Financial Start Date');
$this->excel->getActiveSheet()->setCellValue('S3', date('d-m-Y',strtotime($company_profile[0]->bus_fy_startdate)));
$this->excel->getActiveSheet()->setCellValue('T2', 'Financial End Date');
$this->excel->getActiveSheet()->setCellValue('T3', date('d-m-Y',strtotime($company_profile[0]->bus_fy_enddate)));
$this->excel->getActiveSheet()->setCellValue('U2', 'Currency');
$this->excel->getActiveSheet()->setCellValue('U3', $company_profile[0]->currency);
$this->excel->getActiveSheet()->setCellValue('V1', 'Web Presence');
$this->excel->getActiveSheet()->mergeCells('V1:W1');
$this->excel->getActiveSheet()->getStyle('V1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('V2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('V2', 'WEBSITE URL');
$this->excel->getActiveSheet()->setCellValue('V3', $company_profile[0]->bus_website_url);
$this->excel->getActiveSheet()->setCellValue('W2', 'LIST OF CLIENTS');
$this->excel->getActiveSheet()->setCellValue('W3', $company_profile[0]->bus_loc);
$this->excel->getActiveSheet()->setCellValue('X2', 'CASE STUDIES');
$this->excel->getActiveSheet()->setCellValue('X3', $company_profile[0]->bus_case_study);

$this->excel->getActiveSheet()->setCellValue('AA1', 'Social Media');
$this->excel->getActiveSheet()->mergeCells('AA1:AB1');
$this->excel->getActiveSheet()->getStyle('AA1:AB1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('AA1')->getFont()->setBold(true);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AA2', 'FACEBOOK URL');
$this->excel->getActiveSheet()->setCellValue('AA3', $company_profile[0]->bus_facebook);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AB2', 'TWITTER URL');
$this->excel->getActiveSheet()->setCellValue('AB3', $company_profile[0]->bus_twitter);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AC2', 'LINKEDIN URL');
$this->excel->getActiveSheet()->setCellValue('AC3', $company_profile[0]->bus_linkedin);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AD2', 'YOUTUBE URL');
$this->excel->getActiveSheet()->setCellValue('AD3', $company_profile[0]->bus_youtube);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AE2', 'INSTAGRAM URL');
$this->excel->getActiveSheet()->setCellValue('AE3', $company_profile[0]->bus_instagram);
$lastcount++;
$this->excel->getActiveSheet()->setCellValue('AF2', 'Whatsapp Number');
$this->excel->getActiveSheet()->setCellValue('AF3', $company_profile[0]->bus_googleplus);


$filename='Company Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
	ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>