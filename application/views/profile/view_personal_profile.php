 <?php $this->load->view('template/header.php');?>
 <link href="<?php echo base_url();?>public/css/style-form.css" type="text/css" rel="stylesheet">
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->

    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
       <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
<?php foreach($profile_data as $row) { ?>

        <section id="content" class="view bg-white">
                <div class="container">
                  <div class="list-top">
                      <div class="list-left">
                        <div class="top-barl">
                          <h4><a href="<?php echo base_url(); ?>profile/manage_personal_profile"><span>Back to My Profiles</span></a></h4>
                        </div> <!-- top-bar END -->
                      </div> <!-- list-left END -->
                        <div class="list-right">
                            
                              <div class="right-2">
                              <a href="#">Delete this profile</a>
                              </div> <!-- right-2 end -->
                              <div class="right-1">
                              <a target="_blank" href="<?php echo base_url(); ?>profile/print-personal-profile/<?php echo $row->reg_id; ?>"><i class="icon print"></i></a>
                            </div><!-- right-1 end -->
                          
                        </div> <!-- list-right end -->
                  </div> <!-- list-top END -->
                </div>
          <div class="container">
                <div id="tbl-bg-view">
                  <div class="tbl-bg-view-l">
                    
                  </div>
                  <div class="tbl-bg-view-r">
                    
                  </div>
                </div>
            <div class="data-list">
                <div class="first-list">
                    <div class="first-left" style="display: flex;">
                   
                    <?php if($row->reg_profile_image !=''){  ?>
                      <img style="border-radius: 50%; box-shadow: 0px 0px 0px 2px #50e3c2; width: 35px; height: 35px; margin: 0 10px 0 50px;" src="<?php echo base_url();?>public/upload/personal_images/<?php echo $row->reg_id; ?>/thumbs/<?php echo $row->reg_profile_image; ?>" alt="personal-image">  
                   <?php } else{ ?>
                      <a href="#"><i class="material-icons">camera_alt</i></a>                      
                    <?php } ?>  
                        <h5><?php echo ucfirst($row->reg_username); ?></h5>
                        
                    </div> <!-- first-left end-->
                      <div class="first-right">
                        <a href="<?php echo base_url();?>profile/edit_personal_profile/<?php echo $row->reg_id; ?>"><i class="material-icons" style="color: #fff; font-size: 20px;">edit</i></a>
                      </div> <!-- first-right end-->
                  
                </div> <!-- first-list end-->
                <div class="data-lists">
                            <div class="data-list-left">
                              <i class="icon company"></i>
                              <span>COMPANY</span>

                                <p><?php echo @$company_name[0]->bus_company_name; ?></p>  
                            </div> <!-- data-list-left end -->
                           <div class="data-list-left">
                              <i class="icon mail"></i> <span>EMAIL ADDRESS</span>
                                <p class="data-list-p"><?php echo $row->reg_email; ?></p>  
                            </div> <!-- data-list-left end -->
                             <div class="data-list-left">
                              <i class="icon access"></i>
                              <span>ACCESS</span>
                              <?php if($row->reg_admin_type =='-1'){ ?>
                                <p>ADMIN</p> 
                                <?php } else{ ?>
                                <p><?php echo $access_data[0]->access; ?></p> 
                               <?php } ?>

                            </div> <!-- data-list-left end -->
                             <div class="data-list-left">
                              <i class="material-icons"  style="color: #51e2c5;">phone_iphone</i><span>MOBILE NO.</span>
                                <p class="data-list-p">+91 <?php echo $row->reg_mobile; ?></p>
                            </div> <!-- data-list-left end -->

                            <div class="data-list-left">
                              <i class="material-icons"  style="color: #51e2c5;">phone_iphone</i><span>DESIGNATION</span>
                                <p class="data-list-p"><?php echo $row->reg_designation; ?></p>
                            </div>

                            <div class="data-list-left">
                              <i class="material-icons"  style="color: #51e2c5;">phone_iphone</i><span>DIGITAL SIGNATURE</span>
                                <?php if($row->reg_degital_signature !=''){  ?>
                                <p><img style="border-radius: 50%; box-shadow: 0px 0px 0px 2px #50e3c2; width: 35px; height: 35px; margin: 0 10px 0 50px;" src="<?php echo base_url();?>public/upload/degital_signature/<?php echo $row->reg_id; ?>/thumbs/<?php echo $row->reg_degital_signature; ?>" alt="digi-signature">  
                               <?php } else{ ?>
                                  <a href="#"><i class="material-icons">camera_alt</i></a>                      
                                <?php } ?>
                              </p>
                            </div>
                    </div>
                            
              </div> <!-- data-lists end -->
             
            </div> <!-- data-list end -->
             
          </div><!--Container ends here-->
           
        </section>
<?php } ?>        
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->

   <?php $this->load->view('template/footer.php');?>