<?php $this->load->view('template/header.php');?>
<style type="text/css">
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after {
      width: 270px;
   }
   
   .whatsapp{
	   margin-left:-8px !important;
   }
</style>
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12 m6">
                    <a class="go-back underline" href="<?php echo base_url();?>profile/company-profile">Back to My Company Profiles</a>
                  </div>
                  <div class="col l6 s12 m6">
                    <div class="right-2">
                      <?php $id=$company_profile[0]->bus_id; ?>
                      
                     
                    </div>
                    <div class="right-1">
                      <a href="javascript:void(0);"><i class="icon print"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12 view-box blue-box before-green-bg green-bg-right">
                  <div class="box-title-info">
                    <div class="row">
                      <div class="col l12 s12 m12">
                        <div class="col l1 s1 m1">
                          <div class="row">
                            <?php if($company_profile[0]->bus_company_logo != '') {?>
                            <div class="col l12 m12 s12 " style="margin: -4px 40px;">
                                  <div class="uploader-placeholder" style="box-shadow: 0px 0px 0px 2px #50e3c2; border:none; width: 35px; height: 35px; border-radius: 50%;">
                                    <input type="hidden" id="image_info" name="image_info" value="<?php echo ($company_profile[0]->bus_company_logo)?$company_profile[0]->bus_company_logo:''; ?>">
                                    <input type="hidden" id="img_folder" name="img_folder" value="<?php echo ($company_profile[0]->bus_company_logo)?'company_logos/'.$company_profile[0]->bus_id :'';?>">
                                  </div>
                            </div>
                            <?php }else { ?>
                              <div class="col l12 m12 s12 right-align">
                                <a href="javascript:void(0);"><i class="material-icons view-icons">camera_alt</i></a>
                              </div>
                              <?php } ?>
                          </div>
                        </div>
                        <div class="col l10 s10 m10">
                          <h4 class="view-box-title"><?php echo $company_profile[0]->bus_company_name; ?></h4>
                        </div>
                        <div class="col l1 s1 m1">
                          <a href="<?php echo base_url(); ?>profile/edit-company-profile/<?php echo $id; ?>"><i class="material-icons" style="color: #fff; font-size: 20px;">edit</i></a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="address_info">
                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="row">
                          <div class="col l6 s6 m6">
                            <div class="col l12 m12 s12">

                              <div class="row address-block">
                                <div class="col l2 s1 m1 right-align"><i class="material-icons view-icons">location_on</i></div>
                                <div class="col l10 s10 m10">
                                  <span class="view-label">BILLING ADDRESS</span>
                                  <p class="view-content"><?php echo $company_profile[0]->bus_billing_address; ?></p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col l2 s1 m1 right-align"><i class="state-icons view-icons"></i></div>
                                <div class="col l10 s10 m10">
                                  <div class="row">
                                    <div class="col s12 m6 l6">
                                      <span class="view-label">State</span>
                                      <p class="view-content">
                                        <?php
                                        $states = $this->Profile_model->selectData("states",'*',array("state_id"=>$company_profile[0]->bus_billing_state));
                                        $state = (count($states) > 0)? $states[0]->state_name:'';
                                        echo $state;
                                        ?>
                                      </p>
                                    </div>
                                    <div class="col s12 m6 l6">
                                      <div class="row">
                                        <div class="col l2 s2 m2 right-align">
                                          <i class="country-icons view-icons"></i>
                                        </div>
                                        <div class="col l10 s10 m10">
                                          <span class="view-label">country</span>
                                          <p class="view-content">
                                            <?php
                                            $countries = $this->Profile_model->selectData("countries",'*',array("country_id"=>$company_profile[0]->bus_billing_country));
                                            $country = (count($countries) > 0)? $countries[0]->country_name:'';
                                            echo $country;
                                            ?>
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="row" hidden>
                            <div class="col l6 s6 m6">

                              <div class="col l12 m12 s12">

                                <div class="row address-block">
                                  <div class="col l2 s1 m1 right-align"><i class="material-icons view-icons">location_on</i></div>
                                  <div class="col l10 s10 m10">
                                    <span class="view-label">SHIPPING ADDRESS</span>
                                    <p class="view-content"><?php echo $company_profile[0]->bus_shipping_address; ?></p>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col l2 s1 m1 right-align"><i class="state-icons view-icons"></i></div>
                                  <div class="col l10 s10 m10">
                                    <div class="row">
                                      <div class="col s12 m6 l6">
                                        <span class="view-label">State</span>
                                        <p class="view-content">
                                          <?php
                                           $states = $this->Profile_model->selectData("states",'*',array("state_id"=>$company_profile[0]->bus_shipping_state));
                                          $state = (count($states) > 0)? $states[0]->state_name:'';
                                          echo $state;
                                          ?>
                                        </p>
                                      </div>
                                      <div class="col s12 m6 l6">
                                        <div class="row">
                                          <div class="col l2 s2 m2 right-align">
                                            <i class="country-icons view-icons"></i>
                                          </div>
                                          <div class="col l10 s10 m10">
                                            <span class="view-label">country</span>
                                            <p class="view-content">
                                              <?php
                                              $countries = $this->Profile_model->selectData("countries",'*',array("country_id"  =>$company_profile[0]->bus_shipping_country));
											  $country = (count($countries) > 0)? $countries[0]->country_name:'';
                                              echo $country;
                                              ?>
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box before-red-bg red-bg-right">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Statutory Info</h4>
                    </div>
                  </div>

                  <div class="col l12 s12 m12">
                    <div class="row pannumber-section">
                      <div class="col l1 s12 m2"></div>
                      <div class="col l2 s11 m11 pannumber">
                        <label>PAN NUMBER</label>
                        <input type="text" disabled value="<?php echo $company_profile[0]->bus_pancard;?>">
                      </div>
                      <div class="col l1 s1 m1">
                        <!-- <img src="<?php echo base_url(); ?>asset/images/edit-black.png" alt="" class="edit-icon"> -->
                      </div>
                    </div>

                    <div class="row">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l2 s11 m11">
                        <label>GSTIN NUMBER <span class="gray badge"><?php echo count($gst_details) ?></span></label>
                      </div>
                    </div>

                    <div class="row mini-box-row-cover">
                      <div class="col l1 s1 m1"></div>
                      <div class="col l10 s10 m11">
                        <div class="row">
                        <?php
                        if($gst_details != '') {
                          foreach($gst_details as $gst) {
                        ?>
                            <div class="col s12 m6 l6">
                              <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                <div class="row">
                                  <div class="col l1 s1 m1 checkbox-cover">
                                      <input type="checkbox" class="purple" disabled>
                                      <label></label>
                                  </div>
                                  <div class="col l5 s4 m4">
                                    <div class="gst-no-one">
                                      <label>GST NO.</label>
                                      <input type="text" disabled value="<?php echo $gst->gst_no;?>">
                                    </div>
                                  </div>
                                  <div class="col l4 s4 m4">
                                    <label>PLACE OF SUPPLY</label>
                                      <input type="text" disabled value="<?php echo $gst->place;?>">
                                  </div>
                                  <div class="col l2 s4 m4">
                                    <div class="hover-edit">
                                      <!-- <a href="javascript:void(0);"><img src="<?php echo base_url();?>asset/images/edit-black.png" alt="" class="edit-munbai-icon"></a> -->
                                      <!-- <a href="javascript:void(0);"><img src="<?php echo base_url();?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        <?php
                          }
                        }
                        ?>
                        </div>
                      </div>
                      <div class="col l1 s1 m1"></div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Type of Company</h4>
                    </div>
                  </div>

                  <div class="row typeofcompany-fields">
                    <div class="inner_row_container">
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <?php
                            $company_type = $this->Profile_model->selectData("company_type",'*',array('id'=>$company_profile[0]->bus_company_type));
                            $company_type = (count($company_type) > 0) ? $company_type[0]->company_type : '';
                            ?>
                            <label>TYPE OF COMPANY</label>
                            <input type="text" disabled value="<?php echo $company_type; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <label>DATE OF INCORPORATION</label>
                            <input type="text" disabled value="<?php echo ($company_profile[0]->bus_incorporation_date != '' && $company_profile[0]->bus_incorporation_date != '0000-00-00')?date("d-m-Y",  strtotime($company_profile[0]->bus_incorporation_date)):''; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <?php
                            $company_revenue = '';
                            if($company_profile[0]->bus_company_revenue != '') {
                              $company_revenue = $company_profile[0]->bus_company_revenue;
                              if($company_revenue == '5'){
                                $company_revenue = '&lt; 5 Cr';
                              }
                              elseif($company_revenue == '5-20'){
                                $company_revenue = '5-20 Cr';
                              }
                              elseif($company_revenue == '20-50'){
                                $company_revenue = '20-50 Cr';
                              }
                              elseif($company_revenue == '50-100'){
                                $company_revenue = '50-100 Cr';
                              }
                              elseif($company_revenue == '100+'){
                                $company_revenue = '100 Cr+';
                              }
                              else { 
                                $company_revenue = '';
                              }
                            } else {
                              $company_revenue = '';
                            }
                            ?>
                            <label>REVENUE</label>
                            <input type="text" disabled value="<?php echo $company_revenue; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <label>NO. OF EMPLOYEES</label>
                            <input type="text" disabled value="<?php echo $company_profile[0]->bus_company_size; ?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

				  <div class="row typeofcompany-fields">
                    <div class="inner_row_container">
						<div class="col l3 s12 m6">
							<div class="row">
							<div class="col l2 s1 m1">
								<img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
							</div>
							<div class="col l10 s11 m11">
								<label>OPENING CASH BALANCE</label>
								<input type="text" disabled value="<?php echo $company_profile[0]->opening_cash_balance; ?>">
							</div>
							</div>
						</div>
						<div class="col l3 s12 m6">
							<div class="row">
							<div class="col l2 s1 m1">
								<img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
							</div>
							<div class="col l10 s11 m11">
								<label>OPENING CASH BALANCE DATE</label>
								<input type="text" disabled value="<?php echo date('d-m-Y',strtotime($company_profile[0]->cash_balance_date)); ?>">
							</div>
							</div>
						</div>
						<div class="col l3 s12 m6">
							<div class="row">
							<div class="col l2 s1 m1">
								<img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
							</div>
							<div class="col l10 s11 m11">
								<label>OPENING PETTY CASH BALANCE</label>
								<input type="text" disabled value="<?php echo $company_profile[0]->opening_petty_balance; ?>">
							</div>
							</div>
						</div>
						<div class="col l3 s12 m6">
							<div class="row">
							<div class="col l2 s1 m1">
								<img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
							</div>
							<div class="col l10 s11 m11">
								<label>OPENING PETTY CASH BALANCE DATE</label>
								<input type="text" disabled value="<?php echo date('d-m-Y',strtotime($company_profile[0]->petty_balance_date)); ?>">
							</div>
							</div>
						</div>
					</div>
				  </div>
					
                  <div class="row">
                    <div class="col l12 s12 m12">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l2 s11 m11">
                         <?php 
							  $tot_service = '';
							  if($company_profile[0]->bus_services_keywords != '') {
								$tot_services = explode("|@|",$company_profile[0]->bus_services_keywords);
								$tot_service = count($tot_services);
							  } else {
								$tot_service = '0';  
							  }
							  ?>
                        <label>SERVICES OFFERED  <span class="gray badge"><?php echo $tot_service; ?></span></label>
                      </div>
                    </div>
                  </div>

                  <div class="row mini-box-row-cover">
                    <div class="col l1 s1 m1"></div>
                    <div class="col l10 s10 m11">
                      <div class="row">
                      <?php
      					  $services = $company_profile[0]->bus_services_keywords;
                            if($services != '') {
      						  $services = explode("|@|",$services);
      						  $i = 1;
                              foreach($services as $service) {
                      ?>
                          <div class="col s12 m6 l6">
                            <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                              <div class="row">
                                <div class="col l1 s1 m1 checkbox-cover">
                                    <input type="checkbox" class="purple" disabled>
                                    <label></label>
                                </div>
                                <div class="col l9 s9 m9">
                                  <div class="gst-no-one">
                                    <label>KEYWORD <?php echo $i;?></label>
                                    <input id="social<?php echo $i;?>" name="social<?php echo $i;?>" type="text" value="<?php echo $service;?>" disabled="">
                                  </div>
                                </div>
                                <div class="col l2 s4 m4">
                                  <div class="hover-edit">
                                    <!-- <a href="javascript:void(0);"><img src="<?php echo base_url();?>asset/images/edit-black.png" alt="" class="edit-munbai-icon"></a>
                                    <a href="javascript:void(0);"><img src="<?php echo base_url();?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      <?php $i++; } } ?>
                      </div>
                    </div>
                    <div class="col l1 s1 m1"></div>
                  </div>

                  <div class="row branch-box-view" hidden>
                    <div class="col l12 s12 m12">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l2 s11 m11">
                        <label>BRANCH DETAILS <span class="gray badge"><?php echo ($branch_info != '')?count($branch_info):'0'; ?></span></label>
                      </div>
                    </div>
                  </div>

                  <div class="row mini-box-row-cover">
                    <div class="col l1 s1 m1"></div>
                    <div class="col l10 s10 m11">
                      <div class="row">
                        <div class="row bottom">
                      <div class="col s12 m12 l12">
                        <div class="col s12 m12 l12 pl-5 pr-5">
                          <div class="row">
                          <?php
						  if($branch_info != '') {
						  foreach($branch_info as $branch) {
						  ?>  
                            <div class="col s12 m12 l6 pr-4">
                              <div class="row">
                                <div class="col s12 m12 l12 view-inner-box">
                                  <div class="col s1 m1 l1">
                                    <input type="checkbox" id="check<?php echo $branch->cbranch_id;?>" disabled="disabled" />
                                    <label for="check<?php echo $branch->cbranch_id;?>"></label>
                                  </div>
                                  <div class="col s8 m8 l5 pl-6">
                                    <label class="input-lable" for="social<?php echo $branch->cbranch_id;?>">BRANCH</label>
                                    <input id="social<?php echo $branch->cbranch_id;?>" type="text" value="<?php echo $branch->cbranch_name; ?>" disabled="">
                                  </div>
                                  <div class="col s8 m8 l4">
                                    <label class="input-lable" for="social1">CITY</label>
                                    <input id="social<?php echo $branch->cbranch_id;?>" type="text" value="<?php echo $branch->cbranch_city; ?>" disabled="">
                                  </div>
                                  <div class="col s1 m1 l1 ">
                                    <!-- <a href="javascript:void(0);" class="edit box-icon">
                                      <img src="<?php echo base_url(); ?>asset/images/edit-black.png" alt="">
                                    </a> -->
                                  </div>
                                  <div class="col s1 m1 l1 ">
                                    <!-- <a href="javascript:void(0);" class="delete box-icon">
                                      <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="">
                                    </a> -->
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php } } ?>

                          </div>
                          
                        </div>
                      </div>
                    </div>
                      </div>
                    </div>
                    <div class="col l1 s1 m1"></div>
                  </div>

                </div>
              </div>


              <div class="row">
                <div class="col l12 m12 s12 view-box white-box">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Integrate Payment Gateway</h4>
                    </div>
                  </div>

                  <div class="row typeofcompany-fields">
                    <div class="inner_row_container">
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <label>Api Key</label>
                            <input type="text" disabled value="<?php echo $company_profile[0]->api_key;  ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s12 m6">
                        <div class="row">
                          <div class="col l2 s1 m1">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                          <div class="col l10 s11 m11">
                            <label>Auth Token</label>
                            <input type="text" disabled value="<?php echo $company_profile[0]->auth_token;  ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s12 m6">
          
                      </div>

                    </div>
                  </div>

                </div>
              </div>

              <!-- <div class="row">
                <div class="interate-blue-bar col l12 s12 m12">
                  <div class="inner_row_container">
                    <div class="row">
                      <div class="col l12 s12 m12">
                        Integrate Payment Gateway
                        <div class="right"><img src="<?php //echo base_url(); ?>asset/images/arrow.png" alt="" class="arrow-icon"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box after-red-bg red-bg-left">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Documents</h4>
                    </div>
                  </div>

                  <div class="col l12 s12 m12 pb-3">
                    <div class="row">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div><?php 
					  $legal=0;$purchase=0;$other_d=0;$Memorandum=0;
					  foreach($legal_documents as $l){
					  if($l->legal_doc_type=='legal'){
							 $legal++;	
						}
						if($l->legal_doc_type=='purchase_order'){
							 $purchase++;	
						}
						
					  }
					 foreach($other_documents as $o){
					  if($o->other_doc_type=='other'){
							 $other_d++;	
						}
						if($o->other_doc_type=='Memorandum'){
							 $Memorandum++;	
						}
						
					  } 					  ?>
                      <div class="col l3 s11 m11">
                        <label class="lower-label">Legal documents <span class="gray badge"><?php echo ($legal_documents != '')?$legal:'0'; ?></span></label>
                      </div>
                      <div class="col l5 s11 m11 right">
                        <div class="row">
                          <div class="col l10 m10 s10">
                            <div class="row">
                              <div class="col l10 s10 m10">
                                <p class="file_size_notify">File size should be no more then 500kb.</p>
                              </div>
                              <div class="col l2 s2 m2">
                                <div class="row">
                                  <i class="clould-icon"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col l2 m2 s2"></div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <?php
                          if($legal_documents != ''){
                            foreach($legal_documents as $legal) {
							if($legal->legal_doc_type=='legal' ) {
                          ?>
                            <div class="row document-box-view blueborder">
                              <div class="col l4 m4 s6 filedetails">
                                <div class="col l4 m4 s4 text-center">
                                  <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                </div>
                                <div class="col l8 m8 s8">
                                  <div clas="row">
                                    <p class="filename"><a style="color: #5E5F61;" href="<?php echo base_url(); ?>public/upload/company_legal_documents/<?php echo $legal->legal_document; ?>" target="_blank"><?php echo $legal->legal_document; ?></a></p>
                                    <span class="filesize">File Size <?php echo $legal->legal_document_size; ?> kb</span>
                                  </div>
                                </div>

                              </div>
                              <div class="col l4 m4 s6">
                                <div class="row">
                                  <div class="col l6 m6 s6 start-date-box">
                                    <label class="active">Start Date</label>
                                    <input type="text" value="<?php echo ($legal->legal_start_date != '' && $legal->legal_start_date != '0000-00-00')?date("d-m-Y",  strtotime($legal->legal_start_date)):''; ?>" disabled="">
                                  </div>
                                  <div class="col l6 m6 s6 end-date-box">
                                    <label class="active">End Date</label>
                                    <input type="text" value="<?php echo ($legal->legal_end_date != '' && $legal->legal_end_date != '0000-00-00')?date("d-m-Y",  strtotime($legal->legal_end_date)):''; ?>" disabled="">
                                  </div>
                                </div>
                              </div>
                              <div class="col l3 m3 s5 reminder">
                                <?php
                                $remainder = $legal->legal_reminder;
                                if($remainder == '1'){
                                  $remainder ='One week before';  
                                }elseif($remainder == '2'){
                                  $remainder ='Fortnight';
                                }elseif($remainder == '3'){
                                  $remainder ='One month before';
                                }elseif($remainder == '4'){
                                  $remainder ='Two months before';
                                }else{
                                  $remainder ='';
                                }
                                ?>   
                                <p>
                                  <img src="<?php echo base_url(); ?>asset/images/alarom.png" alt="alarm" class="alarom"> 
                                  <?php echo $remainder; ?>
                                </p>
                                
                              </div>
                              <div class="col l1 m1 s1 text-center">
                                <!-- <a href="javascript:void(0);"> <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete"></a> -->
                              </div>
                            </div>
                          <?php
							} }
                          }
                          ?>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="col l12 s12 m12 pb-3">
                    <div class="row">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s11 m11">
                        <label class="lower-label">Purchase Order <span class="gray badge"><?php echo ($legal_documents != '')?$purchase:'0'; ?></span></label>
                      </div>
                      <div class="col l5 s11 m11 right">
                        <div class="row">
                          <div class="col l10 m10 s10">
                            <div class="row">
                              <div class="col l10 s10 m10">
                                <p class="file_size_notify">File size should be no more then 500kb.</p>
                              </div>
                              <div class="col l2 s2 m2">
                                <div class="row">
                                  <i class="clould-icon"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col l2 m2 s2"></div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <?php
                        	if($legal_documents != '') {
							foreach($legal_documents as $po) {
								if($po->legal_doc_type=='purchase_order' ) {
                          ?>
                            <div class="row document-box-view greenborder">
                              <div class="col l4 m4 s6 filedetails">
                                <div class="col l4 m4 s4 text-center">
                                  <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                </div>
                                <div class="col l8 m8 s8">
                                  <div clas="row">
                                    <p class="filename"><a style="color: #5E5F61;" href="<?php echo base_url(); ?>public/upload/company_po_documents/<?php echo $po->legal_document; ?>" target="_blank"><?php echo $po->legal_document; ?></a></p>
                                    <span class="filesize">File Size <?php echo $po->legal_document_size; ?> kb</span>
                                  </div>
                                </div>

                              </div>

                              <?php /* ?>
                              <div class="col l4 m4 s6">
                                <div class="row">
                                  <div class="col l6 m6 s6 start-date-box">
                                    <label class="active">Purchase Order</label>
                                    <input type="text" value="<?php echo $po->po_num; ?>" disabled="">
                                  </div>
                                  <div class="col l6 m6 s6 end-date-box">
                                    <label class="active">Purchase Order Date</label>
                                    <input type="text" value="<?php echo ($po->po_date != '' && $po->po_date != '0000-00-00')?date("d-m-Y",  strtotime($po->po_date)):''; ?>" disabled="">
                                  </div>
                                </div>
                              </div>

                              <?php */ ?>

                              <div class="col l4 m4 s6">
                                <div class="row">
                                  <div class="col l6 m6 s6 start-date-box">
                                    <label class="active">Start Date</label>
                                    <input type="text" value="<?php echo ($po->legal_start_date != '' && $po->legal_start_date != '0000-00-00')?date("d-m-Y",  strtotime($po->legal_start_date)):''; ?>" disabled="">
                                  </div>
                                  <div class="col l6 m6 s6 end-date-box">
                                    <label class="active">End Date</label>
                                    <input type="text" value="<?php echo ($po->legal_end_date != '' && $po->legal_end_date != '0000-00-00')?date("d-m-Y",  strtotime($po->legal_end_date)):''; ?>" disabled="">
                                  </div>
                                </div>
                              </div>
                              <div class="col l3 m3 s5 reminder">
                                <?php
                                $remainder = $po->legal_reminder;
                                if($remainder == '1'){
                                  $remainder ='One week before';  
                                }elseif($remainder == '2'){
                                  $remainder ='Fortnight';
                                }elseif($remainder == '3'){
                                  $remainder ='One month before';
                                }elseif($remainder == '4'){
                                  $remainder ='Two months before';
                                }else{
                                  $remainder ='';
                                }
                                ?>
                                <p>
                                  <img src="<?php echo base_url(); ?>asset/images/alarom.png" alt="alarm" class="alarom"> 
                                  <?php echo $remainder; ?>
                                </p>
                                
                              </div>
                              <div class="col l1 m1 s1 text-center">
                                <!-- <a 	href="javascript:void(0);"> <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete"></a> -->
                              </div>
                            </div>
                          <?php
								} }
                          }
                          ?>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="col l12 s12 m12 pb-3">
                    <div class="row">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s11 m11">
                        <label class="lower-label">Other Documents <span class="gray badge"><?php echo ($other_documents != '')?$other_d:'0'; ?></span></label>
                      </div>
                      <div class="col l5 s11 m11 right">
                        <div class="row">
                          <div class="col l10 m10 s10">
                            <div class="row">
                              <div class="col l10 s10 m10">
                                <p class="file_size_notify">File size should be no more then 500kb.</p>
                              </div>
                              <div class="col l2 s2 m2">
                                <div class="row">
                                  <i class="clould-icon"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col l2 m2 s2"></div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <?php
                          if($other_documents != ''){
                            foreach($other_documents as $other) {
							 if($other->other_doc_type=='other' ) {	
                          ?>
                            <div class="row document-box-view greenborder">
                              <div class="col l4 m4 s6 filedetails">
                                <div class="col l4 m4 s4 text-center">
                                  <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                </div>
                                <div class="col l8 m8 s8">
                                  <div clas="row">
                                    <p class="filename"><a style="color: #5E5F61;" href="<?php echo base_url(); ?>public/upload/company_other_documents/<?php echo $other->other_doc_name; ?>" target="_blank"><?php echo $other->other_doc_name; ?></a></p>
                                    <span class="filesize">File Size <?php echo $other->other_doc_size; ?> kb</span>
                                  </div>
                                </div>
                              </div>

                              <div class="col l4 m4 s6">
                                <div class="row">
                                  <div class="col l6 m6 s6 other_document-name">
                                    <label class="active">Document Name</label>
                                    <input type="text" value="<?php echo  $other->other_document;?>" disabled="">
                                  </div>
                                </div>
                              </div>
                              <div class="col l1 m1 s1 text-center other_document-deleted right">
                                <!-- <a href="javascript:void(0);"> <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete"></a> -->
                              </div>
                            </div>
                          <?php
							 } }
                          }
                          ?>
                        </div>
                      </div>
                    </div>

                  </div>
				<div class="col l12 s12 m12 pb-3">
                    <div class="row">
                      <div class="col l1 s12 m2">
                        <div class="row">
                          <div class="col l6 s6 m6"></div>
                          <div class="col l6 s6 m6">
                            <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                          </div>
                        </div>
                      </div>
                      <div class="col l3 s11 m11">
                        <label class="lower-label">Memorandum of Association <span class="gray badge"><?php echo ($other_documents != '')?$Memorandum:'0'; ?></span></label>
                      </div>
                      <div class="col l5 s11 m11 right">
                        <div class="row">
                          <div class="col l10 m10 s10">
                            <div class="row">
                              <div class="col l10 s10 m10">
                                <p class="file_size_notify">File size should be no more then 500kb.</p>
                              </div>
                              <div class="col l2 s2 m2">
                                <div class="row">
                                  <i class="clould-icon"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col l2 m2 s2"></div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <?php
                          if($other_documents != ''){
                            foreach($other_documents as $mdoc) {
							 if($mdoc->other_doc_type=='Memorandum' ) {	
                          ?>
                            <div class="row document-box-view blueborder">
                              <div class="col l4 m4 s6 filedetails">
                                <div class="col l4 m4 s4 text-center">
                                  <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                </div>
                                <div class="col l8 m8 s8">
                                  <div clas="row">
                                    <p class="filename"><a style="color: #5E5F61;" href="<?php echo base_url(); ?>public/upload/company_moa_documents/<?php echo $mdoc->other_doc_name; ?>" target="_blank"><?php echo $mdoc->other_doc_name; ?></a></p>
                                    <span class="filesize">File Size <?php echo $mdoc->other_doc_size; ?> kb</span>
                                  </div>
                                </div>
                              </div>

                              <div class="col l4 m4 s6">
                                <div class="row">
                                  <div class="col l6 m6 s6 other_document-name">
                                    <label class="active">Document Name</label>
                                    <input type="text" value="<?php echo  $mdoc->other_document;?>" disabled="">
                                  </div>
                                </div>
                              </div>
                              <div class="col l1 m1 s1 text-center other_document-deleted right">
                                <!-- <a href="javascript:void(0);"> <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete"></a> -->
                              </div>
                            </div>
                          <?php
							 } }
                          }
                          ?>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box after-green-bg green-bg-right">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Bank Details</h4>
                    </div>
                  </div>

                  <div class="col l12 m12 s12">
                    <div class="row">
					<?php foreach($bank_details as $bank){ ?>
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <div class="row document-box-view blueborder bankdetails">
                            <div class="row">
                              <div class="col l1 s1 m1 text-center check-icon divider-right">
                                <img src="<?php echo base_url(); ?>asset/images/icic-right.png" alt="right" class="icic-right"> 
                              </div>
                              <div class="col l2 s2 m2">
                                <label class="active">Bank Name</label>
                                <input type="text" value="<?php echo ($bank->cbank_name != '')?$bank->cbank_name:''; ?>" disabled="">
                              </div>
                              <div class="col l2 s2 m2">
                                <label class="active">Account NO.</label>
                                <input type="text" value="<?php echo substr($bank->cbank_account_no,0,2); ?>XXX XXX<?php echo substr($bank->cbank_account_no,-4); ?>" disabled="">
                              </div>
                              <div class="col l2 s2 m2">
                                <label class="active">IFSC CODE</label>
                                <input type="text" value="<?php echo ($bank->cbank_ifsc_code != '')?$bank->cbank_ifsc_code:''; ?>" disabled="">
                              </div>
                              <div class="col l2 s2 m2">
                                <label class="active">BRANCH NAME</label>
                                <input type="text" value="<?php echo ($bank->cbank_branch_name != '')?$bank->cbank_branch_name:''; ?>" disabled="">
                              </div>
							  <div class="col l2 s2 m2">
                                <label class="active">OPENING BANK BALANCE</label>
                                <input type="text" value="<?php echo ($bank->opening_bank_balance != '')?$bank->opening_bank_balance:''; ?>" disabled="">
                              </div>
							  <div class="col l3 s3 m3">
                                <label class="active">OPENING BANK BALANCE DATE</label>
                                <input type="text" value="<?php echo ($bank->opening_balance_date != '')?date('d-m-Y',strtotime($bank->opening_balance_date)):''; ?>" disabled="">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
					<?php } ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box after-red-bg red-bg-left">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Customization</h4>
                    </div>
                  </div>

                  <div class="col l12 m12 s12">
                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <div class="row fields-row">
                            <div class="col l7 s7 m12">
                              <div class="row">
                                <div class="col l4 m4 s6">
                                  <div class="row">
                                    <label class="active date-icon"><span class="green-date-icon"></span> Start Date</label>
                                    <input type="text" value="<?php echo ($company_profile[0]->bus_fy_startdate != '' && $company_profile[0]->bus_fy_startdate != '0000-00-00')?date("d-m-Y",  strtotime($company_profile[0]->bus_fy_startdate)):''; ?>" disabled="">
                                  </div>
                                </div>
                                <div class="col l4 m4 s6 end-date-box">
                                  <label class="active date-icon"><span class="red-date-icon"></span> End Date</label>
                                  <input type="text" value="<?php echo ($company_profile[0]->bus_fy_enddate != '' && $company_profile[0]->bus_fy_enddate != '0000-00-00')?date("d-m-Y",  strtotime($company_profile[0]->bus_fy_enddate)):''; ?>" disabled="">
                                </div>
                                <div class="col l4 m4 s6 end-date-box">
                                  <label class="active radio-icon"><span class="green-radio-icon"></span> Date Format</label>
                                  <input type="text" value="<?php echo ($company_profile[0]->bus_dateformat)?$company_profile[0]->bus_dateformat:''; ?>" disabled="">
                                </div>
                              </div>
                            </div>
                            <div class="col l5 s7 m12">
                              <div class="row">
                                <div class="col l6 m6 s6 end-date-box">
                                  <label class="active radio-icon"><span class="green-radio-icon"></span> Currency</label>
                                  <?php $currency_data = $this->Profile_model->selectData("currency",'*',array("currency_id"=>$company_profile[0]->bus_currency_format));
                                  $currency = (count($currency_data) > 0)?$currency_data[0]->currencycode:'';
                                  ?>
                                  <input type="text" value="<?php echo $currency; ?>" disabled="">
                                </div>
                                <div class="col l6 m6 s6 end-date-box">
                                  <label class="active radio-icon"><span class="green-radio-icon"></span> Timezone</label>
                                  <input type="text" value="<?php echo ($company_profile[0]->bus_timezone)?$company_profile[0]->bus_timezone:''; ?>" disabled="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col l12 m12 s12 view-box white-box after-green-bg green-bg-right">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Web Presence</h4>
                    </div>
                  </div>

                  <div class="col l12 m12 s12">
                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <div class="row mini-box-row-cover">
                            <div class="row">
                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <input type="checkbox" class="purple" disabled="">
                                      <label></label>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">WEBSITE URL</label>
                                        <input type="text" value="<?php echo $company_profile[0]->bus_website_url; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <input type="checkbox" class="purple" disabled="">
                                      <label></label>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">LIST OF CLIENTS</label>
                                        <input type="text" value="<?php echo $company_profile[0]->bus_loc; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <input type="checkbox" class="purple" disabled="">
                                      <label></label>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">CASE STUDIES</label>
                                        <input type="text" value="<?php echo $company_profile[0]->bus_case_study; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="col l1 s1 m1"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col l12 m12 s12 view-box white-box after-red-bg red-bg-left">
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <h4 class="white-box-title">Social Media</h4>
                    </div>
                  </div>

                  <div class="col l12 m12 s12">
                    <div class="row">
                      <div class="col l12 m12 s12">
                        <div class="inner_row_container">
                          <div class="row mini-box-row-cover">
                            <div class="row">
                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="facebook-icon social-icon"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">FACEBOOK URL</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_facebook != '')? $company_profile[0]->bus_facebook : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="twitter-icon social-icon"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">TWITTER URL</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_twitter != '')? $company_profile[0]->bus_twitter : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="linkedin-icon social-icon"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">LINKEDIN URL</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_linkedin != '')? $company_profile[0]->bus_linkedin : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="youtube-icon social-icon"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">YOUTUBE URL</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_youtube != '')? $company_profile[0]->bus_youtube : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="instagram-icon social-icon"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">INSTAGRAM URL</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_instagram != '')? $company_profile[0]->bus_instagram : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m6 l6">
                                <div class="col l12 m12 s12 gstnumber_covers mini-fields-box">
                                  <div class="row">
                                    <div class="col l1 s1 m1 checkbox-cover">
                                      <i class="field-prefix whatsapp"></i>
                                    </div>
                                    <div class="col l9 s9 m9">
                                      <div class="gst-no-one">
                                        <label class="active">WHATSAPP NUMBER</label>
                                        <input type="text" value="<?php echo ($company_profile[0]->bus_googleplus != '')? $company_profile[0]->bus_googleplus : ''; ?>" disabled="">
                                      </div>
                                    </div>
                                    <div class="col l2 s4 m4">
                                      <div class="hover-edit">
                                        
                                        <!-- <a href="javascript:void(0);"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="" class="delete-munbai-icon"></a> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="col l1 s1 m1"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          </section>
      </div>
    </div>


<div id="view_deactive_cmp_profile" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate Company Profile</h4>

      <input type="hidden" id="cmp_profile" name="cmp_profile" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to deactivate this company profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l8 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_cmp">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>


<div id="view_active_cmp_profile" class="modal">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Activate Company Profile</h4>

      <input type="hidden" id="cmp_profile_2" name="cmp_profile_2" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to activate this company profile?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l4 s12 m12"></div>

        <div class="col l8 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_cmp">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>
<?php $this->load->view('template/footer.php');?>