	
 <script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
	<style>
	#send_email_company_modal .modal-body{
		margin:0px !important;
		/*background-color:#f5f5f0*/
		margin-left:25px !important;
		width:90%;
	}
	
	.modal-header {
		padding: 30px;	
	}
	
	#mail_sub{
		background-color: #7864e9 !important;
		height:42px !important;
		padding: 0px 2rem !important;
		font-size:13px;
	}
	
	#mailto_id{
		border:1px solid #e0e0d1 !important;
		height:2rem !important;
		margin:0 0 8px 0 !important;
	}
	
	#subject{
		border:1px solid #e0e0d1 !important;
		height:2rem !important;
		margin:0 0 0px 0 !important;
	}
	
	#com_cc_mailto{
		border:1px solid #e0e0d1 !important;
		height:2rem !important;
		margin:0 0 0px 0 !important;
	}
	
	#message{
		border:1px solid #e0e0d1 !important;
	}
	
	#mail_close{
		background-color:white;
		color:#c2c2a3;
		font-size:13px;
	}
	
	.filed_name{
		font-size:14px;
		color:#595959;
	}
	
	#cke_1_contents{
		height:120px !important;
	}

	</style>
	<!--<div class="modal-header">
		<h4>Send Company Details</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png"></a>
	</div>
	<div class="modal-body">
        <form id="mail_frm" name="mail_to" class="" method="POST" accept-charset="utf-8" novalidate="true">
            <div class="row" style="margin-top:20px;">
				<div class="col s12 m12 l12">
                    <div class="col s12 m12 l12 input-group border-bottom">
                        <span> Mail To : </span>
						<input class="form-control" name="email_id" id="mailto_id" type="text" placeholder="Mail To">
                    </div>
                </div>
                
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group border-bottom" style="margin-top:10px;">
						<span> Subject :</span>
						<input class="form-control" name="subject" id="subject" type="text" placeholder="Subject">
					</div>
                </div>
                <input type="hidden" id="company_id" name="company_id" value="" />
				<div class="col s12 m12 l12">
					<div class="form-group login_btn_1" style="text-align:right; margin-top:15px;">
						<input type="submit" id="mail_sub" class="btn" value="SUBMIT" name="mail_submit">
					</div>
                </div>
            </div>
        </form>
    </div>-->
	
	<div class="modal-header" style="padding-top:21px !important;">
		<h4>Send Email</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
	</div>
	<div class="modal-body">
		<form id="mail_frm" name="mail_to" class="" method="POST" accept-charset="utf-8" novalidate="true">
            <div class="row" style="margin-top:20px;">
				<div class="col s12 m12 l12">
                    <div class="col s12 m12 l12 input-group">
                        <span class="filed_name"><b> To</b><span class="required_field">*</span><i>(Separate multiple IDs with comma):</i></span>
						<input class="form-control" name="email_id" id="mailto_id" type="text" placeholder="">
                    </div>
                </div>
				<div class="col s12 m12 l12">
                    <div class="col s12 m12 l12 input-group">
                        <span class="filed_name"><b> Cc :</b></span>
						<input class="form-control" name="com_cc_mail" id="com_cc_mailto" type="text" placeholder="">
                    </div>
                </div>
                <input type="hidden" id="sub_id" name="sub_id" value="">
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group" style="margin-top:10px;">
						<span class="filed_name"><b> Subject<span class="required_field">*</span>:</b></span>
						<input class="form-control" name="subject" id="subject" type="text" placeholder="">
					</div>
                </div>
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group" style="margin-top:10px;">
						<span class="filed_name"><b> Message<span class="required_field">*</span>:</b></span>
						<textarea class="form-control" style="height:75px !important;" name="message" rows="7" id="message" placeholder="" style="font-size:13px;"></textarea>
					</div>
                </div>
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l12" style="margin-top:10px;">
						<img class="" height="30" width="30" src="<?php echo base_url(); ?>asset/images/attach.png" alt="attachment"><img class="" height="30" width="30" src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf"></img>
						<label>File attached</label>
					</div>
                </div>
                <input type="hidden" id="ce_type" name="company" value="company" />
				<input type="hidden" id="company_id" name="company_id" value="" />
				<div class="col s12 m12 l12">
					<div class="form-group login_btn_1" style="text-align:right; margin-top:15px;">
						<button type="button" id="mail_close" class="btn btn-flat theme-primary-btn theme-btn theme-btn-large modal-close" data-dismiss="modal">CANCEL</button>
						<input type="submit" id="mail_sub" class="btn" value="SEND" name="mail_submit">
					</div>
                </div>
            </div>
        </form>
    </div>
	
	<script type="text/javascript" src="<?php echo base_url();?>public/js/bootstrap.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
	<script>
	$(document).ready(function($) {
			
		$("#mail_frm").submit(function(e){
			for ( instance in CKEDITOR.instances ) {
			        CKEDITOR.instances[instance].updateElement();
			    }
				e.preventDefault();
			}).validate({

				rules:{
					email_id:{
						required:true,
						//email:true,
						multiemail:true,
					},
					subject:{
						required:true,
					},
					message:{
						required:true,
					},
					com_cc_mailto:{
						multiemail:true,
					},
				},

				messages:{
					email_id:{
						required:"Email address is required",
						email:"Invlid Email"
					},
					subject:{
						required:"Subject is required",
					},
					message:{
						required:"Message is required",
					},
				},
				submitHandler:function(form){

						var company_id = $('#company_id').val();
						var mailto_id = $('#mailto_id').val();
						var subject = $('#subject').val();
						var message = $('#message').val();
						var cc_mail = $('#com_cc_mailto').val();
						$('.btn').prop('disabled', true);
								$.ajax({
								url:base_url+'profile/email_company_profile',
								type:"POST",
								data:{
										"company_id":company_id,
										"email_to":mailto_id,
										"email_subject":subject,
										"email_message":message,
										"email_cc":cc_mail,
									 },
								success:function(res){
									//alert(res);
										if(res == 1)
										{
											$("#company_id").val('');
											$("#mailto_id").val('');
											//$("#subject").val('');
											$('#com_cc_mailto').val('');
											$('#message').val('');
											//$('#mail_to').modal('hide');
											$('#send_email_company_modal').modal('close');
											$('.btn').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#company_id").val('');
											$("#mailto_id").val('');
											//$("#subject").val('');
											$('#com_cc_mailto').val('');
											$('#message').val('');
											//$('#mail_to').modal('hide');
											$('#send_email_company_modal').modal('close');
											$('.btn').prop('disabled', false);

											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');											
										}
									},
					});
				},
			});
		
		jQuery.validator.addMethod(
		    "multiemail",
		     function(value, element) {
		         if (this.optional(element)) // return true on optional element 
		             return true;
		         var emails = value.split(/[;,]+/); // split element by , and ;
		         valid = true;
		         for (var i in emails) {
		             value = emails[i];
		             valid = valid &&
		                     jQuery.validator.methods.email.call(this, $.trim(value), element);
		         }
		         return valid;
		     },

		    jQuery.validator.messages.email
		);
				
	});

	</script>

	<script type="text/javascript">

   /*$(function() {
    $('#froala-editor').froalaEditor({
      placeholderText: 'Enter your message here.'
    })
  });*/

$(document).ready(function() {
  // setTimeout(function(){ 
  //   $(location).attr('href');
  //     var pathname = window.location.pathname;
  //     if(pathname.indexOf("my-sales")) {
  //         $('#mysales').addClass('active');      
  //     } 

  //  }, 500);

 CKEDITOR.replace( 'message', { toolbar : 'Basic' });

 var ce_type=$('#ce_type').val();
 $('#Subject').val('');
  CKupdate();
  $.ajax({
          url:base_url+'customise_emails/get_customise_email',
          type:"POST",
          data:{"ce_type":ce_type},
          success:function(res){
                  var data = JSON.parse(res);
                  //alert(data);
                  //alert(ce_type);
                  if(data != false)
                  {
                    $('#subject').val(data[0].ce_subject);
                    //$('#ce_id').val(data[0].ce_id);
                    CKEDITOR.instances['message'].setData(data[0].ce_message);
                  }
             },
    }); 


});

function CKupdate(){
    for ( instance in CKEDITOR.instances ){
        CKEDITOR.instances[instance].updateElement();
        CKEDITOR.instances[instance].setData('');
   }
}
</script>