

    <!-- //////////////////////////////////////////////////////////////////////////// -->
	 <?php $this->load->view('template/header.php');?>
    <!-- START MAIN -->
   
    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">
          <div class="page-header">
            <div class="container">
              <h2 class="page-title">My Company Profile</h2>
            </div>
          </div>

          <div class="page-content">
            <div class="container">
              <div class="welcom-container">
                <div class="Welcome-image">
                  <img height="200" width="200" src="<?php echo base_url();?>asset/css/img/icons/blank-stage/ei-blank.gif" class="gifblank" alt="com-blank">
                </div>
                <div class="welcom-content">
                  <p class="welcom-title">Hey <?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?>!</p>
                  <p class="welcom-text">Welcome aboard! How about having all your company profiles here :)<br/>Let’s get started by adding one below.</p>
                  <p class="welcome-button">
                    <a class="btn btn-welcome" href="<?php echo base_url();?>profile/add-company-profile">ADD NEW COMPANY PROFILE</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->

        </div>

        <!-- END WRAPPER -->

      </div>

      <!-- END MAIN -->
<script type="text/javascript">
 // var bus_type= "<?php // echo $select_service; ?>";

</script>
	 <?php $this->load->view('template/footer.php');?>
 