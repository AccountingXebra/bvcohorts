 <?php $this->load->view('template/header.php');?>
    <!-- START MAIN -->
    <style type="text/css">
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 12px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 30px;
	  color: #000;
	  
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 40px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 12PX;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 40px;
		padding: 6px;
		border: 1px solid #d4d8e4;
		background: #f8f9fd;
		border-radius: 5px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 90%;
		max-width: 100%;
	}
	.select2-search__field::placeholder {
	  content: "Search Here";
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: 0;
	}
	.select2-container {
	  width: 200px !important;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	
	.dataTables_scrollBody{
		overflow:hidden !important;
		height:100% !important;
	}

	.dataTables_scrollHead{
		margin-bottom:-24px !important;
	}

	table.dataTable thead .sorting {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
		ackground-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
		background-position: 110px 15px !important;
	}
    .table-type1 [type="checkbox"] + label {
		margin: 0 0 0 8px !important;
    }
    .dataTables_length {
		margin-left: 500px;
	}

	#personal-profile_length{
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:97px;
		margin-top:5px;
		margin-left:52%;
	}
	
	#personal-profile_length .dropdown-content {
		min-width: 97px;
		margin-top:40% !important;
	}
	
	#personal-profile_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	#personal-profile_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}
	
	.personal_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}
	
	.sticky {
		position: fixed;
		top: 70px;
		width: 75.6%;
		z-index:999;
		background: white;
		color: black;
		
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}
	
	a.filter-search.btn-search.btn.active {
		width:29% !important;
	}
	
	a.addmorelink{
		margin-top:-15px;  
	}

	.ex{
		margin:-28px 0 0 48px !important;
	}
    </style>
    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content" class="bg-cp profile-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">My Personal Profiles <small class="grey-text">(<?php echo count($personal_profile); ?> Total)</small></h5>
                  
                  <ol class="breadcrumbs">
                    <li><a href="#">My Profile</a>
                    </li>
                    <!-- <li class="active">My Personal Profiles</li> -->
                  </ol>
                </div>
                <div class="col s10 m6 l6">
                  <a class="btn btn-theme btn-large right" href="<?php echo base_url(); ?>profile/add-personal-profile">ADD NEW  PERSONAL PROFILE</a>
                </div>
              </div>
            </div>
          </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12">
                <a href="javascript:void(0);" class="addmorelink right" id="reset" title="Reset all">Reset</a>
                </div>
				<div class="col l6 s12 m12">
                 
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown04' class='dropdown-content'>
                      <li><a href="javascript:void(0)" id="download_multiple_profiles" data-multi_download="0"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 21px;height: 20px;"><p class="ex">Export</p></a></li>
                      <!--li><a href="javascript:void(0)" id="email_multiple_profiles" ><i class="dropdwon-icon icon email"></i> Email</a></li-->
                      <li><a href="javascript:void(0)" id="print_multiple_profiles" ><i class="material-icons dp48">print</i> Print</a></li>
                      <li> <a href="javascript:void(0)" id="deactive_multiple_profiles" data-multi_profiles="0"><?php 
                       
                      if(count($personal_profile)==count($inactivate_personal_profile)){ ?><i class="dropdwon-icon icon activate" id="activate" data-act="reactivate"></i> Re-activate<?php }else{?><i class="dropdwon-icon icon deactivate" id="activate" data-act="deactivate"></i> Deactivate <?php } ?></a></li>
                    </ul>
                    <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_personal_profile" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
					</a>
					</div>
                   
					<div class="col l6 s12 m12">
						<div class="action-btn-wapper right">
							<div class="bulk location-drop">
							<select class="js-example-basic-single" name="name_pr" id="name_pr">
								<option value="">NAME</option>
								<option value="">ALL</option>
								<?php if($personal_profile != '') {
                                foreach($personal_profile as $profile) { ?>
                                <option  value="<?php echo $profile->reg_username; ?>"><? echo strtoupper($profile->reg_username);?></option>
                                <?php }  } ?>
							</select>
							</div>
						</div> 
					</div>
             </div>
            </div>
          </div>

     <div class="container">
    <div class="row">
      <div class="col l12 s12 m12">
     <!-- <form>-->
        <table id="personal-profile" class="responsive-table display table-type1 " border="0" cellspacing="0"  width="100%">
        <thead id="fixedHeader">
          <tr>
          <th>
            <input type="checkbox" id="personal_profile_bulk" name="personal_profile_bulk" class="filled-in purple" />
            <label for="personal_profile_bulk" style="margin: unset !important;"></label>
          </th>
          <th style="width: 50px;"></th>
          <th class="comp-sort-name" style="width: 200px;">Name</th>
          <th style="width: 300px;">Email ID</th>
          <th style="width: 200px;">Phone Number</th>         
          <th style="width: 150px;">Access</th>
          <th style="width: 5px;">Action</th>
          </tr>
        </thead>
        <tbody class="scrollbody">
         
        </tbody>
        </table>
      <!--</form>-->
      </div>
    </div>
    </div>               
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
 <!-- END MAIN -->
 <script type="text/javascript">
    $(document).ready(function() {
		$('.js-example-basic-single').select2();
		
		$("#reset").click(function(){
			$('input[type=checkbox]').each(function(){
				this.checked=false;
			});
			$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			//$(".js-example-basic-single").trigger('change.select2');
			$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
			profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
			$('select').material_select();
		});	
	});
 </script>
 <?php $this->load->view('template/footer.php');?>