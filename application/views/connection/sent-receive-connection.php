<?php $this->load->view('template/header'); ?>
<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<style type="text/css">
	#breadcrumbs-wrapper{
		padding-bottom:10px !important;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 260px 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}
	
	.top-label{
		font-size:13px;
		margin-top:30px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.company-div{
		height: 150px;
		width: 48% !important;
		margin: 10px;
		padding: 10px;
		border-radius:5px;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:14px !important;
	}
	
	.withdraw{
		background-color:#fff !important;
		border:1px solid #ccc !important;
		color: #ccc !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px -5px;
	}
	
	.req_accept{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px -5px;
	}
	
	.req_decline{
		background-color:#fff !important;
		border:1px solid #ccc !important;
		color: #ccc !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px -5px;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-logo{
		margin:10px 0;
	}
	
	.org-loc{
		font-size:12px !important;
	}
	
	.bulk-action{
		margin-top:15px;
	}
	
	.bulk-check{
		margin-right:-50px;
	}
	
	.send-rec-tab{
		text-align:right;
		margin-top:25px;
	}
	
	.recei-tab{
		border-left:3px solid #ccc;
	}
	
	#sent_tab, #recei_tab{
		color:#ccc !important;
	}
	
	#sent_tab.active, #recei_tab.active{
		color:#000 !important;
	}
	
	.org-info{
		margin: 8% 0 !important;
	}
	
	.org-person{
		margin: 5% 0 !important;
	}
	
	label.label-bulk {
		margin: 27px 0 0 0 !important;
	}
	
	.new-req-noti{
		background-color:#50E2C2 !important;
		height:43px !important;
		margin-top:2px;
		padding:10px;
	}
	
	.noti-label{
		color:#fff !important;
		font-size:15px;
	}
</style>
<!-- START MAIN -->
<div id="main" height="80%">
  <!-- START WRAPPER -->
  <div class="wrapper">
    <!-- START LEFT SIDEBAR NAV-->
    <?php $this->load->view('template/sidebar'); ?>
    <!-- END LEFT SIDEBAR NAV-->
    <!-- START CONTENT -->
    <section id="content" class="bg-cp sales_invoices-search">
	  <div class="row text-center new-req-noti" hidden>
		<label class="noti-label"><b>You have made 3 new connections</b></label>
	  </div>
      <div id="breadcrumbs-wrapper">
        <div class="container">
		  <div class="row" style="margin:-20px 0 10px -12px;">
			<div class="col l6 s12 m6">
				<a class="go-back underline" href="<?php base_url(); ?>no_connection">Back to My Ecosystem</a>
            </div>
		  </div>
          <div class="row">
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title my-ex">Connection Requests</h5>
              <ol class="breadcrumbs">
                <li><a href="">MY COMMUNITY / MY ECOSYSTEM / NEW CONNECTION REQUESTS</a>
              </ol>
            </div>
            <div class="col s2 m6 l4">
                <input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="Search by company name">
            </div>
			<div class="col s2 m6 l2">
				<a href="<?php base_url(); ?>no_connection" id="find_contact" class="btn btn-theme btn-large modal-trigger">FIND CONTACTS</a>
			</div>
         </div>
       </div>
     </div>

     <div class="container custom content-div">
		<div class="row">
			<div class="col l12 s12 m12">
				<div class="col l1 s12 m12 bulk-check">
				   <input type="checkbox" id="sent_rece_bulk" name="sent_rece_bulk" value="1" class="left-label sent_receive_bulk modal-trigger modal-close">
				   <label class="checkboxx2 checkboxx-in " for="sent_rece_bulk"><span class="check-boxs"></span></label>      
				</div>
				<div class="col l4 s12 m12 bulk-action">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown004' class='dropdown-content'>
                    <li><a id="email_multiple_billing_documents"><i class="dropdwon-icon icon email"></i>Email</a></li>
                    <li><a id="download_multiple_invoices" data-multi_invdownload="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px">Download</a></li>
                    <li><a id="print_multiple_billing_documents"><i class="material-icons">print</i>Print</a></li>
                    <li><a id="deactive_multiple_invoices" data-multi_invoices="0" ><i class="material-icons">delete</i>Delete</a></li>
                  </ul>
			  </div>
			  <div class="col l7 s12 m12 send-rec-tab">
				<div class="col l9 s12 m12"></div>
				<div class="col l2 s12 m12">
					<a id="recei_tab" href="#receive_request">RECEIVED</a>
				</div>
				<div class="col l1 s12 m12 recei-tab">
					<a id="sent_tab" class="active" href="#sent_request">SENT</a>
				</div>
			  </div>
		   </div>
	   <div class="row" id="sent_request" style="margin-top:20px !important;">
			<div class="col l12 s12 m12">
				<div class="col l12 s12 m12">
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info">	
							  <input type="checkbox" id="req_bulk_action1" name="req_bulk_action1" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action1"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info">	
								<img width="100" height="50" src="https://localhost/xebra/public/images/Eazy-Invoice-Logo.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 text-center org-person">	
								<p class="org-name"><b>Security Assured Services</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Pune</b></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info">	
								<a href="#" id="req_withdraw" class="withdraw btn btn-theme btn-large">WITHDRAW</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info">	
							  <input type="checkbox" id="req_bulk_action2" name="req_bulk_action2" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action2"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info">	
								<img width="100" height="50" src="https://localhost/xebra/public/images/sos.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 text-center org-person">	
								<p class="org-name"><b>Windchimes Communication</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Mumbai</b></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info">	
								<a href="#" id="req_withdraw" class="withdraw btn btn-theme btn-large">WITHDRAW</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info">	
							  <input type="checkbox" id="req_bulk_action2" name="req_bulk_action2" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action2"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info">	
								<img width="100" height="50" src="https://localhost/xebra/public/images/suh.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 text-center org-person">	
								<p class="org-name"><b>Shufl Pvt. Ltd.</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Ratnagiri</b></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info">	
								<a href="#" id="req_withdraw" class="withdraw btn btn-theme btn-large">WITHDRAW</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
				</div>
            </div>
	   </div>
	   
	   <div class="row" id="receive_request" style="margin-top:20px !important;">
			<div class="col l12 s12 m12">
				<div class="col l12 s12 m12">
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info" style="width:7%">	
							  <input type="checkbox" id="req_bulk_action1" name="req_bulk_action1" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action1"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info" style="width:18%">	
								<img width="80" height="50" src="https://localhost/xebra/public/images/suh.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 org-person" style="width:40%">	
								<p class="org-tagline"><b>Shufl Consumer</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Pune</b></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info" style="width:35%; padding:0px !important; margin-left:-20px;">	
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info" style="width:7%">	
							  <input type="checkbox" id="req_bulk_action2" name="req_bulk_action2" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action2"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info" style="width:18%">	
								<img width="80" height="50" src="https://localhost/xebra/public/images/sos.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 org-person" style="width:40%">	
								<p class="org-tagline"><b>Security Assured Services</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Pune</b></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info" style="width:35%; padding:0px !important; margin-left:-20px;">	
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					
					<!----------------      END             --------------------->
					<div class="col l6 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l1 text-center org-info" style="width:7%">	
							  <input type="checkbox" id="req_bulk_action2" name="req_bulk_action2" value="1" class="left-label sent_bulk_action">
							  <label class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action2"><span class="check-boxs"></span></label>
							</div>
							<div class="col s12 m12 l2 text-center org-info" style="width:18%">	
								<img width="80" height="50" src="https://localhost/xebra/public/images/xebra-logo.png" alt="Logo" class="org-logo">
							</div>
							<div class="col s12 m12 l5 org-person" style="width:40%">	
								<p class="org-tagline"><b>Xebra</b></p>
								<label class="org-tagline"><b>Boost Your Business</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc" style="margin-top:-10px !important;"><b>Bandra</b></label>
							</div>
							<div class="col s12 m12 l3 text-center org-info" style="width:35%; padding:0px !important; margin-left:-20px;">	
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					
					<!----------------      END             --------------------->
				</div>
            </div>
	   </div>
	   
	   <div class="row">
			<div class="col l12 s12 m12"> <p> &nbsp; &nbsp; </p> </div>
	   </div>
    </div>
   </section>
   <!-- END CONTENT -->
   </div>
   <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<script type="text/javascript">
	$(document).ready(function() {
		$("#receive_request").hide();
		$('#recei_tab').on('click', function(){
			$("#sent_request").hide();
			$("#receive_request").show();
			$("#recei_tab").addClass('active');
			$("#sent_tab").removeClass('active');
		});
		$('.withdraw').on('click', function(){
			console.log('OK');
			$(this).closest(".company-div").remove();
		});
		$('#sent_tab').on('click', function(){
			$("#sent_request").show();
			$("#receive_request").hide();
			$("#recei_tab").removeClass('active');
			$("#sent_tab").addClass('active');
		});
	}); 
</script>

<?php $this->load->view('template/footer'); ?>