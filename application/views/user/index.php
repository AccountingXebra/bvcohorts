<?php
	$this->load->view("/template/header");
?>
<section class="content-header">
    <h1>
        User List
    </h1>
    <?php
        $this->load->view("/template/bread_crumb");
    ?>
</section>
<section class="content">
    <div class="row">
         <div class="col-md-12">
             <div id="flash_msg" class="">
                <span id="msg_contant"></span>
            </div>
         </div>   
    </div>
	<div class="row">
	   <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                   <h3 class="box-title">User List</h3>
                </div>
                <div class="box-body table-responsive">
                    <table id="adminTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>                                 
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Facebook ID</th>
                                <th>Gender </th>
                                <th>Address</th>
                                <th>PostCode</th>
                                <th>Country</th>
                                <th>Subscribe </th>
                                <th>Status </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                    </table>
                </div><!-- /.box-body -->
            </div>
          
        </div>
	</div>

</section>
<script src="<?=public_path()?>js/page/user.js" type="text/javascript"></script>
<?php
	$this->load->view("/template/footer");
?>
