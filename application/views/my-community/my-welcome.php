<?php $this->load->view('my-community/Cohorts-header');?>
<style>
	
	p{
		text-align: justify;
	}
	
	.direct:hover {
		color: #bc343a !important;
	}
	@media only screen and (max-width: 600px) {
		img.logo_style_2 { width:100%; height:100%; }	
	}
</style>

<!-- START MAIN -->
<div id="main" style="padding:2% 0 0 0px !important; background-color:#fff; height:auto;">
	<!-- START WRAPPER -->
	<div class="wrapper">
		<!-- START CONTENT -->
		<section id="content">
			<div class="row" style="padding:0.5% 0;">
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l1"></div>
					<div class="col s12 m12 l4" style="padding:4% 0;">
						<img width="410" height="330" style="" src="<?php echo base_url(); ?>asset/images/cohorts/buzz-comm.png" alt="Logo" class="text-hide-collapsible logo_style_2"/>
					</div>
					<div class="col s12 m12 l6">
						<div class="row">
							<p>Welcome To Bharat Vaani</p>
							<p>
								Bharat Vaani is a platform for innovators and inventors to leverage the power of a community. A community that you can use as a sounding board or share your success, doubts, learnings, and experiences. Leverage this community to talk about growth, revenues, motivation, coaching, mentorship, and above all, emotional and mental wellbeing.
							</p>
							<p>
								Down the ages, invention and the spirit of innovation have often been an intensely personal quest and the journey of an innovator a long and lonely one. It doesn’t have to be anymore- A community of inventors is a good thing. Bharat Vaani brings as many innovators together on a single platform to leverage the power of a group.
							</p>
							<p>
								We are Bharat’s first digital community for inventors and innovators. It’s a place to draw inspiration, build connections, generate growth, avail mentoring, and get funding for your innovation
							</p>
							<p>
								We would love to hear about your experiences with Bharat Vaani and how we can improvise it. Do send in your ideas and suggestions to <a style="color:#bc343a;" href="mailto:contactus@bharatvaani.in">contactus(at)bharatvaani.in</a> 
							</p>
						</div>	
						<div class="row">
							<div class="col s12 m12 l2" style="margin-right:-15px;">
								<img style="border-radius: 25%; height: 85px; width: 68px;" src="<?php echo base_url(); ?>public/images/sapna.jpg" alt="Logo" class=""/>
							</div>
							<div class="col s12 m12 l8">
								<p style="padding-top:4px;"><a class="direct" style="color: #000;" href="https://www.linkedin.com/in/sapnabakshi18" target="_blank" rel="noopener noreferrer"><strong>Sapna Bakshi</strong></a></br>Co-Founder - Bharat Vaani</p>
							</div>
							
						</div>
						<div class="row" style="margin-left:10%;">
							<center><a style="color:#bc343a; margin-top: -46px;" class="nav-link" href="<?= base_url(); ?>community/my-profile">LET’S GET YOU STARTED BY CREATING YOUR CORPORATE PROFILE</a> <!--| <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-connections">See Connections</a> | <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-marketplace">Marketplace</a>--></center>
						</div>
					</div>					
					<div class="col s12 m12 l1"></div>
					
				</div>
			</div>
		</section> <!-- END CONTENT -->
	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->


<script type="text/javascript">
	$('.js-example-basic-multiple').select2();
</script>
<?php $this->load->view('template/footer.php');?>