<?php $this->load->view('my-community/Cohorts-header');?>
    <style type="text/css">
	.videosection.mar25{ padding-top:25px !important; }	
    #content{ padding: 0% 0 3% 0; border-bottom: 1px solid #f7f7f7; }
	.interview_row .pp-header{ padding:2.5% 0; }
	.interview_row .pp-header h1{ color:#595959; font-size:32px; }
	.interview_row .pp{ padding: 5px 0; line-height:25px; font-size:18px; text-align: justify; } 
	.interview_row .tc-info{ padding: 0 12%; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.blog-disp .blog-title{
		width:100%;
		margin:15px 0 15px 10px;
		text-align:left;
		line-height:25px;
	}
	.blog-disp .brief-content{
		width:100%;
		margin:0 0 6% 10px;
		text-align:left;
	}
	.playpause {
		background-image:url("<?=base_url('asset/images/overlay.png');?>");
		background-repeat:no-repeat;
		background-repeat: no-repeat;
		width: 40%;
		height: 15%;
		position: absolute;
		left: -18%;
		right: 0%;
		top: -30%;
		bottom: 0%;
		margin: auto;
		background-size: contain;
		background-position: center;
		cursor:pointer;
	}
	.top-filter{ border-bottom:1px solid #ecedef; width:105.5% !important }
	.top-filter p{ font-size:18px; font-weight:600;	}
	.container { width: 100% !important; padding: 0px;}
	.cat_videos{ height: 100%; margin:8% 0 0 0; }
	.btn-dropdown-select{ width:210px; } 
	.videosection{ border-left:1px solid #ecedef; margin: 0; padding: 0px 0 0 25px !important; }
	.video-owner, .video-title, .brief-content{ text-align:left; margin:5px 0 0 0; } 
	#breadcrumbs-wrapper { padding: 25px 0 15px 0; }
	.select-cat p{ font-size:16px; font-weight:600;	}
	.catselect{ text-transform:uppercase; }
	.vid_cat{ margin-bottom:6%; }
	.blog-disp{ margin:0 0 20% -15px; border-radius: 10px; width: 103%; height:290px; box-shadow: 0 2px 5px #ebecf0; -moz-box-shadow: 0 2px 5px #EBECF0; -o-box-shadow: 0 2px 5px #EBECF0; -webkit-box-shadow: 0 2px 5px #ccc; -ms-box-shadow: 0 2px 5px #EBECF0;}
	.btn-dropdown-select ul.dropdown-content{ height:250px !important; overflow-y:scroll !important; }
	#main .ps-container > .ps-scrollbar-y-rail, #main .ps-container > .ps-scrollbar-y-rail > .ps-scrollbar-y{ display:none !important; }
    </style>
    <div id="main" style="padding-left:0px !important;">
		<div class="wrapper">
			<section id="content" class="bg-cp">
			<div id="breadcrumbs-wrapper">
				<div class="container custom">
					<div class="row">
						<div class="col s10 m6 l6" style="margin:0 0 0 2%;">
							<h5 class="breadcrumbs-title my-ex">Interviews</h5>
							<ol class="breadcrumbs">
								<li><a href="index.html">LEARNING / INTERVIEWS</a>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
					<div class="row blog-content">
						<div class="col s12 m12 l2">
							<div class="col s12 m12 l12 top-filter text-center">
								<p>Filter By</p>
							</div>
							<div class="col s12 m12 l12 cat_videos">
								<select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown vid_cat' id="innovo_name" name="innovo_name">
									<option value="">INNOVATOR'S NAME</option>
									<option value="all">ALL</option>
									<?php foreach($interviews as $inws){ ?>
										<option value="<?=$inws->name?>"><?=$inws->name?></option>
									<?php } ?>
								</select>
								<!--<select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown vid_cat' id="company_name" name="company_name">
									<option value="">COMPANY NAME</option>
									<option value="all">ALL</option>
								</select>-->
							</div>
						</div>
						<div class="col s12 m12 l10 mar25 videosection">
						<div class="col s12 m12 l12 select-cat" hidden>
							<p class="catselect">ALL INNOVATOR</p>
						</div>
						<?php 
                         if(count($interviews)>0){
						foreach($interviews as $inws){ ?>
						<div class="col s12 m12 l3">
							<div class="blog-disp">
								<a href="<?php echo base_url();?>interviews/<?=$inws->url?>"><video style="width:280px; height:158px; border-radius:10px; object-fit:fill; margin-right: 0px;" class="" src="<?=$inws->video."/".$inws->filename?>" poster="<?=$inws->cover?>" data-src="#" frameborder="0"></video>
						</a>
								<a href="<?php echo base_url();?>interviews/<?=$inws->url?>"><!--<div class="playpause"></div>--></a>
						<p class="blog-title"></p>
						<p class="blog-title"><strong>Interview of</strong> - <?=$inws->name?></p>
						<p class="brief-content"><strong>Conducted by</strong> - <?=$inws->conduct?></p>
							</div>	
						</div>
						
						<?php } }else{ echo "No Interviews Available";} ?>
						</div>	
					</div>
			</div>	
			</section>
		</div>	
	</div>	
	<script>
	$(document).ready(function() {
		$("#innovo_name").on("change",function(){
			var cat_name = $(this).val();
			if(cat_name == 'all'){
				$('.catselect').text("ALL INNOVATOR");
			}else{ $('.catselect').text(cat_name); }
		});
		
		$("#company_name").on("change",function(){
			var cat_name = $(this).val();
			if(cat_name == 'all'){
				$('.catselect').text("ALL COMPANY");
			}else{ $('.catselect').text(cat_name); }
		});
	});		
	</script>
<?php $this->load->view('template/footer'); ?>	