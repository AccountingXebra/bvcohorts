 <?php $this->load->view('my-community/Cohorts-header');?>

    <!-- END HEADER -->
    <style type="text/css">
    .add-new {
		margin: 0 0 0 0 !important;
    }
	.section_li {
		margin: 0 0 0 0 !important;
    }
    .categories>li {
		float: none;
		display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
        padding: 0 40px !important;
		border-radius: 6px !important;
        line-height: 45px !important;
        height: 45px !important;
	}
    ul.categories {
		text-align: center;
        margin:0;
        width: 100%;
    }
    .tabs {
		height:auto;
        background: transparent !important;
        width: 100% !important;
    }
    ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
		min-height: auto !important;
    }
	a.btn.btn-theme.btn-large.section_li.active {
		background-color: #7864e9 !important;
		color: #f8f9fd;
		border: 0px;
	}
	.tabs .tab{
		padding: 0 5px 0 5px !important;
	}
	.collapsible {
		border: 0px;
		-webkit-box-shadow: none;
	}
	.document-box-view.redborder.active {
		border-left: 4px solid #ff7c9b;
	}
	.collapsible-header{
		border-bottom:none;
	}
	.collapsible-header.active{
		padding-bottom: 0px;
		font-weight: 600;
	}
	.collapsible-body{
		padding: 0 1rem 1rem 1rem;
	}
	.red-bg-right:before {
		top: -32px !important;
	}
	.green-bg-right:after{
		bottom: -25px !important;
		left: 50px;
	}
	ul:not(.browser-default) > li .li_cls {
		list-style-type: disc !important;
	}
	.btn-theme{
		padding:0px !important;
	}
	.art-title{
		margin:2% 0 0 0;	
	}
	.short-article h6, a{
		color:#7864e9;
	}
	.short-article p{
		text-align:left;
	}
	
	.short-article{
		margin-bottom:4%;
	}
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
			<!--div id="breadcrumbs-wrapper" style="margin-bottom: -2%;">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">Frequently Asked Questions</h5>
                </div>
              </div>
            </div>
			</div-->
        <div id="bulk-action-wrapper">
          <!--div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li">Article</a></li>
						<li class="tab"><a href="#test2" class="btn btn-theme btn-large section_li">Media</a></li>
					</ul>
                 </div>
              </div>
          </div-->
        </div>

		<div class="container custom">
			<div class="row">
				<div class="col l12 s12 m12 art-title">
					<!-- Article Start -->
					<div id="test1" class="col s12 inner_row_container" style="margin:0 8%;">
						<div class="col l12 s12 m12">
							<div class="col l5 s12 m12 short-article">
							<center>
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/execu.png" alt="" class="text-hide-collapsible logo_style_2"/>
								<h6><strong>Don’t let the lockdown impact your business</strong></h6>
								<p>There are still a series of applications that you use to carry out work while being at home. Here’s a list of 5 SaaS applications that have a free version and can assist you across different functions of yours.</br>
								<a style="text-align:left !important;" href="">Read More</a></p>
							</center>
							</div>
							<div class="col l5 s12 m12 short-article">
								<center>
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/second.jpg" alt="" class="text-hide-collapsible logo_style_2"/>
								<h6><strong>Why every founder must do business financial analysis during this lockdown</strong></h6>
								<p>While desperate times require desperate measures, it is advisable to take stock of the situation. Simplifying business numbers is a step in that direction</br>
								<a style="text-align:left !important;" href="">Read More</a></p>
								</center>
							</div>
							<div class="col l5 s12 m12 short-article">
								<center>
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/third.jpeg" alt="" class="text-hide-collapsible logo_style_2"/>
								<h6><strong>Four free business applications you can use while working from home</strong></h6>
								<p>The author had written this article for AdGully and it has been reproduced here. Zoom allows users to carry out to collaborate and work with each.</br>
								<a style="text-align:left !important;" href="">Read More</a></p>
								</center>
							</div>
							<div class="col l5 s12 m12 short-article">
								<center>
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/four.png" alt="" class="text-hide-collapsible logo_style_2"/>
								<h6><strong><a href="<?php //echo base_url(); ?>community/cohorts_blog_first">The 10 features in an ideal Online Invoicing Software</a></strong></h6>
								<p>The 10 features in an ideal Online Invoicing Software Unlock The Power Of Automated Invoicing Whether you're a service provider or a product-based enterprise, today's market is getting competitive than it was ever before. Retaining clients & growing revenue is the key to business growth.…</br>
								<a style="text-align:left !important;" href="<?php //echo base_url(); ?>community/cohorts_blog_first">Read More</a></p>
								</center>
							</div>
						</div>
					</div>
					<!-- Article End -->
					
					<!-- Media Start -->
					<div id="test2" class="col s12 inner_row_container" style="display:none;">
						Media
					</div>
					<!-- Media End -->
				</div>
			</div>
		</div>

        </section>
        <!-- END CONTENT -->
		</div>
		<!-- END WRAPPER -->
	</div>
    <!-- END MAIN -->

	<script>
		if ($('.menu-icon').hasClass('open')) {
			$('.menu-icon').removeClass('open');
			$('.menu-icon').addClass('close');
			$('#left-sidebar-nav').removeClass('nav-lock');
			$('.header-search-wrapper').removeClass('sideNav-lock');
			$('#header').addClass('header-collapsed');
			$('#logo_img').show();
			$('#eazy_logo').hide();
			$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
			$('#main').toggleClass('main-full');
		}
	</script>
    <?php $this->load->view('template/footer'); ?>
