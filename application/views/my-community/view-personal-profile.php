<?php $this->load->view('my-community/Cohorts-header');?>
<style type="text/css">
	.green-bg-right:before {
		top: -39px !important;
		width: 270px;
	}
	.green-bg-right:after {
		width: 270px;
	}
	.view-box {
		width: 60%;
		border-radius: 6px;
		margin: 0 18%;
		padding:2.5% !important;
	}
	.profile_img{ text-align }
	.profile_details{ padding: 0% 10% 2% 10%; }
	.profile_details p{ font-size:15px; }
	.profile_details h6{ padding:5% 0 2% 0;}
	.profile_details .social-icon{ padding:2% 3% 0 0;}
	.edit_profile{ margin:18px 0 0 0; }
	.plain-page-header { padding: 50px 0 10px 20px; }
	.editpencil{ font-size: 20px; background-color: #ccc; padding: 5px; border-radius: 24px; }
	i.material-icons.editpencil { color: #000; }
	.profile_img img{ display: block; max-width:230px; max-height:110px; width: auto; height: auto; }
</style>
    <!-- START MAIN -->
	<div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
					<div class="col l6 s12 m6">
						<a class="go-back underline" href="javascript:window.history.go(-1);">Back to Home</a>
					</div>
					<div class="col l6 s12 m6">
						<div class="right-2"></div>
					</div>
				</div>
              </div>
            </div>
            <div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8 view-box white-box">
							<div class="row">
								<div class="col l3 m12 s12">
									<div class="profile_img text-center">
										<?php if(isset($per_profile[0]->reg_profile_image) && $per_profile[0]->reg_profile_image !=""){ ?>
											<img width="150" height="150" src="<?php echo base_url(); ?>public/upload/personal_images/<?=$per_profile[0]->reg_id?>/<?=$per_profile[0]->reg_profile_image?>" alt="user" class=""></img>
										<?php } ?>	
									</div>	
								</div>
								<div class="col l8 m12 s12">
									<div class="profile_details">
										<p><strong>Name</strong>: <?=$per_profile[0]->reg_username?> <p>
										<p><strong>Date Of Birth</strong>: <?=date('d/m/Y',strtotime($per_profile[0]->birth_date))?><p>
										<p><strong>Bio</strong>: <?=$per_profile[0]->bio?><p>
										<p><!-- Add Bio here only --><p>
										<h6><strong>Company Information: </strong></h6>
										<div class="profile_img text-center">
											<?php if(isset($comp_profile[0]->bus_company_logo) && $comp_profile[0]->bus_company_logo !=""){ ?>
												<img width="150" height="150" src="<?php echo base_url(); ?>public/upload/company_logos/<?=$comp_profile[0]->bus_id?>/<?=$comp_profile[0]->bus_company_logo?>" alt="user" class=""></img>
											<?php } ?>	
										</div>	
										<p><strong>Company Name</strong>: <?=$comp_profile[0]->bus_company_name?></p> 
                                        <p><strong>Designation</strong>: <?=$per_profile[0]->reg_designation?></p>
                                        <p><strong>Email Id</strong>: <?=$per_profile[0]->reg_email?></p> 
                                        <p><strong>Mobile</strong>: <?=$per_profile[0]->reg_mobile?></p> 

                                        <p><strong>Website Url</strong>: <?=$comp_profile[0]->bus_website_url?> </p>
										<p><strong>Date of incorporation</strong>: 
										<?php if($comp_profile[0]->bus_incorporation_date != "0000-00-00"){ ?>
										<?=date('d-m-Y',strtotime($comp_profile[0]->bus_incorporation_date)) ?>  <?php } ?></p>
                                        <p><strong>Area of Expertise</strong>: </p>		<div class="row"><?php
									$services = explode("|@|", $comp_profile[0]->bus_services_keywords ?? '');
									//explode("|@|",$comp_profile[0]->bus_services_keywords);
									for($i=0;$i<count($services);$i++)
									{
										if($services[$i])
										{
											?>
											<div class="col l6 s6 m6" style="height:40px !important;">
												<label class="select_loc"> <?php echo strtoupper($services[$i] ?? ''); ?> </label>
											</div>
											<?php
										}
									}
									?></div>
                                            
										<h6><strong>Social Media: </strong></h6>
										<a class="social-icon" href="<?=$comp_profile[0]->bus_facebook?>" target="_blank"><img width="30" height="30" src="<?php echo base_url(); ?>asset/css/img/icons/facebook.png" alt="Facebook Logo"/></a>
										<a class="social-icon" href="<?=$comp_profile[0]->bus_twitter?>" target="_blank"><img width="30" height="30" src="<?php echo base_url(); ?>asset/css/img/icons/twitter.png" alt="Twitter Logo"/></a>
										<a class="social-icon" href="<?=$comp_profile[0]->bus_linkedin?>" target="_blank"><img width="30" height="30" src="<?php echo base_url(); ?>asset/css/img/icons/linkedin.png" alt="Twitter Logo"/></a>
										<a class="social-icon" href="<?=$comp_profile[0]->bus_instagram?>" target="_blank"><img width="30" height="30" src="<?php echo base_url(); ?>asset/css/img/icons/instagram.png" alt="Instagram Logo"/></a>
										<a class="social-icon" href="<?=$comp_profile[0]->bus_youtube?>" target="_blank"><img width="30" height="30" src="<?php echo base_url(); ?>asset/css/img/icons/youtube.png" alt="Youtube Logo"/></a>
										<!--<a class="social-icon" href="<?//=$comp_profile[0]->bus_googleplus?>" target="_blank"><img width="30" height="30" src="<?php //echo base_url(); ?>asset/css/img/icons/vimeo.png" alt="Vimeo Logo"/></a>-->
									</div>	
								</div>
								<div class="col l1 m12 s12 edit_profile">
									<a title="Edit Your Profile" href="<?php echo base_url(); ?>community/add_profile"><i class="editpencil material-icons">edit</i></a>
								</div>	
							</div>
						</div>
						<div class="col-md-2">&nbsp;</div>
					</div>
				</div>
			</div>
        </section>
	</div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		
	}):
</script>
	<?php $this->load->view('template/footer.php');?>
