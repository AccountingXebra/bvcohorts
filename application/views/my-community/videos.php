<?php $this->load->view('my-community/Cohorts-header');?>
    <style type="text/css">
	@media screen and (max-width: 1280px) and (max-height: 960px) {
		.blog-disp{
			width: 83% !important;
		}
		.video-title{ height:60px; }
	}
    #content{ padding: 0 0 0 0; border-bottom: 1px solid #f7f7f7; }
	.interview_row .pp-header{ padding:2.5% 0; }
	.interview_row .pp-header h1{ color:#595959; font-size:32px; }
	.interview_row .pp{ padding: 5px 0; line-height:25px; font-size:18px; text-align: justify; } 
	.interview_row .tc-info{ padding: 0 12%; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.playpause {
		background-image:url("<?=base_url('asset/images/overlay.png');?>");
		background-repeat:no-repeat;
		background-repeat: no-repeat;
		width: 40%;
		height: 15%;
		position: absolute;
		left: -18%;
		right: 0%;
		top: -30%;
		bottom: 0%;
		margin: auto;
		background-size: contain;
		background-position: center;
		cursor:pointer;
	}
	.mar25{ margin-top:25px; }
	.top-filter{ border-bottom:1px solid #ecedef; width:105.5% !important }
	.top-filter p{ font-size:18px; font-weight:600;	}
	.container { width: 100% !important; padding: 0px;}
	.cat_videos{ height: 100%; margin:8% 0 0 0; }
	.btn-dropdown-select{ width:105%; } 
	#main .ps-container > .ps-scrollbar-y-rail, #main .ps-container > .ps-scrollbar-y-rail > .ps-scrollbar-y{ display:none !important; }
	.videosection{ border-left:1px solid #ecedef; margin: 0; padding: 0px 0 0 25px !important; }
	.video-owner, .video-title, .brief-content{ text-align:left; margin:5px 0 0 0; height:60px; } 
	#breadcrumbs-wrapper { padding: 25px 0 15px 0; }
	.select-cat p{ font-size:16px; font-weight:600;	}
	.catselect{ text-transform:uppercase; }
	.mar300{ margin-bottom:16%; }
	#submit{ height: 38px; background-color: #bc343a; border: 1px solid #ccc; border-radius: 5px; color:#fff; font-size:13px; width:45%; margin:5% 0 0 0;}
	.btn-dropdown-select ul.dropdown-content{ height:250px !important; overflow-y:scroll !important; }
	.eachVideoRow{ border-radius:10px; border:2px solid #ccc; width: 30% !important; margin: 0 2% 2% 0; }
	.pageindex.current{ background-color: #ff7c9b; border-radius: 5px; color:#fff; border:1px solid #ff7c9b;}
	.pageindex .prev, .pageindex .next{ font-size:25px; }
	.pageindex .page:hover{ cursor:pointer; }
	.pageindex{  margin: 3% 5% 3% 0; text-align:right; }
	.pageindex a{ padding: 10px 15px; color:#313131; }
	.pageindex{ padding: 10px 15px;  background-color: transparent; border:1px solid #ccc;
    border-radius: 5px; color: #313131; text-align:  unset; margin: 0 10px; font-size: 15px; font-weight: 500; }
	#paginator{ float: right; margin: 0% 5% 0% 5%; }
	#content .container .row.blog-content { margin-bottom: 2%; }
    </style>
    <div id="main" style="padding-left:0px !important;">
		<div class="wrapper">
			<section id="content" class="bg-cp">
			<div id="breadcrumbs-wrapper">
				<div class="container custom">
					<div class="row">
						<div class="col s10 m6 l6" style="margin:0 0 0 2%;">
							<h5 class="breadcrumbs-title my-ex">VIDEOS<small class="grey-text">(Total <?=count($video)?> )</small></h5>
							<ol class="breadcrumbs">
								<li><a href="index.html">COMMUNITY / VIDEOS</a>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row blog-content">
					<div class="col s12 m12 l2">
						<div class="col s12 m12 l12 top-filter text-center">
							<p>Filter By</p>
						</div>
						<div class="col s12 m12 l12 cat_videos">
							<form name="search_catform" id="search_catform" method="post">
								<select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown vid_cat' id="video_cat" name="video_cat">
									<option value="">CATEGORIES</option>
									<option value="">ALL</option>
									<option value="Science">SCIENCE & TECHNOLOGY</option>
									<option value="Pets">SAFETY & ENVIRONMENT</option>
									<option value="People">INSPIRING INNOVATORS</option>
									<option value="Health">HEALTH & FITNESS</option>
									<option value="Lifestyle">TRAVEL & LIFESTYLE</option>
									<option value="Education">URBAN & RURAL LIFE</option>
								</select>
								<input type="submit" id="submit" name="submit" value="APPLY">
							</form>
						</div>
					</div>
					<div class="col s12 m12 l10 mar25 videosection">
					<div class="col s12 m12 l12 select-cat">
						<p class="catselect">ALL CATEGORIES</p>
					</div>
					<?php if(count($video)>0){ ?>
						<!-- foreach($video as $vid){  -->
						<?php 
							$nb_elem_per_page = 8;
							$page = isset($_GET['page'])?intval($_GET['page']-1):0;
							$number_of_pages = intval(count($video)/$nb_elem_per_page)+2;
							foreach (array_slice($video, $page*$nb_elem_per_page, $nb_elem_per_page) as $vid) { ?>

						<?php $likes=$this->Adminmaster_model->selectData('likes', '*', array('video_id'=>$vid->id,'vid_like'=>1),'id','DESC');
							$countLikes=count($likes);
							if($countLikes >=0 && $countLikes<=1000){
								$subNo=$countLikes;
								$number=$subNo;
							}else if($countLikes >=1000 && $countLikes<=100000){
								$subNo=$countLikes/1000;
								$number=$subNo."K";
							}else{
								$subNo=$countLikes/100000;
								$number=$subNo."L";
							} 
							$views1=$this->Adminmaster_model->selectData('views', '*', array('video_id'=>$vid->id),'id','DESC');  
							$countViews=count($views1);
							if($countViews >=0 && $countViews<=1000){
								$subNoV=$countViews;
								$numberView=$subNoV;
							}else if($countViews >=1000 && $countViews<=100000){
								$subNoV=$countViews/1000;
								$numberView=$subNoV."K";
							}else{
								$subNoV=$countViews/100000;
								$numberView=$subNoV."L";
							}   ?>
					<div class="col s12 m12 l3">
						<div class="text-center blog-disp">
								<a href="<?php echo base_url();?>video/<?=$vid->url?>">
										<video <?php if($vid->id==23){ ?> style="width:99%; height:150px;"<?php }else{ ?>style="width:99%; height:150px;"<?php } ?> class="otherVideo youtube editor-vl" src="<?//=$vid->video?>/<?//=$vid->filename?>" poster="<?=$vid->thumbnail?>" data-src="" frameborder="0"></video>
										<!--<div class="playpause"></div>-->		
									</a>	
							<p class="video-title"><?=$vid->title;?></p>
							<!--<p class="video-owner"><strong><?//=$vid->uploaded_by?></strong></p>-->
							<p class="brief-content mar300"><?=$numberView?> Views | <?= date('M d, Y',strtotime($vid->created_at)); ?></p>
						</div>	
					</div>
					<?php } ?> 
					<?php }else{echo "No video Available";}?>
					</div>	
					<div class="row" id='paginator'>
						<?php if(count($video) > 8 ) {
							$current="current";
							for($i=1;$i<$number_of_pages;$i++){ 
								if(isset($_GET['page']) && $_GET['page'] == $i){ $current="current"; 
								}else if(!isset($_GET['page']) && $i == 1){ $current="current"; 
								}else{ $current=""; }
								echo '<a class="pageindex '.$current.'" href="?page='.$i.'">'.$i.'</a>';
							}
						} ?>
					</div>						
				</div>
			</div>	
			</section>
		</div>	
	</div>
<script>
	$(document).ready(function() {
		$("#video_cat").on("change",function(){
			var cat_name = $(this).val();
			if(cat_name == 'all'){
				$('.catselect').text("ALL CATEGORIES");
			}else{ $('.catselect').text(cat_name); }
		});
	});		
</script>
<?php $this->load->view('template/footer'); ?>	