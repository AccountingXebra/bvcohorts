<?php $this->load->view('my-community/Cohorts-header');?>
    <!-- END HEADER -->
    <style type="text/css">
	#main{ padding-left:2% !important; }
	.add-new {
         margin: 0 0 0 0 !important;
    }
     .section_li {
         margin: 0 0 0 0 !important;
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #bc343a;
        border: 1px solid #bc343a;
    }
    .btn-theme{
            padding: 0 40px !important;
            border-radius: 6px !important;
            line-height: 45px !important;
            height: 45px !important;
      }
      ul.categories {
          text-align: center;
           margin:0;
           width: 100%;
      }
     /* li {
          padding: 0 5px 0 5px;

      }*/
      .tabs {
          height:auto;
          background: transparent !important;
          width: 100% !important;
      }
      ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
      min-height: auto !important;
      }
  a.btn.btn-theme.btn-large.section_li.active {
    background-color: #bc343a !important;
    color: #f8f9fd;
    border: 0px;
}
.tabs .tab{
  padding: 0 5px 0 5px !important;
}
.collapsible {
    border: 0px;
    -webkit-box-shadow: none;
  }
  .document-box-view.redborder.active {
    border-left: 4px solid #ffa700;

}
.collapsible-header{
  border-bottom:none;

}

	.collapsible-header.active{
        padding-bottom: 15px;
        font-weight: 600;
	}

.collapsible-body{
         padding: 0 1rem 1rem 1rem;
}
/*.collapsible-body span {
    color: #464646;
}*/
.red-bg-right:before {

    top: -32px !important;
  }
  .green-bg-right:after{
    bottom: -25px !important;
    left: 50px;
  }

  ul:not(.browser-default) > li .li_cls {
    list-style-type: disc !important;
}
	.btn-theme{
		padding:0px !important;
	}
	.tag-line{
		font-size:17px;
		color:#595959;
		margin-bottom:-20px;
	}
    </style>
    <div id="main">
      <div class="wrapper">
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">Biz Templates</h5>

                  <ol class="breadcrumbs">
                    <li><a href="index.html">START-UP KIT / BIZ TEMPLATES</a>
                  </ol>
                </div>

              </div>

            </div>
        </div>
        <div id="bulk-action-wrapper">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Attendance & </br>Leaves</p></a></li>
						<!--<li class="tab"><a  href="#test2"  class="btn btn-theme btn-large section_li">Business</a></li>-->
						<li class="tab"><a href="#test3" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Health & </br>Safety</p></a></li>
						<li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li">Legal</a></li>
						<li class="tab"><a  href="#test5"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Misc HR</br>Policies</p></a></li>
						<li class="tab"><a  href="#test7"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">On Boarding & </br> Induction</p></a></li>
						<li class="tab"><a  href="#test6"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Off Boarding </br>Exit & FNF</p></a></li>
						<li class="tab"><a href="#test8"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Performance </br>Appraisal</p></a></li>
						<li class="tab"><a href="#test9"  class="btn btn-theme btn-large section_li">Recruitment</a></li>
						<li class="tab"><a href="#test10"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Salary & </br>Compensation</p></a></li>
						<li class="tab"><a href="#test11"  class="btn btn-theme btn-large section_li">Training</a></li>
					</ul>
                 </div>
              </div>
          </div>
        </div>

		<div class="container custom">
			<div class="row">
				<div class="col l12 s12 m12">
					<div class="col l12 s12 m12">
						<p class="tag-line">You can download the policies and guides by clicking on the document name</p>
					</div>	
				</div>
				<div class="col l12 s12 m12 mt-5 before-red-bg red-bg-right after-green-bg green-bg-right" style="margin-bottom:5%;">
                <div id="test1" class="col s12 inner_row_container">
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
						<div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Attendance & Leaves/Policy - Annual Leave Plan.doc" download>Policy - Annual Leave Plan</a></div>
						<!-- onclick="export_annual();" -->
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Attendance & Leaves/Policy - Attendance.docx" download>Policy - Attendance</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Attendance & Leaves/Policy - Compensatory Off.doc" download>Policy - Compensatory Off</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Attendance & Leaves/Policy - For Leave.doc" download>Policy - For Leave</a></div>
                      </li>
                    </ul> 
				</div>
                <!--<div id="test2" class="col s12 inner_row_container">
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"> </div>
                      </li>
                    </ul>
                  </div>-->
                <div id="test3" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Health & Safety/Checklist - Workplace Safety.docx" download>Checklist - Workplace Safety</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Health & Safety/Medical Fitness Certificate Template.pdf" download>Medical Fitness Certificate Template</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Health & Safety/Policy - Sexual Harassment At Workplace.docx" download>Policy - Sexual Harassment At Workplace</a></div>
                      </li>
                    </ul>
                  </div>
                <div id="test4" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
						<div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Legal/Confidential Information Agreement.docx" download>Confidential Information Agreement</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Legal/Guide - Company Law.doc" download>Guide - Company Law</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Legal/Template - NDA Client & Vendor.doc" download>Template - NDA Client & Vendor</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Legal/Template - NDA Employee.doc" download>Template - NDA Employee</a></div>
                      </li>
                    </ul>
                  </div>
                <div id="test5" class="col s12 inner_row_container">
                    <ul class="collapsible">
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Anniversary & Birthday.doc" download>Policy - Anniversary & Birthday</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Annual Day Celebration.doc" download>Policy - Annual Day Celebration</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Canteen.doc" download>Policy - Canteen</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Cash Security System.docx" download>Policy - Cash Security System</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Code of Conduct.doc" download>Policy - Code of Conduct</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Discipline.doc" download>Policy - Discipline</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Dress Code.doc" download>Policy - Dress Code</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Email and Internet.docx" download>Policy - Email and Internet</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Mobile.doc" download>Policy - Mobile</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Transfer.doc" download>Policy - Transfer</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Travel.doc" download>Policy - Travel</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy - Work From Home.docx" download>Policy - Work From Home</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Policy on Use of Social Media by Employees.docx" download>Policy on Use of Social Media by Employees</a></div>
					  </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Misc HR Policies/Process - Cash Disbursement.doc" download>Process - Cash Disbursement</a></div>
					  </li>
                    </ul>
                  </div>
                <div id="test6" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Calculating Template - F&F Settlement.xlsx" download>Calculating Template - F&F Settlement</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Letter - Employment Contract Termination.docx" download>Letter - Employment Contract Termination</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Letter - Experience & Relieving.docx" download>Letter - Experience & Relieving</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Template - Exit Interview.doc" download>Template - Exit Interview</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Template - Handing & Taking Over.docx" download>Template - Handing & Taking Over</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Offboarding Exit and FNF/Template - No Dues.doc" download>Template - No Dues</a></div>
                      </li>
                    </ul>
                  </div>
				<div id="test7" class="col s12 inner_row_container">
                    <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Onboarding & Induction/Checklist - Induction.pdf" download>Checklist - Induction</a></div>
                       </li>
                       <li class="row document-box-view redborder">
						  <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Onboarding & Induction/Letter - Welcome New Employee.docx" download>Letter - Welcome New Employee</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Onboarding & Induction/Template - Induction.doc" download>Template - Induction</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Onboarding & Induction/Template - Joining formality.docx" download>Template - Joining formality</a></div>
                       </li>
                    </ul>
                  </div>
				<div id="test8" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Letter - Confirmation.doc" download>Letter - Confirmation</a></div>
                       </li>
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Letter - Performance Improvement.docx" download>Letter - Performance Improvement</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Letter - Performance Warning.docx" download>Letter - Performance Warning</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                          <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Letter - Promotion.doc" download>Letter - Promotion</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Performance Appraisal Form 1.docx" download>Performance Appraisal Form 1</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Performance Appraisal Form 2.doc" download>Performance Appraisal Form 2</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Performance Appraisal Guide.doc" download>Performance Appraisal Guide</a></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Performance Appraisal/Template - Employee Competency.docx" download>Template - Employee Competency</a></div>
                       </li>
                    </ul>
                  </div>
                <div id="test9" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Checklist - Recruitment & Selection.doc" download>Checklist - Recruitment & Selection</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/How To - Conduct Employee Background Check.docx" download>How To - Conduct Employee Background Check</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Internship Certificate.doc" download>Internship Certificate</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Address Proof.docx" download>Letter - Address Proof</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Appointment of Auditor.doc" download>Letter - Appointment of Auditor</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Appointment of Consultants.doc" download>Letter - Appointment of Consultants</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Appointment of Employee.docx" download>Letter - Appointment of Employee</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Interview Call.doc" download>Letter - Interview Call</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Letter - Offer Employee.doc" download>Letter - Offer Employee</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Policy - Employee Referral.doc" download>Policy - Employee Referral</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Template - Job Description.docx" download>Template - Job Description</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Template - Manpower Planning.xls" download>Template - Manpower Planning</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Template - Recruitment Report.xls" download>Template - Recruitment Report</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Recruitment/Template - Reference Check.doc" download>Template - Reference Check</a></div>
                      </li>
                    </ul>
                  </div>			  
				<div id="test10" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Agreement - Advance on Salary.docx" download>Agreement - Advance on Salary</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/How To - Claim Gratuity.docx" download>How To - Claim Gratuity</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Letter - Bank A_C Opening Introduction.doc" download>Letter - Bank A_C Opening Introduction</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Letter - Bonus.docx" download>Letter - Bonus</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Template - Annual Increment.doc" download>Template - Annual Increment</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Template - Incentive for Bus Dev.xlsx" download>Template - Incentive for Bus Dev</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Salary & Compensation/Template - Salary Slip.xls" download>Template - Salary Slip</a></div>
                      </li>
                    </ul>
                  </div>
				<div id="test11" class="col s12 inner_row_container">
                    <ul class="collapsible">
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Training/Template - Annual Training Calendar.xlsx" download>Template - Annual Training Calendar</a></div>
                      </li>
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Training/Template - Training evaluation feedback.doc" download>Template - Training evaluation feedback</a></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header"><a style="color:#bc343a;" target="_blank" href="<?php echo base_url(); ?>asset/images/kn-bank/Training/Template - Training Need Analysis.docx" download>Template - Training Need Analysis</a></div>
                      </li>
                    </ul>
				</div>
                
				</div>
			</div>
		</div>

    </section>
	</div>
	</div>
    <!-- END MAIN -->
	<script>
	if ($('.menu-icon').hasClass('open')) {
		$('.menu-icon').removeClass('open');
		$('.menu-icon').addClass('close');
		$('#left-sidebar-nav').removeClass('nav-lock');
		$('.header-search-wrapper').removeClass('sideNav-lock');
		$('#header').addClass('header-collapsed');
		$('#logo_img').show();
		$('#eazy_logo').hide();
		$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
		$('#main').toggleClass('main-full');
	}
	</script>
	<script>
		function export_annual(){
			window.location=base_url+'resource_center/download_annual_leave/';
		}
	</script>
    <?php $this->load->view('template/footer'); ?>
