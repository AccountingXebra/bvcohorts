<?php $this->load->view('my-community/Cohorts-header'); ?>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <style type="text/css">
    .add-new {
         margin: 0 0 0 0 !important;
    }
     .section_li {
         margin: 0 0 0 0 !important;
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
            padding: 0 40px !important;
            border-radius: 6px !important;
            line-height: 45px !important;
            height: 45px !important;
      }
      ul.categories {
          text-align: center;
           margin:0;
           width: 100%;
      }
     /* li {
          padding: 0 5px 0 5px;

      }*/
      .tabs {
          height:auto;
          background: transparent !important;
          width: 100% !important;
      }
      ul.tabs li {
        border:none !important;
        float: left;
        width: 135px !important;
    }
    li.tab a.active {
      min-height: auto !important;
      }
  a.btn.btn-theme.btn-large.section_li.active {
    background-color: #7864e9 !important;
    color: #f8f9fd;
    border: 0px;
}
.tabs .tab{
  padding: 0 5px 0 5px !important;
}
.collapsible {
    border: 0px;
    -webkit-box-shadow: none;
  }
  .document-box-view.redborder.active {
    border-left: 4px solid #ff7c9b;

}
.collapsible-header{
  border-bottom:none;

}

.collapsible-header.active{

    padding-bottom: 0px;
        font-weight: 600;
}

.collapsible-body{
         padding: 0 1rem 1rem 1rem;
}
/*.collapsible-body span {
    color: #464646;
}*/
.red-bg-right:before {

    top: -32px !important;
  }
  .green-bg-right:after{
    bottom: -25px !important;
    left: 50px;
  }

  ul:not(.browser-default) > li .li_cls {
    list-style-type: disc !important;
}
	.btn-theme{
		padding:0px !important;
	}
	
	a.btn.btn-theme.btn-large.section_li{ width:125px; }
	.tabs.tabs-fixed-width { display: contents !important; }
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">FAQs & Knowledge Bank</h5>

                  <ol class="breadcrumbs">
                    <li><a href="index.html">MY RESOURCE CENTER / FAQs & KNOWLEDGE BANK</a>
                  </ol>
                </div>

              </div>

            </div>
        </div>
        <div id="bulk-action-wrapper">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <!-- <ul class="categories db tabs">
                      <li> <a  href="#test" class="btn btn-theme btn-large section_li">CATEGORY A</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY B</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY C</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY D</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY E</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY F</a></li>


                    </ul> -->
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test3" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Business </br>Insights
						</p></a></li>
						<li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Clients, Sales</br>& Receipts</p></a></li>
						<li class="tab"><a  href="#test15"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Credit</br> Notes</p></a></li>
						<li class="tab"><a  href="#test2"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Vendors, Exp</br>& Payments</p></a></li>
						<li class="tab"><a  href="#test16"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Debit</br> Notes</p></a></li>
						<li class="tab"><a href="#test10"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Purchase & </br>Inventory</p></a></li>
						<li class="tab"><a  href="#test7"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Assets & </br>Depreciation</p></a></li>
						<li class="tab"><a  href="#test8"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Employees & </br> HRMS</p></a></li>
						<li class="tab"><a  href="#test19"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Salary Exp &</br>Exp Vouchers</p></a></li>
						<li class="tab"><a  href="#test9"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Cash, Petty Cash,</br>Bank & Cards</p></a></li>
						<li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li"><p style="padding: 10px 0px; line-height: 18px; margin: 5px 0 0 0;">Tax</p></a></li>
						<li class="tab"><a href="#test18"  class="btn btn-theme btn-large section_li"><p style="padding: 10px 0px; line-height: 18px; margin: 5px 0 0 0;">Accountancy</p></a></li>
						<li class="tab"><a href="#test20"  class="btn btn-theme btn-large section_li"><p style="padding: 10px 0px; line-height: 18px; margin: 5px 0 0 0;">Doc Locker</p></a></li>
						<li class="tab"><a href="#test11"  class="btn btn-theme btn-large section_li test11"><p style="padding: 10px 0px; line-height: 18px; margin: 5px 0 0 0;">My Profile</p></a></li>
						<li class="tab"><a href="#test5"  class="btn btn-theme btn-large section_li test5"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Subscription & </br>My Accounts</p></a></li>
						<li class="tab"><a href="#test6"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Security &</br> Privacy</p></a></li>
						<li class="tab"><a  href="#test17"  class="btn btn-theme btn-large section_li"><p style="padding: 10px 0px; line-height: 18px; margin: 5px 0 0 0;">Settings</p></a></li>
						<li class="tab"><a href="#test12"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Mobile </br>Application</p></a></li>
					</ul>

                   <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                 </div>
              </div>
          </div>
        </div>

          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12"><!-- before-red-bg red-bg-right after-green-bg green-bg-right -->

                  <div id="test1" class="col s12 inner_row_container ">
                   <!--  <div class="row document-box-view blueborder"> -->
                    <ul class="collapsible">
                      <!-- CLIENT MASTER -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How can I add a new client?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on ‘My Sales’ and then select ‘Client Master’.</br>
							 ●	If you are creating your first client, then click on ‘Add New Client’.</br>
							 <img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/147.png" alt=""></img></br>
							 ●	If you have already created a client before, then click on ‘Add New Client’.</br>
							 <img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/148.png" alt=""></img></br>
								●	You will enter the Create New Client section:</br>
								1.	If you are our paid plan user, then you will get access to the GST verification feature. Simply punch in your client’s GST number. Xebra will auto-verify the GST registration and fill up most of the cells automatically.</br>
								2.	If you are on our free walk plan, you will have to manually punch in all the details.</br>
								●	If your shipping address is the same as the billing address, click on the tiny green checkbox (shown in the image below)</br>
							<img width="170" height="70" src="<?php echo base_url(); ?>public/images/faqs-images/149.png" alt=""></img></br>
							●	If your client is your vendor, click on the checkbox below to create a replica profile in the vendor master.</br>
							●	Fill in their details of the contact person. To know how to add a contact person to your client profile, click here.</br>
							●	Select the days of credit you intend to extend for that client. You can set reminders for payment follow-up basis this entry.</br>
							●	If you have any closing balance with your client from last year, then you can add it as an opening balance along with appropriate Debit or Credit.</br>
							●	You can set a Revenue Target Alert for this client by clicking on the ‘+’ sign.</br>
							●	If you want to know whether you have billed X amount of revenue to that client within a specific time interval, you can set an alert here.</br>
							●	E.g., You can set an alert if your revenue from this client doesn’t cross ten lakhs by 31st August.</br>
							●	Upload these specific client-related documents like your legal contract or your purchase order in the Documents Locker.</br>
							●	You can retrieve these documents from ‘My Document Locker’ in the profile dropdown at the top right corner of your screen.</br>
							●	Ensure you click on ‘Save’ after uploading your documents and filling in all the relevant details on the page.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add single or multiple contact persons to my client profile?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on My Sales visible and then select ‘Client Master’.</br>
							 ●	Click on the ‘+’ sign of Contact Person.</br>
							 ●	A Pop-up will appear, as shown in the image below.</br>
							 <img width="550" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/150.png" alt=""></img></br>
							 ●	Select this contact person as your primary contact if you want all the emails sent to him/her.</br>
							 ●	You can also add his/her birthday and anniversary and set an alert from ‘Set Alerts’ in ‘My Settings’. This way you don’t forget to wish them on their special day. Repeat the process if you want to add multiple contact names for that client.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I change the primary contact of one of my clients?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Go to ‘Client Master’ and select ‘Edit’ of the client whose primary contact you would like to change.</br>
							 ●	In the, e.g. below, if you want to change the POC from Magesh to Megha, then simply click on the circular  checkbox against Megha’s name, and she will become the POC for that client.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit or delete a contact’s details of a specific client of mine?</div>
                        <div class="collapsible-body"><span>
                          <p>If you want to edit, click on the pencil icon at the end of the profile row. Click on the ‘X’ to delete.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add an international client to my client profile?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on ‘My Sales’ and then select ‘Client Master’.</br>
							 ●	Click on ‘Add New Client’ and fill in client details in the create section. Select the appropriate country of your client from the dropdown.</br>
							 ●	Select the appropriate currency for that country and ‘Save’.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit a client profile?</div>
                        <div class="collapsible-body"><span>
                          <p>●	In the list section of Client Master, you will see three dots that represent the action button. Hover on those and click ‘Edit’.</br>
							 ●	You will be directed to the edit client page, where you can make the changes.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How can I set a client-wise revenue Target alert for myself?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can set a Revenue Target Alert for yourself by clicking on the ‘+’ sign shown in the image below. </br>
							 ●	If you want to know whether you have billed X amount of revenue to that client within a specific time interval, you can set an alert here.</br>
							 ●	E.g. you can set an alert if your revenue from this client doesn’t cross ten lakhs by 31st August.</br>
							 ●	You can set an alert when your revenue is either above target/ below target by choosing the appropriate option from the dropdown and customized period as per your requirement.</br>
							 ●	Next, select the alert method by clicking either on Web, SMS or Email or all of three.</br>
							 ●	Alternatively, you can click on ‘Settings’ and select ‘Set Alerts’. Further, click on ‘For Clients/Vendors’ and similarly set the alert as explained above.</br>
							 <img width="270" height="550" src="<?php echo base_url(); ?>public/images/faqs-images/151.png" alt=""></img></br>
							 ●	You can set an alert when your revenue is either above target/ below target by choosing the appropriate option from the dropdown.</br>
							 ●	Set it to yearly/ bi-yearly/ quarterly/ monthly alerts as per your requirement. Select the kind of alert you would like to receive.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I retrieve or download the documents that I have added to my document locker?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Click on the profile dropdown in the top right-hand corner of the screen and click on ‘My Document Locker’.</br>
							 <img width="270" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/152.png" alt=""></img></br>
							 ●	You can then select an appropriate filter and download the specific document you require.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/153.png" alt=""></img></br>
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I retrieve or delete the documents that I have added to my document locker?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on the profile dropdown and select ‘My Document Locker’. Select the document you want to retrieve. Hover on Action 3 dots and select ‘Download’.</br>
							 ●	Similarly, select delete from the dropdown in case you want to remove that document.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/154.png" alt=""></img></br>
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I provide or remove client portal access to my client?</div>
                        <div class="collapsible-body"><span>
                          <p>●	There are two ways you can give out access to the client portal:</br>
 
							 a: While creating/editing a new client:</br>
							 While adding the client details in the client profile, you can toggle the button to the right. An email will be sent out to the primary contact person of that client with the login credentials.
							 (Read here to know what can client access from the client portal)
							 Similarly, if you move the toggle switch back to the left, access to the portal will be revoked.</br>
 
							 b. From the list section if the client is already created:</br>
							 In the list section, simply toggle the button to the right to provide the client portal access. An email will be sent out to the primary contact person of that client with the login credentials.
							 (Read here to know what can be accessed from the client portal by a client)
							 Similarly, if you move the toggle switch back to the left, access to the portal will be revoked.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is this invite button on each row of my client master?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can invite your client to start using Xebra and thereby become a part of our partner referral program.</br>
							 ●	When you click on the ‘Invite’ button, an email invite will pop up with a pre-filled message. You can edit the copy and subject line or send it as is to the relevant person of your client’s organization.</br>
							 ●	When the client signs up for any of Xebra’s paid plans, you get to win a substantial referral fee as a token of our gratitude for recommending us.</br>
                             ●	Your invite will be automatically saved, and you can track all your invites in the referral section.</br>
							 ●	You can also invite more clients, vendors, suppliers, partners, fellow business owners from here. Additionally, you can read about the monetary benefits here - https://www.xebra.in/refer-earn/</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How can I deactivate a client?</div>
                        <div class="collapsible-body"><span>
                          <p>●	In the list section of Client master, hover over the action button dots. Click on the ‘Deactivate’.</br>
							 ●	A confirmation pop-up box will appear.</br>
							 ●	Once you deactivate your client, it won’t be visible in the list section. You will have to select ‘Deactivated’ in the filter to view the list of all those clients.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">The space allotted for uploading documents has been used up. What do I do now?</div>
                        <div class="collapsible-body"><span>
                          <p>When the allotted space has been used up, you can either delete some previous documents or buy more space.</br> 
							 Follow the steps below to buy more space:</br>
							 ●	Click on the profile dropdown in the top right-side corner of your screen. Click on ‘My Accounts’ and then further click on ‘Subscribe & Shop’.</br>
							 ●	A pop up will appear, as shown in the image below. Select ‘Buy Add-ons’ and click on ‘Proceed’.</br>
							 <img width="550" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/110.png" alt=""></img></br>
							 ●	Select the amount of space you would like to buy and click on ‘Proceed’.</br>
							 ●	Verify your auto-filled details and check the Terms and Condition box and proceed.</br>
							 <img width="550" height="600" src="<?php echo base_url(); ?>public/images/faqs-images/155.png" alt=""></img></br>
							 ●	The Summary page will show you all the details. Verify and click on ‘Pay’.</br>
							 ●	You will be directed to the payment gateway.</br>
							 <img width="350" height="800" src="<?php echo base_url(); ?>public/images/faqs-images/156.png" alt=""></img></br>
							 ●	Your invoice will be emailed to you. You can also view it by clicking the profile dropdown and select ‘My Account’ and ‘My Billing History’ in that.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I download my invoice of purchase of Xebra subscriptions or Add-ons?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Click on the profile dropdown in the top right-hand corner of your screen. Click on ‘My Accounts’ and then further click on ‘My Billing History’.</br>
							 ●	Select the invoice you want to download and click on Action buttons or bulk actions.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I add a client who does not have a GSTIN no.?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can add a client that does not have a GSTIN no. Simply leave the GSTIN no. blank and move onto adding the other client details (to learn how to create a new client profile, click here).
							</p></span>
						</div>
                      </li>
					  <!-- BILLING DOCUMENTS -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a proforma invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Click on ‘My Sales’ and select ‘Billing documents’.</br>
							 ●	Hover on ‘Add New Billing Doc’ and select proforma invoice from the dropdown.</br>
							 <img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/157.png" alt=""></img></br>
							 ●	If you have already created the client from client master, then select it from the client name dropdown Xebra will autofill the GST details, place of supply and billing/ shipping address. Mention the reference PO no. and PO date, if any.</br>
							 ●	If you haven’t created a client already, then click on ‘Add New Client’ in the dropdown, with the details in the pop-up that appears.</br>
							 ●	If you have created an item already from Stock Item Master, select the item and item description from the dropdown.</br>
							 ●	If you haven’t created an item already, click on ‘Add New Item’ from the Select Item dropdown. Fill in the details in the pop up that appears.</br>
							 ●	The HSN and UOM details will be auto-filled when you select your item. Add in the desired quantity, rate and discount if applicable. The total amount will be auto calculated by Xebra.</br>
							 ●	If you would like to add multiple items in the same proforma invoice, click on ‘Add Another Line Item’ and follow the same directions.</br>
							 ●	You can add Equalization Levy, Late fees or Reverse Charges in your pro forma invoice.</br>
							 ●	You can add terms and conditions, if any, to your invoice. If you would like those for all the future proforma invoices, then click on the check box. </br>
							 ●	You can add your social media handles by clicking on ‘Customize’.</br>
							 ●	Lastly, you can add notes in your invoice, if any and click on ‘Save’.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new sales invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on ‘My Sales’ and select ‘Billing Documents’. </br>
							 ●	Hover on ‘Add New Billing Docs’ and select ‘Sales Invoice’ from the dropdown.</br>
							 <img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/157.png" alt=""></img></br>
							 ●	You will be directed to the create sales invoice page.</br>
							 ●	If you have already created the client from client master, then select it from the client name dropdown Xebra will autofill the GST details, place of supply and billing/ shipping address. Mention the reference PO no. and PO date, if any.</br>
							 ●	If you haven’t created a client already, then click on ‘Add New Client’ in the dropdown with the details in the pop-up that appears.</br>
							 ●	If you have created an item already from Stock Item Master, select the item and item description from the dropdown.</br>
							 ●	If you haven’t created an item already, select ‘Add New Item’ from the ‘Select Item’ dropdown. Fill in the details in the pop up that appears.</br>
							 ●	The HSN and UOM will be auto-filled when you select your item. Add in your desired quantity, rate and discount if applicable. The total amount will be auto calculated by Xebra.</br>
							 ●	If you would like to add multiple items in the same sales invoice, click on ‘Add Another Line’ and follow the same directions.</br>
							 ●	You can also add in equalization levy, late fees, reverse charge and expense vouchers if applicable.
							 To learn how to add an Equalization Levy, click here</br>
							 To learn how to add late fees, click here</br>
							 To learn how to add a reverse charge, click here</br>
							 To learn how to attach an expense voucher, click here</br>
							 ●	You can add terms and conditions, if any, to your invoice. If you would like those for all the future sales invoices, then click on the check box.</br>
							 ●	Next, you can add your bank details to your invoice by clicking on ‘Customize’.</br>
							 ●	You can also add your social media handles by clicking on ‘Customize’ and filling in your footer details.</br>
							 ●	Lastly, you can add notes to your invoice. You can select ‘Preview’ to have a final look before saving it.</br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How can I add an estimate?</div>
                        <div class="collapsible-body"><span>
                          <p>●	On your Xebra dashboard, click on ‘My Sales’ and select ‘Billing documents’.</br> 
							 ●	Hover on ‘Add New Billing Doc’, and select ‘Estimate’ from the dropdown options. </br>
							 <img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/157.png" alt=""></img></br>
							 ●	If you have already created the client from client master, then select it from the client name dropdown Xebra will autofill the GST details, place of supply and billing/ shipping address. Mention the reference PO no. and PO date, if any.</br>
							 ●	If you haven’t created a client already, then click on ‘Add New client’ in the dropdown will the details in the pop up that appears.</br>
							 ●	If you have created an item already from Stock Item Master, then select the item and item description from the dropdown.</br>
							 ●	If you haven’t created an item already, select ‘Add New Item’ from the ‘Select Item’ dropdown. Fill in the details in the pop-up that appears.</br>
							 ●	The HSN and UOM will be auto-filled when you select your item. Add in your desired quantity, rate and discount if applicable. The total amount will be auto calculated by Xebra.</br>
							 ●	If you would like to add multiple items in the same Estimate, click on ‘Add Another Line’ and follow the same directions.</br>
							 ●	You can also add in equalization levy, late fees, reverse charge and expense vouchers if applicable.</br>
							 To learn how to add an Equalization Levy, click here</br>
							 To learn how to add late fees, click here</br>
							 To learn how to add a reverse charge, click here</br>
							 To learn how to attach an expense voucher, click here</br>
							 ●	You can add terms and conditions, if any, to your Estimate. If you would like those for all the future Estimates, then click on the check box.</br>
							 ●	Next, you can add your bank details to your invoice by clicking on ‘Customize’.</br>
							 ●	You can add your social media handles by clicking on ‘Customize’ and filling in your footer details.</br>
							 ●	Lastly, you can add notes in your Estimate and click ‘Save.</br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I print my Pro Forma/Sales invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can print your pro forma/sales invoice directly at the time of making it.</br>
							 ●	Alternatively, from the list section, click on the Action three dots and select ‘Print’.</br>
							 ●	If you want to print invoices in bulk, then select ‘Print’ from the ‘Bulk Action’ dropdown.</br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a logo to my invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	First, upload your company logo in your company profile.</br>
							 ●	After you have done that, click on the ‘Customize Invoice’ button to create a new invoice.</br>
							 <img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/158.png" alt=""></img></br>
							 ●	You can select from multiple invoices templates. Post that, you can decide on which information should appear on the invoice, including your logo.</br>
							 <img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/159.png" alt=""></img></br>
							 •	Select company logo checkbox. You can see a reference of it on the right-hand side. Select Set For all Invoices, and this logo will now appear in all your invoices.</br>
							 <img width="900" height="600" src="<?php echo base_url(); ?>public/images/faqs-images/160.png" alt=""></img></br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add GST, CIN, PAN number or Bank Details to my invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	First, upload your company GST, CIN, PAN and Bank Details in your company profile</br>
							 ●	After you have done that, click on the ‘Customize Invoice’ button to create a new invoice.</br>
							 <img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/158.png" alt=""></img></br>
							 ●	The content section will provide you with multiple checkboxes. When you click on any of them, those details will appear in the invoice template on the right-hand side.</br>
							 <img width="450" height="550" src="<?php echo base_url(); ?>public/images/faqs-images/161.png" alt=""></img></br>
							 ●	Select the relevant entries you want to appear on the invoice and then click ‘Set for all Invoices’.</br>
							 <img width="900" height="600" src="<?php echo base_url(); ?>public/images/faqs-images/162.png" alt=""></img></br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add an equalization levy charge to the invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	While creating a new invoice, select the appropriate client and items to be billed.</br>
							 ●	To add an equalization levy charge, click on the green checkbox below the invoice details table.</br>
							 ●	Fill in the details mentioned in the pop-up box. Enter equalization levy % and amount. Equalization Levy amount means the total amount on which levy has to be charged. If this is the standard equalization levy percentage and amount, then you can set it as default.</br>
							 ●	The equalization levy charges will be directly added to the invoice details.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I attach expense vouchers with the invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	While creating a new invoice, select the appropriate client and items to be billed.</br>
							 ●	To attach expense vouchers, click on the ‘Attach Expense Vouchers’ link below the invoice details table.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/163.png" alt=""></img></br>
							 ●	If there are any pending expense vouchers for that client created by you or your colleagues, those will appear in the pop-up box.
							 <img width="850" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/164.png" alt=""></img></br>
							 ●	Add GST %, if any, and then click on ‘Add to Invoice’ against the relevant voucher you want to attach. The voucher will appear in the invoice table with all the details.</br>
							 <img width="450" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/165.png" alt=""></img></br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add late fees to the invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	While creating a new invoice, select the appropriate client and items to be billed.</br>
							 ●	To add in the late fees, click on the small green checkbox below the invoice details table.</br>
							 ●	Fill in the details of the pop-up box. You can set this as default if this is your standard amount of late fees.</br>
							 ●	The late fees amount and the item will be reflected in the invoice details table.</br>
							 <img width="900" height="280" src="<?php echo base_url(); ?>public/images/faqs-images/166.png" alt=""></img></br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make invoices from multiple GST numbers of my company?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Add your multiple GST details in the company profile. These will now be visible on the top white dropdown. You can select the appropriate GST number of your company. Then follow the process of making the invoice.</br>
							 ●	The dropdown at the top will allow you to toggle between multiple GST numbers of your company.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record credit note against a particular invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can record credit note against an invoice in two ways:</br>
							 ●	<strong>Shortcut:</strong></br>
							Go to the list section of billing documents. Click on the three action dots of the invoices on which you want to create the credit note. Select ‘Credit note’, and you will be taken to create a credit note section.</br>
							 ●	Longer approach:</br>
							Click on ‘Credit notes’ in ‘My Sales’ modules. Select ‘Add new credit note’. Select the appropriate client and the invoice number against which you want to create the credit note.</br>
							 ●	Select the reason for the credit note and items on which you want to raise the credit note.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record sales receipt against a particular invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can record sales receipts against an invoice in two ways:</br>
							●	Shortcut:</br>
							Go to the list section of billing documents. Click on Action three dots of the invoices against which you want to record the receipts. Select ‘Receipts’, and you will be taken to Create sales receipt section. The cells will be pre-filled with those invoice details</br>
							●	Longer approach:</br>
							Click on ‘Sales Receipts’ in Receipts and Payments modules. Select ‘Add New Sales Receipt’. Select the appropriate client and the invoice number.</br>
							●	If you have received a Bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							●	Fill in the balance details and click on ‘Save’.</br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record bad debt against an Invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can record bad debts against an invoice in two ways:</br>
							●	Shorter approach:</br>
							Go to the list section of billing documents. Click on Action three dots of the invoices against which you want to record the bad debts. Fill up the details of the pop-up.</br>
							●	Longer approach:</br>
							Click on ‘Sales Receipts’ in ‘Receipts and Payments’ module. Select ‘Add New Sales Receipt’. Select the appropriate client and the invoice number.</br>
							●	In the Receipts section, in the ‘Nature of Receipts’ dropdown, select ‘Bad Debts’ in case of complete Bad Debts. In case of partial bad debts, then select ‘Part Payment’ in ‘Nature of Receipts’ cell. Fill in the amount of the bad debt in the cell below.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/167.png" alt=""></img></br>
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I carry out e-invoicing?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Go billing documents and click on Action 3 dots of the invoice to carry out e-invoicing. Select ‘E-invoicing’ from the dropdown options.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/168.png" alt=""></img></br>
							●	Xebra will automatically get the e-invoicing done, and your invoice will have a QR code and IRN number printed on it. The green dot at the end of that row turns purple, indicating successful e-invoicing of that particular invoice.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What does Xpress mean, and how do I use it?</div>
                        <div class="collapsible-body"><span>
                          <p>●	Xpress is a superfast way of sending a document to your client who is also using Xebra. You don’t have to email your invoices to them but simply Xpress them. When you Xpress an invoice or credit note, then your client receives it immediately.</br>
							 ●	If the client is not using Xebra, then the feature will be inactive. You could invite that client to start using Xebra and become a part of our referral program.</br>
							 How to use it</br>
							 ●	Click on ‘My Sales’ and select ‘Billing Documents’. Hover on Action 3 dots of the invoice that you want to Xpress deliver. Click on ‘Xpress’.</br>
							 ●	If your client is not a Xebra user, you will see the below notification on the top of your home page. </br>
							 <img width="800" height="60" src="<?php echo base_url(); ?>public/images/faqs-images/169.png" alt=""></img></br>
							 ●	If your client is also a Xebra user, then he will get your invoice instantly.
							</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Why do I not see the Edit option in a few invoices?</div>
                        <div class="collapsible-body"><span>
                          <p>All the invoices that have the status of Part or Full received cannot be edited. It means that you have already recorded receipts against those invoices.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I view my past invoices?	</div>
                        <div class="collapsible-body"><span>
                          <p>●	To view your past invoices, click on ‘My Sales’ and then ‘Billing Documents’.</br>
							 ●	Click on the invoice number and date of the invoice you want to view.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I directly convert an estimate to a sales invoice at a later date?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes. Select the Estimate and hover on the Action three dots. Select ‘Convert to Invoice’ to automatically convert your Estimate into an Invoice. You will retain a copy of your Estimate as well.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a digital signature to my invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>●	The first step is to upload your signature in ‘My Personal Profile’. Go to your Xebra dashboard, and click on the Profile dropdown.</br>
							 ●	Click on ‘My Personal Profile’. Upload your signature in the cell given and click ‘Save’.</br>
							 <img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/170.png" alt=""></img></br>
							 ●	Now, in the create invoice section, click on ‘Customize Invoice’. In the Content section, select the check box of ‘Add Digital Signature’. It will show your signature on the invoice.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/171.png" alt=""></img></br></p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record retainer invoices?</div>
                        <div class="collapsible-body"><span>
                          <p>●	 If you are creating a new invoice (to learn how to create a new invoice, click here), fill in your relevant billing details.</br>
							 •	If you have already created the client from client master, then select it from the ‘Client Name’ dropdown Xebra will autofill the GST details, place of supply and billing/ shipping address. Mention the reference PO no. and PO date, if any.</br>
							 •	If you haven’t created a client already, then click on ‘Add New Client’ in the dropdown with the details in the pop-up that appears.</br>
							 •	Under ‘Nature of invoice’, select ‘Recurring’ from the dropdown options. This option will enable you to create a retainer invoice.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/172.png" alt=""></img></br>
							 •	If you have created an item already from Stock Item Master, select the item and item description from the dropdown.</br>
							 •	If you haven’t created an item already, select ‘Add New Item’ from the Select Item dropdown. Fill in the details in the pop up that appears.</br>
							 •	The HSN and UOM will be auto-filled when you select your item. Add in your desired quantity, rate and discount if applicable. The total amount will be auto calculated by Xebra.</br>
							 •	If you would like to add multiple items in the same sales invoice, click on ‘Add Another Line’ and follow the same directions.</br>
							 •	You can also add in equalization levy, late fees, reverse charge and expense vouchers if applicable.</br>
							 To learn how to add an Equalization Levy, click here</br>
							 To learn how to add late fees, click here</br>
							 To learn how to add a reverse charge, click here</br>
							 To learn how to attach an expense voucher, click here</br>
							 •	You can add terms and conditions, if any, to your invoice. If you would like those for all the future sales invoices, then click the check box.</br>
							 •	Next, you can add your bank details to your invoice by clicking on ‘Customize’.</br>
							 •	You can add your social media handles by clicking on ‘Customize’ and filling in your footer details.</br>
							 •	Lastly, you can add notes to your invoice. You can select ‘Preview’ to have a final look before saving it.</br>
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I delete a particular row from the table?</div>
                        <div class="collapsible-body"><span>
                          <p>●	If you are creating a new invoice (to learn how to create a new invoice, click here), fill in your relevant billing details.</br>
							 ●	To delete a row from an invoice that is already created, click on ‘Edit’ (to learn how to edit a new invoice, click here).</br>
							 ●	In the billing details table, select the line item and hover on the amount section. A pink ‘X’ button will appear. Click on it to delete the row.</br>
							 <img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/173.png" alt=""></img></br></p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record and account for forex gain or loss that accrues because of my international client?</div>
                        <div class="collapsible-body"><span>
                          <p>●	In the scenario of receiving money from your international client or making payment to a global vendor, Xebra auto-records the forex gain or loss that might have accrued.</br>
							 ●	In the sales receipt module, go to create a new entry and select the international client from the client name dropdown. Xebra distinguishes the international client from a domestic one and asks you to enter the global currency exchange rate with that of the domestic.</br>
							 ●	It then compares the forex rate at the time of making the invoice and now at the time of recording the receipt. Accordingly, the amount will be calculated and shown either in the Forex gain cell or forex loss cell.</br>
							 <img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/174.png" alt=""></img></br>
							 ●	Follow similar steps for recording payment to an international vendor.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record bulk receipts from my client?</div>
                        <div class="collapsible-body"><span>
                          <p>•	You can record sales receipts against an invoice in two ways:</br>
							 •	Shortcut:</br>
							 •	Go to the list section of billing documents. Click on Action three dots of the invoices against which you want</br> to record the receipts. Select ‘Receipts’, and you will be taken to Create sales receipt section. The cells will be pre-filled with those invoice details.</br>
							 <img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/168.png" alt=""></img></br>
							 •	Longer approach:</br>
							 •	Click on ‘Sales Receipts’ in the ‘Receipts and Payments’ modules. Select ‘Add new sales receipt’. Select the appropriate client and the invoice number.</br>
							 <img width="350" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/175.png" alt=""></img></br>
							 ●	If you have received a Bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							 ●	Fill in the balance details and click ‘Save’.
							 </p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record part receipts from my client?</div>
                        <div class="collapsible-body"><span>
                          <p>●	You can record sales receipts against an invoice in two ways:</br>
							 ●	Shortcut:</br>
							 Go to the list section of billing documents. Click on Action three dots of the invoices against which you want to record the receipts. Select ‘Receipts’, and you will be taken to Create sales receipt section. The cells will be pre-filled with those invoice details.</br>
							 ●	Under the status of payment, click on ‘Part Payment.’ Similarly, for full payment, select ‘Full Payment’ from the dropdown options.</br>
							 <img width="350" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/176.png" alt=""></img></br>
							 ●	Longer approach:
							 Click on ‘Sales Receipts’ in the ‘My Receipts and Payments’ modules. Select ‘Add New Sales Receipt’. Select the appropriate client and the invoice number.</br>
							 <img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/175.png" alt=""></img></br>	
							 ●	If you have received a Bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							 ●	 Fill in the balance details and click ‘Save’.
							 </p></span>
						</div>
                      </li>
					  
					  
					  
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record international clients and make an estimate or invoice in international currency?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can record international clients from Client master. While filling up the data of that client, you can select the currency in which you want to invoice them in</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What are Items?</div>
                        <div class="collapsible-body"><span>
                          <p>Items are used when you’re billing for goods or products sold to your Client. These Items can be tangible (like parts) or intangible (like a hosting package). You can create these from Item Master in Sales Module</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">I tried to create a new item, but an error says the name is taken</div>
                        <div class="collapsible-body"><span>
                          <p>Item names must be unique. If the name is taken, the Item name already exists elsewhere in your Archived Items section, or in the deactivated section.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I upload more than 1 company logo?</div>
                        <div class="collapsible-body"><span>
                          <p>No, you can upload only one company logo.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">I am not from India. Will Xebra be useful to me?</div>
                        <div class="collapsible-body"><span>
                          <p>The tax module is customized to Indian tax laws. So apart from that Xebra would be useful to you</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Does Xebra support other language apart from English?</div>
                        <div class="collapsible-body"><span>
                          <p>Not at the moment</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How much time does it take to get Xebra set up?</div>
                        <div class="collapsible-body"><span>
                          <p>In an instant. Xebra is a SaaS product who you can start</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I convert an estimate into an invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>You can select a particular estimate in list view of billing documents and click on ‘Convert
							To Invoice’ option in action buttons</p></span>
						</div>
                      </li>
                     <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I charge Equalization levy to my client?</div>
                        <div class="collapsible-body"><span><p>There is a check box provided in the invoice which you need to click. You will be asked to fill the percentage of Equalisation Levy and the amount that needs to be collected. Once you enter in the details, it will appear as a line item in your invoice</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">I can’t find a section to add late fees?</div>
                        <div class="collapsible-body"><span><p>Click on customise in the invoice. In the content tab, select the option of adding Late fees. You can click on option of Set as Default if you want that cell to appear in all invoices </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">I want to write my company’s address in invoice footer. How do I do that?</div>
                        <div class="collapsible-body"><span><p>Very simple. Click on customize at the bottom white bar of your invoice. Select the Footer option and select the options that should appear in the footer of your invoice. Don’t forget to click on ‘Set As Default’ so that you don’t have to do it for every invoice</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the level of customisations that are possible with it?</div>
                        <div class="collapsible-body"><span><p>We realise that every organization is their way of conducting business and making billing documents. So, we have provided a slew of features to adapt Xebra completely as per your requirement. Few of things that you can do</p>
                      <ul style="margin-left: 20px;">
                        <li class="li_cls">Complete customisation of invoices</li>
                        <li class="li_cls">Add payments to your invoices</li>
                        <li class="li_cls">Alerts to notify about client’s birthdays and anniversaries</li>
                        <li class="li_cls">Personalise notification on late-payers</li>
                      </ul>
						</span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How will I be reminded of my outstanding payments?</div>
                        <div class="collapsible-body"><span><p>You can set reminders of credit period for each client individually and Xebra will automatically set reminder emails to your client if they cross the credit period.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I Able to change the credit period set for my clients?</div>
                        <div class="collapsible-body"><span><p>Yes, you can change it from client master & edit the information for that client.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I delete the clients that I am not working with anymore?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them & also can reactivate if needed in future. You can deactivate clients in action button dropdown in client master.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is it possible to record recurring invoices like retainer amount without having to make an entry for it every time?</div>
                        <div class="collapsible-body"><span><p>Yes, while creating an invoice, select recurring in nature of invoice dropdown. You can set the recurring to the number of times you want. Xebra will automatically generate an invoice of that amount at the set frequency</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I able to delete the services which our company have discontinued?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them in item master from action button dropdown. You can reactivate that service if required in future</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record my total or partial bad debts against my invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View and click on Bad Debts option in Action Button. You will be guided to Sales Receipt module where you will record either entire or partial amount as bad debts</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record part payment and part bad debt on an invoice?</div>
                        <div class="collapsible-body"><span><p>In the Sales Receipt module, select the invoice against which you wish to record the details. In the receipt section select Part Received in Receipt Status and add the relevant amount in Receipt Amt and Bad Debts Amount cell</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record amount received from my clients against multiple invoices?</div>
                        <div class="collapsible-body"><span><p>While recording the receipt from your client in Sales Receipt module, simply click on Add Another Invoice button and you can select the 2nd invoice against which you have received the payment. You will be saved the hassle of recording the second payment separately </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record forex gain or loss incurred because of international client payment timing?</div>
                        <div class="collapsible-body"><span><p>Select the appropriate currency for your client in the client master. Then enter the forex rate at the time of making the invoice and at the time of receiving the payment. Xebra will automatically the forex gain or loss derived from it </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I customise or stop my alerts?</div>
                        <div class="collapsible-body"><span><p>Go to Settings and Click on Set Alerts & Notifications. Select the appropriate module and type of notification you wish to change</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How many types of alerts are there?</div>
                        <div class="collapsible-body"><span><p>You can select from 3 types of notification: Web, Email and/or SMS</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">After providing access to clients in client portal, can we restrict them afterwards?</div>
                        <div class="collapsible-body"><span><p>Yes, you can deny them access by simple toggle button in client master</p></span></div>
                      </li>
                      
					  
					  <!--li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record my complete or partial bad debts against my invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View and click on Bad Debts option in Action Button. You will be guided to Sales Receipt module where you will record either entire or partial amount as bad debts</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I make invoices and record sales receipt from the mobile application?</div>
                        <div class="collapsible-body"><span><p>You will be able to record that from your web interface only</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I customise or stop my notifications?</div>
                        <div class="collapsible-body"><span><p>Go to Settings and Click on Set Alerts &amp; Notifications. Select the appropriate module and type of notification you wish to change</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How many types of notification are there?</div>
                        <div class="collapsible-body"><span><p>You can select from 3 types of notification: Web, Email and/or SMS</p></span></div>
                      </li-->

                    </ul> <!-- </div> -->
                  </div>
                  <div id="test2" class="col s12  inner_row_container">
                    <ul class="collapsible">
                      <!--li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record expenses incurred with international vendors and record a payment in international currency?</div>
                        <div class="collapsible-body"><span><p>Yes, you can record international vendors from vendor master. While filling up the data of that vendor, you can select the currency in which you want to pay them</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I able to delete the vendor whose services we have discontinued?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them in vendor master from action button drop down. You also have the option to reactivate them should you resume working with them</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I create company expense with GST reverse charge or input tax credit?</div>
                        <div class="collapsible-body"><span><p>Yes, you have to just check the check box while creating a company expense.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I select different GST while recording each expense?</div>
                        <div class="collapsible-body"><span><p>Xebra will automatically select the right GST basis the place of supply of the vendor.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can we direct create a purchase order without creating expense?</div>
                        <div class="collapsible-body"><span><p>No, first, you have to create an expense, and then you have to select the vendor for whom you want to make that purchase order.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is it possible to record recurring expenses like rent without having to make an entry for it every time?</div>
                        <div class="collapsible-body"><span><p>Yes, in company expense module, you select the expense and then choose recurring in nature of the expense. You can set the recurring to the number of times you want. Xebra will automatically record the expense of that amount at the set frequency.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record my non-payment or partial payment against my vendor’s invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View in Company Expenses and click on Make Payment option in Action Button. You will be guided to Expense Payment module where you will record the entry</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record bulk payment made to a vendor’s multiple invoices?</div>
                        <div class="collapsible-body"><span><p>While recording the expense payment, simply click on add another invoice button which will allow you to club another invoice from the same vendor for the payment</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record the expenses paid to an International vendor in foreign exchange?</div>
                        <div class="collapsible-body"><span><p>Follow these steps for correct recording of an international currency payment</br>
						1. Select appropriate country & Currency while recording in the vendor master</br>
						2. When you record your company expense for that vendor, the system will automatically ask you for a foreign exchange rate to the filled that is prevailing in the current time</br>
						3. When you make the payment after say a period of one month, the system will again prompt you to enter the foreign exchange rate prevalent then and auto-calculate the forex gain or loss that you might have incurred
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record forex gain or loss incurred because of international vendor payment timing?</div>
                        <div class="collapsible-body"><span><p>Select the appropriate currency for your vendor in the vendor master. Then enter the forex rate at the time of making the invoice and at the time of receiving the payment. Xebra will automatically the forex gain or loss derived from it</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">After providing access to vendors in the vendor portal, can we restrict them afterwards?</div>
                        <div class="collapsible-body"><span><p>Yes, you can deny them access by simple toggle button in the vendor master.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record company expenses and expense payment from the mobile application?</div>
                        <div class="collapsible-body"><span><p>No, you will be able to record that from your web interface only</p></span></div>
                      </li-->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add a new vendor/s to my profile?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Vendor master’.</br>
							2.	Click on ‘Add New Vendor’ option on the RHS of the screen.</br>
							3.	You will be directed to the create vendor page. Fill in the company name, address, GSTIN and other relevant details, then click on 'Save'.</br>
							4.	You will be able to view the vendor details on the vendor list page.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit or deactivate the vendor's profile?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Vendor Master’.</br>
							2.	In your vendor list, hover on the three dots of the specific vendor and click on ‘Deactivate’ in the dropdown option.</br>
							3.	Select ‘Deactivate’ on the confirmation pop up that will appear.</br>
							<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/63.png" alt=""></img></p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add an international vendor to my profile?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Vendor Master’.</br>
							2.	Click on ‘Add New Vendor’ option.</br>
							3.	Fill in the international company name, their local address, GSTIN and other relevant details, then click on 'Save'.</br>
							4.	You will be able to view the new vendor details on the vendor list page.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I give out vendor portal access to my vendor? What will he be able to access?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on My Purchases and Expenses and then select ‘Vendor Master’. From the vendor list on the RHS, activate the toggle switch under Vendor portal access.</br>
							2.	A confirmation pop up will appear as shown in the image.</br>
							<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/64.png" alt=""></img></br>
							3.	Click on Yes and confirm.</br> 
							Alternatively, you can also give vendor portal access while creating a new vendor or editing a current vendor.  To learn how to edit particular vendor details, click here</br>
							<img width="650" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/65.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I change the contact person for a specific vendor profile?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Vendor master’.</br>
							2.	You will be able to view your vendor list on the RHS of the screen. </br>
							3.	Hover on the three dots of the specific vendor's row and click on ‘Edit’ in the dropdown option.</br>
							4.	On the edit vendor page, go to the contact person section and click on the '+' sign.</br>
							<img width="650" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/65.png" alt=""></img></br>
							4. A pop up will appear, as shown in the image below. </br>
							5. Fill in the name, email id, mobile no. and anniversary etc., as applicable. </br>
							6. After filling in all the details, check the green checkbox 'Set as Primary Contact'. Finally, click on ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/66.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is this invite button on each row of my vendor master?</div>
							<div class="collapsible-body"><span><p>1.	The invite button is a unique feature of Xebra, which allows you to directly send an automated email invite to your vendor from the application. When you click on ‘Invite’, a pop up will appear as shown in the image.</br>
							<img width="450" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/67.png" alt=""></img></br>
							2.	Fill in the relevant details, and make changes in the email as per your requirement, then click on ‘Send’. Your vendor will have received an automated E-invite from Xebra.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I add a vendor who doesn’t have a GST number?</div>
							<div class="collapsible-body"><span><p>Yes, you can add in a vendor that does not have a GSTIN no. by simply leaving the GST box blank. </br>
							To learn how to create a new vendor, click here.</p></span></div>
						</li>
						<!-- PURCHASE AND EXPENSE MASTER -->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I create a new expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My purchases and Expenses’ and then select ‘Purchase and Expense Master’. Click on ‘Add New Expense'.</br>
							2.	Fill in the statutory info like the GST, TDS and any other taxes relevant to this expense, and then click on ‘Save’. </br>
							3.	You can view the new expense in the expenses list on the main page, as shown in the image.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/68.png" alt=""></img></br></p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit and deactivate a particular expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My purchases and Expenses’ and then select ‘Purchase and Expense’.</br>
							2.	In your expenses list, hover on the three dots next to the specific vendor. Select ‘Deactivate’ from the dropdown options.</br>
							3.	Select ‘Deactivate’ on the pop-up that appears.</br>
							<img width="500" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/69.png" alt=""></img></br></p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export single or multiple expenses?</div>
							<div class="collapsible-body"><span><p>To download a single expense:</br>
							1.	On your Xebra dashboard, click on ‘My purchases and Expenses’ and then select ‘Purchase and Expense Master’.</br>
							2.	In your expense list, hover on the three vertical dots of your selected expense.</br>
							3.	Click on ‘Export’ from the dropdown options.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/70.png" alt=""></img></br>
							To download multiple expenses:</br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Purchase and Expense Master’.</br>
							2.	Your list of expenses will appear</br>
							3.	Select the expenses that you would like to export and hover on the Bulk Actions, Select ‘Export’ from the dropdown </br>
							<img width="450" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/71.png" alt=""></img></br>
							4.	Your desired expenses will be exported in excel format.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an alert for an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the ‘Settings’ icon on the top right corner of the screen.</br>
							Click on ‘Set alert for’ and select ‘Stock Item / Expense’ from the dropdown options.</br>
							<img width="250" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/72.png" alt=""></img></br>
							2.	Click on ‘Add New Alert’ and fill in the details.</br>
							3.	Give an appropriate alert name and select the alert for ‘Above/Below’ revenue target by clicking on the dropdown options. Select the exact date you would like to be notified and the type of notification you would like to receive - web, email, mobile or all 3. no. icon (when you select them, it will turn green), then click on ‘Save’.</br>
							<img width="450" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/73.png" alt=""></img>
							</p></span></div>
						</li>
						<!-- COMPANY EXPENSES -->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add a new company expense?</div>
							<div class="collapsible-body"><span><p>1.	Go to your Xebra dashboard, and click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	To create a new company expense, click on ‘Add New Company Expense’, visible on the RHS of the screen.</br>
							3.	On the create company expense page, select the type of expense from the dropdown options. Then select the expense name and ‘Nature of Expense’ (one time/ recurring) from the dropdown options.</br>
							4.	Select the expense category from the dropdown options. You can also upload a soft copy of the invoice.</br>
							<img width="1000" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/74.png" alt=""></img></br>
							5.	Then fill in the details in the company expense table and customise any other relevant information. Then click on ‘Save’.</br>
							6.	You can also add in the equalisation levy/ reverse charge fees by clicking on the tiny checkbox below the expense details table.</br>
							7.	Your company expense details will be updated on the main page, as shown in the image below.</br>
							<img width="1000" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/75.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit and delete a company expense?</div>
							<div class="collapsible-body"><span><p><strong>To edit a company expense, follow the steps below:</strong></br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expenses list, hover on the three dots next to the relevant expense. A drop down will appear.</br>
							3.	Click on ‘Edit’. You will be directed to the edit company expense page.</br>
							<img width="600" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/76.png" alt=""></img></br>
							4.	Make the desired changes and click on ‘Save’.</br>
							<strong>To delete a company expense, follow the steps below:</strong></br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expense list, hover on the three dots next to the relevant expense. Select ‘Delete’ from the dropdown options.</br>
							3.	Select delete on the confirmation pop-up that will appear.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Do I need to manually add in details of a new expense order to my accounting report?</div>
							<div class="collapsible-body"><span><p>No. You do not need to manually add in your accounting report's details of your expense order. Xebra auto adds all the details in your accounting report.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I create a purchase order on an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expense list, hover on the three dots next to the relevant expense. A drop down will appear.</br>
							3.	Click on ‘Purchase Order’. It will directly take you to a create purchase order page.</br>
							4.	You will have to add in the PO manually. Start and end date. </br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/77.png" alt=""></img></br>
							5.	Fill in the relevant details, and click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add a reference purchase number while recording an expense?</div>
							<div class="collapsible-body"><span><p>To add a reference purchase number while recording an expense, follow the steps below:</br>
							1.	On your Xebra dashboard, click on My purchases and expenses and then select ‘Company Expense’.</br>
							2.	To create a new company expense, click on ‘Add New Company Expense’, visible on the RHS of the screen. </br>
							3.	On the create company expense page, select the type of expense from the dropdown options. Then select the expense name and Nature (one time/ recurring) from the dropdown options.</br>
							4.	Subsequently, select the expense category from the dropdown options. You can also upload a soft copy of the invoice. </br>
							5.	You can add in the reference no. and date as shown in the image on the RHS corner of the page.</br>
							<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/78.png" alt=""></img></br>
							6.	Further, add the expense details in the expense table and click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I create a debit note on an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expense list, hover on the three dots next to the relevant expense. A drop down will appear.</br>
							3.	Click on ‘Debit Note’. It will direct you to the create debit note page.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/79.png" alt=""></img></br>
							4.	Fill in the expense name and reason for the debit note from the dropdown options.</br>
							5.	You can also add in the purchase return and debit note details. You can also add the equalisation levy charge by clicking on the tiny green checkbox. </br>
							6.	After filling in all the relevant details, click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record TDS against a particular expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts and Payments’ and then select ‘Payments’. Further select ‘Purchase & Expense payments’.</br>
							<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/80.png" alt=""></img></br>
							2. On the RHS of the screen, click on ‘Add New Expense Payment’, and you will be directed to the create payment page. </br>
							3. Fill in the vendor name and expense number, and Xebra will auto-fill the other expense details.</br>
							4. Under ‘Payment Details’, fill in the payment status, mode of payment and payment amount. </br>
							5. If you wish to add in TDS amount, interest amount or penalty charges, you can do so as shown in the image.</br>
							6. You can also add any notes and upload any relevant documents in the designated boxes given below. After checking all the relevant details, click on ‘Save’.</br>
							<img width="400" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/81.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit a particular TDS percentage of an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expense list, hover on the three dots next to the relevant expense. Select ‘Edit’ from the dropdown options.</br>
							3.	On the edit expense page, scroll below to find the TDS% option under Tax information.</br>
							4.	After checking all your details, click on ‘Save’.</br>
							<img width="800" height="80" src="<?php echo base_url(); ?>public/images/faqs-images/82.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a payment against an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	In your company expense list, hover on the three dots next to the relevant expense. A drop down will appear.</br>
							3.	Click on ‘Payment’. It will direct you to the create payment page.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/83.png" alt=""></img></br>
							4.	Fill in the expense amount, expense category and other details.</br>
							5.	You can also create another payment by clicking on the button.</br>
							6.	Then fill in your payment details as shown in the image below. </br>
							7.	You can also upload any documents that you may have related to the payment. </br>
							8.	After checking all the above details, click on ‘Save’.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/84.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a recurring expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Expense’.</br>
							2.	To create a recurring expense, click on ‘Add New Company Expense’, visible on the RHS of the screen.</br>
							3.	On the create company expense page, select the type of expense from the dropdown options. Then select the expense name, and under the Nature of the expense, select ‘Recurring’ from the dropdown options.</br>
							4.	You can also add the date of recurrence, no of times, and the frequency as shown in the below image.</br>
							<img width="600" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/85.png" alt=""></img></br>
							5.	On the RHS of the screen, select the expense category from the dropdown options. You can also upload a soft copy of the invoice. </br> 
							6.	Then fill in the details in the company expense table and customise any other relevant information. Then click on ‘Save’.</br>
							7.	You can also add in the equalisation levy/ reverse charge fees by clicking on the tiny checkbox below the expense details table.</br>
							8.	Your company expense details will be updated on the main page, as shown in the image below.</br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/75.png" alt=""></img></br>
							If you want to add an equalisation levy charge to an already created expense, edit your expense and follow the same steps. To learn how to edit an expense, click here.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add an equalisation levy to an expense?</div>
							<div class="collapsible-body"><span><p>To add an equalisation levy charge to an expense, follow the steps below:
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br>
							2.	To create a new company expense, click on ‘Add New Company Expense’, visible on the RHS of the screen. If you want to add the equalisation levy charge to an already created expense, click on edit expense. To learn how to edit an expense, click here.</br>
							3.	On the create company expense page, select the type of expense from the dropdown options. Then select the expense name and Nature (one time/ recurring) from the dropdown options.</br>
							4.	On the RHS, select the expense category from the options. You can also upload a soft copy of the invoice.</br> 
							5.	Then fill in the details in the  Company expense table and customise any other relevant information </br>
							6.	You can add in the equalisation levy charge by clicking on the tiny checkbox below the expense details table.</br>
							7.	A pop up will appear, as shown in the image below.</br> 
							<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/86.png" alt=""></img></br>
							8.	Fill in the equalisation levy details, as shown in the image and click on ‘Save’.</br>
							9.	Your charges will show in the expense table as shown in the image.</br>
							<img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/87.png" alt=""></img></br>
							10.	After checking all your expense details, click on ‘Save’.</br>
							11.	Your company expense details will be updated on the main page.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a reverse charge in expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Company Expense’.</br> 
							2.	To create a new company expense, click on ‘Add New Company Expense’, visible on the RHS of the screen. If you want to add the reverse charge to an already created expense, click on edit expense. To learn how to edit an expense, click here.</br> 
							3.	On the create company expense page, select the type of expense from the dropdown options. Then select the expense name and Nature (one time/ recurring) from the dropdown options.</br> 
							4.	On the RHS, select the expense category from the dropdown options. You can also upload a soft copy of the invoice. </br> 
							5.	Then fill in the details in the company expense table and customise any other relevant details. </br> 
							6.	You can add in the reverse charge by clicking on the tiny checkbox below the expense details table.</br> 
							7.	Xebra will automatically reverse the charge in the expense table, as shown in the image below.</br> 
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/88.png" alt=""></img></br>
							8.	After checking all the details, click on ‘Save’. Your reverse charge will be recorded.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I delete a particular row from the table?</div>
							<div class="collapsible-body"><span><p>1. When you are creating a new expense (to learn how to create a new expense, click here), fill in your relevant details in the expense document. </br>
							OR to delete a row from a fee that is already created, edit the invoice (to learn how to edit an expense, click here)</br> 
							2. In the expense table, go to the RHS corner of the desired line item and hover near the amount section, an ‘X’ button will appear as shown in the image. </br>
							3. Click on the ‘X’ to delete your row and click on ‘Save’.</br> 
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/89.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How can I make a company expense payment directly from my Xebra portal?</div>
							<div class="collapsible-body">
								<p>
									•	On your Xebra dashboard, click on ‘My Purchases & Expenses’ and again on ‘Company Expenses’.</br>
									<img width="200" height="370" src="<?php echo base_url(); ?>public/images/faqs-images/238.png" alt=""></img></br>
									•	From the list of company expenses, hover on the three dots next to the one you would like to make the payment against.</br>
									•	A dropdown will appear from where you will select ‘Payment’. </br>
									<img width="900" height="370" src="<?php echo base_url(); ?>public/images/faqs-images/239.png" alt=""></img></br>
									•	An OTP confirmation popup will appear (please note- this feature of OTP confirmation will only appear If you have already integrated your ICICI bank account with Xebra).</br>
									<img width="950" height="465" src="<?php echo base_url(); ?>public/images/faqs-images/240.png" alt=""></img></br>
									•	To learn how to integrate your ICICI account directly with Xebra, <a class="bColor ohhh">click here</a>.</br>
									•	After entering the correct OTP, you will be directed to a create payment page to verify the amount
									</br> 
									<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/241.png" alt=""></img></br>
									•	All the details on the create payment page will already be pre-filled, except the payment status. </br>
									•	Click on the arrow sign and under payment status- make sure you fill in the correct amount received (full/half/part).</br>
									•	Double-check all the amounts and click on ‘Save’. Your bank account and all accounting statements will be automatically updated </br>
									•	You will receive a confirmation notification for the same.</br>
									<img width="350" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/242.png" alt=""></img></br>
									•	You can also check if the payment has been recorded by clicking on the ‘My Receipts and Payments’ module and selecting ‘Payments’ from the dropdown options.</br>
									•	Under payments, click on Purchases & expense payments from the dropdown options.</br>
									<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/243.png" alt=""></img></br>
									•	You will be able to view your recorded payment on the RHS of the screen.
								</p>
							</div>
						</li>	
                    </ul>
                  </div>
                  <div id="test3" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How is Revenue calculated in the formula?</div>
                        <div class="collapsible-body"><span><p>Our formula is Total Revenue (minus) Total Credit Notes (minus) Total Bad Debts</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I get additional graphs pertaining to my business?</div>
                        <div class="collapsible-body"><span><p>If you would like customized graphs for your business, send us an email detailing the same. Our team will reach out to you and understand your requirements and take the discussion forward accordingly</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does BI tool help me understand my outstanding credit?</div>
                        <div class="collapsible-body"><span><p>Yes, In our credit history sub-module in Business Insights, you will get O/S credit information both client-wise and invoice-wise.</p></span></div>
                      </li>
                    </ul>
                  </div>
                  <div id="test4" class="col s12 inner_row_container">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What does TDS stand for?</div>
							<div class="collapsible-body"><span><p>TDS stands for tax deducted at source.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">When is TDS to be deducted?</div>
							<div class="collapsible-body"><span><p>According to the income tax act, in case of certain prescribed payments (E.g. Interest, commission, brokerage, rent, etc.), the person making the payment is required to deduct tax at source (TDS) if payment exceeds certain threshold limits. TDS has to be deducted at the rates prescribed by the tax department. </p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Who are a deductor and a deductee?</div>
							<div class="collapsible-body"><span><p>The company or person that makes the payment after deducting TDS is called a deductor and the company or person receiving the payment is called the deductee.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the full form of TAN?</div>
							<div class="collapsible-body"><span><p>The full form of TAN is Tax Deduction and Collection Account Number. </p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Who must have a TAN?</div>
							<div class="collapsible-body"><span><p>Under the Income Tax Act, 1961, TAN is granted to all those peoples and companies who are allowed to pay or deduct tax on cash payments by them by the Indian Income Tax Department. It is compulsory to include the TAN in all TDS returns & Challans when paying tax under Section 203A of the Income Tax Department.</br>
							•	A taxpayer can’t pay the tax without having a TAN.</br>
							•	The IT division confirms the amount upon receipt of the tax amount and, by accident, if the individual has paid an additional tax, then the amount will be returned directly into his account.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the Format of the TAN number?</div>
							<div class="collapsible-body"><span><p>•	TAN is an alphanumeric number of 10 digits.</br>
							•	The first three TAN letters reflect a city, the fourth letter is the taxpayer’s initials, and the next five digits and the last character makes the TAN different.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How and where to apply for TAN?</div>
							<div class="collapsible-body"><span><p>•	The taxpayer must fill out Form No. 49B to apply for a TAN.</br>
							•	Individual must then add all of the documents required, including proof of identity and address verification, and send it to the NSDL or Tax Information Network Facilitation Center website.</br>
							•	The NSDL division confirms the information upon obtaining the TAN application form and transfers the application form to the IT division.
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is a Challan Identification Number or CIN?</div>
							<div class="collapsible-body"><span><p>Every Income Tax Challan is identified by CIN which contains the Bank BSR code, date of deposit and challan serial no
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">When is tax deducted?</div>
							<div class="collapsible-body"><span><p>Tax is deducted at the time of making the prescribed payment or credit of the income/payment to the deductee, whichever is earlier. In the case of TDS on salary, the tax is deducted at the time of actual payment. In the case of TDS on rent, tax is deducted at the time of credit of rent for the last month of the year.
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What are the consequences of TDS default and non-payment to the government?</div>
							<div class="collapsible-body"><span><p>Failure to deduct tax, deducting less tax, delay in payment of the deducted tax to the Government, makes the deductor an assessee liable to a penalty, which is equal to the amount for which the assessee is a deemed defaulter.</br>
							•	In case of non-deduction or less deduction or delay in deduction of tax, an interest of 1% is levied on the tax per month or part of the month. In case of delay in payment of tax after deducting, the interest of 1.5% is levied per month or part of the month. The interest is applicable till the time the tax deducted is not credited to the Government. </br>
							•	In case of delay in payment of tax after collecting it, interest at 1% per month or part of the month shall be levied till the time the tax is not paid.</br>
							•	Failure on part of the deductor to pay the tax deducted at source, to the credit of Central Government makes him liable to rigorous imprisonment of a minimum period of three months but which may extend to seven years. </br>
							•	Delay in filing TDS quarterly statements attracts:</br>
							1. A late fee of Rs. 200/- per day for each day of default, subject to certain limits (u/s 234E)</br>
							2. Minimum penalty of Rs. 10, 000 (may be extended to Rs. 1 lakh) (u/s 271H)
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the duty of the deductor if the deductee does not furnish his PAN?</div>
							<div class="collapsible-body"><span><p>If the deductee does not furnish PAN or furnishes incorrect PAN to the deductor, the deductor shall deduct tax at source that is higher than the following rates: </br>
							(a) The rate prescribed in the Aes </br>
							(b) At the rate in force, i.e., the rate mentioned in the Finance Act </br>
							(c) At the rate of 20%</br>
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">When is the TDS deposited to the credit of the government?</div>
							<div class="collapsible-body"><span><p><img width="550" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/230.png" alt=""></img></br>
							</p></span></div>
						</li>	
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the due date of submission of the TDS statement?</div>
							<div class="collapsible-body"><span><p><img width="500" height="160" src="<?php echo base_url(); ?>public/images/faqs-images/231.png" alt=""></img></br>
							</p></span></div>
						</li>	
						
						
						
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record GST payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts and Payments’. Further, click on ‘Payments’ and select ‘Tax Payments’ and then click on ‘GST’.</br>
							2.	Click on ‘Add New GST Payment.’ </br>
							3.	Fill in your IGST, CGST, SCGT, CESS, Receipt Details, Payment Details and click ‘Save’.</br>
							<img width="600" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/191.png" alt=""></img></p></span></div>
						</li>
						
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the difference between Tax Summary and Tax payment in the ‘My Receipts and Payment’ module?</div>
							<div class="collapsible-body"><span><p>The Tax Summary will show all your Tax entries that you have made. In comparison, Tax Payments modules are where you can record your tax-related payment entries.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Do I have to calculate the GST amount manually?</div>
							<div class="collapsible-body"><span><p>No. While filling your Company Expense sheet, fill in your GSTIN details, Rate and your GST amount will be calculated automatically.</br>
							<img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/192.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I get to view GST month-wise and client-wise?</div>
							<div class="collapsible-body"><span><p>To view GST Tax Payment:</br>
							1.	On your Xebra dashboard, click on ‘My Receipts and Payments’. Further, click on ‘Payments’ and select ‘Tax Payments’ and then click on ‘GST’.</br>
							2.	On your GST page, click on Select ‘FY dropdown’ and Select ‘Month’ to see your GST Payments month-wise.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/193.png" alt=""></img>
							</br>
							To view GST Tax Summary:</br>
							1.	On your Xebra dashboard, click on ‘Tax Summary’ and select ‘GST’.</br>
							2.	On your GST page, select client, start and end date to view your GST summary month-wise and client-wise.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/194.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is GSTR1 and GSTR3B?</div>
							<div class="collapsible-body"><span><p>GSTR-1 is a sales return that is required to be filed by every GST registered person. Taxpayers are to enter details relevant to their sales & outward supplies in the GSTR 1 sales return. </br>
							GSTR3B is a monthly self-declaration to be filed by a registered GST dealer along with GSTR 1 and GSTR 2 return forms. It is a simplified return to declare summary GST liabilities for a tax period.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is the use of the GST computation table?</div>
							<div class="collapsible-body"><span><p>The GST computation table is where you will be able to see all your entries with the bifurcation of their tax amounts.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/195.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I also file GST returns from Xebra?</div>
							<div class="collapsible-body"><span><p>No. As of now, Xebra auto-calculates your GST outgo, which you can export to a spreadsheet. You will then have to upload that file onto an authorized GST filing provider.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I retrieve the uploaded challan copy of TDS?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
							2.	On your Document Locker page, you will be able to see all your uploaded documents from each module.</br>
							3.	Hover over the three dots against the entry you want to download.</br>
							4.	Your document should get downloaded onto your device.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">I am unable to upload the TDS challan anymore. What do I do?</div>
							<div class="collapsible-body"><span><p>1.	You can write to support@xebra.in and mention your registered email id and business name along with your query.</br>
							2.	You will hear from us shortly.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Why am I not able to edit or delete GST entries from Tax Summary?</div>
							<div class="collapsible-body"><span><p>1.	You won’t get an option to delete your GST entries from Tax Summary as it is only a listing of all your tax entries.</br>
							2.	If you wish to delete your entry, do it from the GST section in Tax Payment in the My Receipts & Payments module.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, email, export, print and delete a GST payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’ and select ‘GST’. </br>
							2.	Find the one you would like to edit, email, export, email, or delete in your GST list.</br>
							3.	Hover over the three dots you will get edit, export, email or delete options. </br>
							4.	If you click delete, a confirmation pop up will appear. Click on ‘Delete’ to permanently delete that specific entry.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/196.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I email, export, print and delete GST payment entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’ and select ‘GST’.</br>
							2.	You will be able to view your GST list.</br>
							3.	Select your entries by ticking the boxes at the start of each row. Hover on ‘Bulk Actions’ and select edit, export, email, or delete from the dropdown.</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear. Click ‘Delete’ if you want to permanently delete all those selected entries.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/197.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an expense TDS payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts and Payments’. Further, click on ‘Payments’ and select Tax Payments. Then click on TDS and choose Expenses.</br>
							2.	Click on ‘Add New TDS Payment.’</br>
							3.	Fill in your TDS details, Challan details, Payment details, and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/198.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an employee TDS payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, select ‘Tax Payments’, click on ‘TDS’ and select ‘Employee’.</br>
							2.	On your TDS Employment Payment page, click on ‘Add New TDS Employee Payment.’ </br>
							3.	Fill in your TDS details, Challan details, Payment details, and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/199.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I know if my client has paid the TDS deducted from my invoice?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on ‘Tax Summary’. Further, click on the TDS dropdown and select ‘For Clients.’</br>
							2.	You will see the TDS Status on the RHS of the TDS module. After checking the 26 AS form, you can toggle the button to the right indicating that the client has paid the TDS deducted from the invoice to the government.</br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/200.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I compare the TDS deducted by the client to what he has paid on the government portal?</div>
							<div class="collapsible-body"><span><p>When you check the 26 AS form, you can see the TDS amount paid by your clients. Compare that amount with your TDS entry or entries for that time period</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit a specific TDS Head and TDS percentage assigned to an expense?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on ‘My Expenses’ and select ‘Expense Master’.</br>
							2.	Choose the entry you would like to edit, hover over the three dots and choose ‘Edit’.</br>
							3.	On the edit Expense Master page, under Tax info, you will get a dropdown to Select ‘TDS Head’ as applicable, enter the TDS percentage and click ‘Save’.</br>
							<img width="400" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/201.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Do I have to calculate the TDS amount manually?</div>
							<div class="collapsible-body"><span><p>No. While filling your Company Expense sheet, tick the TDS box fill in your Tax Applicable, TDS Head, TDS percent and it will calculate the TDS amount automatically.</br>
							<img width="1000" height="100" src="<?php echo base_url(); ?>public/images/faqs-images/202.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I get to view TDS month-wise and vendor-wise, or employee-wise?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on ‘Tax Summary’ and select ‘TDS dropdown’. Further, choose ‘Client or Vendor or Employee’.</br>
							2.	On your TDS section, you set the start and end date accordingly to view month wise.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I also file TDS returns from Xebra?</div>
							<div class="collapsible-body"><span><p>No. As of now, Xebra auto-calculates your TDS outgo, which you can export to a spreadsheet. You will then have to upload that file onto an authorized TDS filing provider.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I retrieve the uploaded challan copy of TDS?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
							2.	On your Document Locker page, you will be able to see all your uploaded documents from each module.</br>
							3.	Hover over the three dots against the entry you want to download.</br>
							4.	Your document should get downloaded onto your device.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">I am unable to upload the TDS challan anymore. What do I do?</div>
							<div class="collapsible-body"><span><p>1.	You can write to support@xebra.in and mention your registered email id and business name along with your query.</br>
							2.	You will hear from us shortly.	
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Why am I not able to edit or delete TDS entries from Tax Summary?</div>
							<div class="collapsible-body"><span><p>1.	You won’t get an option to delete your TDS entries from Tax Summary as it is a summary of all your entries.</br>
							2.	If you wish to delete your entry, do it from the TDS module under ‘Tax Payment’ in ‘My Receipts & Payments’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, email, export, print, and delete an Expense or Employee TDS payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’, then click on ‘TDS’ and select ‘Expense or Employee’.</br>
							2.	Find the one you would like to edit, email, export, email, or delete in your TDS list.</br>
							3.	Hover over the three dots you will get edit, export, email or delete options. </br>
							4.	If you click delete, a confirmation pop up will appear, your selected TDS entry will then be deleted.</br>
							<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/203.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I email, export, print and delete Expense or Employee TDS payment entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’, select ‘TDS’ and then select ‘Expense or Employee’.</br>
							2.	You will be able to view your Expense or Employee TDS entries list.</br>
							3.	Select your entries by ticking the boxes at the start of each row. Hover on ‘Bulk Actions’ and select edit, export, email, or delete from the dropdown.</br>
							4.	If you click delete, a confirmation pop-up will appear. Click ‘Delete’ if you want to permanently delete all those selected entries.</br>
							<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/204.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an Equalisation Levy payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’ and select ‘Equalization Levy’.</br>
							2.	On your Equalisation Levy page, click on ‘Add New Equalization Levy Payment.’</br> 
							3.	Fill in your details, Receipt details, Payment details and click ‘Save’.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/205.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I retrieve the uploaded challan copy of Equalisation Levy?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
							2.	On your Document Locker page, you will be able to see all your uploaded documents from each module. You can use a filter to identify a specific document. </br>
							3.	Hover over the three dots against the entry you want to download.</br>
							4.	Your document should get downloaded onto your device.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Why is there an Equalisation Levy entry from the client and vendor side?</div>
							<div class="collapsible-body"><span><p>Equalization levy is a direct tax at 6% on payment made to a non-resident by an Indian resident (Business Transactions) with an annual payment exceeding of Rs. 1,00,000 in a financial year in respect of online advertisements and any provision for digital advertising space or facilities/ service for the purpose of online advertisement.</br>
							This tax is paid on the behalf of the client and therefore will be reimbursed from the client, similarly you will need to pay equalization levy on a purchase made through a Vendor.

							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I also file Equalisation Levy returns from Xebra?</div>
							<div class="collapsible-body"><span><p>You can download the Equalisation Levy Tax summary from My Tax Summary and be able to file for Equalisation Levy Tax Returns.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Why am I not able to edit or delete Equalisation Levy entries from Tax Summary?</div>
							<div class="collapsible-body"><span><p>1.	You will not get an option to delete your Equalisation Levy entries from Tax Summary as it is a summary of all your entries.</br>
							2.	If you wish to delete your entry, do it from Equalisation Levy under ‘Tax Payment’ in ‘My Receipts & Payments’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, email, export, print, and delete an Equalisation Levy payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’, and select ‘Equalization Levy’.</br>
							2.	In your Equalisation Levy list, find the one you would like to edit, email, export, email, or delete.</br>
							3.	Hover on the three dots, and you will get edit, export, email or delete options. </br>
							4.	If you click delete, a confirmation pop up will appear, your desired Equalisation Levy entry will then be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/206.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, email, export, print, and delete an Equalisation Levy payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’. Further, click on ‘Tax Payments’, and select ‘Equalization Levy’.</br>
							2.	In your Equalisation Levy list, find the one you would like to edit, email, export, email, or delete.</br>
							3.	Hover on the three dots, and you will get edit, export, email or delete options. </br>
							4.	If you click delete, a confirmation pop up will appear, your desired Equalisation Levy entry will then be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/207.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What are the four Employee related expenses that are covered here?</div>
							<div class="collapsible-body"><span><p>The Employee related expense covered here are</br>
							•	Professional Tax</br>
							•	Employee Provident Fund</br>
							•	Emp State Insurance Corporation</br>
							•	Expense Voucher Reimbursement</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a professional tax entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’, click on ‘Tax Payments’ and select ‘Emp Tax & Reimb.’</br>
							2.	On your ‘Emp Tax & Reimb’ page, click on ‘Add New Emp Tax & Reimb.’</br>
							3.	Fill in your details, choose Professional Tax from the Type of Expense dropdown, Challan details, Payment details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/208.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an Employee Provident Fund (EPF) entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on My Receipts & Payments. Further, click on Tax Payments and select ‘Emp Tax & Reimb.’</br>
							2.	On your ‘Emp Tax & Reimb’ page, click on ‘Add New Emp Tax & Reimb’ </br>
							3.	Fill in your details, choose Employee Provident Fund from the Type of Expense dropdown, Challan details, Payment details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/208.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an Employee State Insurance Corporation (ESIC) entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on My Receipts & Payments. Further, click on Tax Payments and select ‘Emp Tax & Reimb.’</br>
							2.	On your ‘Emp Tax & Reimb’ page, click on ‘Add New Emp Tax & Reimb.’</br>
							3.	Fill in your details, choose Employee State Insurance Corporation from the Type of Expense dropdown, Challan details, Payment details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/208.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record an expense voucher payment entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’, click on ‘Tax Payments’ and select ‘Emp Tax & Reimb’.</br>
							2.	On your Emp Tax & Reimb page, click on ‘Add New Emp Tax & Reimb’ on the RHS of the page.</br>
							3.	Fill in your details, choose Expense Voucher Reimbursement from the Type of Expense dropdown, Challan details, Payment details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/208.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I file professional tax levy returns from Xebra?</div>
							<div class="collapsible-body"><span><p>You can download the Professional Tax Levy summary from My Tax Summary and be able to file for Professional Tax Levy Returns
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">I am unable to upload challans anymore. What do I do?</div>
							<div class="collapsible-body"><span><p>1.	You can write to support@xebra.in and mention your registered email id and business name along with your query.</br>
							2.	You will hear from us shortly.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Why am I not able to edit or delete Equalisation Levy entries from Tax Summary?</div>
							<div class="collapsible-body"><span><p>1.	You will not get an option to delete your Equalisation Levy entries from Tax Summary as it is a summary of all your entries</br>
							2.	If you wish to delete your entry you, do it from Equalisation Levy under ‘Tax Payment’ in ‘My Receipts & Payments’.</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, email, export, print and delete a professional tax, Employee Provident Fund?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’, click on ‘Tax Payments’, and select ‘Emp Tax & Reimb.’</br>
							2.	In your ‘Emp Tax & Reimb’ list, find the one you would like to edit, email, export, email, or delete.</br>
							3.	Click on ‘Type of Employee Expense’ dropdown and select ‘Employee Provident Fund’.</br>
							4.	Hover over the three dots you will get edit, export, email or delete options. </br>
							5.	If you click delete, a confirmation pop up will appear, your desired TDS entry will then be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/209.png" alt=""></img></p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I email, export, print and delete professional tax or Employee Provident Fund entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’, click on ‘Tax Payments’ and select ‘Emp Tax & Reimb.’</br>
							2.	You will be able to view your ‘Emp Tax & Reimb’ list on the RHS of your screen.</br>
							3.	Click on ‘Type of Employee Expense’ dropdown and select Employee Provident Fund</br>
							4.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to edit, export, email, or delete.</br>
							5.	If you click delete, a confirmation pop-up will appear, and your selected entry will be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/210.png" alt=""></img></p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I email, export, print and delete professional tax or Employee Provident Fund entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Receipts & Payments’, click on ‘Tax Payments’ and select ‘Emp Tax & Reimb.’</br>
							2.	You will be able to view your ‘Emp Tax & Reimb’ list on the RHS of your screen.</br>
							3.	Click on ‘Type of Employee Expense’ dropdown and select ‘Professional Tax or Employee Provident Fund’.</br>
							4.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to edit, export, email, or delete.</br>
							5.	If you click delete, a confirmation pop-up will appear, and your selected entry will be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/210.png" alt=""></img></p></span></div>
						</li>
					
						
					   
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I cross-check my TDS amount that the client has deducted?</div>
                        <div class="collapsible-body"><span><p>Xebra will give you the invoice wise TDS amount that your client has deducted. You can log in to form 26AS and cross-check whether your client has paid that amount to the government. Once you have ascertained that, you can toggle the status to Paid in your list view. </p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I reconcile the TDS that I have deducted from my vendor’s payment?</div>
                        <div class="collapsible-body"><span><p>Simply click on TDS Vendors in Tax Summary module to know the exact status of how much TDS you have paid and for which vendor. This saves your time immensely</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How does GST Tax Summary Work?</div>
                        <div class="collapsible-body"><span><p>GST Tax Summary List view gives you your GST liability for the particular time period basis the invoices you have entered. Ensure that you have prepared all your invoices from Xebra itself for an accurate GST amount calculation. You can select a specific time period and then Bulk Export it to excel. This will save your time drastically which otherwise would have been spent in filling an excel sheet to be filled as part of GSTIN returns </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I track the Professional tax, EPF or ESIC of each employee?</div>
                        <div class="collapsible-body"><span><p>You will get all the details of Prof Tax, EPF and ESIC, employee wise in Emp Tax & Reimb module in Tax summary. This will give you a comprehensive view of how much has been paid and in remaining to be paid</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add multiple GSTIN of my company in Xebra?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add as many GSTIN you have for your company in the module My Company Profile</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to prepare individual ledger account after making my expense, sales or any other entries?</div>
                        <div class="collapsible-body"><span><p>No, you just have to record your entry. Ledgers will be created automatically basis those.</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to use another software to make my ledger & final account?</div>
                        <div class="collapsible-body"><span><p>No, Xebra will automatically create all ledgers & financial statements for your business.</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Are my numbers in the ledgers, P&L and Balance sheet updated real-time?</div>
                        <div class="collapsible-body"><span><p>Yes. Xebra automatically updates all these accounting reports in real-time with every entry you pass in the application.</p></span></div>
                      </li>
                    </ul>
                  </div>
                  <div id="test5" class="col s12 inner_row_container test5">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How can I select my subscription plan?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
								2.	Click on ‘Add New Company Profile’ on the RHS of the screen.</br>
								3.	Choose your plan based on the requirements of your company.</br>
								4.	After payments, enjoy the benefits of your plan.</br>
							<img width="900" height="600" src="<?php echo base_url(); ?>public/images/faqs-images/93.png" alt=""></img></br>
							Alternatively, </br>
							1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and click on ‘Subscription and Shop’.</br>
							2.	You will see a pop up with the option to buy a new subscription or to renew your subscription</br>
							3.	Click on the appropriate option and choose your plan based on the requirements of your company.</br>
							4.	After payments, enjoy the benefits of your plan.</br>
							<img width="200" height="800" src="<?php echo base_url(); ?>public/images/faqs-images/94.png" alt=""></img>
							<img width="450" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/95.png" alt=""></img>
							</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
						<div class="collapsible-header">How can I check my deposit and withdrawals?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and then select ‘My Wallet’.</br>
						2.	On your My Wallet page, you will be able to see your deposits and withdrawals.</br>
						<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/101.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I check my billing history?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘Billing History’.</br>
						2.	On your Billing History page, you will be able to see your payments.</br>
						<img width="200" height="800" src="<?php echo base_url(); ?>public/images/faqs-images/94.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I buy more space?	</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and click on ‘Subscription and Shop’.</br>
						2.	You will see a pop up with the option to buy Add-Ons.</br>
						3.	On the Add-On page, you will get an option to purchase more Document Space. Select how much space you would like to purchase.</br>
						<img width="650" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/108.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Will I be able to access my documents even after my subscription has lapsed?</div>
                        <div class="collapsible-body"><span><p>Yes, you will be able to access your documents even after your subscription has lapsed.
						</p></span></div>
                      </li>
					  <!-- My Account (Subscribe and shop) -->
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I buy a new subscription?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and click on ‘Subscription and Shop’</br>.
						2.	You will see a pop up with the option to buy a new subscription.</br>
						3.	Click on the appropriate option and choose your plan based on the requirements of your company.</br>
						4.	After payments, enjoy the benefits of your plan.</br>
						<img width="550" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/109.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I renew my subscription?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and click on ‘Subscription and Shop’.</br>
						2.	You will see a pop up with the option to renew your subscription.</br>
						3.	Click on the appropriate option and choose your plan based on the requirements of your company.</br>
						4.	After payments, enjoy the benefits of your plan.</br>
						<img width="550" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/95.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I buy add-ons with my subscription?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and click on ‘Subscription and Shop’.</br>
						2.	You will see a pop up with the option to ‘Buy Add-Ons’.</br>
						3.	Select the relevant add on you would like to add based on your requirements.</br>
						<img width="550" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/110.png" alt=""></img>
						</p></span></div>
                      </li>
					  <!-- My Account (Billing History) -->
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I check my billing history?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘Billing History’.</br>
						2.	On your Billing History page, you will be able to see your payments.</br>
						<img width="200" height="700" src="<?php echo base_url(); ?>public/images/faqs-images/94.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I email, download, or print my billing history?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘Billing History’.</br>
						2.	On your Billing History page, select the entry you would like to take an action.</br>
						3.	Hover over the three dots, and you will be able to email, download or print your billing details.</br>
						<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/111.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I check my wallet?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘My Wallet’.</br>
						2.	You will be able to see your transactions on the My Wallet page.</br>
						<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/112.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I add money to my wallet?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘My Wallet’.</br>
						2.	Select ‘Add Money’ of the RHS of the page.</br>
						3.	On the Add Money page, enter the amount and select the mode of transfer. Click 'Add Money' to your wallet.</br>
						<img width="500" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/113.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I export my wallet details?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘My Wallet’.</br>
						2.	On the My Wallet page, select the entry you would like to export.</br>
						3.	Hover over the ‘Bulk Action’ button, and you can export one or multiple entries.</br>
						<img width="800" height="280" src="<?php echo base_url(); ?>public/images/faqs-images/114.png" alt=""></img>
						</p></span></div>
                      </li>
					  <!-- My Activity History -->
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I email, print, download my activity history?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘My Activity History’.</br>
						2.	On the My Activity History page, select the entry you would like to take action.</br>
						3.	Hover over the three dots, and you can email, print, download the activity.</br>
						<img width="800" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/115.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I email, print, or download my activity history in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘My Account’ and select ‘My Activity History’.</br>
						2.	On the My Activity History page, select the entry/entries you would like to take action on by ticking the checkboxes.</br>
						3.	Hover over the bulk action dropdown, and you can email, print or download the selected entries.</br>
						<img width="800" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/116.png" alt=""></img>
						</p></span></div>
                      </li>
					  <!-- Change Password -->					  
						<li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I change my password?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the Profile dropdown. Click on ‘My Account’ and select ‘Change Password’.</br>
						2.	A pop-up box will appear, enter your current and new password and click 'Change Password' to save your details.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/117.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How often do I have to renew my subscription?</div>
                        <div class="collapsible-body"><span><p>You have to renew your subscription annually</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will, it auto-renew my subscription for next year?</div>
                        <div class="collapsible-body"><span><p>No. We have provided you with an option of doing it manually from the subscription section in Profile Dropdown</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">After the end of my subscription will I able to get my documents from the cloud?</div>
                        <div class="collapsible-body"><span><p>Yes, you will be able to download PDFs & export to excel all your business financial documents.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How much cloud space do you allocate per user?</div>
                        <div class="collapsible-body"><span><p>It varies with the plan that you have subscribed for. For latest updates, check the pricing page on the website or Buy Add-ons in Renew subscription in the Profile dropdown</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Is there a limit on how many SMS we have with signup?</div>
                        <div class="collapsible-body"><span><p>Yes. It varies with the plan that you have subscribed for. For latest updates, check the pricing page on the website or Buy Add-ons in Renew subscription in the Profile dropdown.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I buy additional space or SMS?</div>
                        <div class="collapsible-body"><span><p>Simply click on the subscription &amp; Billing section and select the quantity of space or SMS you require. You can make the payment at check-out.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the refund policy?</div>
                        <div class="collapsible-body"><span><p>Xebra will not refund any money should you choose to cancel your plan or stop using the product in the middle of your subscription period
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I change my plan later?</div>
                        <div class="collapsible-body"><span><p>Yes, you can upgrade your plan at any point in time. Once you upgrade, we will deduct the pro-rata cost of your current pending plan from the total billing
						If you downgrade your plan, then that will be applicable only once the existing plan period is completed</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
							<div class="collapsible-header">How can I integrate my ICICI bank account directly with my Xebra portal?</div>
							<div class="collapsible-body">
							<!-- First Method -->
							<span><p>There are two ways to integrate your ICICI bank account with your Xebra portal:</p>
							<p><b>First Method</b>-</br>
								•	Go to your Xebra dashboard and click on the profile icon on the RHS top corner of the page. </br>
								•	Select ‘My Company Profile’ from the dropdown options.
							</p>
							<img width="250" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/90.png" alt=""></img>
							<p>
								•	In the company profile list, hover on the three dots on the RHS of the company name list.</br>
								•	A dropdown will appear where you have to click on ‘edit’.</br>
								•	Click on the ‘+’ sign next to the bank details section on the edit info page. </br>
								•	Fill in your registered ICICI bank details and make sure you re-check all the numbers entered by you.</br>
							</p>
							<img width="350" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/232.png" alt=""></img>
							<p>•	Click on the small green checkbox at the bottom and then click on ‘Save’. </br>
							•	You will receive a notification (shown below) confirming the integration.</p>
							<img width="280" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/233.png" alt=""></img>
							</span>
							<p>
								<b>Second Method</b>-</br>
								•	On your Xebra dashboard, click on your profile icon. Select ‘My Account’ and click on ‘My Wallet’ from the dropdown options.</br>
								<img width="200" height="650" src="<?php echo base_url(); ?>public/images/faqs-images/234.png" alt=""></img></br>
								•	Click on ‘Add Money’ on the RHS corner of the screen</br>
								•	On the add money page, fill in the amount you want to add and select ICICI Bank in the mode of transfer (as shown below) and click on the ‘Add Money’ button.
								<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/235.png" alt=""></img></br>
								•	A popup will appear where you will need to fill in your registered ICICI bank details </br>
								<img width="350" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/232.png" alt=""></img></br>
								•	Click on the small green checkbox at the bottom and then click on ‘Save’. </br>
								•	You will receive a notification (Shown below) confirming the integration.</br>
								<img width="280" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/233.png" alt=""></img>
							</p>
							</div>
                       </li>
					  <li class="row document-box-view redborder">
							<div class="collapsible-header">How can I add money directly from my ICICI bank account to my Xebra wallet?</div>
							<div class="collapsible-body">
								<p>
									•	On your Xebra dashboard, click on your profile icon. Select ‘My Account’ and click on ‘My Wallet’ from the dropdown options.</br>
									<img width="200" height="650" src="<?php echo base_url(); ?>public/images/faqs-images/234.png" alt=""></img></br>
									•	Click on ‘Add Money’ on the RHS corner of the screen</br>
									•	On the add money page, fill in the amount you want to add and select ICICI Bank in the mode of transfer (as shown below) and click on the ‘Add Money’ button.</br>
									<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/235.png" alt=""></img></br>
									•	An OTP confirmation popup will appear (<a class="bColor ohhh">click here</a> to learn how to integrate your ICICI bank account with your Xebra profile) </br>
									<img width="950" height="465" src="<?php echo base_url(); ?>public/images/faqs-images/236.png" alt=""></img></br>
									•	On entering the correct OTP, you will receive a confirmation notification</br> 
									<img width="400" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/237.png" alt=""></img>
								</p>
							</div>
						</li>	
                    </ul>
                  </div>
                  <div id="test6" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I have to enter in dual OTP every time I log in?</div>
                        <div class="collapsible-body"><span><p>NO, it will ask you for it when you are signing up for the first time and every time you log
in after you have changed your password</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Why did the system simply log me out on its own?</div>
                        <div class="collapsible-body"><span><p>If your Xebra browser tab remains unattended for more than 60 minutes, then
system would auto log out for your safety</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">On which cloud server have you hosted the application?</div>
                        <div class="collapsible-body"><span><p>We have partnered with Google Cloud and have hosted it there </p></span></div>
                      </li>

                    </ul>
                  </div>
				  <div id="test7" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <!--li class="row document-box-view redborder">
                         <div class="collapsible-header">Which assets should I add in Asset Tracker?</div>
                        <div class="collapsible-body"><span><p>We would encourage you to record all your company assets so that they are properly tracked, and you know their current status at any point in time</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Why can’t I add the asset directly in Asset Purchase module?</div>
                        <div class="collapsible-body"><span><p>You have to first add the asset in the Vendor & Asset Master module and basis that it will appear in the Asset Name dropdown in Asset Purchase.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does Xebra automatically calculate depreciation of my assets?</div>
                        <div class="collapsible-body"><span><p>Yes, Xebra is automated accounting software and is capable of calculating the depreciation automatically.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to calculate depreciation for every year?</div>
                        <div class="collapsible-body"><span><p>No, you can just schedule the depreciation by setting start date, useful life & rate of depreciation in asset purchase module.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Which method is used to calculate depreciation?</div>
                        <div class="collapsible-body"><span><p>You can choose SLM or WDV method of depreciation from asset purchase while setting the date & rate of depreciation.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add any pre-owned asset, which is currently used for business purpose?</div>
                        <div class="collapsible-body"><span><p>Yes, you can easily add a pre-owned asset in asset purchase by checking the check box of a pre-owned asset, but before that, you have to add asset & vendor in asset & vendor master.</p></span></div>
                      </li-->
					  <!--Vendor and Asset Master-->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new asset?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on My Assets & Depreciation and then select ‘Vendor & Asset Master’.</br>
						<img width="280" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/35.png" alt=""></img></br>
						2.	Click on the ‘Add New Asset button’ on the RHS of the screen.</br> 
						3.	Fill in your No, Asset Date of purchase, Asset Name, Asset Description, Nature of the Asset (Tangible or Intangible), type of Asset and other relevant details, then click on ‘Save.’</br>
						4.	You will be able to view your New Asset Details on the Vendor and Asset Master page.</br>
						<strong>Alternatively,</strong> </br>
						1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and then select ‘Asset Purchase’.</br>
						2.	Click on the ‘Add New Purchase Asset’ button on the RHS of the screen. </br>
						3.	You will be redirected to the New Purchase Asset Page. Fill in the Date, Asset No. & Name, Invoice number, Vendor Name, GSTIN. Choose the place of supply and upload your purchase bill from your device (to know more, click here)</br>
						4.	After filling out all the relevant details, you will see your amount on the Asset Purchase Table.</br>
						5.	You will be able to view your New Purchase Asset Details on the Asset Purchase page.</br>
						<img width="800" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/36.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I upload my asset documents?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Vendor and Asset Master’.</br>
						2.	While creating a new Vendor & Asset Master or editing your Vendor & Asset Master, under asset documents, you will find an option to upload your documents</br>
						3.	Press the cloud upload icon, and you will be able to select and upload your documents from your device.</br>
						<img width="500" height="130" src="<?php echo base_url(); ?>public/images/faqs-images/37.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a new tax apart from GST and Cess?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Vendor and Asset Master’. </br>
						2.	While creating a New Vendor & Asset Master or editing your Vendor & Asset Master, you can add your tax under Statutory Info. You can add other taxes apart from GST and Cess.</br>
						3.	Press the cloud upload icon, and you will be able to select and upload your documents from your device.</br>
						<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/38.png" alt=""></img></br>
						3.	You will find an option ‘Other Taxes.’ On clicking the plus icon, you will add the Tax name and percentage of the tax.</br>
						4.	In this option, you can add/subtract multiple taxes, which is suitable for your product.</br>
						<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/39.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add an asset type that is not mentioned in the dropdown?</div>
                        <div class="collapsible-body"><span><p>1.	You can write to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> and mention your registered email id and business name along with the Asset you want us to include.</br>
						2.	You will hear from us shortly.
						</p></span></div>
                      </li>	
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email or delete my entry?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select Vendor & Asset Master. </br>
						2.	In your Vendor & Asset Master list, find the one you would like to edit, export, email or delete.</br>
						3.	Hover over the 3 dots you will get edit, export, email or delete options. </br>
						<img width="850" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/40.png" alt=""></img></br>
						4.	If you click ‘Delete’, a confirmation pop-up will appear, and your selected Asset will be deleted.</br>
						</p></span></div>
                      </li>	
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email or delete my entries in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Vendor & Asset Master’. </br>
						2.	You will be able to view your list of Assets on the RHS of your screen</br>
						3.	Check the boxes of the entries that you want to act on. Hover on bulk action and select the relevant action from the dropdown</br>
						4.	If you click delete, a confirmation pop-up will appear, and your desired Asset will be deleted.</br>
						<img width="700" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/41.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I register a domestic and international vendor?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Vendor & Asset Master’. </br>
						2.	While creating or editing a new Vendor & Asset Master record, you can select the country of that vendor. </br>
						3.	Select the appropriate currency of that country so that all the payments made to that vendor are in that currency.</br>
						<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/42.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I retrieve my documents that I have uploaded?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
						2.	On your Document Locker page, you will be able to see all your uploaded documents from each module.</br>
						3.	Hover over the 3 dots against the entry you want to download.</br>
						4.	Your document should get downloaded onto your device.</br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I upload multiple bank accounts of the vendor?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Vendor & Asset Master’. </br>
						2.	On create or edit Vendor & Asset Master, you will notice the Bank Details section. Click on the ‘+’ icon and enter your bank details in the pop-up box.</br>
						<img width="500" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/43.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <!-- Asset Purchase -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a new asset purchase?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and then select ‘Asset Purchase’.</br>
						<img width="250" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/44.png" alt=""></img></br>
						<img width="800" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/45.png" alt=""></img></br>
						2.Click on the ‘Add New Purchase Asset’ button on the RHS of the screen. </br>
						3.You will be redirected to the New Purchase Asset Page. Fill in the Date, Asset No. & Name, Invoice number, Vendor Name, GSTIN. Choose the place of supply and upload your purchase bill from your device (to know more, click here)</br>
						4.After filling out all the relevant details, you will see your amount on the Asset Purchase Table.</br>
						5.You will be able to view your New Purchase Asset Details on the Asset Purchase page.</br>
						<img width="800" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/36.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I upload my purchase bill?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Purchase’. </br>
						2.	In the Add or edit New Asset Purchase, you will see an option to upload your purchase bill. </br>
						3.	Click on the cloud icon and upload the document in the pop-up.</br>
						<img width="400" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/46.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add an asset type that is not mentioned in the dropdown?</div>
                        <div class="collapsible-body"><span><p>1.	You can write to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> and mention your registered email id and business name along with the asset type you want us to include.</br>
						2.	You will hear from us shortly.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Which depreciation method applies to my purchase?</div>
                        <div class="collapsible-body"><span><p>1.	If you follow the Written Down Value method of depreciation, simply select the Company Act as the dropdown option when recording the purchase of that Asset. Fill in the other depreciation details for that Asset. Xebra auto-calculates your entire depreciation schedule for that Asset and can be viewed in the Depreciation Schedule Module</br>
						2.	If you are using the Straight Line method, you will have to select the Income Tax Act as the dropdown option. You will have to calculate the depreciation manually for each Asset and record your transactions manually from the ‘My Accounts’ module
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I retrieve my Asset Purchase documents I have uploaded?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
						2.	On your Document Locker page, you will be able to see all your uploaded documents from each module.</br>
						3.	Hover over the 3 dots against the entry you want to download.</br>
						4.	Your document should get downloaded onto your device.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email, print, or delete my entry?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Purchase Asset’. </br>
						2.	In your Purchase Asset list, find the one you would like to edit, export, email or delete.</br>
						3.	Hover over the 3 dots you will get edit, export, email or delete options. </br>
						<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/47.png" alt=""></img></br>
						4.	If you click delete, a confirmation pop up will appear, your desired Purchase Asset will then be deleted
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email or delete my entries in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and then select ‘Asset Purchase’. </br>
						2.	You will be able to view your list of Purchase Assets on the RHS of your screen</br>
						3.	Select the entries you would like to act on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to edit, export, email, or delete.</br>
						4.	If you click delete, a confirmation pop-up will appear, and your selected Asset will be deleted.</br>
						<img width="300" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/48.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make a payment?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and then select ‘Asset Purchase’. </br>
						2.	On the Purchase Asset page, against each entry, you will see 3 dots under the action column. </br>
						3.	When you hover your mouse over the 3 dots, you will see options such as Edit, Export, Payment, Email, Print and Delete. Once you click the payment option, you will see your payment amount and payment details.</br>
						<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/47.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record Payment towards my purchased Asset?</div>
                        <div class="collapsible-body"><span><p>There are two methods to record Payment against an asset:</br>
						<strong>Shortcut method:</strong></br>
						1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and then select ‘Asset Purchase’. </br>
						2.	On the Purchase Asset page against an individual entry, you will see 3 dots under the action column. </br>
						3.	Hover over the 3 dots you and select ‘Payment’ from the option.</br>
						4.	You will be taken to the Asset Payment section, where the details of the asset will be pre-filled. Enter in your payment options and click ‘Save’.</br>
						<strong>Longer method</strong>:</br>
						1.	On your Xebra dashboard, click on ‘My Receipts & Payments’ and select ‘Asset Payment’. </br>
						2.	Click on ‘Add New Asset Payment’ and select the asset number whose Payment you want to make.</br>
						3.	Xebra will auto-fill the details based on the Asset number. Select the other payment details and click ‘Save’.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record part payments towards my purchased Asset?</div>
                        <div class="collapsible-body"><span><p>When you are recording your Payment for a particular asset, you will see the ‘Nature of Payment’. Select ‘Part Payment’ from the dropdown options in case you are making payments in parts as in EMI.
						</p></span></div>
                      </li>
					  <!-- Asset Sales -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new asset sale?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and then select ‘Asset Sales’.</br>
						<img width="250" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/49.png" alt=""></img></br>
						<img width="900" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/50.png" alt=""></img></br>
						2.	Click on the ‘Add New Asset Sales’ button on the RHS of the screen. </br>
						3.	You will be redirected to the Create New Asset Sales. Fill out Asset Date, Asset No. & Name. You will be required to fill in the Purchaser’s Name, Pan No, Billing Address, Country, Purchaser GST No and Place of Supply. Select if you are selling your product on a Profit, Loss or Nil and the amount.</br>
						<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/51.png" alt=""></img></br>
						4.	In the Asset Sales table, you will see the total amount after deduction of the taxes.</br>
						<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/52.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How can I add a new asset sale for a domestic or international purchaser?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Sales’. </br>
						2.	Click on create or edit Asset Sale and fill out your purchaser details. </br>
						3.	Fill out the Purchaser Name, Billing Details and Country.</br>
						<img width="400" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/53.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email, print or delete my asset sales?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Sales’. </br>
						2.	In your Asset Sales list, find the one you would like to edit, export, email or delete.</br>
						3.	Hover over the 3 dots you will get edit, export, email or delete options. </br>
						4.	If you click delete, a confirmation pop up will appear, your desired Purchase Asset will then be deleted</br>
						<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/54.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email or delete my asset sale entries in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Sales’. </br>
						2.	You will be able to view your list of Asset Sales on the RHS of your screen.</br>
						3.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to edit, export, email, or delete.</br>
						4.	If you click delete, a confirmation pop-up will appear, and your desired Asset will be deleted.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/55.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I download the receipt for an asset sale?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on My ‘Assets & Depreciations’ and select ‘Asset Sales’. </br>
						2.	On this page, against individual entries, you will see 3 dots under the action column. </br>
						3.	When you hover your mouse over the 3 dots, you will see options such as Edit, Export, Receipt, Email, Print and Delete. Once your click the receipt option, you will be able to see your receipt details.</br>
						<img width="900" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/54.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do you know if your asset sale is in a profit or loss?</div>
                        <div class="collapsible-body"><span><p>Subtract your original purchase amount with your sales amount to know if you’re selling your product at a profit or loss.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/56.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record receipts against my Asset sold?</div>
                        <div class="collapsible-body"><span><p>There are two methods to record receipts against an assets sold:</br>
						<strong>Shortcut method:</strong></br>
						1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and then select ‘Asset Sales’. </br>
						2.	On the Asset Sales page against an individual entry, you will see 3 dots under the action column. </br>
						3.	Hover over the 3 dots you and select ‘Receipt’ from the option</br>
						4.	You will be taken to the Asset Sales section, where the details of the receipt will be pre-filled. Enter in your receipt options and click ‘Save’.</br>
						<strong>Longer method:</strong></br>
						1.	On your Xebra dashboard, click on ‘My Receipts & Payment’ and select ‘Asset Sales Receipt’. </br>
						2.	Click on ‘Add New Asset Sales Receipt’ and select the asset number whose Receipt you want to make.</br>
						3.	Xebra will auto-fill the details based on the Asset number. Select the other receipt details and click ‘Save’.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record part receipts against my Asset sold?</div>
                        <div class="collapsible-body"><span><p>When you are recording your Receipts for a particular asset, you will see the Nature of Receipt. Select ‘Part Receipt’ from the dropdown options in case you are making payments in parts as in EMI.
						</p></span></div>
                      </li>
					  <!-- Asset Tracker -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a new asset tracker?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and choose the option ‘Asset Tracker’.</br>
						<img width="250" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/57.png" alt=""></img></br>
						<img width="900" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/58.png" alt=""></img></br>
						2.	Click on the ‘Add New Asset Tracker’ button on the RHS of the screen. </br>
						3.	On the Create New Asset Tracker, fill in your Select Asset No, Asset Name and Asset Description. After filling out the Asset Details, AMC Details, you will receive an option to upload AMC documents (to know more, click here)</br>
						4.	You will be able to see your details on the Asset Tracker page.</br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">The Asset No dropdown does not show anything. What do I do?</div>
                        <div class="collapsible-body"><span><p>You can write to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> and mention your registered email id and business name along with your query. You will hear from us shortly.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I set a reminder for my asset warranty?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Tracker’.</br>  
						2.	On the create Asset Tracker or edit Asset Tracker page, you will be able to set a reminder under Asset Details.</br>
						3.	You can set the date of the warranty and set a date for when you want a reminder which will be sent to you at the email address you have given.</br>
						<img width="600" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/59.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">The Asset is used by another employee now. How do I record that change?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and then select ‘Asset Tracker’.</br>
						2.	On your Asset Tracker page, select the Asset from which you want to change the employee’s name.</br>
						3.	Hover over the 3 dots under the action column, select ‘Edit’. You can change the employee name under Asset Details.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What do you mean by AMC?</div>
                        <div class="collapsible-body"><span><p>An Annual Maintenance Contract (AMC) is an agreement with a service provider for the repair and maintenance of property used by your company.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I upload my AMC documents?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Tracker’. </br>
						2.	On the create Asset Tracker or edit Asset Tracker page, under AMC documents, you will find an option to upload your documents</br>
						3.	On clicking the cloud icon, you will get a pop up where you will upload your documents from your device.</br>
						<img width="600" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/60.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I retrieve my uploaded documents?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra Dashboard, click on the profile dropdown and select ‘My Document Locker’.</br>
						2.	On your Document Locker page, you will be able to see all your uploaded documents from each module.</br>
						3.	Hover over the 3 dots against the entry you want to download.</br>
						4.	Your document should get downloaded onto your device.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, export, email, print and delete the details of my asset tracker?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Tracker’. </br>
						2.	In your Asset Tracker list, find the one you would like to edit, export, email or delete.</br>
						3.	Hover over the 3 dots you will get edit, export, email or delete options. </br>
						4.	If you click delete, a confirmation pop up will appear, your desired Purchase Asset will then be deleted.</br>
						<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/61.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit, email, export, print and delete my asset trackers in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciations’ and select ‘Asset Tracker’. </br>
						2.	You will be able to view your list of Asset Tracker on the RHS of your screen.</br>
						3.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then select your option from the bulk action dropdown to edit, export, email or delete.</br>
						4.	If you click delete, a confirmation pop-up will appear, and your desired Asset will be deleted.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/62.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <!-- Depreciation Schedule -->
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How to check my depreciation schedule?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and choose the option ‘Depreciation Schedule’. </br>
						2.	On this page, you will be able to check your depreciation schedule.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Which method of depreciation are you following?</div>
                        <div class="collapsible-body"><span><p>1.	Xebra is auto-calculating the depreciation value if you follow are following the Written Down Value method and auto-updating your accounting reports.</br>
						2.	For the Straight-Line method, you must calculate the depreciation amount manually and enter the accounting entries from the My Accounts module.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Where & how do I add the depreciation details for an asset?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and then choose ‘Asset Purchase’. </br>
						2.	While creating or editing your asset purchase, you will be able to add your depreciation details.</p></span></div>
                      </li>
					  
					  <li class="row document-box-view redborder">
							<div class="collapsible-header">How can I directly make an asset purchase payment from my Xebra account?</div>
							<div class="collapsible-body">
								<p>
									•	On your Xebra dashboard, click on ‘My Assets & Depreciation’ and further click on ‘Asset Purchase’.</br>
									<img width="250" height="320" src="<?php echo base_url(); ?>public/images/faqs-images/244.png" alt=""></img></br>
									•	From the list of asset purchases, hover on the three dots next to the one you would like to make the payment against.</br>
									•	A dropdown will appear where you click on ‘Payment’. </br>
									<img width="1000" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/245.png" alt=""></img></br>
									•	An OTP Confirmation pop up will appear (please note- this feature of OTP confirmation will only appear If you have already integrated your ICICI bank account with Xebra). </br>
									<img width="950" height="465" src="<?php echo base_url(); ?>public/images/faqs-images/240.png" alt=""></img></br>
									•	To learn how to integrate your ICICI account directly with Xebra, <a class="bColor ohhh">click here</a>.</br>
									•	After putting in the correct OTP, you will be directed to a create payment page to verify the amount and click on ‘save’.</br> 
									<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/241.png" alt=""></img></br>
									•	All the details on the create payment page will already be pre-filled, except the payment status. </br>
									•	Click on the arrow sign, and under payment status- make sure you fill in the correct amount received (full/half/part).</br>
									•	Check all the other details correctly and then click on ‘Save’.</br>
									•	You will receive a confirmation notification for the same.</br>
									<img width="350" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/242.png" alt=""></img></br>
									•	Additionally, you can also check if the payment has been recorded by clicking on ‘my receipts and payments’ and selecting ‘Payments’ from the dropdown options.</br>
									•	Under payments, click on ‘Asset Payments’ from the dropdown options.</br>
									<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/246.png" alt=""></img></br>
									•	You will be able to view your recorded payment on the screen.
								</p>
							</div>
						</li>
                    </ul>
                  </div>
				  <div id="test8" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">What does Employee Access mean? How do I activate or deactivate that?</div>
                        <div class="collapsible-body"><span><p>Employee Access allows you to give log in permission to your employees. You can select the ones you want to access to the Xebra platform from toggle switch in Employee Master section. Once you give access that particular employee will receive his/her own login credentials and be able to log in. They will be able to:</br>
						•	Create their expense vouchers and see the payment status</br>
						•	View their monthly salary and download their salary slips</br>
						•	View their monthly TDS and other taxes deductions and download a copy for themselves
						</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can an employee make any changes to any records or module from their login?</div>
                        <div class="collapsible-body"><span><p>No, the only module that an employee can use create and edit feature is their expense voucher. The employee login feature does not allow any create or edit or delete feature. You can read more about the access list in My Person Profile / Create</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">What is the difference between HR & Employee login?</div>
                        <div class="collapsible-body"><span><p>When an employee logs in, he or she can only see their expense vouchers, salary and TDS deductions
						When an HR manager logs in, the person will get complete access to My Employees module along with their individual access to expense voucher, salary and TDS deduction. You can read more about the access list in My Person Profile / Create
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I track leaves of each employee?</div>
                        <div class="collapsible-body"><span><p>The leaves are auto-calculated when you record the entry in the Salary Expense sub-module. You will be able to see the leaves taken by an employee during each month or year
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Which modules can be accessed with HR Login?</div>
                        <div class="collapsible-body"><span><p>When an HR manager logs in, the person will get complete access to My Employees module along with their individual access to expense voucher, salary and TDS deduction. You can read more about the access list in My Person Profile / Create
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record any advance that I have given to an employee?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Additions tab
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record Gratuity, Super Annuation benefits or joining bonus or sales incentive?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Additions tab
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record any advance given to an employee?</div>
                        <div class="collapsible-body"><span><p>Go to Salary Expense sub-module and select a particular employee. From there, click on Additions and select Advance from the dropdown option and Save
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record my share of EPF or ESIC?</div>
                        <div class="collapsible-body"><span><p>Yes, you can record it salary expense sub module for individual employees and then make payment entries in Receipts and Payments Module
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record appraisal discussion for the individual employee and set reminders for it?</div>
                        <div class="collapsible-body"><span><p>Yes, you can set reminders for appraisal in Employee Master and record appraisal discussion of each employee in the Appraisal submodule
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">What is the use of Notice Board?</div>
                        <div class="collapsible-body"><span><p>Notice Board is a module where you can put up your company’s policies, annual holidays, company events, announcements and leave policy. This central system will ensure that all employees across locations can get access to the policies applicable to them 
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record leave policy for employees on a contract basis?</div>
                        <div class="collapsible-body"><span><p>Yes it allows you to record leave policy for Full Time and well as Employees on Contract basis 
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">We have different leave policy for each location. What do we do?</div>
                        <div class="collapsible-body"><span><p>Xebra’s leave tracker module allows you to create leave policy separately for each location 
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to manually track the leaves for each employee while calculating their salary?</div>
                        <div class="collapsible-body"><span><p>No, Xebra auto-tracks the leaves and auto-records it in salary module to make it easier to calculate salaries for each employee 
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">We have our leave approval policy?</div>
                        <div class="collapsible-body"><span><p>Xebra allows each employee to set-up their approver policy. This will ensure that their senior is notified when leave application is made. That person can either approve or reject the application which will be recorded in the system accordingly 
						</p></span></div>
                      </li>
					  <!--li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record the EPF and ESIC in payments?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Deductions tab
						</p></span></div>
                      </li-->
                    </ul>
                  </div>
                  <div id="test9" class="col s12 inner_row_container">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I enter Opening Cash Balance?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘My Company Profile’.</br>
							2.	On the Create/Edit Company Profile under bank details. Under Company information, you will find your cash opening balance.(To know what is opening balance click here)</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/211.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit Opening Cash Balance?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘Company Profile’.</br>
							2.	Hover on the three dots and select ‘Edit’. Under Company Information, you can edit your cash opening balance. </br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/211.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record the receipts in cash? </div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘Receipts and Payments’.</br>
							2.	Click on ‘Sales Receipts’ and then select ‘Add new sales receipt’. Select the cash option under ‘Mode of Receipts’</br>
							3.	If you have received a bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							4.	Fill in the balance details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/212.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record the payments made in cash?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘Receipts & Payments’ and select ‘Payments’.</br>
							2.	Click on ‘Add New Expense Payment’ on the RHS of the page.</br>
							3.	Under Payment details, click on ‘Mode of Payment’ dropdown and select cash. Fill in the relevant information and click ‘Save.’</br>
							<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/213.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record cash deposited or withdrawn from the bank?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	Click on ‘Add New Cash Entry’ on the RHS of the page.</br>
							3.	On the Create Cash Entry page, under particulars, select deposit or withdrawn. Fill in the relevant details and click ‘Save’.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/214.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record cash transferred to a petty cash account?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	Click on ‘Add New Cash Entry’ on the RHS of the page.</br>
							3.	On the Create Cash Entry page, under particulars, select ‘Add to Petty Cash’. Fill in the relevant details and click ‘Save’.</br>
							4.	You can see this transaction in your bank statement.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/214.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit or delete a cash entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	Select the entries you would like to edit or delete.</br>
							3.	Hover over the 3 dots, and you will see the ‘Edit’ and ‘Delete’ option.</br>
							4.	If you click delete, a confirmation pop up will appear, your desired entry will then be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/215.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email, or print a cash entry in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	You will be able to view your list of Card Statements on the RHS of your screen.</br>
							3.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left. Hover on bulk action and select the appropriate action from the dropdown.</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear, and the chosen entry will be deleted</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/216.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Where is the opening balance coming from?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘Company Profile’.</br>
							2.	On the ‘Create/Edit’ Company Profile under bank details. The opening balance amount filled in will reflect on the opening balance on your bank statement.</br>
							(To know what is opening balance click here)</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/217.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit the opening balance?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘Company Profile’.</br>
							2.	Hover over the 3 dots and select ‘Edit’. On the Edit company profile page under Bank Details, you will get an option to edit.</br>
							3.	Click on the ‘pencil’ icon, and you can edit your opening balance for the selected account and click ’Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/217.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record the receipts in the bank? </div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘Receipts and Payments’.</br>
							2.	Click on ‘Sales Receipts’ and then select ‘Add new sales receipt’. Select the Bank Transfer option under ‘Mode of Receipts’.</br>
							3.	If you have received a bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							4.	Fill in the balance details and click ‘Save’.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/212.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record the payments made in the bank?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘Receipts & Payments’ and select ‘Payments’.</br>
							2.	Click on ‘Add New Expense Payment’ on the RHS of the page.</br>
							3.	Under Payment details, click on ‘Mode of Payment’ dropdown and select ‘Bank Transfer’. Fill in the relevant information and click ‘Save.’</br>
							<img width="300" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/213.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record cash deposited/withdrawn from the bank?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	Click on ‘Add New Cash Entry’ on the RHS of the page.</br>
							3.	On the Create Cash Entry page, under particulars, select deposit or withdrawn and the bank account. Fill in the relevant details and click ‘Save’.</br>
							4.	You will be able to see this entry on your Bank Statement page.</br>
							<img width="400" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/214.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I show money transfer from one bank account to another?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Bank Statement’.</br>
							2.	Click on ‘Add New Bank Transfer Entry’ on the RHS of the page.</br>
							3.	On the Create Bank Entry page, select the bank account you would like to transfer from. Fill in the relevant details and click ‘Save’.</br>
							<img width="400" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/218.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record multiple bank accounts?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select Company Profile.</br>
							2.	Hover over the 3 dots and select ‘Edit’. On the Edit company profile, under Bank Details, you will add multiple bank accounts.</br>
							3.	Click on the ‘+’ icon and fill in your details and click ‘Save’.</br>
							<img width="450" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/219.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record payment made towards the credit card? </div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘Receipts and Payments’.</br>
							2.	Click on ‘Sales Receipts’ and then select ‘Add new sales receipt’. Select the Bank Transfer option under ‘Mode of Receipts’</br>
							3.	If you have received a bulk amount, you can also combine another invoice with it by simply clicking on the ‘+’ sign.</br>
							4.	Fill in the balance details and click ‘Save’.</br>
							<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/212.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit or delete an entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Bank Statement’.</br>
							2.	On your Bank Statement page, find the one you would like to edit or delete.</br>
							3.	Hover over the 3 dots, and you will get an option to edit or delete.</br>
							4.	If you click ‘Delete’, a confirmation pop up will appear, your desired entry will then be deleted.</br>
							<img width="900" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/220.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email and print bank entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Bank Statement’.</br>
							2.	You will be able to view your list of Bank Statements on the RHS of your screen.</br>
							3.	Hover over the ‘Bulk Action’ dropdown, and you will be able to see the export, email and print option</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear, and your chosen entry will be deleted.</br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/221.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add my opening Petty Cash balance?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘Company Profile’.</br>
							2.	On the Create/Edit Company Profile under bank details. Under Company Information, you will find your petty cash opening balance.</br>
							(To know what is opening balance click here)</br>
							<img width="500" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/211.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit the opening Petty Cash balance?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click your profile dropdown and select ‘Company Profile’.</br>
							2.	On the Edit Company Profile under bank details. Under Company information, you can edit your petty cash opening balance.</br>
							<img width="500" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/211.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record cash transferred to a petty cash account?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Cash Statement’.</br>
							2.	Click on ‘Add New Cash Entry’ on the RHS of the page.</br>
							3.	On the Create Cash Entry page, under particulars, select ‘Petty Cash’ and the bank account. Fill in the relevant details and click ‘Save’.</br>
							4.	You will be able to see this entry on your Bank Statement page.</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/222.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record expenses in a petty cash account?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Petty Cash’.</br>
							2.	Click on ‘Add New Petty Cash’ on the RHS of the page.</br>
							3.	On the Create Petty Cash Statement, select if you want to debit or credit the amount and the other relevant details and click ‘Save.’</br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/223.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit and delete petty cash entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Petty Cash’.</br>
							2.	On your Petty Cash page, find the one you would like to edit or delete.</br>
							3.	Hover over the 3 dots, and you will get an option to edit or delete.</br>
							4.	If you click delete, a confirmation pop-up will appear, your chosen entry will then be deleted.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/224.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email, and print petty cash entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Petty Cash’.</br>
							2.	You will be able to view the list of your Petty Cash.</br>
							3.	Hover over the bulk action dropdown, and you will see the export, email and print option.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/225.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record the payments made by card?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Card Statement’.</br>
							2.	Click on ‘New Card Payment’ on the RHS of your screen.</br>
							3.	Select the Card, Bank Name and amount and click on ‘Save.’ </br>
							<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/226.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record payment made towards the credit card? </div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Card Statement’.</br>
							2.	Click on ‘Add New Card Payment’ on the RHS of your screen.</br>
							3.	On the Record Card Payment page, select your card, bank name, the amount and click ‘Save’.</br>
							<img width="900" height="80" src="<?php echo base_url(); ?>public/images/faqs-images/227.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record multiple debit or credit cards?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
							2.	On Add or Edit Company Profile, you will notice the debit & credit card details section. Click on the '+' icon and enter your card details.</br>
							<img width="600" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/228.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit, export, email, and print card entry?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Card Statement’.</br>
							2.	On your Petty Cash page, find the one you would like to edit or delete.</br>
							3.	Hover over the 3 dots, and you will get an option to edit or delete.</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear, your desired entry will then be deleted.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email and print card entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Card Statement’.</br>
							2.	You will be able to view the list of your Petty Cash.</br>
							3.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to export, email or delete.</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear, and your desired entry will be deleted.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/229.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email and print card entries in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Cash, Bank & Cards’ and select ‘Card Statement’.</br>
							2.	You will be able to view the list of your Petty Cash.</br>
							3.	Select the entries you would like to take action on by selecting (ticking) the boxes on the extreme left side of your entry and then choose your option from the bulk action dropdown to export, email or delete.</br>
							4.	If you click ‘Delete’, a confirmation pop-up will appear, and your desired entry will be deleted.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/229.png" alt=""></img>
							</p></span></div>
						</li>
						
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my opening Bank Balance?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module while entering your bank details</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record money transferred from my one bank account to another?</div>
                        <div class="collapsible-body"><span><p>Go to Bank Statement and select Record bank Transfer. All the bank accounts that you entered from My Company Profile module will be reflected in that dropdown. You can select the respective banks and make the entry</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a bank entry about a sales receipt or vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant submodule under Receipts & Payments section and edit or delete from there. You will not be able to edit or delete from Bank Statement</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my opening Cash Balance?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module in Customisation section</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record cash deposited or withdrawn from my bank account?</div>
                        <div class="collapsible-body"><span><p>Go to Cash Statement and select Add Cash Entry. You can select deposit or withdraw and the appropriate bank name against it to make the entry</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a cash entry about a sales receipt or vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant submodule under Receipts & Payments section and edit or delete from there. You will not be able to edit or delete from Cash Statement</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record my opening petty cash statement?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module in the Customization section</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record adding more cash for petty cash expenses?</div>
                        <div class="collapsible-body"><span><p>Go to Cash Statement and click on Add Cash Entry. Select Add to Petty Cash option in the particulars and enter the amount</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add more than 1 bank/card account?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add multiple bank/card account</p></span></div>
                      </li>

						<li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my different types of credit and debit cards?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module. In that section, you can record as many of your Debit and Credit cards</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I make card payments?</div>
                        <div class="collapsible-body"><span><p>Go to Card Statement and click on Record Card Payment. You Can select the card you want to make the payment for along with the statement period.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a card entry about a vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant submodule under Receipts & Payments section and edit or delete from there. You will not be able to edit or delete from Card Statement</p></span></div>
                      </li>
                    </ul>
                  </div>
				  
				  <div id="test10" class="col s12 inner_row_container">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I create a purchase order?</div>
							<div class="collapsible-body"><span><p>1.	If you are creating your first Purchase Order, you will simply see a New Page screen where you click on the ‘Add New Purchase Order’ button.</br>
							2.	If someone from your team has already created one before, then on your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Purchase Orders’.</br>
							3.	To create a new purchase order, click on ‘Add New Purchase Order’.</br>
							<img width="450" height="360" src="<?php echo base_url(); ?>public/images/faqs-images/121.png" alt=""></img></br>
							4.  On the create purchase order page, select the type of the order from the dropdown options. Then select the vendor name. Xebra will auto-fill the billing and shipping details on the RHS of the screen.</br>
							5. Then fill in the purchase order details and customize any other relevant information. Then click on ‘Save’.</br>
							6. Your purchase order details will be updated on the main page, as shown in the image below.</br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/122.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit and delete a purchase order?</div>
							<div class="collapsible-body"><span><p>To edit a purchase order, follow the steps below:</br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Purchase Order’. </br>
							2.	In your purchase order list, find the one you want to edit or delete. Hover on the three dots of the particular row and select the edit or delete option.</br>
							3.	If you click on ‘Delete’, a confirmation pop-up will appear.</br>
							<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/123.png" alt=""></img></br>
							4.	Your desired purchase order will then be deleted.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How to export a single or multiple purchase order?</div>
							<div class="collapsible-body"><span><p>To download a single purchase order, follow the steps below:  </br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’, then select ‘Purchase Order’.</br>
							2.	From your purchase order list, hover near the three dots next to the relevant purchase order. A drop down will appear. </br>
							3.	Click on ‘Download’ from the dropdown options.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/124.png" alt=""></img></br>
							4.	Your desired purchase order will be downloaded on your computer.</br>
							To download multiple purchase orders:</br>
							1. On your Xebra dashboard, click on ‘My Purchases and Expenses’, then select ‘Purchase Order’.</br>
							2. You will be able to view your list of purchase orders on the RHS screen.</br>
							3. Select all the orders from your list that you would like to export and click on the ‘Bulk Actions' option on the LHS of the screen.</br>
							4. Your desired purchase orders will be downloaded on your computer.</br>	
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/125.png" alt=""></img></br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I set a reminder for the renewal of the purchase order?</div>
							<div class="collapsible-body"><span><p>You can upload the purchase order document for a particular client or vendor in ‘My Document Locker’ under client or vendor master.</br>
							After uploading the document, you can set a reminder to ensure that you renew it well in time.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Is it possible to create a purchase order on an international vendor?</div>
							<div class="collapsible-body"><span><p>Yes, it is possible to create a purchase order on an international vendor.</br>To learn how to create a new purchase order, click here. 
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I customize my terms and conditions in the purchase order?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’, then select ‘Purchase Order’.</br>
							2.	If you are creating a new purchase order, click on ‘Add New Purchase Order’ on the RHS of the purchase orders page.</br>
							3.	On the create purchase order page, select the type of the order from the dropdown options. Then select the vendor name. Xebra will auto-fill the billing and shipping details on the RHS of the screen.</br>
							4.	Then fill in the purchase order details and customize any other relevant information. </br>
							5.	Below the purchase order details, Add the terms and conditions in the box as shown in the image.</br>
							6.	If you would like to set these terms and conditions for all future purchase orders, click on ‘Set as Default’.</br>	
							<img width="600" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/126.png" alt=""></img></br>
							If you want to edit the terms and conditions of a purchase order that is already created, click here to learn how to edit a purchase order.
							</p></span></div>
						</li>
						<!-- COMPANY PURCHASES -->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add a new company purchase?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’.</br>
							2.	Click on ‘Add New Company Purchase’.</br>
							3.	On the create company purchase page, select the purchase date and vendor name. Then select the ‘Nature of Purchase’ (one time/ recurring) from the dropdown options.</br>	
							<img width="600" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/127.png" alt=""></img></br>
							4.	Further, add the details in the purchase table and click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I edit and delete a company purchase?</div>
							<div class="collapsible-body"><span><p>To edit a company purchase, follow the steps below:</br>	
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’.</br>	
							2.	In your company purchases list, hover on the three dots next to the relevant purchase and click on ‘Edit’ from the dropdown.</br>	
							<img width="600" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/128.png" alt=""></img></br>
							4.	Make the desired changes and click on ‘Save’.</br>
							To delete a company purchase, follow the steps below:</br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’.</br>
							2.	In your company purchases list, hover on the three dots next to the relevant purchase and click on ‘Delete’ from the dropdown.</br>
							3.	On the confirmation pop-up, click ‘Delete.’</br>
							<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/123.png" alt=""></img>
							</p></span></div>
						</li>		
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add the vendor’s bill number as my reference number while recording a purchase?</div>
							<div class="collapsible-body"><span><p>To add a reference purchase number while recording a purchase, follow the steps below:</br>
							1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’.</br>
							2.	To create a new company purchase, click on ‘Add New Company Purchase.’</br>
							3.	On the create company purchase page, select the purchase date and vendor name. Then choose the nature of the purchase (one time/ recurring) from the dropdown options.</br>
							4.	Subsequently, you can also upload a soft copy of the purchase invoice. </br>
							5.	You can add in the reference no. and date as shown in the image on the RHS corner of the page.</br>
							<img width="700" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/129.png" alt=""></img></br>
							6.	Further, add the purchase details in the purchase table and click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a payment against a purchase?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’.</br>
							2.	In your company purchases list, hover on the three dots next to the relevant purchase and click on ‘Payment’ from the dropdown.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/131.png" alt=""></img></br>
							3. Fill in the purchase amount, expense category and other details.</br>
							4. You can also create another payment by clicking on the ‘+’ button. </br>
							5. Then fill in your payment details as shown in the image below. </br>
							<img width="500" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/132.png" alt=""></img></br>
							6. You can also upload any documents that you may have related to the payment. </br>
							7. After checking all the above details, click on ‘Save’.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I record a recurring purchase?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Company Purchase’ from the dropdown options.</br>
							2.	Click on ‘Add New Company Purchase.’</br>
							3.	On the create company purchase page, select the type of purchase from the dropdown options. Then select the purchase name, and under Nature of purchase, select ‘Recurring’ from the dropdown options.</br>
							4.	You can also add the date of recurrence, no of times, and the frequency as shown in the below image.</br>
							<img width="600" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/133.png" alt=""></img></br>
							5.	On the RHS of the screen, you can also upload a soft copy of the invoice. </br>
							6.	Then fill in the details in the company purchase table and customize any other relevant information. Then click on ‘Save’.</br>
							If you want to make a recurring company purchase of an already created purchase, edit your purchase and follow the same steps. To learn how to edit a purchase, click here.</br>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I delete a particular row from the table?</div>
							<div class="collapsible-body"><span><p>If you are creating a new company purchase (to learn how to create a new company purchase, click here), Fill in your relevant details in the purchase document.</br>
							OR to delete a row from a company purchase that is already created, edit the company purchase (to learn how to edit a company purchase, click here) </br>
							In the company purchase table, go to the selected line item and hover on the amount section. A pink ‘X’ button will appear, as shown in the image. Click on it, and your row will be deleted from the table. After making all the desired changes, click on ‘Save’.</br>
							<img width="900" height="230" src="<?php echo base_url(); ?>public/images/faqs-images/134.png" alt=""></img></br>
							</p></span></div>
						</li>
						<!-- STOCK ITEM MASTER / INVENTORY (For E-commerce and Trading Companies) -->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add my stock items?</div>
							<div class="collapsible-body"><span><p>●	There are two ways to add stock items:</br>
							1.   On your Xebra dashboard, click on ‘My Sales’ and select ‘Stock Item / Inventory Master’.
							2a. If you are creating your first stock item, click on ‘Add New Stock Item / Inventory’.</br>
							<img width="800" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/135.png" alt=""></img></br>
							2b. If you have already created an item before, click on ‘Add New Stock Item.’</br>
							<img width="800" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/136.png" alt=""></img></br>
							●	Fill in the relevant details like item name, descriptions, HSN, the associated taxes & percentage</br>
							●	Then add in your product information like opening stock and purchase rate. The system will auto-calculate the Value of that stock. Click on ‘Save’ after all the details have been added.</br>
							<img width="600" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/137.png" alt=""></img></br>
							●	The second way of adding stock items is through an invoice. Click on ‘My Sales’ and select ‘Billing Documents’. In Add New Sales Invoice, select the client name</br>
							<img width="800" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/138.png" alt=""></img></br>
							●	 Scroll down to the invoice table, click on ‘Add New Stock Item’.</br>
							●	A pop-up box will appear where you enter all the details of that stock item.</br>							 
							●	When you click on ‘Save’, your new stock item will appear in the list. It will also be saved in the Stock Item Master module.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is opening stock?</div>
							<div class="collapsible-body"><span><p>Opening Stock is the amount of stock you purchase initially to commence operations.
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Do I have to manually update the purchase and sales of each stock item in the inventory module?</div>
							<div class="collapsible-body"><span><p>No. Xebra will auto-update all your purchases and sales of individual products. Whenever you raise an invoice of a particular item, it will be recorded as sales, and that much quantity will be deducted. Vice-versa for purchase of that item. You will know the closing stock quantity at all times of individual stock items
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">I created my stock item from the invoice. Will it appear in the inventory module?</div>
							<div class="collapsible-body"><span><p>Yes - it doesn’t matter where you have created your stock item from.</br>
							To learn how to add a new stock item while creating an invoice, click here
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I change the GST % or Cess % for an item?</div>
							<div class="collapsible-body"><span><p>●	Click on ‘My Sales’ and select ‘Stock Item Master’ from the dropdown options.</br>
							●	In the list section, hover on the Action three vertical dots. Click on ‘Edit’ from the dropdown, change the GST or Cess % and save. Do note that the effect of the revised % will be effective only on the new invoices made after that change</br>
							</p></span></div>
						</li>
						
						
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I record international vendors and record a payment in international currency?</div>
							<div class="collapsible-body"><span><p>Yes, you can record international vendors from vendor master. While filling up the data of that vendor, you can select the currency in which you want to pay them</p></span></div>
						</li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Is it possible to record recurring purchases without having to make an entry for it every time?</div>
                        <div class="collapsible-body"><span><p>Yes, in company purchase module, you select the purchase and then select recurring in nature of the purchase. You can set the recurring to the number of times you want. Xebra will automatically record the purchase of that amount at the set frequency</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I select different GST while recording each purchase?</div>
                        <div class="collapsible-body"><span><p>Xebra will automatically select the right GST basis the place of supply of the vendor</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record my non-payment or partial payment against my vendor’s invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View in Company Purchase and click on Make Payment option in Action Button. You will be guided to Purchase Payment module where you will record the entry</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record a bulk payment made to a vendor’s multiple invoices?</div>
                        <div class="collapsible-body"><span><p>While recording the purchase payment, simply click on add another invoice button which will allow you to club another invoice from the same vendor for the payment</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record the payment to an International vendor in foreign exchange?</div>
                        <div class="collapsible-body"><span><p>Follow these steps for correct recording of an international currency payment</p>
						<ul style="margin-left: 20px;">
                        <li class="li_cls">1. Select appropriate country & currency while recording in the vendor master</li>
                        <li class="li_cls">2. When you record your company expense for that vendor, the system will automatically ask you for a foreign exchange rate to the filled that is prevailing in the current time</li>
                        <li class="li_cls">3. When you make the payment after say a period of one month, the system will again prompt you to enter the foreign exchange rate prevalent then and auto-calculate the forex gain or loss that you might have incurred</li>
                      </ul></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record forex gain or loss incurred because of international vendor payment timing?</div>
                        <div class="collapsible-body"><span><p>Select the appropriate currency for your vendor in the vendor master. Then enter the forex rate at the time of making the invoice and at the time of receiving the payment. Xebra will automatically the forex gain or loss derived from it</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">After providing access to vendors in the vendor portal, can we restrict them afterwards?</div>
                        <div class="collapsible-body"><span><p>Yes, you can deny them access by simple toggle button in the vendor master</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record company purchase and purchase payment from the mobile application?</div>
                        <div class="collapsible-body"><span><p>No, you will be able to record that from your web interface only</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I check on my inventory?</div>
                        <div class="collapsible-body"><span><p>You will get complete inventory status of each item in Stock Item Master in Sales Module</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record the opening stock for each item?</div>
                        <div class="collapsible-body"><span><p>You can record the opening stock in Stock Item / Inventory Master while recording your product</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to manually enter purchase and sales of each item in inventory?</div>
                        <div class="collapsible-body"><span><p>No. Every time you record a purchase or sales entry, Xebra will automatically update your inventory status for each item</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I view the inventory of each stock item separately?</div>
                        <div class="collapsible-body"><span><p>Simply click on the Stock Item filter and select the product whose inventory you want to view</p></span></div>
                      </li>
					  
					  <li class="row document-box-view redborder">
							<div class="collapsible-header">How can I make a Company purchase payment directly from my Xebra portal?</div>
							<div class="collapsible-body">
								<p>
									•	On your Xebra dashboard, click on ‘My Purchases & Expenses’ and then click on ‘Company Purchases’.</br>
									<img width="250" height="320" src="<?php echo base_url(); ?>public/images/faqs-images/247.png" alt=""></img></br>
									•	From the list of company purchases, hover on the three dots next to the one you would like to make the payment against.</br>
									•	A dropdown will appear where you click on ‘Payment’. </br>
									<img width="1000" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/248.png" alt=""></img></br>
									•	An OTP confirmation popup will appear (please note- this feature of OTP confirmation will only appear If you have already integrated your ICICI bank account with Xebra). </br>
									<img width="950" height="465" src="<?php echo base_url(); ?>public/images/faqs-images/240.png" alt=""></img></br>
									•	To learn how to integrate your ICICI account directly with Xebra, <a class="bColor ohhh">click here</a>.</br> 
									•	After putting in the correct OTP, you will be directed to a create payment page to verify the amount and click on ‘save’.</br> 
									<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/241.png" alt=""></img></br>
									•	All the details on the create payment page will already be pre-filled, except the payment status. </br>
									•	Click on the arrow sign, and under payment status- make sure you fill in the correct amount received (full/half/part).</br>
									•	Check all the other details correctly and then click on ‘Save’.</br>
									•	You will receive a confirmation notification for the same.</br>
									<img width="350" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/242.png" alt=""></img></br>
									•	Additionally, you can also check if the payment has been recorded by clicking on ‘my receipts and payments’ and selecting ‘Payments’ from the dropdown options.</br>
									•	Under payments, click on ‘Pur. & Expense Payments’ from the dropdown options.</br>
									<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/246.png" alt=""></img></br>
									•	You will be able to view your recorded payment on the RHS of the screen.
								</p>
							</div>
						</li>
                    </ul>
                  </div>
				  
				  <div id="test11" class="col s12 inner_row_container test11">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I add a new company profile?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your Profile dropdown button, and select ‘Company Profile’.</br>
						2.	Click on ‘Add New Company Profile’ on the RHS of the screen.</br>
						3.	Choose your plan based on your company's requirements, buy your add-ons, fill in your company details, and proceed with your payment.</br>
						<img width="250" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/90.png" alt=""></img>
						</p></span></div>
                       </li>
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header">How to add a company logo after I have created my company profile?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your Profile dropdown button, select ‘Company Profile’.</br>
						2.	On your company profile list, find the one you would like to add the company logo to.</br>
						3.	Hover over the three dots, and you will get the ‘Edit’ option.</br>
						4.	On the Edit Company Profile, you will get an option to upload your company profile. Click on the option, and you will be able to upload the file from your device.</br>
						<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/91.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit my GST details?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On your company profile list, find the one you would like to add the company logo to.</br>
						3.	Hover over the three dots, and you will get the ‘Edit’ option.</br>
						4.	On the Edit Company Profile, under Statutory Info, click on the 'pencil' icon, and you will be able to edit your GSTIN details.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/92.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I add multiple GST Numbers?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On your company profile list, find the one you would like to add the company logo to.</br>
						3.	Hover over the three dots, and you will get the ‘Edit’ option.</br>
						4.	On the Edit Company Profile, under Statutory Info, you will be able to add multiple GSTIN numbers depending on the plan you have chosen.</br>
						5.	Click on the '+' to add your GSTIN number and other details. Click 'Save' after you're done.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/92.png" alt=""></img>
						</p></span></div>
                      </li>
					  
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I add my bank details?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the bank details section. Click on the '+' icon and enter your bank details in the pop-up box.</br>
						<img width="500" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/96.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add an International bank account?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add an international bank account.</br>
						1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the bank details section. Click on the '+' icon and enter your bank details in the pop-up box.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add more than one bank account?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add multiple bank accounts.</br>
						1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the bank details section. Click on the '+' icon and enter your bank details in the pop-up box.</br>
						<img width="500" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/96.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit my bank details?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	In Edit Company Profile, you will notice the bank details section. Click on the 'Pencil' icon to edit your bank details.</br>
						<img width="500" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/96.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I add multiple cards?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, and select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the debit & credit card details section. Click on the '+' icon and enter your card details.</br>
						<img width="500" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/97.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit my debit and credit card details?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the debit & credit card details section. Click on the 'pencil' icon, and you will be able to edit your card details.</br>
						<img width="500" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/97.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I retrieve or delete my uploaded documents?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the Document Locker section. Click on the 'X' icon to delete your documents.</br>
						Alternatively,</br>
						1.	On your Xebra dashboard, click on your profile dropdown button. Click on ‘Document Locker’.</br>
						2.	On the Document Locker page, select the document you would like to download or delete.</br>
						3.	Hover over the three dots, and you will find the download and delete option.</br>
						<img width="800" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/98.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">What do you mean by opening a cash balance?</div>
                        <div class="collapsible-body"><span><p>The opening balance is the amount of money a business starts with at the beginning of the reporting period, usually the first day of the month: opening balance = closing balance of the previous period. If there is no previous period, then the opening balance will be zero.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">What do you mean by opening petty cash balance?</div>
                        <div class="collapsible-body"><span><p>A petty cash fund is a small amount of cash kept on hand to pay for minor expenses, such as office supplies or reimbursements. An opening petty cash balance is an amount of money you have at the beginning of the financial year (opening balance = closing balance of the previous period). If there is no previous balance, the opening balance will be zero.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I add multiple client and case study URLs?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On Add or Edit Company Profile, you will notice the Web Presence section. You can add your client list and case study URLs.</br>
						<img width="550" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/99.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit, export, email, print, or deactivate my account?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘Company Profile’.</br>
						2.	On your company profile list, find the one you would like to add the company logo to.</br>
						3.	Hover over the three dots, and you will get the edit, export, email, print or deactivate options.</br>
						4.	If you choose to deactivate your account, a confirmation pop-up will appear, and your company account will be deactivated.</br>
						<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/100.png" alt=""></img>
						</p></span></div>
                      </li>
					  
					  <!-- My Personal Profile -->
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I add a new personal profile?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	Click on ‘Add New Personal Profile’ on the RHS on the screen.</br>
						3.	Choose your plan based on your company's requirements, buy your add-ons, fill in the user details, and proceed with your payment.</br>
						<img width="200" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/90.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How to upload a profile picture?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	On Add or Edit your Personal Profile page, you can add or change your profile picture.</br>
						<img width="600" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/102.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How to upload my signature</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	On Add or Edit your Personal Profile page, you can add or change your signature.</br>
						<img width="600" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/102.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I edit, export, print or deactivate my profile?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	On your personal profile list, find the one you would like to take action on.</br>
						3.	Hover over the three dots, and you will get the edit, export, print or deactivate options.</br>
						4.	If you choose to deactivate your account, a confirmation pop-up will appear, and your company account will be deactivated.</br>
						<img width="900" height="320" src="<?php echo base_url(); ?>public/images/faqs-images/103.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I export, print, or deactivate my profile in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	On your personal profile list, find the one you would like to take action on.</br>
						3.	Hover over the ‘Bulk Action’ dropdown, and you will get the edit, export, print or deactivate options.</br>
						4.	If you choose to deactivate your account, a confirmation pop-up will appear, and your company account will be deactivated.</br>
						<img width="900" height="320" src="<?php echo base_url(); ?>public/images/faqs-images/104.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I change the access level of any user at any point of my subscription?	</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Personal Profile’.</br>
						2.	On My Personal Profile, select the entry you want to edit. Hover over the three dots, and you will get the edit option.</br>
						3.	On the Edit Personal Profile page under the designation, you will be able to change the access level of any user.</br>
						4.	Please note only users with Admin access will be able to carry out this change.</br>
						<img width="500" height="420" src="<?php echo base_url(); ?>public/images/faqs-images/105.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Why do I need to upload my signature?</div>
                        <div class="collapsible-body"><span><p>You will need to upload your signature if you wish to use it on your billing documents
						</p></span></div>
                      </li>
					  <!-- My Document Locker -->
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I download, email, or delete my documents?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Document Locker’.</br>
						2.	On the Document Locker page, select the entry you want to act on.</br>
						3.	Hover over the 3 dots, and you will get to download, email or delete the document.</br>
						<img width="900" height="360" src="<?php echo base_url(); ?>public/images/faqs-images/106.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I download, email, or delete my documents in bulk?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Document Locker’.</br>
						2.	On the Document Locker page, select the entries you want to act on by ticking the box on the LHS of your entry</br>
						3.	Hover over the ‘Bulk Action’ dropdown, and you will get to download, email, or delete the documents.</br>
						<img width="900" height="360" src="<?php echo base_url(); ?>public/images/faqs-images/107.png" alt=""></img>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I know how much space I have used up already?</div>
                        <div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your profile dropdown button, select ‘My Document Locker’.</br>
						2.	On the Document Locker page, under file size, you will be able to see how much space you have used up.
						</p></span></div>
                      </li>
					  
					  
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does it capture the user's activities with CA, HR, Accountant and Employee login?</div>
                        <div class="collapsible-body"><span><p>Yes, if you have Admin access, you will be able to see the activities done by CA, HR, Accountant and Employee users</p></span></div>
                      </li>
						
					  <!-- Referral Programme -->
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How can I edit my referral details?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the Profile dropdown. Click on ‘My Account’ and select ‘My Referral Programme’.</br>
							2.	Click on ‘Edit’ My Referral Details on the RHS of the page.</br>
							3.	On the Edit My Referral Details page, you can make the necessary changes and click 'Save.'</br>
							<img width="200" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/90.png" alt=""></img>
							<img width="550" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/118.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How can I send an invitation to the referral programme?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the Profile dropdown. Click on ‘My Account’ and select ‘My Referral Programme’.</br>
							2.	On the My Referral Programme page, you will send an invite by email and SMS.</br>
							3.	You will be able to see your referrals below.</br>
							<img width="850" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/119.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How can I download my referrals?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the Profile dropdown. Click on ‘My Account’ and select ‘My Referral Programme’.</br>
							2.	On the My Referral Programme page, select the entry you would like to download by ticking the box on the LHS.</br>
							3.	Hover over the ‘Bulk Action’ drop down and download the selected referrals.</br>
							<img width="850" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/120.png" alt=""></img>
							</p></span></div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I add my bank account details for my referral programme?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the Profile dropdown. Click on ‘My Account’ and select ‘My Referral Programme’.</br>
							2.	Click on ‘Edit My Referral Details’ on the RHS of the page.</br>
							3.	On the Edit My Referral Details page, you can add your account details for your referral programme under bank account details.</br>
							<img width="550" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/118.png" alt=""></img>
							</p></span></div>
						</li>
					   
					   <li class="row document-box-view redborder">
                         <div class="collapsible-header">I didn’t get an email with an OTP while signing up</div>
                        <div class="collapsible-body"><span><p>Usually, this problem should not happen but if it does, do check your spam box</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I access my uploaded documents in Xebra?</div>
                        <div class="collapsible-body"><span><p>You can access your documents through Document Locker.</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Will it be safe to upload my legal or company document here?</div>
                        <div class="collapsible-body"><span><p>Yes, your document will be highly secure as we host those on google cloud for uploading documents through Xebra.</p></span></div>
                      </li>
					  
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">When is a referral considered successful?</div>
                        <div class="collapsible-body"><span><p>For a referral to be successful, the user has to undertake a paid monthly or annual subscription of Xebra. Also at the time of payment, he has to punch in your referral code</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will the referral be considered valid if the user subscribes without entering the code?</div>
                        <div class="collapsible-body"><span><p>No, the user has to enter your specific referral code for you to earn your referral fee</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is there a limit on how much I can earn via the referral program?</div>
                        <div class="collapsible-body"><span><p>No, there is no limit. You can refer us to as many businesses as you like
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">When do I get my referral reward?</div>
                        <div class="collapsible-body"><span><p>Your total referral amount will be calculated and summed up for each month. You will be paid in the first week of succeeding month</p></span></div>
                      </li>
					  
                      <li class="row document-box-view redborder icLink">
							<div class="collapsible-header">How can I integrate my ICICI bank account directly with my Xebra portal?</div>
							<div class="collapsible-body">
							<!-- First Method -->
							<span><p>There are two ways to integrate your ICICI bank account with your Xebra portal:</p>
							<p><b>First Method</b>-</br>
								•	Go to your Xebra dashboard and click on the profile icon on the RHS top corner of the page. </br>
								•	Select ‘My Company Profile’ from the dropdown options.
							</p>
							<img width="250" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/90.png" alt=""></img>
							<p>
								•	In the company profile list, hover on the three dots on the RHS of the company name list.</br>
								•	A dropdown will appear where you have to click on ‘edit’.</br>
								•	Click on the ‘+’ sign next to the bank details section on the edit info page. </br>
								•	Fill in your registered ICICI bank details and make sure you re-check all the numbers entered by you.</br>
							</p>
							<img width="350" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/232.png" alt=""></img>
							<p>•	Click on the small green checkbox at the bottom and then click on ‘Save’. </br>
							•	You will receive a notification (shown below) confirming the integration.</p>
							<img width="280" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/233.png" alt=""></img>
							</span>
							<p>
								<b>Second Method</b>-</br>
								•	On your Xebra dashboard, click on your profile icon. Select ‘My Account’ and click on ‘My Wallet’ from the dropdown options.</br>
								<img width="230" height="650" src="<?php echo base_url(); ?>public/images/faqs-images/234.png" alt=""></img></br>
								•	Click on ‘Add Money’ on the RHS corner of the screen</br>
								•	On the add money page, fill in the amount you want to add and select ICICI Bank in the mode of transfer (as shown below) and click on the ‘Add Money’ button.
								<img width="450" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/235.png" alt=""></img></br>
								•	A popup will appear where you will need to fill in your registered ICICI bank details </br>
								<img width="350" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/232.png" alt=""></img></br>
								•	Click on the small green checkbox at the bottom and then click on ‘Save’. </br>
								•	You will receive a notification (Shown below) confirming the integration.</br>
								<img width="280" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/233.png" alt=""></img>
							</p>
							</div>
                       </li>
					  
					  <!--li class="row document-box-view redborder">
                        <div class="collapsible-header">Does the subscription auto-renew at the end of the year?</div>
                        <div class="collapsible-body"><span><p>No, you have to select Renew Subscription from Profile dropdown and select the plan and add-ons you wish to subscribe</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How much referral amount can I earn?</div>
                        <div class="collapsible-body"><span><p>If a user purchases an annual subscription; you will earn Rs. 2500 and the user will get Rs. 2500 as discount
						If a user purchases a monthly subscription; you will earn Rs. 250 and the user will get Rs. 250 as discount
						</p></span></div>
                      </li-->
                    </ul>
                  </div>
				  
				  <div id="test12" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I access every module through the mobile app?</div>
                        <div class="collapsible-body"><span><p>No, only selected modules like Business Insights and set alerts can be accessed. Individuals can also view and download their Expense Voucher, Salary, TDS, Professional Tax, EPF & ESIC</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I make invoices and record sales receipt from the mobile application?</div>
                        <div class="collapsible-body"><span><p>No. You can record those transactions from web interface only</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record expenses or purchases or inventory and payments for the same?</div>
                        <div class="collapsible-body"><span><p>No. You can record those transactions from web interface only</p></span></div>
                      </li>

                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I download the Xebra mobile app?</div>
                        <div class="collapsible-body"><span><p>You can download it for Android from Google Play Store. iOS will be coming soon from Apple App Store</p></span></div>
                      </li>
					</ul>
                  </div>
				  
				  <div id="test15" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I carry out e-invoicing for a credit note?</div>
                        <div class="collapsible-body"><span><p>
						●	Carry out e-invoicing for a credit note, follow the steps below-</br>
						●	On your Xebra dashboard, click on ‘My Sales’ and select ‘Sales Return and Credit Note’.</br>
						●	In your credit note list, hover on the three dots of the selected item.</br>
						●	Select ‘E-invoicing’ from the dropdown options.</br>
						<img width="550" height="230" src="<?php echo base_url(); ?>public/images/faqs-images/1.png" alt=""></img></br>
						●	Xebra will automatically get the e-invoicing done, and your credit note will have a QR code and IRN number printed on it. </br>The green dot at the end of that row turns purple, indicating successful e-invoicing of that particular invoice.</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I select the invoice against which I want to make a credit note?</div>
                        <div class="collapsible-body"><span><p>Go to your invoices list and filter the invoice you would like to select from the filter option.</br>You can click on the invoice no. that you would like to credit note. </p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make a credit note against an international client?</div>
                        <div class="collapsible-body"><span><p>
						●	To carry out E-invoicing for an international client, you need to create a new profile for your client.</br>
						●	To learn how to create a new international client profile, click here.</br>
						●	Once you have created your international client profile.</p>
						<p>You can record credit note against an invoice in two ways:</br>
						●	Shortcut:</br>Go to the list section of billing documents. Click on Action three dots of the invoices on which you want to create the credit note. </br>Select ‘Credit note’, and you will be taken to create a credit note section.</br>
						<img width="550" height="230" src="<?php echo base_url(); ?>public/images/faqs-images/2.png" alt=""></img></br>
						●	Longer approach:</br>
						●	Click on ‘Credit Note’ in ‘My Sales’ modules. </br>
						●	Select ‘Add New Credit Note’. </br>
						●	Select the appropriate client and the invoice number against which you want to create the credit note.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/3.png" alt=""></img></br>
						●	Select the reason for the credit note and items on which you want to raise the credit note.
						
						</p></span></div>
                    </li>

                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">Do I have to record the accounting effects separately for each credit note?</div>
                        <div class="collapsible-body"><span><p>No, you do not need to record the accounting effects separately for each client.</br> Xebra auto adds all the accounting details as and when you record them.</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I delete a particular row from the table?</div>
                        <div class="collapsible-body"><span><p>
						●	If you are creating a new credit note (to learn how to create a new credit note, click here), fill in your relevant billing details.</br>
						●	To delete a row from a Credit Note that is already created, click on ‘Edit’. </br>
						●	In the credit notes table, select the line item and hover on the amount section. A pink ‘X’ button will appear. </br>Click on it to delete the row that will be deleted from the table and click on ‘Save’.</br>
						<img width="700" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/4.png" alt=""></img>
						</p></span></div>
                    </li>
					
					</ul>
                  </div>
				  
				  <div id="test16" class="col s12 inner_row_container">
                    <ul class="collapsible">
                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record a debit note against a particular expense or purchase?</div>
                        <div class="collapsible-body"><span><p>
						1.	Go to your Xebra dashboard and click on ‘My Purchases and Expenses’ and then select ‘Purchase Return and Debit Notes’.</br>
						2.	Click on ‘Add new Purchase Return and Debit Note.’ </br>
						<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/5.png" alt=""></img></br>
						3. You will be directed to a new create purchase return and debit note page. </br>
						<img width="600" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/6.png" alt=""></img></br>
						4. Fill in the expense/ purchase name and reason for the debit note from the dropdown options.</br>
						5. You can also add in the purchase return and debit note details. You have an option to add the Equalisation Levy charge by clicking on the green checkbox. </br>
						6. After filling in all the relevant details, click on ‘Save’. 
						</p></span></div>
                      </li>
                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make a debit note for an international vendor?</div>
                        <div class="collapsible-body"><span><p>
						1.	To create a debit note for an international vendor, you need to create an international vendor profile. To learn how to create a new international vendor, click here.</br>
						2.	Next, you go to your Xebra dashboard, click on ‘My Purchases and Expenses’, and then select ‘Purchase Return and Debit Notes’ from</br> the dropdown options. Click on ‘Add New Purchase Return and Debit Note’ option on the RHS corner of the screen.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/7.png" alt=""></img></br>
						3.	You will be directed to a new create purchase return and debit note page.</br>
						<img width="750" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/8.png" alt=""></img></br>
						4.	Fill in the expense/ purchase name and reason for the debit note from the dropdown options.</br>
						5.	You can also add in the purchase return and debit note details. You have an option to add the equalisation levy charge by clicking on the tiny green checkbox. </br>
						6.	After filling in all the relevant details, click on ‘Save’.
						</p></span></div>
                      </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit and delete a particular debit note?</div>
                        <div class="collapsible-body"><span><p>
						To edit a particular debit note, follow the steps below-</br>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Purchase Return and a Debit Note’.</br>
						2.	In your debit notes list, hover on the three dots next to the relevant expense/ purchase. A drop down will appear.</br>
						<img width="700" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/9.png" alt=""></img></br>
						3.	Click on ‘Edit’. You will be directed to the edit purchase return and debit note page.</br>
						4.	Make the desired changes and click on ‘Save’.</br>
						To delete a particular debit note, follow the steps below-</br>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Purchase Return and Debit Note’.</br>
						2.	In your purchase return and debit note list, hover on the three dots next to the relevant expense/ purchase. A drop down will appear. Click on ‘Delete’.</br>
						<img width="350" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/10.png" alt=""></img></br>
						3.	Click on delete on the confirmation pop-up that will appear.
						</p></span></div>
                    </li>
                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the Xpress option, and how do I use it?</div>
                        <div class="collapsible-body"><span><p>Xpress is a superfast way of sending a document to your client who is also using Xebra. You don't have to email your invoices to them but simply Xpress them. When you Xpress an invoice or credit note, then your client receives it immediately.</p>
						<p>If the client is not using Xebra, then the feature will be inactive. You could invite that client to start using Xebra and become a part of our referral program.</br> 
						How to use it</br>
						Click on ‘My Sales’ and select ‘Billing Documents’. Hover on Action 3 dots of the invoice that you want to Xpress deliver. Click on ‘Xpress’.
						</p>
						<p>If your client is not a Xebra user, you will see the below notification on the top of your home page.</br><img width="350" height="50" src="<?php echo base_url(); ?>public/images/faqs-images/11.png" alt=""></img></br>
						If your client is also a Xebra user, then he will get your invoice instantly.</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record a reference expense number and purchase order number?</div>
                        <div class="collapsible-body"><span><p>
						Click here to learn how to add the reference expense number.</br>
						Click here to learn how to add the reference purchase order number.
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add an equalization levy to a debit note?</div>
                        <div class="collapsible-body"><span><p>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and then select ‘Purchase Return and Debit Notes’. Click on ‘Add New Purchase Return and Debit Note’ option on the RHS of the screen.</br>
						Click here to learn how to add the reference purchase order number.
						</br><img width="350" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/12.png" alt=""></img></br>
						If you want to add an Equalisation Levy charge to a debit note that has already been created, edit the debit note. To learn how to edit a debit note, click here.</br>
						2.	You will be directed to a new create purchase return and debit note page.</br>
						<img width="650" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/13.png" alt=""></img></br>
						3. Fill in the expense/ purchase name and reason for the debit note from the dropdown options.</br>
						4. You can also add the purchase return and debit note details by selecting the expense/ purchase item from the dropdown options. </br>
						5. To add the Equalisation Levy charge, click the green checkbox below the purchase return and debit note details. A pop up will appear. </br>
						6. Fill in the Equalisation Levy details and click on ‘Save’. You can also set this charge as default by clicking on the green checkbox at the LHS bottom of the pop-up.</br>
						7. Your charges will auto add and appear in the purchase return and debit note details.</br>
						<img width="650" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/14.png" alt=""></img></br>
						8. After filling in all the relevant details, click on ‘Save’. 
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I delete a particular row from the table?</div>
                        <div class="collapsible-body"><span><p>
						1.	If you are creating a new debit note (to learn how to create a new debit note, click here), fill in your relevant details in the debit note document.</br>
						OR to delete a row from a debit note that is already created, click on edit (to learn how to edit a debit note, click here) </br>
						2.	In the debit note, go to the specific line item and hover on the amount section. A pink ‘X’ symbol will appear. Click on it, and your row will be deleted from the table. After making all the desired changes, click on ‘Save’.
						</br><img width="650" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/15.png" alt=""></img></br>
						</p></span></div>
                    </li>
					</ul>
                  </div>
				  
				  <div id="test20" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I download, email or delete the documents that I have added to my document locker?</div>
                        <div class="collapsible-body"><span><p>
						●	The only user with an admin profile can delete the documents from the document locker.</br>
						●	On your Xebra dashboard, click on profile and select ‘My Document Locker’ from the dropdown.</br>
						●	Select the document you want to retrieve by using the filter. </br>
						<img width="650" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/16.png" alt=""></img></br>
						●	Hover on three dots and click on ‘Download.’ </br>
						●	Follow the same process for Email & Delete.</br>
						<img width="650" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/17.png" alt=""></img></br>
						</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I get more space in my document locker?</div>
                        <div class="collapsible-body"><span><p>When you have run out of the allotted space, click on profile and select ‘My Account’. Further, click on ‘Subscribe and Shop’ and choose ‘Buy Add-ons’.</br>
						Enter the amount of GB space you want to buy, fill in the user details and carry out the payment check out.
						</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I know how much of my document space have I used up?</div>
                        <div class="collapsible-body"><span><p>
						On the Document Locker table, under the title File Size, you will see the cumulative total size of all the files that you have uploaded. </br>If you have crossed the allotted quota of your plan, it will notify you to buy more space.</p></span></div>
                    </li>

                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">Do I have to pay an annual fee for the online doc locker?</div>
                        <div class="collapsible-body"><span><p>No. You only have to pay a one-time fee for buying more space in your document locker.</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I be able to access my documents from the online docker after my subscription is over?</div>
                        <div class="collapsible-body"><span><p>
						No, you will have to renew your subscription to access your documents.</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I be able to access my documents from the online docker after I have deactivated my account?</div>
                        <div class="collapsible-body"><span><p>
						No, you will have to reactivate your account to access your documents.</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Will anyone else be able to access my documents from the online docker after my subscription is over or after I have deactivated my account?</div>
                        <div class="collapsible-body"><span><p>
						No. Nobody gets accessed to your files. After 90 days of deactivating your account or non-renewal of subscription, your account will be deleted entirely, along with all the documents.</p></span></div>
                    </li>
					</ul>
                  </div>
				  
				  <div id="test18" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record entry in Chart of Accounts?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts.’ </br>
						•	Click on ‘Add New Account.’</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/18.png" alt=""></img></br>
						•	Type in your account name and select the account category from the dropdown options.</br>
						•	Then click on account type and select the relevant entry from the dropdown options.</br>
						•	Fill in your opening balance and select debit or credit.</br>
						•	You can also fill in your notes if you have any in the box given below.</br>
						•	After checking all your details, click on ‘Save’.</br>
						<img width="400" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/19.png" alt=""></img></br>
						</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add a new Account category and account type?</div>
                        <div class="collapsible-body"><span><p>You can write in to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> mentioning the new account category or account type you want added.
						</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit and delete individual entries?</div>
                        <div class="collapsible-body"><span><p>
						To edit an individual entry, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts’.</br>
						•	Hover near the three dots of the entry and click on edit from the dropdown options.</br>
						•	Make the changes and click on ‘Save’.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/20.png" alt=""></img></br>
						To delete an individual entry, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts’.</br>
						•	Hover near the three dots of the entry and click on delete from the dropdown options</br>
						•	On the confirmation pop-up, click on ‘Delete’. </br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/20.png" alt=""></img></br>
						</p></span></div>
                    </li>

                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I export and delete entries in bulk?</div>
                        <div class="collapsible-body"><span><p>To export bulk entries, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts’.</br>
						•	Select the desired entries and hover on the ‘Bulk Actions.’ Click on ‘Export’ from the dropdown options.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/21.png" alt=""></img></br>
						To delete bulk entries, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts.’ </br>
						•	Select the desired entries and hover on the ‘Bulk Actions.’ Click on ‘Delete’ from the dropdown options.</br>
						•	On the confirmation pop-up, click on ‘Delete.’</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/21.png" alt=""></img>
						</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter entries according to revenue?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Chart of Accounts.’ </br>
						•	Hover on the ‘By Account Category’ filter above the list and select ‘Revenue.’ </br>
						•	Similarly, to filter by expense, equity or assets, follow the same process.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/22.png" alt=""></img>
						</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">When do I use a journal voucher?</div>
                        <div class="collapsible-body"><span><p>
						You can use a journal entry to make an adjustment entry, like depreciation entry, prepaid expenses, provisional expenses, income entries, write off entries etc.</p></span></div>
                    </li>
					
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make a journal voucher entry?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Journal Vouchers.’</br>
						•	Click on ‘Add New Journal Voucher.’</br>
						•	Then select the type of account from the given dropdown options, and add your debit/ credit amounts.</br>
						•	To add another row, click on the ‘+’ sign next to the created entry.</br>
						•	Add in the narration and click on ‘Save.’</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new account name?</div>
                        <div class="collapsible-body"><span><p>
						You can write in to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> mentioning the new account category or account type you want added in Xebra.</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I make an international journal voucher entry?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Journal Vouchers.’</br>
						•	Click on ‘Add New Journal Voucher.’</br>
						•	In the create page, select the relevant currency from the dropdown. E.g. USD- Dollars.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/23.png" alt=""></img></br>
						•	Then select the type of account from the given dropdown options and add your debit/ credit amounts.</br>
						•	To add another row, click on the ‘+’ sign next to the created entry.</br>
						•	Add in the narration and click on ‘Save.’</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">If I pass a journal voucher entry, do I have to update the accounting report manually?</div>
                        <div class="collapsible-body"><span><p>
						No. Xebra has automated the accounting aspect of the journal voucher entry. You simply need to pass your entry, and accordingly, general ledgers, trial balance, profit & loss and balance sheet will get updated.</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my data from last year in my Journal Vouchers?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Journal Vouchers.’ </br>
						•	At the top of the Journal Vouchers list, you will see the start and end date icons.</br>
						•	Select your relevant year and month, click on ‘Submit’.</br>
						•	Your last year data will be filtered and visible.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/24.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my journal vouchers from Brazil?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Journal Vouchers.’ </br>
						•	At the top of the Journal Vouchers list, you will see the start and end date icons, along with the country/ state filter options.</br>
						•	Click on the ‘All’ option, and select the relevant country from the dropdown.</br>
						•	Similarly, follow the same process to filter entries by state.</br>
						</p></span></div>
                    </li>
					<!-- GENERAL LEDGER -->
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Why am I unable to make and edit ledgers?</div>
                        <div class="collapsible-body"><span><p>
						Xebra has automated entire accounting. So, whenever you create an invoice or record any expense or payment, ledgers are created automatically and updated with those amounts.
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I email, export or print general ledgers?</div>
                        <div class="collapsible-body"><span><p>
						To email ‘General Ledgers’, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘General Ledgers.’ </br>
						•	Hover on ‘Bulk Actions’ and click on ‘Email.’</br>
						•	Follow similar steps for Export and Print of General Ledgers.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/25.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">I spotted an anomaly in the General Ledgers. How do I rectify it?</div>
                        <div class="collapsible-body"><span><p>
						You can write in to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> mentioning the anomaly that you have spotted. Our customer support team will discuss and resolve this on priority.</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my data from last year, in general ledgers?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘General Ledgers.’ </br>
						•	At the top of the list of the general ledger, you will see the start and end date icons.</br>
						•	Select your relevant year and month, click on ‘Submit’.</br>
						•	Your last year data will be filtered and visible. </br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/26.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<!-- Trial Balance -->		
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Why am I unable to edit and make a trial balance?</div>
                        <div class="collapsible-body"><span><p>
						Xebra has automated entire accounting. So, whenever you create an invoice or record any expense or payment, Xebra automatically updates the trial balance with those amounts.</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I email, export or a print trial balance?</div>
                        <div class="collapsible-body"><span><p>
						To email the ‘Trial Balance’, follow the steps below:</br>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Trial Balance.’ </br>
						•	Hover on ‘Bulk Actions’ and click on ‘Email’.</br>
						•	Follow similar steps for Export and Print of Trial Balance.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/27.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">I spotted an anomaly in the Trial Balance. How do I rectify it?</div>
                        <div class="collapsible-body"><span><p>
						You can write in to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> mentioning the anomaly that you have spotted. Our customer support team will discuss and resolve this on priority.
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my data from last year in my trial balance?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Trial Balance.’ </br>
						•	At the top of the trial balance sheet, you will see the start and end date icons.</br>
						•	Select your relevant year and month, click on ‘Submit’.</br>
						•	Your last year data will be filtered and visible. </br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/28.png" alt=""></img></br>
						</p></span></div>
                    </li>
					
					<!-- Accounting Reports -->
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my data from last year in my trial balance?</div>
                        <div class="collapsible-body"><span><p>
						Xebra has automated entire accounting. So, whenever you create an invoice or record any expense or payment, Xebra automatically updates the Profit & Loss and Balance Sheet statements.
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I email, export or print the Profit and Loss Statement and Balance Sheet?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Accounting Reports.’</br> 
						•	You can either select Profit and Loss or Balance Sheet.</br>
						•	Hover on ‘Bulk Actions’ and click on ‘Email’.</br>
						•	Follow similar steps for Export and Print of Trial Balance.</br>
						<img width="800" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/29.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I upload and retrieve my documents?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Accounting Reports.’ </br>
						•	You can either select Profit and Loss or Balance Sheet.</br>
						•	Go to the bottom of your page, where you will see ‘Upload Documents’.</br>
						<img width="600" height="100" src="<?php echo base_url(); ?>public/images/faqs-images/30.png" alt=""></img></br>
						To retrieve your uploaded documents:</br>
						●	Click on the profile dropdown in the top right-hand corner of the screen and click on ‘My Document Locker’ within that.</br>
						<img width="300" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/31.png" alt=""></img></br>
						●	You can then select an appropriate filter and download the specific document you require.</br>
						<img width="600" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/32.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">I spotted an anomaly in the Profit & Loss statement. How do I rectify it?</div>
                        <div class="collapsible-body"><span><p>
						You can write in to <a style="color:#7864e9 !important; text-decoration:none; padding-top:5px;" href="mailto:support@xebra.in">support@xebra.in</a> mentioning the anomaly that you have spotted. Our customer support team will discuss and resolve on a priority</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">In the Profit and Loss statement, what does the total cost of goods include?</div>
                        <div class="collapsible-body"><span><p>
						Total cost of goods sold, include the ‘Opening stock + Purchases - Closing Stock’.</br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I filter my data from last year in my accounting reports?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Accounting Reports.’ </br>
						•	You can either select Profit and Loss or Balance Sheet.</br>
						•	At the top of the sheet, you will see the start and end date icons.</br>
						•	Select your relevant year and month, click on ‘Submit’.</br>
						•	Your last year data will be filtered and visible.</br>
						<img width="800" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/33.png" alt=""></img></br>
						</p></span></div>
                    </li>
					<li class="row document-box-view redborder">
                        <div class="collapsible-header">Where can I find my profit and loss amount before tax?</div>
                        <div class="collapsible-body"><span><p>
						•	On your Xebra dashboard, click on ‘My Accounts’ and select ‘Accounting Reports.’ </br>
						•	Select the Profit and Loss Sheet.</br>
						•	Scroll to the bottom of the page, where you will see your profit and loss amount before tax.</br>
						•	Xebra has automated entire accounting. So, whenever you create an invoice or record any expense or payment, Xebra automatically updates the Profit & Loss and Balance Sheet statements. </br>
						<img width="700" height="150" src="<?php echo base_url(); ?>public/images/faqs-images/34.png" alt=""></img></br>
						</p></span></div>
                    </li>
					</ul>
                  </div>
				  
				  <div id="test17" class="col s12 inner_row_container">
                     <ul class="collapsible">
						<li class="row document-box-view redborder">
							<div class="collapsible-header"><p><strong>Import Data</strong></br>Which modules of data can I import from my existing software?</p></div>
							<div class="collapsible-body"><span>
								<p>1.	On your Xebra dashboard, click on the setting icon and select ‘Import Data’</br>
								   2.	You will get an option to download the following modules</br>
									•	Client Master</br>
									•	Item Master</br>
									•	Vendor Master</br>
									•	Expense Master</br>
									•	Invoice</br>
									•	Credit Notes</br>
								<img width="800" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/177.png" alt=""></img></br></p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I import data from any kind of software?</div>
							<div class="collapsible-body"><span><p>No, you will have to import your data in an excel format in the ‘Import Data’ section in Settings.</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Where do I download the excel sheet meant for importing data?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the setting icon and select ‘Import Data’</br>
							2.	On the Import Data page, choose the module you would like to work on.</br>
							3.	When the page opens, you will get the option ‘Download Sample File.’</br>
							4.	Fill in your details on that file and upload the excel sheet.</br>
							<img width="800" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/178.png" alt=""></img></br></p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I begin importing data for invoice and credit note modules first?</div>
							<div class="collapsible-body"><span><p>No, you will have to first import data to your Client Master, Item Master and Vendor Master before importing data for your Invoice and Credit Note Module.</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I import a client / vendor / item / expense, invoices and credit and debit notes?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on the setting icon and select ‘Import Data’.</br>
							2.	On the Import Data page, choose the module you would like to work on.</br>
							3.	When the module page opens, you will get the option ‘Download Sample File.’</br>
							4.	Fill in your details on that file and click on the ‘cloud’ icon to upload the excel sheet.</br>
							5.	After uploading, you will be directed to the Map Data to see the fields you have filled out. Click on ‘Next’ and Import your file.</br>
							<img width="800" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/178.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Can I add a row or column to the excel sheet that you have given?</div>
							<div class="collapsible-body"><span><p>No, please do not add any rows or columns or make any formatting changes to the sheet.
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">Is there a limit to how many entries I can import for each module?</div>
							<div class="collapsible-body"><span><p>Yes, you can import only 1000 entries at a single time for every individual module.
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"><p><strong>Customize</strong></br>How do I customize my client and vendor portal?</p></div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings dropdown and select ‘Customize’ option.</br>
							2.	You will see the ‘Client Portal’ and ‘Vendor Portal’ option on the Portals and Profiles page.</br>
							3.	Click on the portal that you would like to customize and click ‘Save’ when done.</br>
							<img width="800" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/179.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I customize email copy from each module?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings dropdown and select ‘Customize’ option.</br>
							2.	At the LHS of the screen, you will see options to customize your emails for each module, such as Sales & Exp Emails, My Tax Emails, Receipts & Payments, My Employee Emails, My Asset Emails, My Company Emails, Doc. Locker Emails, Subscription Emails and Alert Emails.</br>
							3.	Select the module you would like to customize and click ‘Save.’</br>
							<img width="100" height="800" src="<?php echo base_url(); ?>public/images/faqs-images/180.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I customize my public profile?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings dropdown and select ‘Customize’ option.</br>
							2.	On the Portals and Profiles page, click on the ‘Public Profile’ option.</br>
							3.	On the Public Profile option, customize your URL and click ‘Save’ when done.</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/181.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I make my profile private or public?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings dropdown and select ‘Customize’ option.</br>
							2.	On the Portals and Profiles page, click on ‘Public Profile’.</br>
							3.	On the Public Profile page, you will get the option ‘Make your Public Profile’, turn on that option and click ‘Save.’</br>
							<img width="900" height="350" src="<?php echo base_url(); ?>public/images/faqs-images/181.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"><p><strong>Set Alerts For</strong></br>Why am I not able to edit the Alert date?</p></div>
							<div class="collapsible-body"><span><p>The alert date in the first row of the create/edit section is meant to record the date you set the alert on and hence is un editable.</br>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What is meant by an alert name?</div>
							<div class="collapsible-body"><span><p>Alert name helps you in recognizing an alert in the scenario of multiple alerts being set up.</br>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What does setting an alert for a client and vendor mean? </div>
							<div class="collapsible-body"><span><p>This alert helps you determine if you are on track to achieve the target set for a particular client. So, you can place an alert for 30th June for 1/4th of your target number and compare it with actual sales. This will help you get a clear picture client-wise and carry out any course correction if required. </br>Vice versa for vendor.
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">What does setting an alert for an item and expense mean? </div>
							<div class="collapsible-body"><span><p>This alert helps you determine if you are on track to achieve the target set for a particular product or service of yours. So, you can set an alert for 30th June for 1/4th of your target number and compare it with actual sales. This will help you get a clear picture item-wise and carry out any course correction if required. </br>Vice versa for expense.
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I set a client/vendor alert?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert For’ dropdown. Click on ‘Client and Vendor’.</br>
							2.	On the Client and Vendor page, click on ‘Add New Alert’ on the RHS of the page.</br>
							3.	Fill in Name your Alert, select the client/vendor, add your alert info and click ‘Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/182.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I set an item / expense alert?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert For’ dropdown. Click on ‘Item/Expense’.</br>
							2.	On the Items/Expense Alert page, click on ‘Add New Alert’.</br>
							3.	Fill in Alert Name, select the Item/Expense, add your alert info and click ‘Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/183.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I set an alert for credit period?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert For’ dropdown. Click on ‘Credit Period’.</br>
							2.	On the Credit Period Alert page, click on ‘Add New Alert’.</br>
							3.	Fill in Alert Name, Client Name, add in your alert info and click ‘Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/184.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I set an alert for birthday and anniversary?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert For’ dropdown. Click on ‘Birthday & Anniversary.’</br>
							2.	On the Birthday & Anniversary Alert page, click on ‘Add New Alert’.</br>
							3.	Select the occasion, Stakeholder, Occasion Date, alert information and click ‘Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/185.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"> For which stakeholders can I set a birthday and anniversary alert?</div>
							<div class="collapsible-body"><span><p>You will be given an option to select from your Employee, Client, and Vendors.</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/186.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"> How do I set an alert for an activity?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert For’ dropdown. Click on ‘Activity/Task’.</br>
							2.	On the Activity/Task Alert page, click on ‘Add New Alert.’</br>
							3.	Label your Activity, select the Date and Time, add in your alert info and click ‘Save.’</br>
							<img width="500" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/187.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"> What are the different types of alerts that are available?</div>
							<div class="collapsible-body"><span><p>There are three types of alerts:</br>
							•	If you want to be notified on the Xebra platform, click on the ‘Bell’ icon</br>
							•	If you want to be notified via message, click the ‘Message’ icon and fill in your mobile number</br>
							•	If you want to be notified via email, click the ‘Email’ icon and fill in your email id.</br>
							•	You can also replace your number or email with that of your colleague in case you want them to be notified about it</br>
							<img width="500" height="100" src="<?php echo base_url(); ?>public/images/faqs-images/188.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header"> How do I edit, export, email and print and delete an alert?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert’ from the dropdown options.</br>
							2.	From the page alerts you have chosen, select the entry you want to edit, export, email, print, and delete.</br>
							3.	Hover over the three dots, and you will see the options to edit, export, email, and print.</br>
							<img width="900" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/189.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">How do I export, email and print alerts in bulk?</div>
							<div class="collapsible-body"><span><p>1.	On your Xebra dashboard, click on your settings icon and select ‘Set Alert’ from the dropdown options.</br>
							2.	From the page alerts you have chosen, select the entry you want to edit, export, email, print, and delete in bulk.</br>
							3.	Select the entries you would like to edit, export, email, print, and delete. Hover over the Bulk Action dropdown and choose the action you would like to take.</br>
							<img width="900" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/190.png" alt=""></img>
							</p></span>
							</div>
						</li>
						<li class="row document-box-view redborder">
							<div class="collapsible-header">I am unable to enter the mobile number or the email id. What do I do?</div>
							<div class="collapsible-body"><span><p>You must first select email or mobile in the type of alert for that input box to get activated.</p></span>
							</div>
						</li> 	

					  </ul>  
				</div>  
				  
				  <div id="test19" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new salary expense?</div>
                        <div class="collapsible-body"><span><p>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ select ‘Salary Expense.’
						2.	Click on ‘Add New Salary Expense.’</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/139.png" alt=""></img></br>
						3.	You will be directed to the create salary expense page. Fill in the details like the employee ID and name. Basis that, Xebra will auto-fill the other employee details.</br>
						<img width="600" height="500" src="<?php echo base_url(); ?>public/images/faqs-images/140.png" alt=""></img></br>
						4.	Further, if you would like to add any additional remunerations, click on the ‘+’ sign, and fill in the details. 
						5.	If you would like to add in the deductions, click on the ‘+’ sign, and fill in the details. After checking all the salary expense details, click on ‘Save’.</br></p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a salary slip for each employee?</div>
                        <div class="collapsible-body"><span><p>
						1.	To create a salary slip for each employee, go to your Xebra dashboard and click on ‘My Purchases and Expenses’. Select ‘Salary Expenses’.</br>
						2.	Hover on the RHS corner of the desired salary expense in the list. A dropdown will appear. Click on ‘Download’.</br>
						3.	A PDF will be downloaded for your desired employee and saved to your desktop.</br>
						<img width="700" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/141.png" alt=""></img></br></p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record a payment against a single salary expense?</div>
                        <div class="collapsible-body"><span><p>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Salary Expenses’. </br>
						2.	Select the specific ‘Salary Expense’ in the table and hover on the three dots. Click on ‘Payment’ from the dropdown.</br> 
						3.	You will be directed to the ‘Add New Salary Payment’ page. Fill in the details like employee ID and name, expense amount, advance is taken, and any other expenses.</br>
						<img width="500" height="400" src="<?php echo base_url(); ?>public/images/faqs-images/142.png" alt=""></img></br>
						4.	Further fill in the payment details like the mode of payment, payment amount, TDS, bank name and any other relevant information. You can also add notes if you have any. 
						5.	After checking all the details, click on ‘Save’.</br>
						<img width="500" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/143.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I edit and deactivate a particular salary expense?</div>
                        <div class="collapsible-body"><span><p>
						<strong>To edit a salary expense, follow the steps below:</strong></br>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Salary Expenses.’ </br>
						2.	In your salary expenses list, hover on the three dots next to the relevant expense. Click on ‘Edit’ from the dropdown.</br>
						3.	Make the desired changes and click on ‘Save’.</br></br>
						<strong>To deactivate a salary expense, follow the steps below:</strong></br>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’ and select ‘Salary Expense.’ </br>
						2.	In your company expense list, hover on the three dots next to the relevant expense. A drop down will appear, click on ‘Deactivate’.</br>
						3.	A confirmation pop up will appear. Select ‘Deactivate’.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I keep track of loan taken by an employee?</div>
                        <div class="collapsible-body"><span><p>
						1. If your company has extended a loan/advance to an employee, it will be recorded in Salary Expense Module. In the create ‘Salary Expense’ section, click on ‘Additions’ and select ‘Loan’ from the dropdown options.</br> 
						2. Once you ‘Save’ the entry, you will be able to track the loan amount for that employee on your list view, as shown below.</br>
						<img width="900" height="250" src="<?php echo base_url(); ?>public/images/faqs-images/144.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I set recurring line items in Additions and Deductions?</div>
                        <div class="collapsible-body"><span><p>
						1. On your Xebra dashboard, click on ‘My Purchases and Expenses’, select ‘Salary Expenses’. 
						On the RHS of the screen, click on ‘Add New Salary Expense’. </br>
						OR, if you would like to add recurring line items in additions and deductions in a salary expense that is already created, click on ‘Edit’ (To learn how to edit a salary expense, click here).</br>
						2. You will be directed to the create salary expense page. Fill in the details like the employee ID and name. Basis that, Xebra will auto-fill the other employee details.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/140.png" alt=""></img></br>
						3. Further, if you would like to add any additional remunerations, click on the ‘+’ sign, and fill in the details. To make it a Recurring Additional remuneration, click on the green checkbox at the bottom of the pop-up, and click on ‘Save’.</br>
						4. Similarly, if you would like to add in the deductions, click on the ‘+’ sign, and fill in the details as shown in the image. To make it a Recurring Deduction, click on the green checkbox at the bottom of the pop-up, and click on ‘Save’.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/145.png" alt=""></img></br>
						After checking all the salary expense details, click on ‘Save’.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I create a new item in Addition or Deduction that is not mentioned in the dropdown?</div>
                        <div class="collapsible-body"><span><p>
						We have covered as many items in Addition and Deduction pop-up boxes that we could think of.</br>
						If you realise that we have missed out on any more generic options, then do email us at support@xebra.in with your suggestion.</br>
						We will indeed look into it and consider the option. Either way, you will hear from us.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">The addition and deduction pop-up show only four rows. How do I record more than four items?</div>
                        <div class="collapsible-body"><span><p>
						Let’s say you have more than 4 line items for the Addition part of the salary slip. </br>
						1. Click on ‘+’ sign in Addition and record the four items.</br>
						2. Then click on the ‘+’ symbol of Addition again to record more line items.</br>
						3. Carry out a similar process for Deduction also.</br>
						OR, if you would like to add recurring line items in additions and deductions in a salary expense that is already created, click on ‘Edit’ (To learn how to edit a salary expense, click here).</br>
						You will be directed to the create salary expense page. Fill in the details like the employee ID and name. Basis that, Xebra will auto-fill the other employee details.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/140.png" alt=""></img></br>
						Further, if you would like to add any additional remunerations, click on the ‘+’ sign, and fill in the details. To make it a recurring additional remuneration, click on the green checkbox at the bottom of the pop-up, and click on ‘Save’.</br>
						Similarly, if you would like to add in the deductions, click on the ‘+’ sign, and fill in the details as shown in the image. To make it a recurring deduction, click on the green checkbox at the bottom of the pop-up, and click on ‘Save’.</br>To add more than four rows in the additions or deductions, click on the ‘+’ sign and repeat the process.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I change the items in ‘Addition or Deduction’ only for a particular month?</div>
                        <div class="collapsible-body"><span><p>
						You can set recurring line items in the salary slip of any employee by selecting checkbox ‘Set as Default’. However, if a particular line item is going to appear just once, then do not click on the checkbox, and it will occur only for that month.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record the salary expenses of an employee stationed internationally?</div>
                        <div class="collapsible-body"><span><p>
						To record the salary of an employee stationed internationally, make an employee profile with all the local international details of the employee.</br>
						Then go on to create a new salary expense with the below-mentioned steps- </br>
						1.	On your Xebra dashboard, click on ‘My Purchases and Expenses’, select ‘Salary Expenses’.  On the RHS of the screen, click on ‘Add New Salary Expense’.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/139.png" alt=""></img></br>
						2.	You will be directed to the create salary expense page. Fill in the details like the employee ID and name. Basis that, Xebra will auto-fill the other employee details.</br>
						3.	In the case of an international employee, his currency will appear in their local currency. </br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/140.png" alt=""></img></br>
						4.	Further, if you would like to add any additional remunerations, click on the ‘+’ sign, and fill in the details. </br>
						5.	If you would like to add in the deductions, click on the ‘+’ sign, and fill in the details. After checking all the salary expense details, click on ‘Save’.
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record salary in multiple currencies?</div>
                        <div class="collapsible-body"><span><p>
						Go to Employee Master and edit the entries of those employees that are based in international locations. </br>
						Click on ‘Edit’ and change the currency from the dropdown options. Now the salary for that employee will be recorded in that new currency.</br>
						<img width="500" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/146.png" alt=""></img></br>
						</p></span></div>
                      </li>
					  
					  <li class="row document-box-view redborder">
							<div class="collapsible-header">How can I make a salary expense payment directly from my Xebra portal?</div>
							<div class="collapsible-body">
								<p>
									•	On your Xebra dashboard, click on ‘My Purchases & Expenses’ and then click on ‘Salary Expenses’.</br>
									<img width="250" height="320" src="<?php echo base_url(); ?>public/images/faqs-images/249.png" alt=""></img></br>
									•	From the list of salary expenses, hover on the three dots next to the one you would like to make the payment against.</br>
									•	A dropdown will appear where you click on ‘Payment’. </br>
									<img width="850" height="450" src="<?php echo base_url(); ?>public/images/faqs-images/250.png" alt=""></img></br>
									•	An OTP Confirmation pop up will appear (please note- this feature of OTP confirmation will only appear If you have already integrated your ICICI bank account with Xebra).</br>
									<img width="950" height="465" src="<?php echo base_url(); ?>public/images/faqs-images/240.png" alt=""></img></br>
									•	To learn how to integrate your ICICI account directly with Xebra, <a class="bColor ohhh">click here</a>.</br>
									•	After putting in the correct OTP, you will be directed to a create payment page to verify the amount and click on ‘save’.</br> 
									<img width="400" height="200" src="<?php echo base_url(); ?>public/images/faqs-images/241.png" alt=""></img></br>
									•	All the details on the create payment page will already be pre-filled, except the payment status. </br>
									•	Click on the arrow sign, and under payment status- make sure you fill in the correct amount received (full/half/part).</br>
									•	Check all the other details correctly and then click on ‘Save’.</br>
									•	You will receive a confirmation notification for the same.</br>
									<img width="350" height="55" src="<?php echo base_url(); ?>public/images/faqs-images/242.png" alt=""></img></br>
									•	Additionally, you can also check if the payment has been recorded by clicking on ‘My Receipts and Payments’ and selecting ‘Payments’ from the dropdown options.
									•	Under payments, click on ‘Salary Payments’ from the dropdown options.</br>
									<img width="250" height="300" src="<?php echo base_url(); ?>public/images/faqs-images/246.png" alt=""></img></br>
									•	You will be able to view your recorded payment on the RHS of the screen.
								</p>
							</div>
						</li>
					  
					</ul>
				  </div>
                </div>
              </div>

        </section>
        <!-- END CONTENT -->
        </div>

        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->


      <!-- ================================================
    Scripts
    ================================================ -->
	<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
<?php $this->load->view('template/footer'); ?>
