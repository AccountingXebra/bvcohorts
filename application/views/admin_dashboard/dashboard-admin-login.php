
<?php $this->load->view('client_portal/client-portal-header'); ?>
<script src="https://www.google.com/recaptcha/api.js"></script>
 <style type="text/css">

	.border-split-form .select-wrapper{
		padding: 7px 0 2px 0 !important;
        top: 0px !important;
	}
	
	.li-autocomplete{
		padding: 2px 0px 2px 20px !important;
		box-shadow: 0 0 1px 0 #bbb !important;
		background-color: #fff !important;
	}
  
	.ul-autocomplete{
		z-index: 99999 !important;
	}
	
	.select-dropdown {
		font-size: 14px !important;
	}
	
	/*.select-wrapper label.error:not(.active) {
		margin: -30px 0 0 -11px;
	}*/
	
	label.full-bg-label {
	  color: #666;
	}
	
	.input-field label.select-label{
	  color: #666;
	}

	/*---Dropdown error message format---*/
		.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -20px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	/*---End dropdown error message format---*/

	.btn-display{
		display: contents;
	}
	
	.adjust-width{
		border-bottom:none !important;
	}
	
	.chkbox2{
		float:left !important;
		margin: 10px 0 0 0;
	}
	
	a.addmorelink{
		padding-top: 10px;
		text-decoration: none;
	}
	
	.footer-btns{
		margin-top:0px !important;
	}
	
	#super_admin_login .remme{
		width:555px !important;
		margin-left:-5%;
	}
	.g-recaptcha{ margin:0 0 0 -10px; }
</style>

	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
        <!-- START CONTENT -->
             
		<form class="create-company-form border-split-form" name="super_admin_login" id="super_admin_login" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Admin_dashboard/check_login">
		  <section id="content">
			<div class="container">
			  <div class="plain-page-header">

				<div class="page-content">
				  <div class="row" style="margin-top:3% !important;">
					<div class="col s12 m12 l4"></div>
					<div class="col s12 m12 l8 remme">
						<div class="row">
							<div class="col s12 m12 l12">
								<h5 class="text-center">Please sign in to enter</h5><br>
								<p class="text-center"></p>
							</div>
						</div>
						<?php $user1=$this->input->cookie('admin_email',true); $pass1=$this->input->cookie('admin_password',true);
                     
            ?>
						<div class="row">
						<div class="box-wrapper bg-white bg-img-green shadow border-radius-6">

						  <div class="box-body">
							<div class="row">
							  <div class="col s12 m12 l12">
								<div class="input-field">
								  <label for="ad_email" class="full-bg-label">EMAIL ID<span class="required_field"> *</span></label>
								  <input id="ad_email" name="ad_email" value="<?=$user1?>" class="full-bg adjust-width" type="text" autocomplete="off">
								</div>
							  </div>
							</div>
							<div class="row">
							  <div class="col s12 m12 l12">
								<div class="input-field">
								  <label for="password" class="full-bg-label">PASSWORD<span class="required_field"> *</span></label>
								  <input id="password" name="password"  class="full-bg adjust-width " type="password" autocomplete="off">
								</div>
								<div class="hide-show-11">
									<span class="show-11-monkey" style="position:absolute; left:90%; top:65%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
								</div>
							  </div>
							</div> 
						  </div>
						</div>
					  </div>
					  <div class="row">
						<div class="drop-down">
							<div class="chkbox2">
								<input type="checkbox" id="remeber_me" name="remeber_me">
								<label class="checkboxx2" for="remeber_me"><span class="check-boxs">Remember me</span></label>
							</div>
							<!-- <a class="addmorelink" href="" style="float: right;">Forgot Password?</a> -->
						</div>
					  </div> 
					  <div class="g-recaptcha brochure__form__captcha" data-sitekey="6Ld_6L8ZAAAAADK3VKD6NRB3xB7hfEmwWd8otra0"></div>
					  <div class="row">
						<div class="drop-down">
							<div class="chkbox2">
								
							</div>
							<!-- <a class="addmorelink" href="" style="float: right;">Forgot Password?</a> -->
						</div>
					  </div>
					<div class="step1 footer-btns">
						<div class="row">
						  <div class="right btn-display">
							<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-2 right">SIGN IN</button>
							<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right" type="button"  onclick="location.href = '<?php echo base_url();?>client-portal';">CANCEL</button>
						  </div>
					  </div>
						
					</div>

					</div>	
					<div class="col s12 m12 l2"></div>
				  </div>

				  <div class="row">
					<div class="col s12 m12 l12">
					  <div class="col s12 m12 l12 input-set">
						<div class="row">
						  
						</div>
					  </div>
					</div>
				  </div>
				  <div class="row">
					<div class="col s12 m12 l12">
					  <div class="col s12 m12 l12 input-set">
						<div class="row mr-1 pl-3">
						</div>
					  </div>
					</div>
				  </div>
						
				</div>
			  </div>
			</div>
		  </section>
		  
		</form>


        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
	  
	  <script>
	$(document).ready(function () {
	$('.hide-show-11').show();
	$('.hide-show-11 span').addClass('show')
				 
	$('.hide-show-11 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(".show-11-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#password').attr('type','text');
			$(".show-11-monkey").removeClass('show');
		} else {
			$(".show-11-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#password').attr('type','password');
			$(".show-11-monkey").addClass('show');
		}
	});
	});
</script>
   <?php $this->load->view('template/footer.php');?>