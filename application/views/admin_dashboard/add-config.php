<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
<!-- START MAIN -->
<style type="text/css">

	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}

	.border-split-form .select-wrapper {
		padding: 7px 0 2px 0 !important;
	}
	
	input.full-bg:not(.first)  {
		border-top: 0 !important; 
	}
  
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute; 
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	} 
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px; 
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}
	
	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 0px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:40px !important; 
		line-height: 40px;
	}
	
	.nature_of_code{
		border-right: 1px solid #EEF2FE !important;
	}
	
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -15px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	
	.step2.footer-btns {
		margin: 10.3% 0 0 0 !important;
	}
	
	#add_config .input-field.padd-n label { left: 12px; }
	
</style>

    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" id="add_config" name="add_config" method="post"  enctype="multipart/form-data">
  <section id="content" class="bg-theme-gray documents-search">
    <div class="container">
      <div class="plain-page-header">
        <div class="row">
          <div class="col l6 s12 m6">
            <a class="bg-l underline" href="<?php echo base_url();?>"></a> 
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
         <div class="page-content">
        
            <div class="row" style="margin-top:2%;">
              <div class="col s12 m12 l1"></div>
                <div class="col s12 m12 l10">
                    
                    <div class="row">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-header"> <h3 class="box-title">Site Config</h3> </div>
                        <div class="box-body">
							<div class="row">
								<div class="input-field col s12 m12 l4 padd-n" style="border-top:1px solid #EEF2FE !important; padding: 0 0 0 10px !important;">
									<label for="plan_name" class="full-bg-label">Plan Name<span class="required_field"> *</span></label>
									<input id="plan_name" name="plan_name[]" class="full-bg adjust-width" type="text" value="<?=@$config[0]->plan_name ?>">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding: 0 0 0 0px !important; border-left:1px solid #EEF2FE !important;">
									<label for="plan_value" class="full-bg-label">Plan Value<span class="required_field"> *</span></label>
									<input id="plan_value" name="plan_value[]" class="full-bg adjust-width first border-right" type="text" value="<?=@$config[0]->plan_value?>" style="padding-left: 29px !important; width: 77% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="discount" class="full-bg-label">DISCOUNT % age<span class="required_field"> *</span></label>
									<input id="discount" name="discount[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[0]->discount?>" style="padding-left: 29px !important; width: 57% !important; margin-left: 15px !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referee" class="full-bg-label">REFEREE<span class="required_field"> *</span></label>
									<input id="referee" name="referee[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[0]->referee?>" style="padding-left: 29px !important; width: 67% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referrer" class="full-bg-label">REFERRER<span class="required_field"> *</span></label>
									<input id="referrer" name="referrer[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[0]->referrer?>" style="padding-left: 29px !important; width: 59% !important;">
								</div>			  
							</div>

                         		<div class="row">
								<div class="input-field col s12 m12 l4 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important; padding: 0 0 0 10px !important;">
									<label for="plan_name1" class="full-bg-label">Plan Name<span class="required_field"> *</span></label>
									<input id="plan_name1" name="plan_name[]" class="full-bg adjust-width" type="text" value="<?=@$config[1]->plan_name ?>">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding:0px !important; border-left:1px solid #EEF2FE !important;">
									<label for="plan_value1" class="full-bg-label">Plan Value<span class="required_field"> *</span></label>
									<input id="plan_value1" name="plan_value[]" class="full-bg adjust-width first border-right" type="text" value="<?=@$config[1]->plan_value?>" style="padding-left: 29px !important; width: 77% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="discount1" class="full-bg-label">DISCOUNT % age<span class="required_field"> *</span></label>
									<input id="discount1" name="discount[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[1]->discount?>" style="padding-left: 29px !important; width: 57% !important; margin-left: 15px !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referee1" class="full-bg-label">REFEREE<span class="required_field"> *</span></label>
									<input id="referee1" name="referee[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[1]->referee?>" style="padding-left: 29px !important; width: 67% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referrer1" class="full-bg-label">REFERRER<span class="required_field"> *</span></label>
									<input id="referrer1" name="referrer[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[1]->referrer?>" style="padding-left: 29px !important; width: 59% !important;">
								</div>			  
                          </div>

                          		<div class="row">
								<div class="input-field col s12 m12 l4 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important; padding: 0 0 0 10px !important;">
									<label for="plan_name2" class="full-bg-label">Plan Name<span class="required_field"> *</span></label>
									<input id="plan_name2" name="plan_name[]" class="full-bg adjust-width" type="text" value="<?=@$config[2]->plan_name ?>">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding:0px !important; border-left:1px solid #EEF2FE !important;">
									<label for="plan_value2" class="full-bg-label">Plan Value<span class="required_field"> *</span></label>
									<input id="plan_value2" name="plan_value[]" class="full-bg adjust-width first border-right" type="text" value="<?=@$config[2]->plan_value?>" style="padding-left: 29px !important; width: 77% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="discount2" class="full-bg-label">DISCOUNT % age<span class="required_field"> *</span></label>
									<input id="discount2" name="discount[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[2]->discount?>" style="padding-left: 29px !important; width: 57% !important; margin-left: 15px !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referee2" class="full-bg-label">REFEREE<span class="required_field"> *</span></label>
									<input id="referee2" name="referee[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[2]->referee?>" style="padding-left: 29px !important; width: 67% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important;">
									<label for="referrer2" class="full-bg-label">REFERRER<span class="required_field"> *</span></label>
									<input id="referrer2" name="referrer[]" class="numeric_number full-bg adjust-width" type="text" value="<?=@$config[2]->referrer?>" style="padding-left: 29px !important; width: 59% !important;">
								</div>			  
                          </div>


                        </div>
                      </div>
                    </div>
					
					<div class="row" style="margin:3% 0 10px -10px;">
						<div class="col s12 m12 l12 right">
							Note: The subscription plan images on website homepage, pricing page and Subscribe & Shop page have to be changed manually if there is a change in plan value or discount
						</div>
					</div>
					
					<div class="row">
						<div class="col s12 m12 l12"> <div class="form-botom-divider"></div> </div>
					</div>
            
					<div class="row">
						<div class="col s12 m12 l6 right">
							<button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
							<button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href ='<?php echo base_url();?>Admin_dashboard/coupon_list_view'" style="margin-right:10px !important;">Cancel</button>
						</div>
					</div>
                     <div class="row">
						<div class="col s12 m12 l12 right">
							
						</div>
					</div>
					</form>
					
					<div class="row">
						<div class="col s12 m12 l12 mt-2"> <div class="form-botom-divider"></div> </div>
					</div>
					
					<div class="row" style="margin-bottom:5%;">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-body">
							<!--div class="row">
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding: 0 0 0 10px !important; height:75px;">
									<label for="" class="full-bg-label">Emailer Particulars</label>
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding: 0 0 0 0px !important; border:1px solid #EEF2FE !important; border-bottom:none; height:75px;">
									<label for="" class="full-bg-label">Start Date</label>
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border:1px solid #EEF2FE !important; border-bottom:none; padding:0px !important; height:75px;">
									<label for="" class="full-bg-label">End date</label>
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; border-right:1px solid #EEF2FE !important; padding:0px !important; height:75px;">
									<label for="" class="full-bg-label">Start Time</label>
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border:1px solid #EEF2FE !important; border-bottom:none; padding:0px !important; height:75px;">
									<label for="" class="full-bg-label">End Time</label>
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border:1px solid #EEF2FE !important; border-bottom:none; padding:0px !important; height:75px; width:16%;">
									<label for="" class="full-bg-label">Action Button</label>
								</div>			  
							</div-->
							<div class="row">
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding: 0 0 0 10px !important; width:24%;">
									<label for="email_parti" class="full-bg-label">Emailer Particulars</label>
									<input id="email_parti" name="email_parti" class="full-bg adjust-width" type="text" value="General Maintenance">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding: 0 0 0 0px !important; border-left:1px solid #EEF2FE !important; width:15%;">
									<label style="left:0px;" for="start_date" class="full-bg-label">Start Date<span class="required_field"> *</span></label>
									<input id="start_date" name="start_date" class="icon-calendar-green config_date full-bg adjust-width" type="text" value="" style="border-top: 1px solid #eef2fe !important; border-right: 1px solid #eef2fe !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_date" class="full-bg-label">Start End<span class="required_field"> *</span></label>
									<input id="end_date" name="end_date" class="icon-calendar-red config_date full-bg adjust-width" type="text" value="" style="border-right: 1px solid #eef2fe !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="start_time" class="full-bg-label">Start Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="start_time" name="start_time" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="9 OR 13.30">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_time" class="full-bg-label">End Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="end_time" name="end_time" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="12 OR 21.45">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:16%; height: 75px; text-align: center; padding: 3% 0 !important;">
									<a href="#" id="send" style="text-decoration:none; font-size:14px; font-weight:500; color:#7864e9;">SEND</a>
								</div>			  
							</div>

                         	<div class="row">
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding: 0 0 0 10px !important; width:24%;">
									<label for="email_parti" class="full-bg-label">Emailer Particulars</label>
									<input id="email_parti1" name="email_parti1" class="full-bg adjust-width" type="text" value="GST Verification Maintenance" style="padding-right: 0px !important; width: 90% !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding: 0 0 0 0px !important; border-left:1px solid #EEF2FE !important; width:15%;">
									<label style="left:0px;" for="start_date" class="full-bg-label">Start Date<span class="required_field"> *</span></label>
									<input id="start_date1" name="start_date1" class="icon-calendar-green new-date config_date full-bg adjust-width" type="text" value="" style="border-top: 1px solid #eef2fe !important; border-right: 1px solid #eef2fe !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_date" class="full-bg-label">Start End<span class="required_field"> *</span></label>
									<input id="end_date1" name="end_date1" class="icon-calendar-red new-date config_date full-bg adjust-width" type="text" value="" style="border-right: 1px solid #eef2fe !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="start_time" class="full-bg-label">Start Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="start_time1" name="start_time1" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="9 OR 13.30">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_time" class="full-bg-label">End Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="end_time1" name="end_time1" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="12 OR 21.45">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:16%; height: 75px; text-align: center; padding: 3% 0 !important;">
									<a href="#" id="send1" style="text-decoration:none; font-size:14px; font-weight:500; color:#7864e9;">SEND</a>
								</div>			  
							</div>

                          	<div class="row">
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding: 0 0 0 10px !important; width:24%;">
									<label for="email_parti" class="full-bg-label">Emailer Particulars</label>
									<input id="email_parti2" name="email_parti2" class="full-bg adjust-width" type="text" value="E-invoice API Maintenance">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="padding: 0 0 0 0px !important; border-left:1px solid #EEF2FE !important; width:15%;">
									<label style="left:0px;" for="start_date" class="full-bg-label">Start Date<span class="required_field"> *</span></label>
									<input id="start_date2" name="start_date2" class="icon-calendar-green new-date config_date full-bg adjust-width" type="text" value="" style="border-top: 1px solid #eef2fe !important; border-right: 1px solid #eef2fe !important;">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_date" class="full-bg-label">Start End<span class="required_field"> *</span></label>
									<input id="end_date2" name="end_date2" class="icon-calendar-red new-date config_date full-bg adjust-width" type="text" value="" style="border-right: 1px solid #eef2fe !important;">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="start_time" class="full-bg-label">Start Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="start_time2" name="start_time2" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="9 OR 13.30">
								</div>
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:15%;">
									<label style="left:0px;" for="end_time" class="full-bg-label">End Time (24 Hr Format)<span class="required_field"> *</span></label>
									<input id="end_time2" name="end_time2" class="numeric_number full-bg adjust-width" type="text" value="" placeholder="12 OR 21.45">
								</div>			  
								<div class="input-field col s12 m12 l2 padd-n" style="border-left: 1px solid #eef2fe; border-top:1px solid #EEF2FE !important; padding:0px !important; width:16%;height: 75px; text-align: center; padding: 3% 0 !important;">
									<a href="#" id="send2" style="text-decoration:none; font-size:14px; font-weight:500; color:#7864e9;">SEND</a>
								</div>			  
							</div>
                        </div>
                      </div>
                    </div>					
                </div>
				
            </div>
			
		</div>
	</div>
	</div>
    </div>
    </section>
    <!--div class="col s12 m12 l12">
		<div class="footer-pay step2 footer-btns last-sec">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <div class="col s12 m12 l5"></div>
                <div class="col s12 m12 l2">
                    <div class="text-center">
						<a class="view"><i class="material-icons know-more-form">keyboard_arrow_up</i></a>
                    </div>
                </div>
                <div class="col s12 m12 l5">
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>Admin_dashboard/coupon_list_view'" style="margin-right:10px !important;">Cancel</button>
                </div>
              </div>
            </div>
        </div>
    </div-->
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>

<div id="review" class="modal fade">
    <div class="row" style="margin-top:30px;">
      <div class="col s12 m12 l12">
      
      <div class="col s12 m12 l10" style="margin-left:43px;">
          <label style="font-size:18px; color:black; text-align:center;"><b>Your total invoiced amount doesn't tally with </b></label><br>
          <label style="font-size:18px; color:black; margin-left:48px;"><b>the total receipt amount entered</b></label>
      </div>
                      
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="right">
            <button type="submit" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">REVIEW</button>
            <button type="button" id="SubForm"  class="btn-flat theme-flat-btn theme-btn theme-btn-large">PROCEED</button>
        </div>
      </div>
</div>
</div>

	<script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.select2-selection__rendered').each(function () {
           $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
        });
		
		$("#coupon_end_date").change(function () {
			/*var startDate = new Date(document.getElementById("coupon_start_date").value);
			var endDate = new Date(document.getElementById("coupon_end_date").value);
			alert(startDate.getTime());
			alert(endDate.getTime());
			if(startDate !="" && endDate !=""){
			if (startDate >= endDate) {
				Materialize.toast('End date should be greater than Start date', 2000,'red rounded');
				document.getElementById("coupon_end_date").value = "";
			}}*/
		});


		$("#send").click(function () {

			var start_date=$("#start_date").val();
			var end_date=$("#end_date").val();
			var start_time=$("#start_time").val();
			var end_time=$("#end_time").val();
			var email_parti=$("#email_parti").val();

			$.ajax({
			url:base_url+'admin_dashboard/maintanance',
			type:"POST",
			data:{'start_date':start_date,'end_date':end_date,'start_time':start_time,'end_time':end_time,'email_parti':email_parti},
			success:function(res){
				Materialize.toast('Email has been successfully sent to all Xebra users.', 2000,'green rounded');
			},
		});
			
		});

		$("#send1").click(function () {
		var start_date=$("#start_date1").val();
			var end_date=$("#end_date1").val();
			var start_time=$("#start_time1").val();
			var end_time=$("#end_time1").val();
			var email_parti=$("#email_parti1").val();

			$.ajax({
			url:base_url+'admin_dashboard/maintanance',
			type:"POST",
			data:{'start_date':start_date,'end_date':end_date,'start_time':start_time,'end_time':end_time,'email_parti':email_parti},
			success:function(res){
				Materialize.toast('Email has been successfully sent to all Xebra users.', 2000,'green rounded');
			},
		});
		});


		$("#send2").click(function () {
			var start_date=$("#start_date2").val();
			var end_date=$("#end_date2").val();
			var start_time=$("#start_time2").val();
			var end_time=$("#end_time2").val();
			var email_parti=$("#email_parti2").val();

			$.ajax({
			url:base_url+'admin_dashboard/maintanance',
			type:"POST",
			data:{'start_date':start_date,'end_date':end_date,'start_time':start_time,'end_time':end_time,'email_parti':email_parti},
			success:function(res){
				Materialize.toast('Email has been successfully sent to all Xebra users.', 2000,'green rounded');
			},
		});
		});
		
		$('.config_date').datepicker({
			autoclose: true,
			format: 'dd/mm/yyyy',
			todayHighlight: true,
		}).on("changeDate", function (e) {

			if($(this).attr('id')=="co_start_date"){
			  $('#co_end_date').val('');
			  $('#co_end_date').datepicker('setStartDate',  e.date).datepicker('update');
			}
			if($(this).attr('id')=="co_start_date1"){
			  $('#co_end_date1').val('');
			  $('#co_end_date1').datepicker('setStartDate',  e.date).datepicker('update');
			}
			if($(this).attr('id')=="co_start_date2"){
			  $('#co_end_date2').val('');
			  $('#co_end_date2').datepicker('setStartDate',  e.date).datepicker('update');
			}
		});
	  });
	</script>
      <!-- jQuery Library -->
  <?php $this->load->view('template/footer.php');?>
      <!-- END MAIN -->