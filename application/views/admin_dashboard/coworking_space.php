<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
<!-- START MAIN -->
<style type="text/css">

	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}

	.border-split-form .select-wrapper {
		padding: 7px 0 2px 0 !important;
	}
	
	input.full-bg:not(.first)  {
		border-top: 0 !important; 
	}
  
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute; 
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	} 
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px; 
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}
	
	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 0px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:35px !important; 
		line-height: 35px;
	}
	
	.nature_of_code{
		border-right: 1px solid #EEF2FE !important;
	}
	
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -15px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	
	.step2.footer-btns {
		margin: 10.3% 0 0 0 !important;
	}
	
</style>

    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" id="create_coworking_space" name="create_coworking_space" method="post" enctype="multipart/form-data">
  <section id="content" class="bg-theme-gray documents-search">
    <div class="container">
      <div class="plain-page-header">
        <div class="row" style="margin-top:10px;">
          <div class="col l6 s12 m6">
            <a class="go-back bg-l underline" href="<?php echo base_url();?>admin_dashboard/coworking_list">Back to Co-Working Space</a> 
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
         <div class="page-content">
        
            <div class="row">
              <div class="col s12 m12 l3"></div>
                <div class="col s12 m12 l6">
                    <div class="row" style="margin-top:6%;">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-header border-bottom">
                          <h3 class="box-title">Create Co-Working Space</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
								<div class="col s12 m12 l9">
								  <div class="row">
									<div class="input-field" style="border-bottom:1px solid #eef2fe;">
									  <label for="co_name" class="full-bg-label">Co-Working Space Name<span class="required_field"> *</span></label>
									  <input id="co_name" name="co_name" class="full-bg adjust-width border-bottom-none" type="text">
									</div>
								  </div>
								</div>
								<div class="col s12 m12 l3">
								  <div class="row">
									<div class="input-field">
									  <div class="uploader-placeholder">
										<label class="up_pic" style="font-size:12px; color:#696969; margin-top:-13px !important; margin-left:27%;">Upload photo</label>
										<input type="file" class="hide-file" id="co_working_image" name="co_working_image">
										<input type="hidden" id="image_info" name="image_info" value="" />
									  </div>
									</div>
								  </div>
								</div>
							</div>
							<div class="col s12 m12 l12 nature_of_code input-set">
								<div class="input-field ">
									<label for="co_address" class="full-bg-label">ADDRESS<span class="required_field"> *</span></label>
									<input id="co_address" name="co_address" class="full-bg adjust-width first border-right" type="text"  value="">
								</div>
							</div>
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l6 nature_of_code input-set">
                                <div class="input-field label-active">
                                    <label for="co_country" class="full-bg-label select-label">Country<span class="required_field"> *</span></label>
                                    <select id="co_country" name="co_country" class="country-dropdown modereceipt check-label">
                                        <option value="">SELECT COUNTRY</option>
										<?php foreach ($countries as $country) { ?>
										<option value="<?php echo $country->country_id; ?>"><?php echo strtoupper($country->country_name); ?></option>
										<?php } ?>
                                    </select>
                                </div>
                              </div>
							  <div class="col s12 m12 l6 nature_of_code input-set">
                                <div class="input-field">
                                    <label for="co_state" class="full-bg-label select-label">State<span class="required_field"> *</span></label>
                                    <select id="co_state" name="co_state" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                    </select>
                                </div>
                              </div>
							  <div class="input-field col s12 m12 l6" style="height:74px; border-right:1px solid #eef2fe; border-top:1px solid #eef2fe;">
                                    <label style="margin-left:-10px;" for="co_city" class="full-bg-label select-label">City<span class="required_field"> *</span></label>
                                    <select id="co_city" name="co_city" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                    </select>
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="co_emailid" class="full-bg-label">EMAIL ID<span class="required_field"> *</span></label>
                                 <input id="co_emailid" name="co_emailid" class="full-bg adjust-width first border-right" type="text"  value="">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="co_contact" class="full-bg-label">CONTACT NO<span class="required_field"> *</span></label>
                                 <input id="co_contact" name="co_contact" class="full-bg adjust-width first border-bottom-none border-right numeric_number" type="text"  value="">
                              </div>							 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
					<div class="row">
                      <div class="col s12 m12 l12 mt-2">
                        <div class="form-botom-divider"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 m12 l6 right">
                       <input type="submit" name="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right" value="Save">
                       <button  class="btn-flat theme-flat-btn theme-btn theme-btn-large right mr-5" type="reset" >Cancel</button>
                      </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	</div>
    </div>	
    </section>
    </form>
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>

	<script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.select2-selection__rendered').each(function () {
           $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
        });
	  });
	</script>
	<script>
		$(document).ready(function() {
		$("#co_country").on("change",function(){
            var country_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_states',
              type:"POST",
              data:{'country_id':country_id},
              success:function(res){
                $("#co_state").html(res);
                $("#co_state").parents('.input-field').addClass('label-active');
                $('#co_state').material_select();
              },
            });
          });
		  
		  $("#co_state").on("change",function(){
            var state_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_cities',
              type:"POST",
              data:{'state_id':state_id},
              success:function(res){
               $("#co_city").html(res);
               $("#co_city").parents('.input-field').addClass('label-active');
               $('#co_city').material_select();
              },
            });
          });
		});
	</script>
    <!-- jQuery Library -->
	<?php $this->load->view('template/footer.php');?>
    <!-- END MAIN -->