
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>

 <style type="text/css">

	.border-split-form .select-wrapper{
        padding: 7px 0 2px 0 !important;
        top: 0px !important;
	}
  
	.li-autocomplete{
		padding: 2px 0px 2px 20px !important;
		box-shadow: 0 0 1px 0 #bbb !important;
		background-color: #fff !important;
	}
	
	.ul-autocomplete{
		z-index: 99999 !important;
	}
	
	.select-dropdown {
		font-size: 14px !important;
	}
	
	label.full-bg-label {
		color: #666;
	}
	
	.input-field label.select-label{
		color: #666;
	}

	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -20px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	
	.btn-display{
		display: contents;
	}
	
	.adjust-width{
	  border-bottom:none !important;
	}
	
	.chkbox2{
	  float:left !important;
		  margin: 10px 0 0 0;
	}
	
	a.addmorelink{
		padding-top: 10px;
		text-decoration: none;
	}
	
	div#breadcrumbs-wrapper {	
		border-bottom: none;
	}
	
	.bg-img-green  {
		margin: 30px 0 0px 0;
	}
	
	#admin_change_password .box-wrapper.bg-white{
		width:555px !important;
		margin: 0 8% !important;
	}
	
	#admin_change_password .remme{
		width:555px !important;
		margin: 6% 4.8% !important;
	}
 </style>

  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
        

		<form class="create-company-form border-split-form" name="admin_change_password" id="admin_change_password" method="post" >
		  <section id="content">
			<div class="container">
			  <div class="plain-page-header">

				<div class="page-content">
				  <div class="row">
					<div id="breadcrumbs-wrapper">
						<div class="container">
							<div class="row">
							<div class="col s12 m12 l10 offset-l1">
							  <!-- <div class="col s10 m6 l6"> -->
								<h5 class="breadcrumbs-title">Change Password<small class="grey-text"></small></h5>

								<ol class="breadcrumbs">
								 
								  <li class="active"><a href="">CHANGE PASSWORD</a></li>
								</ol>
							  </div>
							 
						  </div>
						</div>
					</div></div>
					 <div class="row">
					<div class="col s12 m12 l3"></div>
					<div class="col s12 m12 l6">

					  <div class="row">
						<div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
						  <!-- <div class="box-header">
							
						  </div> -->
						  <div class="box-body">
					   <!--      <div class="row ">
							  <div class="col s12 m12 l12">
								<div class="input-field col s12 m12 l6 padd-n">
									<label for="cust_code" class="full-bg-label">CLIENT NO.<span class="required_field"> *</span></label>
								   <input id="cust_code" name="cust_code" class="full-bg adjust-width border-right" type="text"   readonly="readonly">
								</div>
								<div class="input-field col s12 m12 l6 padd-n">
								  <label for="cust_date" class="full-bg-label">CLIENT DATE<span class="required_field"> *</span></label>
								  <input id="cust_date" name="cust_date" class="full-bg icon-calendar-gray adjust-width border-top-none" type="text"  readonly="readonly">
								</div>
							  </div>
							</div> -->
							<div class="row">
							  <div class="col s12 m12 l12">
									<div class="input-field">
									  <label for="old_password" class="full-bg-label ex-com-name">OLD PASSWORD<span class="required_field"> *</span></label>
									  <input id="cust_name" name="old_password" class="full-bg adjust-width " type="password" autocomplete="off">
									</div>
									<div class="hide-show-5">
										<span class="show-5-monkey" style="position:absolute; left:90%; top:10%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
									</div>
								</div>
							  </div>
							
							<div class="row">
							  <div class="col s12 m12 l12">
								<div class="input-field">
								  <label for="new_password" class="full-bg-label">NEW PASSWORD<span class="required_field"> *</span></label>
								  <input id="new_password" name="new_password" class="full-bg adjust-width " type="password" autocomplete="off">
								</div>
								<div class="hide-show-6">
									<span class="show-6-monkey" style="position:absolute; left:90%; top:45%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
								</div>
							  </div>
							</div> 
							
							<div class="row">
							  <div class="col s12 m12 l12">
								<div class="input-field">
								  <label for="confirm_password" class="full-bg-label">CONFIRM NEW PASSWORD<span class="required_field"> *</span></label>
								  <input id="confirm_password" name="confirm_password" class="full-bg adjust-width " type="password" autocomplete="off">
								</div>
								<div class="hide-show-4">
									<span class="show-4-monkey" style="position:absolute; left:90%; top:76%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
								</div>
							  </div>
							</div> 
							
						
						  </div>
						</div>
					  </div>
					
					 <div class="step1 footer-btns remme">
						<div class="row">
						  <div class="right btn-display">
							<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-2 right">SAVE</button>
							<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right" type="button"  onclick="location.href = '<?php echo base_url();?>Admin_dashboard';">CANCEL</button>
						  </div>
					  </div>
						
					</div>

					</div>
				  </div>

				  <div class="row">
					<div class="col s12 m12 l12">
					  <div class="col s12 m12 l12 input-set">
						<div class="row">
						  
						</div>
					  </div>
					</div>
				  </div>
				  <div class="row">
					<div class="col s12 m12 l12">
					  <div class="col s12 m12 l12 input-set">
						<div class="row mr-1 pl-3">
						</div>
					  </div>
					</div>
				  </div>
						
				</div>
			  </div>
		</section>
	</form>
	
	<script type="text/javascript">
	$(document).ready(function () {
	$('.hide-show-6').show();
	$('.hide-show-6 span').addClass('show')
				 
	$('.hide-show-6 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(".show-6-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#new_password').attr('type','text');
			$(".show-6-monkey").removeClass('show');
		} else {
			$(".show-6-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#new_password').attr('type','password');
			$(".show-6-monkey").addClass('show');
		}
	});
	
	$('.hide-show-4').show();
	$('.hide-show-4 span').addClass('show')
				 
	$('.hide-show-4 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(".show-4-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#confirm_password').attr('type','text');
			$(".show-4-monkey").removeClass('show');
		} else {
			$(".show-4-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#confirm_password').attr('type','password');
			$(".show-4-monkey").addClass('show');
		}
	});
	
	$('.hide-show-5').show();
	$('.hide-show-5 span').addClass('show')
				 
	$('.hide-show-5 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(".show-5-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#cust_name').attr('type','text');
			$(".show-5-monkey").removeClass('show');
		} else {
			$(".show-5-monkey").html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#cust_name').attr('type','password');
			$(".show-5-monkey").addClass('show');
		}
	});
	
	});
</script>
    <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
    </div>
<!-- END MAIN -->
      
<?php $this->load->view('template/footer.php');?>