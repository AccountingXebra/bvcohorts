<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('CHALLENGES DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'CHALLENGES DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'CHALLENGE NO');
$this->excel->getActiveSheet()->setCellValue('B3', 'CHALLENGE DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'CHALLENGE NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'CHALLENGE DESCRIPTION');
$this->excel->getActiveSheet()->setCellValue('E3', 'NATURE OF BUSINESS');
$this->excel->getActiveSheet()->setCellValue('F3', 'START DATE');
$this->excel->getActiveSheet()->setCellValue('G3', 'LOCATION');
$count=4;
foreach ($result as $key => $value) {
	
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['challange_code']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, date('d-m-Y',strtotime($value['challange_date'])));
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['challange_name']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['challange_desc']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['challange_nature']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, date('d-m-Y',strtotime($value['last_day_entry'])));
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['location']);
	$count++;
}

$filename='Challange Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>