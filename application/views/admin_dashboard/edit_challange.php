<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
<!-- START MAIN -->
<style type="text/css">

	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}

	.border-split-form .select-wrapper {
		padding: 7px 0 2px 0 !important;
	}
	
	input.full-bg:not(.first)  {
		border-top: 0 !important; 
	}
  
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute; 
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	} 
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px; 
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}
	
	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 0px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:35px !important; 
		line-height: 35px;
	}
	
	.nature_of_code{
		border-right: 1px solid #EEF2FE !important;
	}
	
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -15px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	
	.step2.footer-btns {
		margin: 10.3% 0 0 0 !important;
	}
	#toast-container {
		top:10% !important;
	}
	.uploader-placeholder{ height:100px !important; }
	::placeholder{ color:#999; font-weight:400; }
</style>

    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" id="edit_challange_form" method="post"  enctype="multipart/form-data">
  <section id="content" class="bg-theme-gray documents-search">
    <div class="container">
      <div class="plain-page-header">
        <div class="row">
          <div class="col l6 s12 m6">
            <a class="go-back bg-l underline" href="<?php echo base_url();?>admin_dashboard/admin_challenge">Back to My Challenge</a> 
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
         <div class="page-content">
        
            <div class="row">
              <div class="col s12 m12 l3"></div>
                <div class="col s12 m12 l6">
                    
                    <div class="row">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-header">
                          <h3 class="box-title">Edit Challenge</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
								<div class="input-field col s12 m12 l6 padd-n">
									<label for="challange_code" class="full-bg-label">CHALLENGE ID<span class="required_field"> *</span></label>
									<input id="challange_code" name="challange_code" class="full-bg adjust-width first border-right" type="text"  value="<?=$challanges[0]->challange_code?>">
								</div>
								<div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important;">
									<label for="challange_date"class="full-bg-label">CHALLENGE DATE <span class="required_field"> *</span></label>
									<input id="challange_date" name="challange_date"class="new-date bdatepicker full-bg adjust-width" type="text" readonly="readonly" value="<?= date('d/m/Y',strtotime($challanges[0]->challange_date)); ?>">
								</div>
							  
							  <!--<div class="col s12 m12 l6 nature_of_code input-set">
                                <div class="input-field">
                                    <label for="coupon_nature" class="full-bg-label select-label">NATURE OF CODE<span class="required_field"> *</span></label>
                                    <select id="coupon_nature" name="coupon_nature" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                        <option value="1">Coupon Code</option>
                                        <option value="2">Referal Code</option>
                                    </select>
                                </div>
                              </div>
							  <div class="input-field col s12 m12 l6" style="height:74px; border-right:1px solid #eef2fe;">
                                    <label style="margin-left:-10px;" for="pack_select" class="full-bg-label select-label">SELECT PACK<span class="required_field"> *</span></label>
                                    <select id="pack_select" name="pack_select" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                        <option value="all">All</option>
                                       
										<option value="trot">TROT</option>
										<option value="canter">CANTER</option>
										<option value="gallop">GALLOP</option>
                                    </select>
                              </div>-->
							  <div class="input-field col s12 m12 l8 padd-n">
                                 <label for="challange_name" class="full-bg-label">CHALLENGE NAME <span class="required_field"> *</span></label>
                                 <input id="challange_name" name="challange_name" class="full-bg adjust-width first border-right" type="text"  value="<?=$challanges[0]->challange_name?>" autocomplete="off" style="height: 70px; border: none; width: 96% !important; margin: 0 -13px !important;">
                              </div>
							  <div class="col s12 m12 l4">
								  <div class="row">
									<div class="input-field">
									  <div class="uploader-placeholder">
										<label class="up_pic" style="font-size: 12px; color: #696969; margin-top: -8px !important; margin-left: 33%;">Upload Image</label>
										<input type="file" class="hide-file" id="challange_image" name="challange_image">
										
									  </div>
									</div>
								  </div>
								</div>
							  <div class="input-field col s12 m12 l12 padd-n">
                                 <textarea id="challange_desc" name="challange_desc" class="full-bg adjust-width first border-right" type="text"  value="" style="height: 95px !important; background-color: transparent !important; border: none !important; margin:0 -7px;" placeholder="CHALLENGE DESCRIPTION"><?=$challanges[0]->challange_desc?></textarea>
                              </div>
							  <div class="col s12 m12 l6 nature_of_code input-set" style="border-top:1px solid #eef2fe;">
                                <div class="input-field label-active">
                                    <label for="challange_nature" class="full-bg-label select-label">NATURE OF BUSINESS<span class="required_field"> *</span></label>
                                    <select id="challange_nature" name="challange_nature" class="country-dropdown modereceipt check-label">
                                        <option value=""> </option>
                                      <option value="<?=$challanges[0]->challange_nature?>" selected><?= strtoupper($challanges[0]->challange_nature)?></option>  
										<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
										<option value="Advertising">ADVERTISING</option>
										<option value="Animation Studio">ANIMATION STUDIO</option>
										<option value="Architecture">ARCHITECTURE</option>
										<option value="Arts & Crafts">ARTS & CRAFTS</option>
										<option value="Audit & Tax">AUDIT & TAX</option>
										<option value="Brand Consulting">BRAND CONSULTING</option>
										<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
										<option value="Consultant">CONSULTANT</option>
										<option value="Content Studio">CONTENT STUDIO</option>
										<option value="Cyber Security">CYBER SECURITY</option>
										<option value="Data Analytics">DATA ANALYTICS</option>
										<option value="Digital Influencer">DIGITAL INFLUENCER</option>
										<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
										<option value="Direct Marketing">DIRECT MARKETING</option>
										<option value="Entertainment">ENTERTAINMENT</option>
										<option value="Event Planning">EVENT PLANNING</option>
										<option value="Florist">FLORIST</option>
										<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
										<option value="Financial and Banking">FINANCIAL & BANKING</option>
										<option value="Gaming Studio">GAMING STUDIO</option>
										<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
										<option value="Hardware Servicing">HARDWARE SERVICING</option>
										<option value="Industry Bodies">INDUSTRY BODIES</option>
										<option value="Insurance">INSURANCE</option>
										<option value="Interior Designing ">INTERIOR DESIGNING</option>
										<option value="Legal Firm">LEGAL FIRM</option>
										<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
										<option value="Mobile Services">MOBILE SERVICES</option>
										<option value="Music">MUSIC</option>
										<option value="Non-Profit">NON-PROFIT</option>
										<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
										<option value="Photography">PHOTOGRAPHY</option>
										<option value="Printing">PRINTING</option>
										<option value="Production Studio">PRODUCTION STUDIO</option>
										<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
										<option value="Publishing">PUBLISHING</option>
										<option value="Real Estate">REAL ESTATE</option>
										<option value="Recording Studio">RECORDING STUDIO</option>
										<option value="Research">RESEARCH</option>
										<option value="Sales Promotion">SALES PROMOTION</option>
										<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
										<option value="Stock & Shares">STOCK & SHARES</option>
										<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
										<option value="Tours & Travel">TOURS & TRAVELS</option>
										<option value="Training & Coaching">TRAINING & COACHING</option>
										<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
										<option value="Therapists">THERAPISTS</option>
										<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
										<option value="Web Development">WEB DEVELOPMENT</option>
                                    </select>
                                </div>
                              </div>

							  <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important; border-right:1px solid #EEF2FE !important;">
                                <label for="last_day_entry"class="full-bg-label">START DATE<span class="required_field"> *</span></label>
                                <input id="last_day_entry" name="last_day_entry"class="new-date bdatepicker full-bg adjust-width icon-calendar-green" type="text" readonly="readonly"  value="<?= date('d/m/Y',strtotime($challanges[0]->last_day_entry)); ?>" autocomplete="off">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #EEF2FE !important;">
                                <label for="location"class="full-bg-label">LOCATION<span class="required_field"> *</span></label>
                                <input id="location" name="location" class="full-bg adjust-width first border-right" type="text"  value="<?=$challanges[0]->location?>" autocomplete="off">
                              </div>
							  
							  <div class="input-field col s12 m12 l6 padd-n">
                                <label for="url"class="full-bg-label">WEBSITE URL<span class="required_field"> *</span></label>
                                <input id="url" name="url" class="full-bg adjust-width first border-right" type="text"  value="<?=$challanges[0]->url?>">
                              </div>
							 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	</div>
    </div>
    </section>
    <div class="col s12 m12 l12">
		<div class="footer-pay step2 footer-btns last-sec">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <div class="col s12 m12 l5"></div>
                <div class="col s12 m12 l2">
                    <div class="text-center">
						<a class="view"><i class="material-icons know-more-form">keyboard_arrow_up</i></a>
                    </div>
                </div>
                <div class="col s12 m12 l5">
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="javascript:window.history.go(-1);" style="margin-right:10px !important;">Cancel</button>	
                </div>
              </div>
            </div>
        </div>
    </div>
    </form>
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>

<div id="review" class="modal fade">
    <div class="row" style="margin-top:30px;">
      <div class="col s12 m12 l12">
      
      <div class="col s12 m12 l10" style="margin-left:43px;">
          <label style="font-size:18px; color:black; text-align:center;"><b>Your total invoiced amount doesn't tally with </b></label><br>
          <label style="font-size:18px; color:black; margin-left:48px;"><b>the total receipt amount entered</b></label>
      </div>
                      
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="right">
            <button type="submit" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">REVIEW</button>
            <button type="button" id="SubForm"  class="btn-flat theme-flat-btn theme-btn theme-btn-large">PROCEED</button>
        </div>
      </div>
</div>
</div>

	<script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.select2-selection__rendered').each(function () {
           $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
        });
		
		$("#coupon_end_date").change(function () {
			var startDate = new Date(document.getElementById("coupon_start_date").value);
			var endDate = new Date(document.getElementById("coupon_end_date").value);
			//alert(startDate.getTime());
			//alert(endDate.getTime());
			if(startDate !="" && endDate !=""){
			if (startDate >= endDate) {
				Materialize.toast('End date cannot be ahead of start date', 20000,'red rounded');
				document.getElementById("coupon_end_date").value = "";
			}}
		});

          $('#coupon_code').keyup(function() {
 var code=$(this).val();
if(code!=""){

  $.ajax({
          url:base_url+'admin_dashboard/get_code',
          type:"POST",
          data:{"code":code},
          success:function(res){
                  var data = JSON.parse(res);
                  if(data == true)
                  {
                    $('#taken').show();
                    $('#available').hide();
                    $('#coupon_code').val("");
                    
                  }else{
                     $('#taken').hide();
                    $('#available').show();
                  }
             },
    }); 
}
  });

	  });
	</script>
      <!-- jQuery Library -->
  <?php $this->load->view('template/footer.php');?>
      <!-- END MAIN -->