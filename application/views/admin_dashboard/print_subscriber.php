<style type="text/css">
	@page {
        size: auto; 
        margin: 0mm;  
    }
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="4"><h2 style="margin: 0">Subscribers</h2></th></tr>
	<tr>
	 <th>REGISTRATION DATE</th>
	 <th>REGISTRATION ID</th>
	 <th>CLIENT NAME</th>
	 <th>EMAIL ID</th>
	 <th>MOBILE NO</th>
	 <th>COUNTRY</th>
	 <th>CITY</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($result as $key => $value) { 

		$created_at =  date("d-m-Y", strtotime($value['createdat']));

  		if($value['reg_istrial']==0)
  		{
  			$reg_istrial = 'NO';
  			$sub_status = 'YES';

  		}else{
  			$reg_istrial = 'YES';
  			$sub_status = 'NO';
  		}
			
		$date1=date_create(date("Y-m-d", strtotime($value['createdat'])));
		$date2=date_create(date("Y-m-d"));
		$diff=date_diff($date1,$date2);
		$days = $diff->format("%a");
	?>
	<tr>
		<td><?= $created_at; ?></td>
		<td><?= 'REG'.$value['reg_id']; ?></td>
		<td><?= $value['reg_username']; ?></td>
		<td><?= $value['reg_email']; ?></td>
		<td><?= $value['reg_mobile']; ?></td>
		<td><?= $value['country_name']; ?></td>
		<td><?= $value['name']; ?></td>
	</tr>	
<?php } ?>

</tbody>
</table>
<script type="text/javascript">
	//window.print();
</script>