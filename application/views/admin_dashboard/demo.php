<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <!-- END HEADER -->
    <style type="text/css">
    .add-new {
		margin: 0 0 0 0 !important;
    }
    .section_li {
         margin: 0 0 0 0 !important;
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    .d_a.btn.btn-theme.btn-large.section_li {
		background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .d_a .btn-theme{
		padding: 0 40px !important;
        border-radius: 6px !important;
        line-height: 45px !important;
        height: 45px !important;
		background-color:#fff;
	}
    ul.categories {
		text-align: center;
        margin:0;
        width: 100%;
    }
     /* li {
          padding: 0 5px 0 5px;

      }*/
      .tabs {
          height:auto;
          background: transparent !important;
          width: 100% !important;
      }
      ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
	}
    li.tab a.active {
      min-height: auto !important;
    }
	a.btn.btn-theme.btn-large.section_li.active {
		background-color: #7864e9 !important;
		color: #fff;
		border: 0px;
	}
	.tabs .tab{
		padding: 0 5px 0 5px !important;
		line-height: 75px;
		min-height: 45px;
		background-color:#fff !important;
	}
	.collapsible {
		border: 0px;
		-webkit-box-shadow: none;
	}
	.document-box-view.redborder.active {
		border-left: 4px solid #ff7c9b;
	}
	.collapsible-header{
		border-bottom:none;
	}
	.collapsible-header.active{
		padding-bottom: 0px;
        font-weight: 600;
	}
	.collapsible-body{
		padding: 0 1rem 1rem 1rem;
	}
	/*.collapsible-body span {
		color: #464646;
	}*/
	.red-bg-right:before {
		top: -32px !important;
	}
	.green-bg-right:after{
		bottom: -25px !important;
		left: 50px;
	}
	ul:not(.browser-default) > li .li_cls {
		list-style-type: disc !important;
	}
	.btn-theme{
		padding:0px !important;
		background-color:#fff;
	}
	.demo-start{
		margin-top:-5%;
	}
	#test13{
		margin: -1.1% 0 20px 0;
	}
	.document-box-view{
		padding:1% 1%;
	}
	.demo-ul{
		padding-left:20px !important;
	}
	.demo-li{
		line-height:30px;
	}
    </style>
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper">
            <div class="container custom">
              <!--div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">DEMO</h5>
                  <ol class="breadcrumbs">
                    <li><a href="index.html">MY RESOURCE CENTER / FAQs</a>
                  </ol>
                </div>
              </div-->
            </div>
        </div>
        <div id="bulk-action-wrapper" class="demo-start">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test13" class="d_a btn btn-theme btn-large section_li">Entire Demo
						</a></li>
						<li class="tab"><a href="#test3" class="d_a btn btn-theme btn-large section_li">Profile Module</a></li>
						<li class="tab"><a href="#test1" class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Sales & </br>Inventory</p></a></li>
						<li class="tab"><a  href="#test2"  class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Expense & </br>Purchase</p></a></li>
						<li class="tab"><a  href="#test7"  class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Assets & </br>Depreciation</p></a></li>
						<li class="tab"><a  href="#test8"  class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">My Receipts & </br>Payments</p></a></li>
						<li class="tab"><a  href="#test9"  class="d_a btn btn-theme btn-large section_li">My Employees</a></li>
						<li class="tab"><a href="#test4"  class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">My Cash, </br>Bank & Card</p></a></li>
						<li class="tab"><a href="#test10"  class="d_a btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">My Tax </br>Summary</p></a></li>
						<li class="tab"><a href="#test11"  class="d_a btn btn-theme btn-large section_li">My Accounts</a></li>
						<!--li class="tab"><a href="#test5"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Subscription </br>& Purchase</p></a></li-->
						<li class="tab"><a href="#test6"  class="d_a btn btn-theme btn-large section_li">Settings</a></li>
						<li class="tab"><a href="#test12"  class="d_a btn btn-theme btn-large section_li">Cohorts</a></li>
					</ul>

                   <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                 </div>
              </div>
          </div>
        </div>

          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12 mt-5">
				  <!-- Sales Module -->	
                  <div id="test1" class="col s12 inner_row_container ">
					<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Sales Module</strong></p>
						<div class="row document-box-view">
						<p><strong>a) Client Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Use the GST no to autofill the client master</li>
							<li class="demo-li">o Upload your legal documents, PO and set a reminder for their renewal date</li>
							<li class="demo-li">o Set revenue alert or Red Flag for each client to send you a notification in case of revenue crosses your set targets within a time-period</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Item Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Set Revenue alert or Red Flag for each item or service to send you a notification in case of revenue crosses your set targets within a time-period</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Billing Documents</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Prepare Estimate, Proforma Invoice or Sales Invoice</li>
							<li class="demo-li">o Completely customize your invoice with different layouts and data points</li>
							<li class="demo-li">o You can make a new client and new item directly from Sales Invoice too</li>
							<li class="demo-li">o Set recurring invoice for your retainer clients which saves your time and hassle</li>
							<li class="demo-li">o Prepare invoices for international clients in multiple currencies and exchange rates</li>
							<li class="demo-li">o Add reimbursable expense vouchers with sales invoices with one simple click</li>
							<li class="demo-li">o Add equalization fee or late fee or reverse charge whenever appropriate</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Credit Notes</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can prepare credit note with one click for a particular invoice from BD list</li>
							<li class="demo-li">o Customize your credit notes and tag with relevant PO no if reqd</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Expense vouchers</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Allow employees to prepare their vouchers and set up your approval process</li>
							<li class="demo-li">o Make the vouchers through the mobile app also</li>
						</ul>
						</div>
						
					 </div>
                  </div>
                  
				  <!-- Expense Module -->
				  <div id="test2" class="col s12  inner_row_container">
                    <div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Expense Module</strong></p>
						<div class="row document-box-view">
						<p><strong>a) Vendor Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Use the GST no to autofill the vendor master</li>
							<li class="demo-li">o Upload your legal documents, PO and set a reminder for their renewal date</li>
							<li class="demo-li">o From Settings icon, set expense alert or Red Flag for each vendor to send you a notification in case a particular expense crosses your set targets within a time-period</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Expense Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Set expense alert or Red Flag for each expense to send you a notification in case of it crossing your set targets within a time-period</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Company Expenses</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Simply select the expense and vendor and record your company expense</li>
							<li class="demo-li">o Click on TDS to record the deduction you have made for that expense</li>
							<li class="demo-li">o Select a recurring expense entry for rent so that the system will auto-create</li>
							<li class="demo-li">o Add equalization fee or reverse charge whenever appropriate</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Purchase Order & Debit Notes</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can prepare purchase order and debit note with one click for a particular company expense from the list</li>
							<li class="demo-li">o	Customize them and tag with relevant exp no & details if reqd</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>e) Petty Cash</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Record the opening balance and date from My Company Profile</li>
							<li class="demo-li">o Keep recording entries in petty cash for each small expense</li>
						</ul>
						</div>
						
					 </div>
                  </div>
                  
				  <!-- Profile Module -->
				  <div id="test3" class="col s12 inner_row_container">
                     <div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Profile Module</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">• User-level access to safeguard the privacy</li>
							<li class="demo-li">• Add multiple GST no. you can view the business of each GST individually or as a combined entity by dropdown on the top white bar</li>
							<li class="demo-li">• Integrate a payment gateway so that you can add Pay button on your invoices. Clients can simply click on it and make payment to you</li>
							<li class="demo-li">• Document locker module allows you to store all your critical company documents at one place</li>
							<li class="demo-li">• Activity History lets you monitor the activities done by all the logins created</li>
						</ul>
					 </div>
                  </div>
                  
				  <!--  My Cash, Bank & Card -->
				  <div id="test4" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong> My Cash, Bank & Card</strong></p>
						<p><strong>a) Cash Statement</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can see all of your cash transaction entries under this whether receipt or payment</li>
							<li class="demo-li">o You can also transfer cash to petty cash easily</li>
							<li class="demo-li">o You can also make an entry for deposit and withdraw from or in your bank account</li>
							<li class="demo-li">o You can enter the amount of opening balance from My Company Profile for cash, petty cash & each bank account</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Bank Statement</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o All of your bank transaction will show here, even if you have multiple bank accounts, you can see them in here</li>
							<li class="demo-li">o Your bank transaction whether receipt or payment will automatically reflect here</li>
							<li class="demo-li">o You can also record a transaction of bank transfer</li>
							<li class="demo-li">o You can create as much as the bank account you want from My Company Profile</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Petty Cash</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can see your petty cash records in here; you can also add petty cash transaction easily</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Card Statement</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o It will show a transaction which took place by mode of cards (Credit and debit cards), you can also record credit card payments</li>
						</ul>
						</div>
						
                  </div>
                  
				  <div id="test5" class="col s12 inner_row_container">
                     <ul class="collapsible">

                      
                    </ul>
                  </div>
                  
				  <div id="test6" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Settings</strong></p>
						<p><strong>a) Import Data</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can import data in CSV format for a client, vendor, item, expense and invoice easily</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Customize Emails</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can customize your emails for every different module in here and while mailing invoice or any such documents, your customized email format will be available</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Set Alerts</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can set alert for your clients/vendors & item/expense which will be helpful for you to know if the expense has crossed the budgeted expense or if the sales is generated from a client or an item is below target and vice versa</li>
							<li class="demo-li">o You can also set the alert for credit period and to keep track of your receipts & you can even know when & whose credit period is coming to an end</li>
							<li class="demo-li">o You can also set the alerts for an important date (Birthday & Anniversary) of your clients, vendors & employees</li>
							<li class="demo-li">o You can set alert for any activity or task and get the reminder for the same</li>
						</ul>
						</div>
                  </div>
				  
				  <div id="test7" class="col s12 inner_row_container">
                     <div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Assets & Depreciation</strong></p>
						
						<div class="row document-box-view">
						<p><strong>a) Vendor & Asset Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o In this, you can record the asset description & nature or type of assets with the tax rate involved in those assets</li>
							<li class="demo-li">o Not only asset description, but you can also add vendor details & contact info from whom you have purchased those assets</li>
							<li class="demo-li">o Also, documents related to those assets can be safely uploaded here</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Asset Purchase</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o After recording the details regarding Vendor & Assets, you can now record the entries for asset purchase, in this section you can also set the depreciation method and rate for the asset purchase</li>
							<li class="demo-li">o It is effortless to record asset purchase entry, as you only have to select the name of the asset other details such as vendor info and asset description will be autofill</li>
							<li class="demo-li">o You also have an option known as "pre-owned" while recording the asset purchase</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Asset Sales</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can also record the entries of asset sale same as asset purchase & you can also enter the profit or loss amount for the asset sold</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Asset Tracker</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o With this, you can easily keep track of your assets, about their working life, warranties & you can even assign an employee for different assets</li>
							<li class="demo-li">o Addition to that you also have an option to enter AMC details, which will be used if you have given the responsibility of your asset to specific AMC provider</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>e) Depreciation Schedule</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can see the depreciation details, which were set while recording asset purchase</li>
							<li class="demo-li">o You can view all aspects such as useful life amount of Depreciation for current and till the end of life of asset just by selecting the asset</li>
						</ul>
						</div>
						
					 </div>
                  </div>
				  
				  <div id="test8" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>My Receipts & Payments</strong></p>
						<p><strong>a) Receipts</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can record your sales receipt easily by selecting client name & invoice number</li>
							<li class="demo-li">o If you received a bulk amount for multiple invoices from the same client, you could also combine those invoices</li>
							<li class="demo-li">o You also have an option for payment status as part/full or bad debts & you can also record if there were any penalty charged to client or bank charges etc. </li>
							<li class="demo-li">o Asset sale receipt is recorded under this module</li>
							<li class="demo-li">o You can also record your foreign sales receipt & their forex gain/loss</li>
							<li class="demo-li">o Forex gain/loss will be calculated based on the difference between the currency rate on the date of sales invoice & sales receipt</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Payments</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can record all of your expense payment with payment status and mode of payment, and you can also upload supporting documents for the same</li>
							<li class="demo-li">o Asset payment is also recorded under this module with payment status & supporting documents</li>
							<li class="demo-li">o The salary payment amount is also recorded here, and you can also provide additional details such as EPF/ESIC/TDS etc.</li>
							<li class="demo-li">o Tax payments are also recorded under this, whether it is GST, Equalization Levy, Employee Tax or even TDS(Employee & Expense)</li>
							<li class="demo-li">o And for your preference, all of this can be exported into excel file in just one click</li>
							<li class="demo-li">o You can also record your payments to foreign vendors here & forex gain/loss related to those transactions</li>
							<li class="demo-li">o Forex gain/loss will be calculated based on the difference between the currency rate on the date of expense record & expense payment date</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Payments</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can record all of your expense payment with payment status and mode of payment, and you can also upload supporting documents for the same</li>
							<li class="demo-li">o Asset payment is also recorded under this module with payment status & supporting documents</li>
							<li class="demo-li">o The salary payment amount is also recorded here, and you can also provide additional details such as EPF/ESIC/TDS etc.</li>
							<li class="demo-li">o Tax payments are also recorded under this, whether it is GST, Equalization Levy, Employee Tax or even TDS(Employee & Expense)</li>
							<li class="demo-li">o And for your preference, all of this can be exported into excel file in just one click</li>
							<li class="demo-li">o You can also record your payments to foreign vendors here & forex gain/loss related to those transactions</li>
							<li class="demo-li">o Forex gain/loss will be calculated based on the difference between the currency rate on the date of expense record & expense payment date</li>
						</ul>
						</div>
						
                  </div>
                  
				  <div id="test9" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>My Employees</strong></p>
						<p><strong>a) Employee Master</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Under this you keep a record of your employees, you create your employees and enter their essential details such as department, designation, contact details, and you can also set their date of appraisal and set a reminder for the same</li>
							<li class="demo-li">o You can also upload their documents such as id proof, educational certificates or any other such documents</li>
							<li class="demo-li">o You can also give access to your employees, so they don't have to provide a physical expense voucher they can directly upload them with their credential of xebra</li>
							<li class="demo-li">o Employees will only see the transactions or information which is related to them, every category of employee will have a different kind of access</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>a) Appraisal</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You record the appraisal details of your employee here; you can write comments, improvement areas and also the CTC with appraisal & set date from when the appraisal will take effect.</li>
						</ul>
						</div>
                  </div>
				  
				  <div id="test10" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>My Tax Summary</strong></p>
						<p><strong>a) TDS</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o It will show the TDS amount, date and even the status of TDS(whether pending or paid) for clients, vendors & also for employees</li>
							<li class="demo-li">o You can export or mail all of the TDS (for Vendor/Client/Employee), and you can compare them with 26AS check the status for TDS payment</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) GST</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o It shows GSTR1 & GSTR3B details that are used to calculate the outstanding GST amount</li>
							<li class="demo-li">o You can select the time-period and export it to excel with a click</li>
							<li class="demo-li">o With the help of this, reconciliation of GST becomes easy. You can check your GST payment status, whether it is fully or partially paid. </li>
							<li class="demo-li">o You already have all your data of GST in My Tax Summary you can just compare them, and you will know if your client or vendor haven't paid your GST share</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Equalisation Levy</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o It is a type of direct tax which is paid by service recipient only if payment is made to a non-resident service provider and you can see the status or pending amount of such transactions in here</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Employee Tax & Reimbursement</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can see the summary of all the employee-related tax/payments or reimbursement details in here</li>
							<li class="demo-li">o You can know the EPF/ESIC/Professional Tax amount that was deducted from employees' salary </li>
							<li class="demo-li">o Expense voucher payments are also shown here. To record an expense voucher, the employee has to login with his credentials</li>
						</ul>
						</div>
                  </div>
				  
				  <div id="test11" class="col s12 inner_row_container">
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>My Accounts</strong></p>
						<p><strong>a) Chart of Account</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o Xebra covers almost every account required to run a business, but if there is any extra account that you wish to prepare, you can do it here</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Journal Vouchers</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o There are specific entries like investments, loans etc. that aren't recorded elsewhere in any module. Those can be done by first creating a relevant account in Chart of Account and then passing appropriate Journal Voucher entry. </li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) General Ledger</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o In here you can see General Ledger which is automatically created for all the accounts that you passed entries for, even the new account you created from chart of account</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Trial Balance</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o The trial balance will also be automatically generated for all the accounts from general ledger as a statement</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>e) Accounting Reports</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o These include the P&L and Balance Sheet of the company. These are also automatically prepared and give a real-time status to the user </li>
						</ul>
						</div>
                  </div>
				  
				  <div id="test12" class="col s12 inner_row_container">
						
						<div class="row document-box-view" style="margin-top:-3%;">
						<p><strong>Cohorts</strong></p>	
						<p><strong>a) Marketplace</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o In the marketplace, you can seek out companies providing different products or services that you might need from time to time</li>
							<li class="demo-li">o You can filter on the category and/or city basis for sharper results</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>b) Connections</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o In this module, you connect your company with other companies that might be your client, vendor or both or neither of it.</li>
							<li class="demo-li">o Once connected, you will be able to send your invoices faster as it's within the same eco-system</li>
							<li class="demo-li">o You can also see your connections here within cohorts, and if you so desire to connect with any company or firm you can send them a request, and you can check their status in the connection request</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>c) Events</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can add an upcoming event which you are aware of and which can be of interest to others also</li>
							<li class="demo-li">o You can also see the events which interest you, and you can set a reminder for the same</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>d) Deals</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o You can create a deal or redeem other deals that are available on the deals page. </li>
							<li class="demo-li">o You can view their details (such as contact person and validity etc.), and if you want any clarification, then you can contact those company</li>
						</ul>
						</div>
						
						<div class="row document-box-view">
						<p><strong>e) Learning</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">o We have added every accounting term which is essential for day to day transaction of business, and if you forget something or stuck at some point you can refer to these terms for your preference</li>
							<li class="demo-li">o Financial ratios are fundamental to know the current status of the business. So we have added all the significant financial ratios which help us to make decisions.</li>
						</ul>
						</div>
						
                  </div>
				  
				  <!-- Entire Demo -->
				  <div id="test13" class="col s12 inner_row_container">
                     
					 <div class="row document-box-view">
						<p style="margin-top:-3%;"><strong>Entire-time duration: 50 mins</strong></p>
						<p><strong>1) Introduction: (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a)	Xebra is a Business-Intelligence suite</li>
							<li class="demo-li">b)	It helps you understand more about your business and its critical parameters like revenue & profit growth, client wise revenue and credit period, list of expenses, track your assets and even maintain payroll</li>
							<li class="demo-li">c)	It combines invoicing, expense & asset tracking, payroll, tax calculation, banking and accounting</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>2) Setting Up: (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a)	Set up a personal profile where you can give out relevant user-level access to your employees and partners like CA</li>
							<li class="demo-li">b)	Set up company profile where you can add multiple GST no's, opening cash and bank balance and upload critical documents of your company</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>3) Sales Invoice: (10 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">
								a) Preparing Estimate or Sales Invoice
								<ul class="demo-ul">
									<li class="demo-li">i)	You can list out all your clients and items individually through their modules (or)</li>
									<li class="demo-li">ii)	Head to Billing Documents (BD) and begin making the invoice</li>
									<li class="demo-li">iii) Action buttons on BD submodule as it is the central hub</li>
								</ul>	
							</li>
							<li class="demo-li">
								b) Expense vouchers
								<ul class="demo-ul">
									<li class="demo-li">i)	Your employees can log in and create expense vouchers on your company or reimbursable from your client. There is an approval hierarchy process that can be set for each one of them if required</li>
									<li class="demo-li">ii)	You can attach any of those vouchers with an invoice to a particular client</li>
								</ul>
							</li>
							<li class="demo-li">
								c) Sales Receipt
								<ul class="demo-ul">
									<li class="demo-li">i) Once you receive the payment from the client, you can record it in this module</li>
									<li class="demo-li">ii) In the case of an international client, you can enter the forex rate, and the system will automatically calculate the net earnings</li>
								</ul>
							</li>
							<li class="demo-li">
								d) Tax Summary
								<ul class="demo-ul">
									<li class="demo-li">i) TDS section will let you know the exact amount of TDS deducted by your client and whether he has paid it. Record it with toggle</li>
									<li class="demo-li">ii)	GST summary is your GSTR1 that you have to file with the government. Simply export it to excel and submit it on the GSTN portal. No more extra work and a considerable saving of time!</li>
								</ul>
							</li>
							<li class="demo-li">
								e) Client portal
								<ul class="demo-ul">
									<li class="demo-li">i) Give access to the primary user of the client, so he can view and download invoices, payments, credit notes, TDS and documentation</li>
									<li class="demo-li">ii)	Each client can only see their entries. Also, they can't edit any documents</li>
								</ul>
							</li>
						</ul>
					 </div>
                  
					 <div class="row document-box-view">
						<p><strong>4) Business Analytics (10 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">
								a) Clients & Revenue
								<ul class="demo-ul">
									<li class="demo-li">i) Track your company's growth rate: MTD & YTD level</li>
									<li class="demo-li">ii)	Understand client and item-wise revenues. Also, compare last year's growth vis-à-vis current one's vis-à-vis target numbers</li>
								</ul>
							</li>
							<li class="demo-li">
								b) Credit History
								<ul class="demo-ul">
									<li class="demo-li">i) Easy tracking of invoices that are due from 30 days onwards to 90 days</li>
									<li class="demo-li">ii)	Identify your critical promising clients from the ones that are holding you back</li>
								</ul>
							</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>5) Expense Booking: (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) Creating vendor and expense master as the master database</li>
							<li class="demo-li">b) Recording company expense received from the vendor and raising Purchase Order and Debit Notes</li>
							<li class="demo-li">
								c) Payment Entry
								<ul class="demo-ul">
									<li class="demo-li">i) Record expense payments and salary payments</li>
									<li class="demo-li">ii)	Record Tax payment entries for GST, TDS, Equalization Levy & Professional Tax</li>
								</ul>
							</li>
							<li class="demo-li">
								d) Vendor portal
								<ul class="demo-ul">
									<li class="demo-li">i) Give access to the primary user of the vendor so he can view and download invoices, payments, credit notes, TDS and documentation</li>
									<li class="demo-li">ii)	Each vendor can only see their entries. Also, they can't edit any documents</li>
								</ul>
							</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>6) Employees & Payroll: (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) Create employee master with all their details and upload their documents</li>
							<li class="demo-li">b) Provide access to them so they can record their expense vouchers</li>
							<li class="demo-li">c) Record salary expense for each employee and email their salary slip directly</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>7) Assets & Depreciation (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) List out the assets purchased and sold across various branches and make entries</li>
							<li class="demo-li">b) Asset tracker allows you to list out all the company assets and their location & usage</li>
							<li class="demo-li">c) Easy to calculate depreciation schedule for your assets</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>8) Cash & Bank (5 mins)</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) All the entries will come in from receipts and payments</li>
							<li class="demo-li">b) You can also record the deposit or withdrawal of fund from any of your listed banks</li>
							<li class="demo-li">c) Petty cash transaction can also be seen under this module</li>
							<li class="demo-li">d) You can also transfer cash to petty cash from cash statement</li>
							<li class="demo-li">e) You can also see the transaction of interbank fund transfer</li>
							<li class="demo-li">f) Card entries are also recorded here you can see all the card related transaction in one place (Debit/Credit card both)</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>9) My Tax Summary</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) This module will provide you with tax summary related to your all business transactions, whether it is TDS (Clients/Vendors/Employees) or GST or Employee Tax & reimbursement</li>
							<li class="demo-li">b) You can track all of the tax-related payments in one place, which will be helpful to know the tax expense for your business.</li>
							<li class="demo-li">c) Petty cash transaction can also be seen under this module</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>10) My Account</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) It is an automated accounting module; every entry that you have made is reflected here.</li>
							<li class="demo-li">b) You can also create a different account in 'Chart of Account' as per your requirement</li>
							<li class="demo-li">c) There is also a sub-module known as JV; you can pass your vouchers entries here (E.g. Prepaid expense)</li>
							<li class="demo-li">d) This module will help you to save time as you don't have to pass separate entries to create a ledger, trial balance, P&L or balance sheet. Every entry that you record will automatically update all the accounting reports</li>
							<li class="demo-li">e) You also have the option to export those accounts or financial statement for reviews.</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>10) My Account</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) It is an automated accounting module; every entry that you have made is reflected here.</li>
							<li class="demo-li">b) You can also create a different account in 'Chart of Account' as per your requirement</li>
							<li class="demo-li">c) There is also a sub-module known as JV; you can pass your vouchers entries here (E.g. Prepaid expense)</li>
							<li class="demo-li">d) This module will help you to save time as you don't have to pass separate entries to create a ledger, trial balance, P&L or balance sheet. Every entry that you record will automatically update all the accounting reports</li>
							<li class="demo-li">e) You also have the option to export those accounts or financial statement for reviews.</li>
						</ul>
					 </div>
					 
					 <div class="row document-box-view">
						<p><strong>11) My Resource Center</strong></p>
						<ul class="demo-ul">
							<li class="demo-li">a) Last but not least we have also added a module for your preference, in this module you can see that we have FAQs for any queries you have or might face in any of the above modules</li>
							<li class="demo-li">b) We have also added the Accounting terms & financial ratio in this module which are required for conducting business.</li>
						</ul>
					 </div>
					 
				  </div>
				  
				  </div>
                </div>
              </div>

        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->


      <!-- ================================================
    Scripts
    ================================================ -->
	<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
    <?php $this->load->view('template/footer'); ?>


<?php $this->load->view('template/footer.php');?>