<?php //$this->load->view('client_portal/client-portal-header'); ?>
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:150px !important;
		}
		
		.select_company{
			width:210px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#ca_partner_table_length .dropdown-content {
			min-width: 95px;
			margin-top:-50% !important;
		}

		#ca_partner_table_length{
			border:1px solid #B0B7CA;
			height:38px;
			border-radius:4px;
			width:110px;
			margin-top:5px;
			margin-left:52%;
		}

		#ca_partner_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.subscriber_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		#ca_partner_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		.subscriber_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}
		
		.subscriber_bulk_action label{
			margin-left:0px !important;
		}
		
		.subscriber_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}
		
		.subscriber_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}
		
		a.addmorelink {
			margin-top:-20px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		.dropdown-content.sub_bulk{
			margin-top:-5px !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(9){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(10){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(11){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(12){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(13){
			text-align:center !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		
		td.profil-img {
			text-align: left;
			vertical-align: middle;
		}
		.city-select{ width:17%; }
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp sub-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">CA Partners<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<!--li><a href="">CA</a-->
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
				<div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_partfilter();" title="Reset all">Reset</a>
                </div>
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
						<li><a id="download_multiple_partner"  data-multi_part="0" ><i class="dropdwon-icon icon download"></i>EXPORT</a></li>
						<!-- <li hidden><a id="send_email_sub_modal"><i class="dropdwon-icon icon email"></i>Email</a></li>
						<li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
						<!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>>
						<li hidden><a  id="deactive_multiple_subscriber"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li-->
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_sub" id="search_sub" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select style="padding-left:10px !important;" name="search_bycompany" id="search_bycompany" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country select_company'>
						<option value="">COMPANY NAME</option>
						<option value="">ALL</option>
						<?php foreach($company as $cmp){ ?>
							<option value="<?=$cmp->company?>"><?=strtoupper($cmp->company);?></option>
						<?php }?>
				   </select>
				   
				   <select style="padding-left:10px !important;" name="search_bybuzz" id="search_bybuzz" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country'>
						<option value="">TYPE OF BUSINESS</option>
						<option value="">ALL</option>
						<?php foreach($business_type as $bus){ ?>
							<option value="<?=$bus->reg_bus_type?>"><?= strtoupper($bus->reg_bus_type);?></option>
						<?php }?>
				   </select>

				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys city-select' id="search_by_city" name="search_by_city">
						<option value="">SELECT CITY</option>
						<option value="">ALL</option>
						<?php foreach($city as $cty){ 
                                $cityname=$this->Admin_dashboard_model->selectData('cities',"*",array('city_id'=>$cty->city));
							?>
							<option value="<?=$cty->city?>"><?= $cityname[0]->name?></option>
						<?php }?>
				   </select>
				   
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg' id="search_by_reg_date" name="search_by_reg_date">
                    <option value="">DAYS SINCE REG</option>
                    <option value="">ALL</option>
                    <option value="15">15-21</option>
                    <option value="22">21-30</option>
                    <option value="31">31+</option>
				   </select>
				   
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="sub_start_date" name="sub_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="sub_end_date" name="sub_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                  <table id="ca_partner_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
						<tr>
							<th style="width:5%">
								<input type="checkbox" class="purple filled-in" name="partner_bulk" id="partner_bulk"/>
								<label for="partner_bulk"></label>
							</th>
							<th style="width: 10%; padding-top:0px !important;">REG. ID</br>REG. DATE</th>
							<th style="width: 15%; text-align:center;">COMPANY NAME</br>& TYPE OF BUSINESS</th>
							<th style="width: 10%; text-align:center;">CA NAME</th>
							<th style="width: 10%; text-align:center;">EMAIL ID</th>
							<th style="width: 10%; text-align:center;">MOBILE NO</th>
							<th style="width: 10%; text-align:center;">CITY</br>COUNTRY</th>
							<th style="width: 10%; text-align:center;">DAYS SINCE</br> REG.</th>
							<th style="width: 10%; text-align:center;">PARTNERS TIER</th>
							<th style="width: 5%; text-align:center;">ACTIONS</th>
                        </tr>
                        <tr>
							<th style="width:5%;"></th>
							<th style="width: 10%; padding-top:0px !important;"></th>
							<th style="width: 15%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 5%; text-align:center;"></th>
                        </tr>
                    </thead>
                    <tbody class="scrollbody">
						<!--<tr>
							<td style="width:10px">
								<input type="checkbox" class="purple filled-in sub_bulk_action" name="subscriber_bulk" id="subscriber_bulk"/>
								<label for="subscriber_bulk"></label>
							</td>
							<th style="width: 80px; padding-top:0px !important;">01-04-2019</th>
                        <td style="width: 80px">Reg01</td>
						<td style="width: 180px">Suhas Sawant</td>
                        <td style="width: 100px">suhas@gmail.com</td>
						<td style="width: 80px">8527419630</td>
						<td style="width: 80px">India</td>
						<td style="width: 80px">Mumbai</td>
                        <td style="width: 80px">Yes</td>
						<td style="width: 80px">9</td>
						<td style="width: 80px">No</td>
						<td style="width: 100px">-</td>
						</tr>-->
                    </tbody>
                  </table>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

   <div id="send_email_subscriber_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
   	 <?php //$this->load->view('admin_dashboard/email_subscriber_details'); ?>
	</div>

      <!-- END MAIN -->
	  <script>
		var bulk_activity = [];
		$("input[id='partner_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".capartner_bulk_action").prop('checked', true);
				$(".capartner_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_partner').attr('data-multi_part',bulk_activity);
			}
			else {
				$(".capartner_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_partner').attr('data-multi_part',0);
			}
		});
		
		function reset_partfilter(){
			$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
			$('select').material_select();
			capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
			$('select').material_select();
		}
	  </script>
      <?php $this->load->view('template/footer'); ?>
