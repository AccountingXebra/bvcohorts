<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('CA PARTNER DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'CA PARTNER DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'REGISTRATION ID');
$this->excel->getActiveSheet()->setCellValue('B3', 'REGISTRATION DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'COMPANY NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'TYPE OF BUSINESS');
$this->excel->getActiveSheet()->setCellValue('E3', 'CA NAME');
$this->excel->getActiveSheet()->setCellValue('F3', 'EMAIL ID');
$this->excel->getActiveSheet()->setCellValue('G3', 'MOBILE NO');
$this->excel->getActiveSheet()->setCellValue('H3', 'COUNTRY');
$this->excel->getActiveSheet()->setCellValue('I3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('J3', 'DAYS SINCE REGISTRATION');
$this->excel->getActiveSheet()->setCellValue('K3', 'PARTNER TIER');
$this->excel->getActiveSheet()->getStyle("A3:K3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	
    $curr_date=date('Y-m-d');
			$validity_date=date('d-m-Y',strtotime($value['createdat']));     
			
			$date1 = date_create($curr_date);
			$date2 = date_create($validity_date);
			$diffr=$date1->diff($date2);
			$days = $diffr->days;

			$country=$this->Admin_dashboard_model->selectData("countries","*", array('country_id' => $value['country'] ));

			$cities=$this->Admin_dashboard_model->selectData("cities","*", array('city_id' => $value['city'] ));
			
			$regid = "CA".$value['reg_id'];
			
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $regid);
	$this->excel->getActiveSheet()->setCellValue('B'.$count,date('d-m-Y',strtotime($value['createdat'])));
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['company']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['reg_bus_type']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['reg_username']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count,$value['reg_email']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['reg_mobile']);
	$this->excel->getActiveSheet()->setCellValue('H'.$count,$country[0]->country_name );
	
	$this->excel->getActiveSheet()->setCellValue('I'.$count, $cities[0]->name);
	$this->excel->getActiveSheet()->setCellValue('J'.$count,$days);
	$this->excel->getActiveSheet()->setCellValue('K'.$count, "");
	
	$count++;
}

$filename='CA Partner Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>