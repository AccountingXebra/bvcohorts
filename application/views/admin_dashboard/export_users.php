<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('USERS DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'USERS DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'REG ID');
$this->excel->getActiveSheet()->setCellValue('B3', 'REG DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'USER NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'EMAIL ID');
$this->excel->getActiveSheet()->setCellValue('E3', 'MOBILE NO');
$this->excel->getActiveSheet()->setCellValue('F3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('G3', 'COUNTRY');
$count=4;
foreach ($result as $key => $value) {

  		$created_at =  date("d-m-Y", strtotime($value['createdat']));
	
	$this->excel->getActiveSheet()->setCellValue('A'.$count, 'REG'.$value['reg_id']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $created_at);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['reg_username']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count,  $value['reg_email']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['reg_mobile']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['name']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['country_name']);
	$count++;
}

$filename='Users Details'.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>