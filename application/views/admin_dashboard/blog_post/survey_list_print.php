<style type="text/css">
	@page {
        size: auto; 
        margin: 0mm;  
    }
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="11"><h2 style="margin: 0">SURVEY DETAILS</h2></th></tr>
	<tr>
		<th>DATE</th>
		<th>EMAIL</th>
		<th>REASON</th>
	</tr>
</thead>
<tbody>
	<?php  foreach($result as $value){ ?>
	<tr>

				
		<td><?=date('d-m-Y',strtotime($value['created_at']));?></td>
		<td><?=$value['email'];?></td>
		<td><?=$value['reason'];?></td>
	</tr>
	<?php } ?>	
</tbody>
</table>
<script type="text/javascript">
	window.print();
</script>