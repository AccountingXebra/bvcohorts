<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <!-- START MAIN -->
<style type="text/css">

	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}

	.border-split-form .select-wrapper {
		padding: 7px 0 2px 0 !important;
	}
	
	input.full-bg:not(.first)  {
		border-top: 0 !important; 
	}
  
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute; 
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	} 
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px; 
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}
	
	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 0px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:35px !important; 
		line-height: 35px;
	}
	
	.nature_of_code{
		border-right: 1px solid #EEF2FE !important;
	}
	
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 
	
	select.error + label.error:not(.active){
		margin: -15px 0 0 -10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	
	.step2.footer-btns {
		margin: 10.3% 0 0 0 !important;
	}
</style>

    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" id="edit_coupon_code" method="post" action="<?php echo base_url(); ?>Admin_dashboard/edit_coupon_code" enctype="multipart/form-data">
  <section id="content">
    <div class="container">
      <div class="plain-page-header">
        <div class="row">
          <div class="col l6 s12 m6">
            <a class="go-back bg-l underline" href="<?php echo base_url();?>admin_dashboard/coupon_list_view">Back to My Coupon</a> 
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
         <div class="page-content">
        	<?php //echo '<pre>'; print_r($result); echo '</pre>'; ?>
            <div class="row">
              <div class="col s12 m12 l3"></div>
                <div class="col s12 m12 l6">
                    
                    <div class="row">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-header">
                          <h3 class="box-title">Edit Coupon</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field col s12 m12 l6 padd-n">
                                  <label for="coupon_code" class="full-bg-label">CREATE COUPON CODE<span class="required_field"> *</span></label>
                                 <input id="coupon_code" name="coupon_code" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->coupon_code; ?>">
                              </div>
                              <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important;">
                                <label for="coupon_date"class="full-bg-label">CODE DATE <span class="required_field"> *</span></label>
                                <input id="coupon_date" name="coupon_date"class="new-date bdatepicker full-bg adjust-width" type="text" readonly="readonly" value="<?= date("d/m/Y",  strtotime($result[0]->code_date)) ?>">
                              </div>
							  <div class="col s12 m12 l6 nature_of_code input-set">
                                <div class="input-field label-active">
                                    <label for="coupon_nature" class="full-bg-label select-label">NATURE OF CODE<span class="required_field"> *</span></label>
                                    <select id="coupon_nature" name="coupon_nature" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                        <option value="1" <?php echo ($result[0]->nature_of_code == 1)?'selected':''; ?>>Coupon Code</option>
                                        <option value="2" <?php echo ($result[0]->nature_of_code == 2)?'selected':''; ?>>Referal Code</option>
                                    </select>
                                </div>
                              </div>
							  <div class="input-field col s12 m12 l6" style="height:74px; border-right:1px solid #eef2fe;">
                                    <label style="margin-left:-10px;" for="pack_select" class="full-bg-label select-label">SELECT PACK<span class="required_field"> *</span></label>
                                    <select id="pack_select" name="pack_select" class="country-dropdown modereceipt check-label">
                                        <option value=""></option>
                                        <option value="all" <?php if($result[0]->pack_select=="all"){echo "selected";} ?>>All</option>
                                       
										<option value="trot" <?php if($result[0]->pack_select=="trot"){echo "selected";} ?>>TROT</option>
										<option value="canter" <?php if($result[0]->pack_select=="canter"){echo "selected";} ?>>CANTER</option>
										<option value="gallop" <?php if($result[0]->pack_select=="gallop"){echo "selected";} ?>>GALLOP</option>
                                    </select>
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n referal_name_div" hidden>
                                 <label for="referal_name" class="full-bg-label">REFERAL NAME <span class="required_field"> *</span></label>
                                 <input id="referal_name" name="referal_name" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->referrel; ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="coupon_desc" class="full-bg-label">DESCRIPTION<span class="required_field"> *</span></label>
                                 <input id="coupon_desc" name="coupon_desc" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->description; ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="coupon_tg" class="full-bg-label">TARGET GROUP<span class="required_field"> *</span></label>
                                 <input id="coupon_tg" name="coupon_tg" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->target_group; ?>">
                              </div>
                               <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="coupon_discount" class="full-bg-label">% DISCOUNT<span class="required_field"> *</span></label>
                                 <input id="coupon_discount" name="coupon_discount" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->discount; ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n">
                                 <label for="coupon_validity" class="full-bg-label">VALIDITY PERIOD<span class="required_field"> *</span></label>
                                 <input id="coupon_validity" name="coupon_validity" class="full-bg adjust-width first border-right" type="text"  value="<?php echo $result[0]->validity; ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important; border-right:1px solid #EEF2FE !important;">
                                <label for="coupon_start_date"class="full-bg-label">START DATE<span class="required_field"> *</span></label>
                                <input id="coupon_start_date" name="coupon_start_date"class="new-date bdatepicker full-bg adjust-width icon-calendar-green" type="text" readonly="readonly" value="<?= date("d/m/Y",  strtotime($result[0]->start_date)) ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important; border-right:1px solid #EEF2FE !important;">
                                <label for="coupon_end_date"class="full-bg-label">END DATE<span class="required_field"> *</span></label>
                                <input id="coupon_end_date" name="coupon_end_date"class="new-date bdatepicker full-bg adjust-width icon-calendar-red" type="text" readonly="readonly" value="<?= date("d/m/Y",  strtotime($result[0]->end_date)) ?>">
                              </div>
                              <input type="hidden" name="id" value="<?php echo $result[0]->promo_id; ?>">
							 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	</div>
    </div>
    </section>
    <div class="col s12 m12 l12">
		<div class="footer-pay step2 footer-btns last-sec">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <div class="col s12 m12 l5"></div>
                <div class="col s12 m12 l2">
                    <div class="text-center">
						<a class="view"><i class="material-icons know-more-form">keyboard_arrow_up</i></a>
                    </div>
                </div>
                <div class="col s12 m12 l5">
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>Admin_dashboard/coupon_list_view'" style="margin-right:10px !important;">Cancel</button>
                </div>
              </div>
            </div>
        </div>
    </div>
    </form>
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>

<div id="review" class="modal fade">
    <div class="row" style="margin-top:30px;">
      <div class="col s12 m12 l12">
      
      <div class="col s12 m12 l10" style="margin-left:43px;">
          <label style="font-size:18px; color:black; text-align:center;"><b>Your total invoiced amount doesn't tally with </b></label><br>
          <label style="font-size:18px; color:black; margin-left:48px;"><b>the total receipt amount entered</b></label>
      </div>
                      
      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="right">
            <button type="submit" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">REVIEW</button>
            <button type="button" id="SubForm"  class="btn-flat theme-flat-btn theme-btn theme-btn-large">PROCEED</button>
        </div>
      </div>
</div>
</div>

	<script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.select2-selection__rendered').each(function () {
           $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
        });
	  });
	</script>
      <!-- jQuery Library -->
  <?php $this->load->view('template/footer.php');?>
      <!-- END MAIN -->