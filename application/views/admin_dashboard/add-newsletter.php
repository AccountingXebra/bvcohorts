<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
<!-- START MAIN -->
<style type="text/css">
	ul.dropdown-content.select-dropdown{
		width:100% !important;
	}
	#cke_blog_content{
		margin:6% 0 0 0;	
	}
	#featured_image{
		margin:6% 0 0 0 !important;
		width:100% !important;
		border-bottom: none !important;
		border-top: none !important;
	}
	#newsletter_file{
		margin:6% 0 0 0 !important;
		width:100% !important;
		border-bottom: none !important;
		border-top: none !important;
	}
	#news_image{
		margin:6% 0 0 0 !important;
		width:100% !important;
		border-bottom: none !important;
		border-top: none !important;
	}
	#excerpt{
		background-color: transparent !important;
		height: 150px !important;
		border-bottom: none !important;
		padding:32px 0 0 20px !important;
		color:#313131;
		font-weight: 500;
	}
	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}
	.border-split-form .select-wrapper {
		padding: 0px 0 2px 0 !important;
		height:85px;
		top:30px;
	}
	
	input.full-bg:not(.first)  {
		border-top: 0 !important; 
	}
  
	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute; 
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	} 
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px; 
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}
	
	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 11px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
		  top:0px;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:35px !important; 
		line-height: 35px;
	}
	
	.nature_of_code{
		border-right: none;
	}
	
	#create_post .error{
		margin: 18px 0 0 10px;
	} 
	
	#create_post label.error:not(.active){
		margin: -15px 0 0 10px; 
	}
	
	select + label.error.active{
		margin-left: -10px;
	}
	
	.step2.footer-btns {
		margin: 10.3% 0 0 0 !important;
	}
	#toast-container {
		top:10% !important;
	}
	#create_post .input-field label.full-bg-label:not(.label-icon).active {
		margin: 14px 2px !important;
	}
	#create_post .input-field.padd-n label {
		left: 11px;
	}
	
	.cke_dialog_ui_input_text{
		height:35px !important;
		margin-bottom:5px !important;
	}
	.cke_dialog_ui_input_select{
		display:block !important;
		height:35px !important;
		
	}
	#blog_content body.add-vo .invoi-drop input, select, textarea {
		height:100% !important;
	}
	.page-content{ margin-top:2%; }
	.select-wrapper input.select-dropdown{margin-left:10px !important;}
	#create_newsletter .error{ margin-left:31px; }
</style>

    <div id="main">
      <!-- START WRAPPER -->
    <div class="wrapper">
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" id="create_newsletter" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin_dashboard/submit_newsletter">
			<section id="content" class="bg-theme-gray documents-search">
			<div class="container">
				<div class="plain-page-header">
					<div class="row">
						<div class="col l6 s12 m6">
							<a class="go-back bg-l underline" href="<?php echo base_url();?>admin_dashboard/newsletter_list">Back to My Newsletter</a> 
						</div>
						<div class="col l6 s12 m6"></div>
					</div>
					<div class="page-content">
					<div class="row">
					<div class="col s12 m12 l3"></div>
						<div class="col s12 m12 l6">
						<div class="row">
							<div class="box-wrapper bg-white shadow border-radius-6">
								<div class="box-body">
								<div class="row">
									<div class="input-field col s12 m12 l12 padd-n">
										<label for="newsletter_name" class="full-bg-label">NEWSLETTER TITLE<span class="required_field"> *</span></label>
										<input id="newsletter_name" name="newsletter_name" class="full-bg adjust-width first border-right" type="text"  value="">
									</div>
									<div class="input-field col s12 m12 l12 padd-n">
										<label for="newsletter_url" class="full-bg-label">NEWSLETTER CUSTOMISE URL<span class="required_field"> </span></label>
										<input id="newsletter_url" name="newsletter_url" class="full-bg adjust-width first border-right" type="text" value="">
									</div>
									<div class="input-field col s12 m12 l12 padd-n" style="border-top:1px solid #eef2fe;">
										<label style="margin:7px 0 0 0;" for="newsletter_year" class="full-bg-label active">NEWSLETTER YEAR <span class="required_field"> *</span></label>
										<?php $year=date('Y'); ?>	
										<select id="newsletter_year" name="newsletter_year" class="country-dropdown modereceipt check-label">
											<option value="0" selected>Select Year</option>
											<?php for($i=$year;$i > $year-5;$i--){ ?>
												<option value="<?=$i?>"><?=$i?></option>
											<?php } ?>
										</select>
									</div>
									<div class="input-field col s12 m12 l12 padd-n" style="border-top:1px solid #eef2fe;">
										<label style="margin:7px 0 0 0;" for="newsletter_month" class="full-bg-label active">NEWSLETTER MONTH <span class="required_field"> *</span></label>	
										<select id="newsletter_month" name="newsletter_month" class="country-dropdown modereceipt check-label">
											<option value="0" selected>Select Month</option>
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</div>
									<div class="input-field col s12 m12 l12 padd-n">
										<label for="newsletter_contain" class="full-bg-label">NEWSLETTER CONTAIN URL<span class="required_field"> *</span></label>
										<input id="newsletter_contain" name="newsletter_contain" class="full-bg adjust-width first border-right" type="text" value="">
									</div>
									<div class="input-field col s12 m12 l12 padd-n" style="border-top:1px solid #eef2fe;">
										<label for="newsletter_file" class="full-bg-label">NEWSLETTER FILE<span class="required_field"> *</span></label>
										<input id="newsletter_file" name="newsletter_file" class="full-bg adjust-width first border-right" type="file"  value="">
									</div>
									<div class="input-field col s12 m12 l12 padd-n" style="border-top:1px solid #eef2fe;">
										<label for="featured_image" class="full-bg-label">COVER IMAGE<span class="required_field"> *</span></label>
										<input id="featured_image" name="featured_image" class="full-bg adjust-width first border-right" type="file"  value="">
									</div>
								</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12 mt-2">
								<div class="form-botom-divider"></div>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l6 right">
								<input type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right" value="Save">
								<button  class="btn-flat theme-flat-btn theme-btn theme-btn-large right mr-5" type="reset" >Cancel</button>
							</div>
						</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </section>
    </form>
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>
	
	<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.js-example-basic-single').select2();
			$('.select2-selection__rendered').each(function () {
				$(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
			});
		});
	</script>
	<!-- jQuery Library -->
	<?php $this->load->view('template/footer.php');?>
	<!-- END MAIN -->