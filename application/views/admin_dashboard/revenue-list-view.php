<?php //$this->load->view('client_portal/client-portal-header'); ?>
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.dataTables_scrollHead{
			margin-bottom:-15px;
		}
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.rev_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:140px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.expense-add{
			width:215px !important;
		}

		.expense-add > input.select-dropdown {
			max-width: 169px !important;
			font-size: 13px !important;
			color:#ffff !important;
		}

		.expense-add span.caret {
			margin-right: 10px;
			color: #ffff !important;
		}
		.dataTables_length {
			margin-left: 500px;
		}

		#my-revenue-admin_length .dropdown-content {
			min-width: 95px;
		}

		#my-revenue-admin_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:105px;
			margin-top:5px;
			margin-left:790px;
		}

		#my-revenue-admin_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.revenue_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		#my-revenue-admin_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		a.addmorelink {
			margin-top:-20px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		table.dataTable.display tbody tr td:nth-child(1){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(2){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(9){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(10){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(11){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(12){
			text-align:center !important;
		}
		
		.btn-stated {
			background-position: 90px center !important;
		}
		.type_sub{
			width:23%;
		}
		.type_coupon{
			width:23%;
		}
		.select-country{
			margin-left:-9%;
		}
		
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		
		.btn-dropdown-action{
			min-width:155px !important;
		}
	</style>
	<!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp rev-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">My Revenue<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">MY REVENUE</a>
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							<!--<a href="<?php //echo base_url(); ?>sales/add_expense_voucher"class="btn btn-theme btn-large right add-new modal-trigger">ADD EXPENSE VOUCHER</a>-->
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" class="addmorelink right" onclick="reset_srtfilter();" title="Reset all">Reset</a>
							</div>
							<input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="rev_start_date" name="rev_start_date" readonly="readonly">
							<input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="rev_end_date" name="rev_end_date" readonly="readonly">
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l2 s12 m12">
				<div class="col l10 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content rev_bulk'>
					  <!--<li><a><img style="margin-right: 26px;" class="icon-img" src="<?php //echo base_url(); ?>public/icons/payment_rcv.png">Make Payment</a></li>-->
					  <!--<li><a id="email_multiple_voucher"><i class="dropdwon-icon icon email"></i>Email</a></li>-->
					  <li><a id="download_multiple_rev"  data-multi_rev="0" ><i class="dropdwon-icon icon download"></i>DOWNLOAD</a></li>
					  <!--li><a id="download_multiple_rev_invoice"  data-multi_rev="0" ><i class="dropdwon-icon icon download"></i>Download Invoice</a></li-->
					 <!--  <li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
					  <!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>-->
					  <!-- <li><a  id="deactive_multiple_cmp_profiles" ><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li> -->
                    </ul>
				</div>
				<div class="col l2 s12 m12 searchbtn">
					<!--<a class="filter-search btn-search btn">
						<input type="text" name="search" id="search_dc" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>-->
				</div>

                </div>
				<div class="col l10 s12 m12">
				<div class="action-btn-wapper right"><!-- <p class="stort-by">STORT BY :</p> -->
                   
				   <select style="padding-left:10px !important;" name="search_bycountry" id="search_bycountry" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country'>
                    <option value="">SELECT COUNTRY</option>
                    <option value="">ALL</option>
                    <?php 
                    for($i=0;$i<count($country);$i++){
                    	?>
                    	<option value="<?php echo $country[$i]['country_id']; ?>"> <?php echo strtoupper($country[$i]['country_name']); ?> </option>
                    	<?php
                    }
                    ?>
				   </select>

				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys city-select' id="search_by_city" name="search_by_city">
                    <option value="">SELECT CITY</option>
                    <option value="">ALL</option>
                     <?php 
                    for($i=0;$i<count($cities);$i++){
                    	?>
                    	<option value="<?php echo $cities[$i]['city_id']; ?>"> <?php echo strtoupper($cities[$i]['name']); ?> </option>
                    	<?php
                    }
                    ?>
				   </select>
				   
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys type_sub' id="search_by_subsc" name="search_by_subsc">
                    <option value="">SUBSCRIPTION TYPE</option>
                    <option value="">ALL</option>
                    <option value="First Time">FIRST TIME</option>
                    <option value="Renewal">RENEWAL</option>
				   </select>
				   
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys type_pack' id="search_by_package" name="search_by_package">
                    <option value="">PACKAGE TYPE</option>
                    <option value="">ALL</option>
                    <!--option value="Monthly">Monthly</option-->
                    <option value="Yearly">YEARLY</option>
                    <option value="Add-Ons">ADD-ONS</option>
				   </select>
				   
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys type_coupon' id="search_by_coupon" name="search_by_coupon">
                    <option value="">COUPON CODE TYPE</option>
                    <option value="">ALL</option>
                     <?php 
                    for($i=0;$i<count($promo_data);$i++){
                    	?>
                    	<option value="<?php echo $promo_data[$i]->coupon_code; ?>"> <?php echo strtoupper($promo_data[$i]->coupon_code); ?> </option>
                    	<?php
                    }
                    ?>
				   </select>
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                <!--form-->
                  <table id="my-revenue-admin" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:5%;">
							<input type="checkbox" class="purple filled-in" name="revenue_bulk" id="revenue_bulk"/>
							<label for="revenue_bulk"></label>
						</th>
                        <th style="width: 10%; text-align:center; padding-top:0px !important;">INVOICE NO </br>PURCHASE DATE</th>
                        <th style="width: 10%; text-align:center;">CLIENT NAME</th>
						<th style="width: 5%; text-align:center;">CITY</br>COUNTRY</th>
						<th style="width: 8%; text-align:center;">NATURE OF BUSINESS</th>
                        <th style="width: 10%; text-align:center;">SUBSCRIPTION </br>TYPE</th>
						<th style="width: 8%; text-align:center;">PACKAGE </br>TYPE</th>
						<th style="width: 10%; text-align:center;">COUPON CODE </br>TYPE</th>
						<th style="width: 10%; text-align:center;">PURCHASE </br>AMT</th>
						<th style="width: 8%; text-align:center;">DISCOUNT </br>AMT</th>
						<th style="width: 10%; text-align:center;">CREDIT NOTE /</br>REFUND</th>
						<th style="width: 10%; text-align:center;">NET PURCHASE </br>AMT</th>
						<!--<th style="width: 100px">ACTIONS</th>-->
                        </tr>
                        <tr>
                        <th style="width:5%;"></th>
                        <th style="width: 10%; padding-top:0px !important;"></th>
                        <th style="width: 10%;"></th>
						<th style="width: 5%;"></th>
						<th style="width: 8%;"></th>
                        <th style="width: 10%;"></th>
						<th style="width: 8%;"></th>
						<th style="width: 10%;"></th>
						<th style="width: 10%; text-align:center;"></th>
						<th style="width: 8%; text-align:center;"></th>
						<th style="width: 10%; text-align:center;"></th>
						<th style="width: 10%; text-align:center;"></th>
                        </tr>
                    </thead>
                    <tbody class="scrollbody">

                    </tbody>
                  </table>
                <!--/form-->
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

    <div id="send_email_exp_voucher_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	    <?php //$this->load->view('sales/vouchers/exp_voucher_email_popup'); ?>
	</div>

      <!-- END MAIN -->
	  <script>
		var bulk_activity = [];
		$("input[id='activity_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".activity_bulk_action").prop('checked', true);
				$(".activity_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',bulk_cmp_profiles);
			}
			else {
				$(".activity_bulk_action").prop('checked',false);
				bulk_activity = [];
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
			}
		});
		
		function reset_srtfilter(){
		  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
		  $('.js-example-basic-single').trigger('change.select2');
		  $('.btn-date,.search-hide-show').val('');
		  $('select').material_select();
		  adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
		  $('select').material_select();
		}
	  </script>
      <?php $this->load->view('template/footer'); ?>
