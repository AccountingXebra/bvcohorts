 <?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <!-- START MAIN -->
    <style type="text/css">
    
	.table-type1 [type="checkbox"] + label {
		margin: 0 0 0 8px !important;
    }
    
	.dataTables_length {
		margin-left: 500px;
	}

	#personal-profile_length{
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:97px;
		margin-top:5px;
		margin-left:780px;
	}
	
	#personal-profile_length .dropdown-content {
		min-width: 97px;
	}
	
	#personal-profile_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	#personal-profile_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}
	
	.personal_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}
	
	div#breadcrumbs-wrapper {
		margin-top: 5px !important;
	}
	.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
    </style>
    
	<div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">

        <!-- START CONTENT -->
        <section id="content" class="bg-cp profile-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">My Profiles <small class="grey-text">(<?php echo count($data); ?> Total)</small></h5>
                  
                  <ol class="breadcrumbs">
                    <li><a href="#">My Profile</a>
                    </li>
                    <!-- <li class="active">My Personal Profiles</li> -->
                  </ol>
                </div>
                <div class="col s10 m6 l6">
                  <a class="btn btn-theme btn-large right" href="<?php echo base_url();?>admin_dashboard/add_profile">ADD NEW Profile</a>
                </div>
              </div>
            </div>
          </div>

          <!--<div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l6 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown04' class='dropdown-content'>
                      <li><a href="javascript:void(0)" id="download_multiple_profiles" data-multi_download="0"><i class="dropdwon-icon icon download"></i> Download</a></li>
                      <li> <a href="javascript:void(0)" id="deactive_multiple_profiles" data-multi_profiles="0"><i class="dropdwon-icon icon deactivate"></i> Deactivate</a></li>
                    </ul>
                    <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_personal_profile" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>
                </div>
             </div>
            </div>
          </div>-->

    <div class="container">
    <div class="row">
		<div class="col l12 s12 m12">
		<!-- <form>-->
		<table id="personal-profile" class="responsive-table display table-type1 " border="0" cellspacing="0"  width="100%">
			<thead id="fixedHeader">
				<tr>
				 
					
					<th class="comp-sort-name" style="width: 200px;">NAME</th>
					<th style="width: 300px;">EMAIL ID</th>
					<th style="width: 200px;">PHONE NUMBER</th>         
					<th style="width: 150px;">ACCESS</th>
          <th style="width: 150px;">STATUS</th>
					<th style="width: 5px;">ACTION</th>  
				</tr>
			</thead>
			<tbody class="scrollbody">
   <?php foreach ($data as $item) { ?>      
      <tr>
   
      	
          <td disabled="disabled"><?php echo $item->reg_username ; ?></td>
          <td><?php echo $item->reg_email;  ?></td>  
           <td><?php echo $item->reg_mobile; ?></td>
          <td><?php echo $item->reg_admin_type; ?></td> 
           <td><?php echo $item->status; ?></td>         
          
      <td style="width: 50px;"> 
        

						<a href="<?php echo base_url("admin_dashboard/edit_admin_profile/".$item->id)?>">edit</a> /
           <?php if($item->status=="Active"){?> <a href="<?php echo base_url("admin_dashboard/delete/$item->id?status=Inactive")?>">deactivate</a><?php } else{ ?><a href="<?php echo base_url("admin_dashboard/delete/$item->id?status=Active")?>">activate</a> <?php } ?>
            

					</td>   
      </tr>
      <?php } ?>
  </tbody>

			</table>
      <!--</form>-->
      </div>
    </div>
    </div>               
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
 <!-- END MAIN -->
   <?php $this->load->view('template/footer.php');?>