<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('REVENUE DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'REVENUE DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'INVOICE NO');
$this->excel->getActiveSheet()->setCellValue('B3', 'PURCHASE DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'CLIENT NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'COUNTRY');
$this->excel->getActiveSheet()->setCellValue('E3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('F3', 'NATURE OF BUSINESS');
$this->excel->getActiveSheet()->setCellValue('G3', 'SUBSCRIPTION TYPE');
$this->excel->getActiveSheet()->setCellValue('H3', 'PAKAGE TYPE');
$this->excel->getActiveSheet()->setCellValue('I3', 'COUPON CODE');
$this->excel->getActiveSheet()->setCellValue('J3', 'PURCHASE AMOUNT');
$this->excel->getActiveSheet()->setCellValue('K3', 'DISCOUNT AMOUNT');
$this->excel->getActiveSheet()->setCellValue('L3', 'CREDIT NOTE / REFUND');
$this->excel->getActiveSheet()->setCellValue('M3', 'NET PURCHASE AMOUNT');
$this->excel->getActiveSheet()->getStyle("A3:M3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	$purchase_date =  date("d-m-Y", strtotime($value['createdat']));
	if($value['promocode']=='FIRST20'){
		$discount_amount = $value['amount'];
	}else{
		$discount_amount = 0;
	}
	$net_purchase_amt = $value['amount'] - $discount_amount;
	$this->excel->getActiveSheet()->setCellValue('A'.$count, " ");
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $purchase_date);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['bus_company_name']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['country_name']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['name']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['nature_of_bus']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['subscription_type']);
	$this->excel->getActiveSheet()->setCellValue('H'.$count, $value['subscription_plan']);
	
	$this->excel->getActiveSheet()->setCellValue('I'.$count, $value['promocode']);
	$this->excel->getActiveSheet()->setCellValue('J'.$count, $value['amount']);
	$this->excel->getActiveSheet()->setCellValue('K'.$count, $discount_amount);
	$this->excel->getActiveSheet()->setCellValue('L'.$count, "");
	$this->excel->getActiveSheet()->setCellValue('M'.$count, $net_purchase_amt);
	$count++;
}

$filename='Subscriber Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>