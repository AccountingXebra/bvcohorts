<?php //$this->load->view('client_portal/client-portal-header'); ?>
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:200px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#coupon_code_table_length .dropdown-content {
			min-width: 95px;
		}

		#coupon_code_table_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:105px;
			margin-top:5px;
			margin-left:790px;
		}

		#coupon_code_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.coupon_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		#coupon_code_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		.coupon_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}
		
		.coupon_bulk_action label{
			margin-left:0px !important;
		}
		
		.coupon_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}
		
		.coupon_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		a.addmorelink {
			margin-top:-25px !important;
		}
		.dropdown-content.sub_bulk{
			margin-top:10px !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp coupon-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">My Coupons<small class="grey-text">(<span id="rev_count"> </span> <?=count($TotalRecord)?> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">MY COUPONS</a>
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							<a class="btn btn-theme btn-large right" href="<?php echo base_url();?>admin_dashboard/add_coupon_code">ADD NEW COUPON</a>	
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
				<!--<a href="<?= base_url(); ?>admin_dashboard/edit_coupon">Edit Coupon</a>-->
			  <div class="row">
                <div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_couponfilter();" title="Reset all">Reset</a>
				</div>
			  </div>
              <div class="row">
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
					  <li hidden><a id="email_coupon_data"><i class="dropdwon-icon icon email"></i>Email</a></li>
					  <li><a id="download_multiple_coupon"  data-multi_sub="0" ><i class="dropdwon-icon icon download"></i>EXPORT</a></li>
					 <!--  <li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
					  <!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>-->
					  <li hidden><a  id="deactive_multiple_coupon"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li>
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_coupon" id="search_coupon" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg' id="search_by_naturec" name="search_by_naturec">
                    <option value="">NATURE OF CODE</option>
                    <option value="">ALL</option>
                    <option value="1">COUPON CODE</option>
                    <option value="2">REFERRAL CODE</option>
				   </select>
				   
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="coupon_start_date" name="coupon_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="coupon_end_date" name="coupon_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                <form>
                  <table id="coupon_code_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:10px">
							<input type="checkbox" class="purple filled-in" name="coupon_bulk" id="coupon_bulk"/>
							<label for="coupon_bulk"></label>
						</th>
                        <th style="width: 80px; padding-top:0px !important;">CODE NO <br>& DATE</th> 
                        <th style="width: 100px">NATURE OF CODE</th>
						<th style="width: 150px">REFERAL NAME</th> 
                        <th style="width: 150px">DESCRIPTION</th>
						<th style="width: 100px">TARGET GROUP</th>
						<th style="width: 80px">VALIDITY</th>
						<th style="width: 80px">START DATE </th>
                        <th style="width: 80px">END DATE</th>
						<th style="width: 80px">% DISCOUNT</th>
						<th style="width: 80px">Status</th>
						<th style="width: 50px">ACTION</th>
                      </tr>
                    </thead>
                    <tbody class="scrollbody">
						
                    </tbody>
                  </table>
                </form>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

    <div id="send_email_coupon_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	    <?php $this->load->view('admin_dashboard/email_template_for_coupon'); ?>
	</div>

      <!-- END MAIN -->
	  <script>
		var bulk_activity = [];
		$("input[id='coupon_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".coupon_bulk_action").prop('checked', true);
				$(".coupon_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_coupon').attr('data-multi_sub',bulk_activity);
			}
			else {
				$(".coupon_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_coupon').attr('data-multi_sub',0);
			}
		});
		
		function reset_couponfilter(){
		$('.action-btn-wapper').find('select').prop('selectedIndex',0);
		$('.js-example-basic-single').trigger('change.select2');
		$('.btn-date,.search-hide-show').val('');
		$('select').material_select();
		couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
		$('select').material_select();
		}
		
	  </script>
      <?php $this->load->view('template/footer'); ?>
