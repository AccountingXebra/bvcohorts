<?php //$this->load->view('client_portal/client-portal-header'); ?>
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		.dataTables_scrollBody{ margin-top:-20px; }
		#delete_challenge .modal-body{ margin:5px 0 0 0 !important; }
		/*....................*/
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 30px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		.btn-dropdown-select ul.dropdown-content {
			width: auto !important;
			white-space: nowrap;
			height: 300px !important;
			overflow-y: scroll !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:150px !important;
		}
		
		.select_company{
			width:210px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			width:200px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#challange_table_length .dropdown-content {
			min-width: 95px;
			margin-top:-48px !important;
		}

		#challange_table_length{
			border:1px solid #B0B7CA;
			height:38px;
			border-radius:4px;
			width:110px;
			margin-top:5px;
			margin-left:52%;
		}

		#challange_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.challange_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:0px !important;
		}

		#challange_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		.challange_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:0px !important;
		}
		
		.challange_bulk_action label{
			margin-left:0px !important;
		}
		
		.challange_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}
		
		.challange_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}
		
		a.addmorelink {
			margin-top:-20px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		.dropdown-content.sub_bulk{
			margin-top:44px !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:left !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(9){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(10){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(11){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(12){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(13){
			text-align:center !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		
		td.profil-img {
			text-align: left;
			vertical-align: middle;
		}
		.city-select{ width:17%; }
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp sub-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 style="margin: 0 0 0 14px;" class="breadcrumbs-title my-ex">Challenges<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<!--li><a href="">CA</a-->
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							<a class="btn btn-theme btn-large right" href="<?php echo base_url();?>admin_dashboard/add_challenge">ADD NEW CHALLENGE</a>	
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
				<div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_partfilter();" title="Reset all">Reset</a>
                </div>
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
						<li><a id="download_multiple_challange"  data-multi_part="0" ><i class="dropdwon-icon icon download"></i>EXPORT</a></li>
						<!-- <li hidden><a id="send_email_sub_modal"><i class="dropdwon-icon icon email"></i>Email</a></li>
						<li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
						<!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>>
						<li hidden><a  id="deactive_multiple_subscriber"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li-->
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_challange" id="search_challange" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select style="padding-left:10px !important;" name="challange_nature" id="challange_nature" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country'>
						<option value="">NATURE OF BUSINESS</option>
						<option value="">ALL</option>
						<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
										<option value="Advertising">ADVERTISING</option>
										<option value="Animation Studio">ANIMATION STUDIO</option>
										<option value="Architecture">ARCHITECTURE</option>
										<option value="Arts & Crafts">ARTS & CRAFTS</option>
										<option value="Audit & Tax">AUDIT & TAX</option>
										<option value="Brand Consulting">BRAND CONSULTING</option>
										<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
										<option value="Consultant">CONSULTANT</option>
										<option value="Content Studio">CONTENT STUDIO</option>
										<option value="Cyber Security">CYBER SECURITY</option>
										<option value="Data Analytics">DATA ANALYTICS</option>
										<option value="Digital Influencer">DIGITAL INFLUENCER</option>
										<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
										<option value="Direct Marketing">DIRECT MARKETING</option>
										<option value="Entertainment">ENTERTAINMENT</option>
										<option value="Event Planning">EVENT PLANNING</option>
										<option value="Florist">FLORIST</option>
										<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
										<option value="Financial and Banking">FINANCIAL & BANKING</option>
										<option value="Gaming Studio">GAMING STUDIO</option>
										<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
										<option value="Hardware Servicing">HARDWARE SERVICING</option>
										<option value="Industry Bodies">INDUSTRY BODIES</option>
										<option value="Insurance">INSURANCE</option>
										<option value="Interior Designing ">INTERIOR DESIGNING</option>
										<option value="Legal Firm">LEGAL FIRM</option>
										<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
										<option value="Mobile Services">MOBILE SERVICES</option>
										<option value="Music">MUSIC</option>
										<option value="Non-Profit">NON-PROFIT</option>
										<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
										<option value="Photography">PHOTOGRAPHY</option>
										<option value="Printing">PRINTING</option>
										<option value="Production Studio">PRODUCTION STUDIO</option>
										<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
										<option value="Publishing">PUBLISHING</option>
										<option value="Real Estate">REAL ESTATE</option>
										<option value="Recording Studio">RECORDING STUDIO</option>
										<option value="Research">RESEARCH</option>
										<option value="Sales Promotion">SALES PROMOTION</option>
										<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
										<option value="Stock & Shares">STOCK & SHARES</option>
										<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
										<option value="Tours & Travel">TOURS & TRAVELS</option>
										<option value="Training & Coaching">TRAINING & COACHING</option>
										<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
										<option value="Therapists">THERAPISTS</option>
										<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
										<option value="Web Development">WEB DEVELOPMENT</option>
				   </select>
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green eventdatepicker date-cng btn-stated out-line" id="challange_start_date" name="challange_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red eventdatepicker date-cng btn-stated out-line" id="challange_end_date" name="challange_end_date" readonly="readonly">
                </div>
				
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                  <table id="challange_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
						<tr>
							<th style="width:5%">
								<input type="checkbox" class="purple filled-in" name="challange_bulk" id="challange_bulk"/>
								<label for="challange_bulk"></label>
							</th>
							<th style="width: 8%; padding-top:0px !important;">CHALLENGE </br>NO & DATE</th>
							<th style="width: 15%; text-align:center;">CHALLENGE NAME</th>
							<th style="width: 17%; text-align:left;">CHALLENGE DESCRIPTION</th>
							<th style="width: 15%; text-align:center;">NATURE OF BUSINESS</th>
							<th style="width: 10%; text-align:center;">START DATE</th>
							<th style="width: 10%; text-align:center;">LOCATION</th>
							<th style="width: 5%; text-align:center;">ACTIONS</th>
                        </tr>
                        <tr>
							<th style="width:5%;"></th>
							<th style="width: 8%; padding-top:0px !important;"></th>
							<th style="width: 15%; text-align:center;"></th>
							<th style="width: 17%; text-align:center;"></th>
							<th style="width: 15%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 10%; text-align:center;"></th>
							<th style="width: 5%; text-align:center;"></th>
                        </tr>
                    </thead>
                    <tbody class="scrollbody">
						<!--<tr>
							<td style="width:10px">
								<input type="checkbox" class="purple filled-in sub_bulk_action" name="subscriber_bulk" id="subscriber_bulk"/>
								<label for="subscriber_bulk"></label>
							</td>
							<th style="width: 80px; padding-top:0px !important;">01-04-2019</th>
                        <td style="width: 80px">Reg01</td>
						<td style="width: 180px">Suhas Sawant</td>
                        <td style="width: 100px">suhas@gmail.com</td>
						<td style="width: 80px">8527419630</td>
						<td style="width: 80px">India</td>
						<td style="width: 80px">Mumbai</td>
                        <td style="width: 80px">Yes</td>
						<td style="width: 80px">9</td>
						<td style="width: 80px">No</td>
						<td style="width: 100px">-</td>
						</tr>-->
                    </tbody>
                  </table>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

	<div id="send_email_subscriber_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
		<?php //$this->load->view('admin_dashboard/email_subscriber_details'); ?>
	</div>
	
	<div id="delete_challenge" class="modal" style="width:35% !important;">
		<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
		<div class="modal-content">
			<div class="modal-header" style="text-align:center;">
				<h4 style="width:93% !important;"> Delete Challenge</h4>        
				<input type="hidden" id="remove_challenge_id" name="remove_salary_id" value="" />
				<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
			</div>
		</div>
		<div class="modal-body" style="width:100% !important; margin-left:0px !important;">
			<p style="color:#595959; font-size:15px; text-align:center;">Are you sure you want to delete this challenge?</p>
		</div>
		<div class="modal-content">
			<div class="modal-footer">
				<div class="row">
					<div class="col l3 s12 m12"></div>
					<div class="col l9 s12 m12 cancel-deactiv">
						<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
						<button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_challenge_btn">DELETE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
      <!-- END MAIN -->
	  <script>
		
		setTimeout(function(){ 
			reset_partfilter();
		}, 1000);	
		var bulk_activity = [];
		$("input[id='partner_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".capartner_bulk_action").prop('checked', true);
				$(".capartner_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_partner').attr('data-multi_part',bulk_activity);
			}
			else {
				$(".capartner_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_partner').attr('data-multi_part',0);
			}
		});
		
		function reset_partfilter(){
			$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
			$('select').material_select();
			challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
			$('select').material_select();	
		}
	  </script>
	  <script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 300) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
	  </script>
      <?php $this->load->view('template/footer'); ?>
