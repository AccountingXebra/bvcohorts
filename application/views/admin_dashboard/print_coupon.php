<style type="text/css">
	@page {
        size: auto; 
        margin: 0mm;  
    }
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="11"><h2 style="margin: 0">COUPON DETAILS</h2></th></tr>
	<tr>
	 <th>COUPON DATE</th>
	 <th>COUPON CODE</th>
	 <th>NATURE OF CODE</th>
	 <th>REFERAL NAME</th>
	 <th>DESCRIPTION</th>
	 <th>TARGET GROUP</th>
	 <th>VALIDITY</th>
	 <th>START DATE</th>
	 <th>END DATE</th>
	 <th>% DISCOUNT</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($result as $key => $value) { 

		$code_date =  date("d-m-Y", strtotime($value['code_date']));

	  		$start_date =  date("d-m-Y", strtotime($value['start_date']));
	  		$end_date =  date("d-m-Y", strtotime($value['end_date']));

	  		if($value['nature_of_code']==1)
	  		{
	  			$nature_of_code = 'Coupon Code';

	  		}else{

	  			$nature_of_code = 'Referal Code';
	  		}
	?>
	<tr>
		<td><?= $code_date; ?></td>
		<td><?= $value['coupon_code']; ?></td>
		<td><?= $nature_of_code; ?></td>
		<td><?= $value['referrel']; ?></td>
		<td><?= $value['description']; ?></td>
		<td><?= $value['target_group']; ?></td>
		<td><?= $value['validity']; ?></td>
		<td><?= $start_date; ?></td>
		<td><?= $end_date; ?></td>
		<td><?= $value['discount']; ?></td>
	</tr>	
<?php } ?>

</tbody>
</table>
<script type="text/javascript">
	//window.print();
</script>