<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('SUBSCRIBER DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'SUBSCRIBER DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'REGISTRATION DATE');
$this->excel->getActiveSheet()->setCellValue('B3', 'REGISTRATION ID');
$this->excel->getActiveSheet()->setCellValue('C3', 'CLIENT NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'EMAIL ID');
$this->excel->getActiveSheet()->setCellValue('E3', 'MOBILE NO');
$this->excel->getActiveSheet()->setCellValue('F3', 'COUNTRY');
$this->excel->getActiveSheet()->setCellValue('G3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('H3', 'TRIAL PERIOD');
$this->excel->getActiveSheet()->setCellValue('I3', 'DAYS SINCE REGISTRATION');
$this->excel->getActiveSheet()->setCellValue('J3', 'SUBSCRIBED STATUS');
$this->excel->getActiveSheet()->setCellValue('K3', 'NATURE OF CODE');
$this->excel->getActiveSheet()->setCellValue('L3', 'PURCHASE AMOUNT');
$this->excel->getActiveSheet()->getStyle("A3:L3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {


  		$created_at =  date("d-m-Y", strtotime($value['createdat']));

  		if($value['reg_istrial']==0)
  		{
  			$reg_istrial = 'NO';
  			$sub_status = 'YES';

  		}else{
  			$reg_istrial = 'YES';
  			$sub_status = 'NO';
  		}
			
		$date1=date_create(date("Y-m-d", strtotime($value['createdat'])));
		$date2=date_create(date("Y-m-d"));
		$diff=date_diff($date1,$date2);
		$days = $diff->format("%a");


	$this->excel->getActiveSheet()->setCellValue('A'.$count, $created_at);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, 'REG'.$value['reg_id']);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['reg_username']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['reg_email']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['reg_mobile']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['country_name']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['name']);
	
	$this->excel->getActiveSheet()->setCellValue('H'.$count, $reg_istrial);
	$this->excel->getActiveSheet()->setCellValue('I'.$count, $days);
	$this->excel->getActiveSheet()->setCellValue('J'.$count, $sub_status);
	$this->excel->getActiveSheet()->setCellValue('K'.$count, $value['promocode']);
	$this->excel->getActiveSheet()->setCellValue('L'.$count, $value['amount']);
	$count++;
}

$filename='Subscriber Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>