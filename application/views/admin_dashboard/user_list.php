<?php //$this->load->view('client_portal/client-portal-header'); ?>
<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.dataTables_scrollBody{ margin-top:-20px; }	
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:200px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#my-users-admin_length .dropdown-content {
			min-width: 95px;
			margin-top:-48px !important;
		}

		#my-users-admin_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:105px;
			margin-top:5px;
			margin-left:790px;
		}

		#my-users-admin_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.subscriber_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		#my-users-admin_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		.subscriber_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}
		
		.subscriber_bulk_action label{
			margin-left:0px !important;
		}
		
		.subscriber_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}
		
		.subscriber_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}
		
		a.addmorelink {
			margin-top:-20px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		.dropdown-content.sub_bulk{
			margin-top:14px !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		table.dataTable.display tbody tr td:nth-child(1){
			
		}
		table.dataTable.display tbody tr td:nth-child(2){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display.table-type1 tbody tr td:last-child {
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(9){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(10){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(11){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(12){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(13){
			text-align:center !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp user-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">My Users<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">MY Users</a>
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
				<div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_srtfilter();" title="Reset all">Reset</a>
                </div>
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
					  <li hidden><a id="send_email_user_modal"><i class="dropdwon-icon icon email"></i>Email</a></li>
					  <li style="border-radius: 5px;"><a id="download_multiple_users"  data-multi_user="0" ><i class="dropdwon-icon icon download"></i>EXPORT</a></li>
					 <!--  <li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
					  <!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>-->
					  <li hidden><a  id="deactive_multiple_users"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li>
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_users" id="search_users" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select style="padding-left:10px !important;" name="search_bycountry_users" id="search_bycountry_users" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country'>
                    <option value="">SELECT COUNTRY</option>
                    <option value="">ALL</option>
                    <?php 
                    for($i=0;$i<count($country);$i++){
                    	?>
                    	<option value="<?php echo $country[$i]['country_id']; ?>"> <?php echo strtoupper($country[$i]['country_name']); ?> </option>
                    	<?php
                    }
                    ?>
				   </select>

				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys city-select' id="search_by_city_users" name="search_by_city_users">
                    <option value="">SELECT CITY</option>
                    <option value="">ALL</option>
                     <?php 
                    for($i=0;$i<count($cities);$i++){
                    	?>
                    	<option value="<?php echo $cities[$i]['city_id']; ?>"> <?php echo strtoupper($cities[$i]['name']); ?> </option>
                    	<?php
                    }
                    ?>
				   </select>
				   
				
				   
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="users_start_date" name="users_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="users_end_date" name="users_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                  <table id="my-users-admin" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:5%">
							<input type="checkbox" class="purple filled-in" name="users_bulk" id="users_bulk"/>
							<label for="users_bulk"></label>
						</th>
                        <th style="width: 10%; padding-top:0px !important; text-align:center;">REG. ID</br>REG. DATE</th>
						<th style="width: 20%; text-align:center;">USER NAME</th>
                        <th style="width: 10%; text-align:center;">EMAIL ID</th>
						<th style="width: 10%; text-align:center;">MOBILE NO</th>
						<th style="width: 10%; text-align:center;">CITY</br>COUNTRY</th>
						<th style="width: 5%; text-align:center;">ACTIONS</th>
                      </tr>
                       
                      <tr>
                        <th style="width:5%;"></th>
                        <th style="width: 10%; padding-top:0px !important; text-align:center;"></th>
						<th style="width: 20%; text-align:center;"></th>
                        <th style="width: 10%; text-align:center;"></th>
						<th style="width: 10%; text-align:center;"></th>
						<th style="width: 10%; text-align:center;"></th>
                       
						<th style="width: 5%; text-align:center;"></th>
                        </tr>
                    </thead>
                    <tbody class="scrollbody">
						<!--<tr>
							<td style="width:10px">
								<input type="checkbox" class="purple filled-in sub_bulk_action" name="subscriber_bulk" id="subscriber_bulk"/>
								<label for="subscriber_bulk"></label>
							</td>
							<th style="width: 80px; padding-top:0px !important;">01-04-2019</th>
                        <td style="width: 80px">Reg01</td>
						<td style="width: 180px">Suhas Sawant</td>
                        <td style="width: 100px">suhas@gmail.com</td>
						<td style="width: 80px">8527419630</td>
						<td style="width: 80px">India</td>
						<td style="width: 80px">Mumbai</td>
                        <td style="width: 80px">Yes</td>
						<td style="width: 80px">9</td>
						<td style="width: 80px">No</td>
						<td style="width: 100px">-</td>
						</tr>-->
                    </tbody>
                  </table>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

	<div id="send_email_user_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
		<?php $this->load->view('admin_dashboard/email_subscriber_details'); ?>
	</div>
	
	<div id="deactive_challenge" class="modal" style="width:35% !important;">
		<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
		<div class="modal-content">
			<div class="modal-header" style="text-align:center;">
				<h4 style="width:93% !important;"> Deactivate User</h4>        
				<input type="hidden" id="remove_user_id" name="remove_user_id" value="" />
				<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
			</div>
		</div>
		<div class="modal-body" style="width:100% !important; margin-left:0px !important;">
			<p style="color:#595959; font-size:15px; text-align:center;">Are you sure you want to deactivate this user?</p>
		</div>
		<div class="modal-content">
			<div class="modal-footer">
				<div class="row">
					<div class="col l3 s12 m12"></div>
					<div class="col l9 s12 m12 cancel-deactiv">
						<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
						<button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_user_btn">DEACTIVATE</button>
					</div>
				</div>
			</div>
		</div>
	</div>

      <!-- END MAIN -->
	  <script>
		$(document).on('click', '.paginate_button', function(e){
			//e.preventDefault();
			//header.classList.remove("sticky");
			$("#fixedHeader").removeClass('sticky');
		});
		var bulk_activity = [];
		$("input[id='users_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".users_bulk_action").prop('checked', true);
				$(".users_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',bulk_cmp_profiles);
			}
			else {
				$(".users_bulk_action").prop('checked',false);
				bulk_activity = [];
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
			}
		});
		
		function reset_srtfilter(){
		  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
		  $('.js-example-basic-single').trigger('change.select2');
		  $('.btn-date,.search-hide-show').val('');
		  $('select').material_select();
		  adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
		  $('select').material_select();
		}
	  </script>
	  <script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 300) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
      <?php $this->load->view('template/footer'); ?>
