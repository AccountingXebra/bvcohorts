<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-tap-highlight" content="no">

	<meta name="description" content="">

	<meta name="keywords" content="">

	<title>Bharat Vaani</title>

	<!-- Favicons-->

	<!-- <link rel="icon" href="../../images/favicon/favicon-32x32.png" sizes="32x32"> -->

	<!-- Favicons-->

	<!-- <link rel="apple-touch-icon-precomposed" href="../../images/favicon/apple-touch-icon-152x152.png"> -->

	<!-- For iPhone -->

	<meta name="msapplication-TileColor" content="#00bcd4">

	<!-- <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png"> -->

	<!-- For Windows Phone -->

	<!-- CORE CSS-->

	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">

	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />

    <?php /*?><link href="<?php echo base_url();?>asset/css/css/style-form.css" type="text/css" rel="stylesheet"><?php */?>

    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">

    <link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">

    <link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">

	<!--Import Google Icon Font-->

	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

    <link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">

    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">

	<link href="<?= base_url(); ?>asset/css/client-portal.css" type="text/css" rel="stylesheet">

	<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script>var base_url = '<?php echo base_url(); ?>';</script>

	<style type="text/css">
	body.layout-semi-dark #header nav.navbar-color {
		height: 80px;
		padding:3px 0 0 0;
	}
	.navbar-fixed .nav-wrapper > ul.right.hide-on-med-and-down {
		margin: 0 0px 0 0 !important;
	}
    .loading_data {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
		background-size: 6%;
		opacity: .9;
	}
	.cmp-logo {
		position: fixed;
		left: 0;
	}
	h1 span.logo-text .bold {
		font-weight: 600;
		color: #7864e9;
	}

	h1 span.logo-text {
		color: #7864e9 !important;
		font-size: 20px;
		text-shadow: 0 0 0 #7965e9;
		font-weight: 400;
	}

	nav .nav-wrapper {
		border-bottom: 1px solid #ccc;
	}

	.nav-wrapper .select-wrapper.company_dropdown_select input.select-dropdown {
		margin-bottom: 7px !important;
	}

	.company_dropdown {
		width: 200px;
		border-right : 1px solid #ccc;
	}

	.ad-head-btn{
		/*background-color: #7864e9;
		color: #ffffff !important;*/
		background-color: #ffffff;
		color: #000000 !important;
		font-weight: 400;
		border: 1px solid #7864e9;
		padding: 0 0px !important;
		height: 40px;
		line-height: 40px;
		width:105px !important;
		margin:0px 2px 0px 2px !important;
		font-size:12px !important;
	}

	.ad_list.active{
		background-color: #7864e9;
		color: #ffffff !important;
	}

	.ad-head-btn:hover{
		background-color: #7864e9 !important;
		color:#ffffff !important;
	}

	.dropdown-select-wrapper {
	  margin-left: 250px;
	}

	.nav-wrapper ul.dropdown-content.select-dropdown {
	  margin: 50px 0 0 -15px !important;
	}

	nav ul.hide-on-med-and-down > li {
		margin-right:10px !important;
	}

	.navbar-fixed nav {
		position: fixed !important;
	}

	.coupon{
		font-size:11px !important;
		line-height:19px !important;
	}
</style>
</head>

	<body class="layout-semi-dark white">
		<div class="loading_data"></div>
		<?php
      	  //$current_user =  $this->session->userdata('aduserData');
        ?>
		<header id="header" class="page-topbar">

		<!-- start header nav-->
		<div class="navbar-fixed">

			<nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">

			<div class="nav-wrapper">
				<div class="dropdown-select-wrapper">
				<div class="cmp-logo">
					<div id='eazy_logo' style="padding-right:0px; padding-bottom:0px;">
						<img width="300" height="75" style="margin:-2px 10px 5px 0px !important;" src="<?php echo base_url(); ?>asset/images/cohorts/bvlogo_black.png" alt="Logo" class="text-hide-collapsible logo_style_2"/>
					</div>
				</div>
				</div>
            <?php if(isset($this->session->userdata['admin_session'])) { ?>

			<ul class="right hide-on-med-and-down">
				<li>
					<a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
						<span class="avatar-status avatar-online">
							<img src="<?php echo base_url();?>asset/css/img/icons/client.png" alt="client-pic">
						</span>
						<span class="user-name">Hi <span class="user-display-name"><?php if(isset($this->session->userdata['admin_session'])) { echo ucfirst($this->session->userdata['admin_session']['admin_name']); } ?></span>
						<span class="arrow-icon down">&#9660;</span>
						<span class="arrow-icon up">&#x25B2;</span>
					</a>
				</li>
				<li>
					
					<a href="<?php echo base_url();?>admin_dashboard/interview_list" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="interview_list" || $this->router->fetch_method()=="add_interview" || $this->router->fetch_method()=="edit_interview" ) { echo "active"; }} ?> ">INTERVIEW</a>
					
					<a href="<?php echo base_url();?>admin_dashboard/videos" class="ad_list btn btn-theme btn-large section_li ad-head-btn profile-button " data-activates="learning-dropdown">Videos</a>
					
					<a href="<?php echo base_url();?>admin_dashboard/admin_challenge" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="admin_challenge" || $this->router->fetch_method()=="add_challenge" || $this->router->fetch_method()=="edit_challenge" ) { echo "active"; }} ?> ">CHALLENGES</a>
					
					<a href="<?php echo base_url();?>admin_dashboard/incubator" class="ad_list btn btn-theme btn-large section_li ad-head-btn profile-button " data-activates="learning-dropdown">Info List</a>
					
					<!--<a href="<?php echo base_url();?>admin_dashboard/demo" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="demo" ) { echo "active"; }} ?> ">DEMO</a>
					
					<a href="<?php echo base_url();?>admin_dashboard/ca_partner" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="ca_partner" ) { echo "active"; }} ?>" id="ca-partner">CA PARTNERS</a>
					
					<a href="<?php echo base_url();?>admin_dashboard/subscriber_list" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="subscriber_list" ) { echo "active"; }} ?>" id="subscr">SUBSCRIBERS</a>-->
					
					<!--a href="<?php //echo base_url();?>admin-dashboard/add-new-offer" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php //if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="feedback_list" ) { echo "active"; }} ?> ">ADD OFFERS</a-->

					<!--a href="<?php //echo base_url();?>admin_dashboard/feedback_list" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php //if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="feedback_list" ) { echo "active"; }} ?> ">FEEDBACK</a>

					<a href="<?php echo base_url();?>admin_dashboard/referearn_list" class="ad_list btn btn-theme btn-large section_li ad-head-btn coupon <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="referearn_list" ) { echo "active"; }} ?> ">REFERRAL PROGRAM</a>

					<!--<?php //if($this->session->userdata['admin_session']["admin_type"]=="Admin") { ?>
					<a href="<?php //echo base_url();?>admin_dashboard/add_profile_list" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php //if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="add_profile_list" ) { echo "active"; }} ?> ">PROFILE</a>
					<?php //} ?>
                  <?php  if($this->session->userdata['admin_session']["admin_type"]=="Admin") { ?>
					<a href="<?php echo base_url();?>admin_dashboard/coupon_list_view" class="ad_list btn btn-theme btn-large section_li ad-head-btn coupon <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="coupon_list_view" ) { echo "active"; }} ?>">COUPON / OFFERS <br>GENERATOR</a>
				<?php } ?>

					<?php if($this->session->userdata['admin_session']["admin_type"]=="Admin") { ?>
					<a href="<?php echo base_url();?>admin_dashboard/revenue_list_view" class="ad_list btn btn-theme btn-large section_li ad-head-btn <?php if($this->router->fetch_class()=="admin_dashboard"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="revenue_list_view" ) { echo "active"; }} ?>" id="revenue">REVENUE</a>
					<?php } ?>-->

				</li>
				<li>

				</li>
            </ul>

			<!-- translation-button -->
            <ul id="translation-dropdown" class="dropdown-content">
				<li>
					<a href="#!" class="grey-text text-darken-1">
					<i class="flag-icon flag-icon-gb"></i> English</a>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-1">
					<i class="flag-icon flag-icon-fr"></i> French</a>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-1">
					<i class="flag-icon flag-icon-cn"></i> Chinese</a>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-1">
					<i class="flag-icon flag-icon-de"></i> German</a>
				</li>
            </ul>
			<!-- setting-dropdown -->

            <ul id="setting-dropdown" class="dropdown-content">
				<li>
					<a href="<?php echo  base_url();?>My_alerts">
					<i class="menu-round-icon"></i>
					<span>My Alerts</span>
					</a>
				</li>
            </ul>
            <!-- notifications-dropdown -->
			
			<ul id="learning-dropdown" class="dropdown-content user-profile-down" style="margin: 15px 0 0 52px;">
              <li class="user-profile-fix">
                <ul>
				  <!--<li>
                    <a style="color:#fff;" class="main_menu_options" href="<?= base_url(); ?>admin_dashboard/coworking_list">
                      Co-Working Space
                    </a>
                  </li>-->
				  <li>
                    <a style="color:#fff;" class="main_menu_options" href="<?= base_url(); ?>admin_dashboard/incubator_list">
                      Incubator
                    </a>
                  </li>
				</ul>
			  </li>
		  	</ul>

            <ul id="notifications-dropdown" class="dropdown-content">
				<li>
					<h6>Notifications
						<span class="new badge">1</span>
					</h6>
				</li>
				<li class="divider"></li>
				<li>
					<a href="<?php echo base_url();?>My_subscription" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle yellow small">warning</span>You have 15<?php //echo $left_time; ?> days left for trial period to end. UPGRADE NOW! </a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
				</li>
				<li>
					<a href="#!" class="grey-text text-darken-2">
					<span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
					<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
				</li>
			</ul>

			<ul id="profile-dropdown" class="dropdown-content user-profile-down" style="min-width: 200px; margin: 0 0 0 0px;">
				<li>
					<div class="user-pofile-image">
					<!--  <img src="<?php echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
					<div class="user-image-layout"></div>
					<div class="user-short-details">
						<span class="name"><?php echo ucfirst($this->session->userdata['admin_session']['admin_name']); ?></span>
						<span class="mail"><?php echo $this->session->userdata['admin_session']['admin_email']; ?></span>
					</div>
                </div>
            </li>
            <li class="user-profile-fix">
				<ul>
					<?php if($this->session->userdata['admin_session']["admin_type"]=="Admin") { ?>
					<li>
						<a style="font-size:12px;" class="" href="<?php echo base_url();?>admin_dashboard/add_profile_list">
							<span>PROFILE</span>
						</a>
					</li>
					<?php } ?>
					<!--<li>
						<a style="font-size:12px;" class="" href="<?php echo base_url();?>admin_dashboard/add_config">
							<span>SITE CONFIG</span>
						</a>
					</li>
					<li>
						<a style="font-size:12px;" class="" href="<?php echo base_url();?>admin_dashboard/users">
							<span>USERS</span>
						</a>
					</li>
					<li>
						<a style="font-size:12px;" class="" href="<?php echo base_url();?>admin_dashboard/feedback_list">
							<span>FEEDBACK</span>
						</a>
					</li>-->
					<li>
						<a style="font-size:12px;" class="" href="<?= base_url(); ?>admin_dashboard/change-password">
							<span>CHANGE PASSWORD</span>
						</a>
					</li>
					<li>
						<a style="font-size:12px;" class="" href="<?= base_url(); ?>admin_dashboard/blog_post_list">
							<span>BLOG POST</span>
						</a>
					</li>
					<!--<li>
						<a style="font-size:12px;" class="" href="<?= base_url(); ?>admin_dashboard/blog_digest_list">
							<span>BLOG DIGEST</span>
						</a>
					</li>
					<li>
						<a style="font-size:12px;" class="" href="<?= base_url(); ?>admin_dashboard/newsletter_list">
							<span>NEWSLETTER</span>
						</a>
					</li>
					<li>
						<a style="font-size:12px;" class="" href="<?= base_url(); ?>admin_dashboard/survey_list">
							<span>SURVEY</span>
						</a>
					</li>-->
					<li class="divider"></li>
					<li class="signout-link">
						<a style="font-size:12px;" href="<?php echo base_url();?>admin_dashboard/logout" class="singout modal-trigger">
							SIGN OUT
						</a>
					</li>
                </ul>
            </li>
            </ul>
          <?php } ?>
          </div>

        </nav>

      </div>

    </header>
    <!-- END HEADER -->
    <script>
		$('.ad-head-btn').on('click', function(){
			$('.ad-head-btn').removeClass('selected');
			$(this).addClass('selected');
		});
	</script>
