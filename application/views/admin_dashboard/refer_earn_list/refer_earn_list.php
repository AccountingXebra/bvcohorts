<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:200px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#refearn_table_length .dropdown-content {
			min-width: 95px;
		}

		#refearn_table_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:105px;
			margin-top:5px;
			margin-left:790px;
		}

		#refearn_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		#refearn_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		a.addmorelink {
			margin-top:-25px !important;
		}
		
		.feed-top{
			margin-left:1px !important;
		}
		
		.ref-code{
			text-align:right;	
			font-size:15px !important;
		}
			
		table.dataTable.display tbody tr:first-child td {
			text-align: left;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		
		table.dataTable.display tbody tr td:nth-child(2){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		.sticky {
			position: fixed;
			top: 82px;
			width: 93%;
			z-index:996;
			background: white;
			color: black;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		
		.btn-dropdown-action{
			min-width:155px !important;
		}
		
		#dropdown004.dropdown-content li > a, .dropdown-content li > span{
			padding: 14px 5px 14px 5px !important;
		}
		#refearn_table .dropdown-button.action-more {
			padding: 12px 30px; 
		}
		.buffer_data {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(<?=base_url('public/images/buffering.gif');?>) 50% 50% no-repeat rgb(249,249,249);
			background-size: 20%;
			opacity: .9;
			image-rendering: inherit;
			image-rendering: initial;
			image-rendering: unset;
		}
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
	<div class="buffer_data" hidden></div>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp coupon-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row feed-top">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">Refer And Earn<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">My Referals</a>
							</ol>
						</div>
						<div class="col s12 m12 l6">
							<p class="ref-code">REFERENCE CODE</p>
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
			  <div class="row">
                <div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_reffilter();" title="Reset all">Reset</a>
				</div>
			  </div>
              <div class="row">
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
						<li><a href="javascript:voice(0);"><img style="margin-right: 17px;" class="icon-img" src="<?php echo base_url(); ?>public/icons/payment_rcv.png" alt="payment">MAKE PAYMENT</a></li>
						<li><a id="download_multiple_refearn" data-multi_ref="0" ><i class="dropdwon-icon icon download"></i>DOWNLOAD</a></li>
						<li><a id="print_multiple_refearn"><i class="material-icons">print</i>PRINT</a></li>
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_refer" id="search_refer" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>
                </div>
				
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select style="padding-left:10px !important;" name="search_ref_name" id="search_ref_name" class='ml-3px border-split-form border-radius-6 btn-dropdown-select select-like-dropdown select-country'>
                    <option value="">RERERRAL NAME</option>
                    <option value="">ALL</option> 
				   </select>

				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys city-select' id="search_ref_code" name="search_ref_code">
                    <option value="">REFERRAL CODE</option>
                    <option value="">ALL</option>
				   </select>
				   
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="refer_start_date" name="refer_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="refer_end_date" name="refer_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                <form>
                  <table id="refearn_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:10px">
							<input type="checkbox" class="purple filled-in" name="refearn_bulk" id="refearn_bulk"/>
							<label for="refearn_bulk"></label>
						</th>
                        <th style="width: 80px; padding-top:0px !important; text-align:center;">PURCHASE DATE</th>
                        <th style="width: 100px; text-align:center;">REFERRAL CODE </th>
						<th style="width: 150px; text-align:center;">REFERRAL NAME</th>
                        <th style="width: 130px; text-align:center;">TYPE OF SUBSCRIPTION</th>
						<th style="width: 80px; text-align:center;">REFERRAL FEE</th>
						<th style="width: 80px; text-align:center;">PAYMENT STATUS</th>
						<th style="width: 50px; text-align:center;">ACTION</th>
                      </tr>
                    </thead>
                    <tbody class="scrollbody">
						
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
	  <script>
		$(document).ready(function() {
		$('.buffer_data').hide();
		$('#icici_otp_send').on('click' ,function(){
			var otp= $("#otp_text").val();
			var debitacc= $("#debitacc").val();
			var creditacc= $("#creditacc").val();
			var ifsc= $("#ifsc").val();
			var amount= $("#amount").val();
			var payeename= $("#payeename").val();
			var corp_id= $("#ic_otp_modal #corp_id").val();
		var user_id= $("#ic_otp_modal #user_id").val();
		var urn_no= $("#ic_otp_modal #urn_no").val();
			var exp_id= $("#exp_id").val();
			var uniq_id=$("#ic_otp_modal #uniq_id").val();
			//alert(payeename);
		if(otp!=""){
			$('.buffer_data').show();
			$.ajax({
            url:base_url+'admin_dashboard/icici_wallet_transaction',
            type:"POST",
            data:{"otp":otp,"debitacc":debitacc,"creditacc":creditacc,"ifsc":ifsc,"amount":amount,"transfer_mode":"icici_bank","payeename":payeename,"corp_id":corp_id,"user_id":user_id,"urn_no":urn_no,'uniq_id':uniq_id},
            success:function(res){

				var data=JSON.parse(res);
                 
                     
                      Materialize.toast(data.MESSAGE, 5000,'green rounded');

            if(data.Response=="SUCCESS"){
				$('.buffer_data').hide();

				    $.ajax({
            url:base_url+'admin_dashboard/create_referral_payment',
            type:"POST",
            data:{"amount":amount,"id":exp_id},
            success:function(res1){

				var data1=JSON.parse(res1);
				if(data1.response=="true"){
					Materialize.toast("Amount Transferred Successfully", 4000,'green rounded'); 
					setTimeout(function(){ 
						$('.close-pop').click();
					}, 500);		
				}else{
					Materialize.toast("Something went wrong", 4000,'red rounded'); 	
				}

			}

		});
            	//window.location.href=base_url+'admin_dashboard/create_referral_payment/'+exp_id;
            
			}else{
				$('.buffer_data').hide();
				if(data.Message=="107889"){
                              Materialize.toast("You have entered wrong OTP", 4000,'red rounded'); 
						}else{
                         Materialize.toast(data.Message, 4000,'red rounded'); 
						}
            }        
            }
			});
		}else{
			$('.buffer_data').hide();
			Materialize.toast('Please enter otp', 4000,'red rounded'); 
		}
		});
		$(".otp_resend").click(function(){
		var exp_id=$("#ic_otp_modal #exp_id").val();
		var amount= $("#amount").val();
		  $.ajax({
                url:base_url+'expense/icici_wallet_expense',
                type:"POST",
                data:{"exp_id":exp_id,'amount':amount},
                
                success:function(res){
					var data=JSON.parse(res);
					console.log(data);
					$('#icici_otp_send').attr('disabled', false);
                    if(data.msg == "No bank")
                    {
						Materialize.toast('No ICICI bank added in your profile .Please add ICICI bank to avail this service.', 5000,'green rounded');
						$('#add_icici_bank_details').modal('open');	

                    }else if(data.msg == "send_otp"){
                    	Materialize.toast('OTP has been resent', 4000,'green rounded'); 
                    //	$('#icici_otp_modal').modal('close');
						$('#icici_otp_modal').modal('open');
						timer(60);					
						
						$('#icici_otp_modal #debitacc').val(data.bank_details[0].cbank_account_no);
						$('#icici_otp_modal #creditacc').val(data.xebra_bank_account);
                        $('#icici_otp_modal #ifsc').val(data.ifsc);
                        $('#icici_otp_modal #amount').val(data.amount);
                        $('#icici_otp_modal #payeename').val(data.nickname);
                         $('#icici_otp_modal #user_id').val(data.bank_details[0].user_id);
                          $('#icici_otp_modal #corp_id').val(data.bank_details[0].corp_id);
                           $('#icici_otp_modal #urn_no').val(data.bank_details[0].urn_no);

                           $('#icici_otp_modal #exp_id').val(exp_id);
                            $('#icici_otp_modal #uniq_id').val(data.uniqid);

                      //
                    } else if(data.msg== "Failure"){
                          Materialize.toast('Beneficiary addition failed.', 5000,'red rounded');
                    }else{
                      //
                       Materialize.toast('Something went wrong.', 5000,'red rounded');
                    }
                  },
			});      
	});


		$('.refer_payment').off().on('click', function(){
						//$('#add_ic_bank_details').modal('open'); 
						var exp_id=$(this).data('refer_id');
						//$('#ic_otp_modal').modal('open');	
						//$('#add_ic_bank_details').modal('open');	
						//var frm=$(form).serialize();
		
						$.ajax({
						url:base_url+'admin_dashboard/icici_wallet_referral',
						type:"POST",
						data:{"exp_id":exp_id},
						success:function(res){
						  var data=JSON.parse(res);
                  
                    if(data.msg == "No bank")
                    {	
						$('#add_icici_bank_details').modal('open'); 
                    }else if(data.bank_details.length > 0 && data.bank_details[0].api_integration=="No" && (data.bank_details[0].urn_no=="" || data.bank_details[0].user_id==""||data.bank_details[0].corp_id=="" )){
                    	$('#add_icici_bank_details').modal('open'); 
						$('#add_icici_bank_details #ac_number').val(data.bank_details[0].cbank_account_no);
						$('#add_icici_bank_details #bank_name').val(data.bank_details[0].cbank_name);
						
						$('#add_icici_bank_details #acc_type').val(data.bank_details[0].cbank_account_type).prop("selected", "selected").change();
						$('#acc_type').material_select();
						$('#add_icici_bank_details #bank_branch_name').val(data.bank_details[0].cbank_branch_name);
						
						$('#add_icici_bank_details #bus_bank_country').val(data.bank_details[0].cbank_country_code).prop("selected", "selected").change();
						$('#bus_bank_country').material_select();
						$('#add_icici_bank_details #ifsc_code').val(data.bank_details[0].cbank_ifsc_code);
						
						$('#add_icici_bank_details #bank_cur_check_edit').val(data.bank_details[0].cbank_currency_code).prop("selected", "selected").change();
						$('#bank_cur_check_edit').material_select();
						$('#add_icici_bank_details #urn_no').val(data.bank_details[0].urn_no);
						
						$('#add_icici_bank_details #corp_id').val(data.bank_details[0].corp_id);
						$('#add_icici_bank_details #user_id').val(data.bank_details[0].user_id);
                    }

                     else if(data.bank_details.length > 0 && data.bank_details[0].api_integration=="No" && data.msg=="FAILURE" ){
                        $('#add_icici_bank_details').modal('open'); 
						
						$('#add_icici_bank_details #ac_number').val(data.bank_details[0].cbank_account_no);
						$('#add_icici_bank_details #bank_name').val(data.bank_details[0].cbank_name);
						
						$('#add_icici_bank_details #acc_type').val(data.bank_details[0].cbank_account_type).prop("selected", "selected").change();
						$('#acc_type').material_select();
						$('#add_icici_bank_details #bank_branch_name').val(data.bank_details[0].cbank_branch_name);
						
						$('#add_icici_bank_details #bus_bank_country').val(data.bank_details[0].cbank_country_code).prop("selected", "selected").change();
						$('#bus_bank_country').material_select();
						$('#add_icici_bank_details #ifsc_code').val(data.bank_details[0].cbank_ifsc_code);
						
						$('#add_icici_bank_details #bank_cur_check_edit').val(data.bank_details[0].cbank_currency_code).prop("selected", "selected").change();
						$('#bank_cur_check_edit').material_select();
						$('#add_icici_bank_details #urn_no').val(data.bank_details[0].urn_no);
						
						$('#add_icici_bank_details #corp_id').val(data.bank_details[0].corp_id);
						$('#add_icici_bank_details #user_id').val(data.bank_details[0].user_id);
                    }
                    else if(data.msg == "send_otp"){
						$('#icici_otp_modal').modal('open');
						timer(60); 
						$('#icici_otp_modal #debitacc').val(data.bank_details[0].cbank_account_no);
						$('#icici_otp_modal #creditacc').val(data.xebra_bank_account);
                        $('#icici_otp_modal #ifsc').val(data.ifsc);
                        $('#icici_otp_modal #amount').val(data.amount);
                        $('#icici_otp_modal #payeename').val(data.nickname);
                         $('#icici_otp_modal #user_id').val(data.bank_details[0].user_id);
                          $('#icici_otp_modal #corp_id').val(data.bank_details[0].corp_id);
                           $('#icici_otp_modal #urn_no').val(data.bank_details[0].urn_no);

                           $('#icici_otp_modal #exp_id').val(exp_id);


                      //
                    } else if(data.msg== "Failure"){
                          Materialize.toast('Beneficiary addition failed.', 5000,'red rounded');
                    }else{
                      //
                       Materialize.toast(data.msg, 5000,'red rounded');
                    }	              
                  },
				});      

					});
		
		$(".bank_save").click(function(){
		if($("#permission_integrate").prop('checked') == true){
			$("#add_icici_bank_form").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					bank_name:{
						required:true,
					},
					ac_number:{
						required:true,
						digits:true,
					},
					acc_type:{
						required:true,
					},
					bus_bank_country:{
						required:true,
					},
					bank_cur_check:{
						required:true,
					},
					ifsc_code:{
						required:true,
						ifscno:true,
					},
					urn_no:{
						required:true,
					},
					corp_id:{
						required:true,
					},
					user_id:{
						required:true,
					},
				},
				messages:{
					bank_name:{
						required:"Bank Name is required",
					},
					ac_number:{
						required:"Bank Account Number is required",
					},
					ifsc_code:{
						ifscno:"Invalid IFSC Code",
					},
					acc_type:{
						required:"Account type is required",
					},
					bus_bank_country:{
						required:"Country is required",
					},
					bank_cur_check:{
						required:"Currency is required",
					},
					urn_no:{
						required:"ALIAS ID is required",
					},
					corp_id:{
						required:"Corporate id is required",
					},
					user_id:{
						required:"User id is required",
					},
				},
				submitHandler:function(form){
					var frmBank=$(form).serialize();
					alert(frmBank);
          $.ajax({
                url:base_url+'admin_dashboard/save_bank',
                type:"POST",
                data:{"frm":frmBank,},
                success:function(res){
          if(res==true){

              $('#add_ic_bank_details').modal('close');
                var exp_id=$("#refer_payment").data('refer_id');

                $.ajax({
                url:base_url+'admin_dashboard/icici_wallet_referral',
                type:"POST",
                data:{"exp_id":exp_id},
                success:function(res){
                  var data=JSON.parse(res);
                  console.log(data);
                    if(data.msg == "No bank")
                    {	
						$('#add_ic_bank_details').modal('open'); 
                    }else if(data.bank_details.length > 0 && data.bank_details[0].api_integration=="No" && (data.bank_details[0].urn_no=="" || data.bank_details[0].user_id==""||data.bank_details[0].corp_id=="" )){
                    	$('#add_ic_bank_details').modal('open'); 
						$('#add_ic_bank_details #ac_number').val(data.bank_details[0].cbank_account_no);
						$('#add_ic_bank_details #bank_name').val(data.bank_details[0].cbank_name);
						
						$('#add_ic_bank_details #acc_type').val(data.bank_details[0].cbank_account_type).prop("selected", "selected").change();
						$('#acc_type').material_select();
						$('#add_ic_bank_details #bank_branch_name').val(data.bank_details[0].cbank_branch_name);
						
						$('#add_ic_bank_details #bus_bank_country1').val(data.bank_details[0].cbank_country_code).prop("selected", "selected").change();
						$('#bus_bank_country1').material_select();
						$('#add_ic_bank_details #ifsc_code').val(data.bank_details[0].cbank_ifsc_code);
						
						$('#add_ic_bank_details #bank_cur_check_edit1').val(data.bank_details[0].cbank_currency_code).prop("selected", "selected").change();
						$('#bank_cur_check_edit1').material_select();
						$('#add_ic_bank_details #urn_no').val(data.bank_details[0].urn_no);
						
						$('#add_ic_bank_details #corp_id').val(data.bank_details[0].corp_id);
						$('#add_ic_bank_details #user_id').val(data.bank_details[0].user_id);
                    }

                     else if(data.bank_details.length > 0 && data.bank_details[0].api_integration=="No" && data.msg=="FAILURE" ){
                        $('#add_ic_bank_details').modal('open'); 
						
						$('#add_ic_bank_details #ac_number').val(data.bank_details[0].cbank_account_no);
						$('#add_ic_bank_details #bank_name').val(data.bank_details[0].cbank_name);
						
						$('#add_ic_bank_details #acc_type').val(data.bank_details[0].cbank_account_type).prop("selected", "selected").change();
						$('#acc_type').material_select();
						$('#add_ic_bank_details #bank_branch_name').val(data.bank_details[0].cbank_branch_name);
						
						$('#add_ic_bank_details #bus_bank_country1').val(data.bank_details[0].cbank_country_code).prop("selected", "selected").change();
						$('#bus_bank_country1').material_select();
						$('#add_ic_bank_details #ifsc_code').val(data.bank_details[0].cbank_ifsc_code);
						
						$('#add_ic_bank_details #bank_cur_check_edit1').val(data.bank_details[0].cbank_currency_code).prop("selected", "selected").change();
						$('#bank_cur_check_edit1').material_select();
						$('#add_ic_bank_details #urn_no').val(data.bank_details[0].urn_no);
						
						$('#add_ic_bank_details #corp_id').val(data.bank_details[0].corp_id);
						$('#add_ic_bank_details #user_id').val(data.bank_details[0].user_id);
                    }
                    else if(data.msg == "send_otp"){
						$('#ic_otp_modal').modal('open');
						timer(60); 
						$('#ic_otp_modal #debitacc').val(data.bank_details[0].cbank_account_no);
						$('#ic_otp_modal #creditacc').val(data.xebra_bank_account);
                        $('#ic_otp_modal #ifsc').val(data.ifsc);
                        $('#ic_otp_modal #amount').val(data.amount);
                        $('#ic_otp_modal #payeename').val(data.nickname);
                         $('#ic_otp_modal #user_id').val(data.bank_details[0].user_id);
                          $('#ic_otp_modal #corp_id').val(data.bank_details[0].corp_id);
                           $('#ic_otp_modal #urn_no').val(data.bank_details[0].urn_no);
                           $('#ic_otp_modal #exp_id').val(exp_id);


                      //
                    }else{
                      //
                       Materialize.toast(data.msg, 5000,'red rounded');
                    }              
                  },
        }); 

          }
        }

      });
						
				}
			});
		}else{
			$('#add_ic_bank_details').modal('close');						
		}			
	});		
		
		});
		
		function reset_reffilter(){
		  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
		  $('.js-example-basic-single').trigger('change.select2');
		  $('.btn-date,.search-hide-show').val('');
		  $('select').material_select();
		  refearnDatatable(base_path()+'admin-dashboard/get_refearn_details/','refearn_table');
		  $('select').material_select();
		}
		
	  </script>
      <?php $this->load->view('template/footer'); ?>
