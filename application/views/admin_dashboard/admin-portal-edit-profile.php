  <?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <!-- START MAIN -->
  <style type="text/css">
	#edit_admin_user_profile input[type=text]:not(.browser-default){
			margin:0px !important;
		}
		#reg_password, #contact_confirm_password{
			margin:0px !important;
		}
		#edit_admin_user_profile .input-field{
			margin-top:0px !important;
		}
    .select-wrapper label.error:not(.active) {
      margin: -30px 0 0 -11px;
    }
    
    /*----------START SEARCH DROPDOWN CSS--------*/
    .select2-container { 
      width: 100% !important;
    }
    
    .select2-container--default .select2-selection--single {
      border:none;
    }
    
    input[type="search"]:not(.browser-default) {
      height: 30px;
      font-size: 14px;
      margin: 0;
      border-radius: 5px;
    }
    
    .select2-container--default .select2-selection--single .select2-selection__rendered {
      font-size: 12px;
      line-height: 35px;
      color: #666;
      font-weight: 400;
    }
    
    .select2-container--default .select2-selection--single .select2-selection__arrow {
      height: 32px;
    }
    
    .select2-search--dropdown {
      padding: 0;
    }
    
    input[type="search"]:not(.browser-default):focus:not([readonly]) {
      border-bottom: 1px solid #bbb;
      box-shadow: none;
    }
    
    .select2-container--default .select2-selection--single:focus {
      outline: none;
    }
    
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
      background: #fffaef;
      color: #666;
    }
    
    .select2-container--default .select2-results > .select2-results__options {
      font-size: 14px;
      border-radius: 5px;
      box-shadow: 0px 2px 6px #B0B7CA;
    }
    
    .select2-dropdown {
      border: none;
      border-radius: 5px;
    }
    
    .select2-container .select2-selection--single {
      height: 53px;
    }
    
    .select2-results__option[aria-selected] {
      border-bottom: 1px solid #f2f7f9;
      padding: 14px 16px;
      border-top-left-radius: 0;
    }
    
    .select2-container--default .select2-search--dropdown .select2-search__field {
      /* border: none;*/
      border: 1px solid #d0d0d0;
      padding: 0 0 0 15px !important;
      width: 93.5%;
      max-width: 100%;
      background: #fff;
      border-radius: 4px;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
    }
    
    .select2-container--open .select2-dropdown--below {
      margin-top: -15px;
    }
    
    ul#select2-currency-results {
      width: 97%;
    }
    
    /*---Dropdown error message format---*/
    .select-wrapper + label.error{
      margin: 18px 0 0 -10px;
    } 
    
    select.error + label.error:not(.active){
      margin: -20px 0 0 -10px; 
    }
    
    select + label.error.active{
      margin-left: -10px;
    }

    .toolattr{
      position: absolute;
      top: 5px;
      right: 7px;
    }

    .list_access{
      position: absolute;
      top: -2px;
      right: 3px;
    }

    #tab-table td, th {
      border: 1px solid #C0C0C0;
    }

    #reg_admin_type-error{
      margin: 0px 0 0 -10px !important;
    }
	
	.ad-access .select-dropdown{
		padding-left:10px !important;
	}
	
	.ad-access.select-wrapper span.caret {
		top:20px !important;
	}
  </style>
    
  <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">   
    
      <?php //$this->load->view('template/sidebar.php');?>
      
      <section id="content">
      <div class="container">
            <div class="plain-page-header">
        <div class="row">
          <div class="col l6 s12 m6">
            <a class="go-back underline" href="<?php echo base_url();?>Admin_dashboard/add_profile_list">Back to My Profile</a> 
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
            </div>
            <div class="page-content">
        <div class="row">
          <div class="col s12 m12 l3"></div> 
          <div class="col s12 m12 l6">

            <form method="post"  id="edit_admin_user_profile" name="edit_admin_user_profile" action="<?php echo base_url('admin_dashboard/update/'.$data->id);?>" enctype="multipart/form-data">

                    <?php


                    if ($this->session->flashdata('errors')){
                        echo '<div class="alert alert-danger">';
                        echo $this->session->flashdata('errors');
                        echo "</div>";
                    }


                    ?>
            <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
            <div class="box-header">
                <h3 class="box-title">Edit Profile</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col s12 m12 l12">
                <div class="col s12 m12 l9">
                  <div class="row">
                  <div class="input-field">
                    <label for="reg_username" class="full-bg-label">First & Last Name<span class="required_field"> *</span></label>

                    <input id="reg_username" name="reg_username" class="full-bg adjust-width" type="text" value="<?php echo $data->reg_username; ?>" >
                  </div>   

                  </div>
                </div>
                <div class="col s12 m12 l3">
                  <div class="row">

                  <div class="input-field"> 
                    <div class="uploader-placeholder" style="background-image: url('http://localhost/invoice/asset/css/img/icons/placeholder.png');">
                    <label class="up_pic" style="font-size:12px; color:#696969; margin-top:-13px !important; margin-left:28px;">Upload your Photo</label>
                    <input type="file" class="hide-file" id="reg_profile_image" value="<?php echo $data->reg_profile_image; ?>" name="reg_profile_image">

                  
                    <input type="hidden" id="image_info" name="image_info" value="" />
                    </div>
                  </div>
                  </div>
                </div>
                </div>
              </div>
              <div class="row">
                <div class="col s12 m12 l12">
                <div class="col s12 m12 l12">
                  <div class="row">
                  <div class="input-field">
                    <label for="reg_email" class="full-bg-label">Email ID<span class="required_field"> *</span></label>
     <input id="reg_email" name="reg_email" class="full-bg adjust-width border-top-none" type="text" value="<?php echo $data->reg_email; ?>">

                  </div>
                  </div>
                </div>
                </div>
               </div>
              <div class="row">
                <div class="col s12 m12 l12">
                <div class="col s12 m12 l6 input-set">
                  <div class="row passcheck">
                  <div class="input-field">
                    <label for="contact_mobile" class="full-bg-label">Password<span class="required_field"> *</span></label>

                    <input id="reg_password" name="reg_password" class="full-bg adjust-width border-top-none show_password password_strength" type="password" onblur="alphanumeric(document.add_personal_profile.reg_password)" value="<?php echo $data->reg_password; ?>">

                    <span id="invalid_password_alpha"  class="error pass_2" style="display:none;font-size:12px;">Please Enter Alphanumeric Values and Special Character for Password</span>
                  </div>
                  <div class="toolattr">
                    <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Minimum 8 Characters, 1 in Uppercase, 1 Number & 1 Special Character"></a>
                  </div>
                  <div class="passattr">
                    <span id="pwdMeter" class="neutral"></span>
                    <input type="checkbox" id="filled-in2" class="show_password_chkbox" />
                    <label for="filled-in2" class="indic">View</label>
                  </div>
                  </div>
                </div>
                 <div class="col s12 m12 l6 input-set">
                  <div class="row passcheck">
                  <div class="input-field">
                    <label for="contact_confirm_password" class="full-bg-label">Confirm Password<span class="required_field"> *</span></label>

                    <input id="contact_confirm_password" name="contact_confirm_password" class="full-bg adjust-width border-top-none show_password_2" type="password" value="<?php echo $data->contact_confirm_password; ?>" onblur="alphanumeric(document.add_personal_profile.reg_password)">

                  </div>
                  <div class="passattr">
                    <span id="" class="neutral"></span>
                    <input type="checkbox" id="confirmpassword" name="confirmpassword" class="show_password_chkbox_2" />
                    <label for="confirmpassword" class="indic">View</label>
                  </div>
                  </div>
                </div>
                </div>
              </div>
              <div class="row">
                <div class="col s12 m12 l12">
                <div class="col s12 m12 l6 input-set ">
                  <div class="row">
                  <div class="input-field">
                    <label for="contact_mobile" class="full-bg-label">Mobile<span class="required_field"> *</span></label>

                    <input id="reg_mobile" name="reg_mobile" class="full-bg adjust-width border-top-none" type="text" maxlength="10" value="<?php echo $data->reg_mobile; ?>"  title="Please enter 10 digits for a valid Mobile number" > 

                  </div>
                  </div>
                </div>
                 <div class="col s12 m12 l6 input-set border-none">
                  <div class="input-field label-active">
                  <label for="country" class="full-bg-label select-label">Access<span class="required_field"> *</span></label>

                  <select class="country-dropdown check-label ad-access" value="<?php echo $data->reg_admin_type; ?>" name="reg_admin_type" id="reg_admin_type" style="padding-left:10px;">
                    <option value='Admin'>Admin</option>
                    <option value='User'>User</option>
                  </select>
                  </div>
                </div>
                </div>
              </div>
            </div>
                    </div>
                      <div class="row">
                      <div class="col s12 m12 l12 mt-2">
                        <div class="form-botom-divider"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 m12 l6 right">
            <input type="submit" id="vrc" class="btn-flat theme-primary-btn theme-btn theme-btn-large right" value="Save">
            <button  class="btn-flat theme-flat-btn theme-btn theme-btn-large right mr-5" type="reset" onclick="location.href = '<?php echo base_url();?>Admin_dashboard/add_profile_list';">Cancel</button>
                      </div>
                    </div>
                </form>
                </div>
                <div class="col s12 m12 l3"></div>
            </div>
        </div>
        </div>           
    </section>
  <!-- END CONTENT -->
</div>
<!-- END WRAPPER -->
</div>
    <script type="text/javascript">
        $(document).ready(function() {
          $("#edit_admin_user_profile").validate({
				
				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					reg_mobile:{

						required:true,

						number:true,

						minlength:10,

						maxlength:10,

					},

					reg_password: {

						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},
				},

				messages:{

					reg_username:{

						required:"Contact Name is required",

					},

					reg_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					reg_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						minlength:"Please Enter Mobile No like (e.g 0123456789)",

						maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					reg_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},

					reg_admin_type:{

						required:"Please Allow any access to New User",

					},

				},
				errorPlacement: function(error, element) {
         
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
			        form.submit();
			      }   
				});
		  
		  
		  $('.js-example-basic-single').select2();
          $('.select2-selection__rendered').each(function () {
              $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
          })
        $("select").change(function () {
        if($(this).val()!=''){
          $(this).valid();
          $(this).closest('.input-field').find('.error').remove();
        }
        });
    
    $("#reg_profile_image").click(function(event){
      $('.up_pic').hide();
    });
    
    $("#reg_degital_signature").click(function(event){
      $('.up_sign').hide();
    });
    
    $("#vrc").click(function(){
      //alert($("#reg_profile_image").val());
      //alert($("#image_info").val());
    });
    
      });
    </script>
  
  

<?php $this->load->view('template/footer.php');?>
