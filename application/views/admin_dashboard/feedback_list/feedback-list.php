<?php $this->load->view('admin_dashboard/admin-portal-header'); ?>
    <style>
		/*....................*/
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:200px !important;
		}

		.btn-dropdown-select > input.select-dropdown {			
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#feedback_table_length .dropdown-content {
			min-width: 95px;
		}

		#feedback_table_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:96px;
			margin-top:5px;
			margin-left:790px;
		}

		#feedback_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		#feedback_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		a.addmorelink {
			margin-top:-25px !important;
		}
		
		.feed-top{
			margin-left:1px !important;
		}
		
		.btn-stated {
			background-position: 90px center !important;
		}
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp coupon-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row feed-top">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">Feedback<small class="grey-text">(<span id="rev_count"> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">My Feedback</a>
							</ol>
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
			  <div class="row">
                <div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_feedbackfilter();" title="Reset all">Reset</a>
				</div>
			  </div>
              <div class="row">
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
					  <!--<li><a id="email_coupon_data"><i class="dropdwon-icon icon email"></i>Email</a></li>-->
					  <li><a id="download_multiple_feedback"  data-multi_feed="0" ><i class="dropdwon-icon icon download"></i>Download</a></li>
					 <!--  <li><a id="download_multiple_rev" data-multi_rev="0" style="display:flex;"><img class="icon-img" src="<?php //echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px;">Export</a></li> -->
					  <!--<li><a id="print_multiple_voucher"><i class="material-icons">print</i>Print</a></li>-->
					  <!--<li><a  id="deactive_multiple_coupon"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li>-->
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn">
						<input type="text" name="search_feedback" id="search_feedback" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper feedbackdate right">
				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="feedback_start_date" name="feedback_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="feedback_end_date" name="feedback_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                
                  <table id="feedback_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead>
                      <tr>
                        <th style="width:10px">
							<input type="checkbox" class="purple filled-in" name="feedback_bulk" id="feedback_bulk"/>
							<label for="feedback_bulk"></label>
						</th>
                        <th style="width: 80px; padding-top:0px !important;">FEEDBACK DATE</th>
                        <th style="width: 100px">USER NAME</th>
						<th style="width: 150px">FEEDBACK COMMENT</th>
                        <th style="width: 150px">ACTION TAKEN</th>
						<th style="width: 100px">BY WHOM</th>
						<th style="width: 80px">DATE OF ACTION</th>
						<th style="width: 25px"></th>
                      </tr>
                    </thead>
                    <tbody>
						
                    </tbody>
                  </table>
               
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
	  <script>
		function reset_feedbackfilter(){
		  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
		  $('.js-example-basic-single').trigger('change.select2');
		  $('.btn-date,.search-hide-show').val('');
		  $('select').material_select();
		  feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
		  $('select').material_select();
		}
		
		function change_button(id){
			$("#act-taken"+id).focus();
			$("#save-info"+id).show();
			$("#edit-info"+id).hide();
		}

		function save_button(id){
			var act_taken=$("#act-taken"+id).val();
			var by_whom=$("#by-whom"+id).val();
			var act_date=$("#act-date"+id).val();

			 $.ajax({
              type: "POST",
              dataType: 'json',
              
              url: base_url+'admin-dashboard/edit_feedback',
              data:{
              	"id":id,
              	"act_taken":act_taken,
                    "by_whom":by_whom,
                    "act_date":act_date
                         
              
              },
              cache: false,
                      success:function(result)
              {
                  feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
		  $('select').material_select();
              }

          });
                
			
		}
	  </script>
      <?php $this->load->view('template/footer'); ?>
