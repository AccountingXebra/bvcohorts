<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('FEEDBACK DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'FEEDBACK DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'FEEDBACK DATE');
$this->excel->getActiveSheet()->setCellValue('B3', 'USER NAME');
$this->excel->getActiveSheet()->setCellValue('C3', 'FEEDBACK');
$this->excel->getActiveSheet()->setCellValue('D3', 'ACTION TAKEN');
$this->excel->getActiveSheet()->setCellValue('E3', 'BY WHOM');
$this->excel->getActiveSheet()->setCellValue('F3', 'DATE OF ACTION');

$this->excel->getActiveSheet()->getStyle("A3:F3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {


  		$feedback_date =  date("d-m-Y", strtotime($value['createdat']));
         $user=$this->Admin_dashboard_model->selectData('registration', '*', array('reg_id'=>$value['reg_id']));
	  		

	$this->excel->getActiveSheet()->setCellValue('A'.$count, $feedback_date);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $user[0]->reg_username);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['feedback']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['act_taken']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['by_whom']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, date("d-m-Y", strtotime($value['act_date'])));
	
	$count++;
}

$filename='Feedback Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>