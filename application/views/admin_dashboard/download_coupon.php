<?php error_reporting(E_ALL & ~(E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('COUPON DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'COUPON DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'COUPON DATE');
$this->excel->getActiveSheet()->setCellValue('B3', 'COUPON CODE');
$this->excel->getActiveSheet()->setCellValue('C3', 'NATURE OF CODE');
$this->excel->getActiveSheet()->setCellValue('D3', 'REFERAL NAME');
$this->excel->getActiveSheet()->setCellValue('E3', 'DESCRIPTION');
$this->excel->getActiveSheet()->setCellValue('F3', 'TARGET GROUP');
$this->excel->getActiveSheet()->setCellValue('G3', 'VALIDITY');
$this->excel->getActiveSheet()->setCellValue('H3', 'START DATE');
$this->excel->getActiveSheet()->setCellValue('I3', 'END DATE');
$this->excel->getActiveSheet()->setCellValue('J3', '% DISCOUNT');
$this->excel->getActiveSheet()->setCellValue('K3', 'STATUS');
$this->excel->getActiveSheet()->getStyle("A3:K3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {


  		$code_date =  date("d-m-Y", strtotime($value['code_date']));

	  		$start_date =  date("d-m-Y", strtotime($value['start_date']));
	  		$end_date =  date("d-m-Y", strtotime($value['end_date']));

	  		if($value['nature_of_code']==1)
	  		{
	  			$nature_of_code = 'Coupon Code';

	  		}else{

	  			$nature_of_code = 'Referal Code';
	  		}


	$this->excel->getActiveSheet()->setCellValue('A'.$count, $code_date);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $value['coupon_code']);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $nature_of_code);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['referrel']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['description']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['target_group']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['validity']);
	$this->excel->getActiveSheet()->setCellValue('H'.$count, $start_date);
	$this->excel->getActiveSheet()->setCellValue('I'.$count, $end_date);
	$this->excel->getActiveSheet()->setCellValue('J'.$count, $value['discount']);
	$this->excel->getActiveSheet()->setCellValue('K'.$count, $value['status']);
	$count++;
}

$filename='COUPON Details '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>