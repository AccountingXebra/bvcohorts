
<!DOCTYPE>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }
		
		#table-price-desk table th, table td {
			text-align: inherit;
			padding: 0em 0em;
			border: none !important;
		}

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
		p{ text-align:center; }
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns" style="border-collapse: collapse;border: 1px solid #fff;background:#fff; margin: 0px 0px 0 0px; width: 100%; border-right: none;">
	<tbody style="border:1px solid #ccc;">   
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
				<tr>
					<td><a href="#" target="_blank"><img style="margin:10px !important; width:185px; height:50px;" width="180" height="65" class="columnImage" src="<?php echo base_url(); ?>public/images/xebra-logo.png" border="0"/></a></td>  
					<td><img style="margin:-9.5% 0 9px 0;" class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0"/></td>
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="text-align:center; font-family: 'Roboto', sans-serif;color: #503cb5;"><h3 style="margin: 20px 0px 20px 0;font-size: 20px;"><strong>Xebra-solving the constant </br>Accounting hassles for MSMEs</strong></h3></td>
    </tr>
    <tr>
        <td align="center" style="text-align:center; font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage" src="<?php echo base_url(); ?>asset/images/newsletter/newsimag8.png" border="0"/></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">74% of small businesses use cloud computing technology for</br>their core processes, since the covid19 pandemic.</p>
			<p style="font-size: 15px">Xebra is one such perfect example of a biz-finance software, that</br>simplifies and automates your small business.</br>It is an automated, SaaS-based unified platform that integrates</br>Business Insights, Invoicing, Expense, Purchase, Inventory, Asset, </br>Payroll, HRMS, Tax, Banking & Accounting modules into one easy application.</p>
			<p style="font-size: 15px">As an entrepreneur, you want to invest majority of your time in </br>improving and expanding your business operations, as well as minimizing costs.</p>
			<p style="font-size: 15px">Read the entire article visit: <a href="https://www.xebra.in/blog/5-ways-small-business-are-adopting-cloud-software" target="_blank" style="color: #503cb5;">https://www.xebra.in/blog/5-ways-small-business-are-adopt-</br>ing-cloud-software</a></p>
			<p style="font-size: 15px">Sign up today to try out our (limited time) free plan for small businesses: </br> <a href="http://bit.ly/39oZbut" target="_blank" style="color: #503cb5;">http://bit.ly/39oZbut</a></p>
			<p style="font-size: 15px">To subscribe to our weekly blog digest,</br> click here <a href="https://xebra.in/blog" target="_blank" style="color: #503cb5;">https://xebra.in/blog</a></p>
		</td>
	</tr>
	<tr><td><img class="columnImage" src="<?php echo base_url(); ?>/asset/images/off.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68; margin:-6px 0 0 0; width:100%;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/xebradotin" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/Xebra.in/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/xebrabiztech/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/xebradotin/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCddSZ6gFMbpIdXeSeucNfTQ?view_as=subscriber" target="_blank"><img style="padding: 0px 0px 10px 0; width:42px; height:40px;" class="" src="<?php echo base_url(); ?>/asset/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	</tbody>
</table>
</body>
</html>
