	<style type="text/css">
		.btn {
			background-color: #bf4b3d;
			color: white;
			padding: 5px 18px;
		}
		.errormsg1 {
		    color: #bf4b3d;
		    font-family: Arial;
		    text-align: center;
		    margin-bottom: 15px;
		 }
		 .form-group{ text-align:left; }
	</style>


	<div class="container col-md-3 mx-auto text-center" style="font-family: Arial">
		<br><br><br>
		<h1 style="font-size: 25px; color: grey; margin-top: 5px;">Admin Login</h1>
		<br><br>

		<?php  
	        if (!empty($success_msg)) {
	            echo '<p class="status-msg success">'.$success_msg.'</p>';
	        } elseif (!empty($error_msg)) {
	            echo '<p class="status-msg error">'.$error_msg.'</p>';
	        }
    	?>

    	<div class="errormsg1">
		    <?php if($msg = $this->session->flashdata('msg')): ?>
		      <?php echo $msg; ?>
		    <?php endif; ?>
		</div>

		<form method="post" action="<?php echo site_url('admin/login'); ?>">
			<div class="form-group">
				<input type="text" name="email" placeholder="EMAIL" class="form-control" required>
			</div>

			<div class="form-group">
				<input type="password" name="password" placeholder="PASSWORD" class="form-control" required>
			</div>

			<div class="form-group">	
				<input class="btn" type="submit" name="loginSubmit" value="LOGIN">
				<a style="text-decoration: none; color: #bf4b3d; margin: 3% 0 0 0; float: right;" href="<?php echo base_url();?>admin/reset-password">Forgot Password?</a>
			</div>
		</form>
	</div>

	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>