  <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
  <style>
		#edit_blog_form_vid{
			margin-top:20px;
		}
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#blog_form_vid .error{ color:red; }
		#excerpt{
			width:40%;
			height:100px;
		}
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-8 mx-auto">
    <form method="post" id="edit_blog_form_vid" name="edit_blog_form_vid" action="" enctype="multipart/form-data">
      <fieldset>
        <div style="text-align: center">
			<p style="color: green; font-family: Arial;">
				<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</p>
        </div>
        <div class="form-group" hidden></div>
        <div class="form-group" hidden></div>
        <div class="form-group">
			<label>Category</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<select class="form-control" name="category" id="category" required >
				<option value="">Select Category</option>
			
				<option value="media"  <?php if($blog[0]->category=="media"){echo "selected";} ?> >MEDIA</option>
				<option value="scheme" <?php if($blog[0]->category=="scheme"){echo "selected";} ?>>SCHEME</option>
				<option value="scheme" <?php if($blog[0]->category=="news"){echo "selected";} ?>>NEWS</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
          <label>Post Name</label>
          <input type="text" class="form-control" name="post_name" id="post_name" placeholder="Post Name" value="<?=$blog[0]->post_name?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
			<div class="form-group">
          <label>Post Customise url</label>
          <input type="text" class="form-control" name="url" id="url" placeholder="Post Customise url" value="<?=$blog[0]->url?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
          <label>Description</label>
          <input type="text" class="form-control" name="description" id="description" value="<?=$blog[0]->description?>" placeholder="Description" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<!-- Author Name -->
		<div class="form-group">
          <label>Author Name</label>
          <input type="text" class="form-control" name="author_name" id="author_name" value="<?=$blog[0]->author_name?>" placeholder="Author Name" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<div class="form-group">
          <label>Alt / Title for Image</label>
          <input type="text" class="form-control" name="alt_name" id="alt_name" placeholder="Alt / Title" value="<?=$blog[0]->alt_name?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<div class="form-group">
			<textarea name="blog_content" id="blog_content"><?=$blog[0]->blog_content?></textarea>
        </div> 
		</br>
		<div class="form-group">
			<label>Featured image</label>
			<input type="file" class="form-control-file" name="featured_image" id="featured_image" >
			<div class="text-danger video-error"></div>
        </div>
		<div class="text-danger file-error"></div>
        <br>
		<div class="form-group">
			<label>Excerpt</label></br>
			<textarea name="excerpt" id="excerpt" maxlength="200"><?=$blog[0]->excerpt?></textarea>
        </div> 
		</br>
        <input style="color:#fff;" type="submit" id="upload_blog" value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_blogs_list">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
			CKEDITOR.replace('blog_content');
		});
	</script>