<style type="text/css">
    
    .contact-header {
      text-align: center;
      font-family: Arial;
      font-size: 17px;
      padding:40px 0;
      color: grey;
      margin-top: -5px;
      margin-bottom: -35px;
    }

    .input-container {
      display: -ms-flexbox;
      display: flex;
      width: 150%;
      margin-bottom: 20px;
      font-family: Arial;
    }

    .icon {
      padding: 15px;
      background: white;
      color: #ccc;
      min-width: 20px;
      text-align: center;
      border: 1.5px solid #ccc;
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }

    .input-field {
      width: 50%;
      padding: 10px;
      outline: none;
      border: 1.5px solid #ccc;
      border-left: 0px;
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    .sub-btn:hover {
      opacity: 1;
    }

    .col-75 {
      float: left;
      width: 100%;
      margin-top: 1px;
      margin-left: -5px;
    }

    .fa-user{
    font-size: 20px;
    }

    #contact-btn{
    border:1px solid #bf4b3d !important;
    background-color: #bf4b3d;
    color:#fff;
    }

    .sub-btn{
    text-align: center;
    margin-top: 5px;
    background-color: #bf4b3d;
    color: white;
    padding: 10px 15px;
    border: none;
    border-radius: 10px;
    cursor: pointer;
    width: 30%;
    opacity: 0.9;
    font-family: Arial;
    margin-left: 185px;
    }

    .errormsg1 {
      color: #bf4b3d;
      font-family: Arial;
    }

</style>

<div class="container-fluid">
    
    <div style="margin-top: 30px; margin-bottom: -10px; text-align: center;">
        <a style="color: grey; font-size: 20px; margin-left: -30px;">Register New User</a>

        <!-- Status message -->
        <div class="errormsg1">
          <?php if($msg = $this->session->flashdata('msg')): ?>
          <?php echo $msg; ?>
          <?php endif; ?>
        </div>
    </div>
	
    <!-- Registration form -->
        <form action="" method="post" style="max-width:500px; margin:auto; padding:3% 1% 3% 6%;">

            <div class="input-container">
              <i class="fa fa-user icon"></i>
              <input class="input-field" type="text" name="first_name" placeholder="FIRST NAME" required>
              <div class="col-sm-6">
                <?php echo form_error('first_name','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="input-container">
              <i class="fa fa-user icon"></i>
              <input class="input-field" type="text" name="last_name" placeholder="LAST NAME" required>
              <div class="col-sm-6">
                <?php echo form_error('last_name','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="input-container">
              <i class="fa fa-envelope icon"></i>
              <input class="input-field" type="email" name="email" placeholder="EMAIL" required>
              <div class="col-sm-6">
                <?php echo form_error('email','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="input-container">
              <i class="fa fa-lock icon"></i>
              <input class="input-field" type="password" name="password" placeholder="PASSWORD" required>
              <div class="col-sm-6">
                <?php echo form_error('password','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="input-container">
              <i class="fa fa-lock icon"></i>
              <input class="input-field" type="password" name="conf_password" placeholder="CONFIRM NEW PASSWORD" required>
              <div class="col-sm-6">
                <?php echo form_error('conf_password','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="input-container">
              <i class="fa fa-phone icon"></i>
              <input class="input-field" type="text" name="phone" placeholder="PHONE NUMBER" required>
              <div class="col-sm-6">
                <?php echo form_error('phone','<p class="errormsg1 help-block">','</p>'); ?>
              </div>
            </div>

            <div class="sub-btn">
                <input type="submit" name="signupSubmit" id="contact-btn" value="Register">
            </div>
        </form>

        <div style="margin-bottom: 85px;">
          
        </div>
</div>