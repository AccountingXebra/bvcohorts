  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <style>
	.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
		.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; color:#bc343a; }
		.datepicker .datelabel{ padding:6% 3%; font-size:13px; }
		.fa.fa-calendar{ margin: -26px 12px 0px 0px; float: right; }
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#create_coworking_space .error{ color:red; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-5 mx-auto">
    <form method="post" id="create_coworking_space" name="create_coworking_space"   enctype="multipart/form-data">
      <fieldset>
        <div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Create Co-Working Space</a>
        </div>

        <div style="text-align: center">
          <p style="color: green; font-family: Arial;">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </p>
        </div>
        
        <div class="form-group" hidden>
         
        </div>

        <div class="form-group" hidden>
         
        </div>

        <!--div class="form-group">
          <select name="language" class="form-control">
            <option value="">Select Language</option>
            <?php //if(count($getLanguage)):?>
              <?php //foreach($getLanguage as $language):?>
                <option value="<?php //echo $language->name;?>"><?php //echo $language->name;?></option>
              <?php //endforeach;?> 
            <?php //else:?>
            <?php //endif;?>
          </select>
          <?php //echo form_error('language', '<div class="text-danger">', '</div>'); ?>
        </div-->  
		<div class="form-group">
			<label>Co-Working Space Name</label>
			<input type="text" class="form-control" name="co_name" id="co_name" value="" placeholder="Co-working space Name" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Upload Photo</label>
			<input type="file" class="form-control-file" name="co_working_image" id="co_working_image" required>
			<div class="text-danger video-error"></div>
        </div>
		</br>
		<div class="form-group">
			<label>Address</label>
			<textarea type="text" class="form-control" name="co_address" id="co_address" placeholder="Co-working space address" required></textarea>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Country</label>
			<select class="form-control" name="co_country" id="co_country" required >
				<option value="">Select Country</option>
				<?php foreach ($countries as $country) { ?>
				<option value="<?php echo $country->country_id; ?>"><?php echo strtoupper($country->country_name); ?></option>
				<?php } ?>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>State</label>
			<select class="form-control" name="co_state" id="co_state" required >
				<option value="">Select State</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>City</label>
			<select class="form-control" name="co_city" id="co_city" required >
				<option value="">Select City</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Email ID</label>
			<input type="text" class="form-control" name="co_emailid" id="co_emailid" placeholder="Email ID" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Contact No.</label>
			<input type="text" class="form-control" name="co_contact" id="co_contact" placeholder="Contact No" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
	
        <input style="color:#fff;" type="submit" id="upload_video_admin"  value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_office">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
		});
		
		$(function () {
			$(".datepicker").datepicker({ 
				autoclose: true, 
				todayHighlight: true
			}).datepicker('update', new Date());
		});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(function() {
		$("#create_coworking_space").submit(function(e){
			e.preventDefault();
		}).validate({

			rules:{
				co_name:{
					required:true,
				},
				co_address:{
					required:true,
				},
				co_country:{
					required:true,
				},
				co_state:{
					required:true,
				},
				co_city:{
					required:true,
				},
				co_emailid:{
					required:true,
				},
				co_contact:{
					required:true,
				},
			},

			messages:{
				co_name:{
					required:"Co-working  name is required",
				},
				co_address:{
					required:"Address is required",
				},
				co_country:{
					required:"Country is required",
				},
				co_state:{
					required:"State is required",
				},
				co_city:{
					required:"City is required",
				},
				co_emailid:{
					required:"Email id is required",
				},
				co_contact:{
					required:"Contact number is required",
				},
			},
			submitHandler: function(form) {
    
		        form.submit();
		       
		    }
		});
	  });
	});
	</script>
	<script>
		$(document).ready(function() {
		$("#co_country").on("change",function(){
            var country_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_states',
              type:"POST",
              data:{'country_id':country_id},
              success:function(res){
                $("#co_state").html(res);
                $("#co_state").parents('.input-field').addClass('label-active');
                $('#co_state').material_select();
              },
            });
          });
		  
		  $("#co_state").on("change",function(){
            var state_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_cities',
              type:"POST",
              data:{'state_id':state_id},
              success:function(res){
               $("#co_city").html(res);
               $("#co_ity").parents('.input-field').addClass('label-active');
               $('#co_city').material_select();
              },
            });
          });
		});
	</script>