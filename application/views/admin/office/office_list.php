<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<style>
	#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
	#content{ margin: 2% 8% 2% 8%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#cowork_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#cowork_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#cowork_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#cowork_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:left !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
	.video_up{ width:85% !important; }
	.no_sub{ width:88% !important; }
	.loader {
		position: fixed;
		left: 0px;
		top: -5%;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
		opacity: 0.8;
	}
	#fixedHeader{ display:none; }
</style>
<div class="loader"></div>
<div id="toast"></div>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-md-9"></div>
			<div class="col-md-3">
				<a style="width: 70%; margin: 0px 0px 15px 23%; font-size: 13px; line-height: 34px; background-color: #bc343a; color: #fff;" href="<?php echo base_url();?>admin/add_office" class="btn" name="" id="" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onmouseover="this.style.border='1px solid #bc343a',this.style.color='#fff'">ADD OFFICE</a>
			</div>	
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="cowork_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr>
								<th style="width:15%;"></th>
								<th style="width:80%;"></th>
								<th style="width:5%;"></th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<script>
$(document).ready(function(){
	$(".loader").fadeOut();
	$('.bvfoot').hide();
});	
</script>