  <!-- <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.20.0/ckeditor.js"></script>
  <style>
		.tooltip {
			position: relative;
			display: contents;
		}

		.tooltip .tooltiptext {
			visibility: hidden;
			font-size: 15px !important;
			width: 230px;
			background-color: #000;
			color: #fff;
			text-align: center;
			border-radius: 6px;
			padding: 5px 0;
			margin: -35px 0 0 0;
			position: absolute;
			z-index: 1;
			height: 35px !important;
		}

		.tooltip:hover .tooltiptext {
			visibility: visible;
		}
		#blog_form_vid{
			margin-top:20px;
		}
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>assets/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#blog_form_vid .error{ color:red; }
		#excerpt{
			width:100%;
			height:100px;
		}
		.cke_contents{ height:280px !important; }
		#url{ text-transform: lowercase; }  
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-8 mx-auto">
    <form method="post" id="blog_form_vid" name="blog_form_vid" action="" enctype="multipart/form-data">
      <fieldset>
        <div style="text-align: center">
			<p style="color: green; font-family: Arial;">
				<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</p>
        </div>
        <div class="form-group" hidden></div>
        <div class="form-group" hidden></div>
        <div class="form-group" style="width:31%; float:left;">
			<label>Category</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<select class="form-control" name="category" id="category" required >
				<option value="">Select Category</option>	
				<option value="media">MEDIA</option>
				<option value="scheme">SCHEME</option>
				<option value="news">NEWS</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		<div class="form-group" style="width:31%; float:left; margin-left:20px;">
			<label>Year</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<?php $year=date('Y'); ?>	
			<select id="newsletter_year" name="newsletter_year" class="country-dropdown form-control" required>
				<option value="0" selected>Select Year</option>
				<?php for($i=$year;$i > $year-8;$i--){ ?>
				<option value="<?=$i?>" <? if(date("Y") == $i){?>selected<? } ?>><?=$i?></option>
				<?php } ?>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		<div class="form-group" style="width:31%; float:left; margin-left:20px;">
			<label>Month</label>
			<select id="newsletter_month" name="newsletter_month" class="country-dropdown form-control">
				<option value="0" <? if(date("m") == 0){?>selected<? } ?>>Select Month</option>
				<option value="1" <? if(date("m") == 1){?>selected<? } ?>>January</option>
				<option value="2" <? if(date("m") == 2){?>selected<? } ?>>February</option>
				<option value="3" <? if(date("m") == 3){?>selected<? } ?>>March</option>
				<option value="4" <? if(date("m") == 4){?>selected<? } ?>>April</option>
				<option value="5" <? if(date("m") == 5){?>selected<? } ?>>May</option>
				<option value="6" <? if(date("m") == 6){?>selected<? } ?>>June</option>
				<option value="7" <? if(date("m") == 7){?>selected<? } ?>>July</option>
				<option value="8" <? if(date("m") == 8){?>selected<? } ?>>August</option>
				<option value="9" <? if(date("m") == 9){?>selected<? } ?>>September</option>
				<option value="10" <? if(date("m") == 10){?>selected<? } ?>>October</option>
				<option value="11" <? if(date("m") == 11){?>selected<? } ?>>November</option>
				<option value="12" <? if(date("m") == 12){?>selected<? } ?>>December</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group" style="margin-top:7%;">
          <label>Post Title (60 Character)</label>
          <input type="text" class="form-control" name="post_name" id="post_name" placeholder="Post Name" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<div class="form-group">
          <label>Post Customise url</label>
		  <p class="tooltip"><img src="<?php echo base_url(); ?>asset/images/infocircle.png"/><span class="tooltiptext">Do not use in special charatcter</span></p>
          <input type="text" class="form-control" name="url" id="url" placeholder="Post Customise url" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<!--<div class="form-group">
          <label>Description</label>
          <input type="text" class="form-control" name="description" id="description" placeholder="Description" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<!-- Author Name -->
		<!--<div class="form-group">
          <label>Author Name</label>
          <input type="text" class="form-control" name="author_name" id="author_name" placeholder="Author Name" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<div class="form-group">
          <label>Alt / Title for Image</label>
          <input type="text" class="form-control" name="alt_name" id="alt_name" placeholder="Alt / Title" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>-->
		
		<div class="form-group">
			<textarea name="blog_content" id="blog_content"></textarea>
        </div> 
		<p style="font-size:18px;">Blog Index Page Deatils</p>
		</br>
		
		<div class="form-group">
			<label>Featured image (Only jpg & png format)</label>
			<input type="file" class="form-control-file" name="featured_image" id="featured_image" required>
			<div class="text-danger video-error"></div>
        </div>
		<div class="text-danger file-error"></div>
        <br>
		<div class="form-group">
			<label>Excerpt (Not exceeding 200 character)</label></br>
			<textarea name="excerpt" id="excerpt" maxlength="200"></textarea>
        </div> 
		</br>
        <input style="color:#fff;" type="submit" id="upload_blog" value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_blogs_list">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
			CKEDITOR.replace('blog_content');
		});
	</script>
	<script>
		$(document).on("change","#featured_image",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["image/jpeg", "image/jpg", "image/png"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Image format is incorrect.'
					});
					$("#featured_image").val('');
				}
				/*else if(filesize_mb > 100) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Upload video size should not be more than 100mb'
					});
					$("#video_up").val('');
				}*/	
			}
		});
		
		$('#post_name').blur(function(){
			var a = $('#post_name').val().replace(" ", "-");
			console.log(humanize(a));
			$('#url').val(humanize(a));
		});
		function humanize(str) {
			var i, frags = str.split(' ');
			for (i=0; i<frags.length; i++) {
				frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
			}
			return frags.join('-');
		}
		
		console.log(base_url+'ck_upload.php');
		CKEDITOR.replace('blog_content', {
			filebrowserUploadUrl: base_url+'admin/ck_upload',
			filebrowserUploadMethod: 'form'
		});	
	</script>