  <style>
		#toast {
			position: fixed;
			z-index: 9999;
			top: 12%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 10px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -400px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
	
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-5 mx-auto">
    <form method="post" action="<?php echo base_url(); ?>admin/upload" enctype="multipart/form-data">
      <fieldset>
        <div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Upload Video</a>
        </div>

        <div style="text-align: center">
          <p style="color: green; font-family: Arial;">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </p>
        </div>
        
        <div class="form-group" hidden>
          <select name="particular" class="form-control" id="particular">
            <option value="Categories">Categories</option>
          </select>
        </div>

        <div class="form-group" hidden>
          <input type="text" name="uploaded_by" id="uploaded_by" value="<?php echo $user['first_name'];?>">
        </div>

        <!--div class="form-group">
          <select name="language" class="form-control">
            <option value="">Select Language</option>
            <?php //if(count($getLanguage)):?>
              <?php //foreach($getLanguage as $language):?>
                <option value="<?php //echo $language->name;?>"><?php //echo $language->name;?></option>
              <?php //endforeach;?> 
            <?php //else:?>
            <?php //endif;?>
          </select>
          <?php echo form_error('language', '<div class="text-danger">', '</div>'); ?>
        </div-->  
        <div class="form-group">
          <label>Video File</label>
          <input type="file" class="form-control-file" name="video" id="video_up" required>
          OR
          <input type="text" class="form-control" name="video_link" id="video_link" placeholder="Paste your Youtube video link">
          <?php echo form_error('video', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger video-error"></div>
        </div>
		<div class="text-danger file-error"></div>
        <div class="form-group">
          <label>Video Title</label>
          <input type="text" class="form-control" name="title" id="video_title" placeholder="Enter your video title" required>
          <?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger title-error"></div>
        </div> 
        <div class="form-group">
          <label>Video Customize Url</label>
          <input type="text" class="form-control" name="cust_url" id="cust_url" placeholder="Enter url" required>
          <?php echo form_error('cust_url', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger url-error"></div>
        </div> 
		
          
        <div class="form-group">
          <label>Video Description</label>
          <textarea class="form-control" rows="7" name="description" id="video_desc" placeholder="Enter your video description" required></textarea>
          <?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger desc-error"></div>
        </div>
        
        <div class="form-group">
          <label>Video Url</label>
          <input type="text" class="form-control" name="video_url" id="video_url" placeholder="Enter url" required>
          <?php echo form_error('cust_url', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger url-error"></div>
        </div> 
		<div class="form-group">
			<label>Upload Video Thumbnail</label>
			<div class="col-lg-6 thumbnail_box left-margin" style="background-image: url('../assets/images/Upthumb.png'); background-size: cover; border: 1px solid rgb(191, 222, 224); height: 110px; background-position: center center; min-width: 185px;">
				<input type="file" class="form-control hide-file-thumbnail" id="thumbnail" name="thumbnail" required>
				<input type="hidden" id="image_info" name="image_info" value="" />
			</div>
		</div>	
        <br>
        <!--Category 1-->
        <div class="form-group">
          <select name="category" class="form-control" id="category" onchange="makeSubmenu(this.value)" required>
            <option value="" disabled selected>Select Main Category</option>
            <!--option value="Artists">Art, Culture & Artists</option-->
            <option value="Science">Science & Technology</option>
            <option value="Pets">Safety & Environment</option>
            <option value="People">Inspiring Innovators</option>
            <option value="Health">Health & Fitness</option>
            <option value="Lifestyle">Travel & Lifestyle</option>
            <option value="Education">Urban & Rural Life</option>
            <!--option value="Child">Child & Elderly</option-->
          </select>

          <?php echo form_error('category', '<div class="text-danger">', '</div>'); ?>

          <br>
          <select name="subcategory" class="form-control" id="categorySelect" size="1" required>
            <option value="" disabled selected>Select Subcategory</option>
            <option></option>
          </select>
        </div>
        <br>
        <!--Category 2-->
        <div class="form-group">
          <select name="category2" class="form-control" id="category2" onchange="makeSubmenu2(this.value)">
            <option value="" disabled selected>Select Main Category</option>
            <!--option value="Artists">Art, Culture & Artists</option-->
            <option value="Science">Science & Technology</option>
            <option value="Pets">Safety & Environment</option>
            <option value="People">Inspiring Innovators</option>
            <option value="Health">Health & Fitness</option>
            <option value="Lifestyle">Travel & Lifestyle</option>
            <option value="Education">Urban & Rural Life</option>
            <!--option value="Child">Child & Elderly</option-->
          </select>

          <?php echo form_error('category2', '<div class="text-danger">', '</div>'); ?>

          <br>
          <select name="subcategory2" class="form-control" id="categorySelect2" size="1">
            <option value="" disabled selected>Select Subcategory</option>
            <option></option>
          </select>
        </div>
        <br>
		<div class="form-group">
			<div class="col-lg-12 login_form_textbox tagsbox">
				<div id="select_tags">
					<input type="text" value="" placeholder="Type Tags"  name="tags" id="tags" />
				</div>
			</div>
		</div>	
		<br>
        <!--Category 3-->
        <!--div class="form-group">
          <select name="category3" class="form-control" id="category3" onchange="makeSubmenu3(this.value)">
            <option value="" disabled selected>Select Main Category 3</option>
            <option value="Artists">Art, Culture & Artists</option>
            <option value="Science">Science & Tech</option>
            <option value="Pets">Pets & Environment</option>
            <option value="People">People</option>
            <option value="Health">Health</option>
            <option value="Lifestyle">Lifestyle & Outdoors</option>
            <option value="Education">Education & Development</option>
            <option value="Child">Child & Elderly</option>
          </select>

          <?php echo form_error('category3', '<div class="text-danger">', '</div>'); ?>

          <br>
          <select name="subcategory3" class="form-control" id="categorySelect3" size="1">
            <option value="" disabled selected>Select Subcategory</option>
            <option></option>
          </select>
        </div>
        <br>
        <!--Category 4-->
        <!--div class="form-group">
          <select name="category4" class="form-control" id="category4" onchange="makeSubmenu4(this.value)">
            <option value="" disabled selected>Select Main Category 4</option>
            <option value="Artists">Art, Culture & Artists</option>
            <option value="Science">Science & Tech</option>
            <option value="Pets">Pets & Environment</option>
            <option value="People">People</option>
            <option value="Health">Health</option>
            <option value="Lifestyle">Lifestyle & Outdoors</option>
            <option value="Education">Education & Development</option>
            <option value="Child">Child & Elderly</option>
          </select>

          <?php echo form_error('category4', '<div class="text-danger">', '</div>'); ?>

          <br>
          <select name="subcategory4" class="form-control" id="categorySelect4" size="1">
            <option value="" disabled selected>Select Subcategory</option>
            <option></option>
          </select>
        </div>
        <br>
        <!--Category 5-->
        <!--div class="form-group">
          <select name="category5" class="form-control" id="category5" onchange="makeSubmenu5(this.value)">
            <option value="" disabled selected>Select Main Category 5</option>
            <option value="Artists">Art, Culture & Artists</option>
            <option value="Science">Science & Tech</option>
            <option value="Pets">Pets & Environment</option>
            <option value="People">People</option>
            <option value="Health">Health</option>
            <option value="Lifestyle">Lifestyle & Outdoors</option>
            <option value="Education">Education & Development</option>
            <option value="Child">Child & Elderly</option>
          </select>

          <?php echo form_error('category5', '<div class="text-danger">', '</div>'); ?>

          <br>
          <select name="subcategory5" class="form-control" id="categorySelect5" size="1">
            <option value="" disabled selected>Select Subcategory</option>
            <option></option>
          </select>
        </div-->

        <br>

        <a style="color:#fff;" type="submit" id="upload_video_admin" value="Submit" class="btn btn-success">Submit</a>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>user/dashboard">Cancel</a> 
      </fieldset>
    </form>
    <br>
  </div>
  <script>
	$(document).ready(function(){
		$(".loader").hide();	
		
		$(document).on("change","#video_up",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filesize_mb =  filesize / 1048576;
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["video/mp4", "video/mov", "video/wmv", "video/flv", "video/avi"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Video Format is incorrect.'
					});
					$("#video_up").val('');
				}else if(filesize_mb > 100) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Upload video size should not be more than 100mb'
					});
					$("#video_up").val('');
				}	
			}
		});
		
		$('#upload_video_admin').on('click',function(){
			//	var str = $("form").serializeArray();
			var flag = 0;
			var title=$('#video_title').val();
            var desc=$('#video_desc').val();
             var url=$('#cust_url').val();
            var category=$('#category').val();
            var category1=$('#category2').val();
            var subcategory=$('#categorySelect').val();
            var subcategory1=$('#categorySelect2').val();
            var tags=$('#select_tags').val();
            var link=$('#video_link').val();
            var username=$('#uploaded_by').val();
			var formData = new FormData();
			formData.append('title', title);
			formData.append('desc', desc);
			formData.append('tags', tags);
			formData.append('url', url);
			formData.append('category',category);
			formData.append('category1',category1);
			formData.append('subcategory',subcategory);
			formData.append('subcategory1',subcategory1);
			formData.append('link',link);
			formData.append('username',username);
					
			formData.append('file', $('#video_up')[0].files[0]);
			formData.append('thumbnail', $('#thumbnail')[0].files[0]);
			var videofile = $('#video_up')[0].files[0];
			if(title == ""){
				$('#video_title').focus();
				flag = 1;
				$('.title-error').html('The Title field is required.');
			}else{
				flag = 0;
				$('.title-error').html();
			}


			if(url == ""){
				$('#cust_url').focus();
				flag = 1;
				$('.url-error').html('The Url field is required.');
			}else{
				flag = 0;
				$('.url-error').html();
			}
			if(desc == ""){
				$('#video_desc').focus();
				flag = 1;
				$('.desc-error').html('The Description field is required.');
			}else{
				flag = 0;
				$('.desc-error').html();
			}
			/*if(videofile == "undefined"){
				$('#video_up').focus();
				flag = 1;
				$('.video-error').html('The Video File field is required.');
			}else{
				flag = 0;
				$('.video-error').html();
			}*/
		if(flag == 0){
			$(".loader").show();
			$.ajax({
			url:base_url+'admin/upload',
				method:"POST",
				data:formData,
				contentType:false,
				cache:false,
				processData:false,
				success:function(data){
					$('#video_up').val('');
					if(data==1){
						//alert("video upload successfully");
						window.setTimeout(function(){
							$(".loader").hide();
						window.setTimeout(function(){
							$("#toast").toast({
								type: 'success',
								message: 'Video Uploaded Successfully!'
							});
						window.setTimeout(function(){	
							window.location.href=base_url+'admin/view-uploaded-video';
						}, 200);
						}, 300);
						}, 500);
					}else{
						//alert("Something went wrong.Try again...");
						$(".loader").hide();
						$("#toast").toast({
							type: 'error_1',
							message: 'Something went wrong. Try again...'
						});
					}
				}
			})
		}
		
		});
	});	
  
  /* Adding Tag Box for upload video */
	$(function(){ 
		$("#select_tags input").on({
				focusout : function() {
				var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
				if(txt) $("<span/>", {text:txt.toLowerCase(), insertBefore:this});
				this.value = "";
			},
			keyup : function(ev) {
				// if: comma|enter (delimit more keyCodes with | pipe)
				if(/(188|13)/.test(ev.which)) $(this).focusout(); 
				return false;
			}
		});
		$('#select_tags').on('click', 'span', function() {
			if(confirm("Remove "+ $(this).text() +"?")) $(this).remove(); 
		});
	});
</script>	