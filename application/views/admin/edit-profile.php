<style type="text/css">
    
    .contact-header {
      text-align: center;
      font-family: Arial;
      font-size: 17px;
      padding:40px 0;
      color: grey;
      margin-top: -5px;
      margin-bottom: -35px;
    }

    .input-container {
      display: -ms-flexbox;
      display: flex;
      width: 150%;
      margin-bottom: 20px;
      font-family: Arial;
    }

    .icon {
      padding: 15px;
      background: white;
      color: #ccc;
      min-width: 20px;
      text-align: center;
      border: 1.5px solid #ccc;
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }

    .input-field {
      width: 50%;
      padding: 10px;
      outline: none;
      border: 1.5px solid #ccc;
      border-left: 0px;
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    .sub-btn:hover {
      opacity: 1;
    }

    .col-75 {
      float: left;
      width: 100%;
      margin-top: 1px;
      margin-left: -5px;
    }

    .fa-user{
    font-size: 20px;
    }

    #contact-btn{
    border:1px solid #bf4b3d !important;
    background-color: #bf4b3d;
    color:#fff;
    }

    .sub-btn{
    text-align: center;
    margin-top: 5px;
    background-color: #bf4b3d;
    color: white;
    padding: 10px 15px;
    border: none;
    border-radius: 10px;
    cursor: pointer;
    width: 30%;
    opacity: 0.9;
    font-family: Arial;
    margin-left: 185px;
    }

</style>

<div class="container-fluid">
    
  <div style="margin-top: 40px; margin-bottom: -5px; text-align: center;">
      <a style="color: grey; font-size: 20px; margin-left: -40px;">Edit Profile Details</a>

      <!-- Status message -->
     <div style="color: green; margin-left: -40px; margin-top: 10px;">
        <?php if($msg = $this->session->flashdata('msg')): ?>
        <?php echo $msg; ?>
        <?php endif; ?>
      </div>
  </div>
	
  <!-- Registration form -->
  <form action="update_profile" method="post" style="max-width:500px; margin:auto; padding:3% 1% 3% 6%;">
    <input class="input-field" type="hidden" name="id"  value="<?=$user['id']?>">
    <div class="input-container">
      <i class="fa fa-user icon"></i>
      <input class="input-field" type="text" name="first_name" placeholder="FIRST NAME" value="<?php echo $user['first_name'];?>">
      <div class="col-sm-6">
        <?php echo form_error('first_name','<p class="help-block">','</p>'); ?>
      </div>
    </div>

    <div class="input-container">
      <i class="fa fa-user icon"></i>
      <input class="input-field" type="text" name="last_name" placeholder="LAST NAME" value="<?php echo $user['last_name'];?>">
      <div class="col-sm-6">
        <?php echo form_error('last_name','<p class="help-block">','</p>'); ?>
      </div>
    </div>

    <div class="input-container">
      <i class="fa fa-envelope icon"></i>
      <input class="input-field" type="email" name="email" placeholder="EMAIL" value="<?php echo $user['email'];?>">
      <div class="col-sm-6">
        <?php echo form_error('email','<p class="help-block">','</p>'); ?>
      </div>
    </div>

    <div class="input-container">
      <i class="fa fa-phone icon"></i>
      <input class="input-field" type="text" name="phone" placeholder="PHONE NUMBER" value="<?php echo $user['phone'];?>">
      <div class="col-sm-6">
        <?php echo form_error('phone','<p class="help-block">','</p>'); ?>
      </div>
    </div>

    <div class="sub-btn">
      <input type="submit" name="Submit" id="contact-btn" value="Update">
    </div>
  </form>
</div>

<br><br><br><br><br><br><br><br>