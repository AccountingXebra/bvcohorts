<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
    <title>Add new Video</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <a class="navbar-brand" href="<?php echo base_url(); ?>admin"><img src="<?php echo base_url();?>assets/images/logo/header_logo.jpg"></a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    </nav>
    <hr>

<div class="container col-md-5 mx-auto">

  <form method="post" action="<?php echo base_url();?>dashboard/editors_save" enctype="multipart/form-data">
  	<fieldset>
    	<legend>Add new Video</legend>

      <div style="text-align: center">
        <p style="color: #bf4b3d; font-family: Arial;">
          <?php if($msg = $this->session->flashdata('msg')): ?>
          <?php echo $msg; ?>
          <?php endif; ?>
        </p>
      </div>

      <div class="form-group"></div>
    	
      <div class="form-group">
        <label>Video File</label>
          <input type="file" name="video" class="form-control-file">
          OR
          <input type="text" name="video_link" class="form-control" placeholder="Paste a video link">
        <?php echo form_error('video', '<div class="text-danger">', '</div>'); ?>
     </div> 

    	<div class="form-group">
    		<label>Title</label>
        <input type="text" name="title" placeholder="Enter video title" class="form-control">
        <?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
  		</div>

      <div class="form-group">
        <label>Language</label>
        <select name="language" class="form-control">
          <option value="">Select Language</option>
          <?php if(count($getLanguage)):?>
            <?php foreach($getLanguage as $language):?>
                <option value="<?php echo $language->name;?>"><?php echo $language->name;?></option>
            <?php endforeach;?> 
          <?php else:?>
          <?php endif;?>
        </select>
        <?php echo form_error('language', '<div class="text-danger">', '</div>'); ?>
      </div>
    	
    	<div class="form-group">
      		<label>Description</label>
            <textarea class="form-control" rows="5" cols="50" name="description" placeholder="Enter video description..." style="color: grey"></textarea>
    		</div>

    		<div col-md-5>
    			<?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
    		</div>
    	
    		<div class="form-group">
    				<button type="submit" name="submit" class="btn btn-success">Submit</button>
    				<?php echo anchor('dashboard/editor_pick', 'Back', ['class'=>'btn btn-danger']); ?>
    		</div>
  	
  	</fieldset>
  	</form>

</div>

</body>
</html>
