
	<div class="container-fluid" style="line-height: 2;">
		<div style="margin-top: 30px; margin-bottom: 23px; margin-left: 20px;">
            <a style="color: grey; font-size: 20px;">Video Details</a>
        </div>
        <div style="margin-left: 20px;">
			<b>Title:</b> <?php echo $post->title; ?>
			<p><b>Category:</b> <?php echo $post->category; ?></p>
			<p><b>Sub-category:</b> <?php echo $post->subcategory;?>, <?php echo $post->subcategory2;?>, <?php echo $post->subcategory3;?>, <?php echo $post->subcategory4;?>, <?php echo $post->subcategory5;?>,</p>
			<p><b>Language:</b> <?php echo $post->language; ?></p>
			<p><b>Description:</b> <?php echo $post->description; ?></p>
			<p><b>Date uploaded:</b> <?php echo $post->date_created; ?></p>
			<p><b>Video:</b></p>
			<?php if($post):?>
				<iframe src="<?php echo $post->video; ?>" width="540" height="305" allowfullscreen></iframe>
			<?php else: ?>
				<p>No Videos Found!</p>
			<?php endif; ?>
		</div>
		<br>
	</div>
