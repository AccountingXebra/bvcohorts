<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style>
	#content{ margin: 2% 2% 2% 2%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#uservideo_details_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#uservideo_details_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#uservideo_details_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#uservideo_details_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(4){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(5){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(6){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(7){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(8){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(9){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(10){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(11){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
	#start_date, #end_date{ float:right; width:20%; color:#bc343a; }
	.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
	.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; color:#bc343a; }
	.datepicker .datelabel{ padding:6% 3%; font-size:13px; }
</style>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-lg-5">		
			<div class="input-field" style="margin:0 0 20px 0px;">
				<a class="nav-link shadow dropdown-toggle" href="#" id="bulk_action" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BULK ACTIONS</a>
				<div class="dropdown-menu" aria-labelledby="bulk_action">
					<a class="dropdown-item print_uservideo" id="user_video_print" data-multi_uservideo="0">PRINT</a>
				</div>
			</div>	
			</div>
			<div class="col-lg-7" style="padding-right: 28px;">
				<div id="end_date" class="datepicker input-group date" data-date-format="dd/mm/yyyy">
					<input id="sub_end_date" name="sub_end_date" class="form-control" type="text" readonly />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<div id="start_date" class="datepicker input-group date" data-date-format="dd/mm/yyyy">
					<input id="sub_start_date" name="sub_start_date" class="form-control" type="text" placeholder="Start Date" readonly />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
			<div class="input-field" style="margin:0 15px 20px 15px; float:right;">
				<a class="cat-drop nav-link shadow dropdown-toggle" href="#" id="category" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CATEGORY</a>
				<div class="dropdown-menu" aria-labelledby="status">
					<a class="dropdown-item">CAT1</a>
				</div>
			</div>
			</div>			
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="uservideo_details_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr style="border:1px solid red;">
								<th style="width:5%; vertical-align:middle">
									<input type="checkbox" id="uservideo_bulk" name="uservideo_bulk" class="filled-in purple" />
									<label for="uservideo_bulk"></label>
								</th>
								<th style="text-align:center; width:10%;">First & Last</br>Name</th>
								<th style="text-align:center; width:12%;">Video Upload</br>Date</th>
								<th style="text-align:center; width:18%;">Video Title</th>
								<th style="text-align:center; width:20%;">Video Description</th>
								<th style="text-align:center; width:10%;">Category 1</th>
								<th style="text-align:center; width:10%;">Category 2</th>
								<th style="text-align:center; width:15%;">Action</th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function(){
	$('.bvfoot').hide();
});	
$(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());
});
</script>