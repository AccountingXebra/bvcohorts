<style>
	input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
	#select_tags{ float: left; padding: 5px; width: 21%; border: 1px solid #ccc; border-radius: 5px; margin: 5px 50% 20px 29%; height: 100px; }
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
</style>
<center>
  	<div class="container-fluid" style="line-height: 2">

	  	<?php //echo form_open("admin/details_change/{$post->id}", ['class'=>'form-horizontal']); ?>
		<form method="post" action="<?php echo base_url(); ?>admin/details_change/<?php echo $post->id ?>" enctype="multipart/form-data">
	  	<fieldset>
	    	<div style="margin-top: 25px; margin-bottom: 23px; margin-left: 20px;">
            	<a style="color: grey; font-size: 20px;">Update Details</a>
        	</div>
	    	<div class="form-group">
	      		<label for="InputTitle" class="col-md-2 control-label" style="margin-right: 425px; font-size: 20px; color: grey;">Title</label>
	      		<div class="col-md-5">
	      			<?php echo form_input(['name'=>'title', 'placeholder'=>'Enter Title', 'class'=>'form-control', 'value'=>set_value('title', $post->title)]); ?>
	    		</div>
	    		<div col-md-5>
	    			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
			
			<div class="form-group">
	      		<label for="InputTitle" class="col-md-2 control-label" style="margin-right: 340px; font-size: 20px; color: grey;"> Customize Url</label>
	      		<div class="col-md-5">
	      			<?php echo form_input(['name'=>'cust_url', 'placeholder'=>'Enter Url', 'class'=>'form-control', 'value'=>set_value('cust_url', $post->url)]); ?>
	    		</div>
	    		<div col-md-5>
	    			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
	    	
	    	<div class="form-group">
	      		<label for="descriptionTextarea" class="col-md-2 control-label" style="margin-right: 360px; font-size: 20px; color: grey;">Description</label>
	      		<div class="col-md-5">
	      			<?php echo form_textarea(['name'=>'description', 'placeholder'=>'Enter Description', 'class'=>'form-control', 'value'=>set_value('description', $post->description)]); ?>
	    		</div>
	    		<div class="col-md-5">
	    			<?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
	    	
	    	<div class="form-group">
	      		<label for="InputTitle" class="col-md-2 control-label" style="margin-right: 340px; font-size: 20px; color: grey;"> Video Url</label>
	      		<div class="col-md-5">
	      			<?php echo form_input(['name'=>'video_url', 'placeholder'=>'Enter Url', 'class'=>'form-control', 'value'=>set_value('cust_url', $post->video_url)]); ?>
	    		</div>
	    		<div col-md-5>
	    			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
			
			<div class="form-group" style="padding:0 3%;">
				<label for="descriptionTextarea" class="col-md-3 control-label" style="margin-right: 250px; font-size: 20px; color: grey;">Upload Video Thumbnail</label>
				<div class="col-md-5 thumbnail_box left-margin" style="background-image: url('<?=$post->thumbnail?>'); background-size: cover; height: 110px; background-position: center center; min-width: 185px; border: 1px solid #ccc; border-radius: 5px;">
					<input type="file" class="form-control hide-file-thumbnail" id="thumbnail" name="thumbnail">
					<input type="hidden" id="image_info" name="image_info" value="<?=$post->thumbnail?>" />
				</div>
			</div>	
			<br>

	    	<div class="form-group">
	    		<label for="categories" class="col-md-2 control-label" style="margin-right: 375px; font-size: 20px; color: grey;">Categories</label>
	    	</div>

	    	<div class="form-group">
	    		<label style="margin-right: 392px; font-size: 17px; color: grey;">Category 1</label><br>
	    		<select class="col-md-4 control-label" style="margin-right: 240px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="category" class="form-control" id="category" onchange="makeSubmenu(this.value)">
	    			<option  value="<?php echo $post->category;?>" disabled selected><?php echo $post->category;?></option>
	    			<option value="Artists">Art, Culture & Artists</option>
		            <option value="Science">Science & Tech</option>
		            <option value="Pets">Pets & Environment</option>
		            <option value="People">People</option>
		            <option value="Health">Health</option>
		            <option value="Lifestyle">Lifestyle & Outdoors</option>
		            <option value="Education">Education & Development</option>
		            <option value="Child">Child & Elderly</option>
	    		</select>
	    		<?php echo form_error('category', '<div class="text-danger">', '</div>'); ?>
	    		<br>
		        <select class="col-md-4 control-label" style="margin-right: 245px; margin-top: 10px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="subcategory" class="form-control" id="categorySelect" size="1">
		           <option value="<?php echo $post->subcategory?>" disabled selected><?php echo $post->subcategory;?></option>
		           <option></option>
		        </select>
	    	</div>

	    	<div class="form-group">
	    		<label style="margin-right: 392px; font-size: 17px; color: grey;">Category 2</label><br>
	    		<select class="col-md-4 control-label" style="margin-right: 240px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="category2" class="form-control" id="category2" onchange="makeSubmenu2(this.value)">
	    			<option  value="<?php echo $post->category2;?>" disabled selected><?php echo $post->category2;?></option>
	    			<option value="Artists">Art, Culture & Artists</option>
		            <option value="Science">Science & Tech</option>
		            <option value="Pets">Pets & Environment</option>
		            <option value="People">People</option>
		            <option value="Health">Health</option>
		            <option value="Lifestyle">Lifestyle & Outdoors</option>
		            <option value="Education">Education & Development</option>
		            <option value="Child">Child & Elderly</option>
	    		</select>
	    		<?php echo form_error('category2', '<div class="text-danger">', '</div>'); ?>
	    		<br>
		        <select class="col-md-4 control-label" style="margin-right: 245px; margin-top: 10px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="subcategory2" class="form-control" id="categorySelect2" size="1">
		           <option value="<?php echo $post->subcategory2?>" disabled selected><?php echo $post->subcategory2;?></option>
		           <option></option>
		        </select>
	    	</div>

	    	<!--div class="form-group">
	    		<label style="margin-right: 430px; font-size: 17px; color: grey;">Category 3</label><br>
	    		<select class="col-md-4 control-label" style="margin-right: 240px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="category3" class="form-control" id="category3" onchange="makeSubmenu3(this.value)">
	    			<option  value="<?php echo $post->category3;?>" disabled selected><?php echo $post->category3;?></option>
	    			<option value="Artists">Art, Culture & Artists</option>
		            <option value="Science">Science & Tech</option>
		            <option value="Pets">Pets & Environment</option>
		            <option value="People">People</option>
		            <option value="Health">Health</option>
		            <option value="Lifestyle">Lifestyle & Outdoors</option>
		            <option value="Education">Education & Development</option>
		            <option value="Child">Child & Elderly</option>
	    		</select>
	    		<?php echo form_error('category3', '<div class="text-danger">', '</div>'); ?>
	    		<br>
		        <select class="col-md-4 control-label" style="margin-right: 245px; margin-top: 10px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="subcategory3" class="form-control" id="categorySelect3" size="1">
		           <option value="<?php echo $post->subcategory3?>" disabled selected><?php echo $post->subcategory3;?></option>
		           <option></option>
		        </select>
	    	</div>

	    	<div class="form-group">
	    		<label style="margin-right: 430px; font-size: 17px; color: grey;">Category 4</label><br>
	    		<select class="col-md-4 control-label" style="margin-right: 240px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="category4" class="form-control" id="category4" onchange="makeSubmenu4(this.value)">
	    			<option  value="<?php echo $post->category4;?>" disabled selected><?php echo $post->category4;?></option>
	    			<option value="Artists">Art, Culture & Artists</option>
		            <option value="Science">Science & Tech</option>
		            <option value="Pets">Pets & Environment</option>
		            <option value="People">People</option>
		            <option value="Health">Health</option>
		            <option value="Lifestyle">Lifestyle & Outdoors</option>
		            <option value="Education">Education & Development</option>
		            <option value="Child">Child & Elderly</option>
	    		</select>
	    		<?php echo form_error('category4', '<div class="text-danger">', '</div>'); ?>
	    		<br>
		        <select class="col-md-4 control-label" style="margin-right: 245px; margin-top: 10px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="subcategory4" class="form-control" id="categorySelect4" size="1">
		           <option value="<?php echo $post->subcategory4?>" disabled selected><?php echo $post->subcategory4;?></option>
		           <option></option>
		        </select>
	    	</div>

	    	<div class="form-group">
	    		<label style="margin-right: 430px; font-size: 17px; color: grey;">Category 5</label><br>
	    		<select class="col-md-4 control-label" style="margin-right: 240px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="category5" class="form-control" id="category5" onchange="makeSubmenu5(this.value)">
	    			<option  value="<?php echo $post->category5;?>" disabled selected><?php echo $post->category5;?></option>
	    			<option value="Artists">Art, Culture & Artists</option>
		            <option value="Science">Science & Tech</option>
		            <option value="Pets">Pets & Environment</option>
		            <option value="People">People</option>
		            <option value="Health">Health</option>
		            <option value="Lifestyle">Lifestyle & Outdoors</option>
		            <option value="Education">Education & Development</option>
		            <option value="Child">Child & Elderly</option>
	    		</select>
	    		<?php echo form_error('category5', '<div class="text-danger">', '</div>'); ?>
	    		<br>
		        <select class="col-md-4 control-label" style="margin-right: 245px; margin-top: 10px; font-size: 17px; color: black; padding: 7px 0 7px 10px !important; border-radius: 5px; width: 20%; border: 1px solid #ccc;" name="subcategory5" class="form-control" id="categorySelect5" size="1">
		           <option value="<?php echo $post->subcategory5?>" disabled selected><?php echo $post->subcategory5;?></option>
		           <option></option>
		        </select>
	    	</div-->
			
			<div class="form-group">
				<div class="col-lg-12 login_form_textbox tagsbox">
					<div id="select_tags">
						<!--<span></span><!-- Add code here for tags -->
						<input type="text" value="<?=$post->tags?>" placeholder="Type Tags"  name="tags" id="tags" />
					</div>
				</div>
			</div>	
			</br>
	    	
    		<div class="form-group">
    			<div class="col-md-10 col-md-offset-2">
    				<div style="margin-left: -340px;">
	    				<?php echo form_submit(['name'=>'submit', 'value'=>'Update', 'class'=>'btn btn-success']); ?>
	    				<?php echo anchor('admin/view-uploaded-video', 'Cancel', ['class'=>'btn btn-danger']); ?>
    				</div>
    			</div>
    		</div>
	  	</fieldset>
	  	<?php //echo form_close(); ?>
		</form>
  	</div>

  	<br><br>

</center>
<script>
  /* Adding Tag Box for upload video */
	$(function(){ 
		$("#select_tags input").on({
				focusout : function() {
				var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
				if(txt) $("<span/>", {text:txt.toLowerCase(), insertBefore:this});
				this.value = "";
			},
			keyup : function(ev) {
				// if: comma|enter (delimit more keyCodes with | pipe)
				if(/(188|13)/.test(ev.which)) $(this).focusout(); 
				return false;
			}
		});
		$('#select_tags').on('click', 'span', function() {
			if(confirm("Remove "+ $(this).text() +"?")) $(this).remove(); 
		});
	});
</script>