  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <style>
	.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
		.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; color:#bc343a; }
		.datepicker .datelabel{ padding:6% 3%; font-size:13px; }
		.fa.fa-calendar{ margin: -26px 12px 0px 0px; float: right; }
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#edit_incubator .error{ color:red; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-5 mx-auto">
    <form method="post" id="edit_incubator" name="edit_incubator"  enctype="multipart/form-data">
      <fieldset>
        <div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Edit Incubator</a>
        </div>

        <div style="text-align: center">
          <p style="color: green; font-family: Arial;">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </p>
        </div>
        
        <div class="form-group" hidden>
         
        </div>

        <div class="form-group" hidden>
         
        </div>

        <!--div class="form-group">
          <select name="language" class="form-control">
            <option value="">Select Language</option>
            <?php //if(count($getLanguage)):?>
              <?php //foreach($getLanguage as $language):?>
                <option value="<?php //echo $language->name;?>"><?php //echo $language->name;?></option>
              <?php //endforeach;?> 
            <?php //else:?>
            <?php //endif;?>
          </select>
          <?php echo form_error('language', '<div class="text-danger">', '</div>'); ?>
        </div-->  
		<div class="form-group">
			<label>Incubator Name</label>
			<input type="text" class="form-control" name="incubator_name" id="incubator_name" value="<?=$incubation[0]->name?>" placeholder="Incubator Name" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Upload Photo</label>
			<input type="file" class="form-control-file" name="incubator_image" id="incubator_image">
			<label><?=$incubation[0]->image?></label>
			<div class="text-danger video-error"></div>
        </div>
		</br>
		<div class="form-group">
			<label>Address</label>
			<textarea type="text" class="form-control" name="incubator_address" id="incubator_address" placeholder="Incubato Address" required><?=$incubation[0]->address?></textarea>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Country</label>
			<select class="form-control" name="incubator_country" id="incubator_country" required >
				<option value="">Select Country</option>
				<option value="<?=$incubation[0]->country?>" selected></option>	
				<?php foreach ($countries as $country) { ?>
				<option value="<?php echo $country->country_id; ?>" <?php if($incubation[0]->country == $country->country_id){?>selected <?php } ?>><?php echo strtoupper($country->country_name); ?></option>
				<?php } ?>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>State</label>
			<?php $costate = $this->Adminmaster_model->selectData('states', '*', array('state_id'=>$incubation[0]->state)); ?>
			<select class="form-control" name="incubator_state" id="incubator_state" required >
				<?php if($incubation[0]->state==""){?>
					<option value=""></option>
				<?php }else{ ?>
					<option value="<?php echo $incubation[0]->state; ?>"><?php echo $costate[0]->state_name; ?></option>
				<?php } ?>
            </select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>City</label>
			<?php $cocity = $this->Adminmaster_model->selectData('cities', '*', array('city_id'=>$incubation[0]->city)); ?>
			<select id="incubator_city" name="incubator_city" class="form-control">
			<?php if($incubation[0]->city==""){?>
				<option value=""></option>
			<?php }else{ ?>
				<option value="<?php echo $incubation[0]->city; ?>"><?php echo $cocity[0]->name; ?></option>
			<?php } ?>
            </select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Email ID</label>
			<input type="text" class="form-control" name="incubator_emailid" id="incubator_emailid" placeholder="Email ID" value="<?=$incubation[0]->email_id?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Contact No.</label>
			<input type="text" class="form-control" name="incubator_contact" id="incubator_contact" placeholder="Contact No" value="<?=$incubation[0]->contact_no?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
	
        <input style="color:#fff;" type="submit" id="upload_video_admin"  value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_incubator">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
		});
		
		$(function () {
			$(".datepicker").datepicker({ 
				autoclose: true, 
				todayHighlight: true
			}).datepicker('update', new Date());
		});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
	$(function() {
		$("#create_incubator").submit(function(e){
			e.preventDefault();
		}).validate({

			rules:{
				incubator_name:{
					required:true,
				},
				incubator_address:{
					required:true,
				},
				incubator_country:{
					required:true,
				},
				incubator_state:{
					required:true,
				},
				incubator_city:{
					required:true,
				},
				incubator_emailid:{
					required:true,
				},
				incubator_contact:{
					required:true,
				},
			},

			messages:{
				incubator_name:{
					required:"Incubator name is required",
				},
				incubator_address:{
					required:"Address is required",
				},
				incubator_country:{
					required:"Country is required",
				},
				incubator_state:{
					required:"State is required",
				},
				incubator_city:{
					required:"City is required",
				},
				incubator_emailid:{
					required:"Email id is required",
				},
				incubator_contact:{
					required:"Contact number is required",
				},
			},
			submitHandler: function(form) {
    
		        form.submit();
		       
		    }
		});
	  });
	});
	</script>
	<script>
		$(document).ready(function() {
		$("#incubator_country").on("change",function(){
            var country_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_states',
              type:"POST",
              data:{'country_id':country_id},
              success:function(res){
                $("#incubator_state").html(res);
                $("#incubator_state").parents('.input-field').addClass('label-active');
                $('#incubator_state').material_select();
              },
            });
          });
		  
		  $("#incubator_state").on("change",function(){
            var state_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_cities',
              type:"POST",
              data:{'state_id':state_id},
              success:function(res){
               $("#incubator_city").html(res);
               $("#incubator_ity").parents('.input-field').addClass('label-active');
               $('#incubator_city').material_select();
              },
            });
          });
		});
	</script>