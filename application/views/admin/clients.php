<div class="container-fluid">
  <div style="margin-top: 30px; margin-bottom: 30px; margin-left: 20px;">
      <a style="color: grey; font-size: 20px;">Details of Users Registered on Website</a>
    
      <div class="errormessage">
        <?php if($msg = $this->session->flashdata('msg')): ?>
        <?php echo $msg; ?>
        <?php endif; ?>
      </div>
  </div>
</div>

<div class="container-fluid text-center"> 
    <table class="table table-hover">
      <thead>
        <tr style="font-weight: bold">
          <th scope="col">Date Registered</th>
          <th scope="col">First Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Email</th>
          <th scope="col">Phone</th>
          <!--<th scope="col">Status</th>-->
          <th scope="col">Action</th>
        </tr>
      </thead>
      
      <tbody>
        <?php if(!empty($posts)){ foreach($posts as $post){ ?>
        <tr class="table-active">
          <td><?= date('d.m.Y',strtotime($post->created)); ?></td>
          <td><?php echo $post->first_name; ?></td>
          <td><?php echo $post->last_name; ?></td>
          <td><?php echo $post->email; ?></td>
          <td><?php echo $post->phone; ?></td>
          <!--<td></td>-->
          <td class="can">
            <?=anchor("admin/client-delete/".$post->id, "Delete", array('onclick' => "return confirm('Do you really want to delete this user?')"))?>
          </td>
        </tr>
        <?php } } else { ?>
          <tr>
            <td>No Users Found!</td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>