
	<div class="container-fluid">
      <div style="margin-top: 30px; margin-bottom: 30px; margin-left: 20px;">
          <a style="color: grey; font-size: 20px;">Uploaded Videos</a>

          <div class="col-md-4 search-panel" style="float: right">
	            <form method="post">
	                <div class="input-group mb-3">
	                    <input type="text" name="searchKeyword" class="form-control" placeholder="Search..." value="<?php echo $searchKeyword; ?>">
	                    <div class="input-group-append">
	                        <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
	                        <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
	                    </div>
	                </div>
	            </form>
        	</div>

        	<div class="languagedrop">
	        	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ગુજરાતી</button>
	        	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		            <a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video">English</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-hindi">हिन्दी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-assamese">অসমিয়া</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-bangla">বাংলা</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-gujarati">ગુજરાતી</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-marathi">मराठी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-tamil">தமிழ்</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-telugu">తెలుగు</a>
	        	</div>
	      	</div>

	      	<div class="languagedrop" style="margin-right: 15px; ">
	        	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Health</button>
	        	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	        		<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-gujarati">Show All</a>
		            <a class="dropdown-item" href="<?php echo base_url();?>admin/category-art-culture-and-artist-gujarati">Art, Culture & Artists</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-child-and-elderly-gujarati">Child & Elderly</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-education-and-development-gujarati">Education & Development</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-health-gujarati">Health</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-lifestyle-and-outdoors-gujarati">Lifestyle & Outdoors</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-people-gujarati">People</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-pets-and-environment-gujarati">Pets & Environment</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-science-and-tech-gujarati">Science & Tech</a>
	        	</div>
	      	</div>

        	<div class="errormessage">
	            <?php if($msg = $this->session->flashdata('msg')): ?>
	            <?php echo $msg; ?>
	            <?php endif; ?>
          	</div>
      </div>
    </div>

	<div class="container-fluid text-center">
	
		<table class="table table-hover">
		  <thead>
		    <tr style="font-weight: bold">
		      <th scope="col">Date Uploaded</th>
		      <th scope="col">Title</th>
		      <th scope="col">Sub-category</th>
		      <th scope="col">Uploaded By</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  
		  <tbody>
		  	<?php if(!empty($posts)){ foreach($posts as $post){ ?>
		    <tr class="table-active">
		    	<td><?= date('d.m.Y',strtotime($post['date_created'])); ?></td>
		    	<td><?php echo $post['title']; ?></td>
		    	<td><?php echo $post['subcategory']; ?>, <?php echo $post['subcategory2']; ?>, <?php echo $post['subcategory3']; ?>, <?php echo $post['subcategory4']; ?>, <?php echo $post['subcategory5']; ?></td>
		    	<td><?php echo $post['uploaded_by']; ?></td>
		    	<td>
		      		<?php echo anchor("admin/video-details/{$post['id']}", 'View Details'); ?>
		      		<?=anchor("admin/videos-delete/".$post['id'], "Delete", array('onclick' => "return confirm('Do you want delete this record')"))?>
		      	</td>
		    </tr>
		    <?php } } else {?>
		    	<tr>
		    		<td>No Records Found!</td>
		    	</tr>
		    <?php } ?>
		  </tbody>
		</table>
		<div class="pagination" style="margin-left: 600px; margin-bottom: -20px;">
            <?php echo $this->pagination->create_links(); ?>
        </div>
	</div>
	<br>