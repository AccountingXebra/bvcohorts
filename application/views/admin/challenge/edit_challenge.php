  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <style>
	.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
		.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; color:#bc343a; }
		.datepicker .datelabel{ padding:6% 3%; font-size:13px; }
		.fa.fa-calendar{ margin: -26px 12px 0px 0px; float: right; }
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#challange_form .error{ color:red; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-5 mx-auto">
    <form method="post" id="edit_challange_form" name="edit_challange_form"  enctype="multipart/form-data">
      <fieldset>
        <div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Edit Challenge</a>
        </div>

        <div style="text-align: center">
          <p style="color: green; font-family: Arial;">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </p>
        </div>
        
        <div class="form-group" hidden>
         
        </div>

        <div class="form-group" hidden>
         
        </div>

        <!--div class="form-group">
          <select name="language" class="form-control">
            <option value="">Select Language</option>
            <?php //if(count($getLanguage)):?>
              <?php //foreach($getLanguage as $language):?>
                <option value="<?php //echo $language->name;?>"><?php //echo $language->name;?></option>
              <?php //endforeach;?> 
            <?php //else:?>
            <?php //endif;?>
          </select>
          <?php //echo form_error('language', '<div class="text-danger">', '</div>'); ?>
        </div-->  
		<div class="form-group">
			<label>Challenge Id</label>
			<input type="text" class="form-control" name="challange_code" id="challange_code" placeholder="Challenge ID" value="<?=$challanges[0]->challange_code?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Challenge Date</label>
			<input type="text" class="form-control datepicker" name="challange_date" id="challange_date" placeholder="Challenge Date" value="<?=date('d-m-Y',strtotime($challanges[0]->challange_date))?>" required>
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Challenge Name</label>
			<input type="text" class="form-control" name="challange_name" id="challange_name" placeholder="Challenge Name" value="<?=$challanges[0]->challange_name?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Upload Image</label>
			<input type="file" class="form-control-file" name="challange_image" id="challange_image" >
			<label style="margin:12px 0 0 0;"><?=$challanges[0]->challange_image?></label>
			<div class="text-danger video-error"></div>
        </div>
		</br>
		<div class="form-group">
			<label>Challenge Description</label>
			<textarea type="text" class="form-control" name="challange_desc" id="challange_desc" placeholder="Challenge Description" required><?=$challanges[0]->challange_desc?></textarea>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
        <div class="form-group">
			<label>Nature Of Business</label>
			<select class="form-control" name="challange_nature" id="challange_nature" required >
				<option value="">Select Nature</option>
					<option value="<?=$challanges[0]->challange_nature?>" selected><?=$challanges[0]->challange_nature?></option>
										<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
										<option value="Advertising">ADVERTISING</option>
										<option value="Animation Studio">ANIMATION STUDIO</option>
										<option value="Architecture">ARCHITECTURE</option>
										<option value="Arts & Crafts">ARTS & CRAFTS</option>
										<option value="Audit & Tax">AUDIT & TAX</option>
										<option value="Brand Consulting">BRAND CONSULTING</option>
										<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
										<option value="Consultant">CONSULTANT</option>
										<option value="Content Studio">CONTENT STUDIO</option>
										<option value="Cyber Security">CYBER SECURITY</option>
										<option value="Data Analytics">DATA ANALYTICS</option>
										<option value="Digital Influencer">DIGITAL INFLUENCER</option>
										<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
										<option value="Direct Marketing">DIRECT MARKETING</option>
										<option value="Entertainment">ENTERTAINMENT</option>
										<option value="Event Planning">EVENT PLANNING</option>
										<option value="Florist">FLORIST</option>
										<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
										<option value="Financial and Banking">FINANCIAL & BANKING</option>
										<option value="Gaming Studio">GAMING STUDIO</option>
										<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
										<option value="Hardware Servicing">HARDWARE SERVICING</option>
										<option value="Industry Bodies">INDUSTRY BODIES</option>
										<option value="Insurance">INSURANCE</option>
										<option value="Interior Designing ">INTERIOR DESIGNING</option>
										<option value="Legal Firm">LEGAL FIRM</option>
										<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
										<option value="Mobile Services">MOBILE SERVICES</option>
										<option value="Music">MUSIC</option>
										<option value="Non-Profit">NON-PROFIT</option>
										<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
										<option value="Photography">PHOTOGRAPHY</option>
										<option value="Printing">PRINTING</option>
										<option value="Production Studio">PRODUCTION STUDIO</option>
										<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
										<option value="Publishing">PUBLISHING</option>
										<option value="Real Estate">REAL ESTATE</option>
										<option value="Recording Studio">RECORDING STUDIO</option>
										<option value="Research">RESEARCH</option>
										<option value="Sales Promotion">SALES PROMOTION</option>
										<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
										<option value="Stock & Shares">STOCK & SHARES</option>
										<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
										<option value="Tours & Travel">TOURS & TRAVELS</option>
										<option value="Training & Coaching">TRAINING & COACHING</option>
										<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
										<option value="Therapists">THERAPISTS</option>
										<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
										<option value="Web Development">WEB DEVELOPMENT</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Start Date</label>
			<input type="text" class="form-control datepicker" name="last_day_entry" id="last_day_entry" value="<?=date('d-m-Y',strtotime($challanges[0]->last_day_entry))?>" placeholder="State Date" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Location</label>
			<input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?=$challanges[0]->location?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Website Url</label>
			<input type="text" class="form-control" name="url" id="url" placeholder="Website URL" value="<?=$challanges[0]->url?>" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
	
        <input style="color:#fff;" type="submit" id="upload_video_admin"  value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_challenge">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
		});

		$(document).on("change","#challange_image",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["image/jpeg", "image/jpg", "image/png"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Image format is incorrect.'
					});
					$("#challange_image").val('');
				}
				/*else if(filesize_mb > 100) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Upload video size should not be more than 100mb'
					});
					$("#video_up").val('');
				}*/	
			}
		});
		
		$(function () {
			$(".datepicker").datepicker({ 
				autoclose: true, 
				todayHighlight: true,
				format:"dd/mm/yyyy",
			}).datepicker('update', new Date());
		});
	</script>