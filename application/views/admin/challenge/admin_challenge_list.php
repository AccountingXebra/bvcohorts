<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<style>
	#content{ margin: 2% 8% 2% 8%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#challenge_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#challenge_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#challenge_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#challenge_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(4){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(5){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(6){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(7){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(8){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(9){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(10){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(11){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
	.video_up{ width:85% !important; }
	.no_sub{ width:88% !important; }
	.loader {
		position: fixed;
		left: 0px;
		top: -5%;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
		opacity: 0.8;
	}
</style>
<div class="loader"></div>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-md-9"></div>
			<div class="col-md-3">
				<a style="width: 70%; margin: 0px 0px 15px 23%; font-size: 13px; line-height: 34px; background-color: #bc343a; color: #fff;" href="<?php echo base_url();?>admin/add_challenge" class="btn" name="" id="" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onmouseover="this.style.border='1px solid #bc343a',this.style.color='#fff'">ADD CHALLENGE</a>
			</div>	
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="challenge_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr style="border:1px solid red;">
								<th style="text-align:center; width:20%;">Challenge </br>No & Date</th>
								<th style="text-align:center; width:20%;">Challenge Name</th>
								<th style="text-align:center; width:20%;">Challenge Description</th>
								<th style="text-align:center; width:10%;">Nature Of Business</th>
								<th style="text-align:center; width:10%;">Start Date</th>
								<th style="text-align:center; width:10%;">Location</th>
								<th style="text-align:center; width:10%;">Action</th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
	<div id="delete_challenge" class="modal" style="width:35% !important;">
		<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
		<div class="modal-content">
			<div class="modal-header" style="text-align:center;">
				<h4 style="width:93% !important;"> Delete Challenge</h4>        
				<input type="hidden" id="remove_challenge_id" name="remove_salary_id" value="" />
				<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
			</div>
		</div>
		<div class="modal-body" style="width:100% !important; margin-left:0px !important;">
			<p style="color:#595959; font-size:15px; text-align:center;">Are you sure you want to delete this challenge?</p>
		</div>
		<div class="modal-content">
			<div class="modal-footer">
				<div class="row">
					<div class="col l3 s12 m12"></div>
					<div class="col l9 s12 m12 cancel-deactiv">
						<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
						<button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_challenge_btn">DELETE</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
$(document).ready(function(){
	$(".loader").fadeOut();
	$('.bvfoot').hide();
});	
</script>