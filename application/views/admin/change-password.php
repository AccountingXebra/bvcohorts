<style type="text/css">
    
    .contact-header {
      text-align: center;
      font-family: Arial;
      font-size: 17px;
      padding:40px 0;
      color: grey;
      margin-top: -5px;
      margin-bottom: -35px;
    }

    .input-container {
      display: -ms-flexbox;
      display: flex;
      width: 150%;
      margin-bottom: 20px;
      font-family: Arial;
    }

    .icon {
      padding: 15px;
      background: white;
      color: #ccc;
      min-width: 20px;
      text-align: center;
      border: 1.5px solid #ccc;
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }

    .input-field {
      width: 50%;
      padding: 10px;
      outline: none;
      border: 1.5px solid #ccc;
      border-left: 0px;
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    .sub-btn:hover {
      opacity: 1;
    }

    .col-75 {
      float: left;
      width: 100%;
      margin-top: 1px;
      margin-left: -5px;
    }

    .fa-user{
    font-size: 20px;
    }

    #contact-btn{
    border:1px solid #bf4b3d !important;
    background-color: #bf4b3d;
    color:#fff;
    }

    .sub-btn{
    text-align: center;
    margin-top: 5px;
    background-color: #bf4b3d;
    color: white;
    padding: 10px 15px;
    border: none;
    border-radius: 10px;
    cursor: pointer;
    width: 30%;
    opacity: 0.9;
    font-family: Arial;
    margin-left: 185px;
    }
    .errormsg1 {
      color: green;
      font-family: Arial;
    }
    .errormsg2 {
      color: #bf4b3d;
      font-family: Arial;
    }
</style>

<div class="container-fluid">
    
  <div style="margin-top: 40px; margin-bottom: -5px; text-align: center;">
    <a style="color: grey; font-size: 20px; margin-left: -40px;">Change Password</a>

    <!-- Status message -->
    <div style="margin-left: -35px;">
      <?php  
        if(!empty($success_msg)){ 
            echo '<p class="errormsg1 status-msg success">'.$success_msg.'</p>'; 
        }elseif(!empty($error_msg)){ 
            echo '<p class="errormsg1 status-msg error">'.$error_msg.'</p>'; 
        } 
      ?>
    </div>
  </div>
	
  <!-- Registration form -->
  <form action="" method="post" style="max-width:500px; margin:auto; padding:3% 1% 3% 6%;">

    <div class="input-container">
      <i class="fa fa-lock icon"></i>
      <input class="input-field" type="password" name="current_password" placeholder="Enter current password">
      <div class="col-sm-6">
        <?php echo form_error('current_password','<p class="errormsg2 help-block">','</p>'); ?>
      </div>
    </div>

    <div class="input-container">
      <i class="fa fa-lock icon"></i>
      <input class="input-field" type="password" name="new_password" placeholder="Enter New Password">
      <div class="col-sm-6">
        <?php echo form_error('new_password','<p class="errormsg2 help-block">','</p>'); ?>
      </div>
    </div>

    <div class="input-container">
      <i class="fa fa-lock icon"></i>
      <input class="input-field" type="password" name="confirm_password" placeholder="Confirm Password">
      <div class="col-sm-6">
        <?php echo form_error('confirm_password','<p class="errormsg2 help-block">','</p>'); ?>
      </div>
    </div>

    <div class="sub-btn">
      <input type="submit" name="Submit" id="contact-btn" value="Update">
    </div>
  </form>
</div>

<br><br><br><br><br><br><br><br><br><br><br>