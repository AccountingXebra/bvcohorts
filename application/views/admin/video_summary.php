<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script> var base_url = '<?php echo base_url(); ?>';</script>
<style>
	#content{ margin: 0% 8% 2% 8%; padding:2% 0; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
	.video_up{ width:85% !important; }
	.no_sub{ width:88% !important; }
	
	.third_graph{ background-color:#fff; padding:2% 1%; margin-top:3%; border-radius: 10px; height:550px; }
	#engage_graph{ padding:0 8%; }
	
	#start_date, #end_date{ float:right; width:30%; }
	.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
	.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; }
	.datelabel{ padding:4% 3%; font-size:13px; }
	.summary-list{ background-color:#f6f5f3; }
	.datepicker-dropdown{ left: 894.219px; width: 18%; padding: 0px 2px; }
	.status-drop.dropdown-toggle::after{ margin-left: 50% !important; font-size: 17px; }
	
	.summary-list .nav-link{ color:#bf4b3d !important; font-size: 12px; font-weight: 500;}
	.summary-list .dropdown-toggle::after{ border-top: 0.5em solid #bf4b3d; }
	.summary-list .dropdown-menu.show{ background-color: #bfdee0; min-width: 12rem; margin-left: 5%; }
	.summary-list .dropdown-item:hover{ background-color: #bfdee0; color:#2fa5a7; }
	.summary-list  .dropdown-item:active{ background-color: #bfdee0; color:#2fa5a7; }
	.summary-list a.dropdown-item { height: auto; padding: 0% 5% !important; color: #010000; font-size: 13px !important; font-weight: 600; text-align:left;}
	.select2-container--default .select2-selection--single .select2-selection__rendered{ border:1px solid #e5e5e5 !important; border-radius:5px; height: 36px; color: #bf4b3d; font-size: 12px; line-height: 36px; box-shadow: 0 0.5rem 3rem rgb(0 0 0 / 8%) !important; }
	.select2-container--default .select2-selection--single .select2-selection__arrow b{ top:60% !important; color:#bf4b3d; border-color: #bf4b3d transparent transparent transparent !important; }
	.select2-container{ margin-top:-4%; }
	.select2-container--open .select2-dropdown--below{ width:160px !important; }
</style>
<div class="wrapper summary-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row">
			<div class="col-lg-12 third_graph">
					<div class="row" style="padding:0 0 0 0">
						<div class="col-lg-12">
							<div class="col-lg-12">
								<!--h1 class="top-2"><span class="graph-title">Engagement</span></h1--> 
							</div>
						</div>
					</div>	
					<div class="row" style="padding:0 8% 4% 0;">
						<div class="col-lg-4">
							<div class="input-field" style="margin:0 15px 20px 14%;">
								<label class="datelabel">Sort By</label>
								<select id="summary_options" name="summary_options">
									<option value="videos">No. of Videos Uploaded</option>
									<option value="subscription"> Subscriptions</option>
									<option value="signup">Signup</option>
									<option value="interaction">Interaction</option>
									<option value="views">Views</option>
									<option value="visitors">Visitors</option>
									<option value="login">Login Time</option>
								</select>
							</div>
						</div>
						<div class="col-lg-8">
							<div id="end_date" class="datepicker input-group date" data-date-format="dd/mm/yyyy">
								<label class="datelabel">End Date</label>
								<input id="end_date_eng" name="end_date_eng" class="form-control" type="text" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
							</div>
							<div id="start_date" class="datepicker input-group date" data-date-format="dd/mm/yyyy">
								<label class="datelabel">Start Date</label>
								<input id="start_date_eng" name="start_date_eng" class="form-control" type="text" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
							</div>
						</div>
					</div>
					
					<div class="row" style="padding:0 0 0 0">
						<div class="col-lg-12"> <p style="border: 2px solid #bd343c; padding: 0 15px; border-radius: 10px; width: 11.5%; font-size: 15px; margin-left: 35px; color: #bd343c; text-align:center;"><span id="graph_yaxis">Videos</span></p> </div>
						<div class="col-lg-12" style="width:80%;">
							<div id="engage_graph"></div>
							<p style=" border: 2px solid #bd343c; border-radius: 10px; width: 7%; font-size: 15px; margin:-6% 5% 0 0; color: #bd343c; float:right; text-align:center;">Months</p>
						</div>
					</div>
					
					</div>				
		</div>
	</div>
    </div>
</div>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#summary_options").select2({ /*placeholder: "Select a programming language", //allowClear: true*/ });
		
		google.charts.load('current', {packages: ['corechart', 'line']});
		google.charts.setOnLoadCallback(drawengagement);


		$("#start_date_eng").change(function(){
			drawengagement();
		})

		$("#end_date_eng").change(function(){
			drawengagement();
		})

		$("#summary_options").change(function(){
			if($(this).val() == "interaction"){
				$("#graph_yaxis").html('Interaction');
			}
			if($(this).val() == "visitors"){
				$("#graph_yaxis").html('Visitors');
				//input.push( ['Month', 'Visitors']);
			}
			drawengagement();
		})

		function drawengagement() {
           
            var options=$("#summary_options").val();

            var start_date=$("#start_date_eng").val();
      var end_date=$("#end_date_eng").val();
      $.ajax({
              type: "POST",
              dataType: 'json',

              url: base_url+'admin/get_videos_summary',
              data:{
              	     "option":options,
                    "start_date":start_date,
                    "end_date":end_date,
                   


              },
              cache: false,
                      success:function(result)
              {

			console.log(result);
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'X');
			data.addColumn('number', ' ');
			data.addColumn({type:'boolean',role:'certainty'});
            var input=[];
			if(result.option=="videos"){
				$("#graph_yaxis").html('Videos');
				input.push( ['Month', 'Videos']);
			}

			if(result.option=="subscription"){
				$("#graph_yaxis").html('Subscriptions');
				input.push( ['Month', 'Subscriptions']);
			}

			if(result.option=="signup"){
				$("#graph_yaxis").html('Signups');
				input.push( ['Month', 'Signups']);
			}

			if(result.option=="interaction"){
				$("#graph_yaxis").html('Interaction');
				input.push( ['Month', 'Interaction(Likes-'+result['summary'][0].like+',comments-'+result['summary'][0].comment+')']);
			}

			if(result.option=="visitors"){
				$("#graph_yaxis").html('Visitors');
				input.push( ['Month', 'Visitors']);
			}

			if(result.option=="views"){
				$("#graph_yaxis").html('Views');
				input.push( ['Month', 'Views']);
			}

			if(result.option=="login"){
				$("#graph_yaxis").html('Login Time');
				input.push( ['Month', 'Login Time']);
			}
              
              if(result['summary'].length > 0){
              	 for(i=0;i<result['summary'].length;i++){
              input.push([result['summary'][i].createdDate,result['summary'][i].videonum]);
			 }
			}else{
                 $("#engage_graph").html("No data");
                 return false;
			}
			
             
			var data = google.visualization.arrayToDataTable(
				
				input
			);


			var options = {
				backgroundColor: 'transparent',	
				//series: {0: {type: 'line',lineDashStyle: [2, 2]}},
				colors: ['#E94D20', '#ECA403', '#63A74A', '#15A0C8', '#4151A3', '#703593', '#981B48'],
				hAxis: {title: ' ',
					titleTextStyle: {color: "#000", fontName: "roboto", fontSize: 12, bold: true, italic: false,},
					//slantedText:true,
					//slantedTextAngle:90,
					minValue: 0,
					gridlines: {color: '#f0f1f3', count: 11},
					textStyle : { fontSize: 12 },
				},
				vAxis: {
					title: ' ',
					titleTextStyle: {color: "#000", fontName: "roboto", fontSize: 14, bold: true, italic: false,},
					minValue: 0,
					gridlines:{ count:3, },
					textStyle : { fontSize: 12 },
				},
				legend:'none',
				height: 400,
				width:'90%',
				//viewWindow:{min:0,},
				//colors: ['#69d9da','#fffaf4'],
				colors: ['#e1475d','#3c9bac'],
				'chartArea': {'width': '90%', 'height': '80%'},
				curveType: 'function',
				pointSize: 7,
				pointShape: 'circle',
				//dataOpacity: 0.5,
			};

			  var chart = new google.visualization.LineChart(document.getElementById('engage_graph'));
			  chart.draw(data, options);
			}


		})


		}


	});
	$(function () {
		$(".datepicker").datepicker({ 
			autoclose: true, 
			todayHighlight: true
		}).datepicker('update', new Date());
	});
</script>	
<script>
$(document).ready(function(){
	$('.bvfoot').hide();
});	
</script>