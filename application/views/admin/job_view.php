
	<div class="container-fluid" style="line-height: 2">
		<div style="margin-top: 30px; margin-bottom: 23px; margin-left: 20px;">
            <a style="color: grey; font-size: 20px;">Job Application Details</a>
        </div>
        <div style="margin-left: 20px;">
			<b>Full Name:</b> <?php echo $post->full_name; ?>
			<p><b>Contact Number:</b> <?php echo $post->contact_number; ?></p>
			<p><b>Email:</b> <?php echo $post->email; ?></p>
			<p><b>Blog Link:</b> <a href="<?php echo $post->blog; ?>" target="_blank"><?php echo $post->blog; ?></a></p>
			<p><b>Linkedin Link:</b> <a href="<?php echo $post->linkedin; ?>" target="_blank"><?php echo $post->linkedin; ?></a></p>
			<p><b>Facebook Link:</b> <a href="<?php echo $post->facebook; ?>" target="_blank"><?php echo $post->facebook; ?></a></p>
			<p><b>Twitter Link:</b> <a href="<?php echo $post->twitter; ?>" target="_blank"><?php echo $post->twitter; ?></a></p>
			<p><b>Function of work:</b> <?php echo $post->work_function; ?></p>
			<p><b>Preferred Location:</b> <?php echo $post->location; ?></p>
			<p><b>Experience:</b> <?php echo $post->experience; ?> yrs</p>
			<p><b>Description about self:</b> <?php echo $post->about_self_on_bv; ?></p>
			<p><b>How do you know Bharat Vaani:</b> <?php echo $post->knowbv; ?></p>
			<p><b>Reason for joining:</b> <?php echo $post->reason_to_join; ?></p>
			<p><b>Date submitted:</b> <?php echo $post->date_created; ?></p>
			<p><b>Resume:</b> </p>
			<?php if($post):?>
				<iframe src="<?php echo $post->resume; ?>" width="825" height="980"></iframe>
			<?php else: ?>
				<p>No Resume Found!</p>
			<?php endif; ?>
		</div>
	</div>
