	<div class="container-fluid">
      <div style="margin-top: 30px; margin-bottom: 20px; margin-left: 20px;">

        	<a style="color: grey; font-size: 20px;">Editor's Pick</a>

          	<div class="col-md-4 search-panel" style="float: right">
	            <form method="post">
	                <div class="input-group mb-3">
	                    <input type="text" name="searchKeyword" class="form-control" placeholder="Search..." value="<?php if(isset($searchKeyword)){echo $searchKeyword;} ?>">
	                    <div class="input-group-append">
	                        <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
	                        <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
	                    </div>
	                </div>
	            </form>
        	</div>

        	<div class="languagedrop">
	        	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">தமிழ்</button>
	        	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		            <a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick">English</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-hindi">हिन्दी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-assamese">অসমিয়া</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-bangla">বাংলা</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-gujarati">ગુજરાતી</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-marathi">मराठी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-tamil">தமிழ்</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/editors-pick-telugu">తెలుగు</a>
	        	</div>
	      	</div>

        	<div class="errormessage">
	        	<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</div>
         	<!-- <?php echo anchor('user/editors_create', 'Add New Video', ['class'=>'btn btn-primary']); ?> -->
      </div>
    </div>

	<div class="container-fluid text-center">	
		<table class="table table-hover">
		  <thead>
		    <tr style="font-weight: bold">
		    	<th scope="col">Date Uploaded</th>
		      	<th scope="col">Title</th>
		      	<th scope="col">Language</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  
		  <tbody>
		  	<?php if(!empty($posts)){ foreach($posts as $post){ ?>
		    <tr class="table-active">
		      <td><?= date('d.m.Y',strtotime($post['date_created'])); ?></td>
		      <td><?php echo $post['title']; ?></td>
		      <td><?php echo $post['language']; ?></td>
		      <td>
		      	<?php echo anchor("admin/editorspick-details/{$post['id']}", 'View'); ?> |
		      	<?php echo anchor("admin/editorspick-update/{$post['id']}", 'Update'); ?> |
		      	<?=anchor("admin/editorspick-delete/".$post['id'], "Delete", array('onclick' => "return confirm('Do you want delete this record')"))?>
		      </td>
		    </tr>
		    <?php } } else { ?>
		    	<tr>
		    		<td>No Records Found!</td>
		    	</tr>
		    <?php } ?>
		  </tbody>
		</table>
		<div class="pagination" style="margin-left: 600px; margin-bottom: -20px;">
            <?php echo $this->pagination->create_links(); ?>
        </div>
	</div>
	<br><br><br><br><br><br><br><br><br><br><br>
