
	<div class="container-fluid">
      <div style="margin-top: 30px; margin-bottom: 30px; margin-left: 20px;">
          <a style="color: grey; font-size: 20px;">Uploaded Videos</a>

          <div class="col-md-4 search-panel" style="float: right">
	            <form method="post">
	                <div class="input-group mb-3">
	                    <input type="text" name="searchKeyword" class="form-control" placeholder="Search..." value="<?php if(isset($searchKeyword)){echo $searchKeyword;} ?>">
	                    <div class="input-group-append">
	                        <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
	                        <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
	                    </div>
	                </div>
	            </form>
        	</div>

        	<div class="languagedrop">
	        	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">অসমিয়া</button>
	        	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		            <a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video">English</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-hindi">हिन्दी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-assamese">অসমিয়া</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-bangla">বাংলা</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-gujarati">ગુજરાતી</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-marathi">मराठी</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-tamil">தமிழ்</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-telugu">తెలుగు</a>
	        	</div>
	      	</div>

	      	<div class="languagedrop" style="margin-right: 15px; ">
	        	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</button>
	        	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	        		<a class="dropdown-item" href="<?php echo base_url();?>admin/view-uploaded-video-assamese">Show All</a>
		            <a class="dropdown-item" href="<?php echo base_url();?>admin/category-art-culture-and-artist-assamese">Art, Culture & Artists</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-child-and-elderly-assamese">Child & Elderly</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-education-and-development-assamese">Education & Development</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-health-assamese">Health</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-lifestyle-and-outdoors-assamese">Lifestyle & Outdoors</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-people-assamese">People</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-pets-and-environment-assamese">Pets & Environment</a>
					<a class="dropdown-item" href="<?php echo base_url();?>admin/category-science-and-tech-assamese">Science & Tech</a>
	        	</div>
	      	</div>

        	<div class="errormessage">
	            <?php if($msg = $this->session->flashdata('msg')): ?>
	            <?php echo $msg; ?>
	            <?php endif; ?>
          	</div>
      </div>
    </div>

	<div class="container-fluid text-center">
	
		<table class="table table-hover">
		  <thead>
		    <tr style="font-weight: bold">
		      <th scope="col">Date Uploaded</th>
		      <th scope="col">Title</th>
		      <th scope="col">Sub-category</th>
		      <th scope="col">Uploaded By</th>
		      <th scope="col">Select</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  
		  <tbody>
		  	<?php if(!empty($posts)){ foreach($posts as $post){ ?>
		    <tr class="table-active">
		    	<td><?= date('d.m.Y',strtotime($post['date_created'])); ?></td>
		    	<td width="20%"><?php echo $post['title']; ?></td>
		    	<td width="30%"><?php echo $post['subcategory']; ?>, <?php echo $post['subcategory2']; ?>, <?php echo $post['subcategory3']; ?>, <?php echo $post['subcategory4']; ?>, <?php echo $post['subcategory5']; ?></td>
		    	<td><?php echo $post['uploaded_by']; ?></td>
		    	<td>
		    		<form method="post" id="myCheckbox">
			    		<p style="margin-bottom: 20px;">
			    			Editor's Pick &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
			    			<input type="checkbox" id="edt<?=$post['id']?>" value="edt" name="edt<?=$post['id']?>" onclick="javascript:change_edt_status('<?=$post['id']?>','edt');" <?php if($post['show_edt']==1){echo "checked";}?>>
			    		</p>

			    		<p>
			    			Inspiring People &nbsp&nbsp 
			    			<input type="checkbox" id="insp<?=$post['id']?>" name="insp<?=$post['id']?>" value="insp" onclick="javascript:change_edt_status('<?=$post['id']?>','insp');" <?php if($post['show_insp']==1){echo "checked";}?>>
			    		</p>
		    		</form>
		    	</td> 		    	
		    	<td>
		      		<?php echo anchor("admin/video-details/{$post['id']}", 'View Details');?>
		      		<br>
		      		<?php echo anchor("admin/edit-details/{$post['id']}", 'Edit Details'); ?>
		      		<br>
		      		<?=anchor("admin/videos-delete/".$post['id'], "Delete", array('onclick' => "return confirm('Do you want to delete this record?')"))?>
		      	</td>
		    </tr>

		   <script>
		    function change_edt_status(id,type){
		    	if(type=='edt'){
		    		var tpe= "Editor's Picks";
		    	} else {
		    		var tpe="Inspiring People";
		    	}
                       
                        if ($("#"+type+id).is( 
                      ":checked")) {
        // if checked ...
        $.ajax({
        	type:"post",
        	dataType:'json',
        	data:{'vid_id':id,'type':type,'status':1},
        	url: '<?php echo base_url();?>admin/change_status_assamese',
        	cache:false,
        	success:function(data){
        		console.log(data);
              if(data==true){
              	alert('Video successfully selected for '+tpe+'.');
              }else{
              	alert(data);
              }
        	}

        });
        
    } else {
         $.ajax({
        	type:"post",
        	dataType:'json',
        	data:{'vid_id':id,'type':type,'status':0},
        	url: '<?php echo base_url();?>admin/change_status_assamese',
        	cache:false,
        	success:function(data){
        		console.log(data);
              if(data==true){
              	alert('Video unselected successfully from '+tpe+'. Please select another video to replace the previous unselected video.');
              }else{
              	alert(data);
              }
        	}

        });
    }
		    }
		    	
		    </script>

		    <?php } } else {?>
		    	<tr>
		    		<td>No Records Found!</td>
		    	</tr>
		    <?php } ?>
		  </tbody>
		</table>
		<div class="pagination" style="margin-left: 600px; margin-bottom: -20px;">
            <?php echo $this->pagination->create_links(); ?>
        </div>
	</div>
	<br>