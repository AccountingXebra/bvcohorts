<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
	#content{ margin: 2% 2% 2% 2%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#signup_details_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#signup_details_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#signup_details_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#signup_details_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(4){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(5){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(6){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(7){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(8){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(9){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(10){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(11){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.city-drop.dropdown-toggle::after{ margin-left: 62% !important; font-size: 17px; }
	.status-drop.dropdown-toggle::after{ margin-left: 50% !important; font-size: 17px; }
	
	.admin-list .nav-link{ color:#bf4b3d !important; font-size: 12px; font-weight: 500;}
	.admin-list .dropdown-toggle::after{ border-top: 0.5em solid #bf4b3d; }
	.admin-list .dropdown-menu.show{ background-color: #bfdee0; min-width: 8.7rem; margin-left: 0%; }
	.admin-list .dropdown-item:hover{ background-color: #bfdee0; color:#2fa5a7; }
	.admin-list  .dropdown-item:active{ background-color: #bfdee0; color:#2fa5a7; }
	.admin-list a.dropdown-item { height: auto; padding: 0% 5% !important; color: #010000; font-size: 13px !important; font-weight: 600; text-align:left;}
	.select2-container--default .select2-selection--single .select2-selection__rendered{ border:1px solid #e5e5e5 !important; border-radius:5px; height: 36px; color: #bf4b3d; font-size: 12px; line-height: 36px; box-shadow: 0 0.5rem 3rem rgb(0 0 0 / 8%) !important; }
	.select2-container--default .select2-selection--single .select2-selection__arrow b{ top:60% !important; color:#bf4b3d; border-color: #bf4b3d transparent transparent transparent !important; }
	.select2-container{ margin-top:-0.9%; }
	.select2-container--open .select2-dropdown--below{ width:160px !important; }
</style>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-lg-6">		
			<div class="input-field" style="margin:0 0 20px 0px;">
				<a class="nav-link shadow dropdown-toggle" href="#" id="bulk_action" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BULK ACTIONS</a>
				<div class="dropdown-menu" aria-labelledby="bulk_action">
					<a class="dropdown-item print_signup" id="signup_details_print" data-multi_signup="0">EXPORT</a>
				</div>
			</div>	
			</div>
			<div class="col-lg-6">
			<div class="input-field" style="margin:0 15px 20px 15px; float:right;">
				<label style="font-size:12px;">STATUS:</label>
				<select id="status" name="status">
					<!-- <option value="">STATUS</option> -->
					<option value="Active">ACTIVE</option>
					<option value="Inactive">INACTIVE</option>
				</select>
			</div>
			<div class="input-field" style="margin:0 0 20px 15px; float:right;" hidden>
				<select id="city" name="city">
					<option value="">SELECT CITY</option>
					<?php foreach($cities as $city){
                       $cityname=$this->Admin_model->selectData("cities","*",array('city_id'=>$city->user_city));
						?>
							<option value="<?=$city->user_city?>"><?=$cityname[0]->name?></option>
					<?php }?>
				</select>
			</div>	
			</div>			
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="signup_details_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr style="border:1px solid red;">
								<th style="width:4%; vertical-align:middle;s">
									<input type="checkbox" id="sign_bulk" name="sign_bulk" class="filled-in purple" />
									<label for="sign_bulk"></label>
								</th>
								<th style="text-align:center; width:10%;">Registered Id</br>& Date</th>
								<th style="text-align:center; width:10%;">First & Last</br>Name</th>
								<!--<th style="text-align:center; width:10%;">Username</th>-->
								<th style="text-align:center; width:10%;">Email Address</th>
								<th style="text-align:center; width:10%;">Profile Link</th>
								<th style="text-align:center; width:8%;">Country</th>
								<th style="text-align:center; width:8%;">City</th>
								<th style="text-align:center; width:8%;">Days since</br>Registeration</th>
								<th style="text-align:center; width:5%;">Status</br>(A/NA)</th>
								<th style="text-align:center; width:17%;">Action</th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>	
<script>
$(document).ready(function(){
	$(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
		$("#status option[value='']").text(' ');
	});
	$('.bvfoot').hide();
	$("#status").select2({ /*placeholder: "Select a programming language", //allowClear: true*/ });
	$("#city").select2({  });
});	
</script>