<center>
  	<div class="container-fluid" style="line-height: 2">

	  	<?php echo form_open("admin/editorspick_change/{$post->id}", ['class'=>'form-horizontal']); ?>
		
	  	<fieldset>
	    	<div style="margin-top: 25px; margin-bottom: 23px; margin-left: 20px;">
            	<a style="color: grey; font-size: 20px;">Update Details</a>
        	</div>
	    	<div class="form-group">
	      		<label for="InputTitle" class="col-md-2 control-label" style="margin-right: 695px; font-size: 20px; color: grey;">Title</label>
	      		<div class="col-md-7">
	      			<?php echo form_input(['name'=>'title', 'placeholder'=>'Enter Title', 'class'=>'form-control', 'value'=>set_value('title', $post->title)]); ?>
	    		</div>
	    		<div col-md-5>
	    			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
	    	
	    	<div class="form-group">
	      		<label for="descriptionTextarea" class="col-md-2 control-label" style="margin-right: 625px; font-size: 20px; color: grey;">Description</label>
	      		<div class="col-md-7">
	      			<?php echo form_textarea(['name'=>'description', 'placeholder'=>'Enter Description', 'class'=>'form-control', 'value'=>set_value('description', $post->description)]); ?>
	    		</div>
	    		<div col-md-5>
	    			<?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
	    		</div>
	    	</div>
	    	
    		<div class="form-group">
    			<div class="col-md-10 col-md-offset-2">
    				<div style="margin-left: 565px;">
	    				<?php echo form_submit(['name'=>'submit', 'value'=>'Update', 'class'=>'btn btn-success']); ?>
	    				<?php echo anchor('admin/editors_pick', 'Cancel', ['class'=>'btn btn-danger']); ?>
    				</div>
    			</div>
    		</div>
	  	</fieldset>
	  	<?php echo form_close(); ?>
  	</div>

  	<br><br><br><br><br>

</center>