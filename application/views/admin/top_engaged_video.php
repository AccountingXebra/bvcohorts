<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<style>
	#content{ margin: 2% 2% 2% 2%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#top_engagedvideo_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#top_engagedvideo_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#top_engagedvideo_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#top_engagedvideo_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(4){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(5){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(6){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(7){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(8){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(9){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(10){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(11){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
</style>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-lg-6"></div>
			<div class="col-lg-6">
			<div class="input-field" style="margin:0 15px 20px 15px; float:right;">
				<a class="cat-drop nav-link shadow dropdown-toggle" href="#" id="category" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CATEGORY</a>
				<div class="dropdown-menu" aria-labelledby="status">
					<a class="dropdown-item">CAT1</a>
				</div>
			</div>
			</div>			
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="top_engagedvideo_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr style="border:1px solid red;">
								<!--th style="width:5%; vertical-align:middle">
									<input type="checkbox" id="uservideo_bulk" name="uservideo_bulk" class="filled-in purple" />
									<label for="uservideo_bulk"></label>
								</th-->
								<th style="text-align:center; width:10%;">Upload</br>Date</th>
								<th style="text-align:center; width:20%;">Video Title</th>
								<th style="text-align:center; width:20%;">Video Description</th>
								<th style="text-align:center; width:10%;">Category</th>
								<th style="text-align:center; width:10%;">Total</br>Engagement</th>
								<th style="text-align:center; width:10%;">Likes</th>
								<th style="text-align:center; width:10%;">Comments</th>
								<th style="text-align:center; width:10%;">Shares</th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<script>
$(document).ready(function(){
	$('.bvfoot').hide();
});	
</script>