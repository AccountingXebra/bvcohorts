  <style>
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#interview_form_vid .error{ color:red; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-5 mx-auto">
    <form method="post" id="interview_form_vid" name="interview_form_vid"  enctype="multipart/form-data">
      <fieldset>
        <div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Upload Interview Video</a>
        </div>

        <div style="text-align: center">
          <p style="color: green; font-family: Arial;">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </p>
        </div>
        
        <div class="form-group" hidden>
         
        </div>

        <div class="form-group" hidden>
         
        </div>

        <!--div class="form-group">
          <select name="language" class="form-control">
            <option value="">Select Language</option>
            <?php //if(count($getLanguage)):?>
              <?php //foreach($getLanguage as $language):?>
                <option value="<?php //echo $language->name;?>"><?php //echo $language->name;?></option>
              <?php //endforeach;?> 
            <?php //else:?>
            <?php //endif;?>
          </select>
          <?php //echo form_error('language', '<div class="text-danger">', '</div>'); ?>
        </div-->  
		<div class="form-group">
			<label>Title</label>
			<input type="text" class="form-control" name="title" id="title" placeholder="Title" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Customize Url</label>
			<input type="text" class="form-control" name="url" id="url" placeholder="Customize Url" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
        <div class="form-group">
			<label>Name</label>
			<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>
			<!--<select class="form-control" name="name" id="name" required >
			<option>Select Name </option>
				<?php //foreach($registeredUser as $user){ ?>
					<option value="<?//=$user->reg_id."-".$user->reg_username?>"><?//=$user->reg_username?></option>
				<?php //} ?>
			</select>-->
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
          <label>Conducted By</label>
          <input type="text" class="form-control" name="conduct" id="conduct" placeholder="Name" required>
          <?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Add Cover Image</label>
			<input type="file" class="form-control-file" name="cover_image" id="cover_image" required>
			<div class="text-danger video-error"></div>
        </div>
		</br>
		<div class="form-group">
			<label>Video Url</label>
			<input type="text" class="form-control" name="interview_url" id="interview_url" placeholder="Interview Url" required>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
          <label>Video File</label>
          <input type="file" class="form-control-file" name="video" id="video_up" required>
		  <div class="text-danger video-error"></div>
        </div>
		<div class="text-danger file-error"></div>
        <br>
	

        <input style="color:#fff;" type="submit" id="upload_video_admin"  value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_interviews">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
		});
	</script>