<div class="container-fluid" style="margin-top: 30px; margin-left: 30px; width: 97%;">

  <div>
    <a style="color: grey; font-size: 20px;">DASHBOARD</a>
  </div>

  <div class="row" style="margin-left: 5%;">
    <div class="col-sm-2">
        <br><br>
        <a style="font-size: 25px; color: grey;"> <img style="margin-right: 40px;" src="<?php echo base_url(); ?>asset/images/logo/id-card.png" width="50" height="50"> <?php echo $count_users; ?></a>
        <br>
        <a style="color: grey; font-family: Arial;">Number of Users</a>
    </div>
    <div class="col-sm-2">
        <br><br>
        <a style="font-size: 25px; color: grey;"><img style="margin-right: 40px;" src="<?php echo base_url(); ?>asset/images/logo/user.png" width="50" height="50"> <?php echo $count_clients; ?> </a>
        <br>
        <a style="color: grey; font-family: Arial;" href="<?php echo base_url();?>admin/clients" target="_blank">Number of clients registered</a>
    </div>
    <div class="col-sm-2">
        <br><br>
        <a style="font-size: 25px; color: grey;"><img style="margin-right: 40px;" src="<?php echo base_url(); ?>asset/images/logo/categories.png" width="50" height="50"> <?php echo $count_categories; ?> </a>
        <br>
        <a style="color: grey; font-family: Arial;" href="<?php echo base_url();?>admin/view-uploaded-video" target="_blank">Categories Videos</a>
    </div>
    <div class="col-sm-3">
        <br><br>
        <a style="font-size: 25px; color: grey;"><img style="margin-right: 60px;" src="<?php echo base_url(); ?>asset/images/logo/editor.png" width="50" height="50"> <?php echo $count_editorspick; ?> </a>
        <br>
        <a style="color: grey; font-family: Arial;" href="<?php echo base_url();?>admin/editors-pick" target="_blank">Editor's Pick Videos</a>
    </div>
    <div cass="col-sm-3">
        <br><br>
        <a style="font-size: 25px; color: grey;"><img style="margin-right: 90px;" src="<?php echo base_url(); ?>asset/images/logo/insp.png" width="50" height="50"> <?php echo $count_inspiringpeople; ?> </a>
        <br>
        <a style="color: grey; font-family: Arial;" href="<?php echo base_url();?>admin/inspiring-people" target="_blank">Inspiring Peoples Videos</a>
    </div>
  </div>

  <br><br><br>

  <div>
    <a style="color: grey; font-size: 20px;">User Details</a>
    
      <a style="margin-left: 350px; color: green;" >
        <?php if($msg = $this->session->flashdata('msg')): ?>
        <?php echo $msg; ?>
        <?php endif; ?>
      </a>
    
    <a style="color: grey; font-size: 20px; float: right; margin-right: 17px; padding: 6px; color: white; border-radius: 10px; background-color: #bf4b3d;" href="<?php echo base_url();?>admin/register-new-user">&nbsp Add New User &nbsp</a>
  </div>

  <br>

  <div class="container-fluid text-center"> 
    <table class="table table-hover">
      <thead>
        <tr style="font-weight: bold">
          <th scope="col">Date Registered</th>
          <th scope="col">First Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Email</th>
          <th scope="col">Phone</th>
          <!--<th scope="col">Status</th>-->
          <th scope="col">Action</th>
        </tr>
      </thead>
      
      <tbody>
        <?php if(!empty($posts)){ foreach($posts as $post){ ?>
        <tr class="table-active">
          <td><?= date('d.m.Y',strtotime($post->created)); ?></td>
          <td><?php echo $post->first_name; ?></td>
          <td><?php echo $post->last_name; ?></td>
          <td><?php echo $post->email; ?></td>
          <td><?php echo $post->phone; ?></td>
          <!--<td></td>-->
          <td class="can">
            <?=anchor("admin/user-delete/".$post->id, "Delete", array('onclick' => "return confirm('Do you really want to delete this user?')"))?>
          </td>
        </tr>
        <?php } } else { ?>
          <tr>
            <td>No Users Found!</td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

  <br><br>

  <div>
    <a style="color: grey; font-size: 20px;">Video Details</a>
    <a style="color: grey; font-size: 20px; float: right; margin-right: 17px; padding: 6px; color: white; border-radius: 10px; background-color: #bf4b3d;" href="<?php echo base_url();?>admin/upload-video">&nbsp Add New Video &nbsp</a>
  </div>

  <br>

  <div class="container">
    <div class="row" style="color: grey;">
      <div class="col-md-4">
        <p style="font-size: 20px; margin-bottom: 10px;">Categories Videos</p>
        <p>Number of videos in Assamese: <?php echo $count_categoriesassamese; ?></p>
        <p>Number of videos in Bangla: <?php echo $count_categoriesbangla; ?></p>
        <p>Number of videos in English: <?php echo $count_categories; ?></p>
        <p>Number of videos in Hindi: <?php echo $count_categorieshindi; ?></p>
        <p>Number of videos in Gujarati: <?php echo $count_categoriesgujarati; ?></p>
        <p>Number of videos in Marathi: <?php echo $count_categoriesmarathi; ?></p>
        <p>Number of videos in Tamil: <?php echo $count_categoriestamil; ?></p>
        <p>Number of videos in Telugu: <?php echo $count_categoriestelugu; ?></p>
        <p>Total number of videos in Categories: <?php echo $count_categoriestotal; ?></p>
      </div>

      <div class="col-md-4">
        <p style="font-size: 20px; margin-bottom: 10px;">Editor's Pick Videos</p>
        <p>Number of videos in Assamese: <?php echo $count_editorspickassamese; ?></p>
        <p>Number of videos in Bangla: <?php echo $count_editorspickbangla; ?></p>
        <p>Number of videos in English: <?php echo $count_editorspick; ?></p>
        <p>Number of videos in Hindi: <?php echo $count_editorspickhindi; ?></p>
        <p>Number of videos in Gujarati: <?php echo $count_editorspickgujarati; ?></p>
        <p>Number of videos in Marathi: <?php echo $count_editorspickmarathi; ?></p>
        <p>Number of videos in Tamil: <?php echo $count_editorspicktamil; ?></p>
        <p>Number of videos in Telugu: <?php echo $count_editorspicktelugu; ?></p>
        <p>Total number of videos in Editor's Pick: <?php echo $count_editorstotal; ?></p>
      </div>

      <div class="col-md-4">
        <p style="font-size: 20px; margin-bottom: 10px;">Inspiring Peoples Videos</p>
        <p>Number of videos in Assamese: <?php echo $count_inspiringpeopleassamese; ?></p>
        <p>Number of videos in Bangla: <?php echo $count_inspiringpeoplebangla; ?></p>
        <p>Number of videos in English: <?php echo $count_inspiringpeople; ?></p>
        <p>Number of videos in Hindi: <?php echo $count_inspiringpeoplehindi; ?></p>
        <p>Number of videos in Gujarati: <?php echo $count_inspiringpeoplegujarati; ?></p>
        <p>Number of videos in Marathi: <?php echo $count_inspiringpeoplemarathi; ?></p>
        <p>Number of videos in Tamil: <?php echo $count_inspiringpeopletamil; ?></p>
        <p>Number of videos in Telugu: <?php echo $count_inspiringpeopletelugu; ?></p>
        <p>Total number of videos in Inspiring Peoples: <?php echo $count_inspiringpeopletotal; ?></p>
      </div>
    </div>
  </div>

</div>

<br><br>