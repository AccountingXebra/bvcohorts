
    <div class="container-fluid">
      <div style="margin-top: 30px; margin-bottom: 30px; margin-left: 20px;">
          <a style="color: grey; font-size: 20px;">Partner With Us</a>
          <div class="col-md-4 search-panel" style="float: right">
              <form method="post">
                  <div class="input-group mb-3">
                      <input type="text" name="searchKeyword" class="form-control" placeholder="Search..." value="<?php if(isset($searchKeyword)){echo $searchKeyword;} ?>">
                      <div class="input-group-append">
                          <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                          <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                      </div>
                  </div>
              </form>
          </div>
          <div class="errormessage">
            <?php if($msg = $this->session->flashdata('msg')): ?>
            <?php echo $msg; ?>
            <?php endif; ?>
          </div>
      </div>
    </div>

  	<div class="container-fluid text-center">
     
  		<table class="table table-hover">
  			<thead>
  				<tr style="font-weight: bold">
            <th scope="col">Date Submitted</th>
  					<th scope="col">Name</th>
  					<th scope="col">Email</th>
  					<th scope="col">Organisation</th>
  					<th scope="col">Purpose</th>		
  					<th scope="col">Action</th>
  				</tr>
  			</thead>
  				<?php if(!empty($posts)){ foreach($posts as $post){ ?>
			    <tr class="table-active">
            <td><?= date('d.m.Y',strtotime($post['date_created'])); ?></td>
			      <td><?php echo $post['name']; ?></td>
			      <td><?php echo $post['email']; ?></td>
			      <td><?php echo $post['organisation']; ?></td>
			      <td><?php echo $post['purpose']; ?></td>
			      <td>
			      	<?php echo anchor("admin/partner-with-us-details/{$post['id']}", 'View'); ?> |
              <?=anchor("admin/partners-delete/".$post['id'], "Delete", array('onclick' => "return confirm('Do you want delete this record')"))?>
			      </td>
			    </tr>
			    <?php } } else { ?>
			    	<tr>
			    		<td>No Records Found!</td>
			    	</tr>
			    <?php } ?>
        </table>
        <div class="pagination" style="margin-left: 600px; margin-bottom: -20px;">
          <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
  <br>