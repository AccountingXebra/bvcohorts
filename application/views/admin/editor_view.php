
	<div class="container-fluid" style="line-height: 2">
		<div style="margin-top: 27px; margin-bottom: 23px; margin-left: 20px;">
            <a style="color: grey; font-size: 20px;">Post Details</a>
        </div>
        <div style="margin-left: 20px;">
			<b>Title:</b> <?php echo $post->title; ?>
			<p><b>Description:</b> <?php echo $post->description; ?></p>
			<p><b>Language:</b> <?php echo $post->language; ?></p>
			<p><b>Date uploaded:</b> <?php echo $post->date_created; ?></p>
			<p><b>Video:</b> </p>
			<?php if($post):?>
				<iframe src="<?php echo $post->video; ?>" width="1080" height="610"></iframe>
			<?php else: ?>
				<p>No Videos Found!</p>
			<?php endif; ?>
		</div>
	</div>