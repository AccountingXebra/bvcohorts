<?php error_reporting(E_ALL & ~(E_DEPRECATED|E_NOTICE|E_WARNING)); ?>
<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('SIGNUP DETAILS');
$this->excel->getActiveSheet()->setCellValue('A1', 'SIGNUP DETAILS');
$this->excel->getActiveSheet()->mergeCells('A1:I1');
$this->excel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'REGISTRATION DATE');
$this->excel->getActiveSheet()->setCellValue('B3', 'REGISTRATION ID');
$this->excel->getActiveSheet()->setCellValue('C3', 'NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'EMAIL ID');
$this->excel->getActiveSheet()->setCellValue('E3', 'PROFILE LINK');
$this->excel->getActiveSheet()->setCellValue('F3', 'COUNTRY');
$this->excel->getActiveSheet()->setCellValue('G3', 'CITY');
$this->excel->getActiveSheet()->setCellValue('H3', 'DAYS SINCE REGISTRATION');
$this->excel->getActiveSheet()->setCellValue('I3', 'STATUS');
$this->excel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(true);
$count=4;


foreach ($result as $key => $value) {

	 $country = $this->Admin_model->selectData("countries", "*", array('country_id' => $value['user_country']));
            $city = $this->Admin_model->selectData("cities", "*", array('city_id' => $value['user_city']));

            $date1 = date_create(date("Y-m-d", strtotime($value['createdat'])));
            $date2 = date_create(date("Y-m-d"));
            $diff = date_diff($date1, $date2);
            $days = $diff->format("%a");

	$this->excel->getActiveSheet()->setCellValue('A'.$count, date('d-m-Y', strtotime($value['createdat'])));
	$this->excel->getActiveSheet()->setCellValue('B'.$count,  $value['reg_id']);
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['reg_username']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['reg_email']);
	$this->excel->getActiveSheet()->setCellValue('E'.$count, base_url() . "profile/" . $value['reg_uniqname']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count,  @$country[0]->country_name);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, @$city[0]->name);
	$this->excel->getActiveSheet()->setCellValue('H'.$count,  $days);
	$this->excel->getActiveSheet()->setCellValue('I'.$count,$value['status']);
	$count++;
}

$filename='Signup.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
ob_end_clean();
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
$objWriter->save('php://output');
?>