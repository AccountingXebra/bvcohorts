  <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <style>
		#quiz_form{
			margin-top:20px;
		}
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#quiz_form .error{ color:red; }
		
		#start_date, #end_date{ width:20%; }
		.datepicker .input-group-addon { padding: 5px 5px; background-color: transparent; border: 1px solid #ced4da; border-left: none; border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; height:35px; }
		.datepicker .form-control { background-color:transparent; border-right:none; font-size:13px; border-top-left-radius: 4px !important; border-bottom-left-radius: 4px !important; padding: 16.5px 5px; text-align:center; color:#bc343a; }
		.datepicker .datelabel{ padding:6% 3%; font-size:13px; }
		.fa.fa-calendar{ margin: -26px 12px 0px 0px; float: right; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-8 mx-auto">
    <form method="post" id="quiz_form" name="quiz_form" action="" enctype="multipart/form-data">
      <fieldset>
        <div style="text-align: center">
			<p style="color: green; font-family: Arial;">
				<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</p>
        </div>
        <div class="form-group" hidden></div>
        <div class="form-group" hidden></div>
        <div class="form-group">
			<label>Title of the Quiz</label>
			<input type="text" class="form-control" name="quiz_name" id="quiz_name" placeholder="Quiz Title" required>
			<div class="text-danger conduct-error"></div>
        </div> 
		<div class="form-group">
			<label>Customize Url</label>
			<input type="text" class="form-control" name="quiz_url" id="quiz_url" placeholder="Customize Url" required>
			<div class="text-danger conduct-error"></div>
        </div> 
		<div class="form-group">
			<label>Quiz Cover Image</label>
			<input type="file" class="form-control-file" name="cover_image" id="cover_image" required>
			<div class="text-danger video-error"></div>
        </div>
        <div class="form-group" id="start_date" style="float:left; margin-right:20px;">
			<label>Start Date</label>
			<input id="sub_start_date" name="sub_start_date" class="datepicker form-control" type="text" placeholder="Start Date" />
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div> 
		<div class="form-group" id="end_date" style="float:left; margin-right:50%;">
			<label>End Date</label>
			<input id="sub_end_date" name="sub_end_date" class="datepicker form-control" type="text" placeholder="End Date" />
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div> 
		<div class="form-group">
			<label>Duration</label></br>
			<input type="text" class="form-control" name="duration" id="duration" placeholder="Time in minutes" required>
			<div class="text-danger conduct-error"></div>
        </div> 
		
		<h3><strong>Questions</strong></h3></br>
		<h4>Questions 1</h4>
		<div class="form-group">
			<input type="text" class="form-control" name="question[]" id="question" placeholder="Add question" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group" style="width:48%; float:left;">
			<input type="text" class="form-control" name="option1[]" id="option1" placeholder="Add first option" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group" style="width:48%; float:left; margin-left:4%;">
			<input type="text" class="form-control" name="option2[]" id="option2" placeholder="Add Second option" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group" style="width:48%; float:left;">
			<input type="text" class="form-control" name="option3[]" id="option3" placeholder="Add third option" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group" style="width:48%; float:left; margin-left:4%;">
			<input type="text" class="form-control" name="option4[]" id="option4" placeholder="Add fourth option" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group">
			<input type="text" class="form-control" name="answer[]" id="answer" placeholder="Add answer" required>
			<div class="text-danger conduct-error"></div>
        </div>
		<div id="question_section" class="onchangeevent"></div>
		
		<a style="color:#fff !important; color:#176981; margin:20px 0;" id="add_anotherquetion" class="btn btn-success">Add More Questions</a>
		</br>
		<input style="color:#fff;" type="submit" id="upload_blog" value="Submit" class="btn btn-success"/>
        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_quiz_list">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
			var counter=1;
			$('#add_anotherquetion').click(function(){
			//	alert('90');
				counter++;
				
				var select='';
				
				select='<h4>Questions '+counter +'</h4><div class="form-group"><input type="text" class="form-control" name="question[]" id="question'+counter+'" placeholder="Add question" required><div class="text-danger conduct-error"></div></div><div class="form-group" style="width:48%; float:left;"><input type="text" class="form-control" name="option1[]" id="option'+counter+'" placeholder="Add first option" required><div class="text-danger conduct-error"></div></div><div class="form-group" style="width:48%; float:left; margin-left:4%;"><input type="text" class="form-control" name="option2[]" id="option'+counter+'" placeholder="Add Second option" required><div class="text-danger conduct-error"></div></div><div class="form-group" style="width:48%; float:left;"><input type="text" class="form-control" name="option3[]" id="option'+counter+'" placeholder="Add third option" required><div class="text-danger conduct-error"></div></div><div class="form-group" style="width:48%; float:left; margin-left:4%;"><input type="text" class="form-control" name="option4[]" id="option'+counter+'" placeholder="Add fourth option" required><div class="text-danger conduct-error"></div></div><div class="form-group"><input type="text" class="form-control" name="answer[]" id="answer'+counter+'" placeholder="Add answer" required><div class="text-danger conduct-error"></div></div>';
				
				$('#question_section').append(select);		
			});
			
			$(document).on("change","#podcast_audio",function(){
				var file_id=$(this).data("val");
				var filesize = (this.files[0].size);
				var filesize_mb =  filesize / 1048576;
				var filename = (this.files[0].name);
				if(filename !=""){
					var file = this.files[0];
					var fileType = file["type"];
					alert(fileType);
					var validImageTypes = ["video/mp4", "audio/mpeg3", "audio/mpeg", "audio/x-aiff"];
					if ($.inArray(fileType, validImageTypes) < 0) {
						$("#toast").toast({
							type: 'error_1',
							message: 'Audio Format is incorrect.'
						});
						$("#podcast_audio").val('');
					}else if(filesize_mb > 100) {
						$("#toast").toast({
							type: 'error_1',
							message: 'Upload audio size should not be more than 100mb'
						});
						$("#podcast_audio").val('');
					}	
				}
			});
		});
		$(function () {
			$(".datepicker").datepicker({ 
				autoclose: true, 
				todayHighlight: true
			}).datepicker('update', new Date());
		});
	</script>