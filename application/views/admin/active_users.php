<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sign_style.css">
<style>
	#content{ margin: 2% 8% 2% 8%; padding:2% 0; border:1px solid #2da5a6; border-radius:10px; }
	.title-name{ padding:0% 2% 2% 2%; font-size:18px; color:#424242; }
	#active_users_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}

	#active_users_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#active_users_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#active_users_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	table.dataTable thead th, table.dataTable thead td{ font-size:12px; }
	table.dataTable.display tbody tr td:nth-child(1){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(2){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(3){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(4){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(5){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(6){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(7){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(8){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(9){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(10){
		text-align:center !important;
	}
	table.dataTable.display tbody tr td:nth-child(11){
		text-align:center !important;
	}
	.action_link{ color:#bc343a; text-decoration:underline; text-align:center; }
	
	.cat-drop.dropdown-toggle::after{ margin-left: 32% !important; font-size: 17px; }
	.video_up{ width:85% !important; }
	.no_sub{ width:88% !important; }
</style>
<div class="wrapper admin-list">
	<div class="container-fluid">
	<div id="content">
		<div class="row" style="padding:0 0 0 0">
			<div class="col-md-3">
				<div class="input-field" style="margin:0 15px 20px 0px;">
					<a class="video_up nav-link shadow dropdown-toggle" href="#" id="video_upload" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">VIDEO UPLOADED</a>
					<div class="dropdown-menu" aria-labelledby="status">
						<a class="dropdown-item">VIDEO1</a>
					</div>
				</div>
			</div>	
			<div class="col-md-3 ">	
				<div class="input-field" style="margin:0 15px 20px -22%;">
					<a class="no_sub nav-link shadow dropdown-toggle" href="#" id="no_subscribers" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NUMBERS OF SUBSCRIBERS</a>
					<div class="dropdown-menu" aria-labelledby="status">
						<a class="dropdown-item">10</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
			<div class="input-field" style="margin:0 15px 20px 15px; float:right;">
				<a class="tot_view nav-link shadow dropdown-toggle" href="#" id="total_view" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">TOTAL VIEWS</a>
				<div class="dropdown-menu" aria-labelledby="status">
					<a class="dropdown-item">CAT1</a>
				</div>
			</div>
			</div>			
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12">
					<table id="active_users_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader">
							<tr style="border:1px solid red;">
								<!--th style="width:5%; vertical-align:middle">
									<input type="checkbox" id="uservideo_bulk" name="uservideo_bulk" class="filled-in purple" />
									<label for="uservideo_bulk"></label>
								</th-->
								<th style="text-align:center; width:20%;">First & Last</br>Name</th>
								<th style="text-align:center; width:15%;">Registered </br> Date</th>
								<th style="text-align:center; width:15%;">Logins/Week</th>
								<th style="text-align:center; width:15%;">Videos</br>Uploaded</th>
								<th style="text-align:center; width:15%;">Total Views</th>
								<th style="text-align:center; width:15%;">Subscribres</th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<script>
$(document).ready(function(){
	$('.bvfoot').hide();
});	
</script>