<style type="text/css">
  .contact-header {
    text-align: center;
    font-family: Arial;
    font-size: 17px;
    padding:40px 0;
    color: grey;
    margin-top: -5px;
    margin-bottom: -35px;
  }

  .input-container {
    display: -ms-flexbox;
    display: flex;
    width: 150%;
    margin-bottom: 20px;
    font-family: Arial;
  }

  .icon {
    padding: 15px;
    background: white;
    color: #ccc;
    min-width: 20px;
    text-align: center;
    border: 1.5px solid #ccc;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }

  .input-field {
    width: 50%;
    padding: 10px;
    outline: none;
    border: 1.5px solid #ccc;
    border-left: 0px;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
  }

  .sub-btn:hover {
    opacity: 1;
  }

  .col-75 {
    float: left;
    width: 100%;
    margin-top: 1px;
    margin-left: -5px;
  }

  .fa-user{
  font-size: 20px;
  }

  #contact-btn{
  border:1px solid #bf4b3d !important;
  background-color: #bf4b3d;
  color:#fff;
  }

  .sub-btn{
  text-align: center;
  margin-top: 5px;
  background-color: #bf4b3d;
  color: white;
  padding: 12px 15px;
  border: none;
  border-radius: 10px;
  cursor: pointer;
  width: 34%;
  opacity: 0.9;
  font-family: Arial;
  margin-left: 205px;
  }

  .errormsg1 {
    color: #bf4b3d;
    font-family: Arial;
  }
</style>

<div class="container-fluid">
  <div style="margin-top: 40px; margin-bottom: -5px; text-align: center;">
    <a style="font-family: Arial; color: grey; font-size: 25px; margin-left: -40px;">Reset Password</a>
    <br><br>
    <div class="errormsg1">
      <?=$this->session->flashdata('msg'); ?>
    </div>
  </div>
  <form action="update-password-save" method="post" style="max-width:600px; margin:auto; padding:2.5% 1% 3% 6%;">

    <div class="input-container">
      <i class="fa fa-envelope icon"></i>
      <input class="input-field" type="email" name="email" placeholder="Please re-enter your email.." required>
    </div>

    <div class="input-container">
      <i class="fa fa-lock icon"></i>
      <input class="input-field" type="password" name="new_password" placeholder="Enter your new Password" required>
    </div>

    <div class="input-container">
      <i class="fa fa-lock icon"></i>
      <input class="input-field" type="password" name="password_conf" placeholder="Confirm Password" required>
    </div>

    <div style="margin-left: 245px;" class="sub-btn">
      <input type="submit" name="Submit" id="contact-btn" value="Reset Password">
    </div>

  </form>
</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>