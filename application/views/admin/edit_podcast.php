  <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
  <style>
		#podcast_form{
			margin-top:20px;
		}
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>assets/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#podcast_form .error{ color:red; }
		#excerpt{ width:40%; height:100px; }
		#pod_description{ width:100%; height:100px; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-8 mx-auto">
    <form method="post" id="edit_podcast_form" name="edit_podcast_form" action="" enctype="multipart/form-data">
      <fieldset>
		<div style="margin-top: 30px; margin-bottom: 30px;">
          <a style="color: grey; font-size: 20px;">Edit Podcast</a>
        </div>
        <div style="text-align: center">
			<p style="color: green; font-family: Arial;">
				<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</p>
        </div>
        <div class="form-group" hidden></div>
        <div class="form-group" hidden></div>
        <div class="form-group">
			<label>Title of the podcast</label>
			<input type="text" class="form-control" name="podcast_name" id="podcast_name" value="<?=$podcast[0]->title?>" placeholder="Podcast Title" required>
			<input type="hidden" class="form-control" name="podcast_id" id="podcast_id" value="<?=$podcast[0]->id?>" >
			<div class="text-danger conduct-error"></div>
        </div>
		<div class="form-group">
			<label>Customize Url</label>
			<input type="text" class="form-control" name="podcast_url" id="podcast_url" placeholder="Customize Url" value="<?=$podcast[0]->url?>" required>
			<div class="text-danger conduct-error"></div>
        </div> 		
		<div class="form-group">
			<label>Podcast Cover Image</label>
			<input type="file" class="form-control-file" name="cover_image" id="cover_image" >
			<div class="text-danger video-error"></div>
			<label style="margin:12px 0 0 0;"><?=$podcast[0]->cover; ?></label>
        </div>
		<div class="form-group">
			<label>Name of the Interviewer</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<select class="form-control" name="name" id="name" required >
			<option>Select Name </option>
				<?php foreach($registeredUser as $user){ ?>
					<option value="<?=$user->reg_id."-".$user->reg_username?>" <?php if($user->reg_id==$podcast[0]->profile_id){echo "selected";}?>><?=$user->reg_username?></option>
				<?php } ?>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		<div class="text-danger file-error"></div>
        <div class="form-group">
			<label>Conducted By</label>
			<input type="text" class="form-control" name="podcast_conduct" id="podcast_conduct" value="<?=$podcast[0]->conduct?>" placeholder="Conducted By" required>
			<div class="text-danger conduct-error"></div>
        </div> 
		<div class="form-group">
			<label>Audio File</label>
			<input type="file" class="form-control-file" name="podcast_audio" id="podcast_audio" >
			<div class="text-danger video-error"></div>
			<label style="margin:12px 0 0 0;"><?=$podcast[0]->filename; ?></label>
        </div>
		<div class="text-danger file-error"></div>
        <br>
		<div class="form-group">
			<label>Alt / Title for Image</label>
			<input type="text" class="form-control" name="alt_name" id="alt_name" placeholder="Alt / Title" value="<?=$podcast[0]->alt_name?>" required>
			<div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<div class="form-group">
			<label>Description</label></br>
			<textarea name="pod_description" id="pod_description"><?=$podcast[0]->pod_description?></textarea>
        </div> 
		<br>
        <input style="color:#fff;" type="submit" id="upload_blog" value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/podcast_list">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
			
			$(document).on("change","#podcast_audio",function(){
				var file_id=$(this).data("val");
				var filesize = (this.files[0].size);
				var filesize_mb =  filesize / 1048576;
				var filename = (this.files[0].name);
				if(filename !=""){
					var file = this.files[0];
					var fileType = file["type"];
					var validImageTypes = ["video/mp4", "audio/mpeg3", "audio/mpeg", "audio/x-aiff"];
					if ($.inArray(fileType, validImageTypes) < 0) {
						$("#toast").toast({
							type: 'error_1',
							message: 'Audio Format is incorrect.'
						});
						$("#podcast_audio").val('');
					}else if(filesize_mb > 100) {
						$("#toast").toast({
							type: 'error_1',
							message: 'Upload audio size should not be more than 100mb'
						});
						$("#podcast_audio").val('');
					}	
				}
			});
		});
	</script>