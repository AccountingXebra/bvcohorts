  <!-- <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.20.0/ckeditor.js"></script>
  <style>
		.tooltip {
			position: relative;
			display: contents;
		}

		.tooltip .tooltiptext {
			visibility: hidden;
			font-size: 15px !important;
			width: 230px;
			background-color: #000;
			color: #fff;
			text-align: center;
			border-radius: 6px;
			padding: 5px 0;
			margin: -35px 0 0 0;
			position: absolute;
			z-index: 1;
			height: 35px !important;
		}

		.tooltip:hover .tooltiptext {
			visibility: visible;
		}
		#edit_blog_form_vid{
			margin-top:20px;
		}
		#toast {
			position: fixed;
			z-index: 9999;
			top: 7%;
			right: 70%;
		}

		.notification {
			display: block;
			position: relative;
			overflow: hidden;
			margin-top: 20px;
			margin-right: 10px;
			padding: 20px;
			width: 300px;
			border-radius: 3px;
			color: white;
			right: -500px;
		}
		.normal {
			background: #273140;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.success {
			background: #44be75;
			border-radius:35px;
			width: 125%;
			text-align: center;
		}
		.error_1 {
			background: #c33c3c;
			border-radius:35px;
			width: 125%;
			text-align: center;
			z-index: 9999;
		}
		#select_tags{ float:left; padding:5px; width: 107%; border:1px solid #ccc; border-radius: 5px; margin: -20px 0 20px -16px; height: 100px;}
		#select_tags > span{ cursor:pointer; display:block; float:left; color:#fff; background:#789; padding:5px; padding-right:25px; margin:4px; }
		#select_tags > span:hover{ opacity:0.7; }
		#select_tags > span:after{ position:absolute; content:"×"; border:1px solid; padding:0px 5px; margin-left:3px; font-size:12px; }
		#select_tags > input{ background:transparent; border:0; margin:4px; padding:7px; width:auto; font-size:14px;  width:100%;     font-size: 15px;}
		input.hide-file-thumbnail { opacity: 0; width: 100%; float: left; height: 100%; cursor: pointer; }
		.loader {
			position: fixed;
			left: 0px;
			top: -5%;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?php echo base_url(); ?>asset/images/logo/last.gif') center no-repeat #fff;
			opacity: 0.8;
		}
		
		.logo-item {
			margin-right:18%;
		}	
		#blog_form_vid .error{ color:red; }
		#excerpt{
			width:100%;
			height:100px;
		}
		.cke_contents{ height:280px !important; }
  </style>
  <div class="loader"></div>
  <div id="toast"></div>
  <div class="container-fluid col-md-8 mx-auto">
    <form method="post" id="edit_blog_form_vid" name="edit_blog_form_vid" action="" enctype="multipart/form-data">
      <fieldset>
        <div style="text-align: center">
			<p style="color: green; font-family: Arial;">
				<?php if($msg = $this->session->flashdata('msg')): ?>
				<?php echo $msg; ?>
				<?php endif; ?>
			</p>
        </div>
        <div class="form-group" hidden></div>
        <div class="form-group" hidden></div>
        <div class="form-group" style="width:31%; float:left;">
			<label>Category</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<select class="form-control" name="category" id="category" required >
				<option value="">Select Category</option>
			
				<option value="media"  <?php if($blog[0]->category=="media"){echo "selected";} ?> >MEDIA</option>
				<option value="scheme" <?php if($blog[0]->category=="scheme"){echo "selected";} ?>>SCHEME</option>
				<option value="news" <?php if($blog[0]->category=="news"){echo "selected";} ?>>NEWS</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		<div class="form-group" style="width:31%; float:left; margin-left:20px;">
			<label>Year</label>
			<!-- <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>-->
			<?php $year=date('Y'); ?>	
			<select id="newsletter_year" name="newsletter_year" class="country-dropdown form-control" required>
				<option value="0" selected>Select Year</option>
				<?php for($i=$year;$i > $year-8;$i--){ ?>
				<option value="<?=$i?>" <?php if($blog[0]->newsletter_year == $i){echo "selected";} ?>><?=$i?></option>
				<?php } ?>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		<div class="form-group" style="width:31%; float:left; margin-left:20px;">
			<label>Month</label>
			<select id="newsletter_month" name="newsletter_month" class="country-dropdown form-control">
				<option value="0" selected>Select Month</option>
				<option value="1" <?php if($blog[0]->newsletter_month=="1"){echo "selected";} ?>>January</option>
				<option value="2" <?php if($blog[0]->newsletter_month=="2"){echo "selected";} ?>>February</option>
				<option value="3" <?php if($blog[0]->newsletter_month=="3"){echo "selected";} ?>>March</option>
				<option value="4" <?php if($blog[0]->newsletter_month=="4"){echo "selected";} ?>>April</option>
				<option value="5" <?php if($blog[0]->newsletter_month=="5"){echo "selected";} ?>>May</option>
				<option value="6" <?php if($blog[0]->newsletter_month=="6"){echo "selected";} ?>>June</option>
				<option value="7" <?php if($blog[0]->newsletter_month=="7"){echo "selected";} ?>>July</option>
				<option value="8" <?php if($blog[0]->newsletter_month=="8"){echo "selected";} ?>>August</option>
				<option value="9" <?php if($blog[0]->newsletter_month=="9"){echo "selected";} ?>>September</option>
				<option value="10" <?php if($blog[0]->newsletter_month=="10"){echo "selected";} ?>>October</option>
				<option value="11" <?php if($blog[0]->newsletter_month=="11"){echo "selected";} ?>>November</option>
				<option value="12" <?php if($blog[0]->newsletter_month=="12"){echo "selected";} ?>>December</option>
			</select>
			<?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
			<div class="text-danger name-error"></div>
        </div> 
		</br>
		<div class="form-group">
          <label>Post Title (60 Character)</label>
          <input type="text" class="form-control" name="post_name" id="post_name" placeholder="Post Name" value="<?=$blog[0]->post_name?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
			<div class="form-group">
          <label>Post Customise url</label>
		  <p class="tooltip"><img src="<?php echo base_url(); ?>asset/images/infocircle.png"/><span class="tooltiptext">Do not use in special charatcter</span></p>
          <input type="text" class="form-control" name="url" id="url" placeholder="Post Customise url" value="<?=$blog[0]->url?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<!--<div class="form-group">
          <label>Description</label>
          <input type="text" class="form-control" name="description" id="description" value="<?=$blog[0]->description?>" placeholder="Description" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		<!-- Author Name -->
		<!--<div class="form-group">
          <label>Author Name</label>
          <input type="text" class="form-control" name="author_name" id="author_name" value="<?=$blog[0]->author_name?>" placeholder="Author Name" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>
		
		<div class="form-group">
          <label>Alt / Title for Image</label>
          <input type="text" class="form-control" name="alt_name" id="alt_name" placeholder="Alt / Title" value="<?=$blog[0]->alt_name?>" required>
		  <div class="text-danger conduct-error"></div>
        </div> 
		</br>-->
		
		<div class="form-group">
			<textarea name="blog_content" id="blog_content"><?=$blog[0]->blog_content?></textarea>
        </div> 
		</br>
		<p style="font-size:18px;">Blog Index Page Deatils</p></br>
		<div class="form-group">
			<label>Featured image (Only jpg & png format)</label>
			<input type="file" class="form-control-file" name="featured_image" id="featured_image" >
			<div class="text-danger video-error"></div>
			<label style="margin:12px 0 0 0;"><?=$blog[0]->filename; ?></label>
        </div>
		<div class="text-danger file-error"></div>	
        <br>
		<div class="form-group">
			<label>Excerpt (Not exceeding 200 character)</label></br>
			<textarea name="excerpt" id="excerpt" maxlength="200"><?=$blog[0]->excerpt?></textarea>
        </div> 
		</br>
        <input style="color:#fff;" type="submit" id="upload_blog" value="Submit" class="btn btn-success"/>

        <a style="background-color: #c91414; color: white; padding-top: 8px; border-radius: 5px; padding-bottom: 12px; padding-right: 15px; padding-left: 15px;" href="<?php echo base_url();?>admin/admin_blogs_list">Cancel</a> 
      </fieldset>
    </form>
    <br>
	</div>
	<script>
		$(window).load(function() {
			$(".loader").fadeOut();
			CKEDITOR.replace('blog_content');
		});

		$(document).on("change","#featured_image",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["image/jpeg", "image/jpg", "image/png"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Image format is incorrect.'
					});
					$("#featured_image").val('');
				}
				/*else if(filesize_mb > 100) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Upload video size should not be more than 100mb'
					});
					$("#video_up").val('');
				}*/	
			}
		});
	</script>