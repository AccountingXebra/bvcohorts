<?php include('header.php'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/contact_partner_us.css">
<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<title>Partner with Bharat's innovation destination</title>
<meta name="description" content="Partner With Us">
<style type="text/css">
  .status-error {
    color: green;
    font-family: Arial;
    font-size: 17px;
  }
  
  .contactFrom .input-container{ margin-bottom:22px; }
	.contactFrom #contact-btn{ width:70px !important; }
	.contactFrom .error {
		font-family: Arial;
		color: red;
		margin-top: 20px;
		margin-bottom: -15px;
		text-align: center;
		width: 60%;
		padding: 10px 0;
	}
	
	.contactFrom .success {
		font-family: Arial;
		color: green;
		margin-top: 20px;
		margin-bottom: -15px;
		text-align: center;
		width: 35%;
		padding: 10px 0;
		color: #fff;
	}

	.text-danger {
		margin: 5px 0 5px 0;
		float: left;
	}
	.a-link{ padding:1.2% 0px 0px 30px; }
	.contact{ padding-top:4% !important; }
	.bvfoot{ margin-top:5% !important; }
	.contactFrom .contact-header {
		text-align: center;
		font-family: Arial;
		font-size: 18px;
		padding: 60px 0 55px 0;
		color: grey;
		margin-top: -25px !important;
		margin-bottom: -60px !important;
	}
	
	.fa{
		padding: 10px 0;
		font-size: 41px;
	}
	::placeholder{ color:#000; }
	input:focus::placeholder, textarea:focus::placeholder{
		color: transparent;
	}
	.contactFrom .input-field{ height:40px; }
	.contactFrom .input-field {
		width: 65%;
		padding: 10px;
		outline: none;
		border: 1.5px solid #ccc;
		border-radius: 10px;
		font-size:15px;
	}
	.contactFrom textarea { width: 98%; margin: 0 0 0 4px; padding: 10px 13px; font-size:15px; }
	.g-recaptcha { margin:2% 0 0% 22%; }
	.bvcontactform{ max-width: 500px; margin-left: 29.5%; padding:1% 1% 3% 6%; }
	.sub-btn{ padding: 8px 28px 8px 0px; margin-left: 67%; }
	.contactFrom .btn{ width:110px; }
	@media only screen and (max-width: 600px) {
		.contactFrom .bvcontactform{ max-width: 500px; margin: 0 6% 0 3%; padding:1% 1% 3% 6%; }
		.contactFrom .contact-header h1{ margin:8% 0 0 0; }
		.contactFrom .g-recaptcha { margin:2% 0 0% 2%; }
		.contactFrom .sub-btn{ padding: 8px 14px 8px 0px; margin-left: 67%; }
		.contactFrom{ margin: 0 2%; }
	}
</style>
<div class="contactFrom">
	<div class="contact-header">
		<h1 style="font-weight: bold; font-size: 30px;">Partner With Us</h1></br>
		<p>Our brand solutions team is a group of creative directors, video editors, copywriters, language experts, <br> Voice over artists, designers and social media managers and who will design and produce right set of brand videos</p>
		<p>You can email us at <a class="red" href="mailto:contactus@bharatvaani.in">partner(at)bharatvaani.in</a> or use the form below</p>
	</div>

  <center>
    <p style="color: green; font-family: Arial;">
      <?php if(!empty($status)){ ?>
        <div class="status-<?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
      <?php } ?>
    </p>
    <br>
  </center>

  <form action="<?php echo base_url();?>index/partnerwithus_save" method="post" style="max-width:500px; margin:auto; border:1px solid #ccc; padding:3% 3% 3% 4%;">

	<div class="col-sm-10">
      <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>
	<div class="input-container">
		<!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-user icon"></i>-->
		<input class="input-field" type="text" placeholder="NAME" name="name">
	</div>
	
	<div class="col-sm-10">
      <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
    </div>
	<div class="input-container">
		<!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-envelope icon"></i>-->
		<input class="input-field" type="text" placeholder="EMAIL" name="email">
	</div>

	<div class="col-sm-10">
      <?php echo form_error('organisation', '<div class="text-danger">', '</div>'); ?>
    </div>
	<div class="input-container">
		<!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-building icon"></i>-->
		<input class="input-field" type="text" placeholder="ORGANISATION" name="organisation">
	</div>

	<div class="col-sm-10">
		<?php echo form_error('purpose', '<div class="text-danger">', '</div>'); ?>
	</div>
	<div class="input-container">
		<!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-comments icon"></i>-->
		<input class="input-field" type="text" placeholder="PURPOSE" name="purpose">
	</div>

	<div class="col-sm-12">
      <?php echo form_error('message', '<div class="text-danger" style="font-size: 17px; font-family: Arial;">', '</div>'); ?>
    </div>
	<div class="col-75">
		<textarea name="message" placeholder="MESSAGE" style="height:200px; width:97.5%;"></textarea>
	</div>
	<div class="col-75">
		<div class="g-recaptcha" data-sitekey="6Lf6zdkZAAAAAHIlA8Jjz1keIIlqRnesZI2m7323"></div>
		<span class="input-contact-captcha"></span>	
	</div>

	<div class="sub-btn" style="margin-right: 48px !important;">
		<button type="submit" id="partner-btn" class="btn">SUBMIT</button>
	</div>

  </form>

  <br><br>
</div>

<div class="contactFrom" hidden>
  <div class="contact-header">
    <h5 style="font-weight: bold; font-size: 20px;">Partner With Us</h5>
    <br>
    <p>Our brand solutions team is a group of <br>creative directors, video editors, copywriters, language experts, Voice over artists, designers and social media managers and who will design and produce right set of brand videos</p>
    <br>
    <p>You can email us at <a href="mailto:contactus@bharatvaani.in">partner@bharatvaani.in</a> or use the form below</p>
    <br><br>
  </div>

  <center>
    <p style="color: green; font-family: Arial;">
      <?php if(!empty($status)){ ?>
        <div class="status-<?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
      <?php } ?>
    </p>
    <br>
  </center>

  <form action="<?php echo base_url();?>index/partnerwithus_save" method="post" style="max-width:500px; margin:auto; border:1px solid #ccc; padding:3% 1% 3% 6%;">

  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="NAME" name="name">
    <div>
      <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>
  </div>

  <div class="input-container">
    <i class="fa fa-envelope icon"></i>
    <input class="input-field" type="text" placeholder="EMAIL" name="email">
    <div>
      <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
    </div>
  </div>

  <div class="input-container">
    <i class="fa fa-building icon"></i>
    <input class="input-field" type="text" placeholder="ORGANISATION" name="organisation">
    <div>
      <?php echo form_error('organisation', '<div class="text-danger">', '</div>'); ?>
    </div>
  </div>

  <div class="input-container">
    <i class="fa fa-comments icon"></i>
    <input class="input-field" type="text" placeholder="PURPOSE" name="purpose">
    <div>
      <?php echo form_error('purpose', '<div class="text-danger">', '</div>'); ?>
    </div>
  </div>

  <div class="col-75">
    <textarea name="message" placeholder="MESSAGE" style="height:200px;"></textarea>
    <div>
      <?php echo form_error('message', '<div class="text-danger" style="font-size: 17px; font-family: Arial;">', '</div>'); ?>
    </div>
  </div>

  <div class="sub-btn" style="margin-right: 48px !important;">
    <button type="submit" id="partner-btn" class="btn">SUBMIT</button>
  </div>

  </form>

  <br><br>
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<?php include('footer.php'); ?>