<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bharat Vaani | Terms & Conditions</title>
	<meta name="description" content="Bharat Vaani | Terms & Conditions">
	<style>
	.heading{
		padding: 3% 0 0 0 !important;
		text-align: center ! important;
		margin-bottom:4%;
	}
	.background-image{
		/*background-image: url("img/C.png");*/
		height:250px; width:150px; float: left;
	}
	.container{
		width:100% ! important;
	}
	
	.first_line{
		
	}
	.tc-row{
		width:80% !important;
		margin-bottom:4%;
	}
	.tc-row p{ text-align:justify; font-size:18px; }
	.heading h1{ text-transform:initial; font-size:30px; }
	</style>
</head>
<body>
	<div class="container tc-row">
		<div class="heading">
			<h1> Terms and Conditions </h1>
		</div>
		<p class="first_line">
			Please read the following terms of service carefully before using the Social Play LLP’s Bharat Vaani community. The use of any Bharat Vaani community will signify your agreement to be bound by the terms. The Bharat Vaani community is made available by Social Play LLP only to persons who are above 18 years of age or of such legal age as may be permissible in your jurisdiction to effectuate a legally binding contract. Do not use the service if you do not agree to these terms and conditions.
		</p>

		<p>
		<strong>Purpose of Agreement</strong></br>
		<strong>Social Play LLP</strong>, an Indian company, based in Mumbai, is making available the services of Bharat Vaani subject to the following terms. Social Play LLP may also offer other services under different terms of service.
		<p>

		<p><strong>The Service</strong></br>
		Social Play LLP provides the service of a community website. You may use the service for your personal or business use in the organization you represent.</p>

		<p>
			<strong>Modification of Terms of Service</strong></br>
			Social Play LLP may modify the Terms upon notice to you at any time through a service announcement on the website <strong>https://www.bharatvaani.in</strong> or by sending an email to your primary email address. You will be provided with the option to terminate your use of the Services if Social Play LLP modifies the Terms in a manner that substantially affects your rights in connection with the use of the Services. Your continued use of the Service after notice of any change to the Terms will be deemed to be your agreement to the amended Terms.
		</p>

		<p><strong>Member Registration Obligations</strong></br>
			In consideration of your use of the Services, you agree to: 
			(a) provide true, accurate, current and complete information about yourself as prompted by the Registration Process and, 
			(b) Maintain and promptly update your Registration Data to keep it true, accurate, current, and complete. 
			If you provide any information that is untrue, inaccurate, outdated, or incomplete, or if Social Play LLP has reasonable grounds to suspect that such information is untrue, inaccurate, outdated, or incomplete, Social Play LLP has the right to terminate your account and refuse current or future use of any or all of the Services. 
		</p>

		<p><strong>Restrictions on Use</strong></br>
			In addition to all other terms and conditions of this Agreement, you shall not:
			(1) transfer the Services or otherwise, make it available to any third party;
			(2) provide any service based on the Services without prior written permission;
			(3) Use the Services in any manner that could damage, disable, overburden, impair or harm any server, network, computer system, resource of Social Play LLP;
			(4) violate any applicable local, state, national or international law;
			(5) use the third party links to sites without agreeing to their website terms & conditions;
			(6) post links to third party sites or uses their logo, company name, etc. without their prior written permission; and 
			(7) create a false identity to mislead any person as to the identity or origin of any communication.
		</p>

		<p><strong>Communications from Social Play LLP</strong></br>
			The Service may include certain communications from Social Play LLP, such as service announcements, administrative messages, SMS and newsletters. You understand that these communications shall be considered part of using the Services. As part of our policy to provide you with total privacy, we also offer you the option of opting out from receiving emails from us. However, this may prevent us from providing effective service to you.
		</p>

		<p><strong>Inactive User Accounts Policy</strong></br>
			We reserve the right to terminate user accounts that are inactive for a continuous period of 90 days. In the event of such termination, all data associated with such a user account will be deleted. We will provide you with prior notice of such termination and the option to back up your data. The data deletion policy may be implemented with respect to any or all of the Services. Each Service will be considered an independent and separate service for the purpose of calculating the period of inactivity. In other words, activity in one of the Services is insufficient to keep your user account in another Service active.
		</p>

		<p><strong>Personal Information and Privacy</strong></br>
			The personal information you provide to Social Play LLP through the Service is governed by Social Play LLP Privacy Policy and applicable laws related to personal information. Your election to use the Service indicates your acceptance of the terms and conditions of the Social Play LLP. You are responsible for maintaining the confidentiality of your username, password and other sensitive information. You are responsible for all activities that occur in your account, and you agree to notify Social Play LLP immediately of any unauthorized use of your account. Social Play LLP is in no way responsible for any loss or damage to you or any third party incurred as a result of any unauthorized access and/or use of your account or otherwise.
		</p>
		<p>
		<strong>Spamming and Illegal Activities</strong></br>
			You agree to be solely responsible for the contents of your transmissions through the Services. You agree not to use the Services for illegal purposes or for the transmission of material that is unlawful, harassing, libellous, invasive of another's privacy, abusive, threatening, harmful, vulgar, pornographic, obscene, or otherwise objectionable, contains viruses, or that infringes or may infringe the intellectual property or other rights of another. You agree not to use the Services for the transmission of "junk mail", "spam", "chain letters", or unsolicited mass distribution of email. Social Play LLP reserves the right to terminate your access to the Services if Social Play LLP believes that you have used the Services for any illegal or unauthorized activity.
		<p>

		<p><strong>Trademark & Property Rights</strong></br>
			<strong>Social Play LLP</strong>, Social Play LLP logo, Bharat Vaani, Bharat Vaani logo, the names of different Social Play LLP and their logos are trademarks of Social Play LLP. You agree not to display or use, in any manner, the Social Play LLP trademarks without Social Play LLP's prior permission. Unless otherwise indicated or anything contained to the contrary or any proprietary material owned by a third party and so expressly mentioned, Social Play LLP owns all Intellectual Property Rights to and into the Bharat Vaani services, including, without limitation, any and all rights, title and interest in and to copyright, related rights, patents, utility models, trademarks, trade names, service marks, designs, know-how, trade secrets and inventions (whether patentable or not), goodwill, source code, meta tags, databases, text, content, graphics, icons, and hyperlinks. You acknowledge and agree that you shall not use, reproduce, or distribute any content from Bharat Vaani belonging to Social Play LLP without obtaining Social Play LLP authorization.<p>

		<p><strong>Data Ownership</strong></br>
			We respect your right to ownership of content created or stored by you. Unless expressly permitted by you, your use of the Services does not grant Social Play LLP any license to use, reproduce, adapt, modify, publish or distribute the content created by you or stored in your Account for Social Play LLP commercial, marketing or any similar purpose. But you grant Bharat Vaani permission to access, copy, distribute, store, transmit, reformat, publicly display and publicly perform the content of your user account solely as required for the purpose of providing the Services to you.
		</p>
		
		<p><strong>Cookies</strong></br>
			We employ the use of cookies. By accessing Bharat Vaani, you agree to use cookies in agreement with our Privacy Policy. Most interactive websites use cookies to let us retrieve the user's details for each visit. Our website uses cookies to enable certain areas' functionality to make it easier for people to visit our website. Some of our affiliate/advertising partners may also use cookies.
		</p>

		<p><strong>User-Generated Content</strong></br> 
			You are responsible for your content. You are legally responsible for all information, data, text, software, music, sound, photographs, graphics, video, messages or other materials ("Content"), which is uploaded, posted or stored through your use of the Services. You grant Bharat Vaani a worldwide, royalty-free, non-exclusive license to host and use the content to provide you with the Services. You agree not to use the Services for any illegal purpose or violate any applicable law or regulation. You are encouraged to archive your content regularly and frequently. You are responsible for any Content that may be lost or unrecoverable through your use of the Services. You must provide all required and appropriate warnings, information, and disclosure.
		</p>

		<p>Parts of this website offer users an opportunity to post and exchange opinions and information in certain website areas. Social Play LLP does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect Social Play LLP's views and opinions, its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Social Play LLP shall not be liable for the Comments or any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.
		</p>
		
		<p>Social Play LLP reserves the right to monitor all Comments and remove any Comments that can be considered inappropriate, offensive, abusive or cause a breach of these Terms and Conditions. You warrant and represent that you are entitled to post the Comments on our website and have all necessary licenses and consents to do so. The Comments do not invade any intellectual property right, including the patent or trademark of any third party, without limitation. The Comments do not contain any defamatory, libellous, offensive, indecent or otherwise unlawful material that is an invasion of privacy. The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</p>
		
		<p>You agree that you will not use the Services to share, store, or in any way distribute financial data that is not in accordance with the law. Any users suspected of having information that involves fraud, embezzlement, money laundering, insider trading, support for terrorism, or any other activity proscribed by law may have their accounts terminated, their financial data erased, and they also may be reported to law enforcement officials in the appropriate jurisdictions. Bharat Vaani is not responsible for the content or data you submit on the website.</p>
		
		<p>You agree not to use the Services to upload, post, distribute, link to, publish, reproduce, engage in or transmit any of the following, including but not limited to:
		• Fraudulent, libellous, offensive, inappropriate or objectionable information or communications of any kind, including without limitation conduct that would encourage or constitute an attack or "flaming" others, or criminal or civil liability under any local or foreign law; indecent, profane (including masked profanity where symbols, initials, intentional misspellings or other characters are used to suggest profane language), obscene, lascivious, sexually explicit, pornographic, abusive, threatening, menacing, misleading, derogatory, illegal or defamatory information or communication, that is likely to disturb public tranquillity and peace, cause feelings of enmity, hatred or ill will between different religious, racial, language or regional groups or castes or communities or disrupts the harmony between them or provokes disturbance, causes hurt to the religious sentiments of a community or insults the modesty of a woman or which contain false information about any person or organization or harms such persons life, reputation and/or property, or any material that interferes with the ability of others to enjoy or utilize the Services. We do not tolerate harassment or denigration based on age, gender, race, religion, national origin, sexual orientation or disability;
		• Content or data that would impersonate someone else or falsely represent your identity or qualifications, or that constitutes a breach of any individual's privacy, including posting images about children or any third party without their consent (or a parent's consent in the case of a minor); 
		• Except as otherwise permitted by Bharat Vaani in writing, advertisements, solicitations, investment opportunities, solicitations, chain letters, pyramid schemes, other unsolicited commercial communication or engage in spamming or flooding;
		• Virus, trojan horse, worm or other disruptive or harmful software or data; and
		• Any information, software or content which is not legally yours and may be protected by copyright or other proprietary rights, or derivative works, without permission from the copyright owner or intellectual property rights owner.
		</p>

		<p><strong>Community forums</strong></br>
			The Services may include a community forum or other social features to exchange content and information with other Services and the public users. Please use respect when you interact with other users. Do not reveal information that you do not want to make public. Users may post hypertext links to the content of third parties for which Social Play LLP is not responsible.
		</p>
		<p>Social Play LLP may freely use the feedback you provide. You agree that Bharat Vaani may use your feedback, suggestions, or ideas in any way, including future modifications of the Services, other products or services, advertising or marketing materials. You grant Bharat Vaani a perpetual, worldwide, fully transferable, sublicensable, non-revocable, fully paid-up, royalty-free license to use the feedback you provide to Bharat Vaani in any way.</p>
		<p>Social Play LLP may monitor content on the services but has no obligation to. We may disclose any information necessary to satisfy our legal obligations, protect Bharat Vaani or its customers, or operate the Services properly. Social Play LLP, in its sole discretion, may refuse to post, remove, or refuse to remove, any Content, in whole or in part, alleged to be unacceptable, undesirable, inappropriate, or in violation of this Agreement. You may transmit or publish content created by you using any of the Services or otherwise. However, you shall be solely responsible for such content and the consequences of its transmission or publication. Any content made public will be publicly accessible through the internet and maybe crawled and indexed by search engines. You are responsible for ensuring that you do not accidentally make any private content publicly available. By making any copyrighted/copyrightable content available on any of the Services, you affirm that you have the consent, authorization or permission, as the case may be from every person who may claim any rights in such content to make such content available in such manner. Further, by making any content available in an aforementioned manner, you expressly agree that Bharat Vaani will have the right to block access to or remove such content made available by you if Bharat Vaani receives complaints concerning any illegality or infringement of third-party rights in such content.</p>

		<p><strong>Social Play LLP Use, Storage and Access </strong></br>
			Social Play LLP shall have the right, in its sole discretion and with reasonable notice posted on the Bharat Vaani site and/or sent to you at the Current Administrator's email address provided in the Registration Data, to revise, update, or otherwise modify the Bharat Vaani and establish or change limits concerning use of the Bharat Vaani, temporarily or permanently, including but not limited to (i) the amount of storage space you have on the Bharat Vaani at any time, and (ii) the number of times (and the maximum duration for which) you may access the Bharat Vaani in a given period. Bharat Vaani reserves the right to make any such changes effective immediately to maintain the security of the system or User Access Information or comply with any laws or regulations and provide you with electronic or written notice within thirty (30) days after such change. You may reject changes by discontinuing the use of the Bharat Vaani to which such changes relate. Your continued use of the Bharat Vaani will constitute your acceptance of and agreement to such changes. From time to time, social Play LLP may perform maintenance upon the Bharat Vaani, resulting in interrupted service, delays or errors in the Bharat Vaani. Bharat Vaani will attempt to provide prior notice of scheduled maintenance but cannot guarantee that such notice will be provided.</p>

		<p><strong>Disclaimer of Warranties </strong></br> 
			You expressly understand and agree that the use of the services is at your sole risk. The services are provided on an as-is-and-as-available basis. Social Play LLP expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. Social Play LLP makes no warranty that the services will be uninterrupted, timely, secure, or virus free. Use of any material downloaded or obtained through the use of the services shall be at your discretion and risk, and you will be solely responsible for any damage to your computer system, mobile telephone, wireless device or data that results from the use of the services or the download of any such material. Whether written or oral, no advice or information obtained by you from Social Play LLP, its employees or representatives shall create any warranty not expressly stated in the terms.
		</p>

		<p><strong>Limitation of Liability</strong></br>
			You agree that Social Play LLP shall, in no event, be liable for any consequential, incidental, indirect, special, punitive, or other loss or damage whatsoever or for loss of business profits, business interruption, computer failure, loss of business information, or other loss arising out of or caused by your use of or inability to use the service, even if Social Play LLP has been advised of the possibility of such damage. Your sole and exclusive remedy for any dispute with Social Play LLP related to any of the services shall be termination of such service. In no event shall Social Play LLP entire liability to you in respect of any service, whether direct or indirect, exceed the fees paid you towards such service.
		</p>
		<p><strong>Indemnification</strong></br>
		You agree to indemnify and hold harmless Social Play LLP, its officers, directors, employees, suppliers, and affiliates from and against any losses, damages, fines and expenses (including attorney's fees and costs) arising out of or relating to any claims that you have used the Services in violation of another party's rights, in violation of any law, in violations of any provisions of the Terms, or any other claim related to your use of the Services
		</p>
		<p><strong>Use with your Mobile Device</strong></br>
			Use of these Services may be available through a compatible mobile device Internet access and may require software. You agree that you are solely responsible for these requirements, including any applicable changes, updates and fees, as well as the terms of your agreement with your mobile device and telecommunications provider. BHARAT VAANI MAKES NO WARRANTIES OR REPRESENTATIONS OF ANY KIND, EXPRESS, STATUTORY OR IMPLIED AS TO.</br>
			(i) THE AVAILABILITY OF TELECOMMUNICATION SERVICES FROM YOUR PROVIDER AND ACCESS TO THE SERVICES AT ANY TIME OR FROM ANY LOCATION;</br>
			(ii) ANY LOSS, DAMAGE, OR OTHER SECURITY INTRUSION OF THE TELECOMMUNICATION SERVICES; AND </br>
			(iii) ANY DISCLOSURE OF INFORMATION TO THIRD PARTIES OR FAILURE TO TRANSMIT ANY DATA, COMMUNICATIONS OR SETTINGS CONNECTED WITH THE SERVICES.
		</p>
		<p><strong>Changes</strong></br>
		We reserve the right to change this Agreement at any time, and the changes will be effective when posted through the Services, on our website for the Services or when we notify you by other means. We may also change or discontinue the Services, in whole or in part. Your continued use of the Services indicates your agreement to the changes.</p>
		
		<p><strong>Social Media Sites</strong></br>
		Bharat Vaani may provide experiences on social media platforms such as Facebook®, Instagram, Twitter® and LinkedIn® that enable online sharing and collaboration among anyone who has registered to use them. Any content you post, such as pictures, information, opinions, or any Personal Information that you make available to other participants on these social platforms, is subject to those platforms' Terms of Use and Privacy Policies. Please refer to those social media platforms to better understand your rights and obligations concerning such content.
		</p>

		<p><strong>Third-Party Products and Services</strong></br> 
			Bharat Vaani may tell you about third-party products or services, including via the Service. Bharat Vaani may offer products and services on behalf of third parties who are not affiliated with Bharat Vaani ("Third Party Products") and/or provide access or links to third party websites ("Third Party Sites"). If you decide to use any Third-Party Products or access any Third-Party Sites, you are solely responsible for selecting reviewing separate product terms, website terms and privacy policies. Bharat Vaani is not affiliated with these Third-Party Products or Third-Party Sites. He does not endorse or recommend Third Party Products even if such products are marketed or distributed via our products, website or associated with Bharat Vaani in any way. You agree that the third parties, and not Bharat Vaani, are solely responsible for the Third-Party Product's performance (including technical support), the content on their websites and their use or disclosure of your data. Bharat Vaani will not be liable for any damages, claims or liabilities arising from the third parties, Third Party Products or Third-Party Sites.
		</p>

		<p><strong>Suspension & Termination</strong></br>
			You agree that Social Play LLP may suspend or terminate your user account and access to the Services for reasons including, but not be limited to, breaches or violations of the Terms of the Social Play LLP Privacy Policy, a request by you to terminate your account, discontinuance or material modification to the Services, unexpected technical issues or problems, extended periods of inactivity and requests by law enforcement or other government agencies. Termination of your Bharat Vaani Account includes eliminating access to the Service, deletion of your Account information such as your email ID and password and deletion of data in your user Account as permitted or required by law. In the event of termination by the Social Play LLP without cause, your sole remedy shall be an entitlement to receive a refund of any advance paid or outstanding at that point in time.
		</p>

		<p><strong>Governing Law & Jurisdiction</strong></br>
			Parties agree that these terms shall be governed by and constructed in accordance with the laws of India without reference to conflict of laws principles. Disputes arising in relation hereto shall be subject to the exclusive jurisdiction of the courts at Mumbai.
		</p>

		<p><strong>END OF TERMS OF SERVICE</strong></br>
			If you have any questions or concerns regarding this Agreement, please contact us at <a class="red mail-text" href="mailto:contactus@socialplay.co.in"><strong>contactus(at)socialplay.co.in</strong></a></p>
		</p>
	 		 
	</div>
	<?php include('footer.php'); ?>
</body>
</html>