<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bharat Vaani | Privacy Policy</title>
	<meta name="description" content="Privacy & Policy">
	<style>
		.heading{
			padding: 3% 2% 0% 2% !important;
			text-align: center ! important;
			margin-bottom:4%;
		}
		
		.background-image{
			/*background-image: url("img/C.png");*/
			height:250px; width:150px; float: left;
		}
		
		.pp-row{
			width:80% !important;
		}
		.pp-row p{
			text-align:justify;
			font-size:18px;
		}
		.heading h1{ text-transform:initial; font-size:30px; }
	</style>
</head>
<body>
	<div class="container pp-row">
		<div class="heading">
			<h1>Privacy and Policy</h1>
		</div>
		<p>This Privacy Policy covers Social Play LLP’s Bharat Vaani (collectively, "Bharat Vaani," "we", "us", or "our"). We provide community website through our owned and operated websites, <a href="<?php echo base_url();?>"><strong>www.bharatvaani.in</strong></a> and <a href="<?php echo base_url();?>"><strong>www.bharatvaani.com</strong></a>. By signing up for, downloading, or using our services, you accept this Privacy Policy.</p>

		<p>At Bharat Vaani, your privacy is our topmost priority. We respect your need for privacy and protect any personal information you may share with us. We do not sell/rent/trade your Personal Information to any other site/source.</p>

		<p>If you select the 'Remember Me' option in Bharat Vaani sign in, a permanent cookie will be stored on your computer. This action enables us to maintain and recall your preferences within the website.</p>

		<P>We collect information on the type of content you consume, the people and accounts you interact with, the time and frequency of your interactions to provide a seamless experience while using Bharat Vaani and suggest videos and user profiles that you may be interested in. We also use your information to respond to you when you contact us.</P>

		<p>We post user testimonials on our website. These testimonials may include names and other Personal Information, and we acquire permission from our users before posting these on our website. Bharat Vaani is not responsible for the Personal Information users elect to post within their testimonials.</p>

		<p>Information such as your username, bio, profile picture, website, company name, awards, and videos will be accessible by the public. The videos you have liked, shared, and commented on will also be available to the public.</p>

		<p>Bharat Vaani will not be held liable for any content (videos, descriptions, personal information) that you upload, which may belong to another person. </p>
		
		<p>We automatically collect certain data types when you use our services, regardless of whether you have an account. This data includes your IP address, technical information about your device (e.g., browser type, operating system, basic device information), the web page you visited, the search query you entered before reaching us, and your activities. We may track your activities using cookies and similar technologies. By using our services, you agree to our use of these methods as set forth </p>
		
		<p>A few pages of our website may contain external links. You are advised to verify the privacy practices of such other websites. We are not responsible for the manner of use or misuse of information made available by you at such other websites.</p>
		
		<p>Certain services within Bharat Vaani may require payment. In the case of using your credit card, your Financial Information will not be stored by us. Your Financial Information will be stored in encrypted form on secure servers of the reputed Payment Gateway Service Provider who is beholden to treating your Personal Information in accordance with this Privacy Policy Statement. The payment gateways we use are highly secured (with SSL encryption) and guarantee no data leakage at any point whatsoever.</p>
		
		<p>We retain your data for as long as you have an account. We may retain logs of automatically collected information (for internal analytics and security purposes), your email address, communications with you, and your transactional data. When we no longer have a business reason for retaining data, we will delete it.</p>
		
		<p>Once you delete any information, we may not be able to recover it. If you have previously made any information public, that information or its thumbnail may be discoverable in a search engine's cache for a time. We have no control over search engines.</p>
		
		<p>In case of any suspected illegal activity or potential violation of our Terms and Conditions, we may need to provide access to your Personal Information and the contents of your user account to our employees and service providers. However, Bharat Vaani will ensure that such access complies with this privacy policy statement and is subject to appropriate confidentiality and security measures.</p>
		
		<p>Our privacy policy statement may be updated from time to time in this section. You are requested to visit the page regularly to check it.</p>

		<p><strong>Contact Us</strong></br>
		If you have any questions or concerns, email us at <a class="red mail-text" href="mailto:contactus@socialplay.co.in"><strong>contactus(at)socialplay.co.in</strong></a> - We shall respond to all inquiries within 30 days of receipt upon ascertaining your identity.</p>
		</br>
	</div>
	<?php include('footer.php'); ?>
</body>
</html>