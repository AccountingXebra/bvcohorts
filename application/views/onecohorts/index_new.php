<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Innovator’s community for Connections, Marketplace, Events, Deals, Learning, Resources</title>
<meta name="description" content="Bharat Vaani is a community for innovators. It also acts as a marketplace to do business and lists out their events, deals, challenges and resources, learning through webinars and coaches">
<head>
</head>
<?php include('header.php'); ?>
<style>
	.topvideo {
		width: 450px;
		height: 350px;
		object-fit: cover;
		margin: 23% 0 0 -4%;
		border: 1px solid transparent;
		border-radius: 10px;
		box-shadow: 0 0px 6px 1px rgb(102 102 102 / 80%);
	}
	.topvideo_mb{
		width: 320px;
		height: 250px;
		object-fit: cover;
		margin: 5% 0 0 0;
		border: 1px solid #ccc;
		border-radius: 10px;
	}
	.toph1{ margin-top:18%; }
	.line {
		background: linear-gradient(to right,#bc343a -90%,#fff 80%);
		position: absolute;
		content: '';
		height: 3px;
		width: 500px;
		left: 13%;
		border: none;
	}	
	.bvvideo {
		background: linear-gradient(to right,#ffff 5%,#bc343a 40%,#bc343a 50%,#fff 85%);
		position: absolute;
		content: '';
		height: 3px;
		width: 400px;
		border:none;
		left:23%;
	}
	.intvideo{
		background: linear-gradient(to right,#efefef 5%,#adafae 40%,#adafae 50%,#efefef 85%);
		position: absolute;
		content: '';
		height: 4px;
		width: 400px;
		border:none;
		left:23%;
	}
	::placeholder{ color:#bc343a; font-weight:600; font-size:15px; }
	.journey{ font-size:45px; font-weight:600; padding:6% 0 0 6%; color:#bc343a; }
	.leftimg{ padding:3% 5%; }
	.sec-title2{ color: #bc343a !important; font-weight:600; font-size:18px; margin-top:-20px; }
	.signupbox h4{ font-size:60px; color: #fff; text-align: left; padding: 0 20%; }
	.freeh4{ font-weight: 600; font-size: 30px; padding-top: 2%; }
	@media screen and (max-width: 1280px) and (max-height: 960px) {
		.mediaiconbar .tw_col{ margin:10.2% 0 0 10.1%; }
		.mediaiconbar .link_col{ margin:17.4% 0 0 -0.9%; }
		.mediaiconbar .fb_col{ margin:10% 0 0 -1.2%; }
		.mediaiconbar .in_col{ margin:17.5% 0 0 -0.3%; }
		.mediaiconbar .yt_col{ margin:10.2% 0 0 -1%; }
		.desk-signup-form.imageSide .head-submit {margin-top:12px;}
		.bv-video .topvideo{margin:26% 0 0 -4%;}
		.desk-signup-form.imageSide .head-submit2 {margin-top:15px;}
		.bvSlide .half-circle{ top:15.8%; }
		.bvSlide .half-circle img { margin: -40% 0 0 9%; }
		.bvSlide .carousel-control-prev-icon.visible { width: 45%; }
		.bvSlide .carousel-control-next-icon.visible { width: 64%; }
		.imageSide .head-submit{ margin-left: 9px !important; margin-top: 13px !important; }
	}
	.tw_col{ margin:9% 0 0 13.7%; }
	.link_col{ margin:15.1% 0 0 -2.9%; }
	.fb_col{ margin:8.7% 0 0 -2.8%; }
	.in_col{ margin:15.3% 0 0 -2.3%; }
	.yt_col{ margin:9% 0 0 -2.3%; }
</style>
<body>
	<section class="homesection">
		<div class="desktop-view">
		<div class="row image">
			<div class="col-md-6 head-row">
				<div class="toph1">
					<h1 class="co-h1">Accelerate </br>your Innovation</h1>
					<hr class="line"></hr>
					<p class="co-desc"><strong>Bharat's </strong>first accelerator community for Science and Technology innovators. Our community helps Indian innovators from setting up their business to scaling it.</p>
				</div>	
				<div class="row martop25">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-sm-6"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
						<div class="col-sm-3 topbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">Free Signup</button></div>
					</form>	
				</div>	
			</div>
			<div class="col-md-6 bv-video">
				<!--<img class="sideimg" width="660" height="550" src="<?php echo base_url(); ?>asset/images/cohorts/bvcover.png" alt="Start-up Kit" title="Start-up Kit"/>-->
				<video class="topvideo" src="<?php echo base_url(); ?>asset/images/cohorts/team/home_video.mp4" poster="" frameborder="0" controls></video>
				<hr class="bvvideo"></hr>
			</div>
		</div>
		</div>
		<div class="row mobile-view">
			<div class="col-sm-6 head-row">
				<div>
					<p class="co-h1">Accelerate </br>your Innovation</p>
					<p class="co-desc">Bharat's first accelerator community for Science and Technology innovators. Our community helps Indian innovators from setting up their business to scaling it</p>
				</div>	
				<div class="col-sm-6 head-row">
					<video class="topvideo_mb" src="<?php echo base_url(); ?>asset/images/cohorts/team/home_video.mp4" poster="" frameborder="0" controls></video>
				</div>	
				<div class="row martop25">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-md-12">
							<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
							<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
						</div>
					</form>	
				</div>	
			</div>
		</div>
		<div class="container martop5">
			<div class="row">
				<div class="col-md-12">
					<h2 class="journey">Where are </br>you on your journey</h2>
				</div>	
			</div>	
			<div class="row leftimg">
				<div class="col-md-5 martop25"><img class="sideimg" width="460" height="321" src="<?php echo base_url(); ?>asset/images/cohorts/1-01.png" alt="Start-up Kit" title="Start-up Kit"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div martop25">
					<h4 class="sec-title">Great prototype, don't know the next step?</h4>
					<p class="sec-title2">We will help you with</p>
					<div class="row">
						<div class="col-md-12">
							<p class="hm_p">Attracting the right Investors</p> 
							<p class="hm_p">Setting up your business</p> 
							<p class="hm_p">Finding an office space</p>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="container martop15">
			<div class="row leftimg">
				<div class="col-md-6 feature-div martop25 one">
					<h4 class="sec-title">Is your business not taking off?</h4>
					<p class="sec-title2">We will help you with</p>
					<div class="row">
						<div class="col-md-12">
							<p class="hm_p">Showcasing your products and service at our Marketplace</p> 
							<p class="hm_p">Optimizing your marketing strategy</p> 
							<p class="hm_p">Connecting with industry mentors</p>
						</div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 martop25 two"><img class="sideimg" width="460" height="321" src="<?php echo base_url(); ?>asset/images/cohorts/2-01.png" alt="Start-up Kit" title="Start-up Kit"/></div>
			</div>
		</div>
		<div class="container martop15">
			<div class="row leftimg">
				<div class="col-md-5 martop25"><img class="sideimg" width="460" height="321" src="<?php echo base_url(); ?>asset/images/cohorts/3-01.png" alt="Start-up Kit" title="Start-up Kit"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div martop25">
					<h4 class="sec-title">Struggling to sustain your business?</h4>
					<p class="sec-title2">We will help you with</p>
					<div class="row">
						<div class="col-md-12">
							<p class="hm_p">Creating more noise about your product and services</p> 
							<p class="hm_p">Connecting with customers all over India</p> 
							<p class="hm_p">Creating videos to upscale your growth</p>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="container martop15">
			<div class="row leftimg">
				<div class="col-md-6 feature-div martop25 one">
					<h4 class="sec-title">Need resources to scale up?</h4>
					<p class="sec-title2">We will help you with</p>
					<div class="row">
						<div class="col-md-12">
							<p class="hm_p">Provide funding from investors</p> 
							<p class="hm_p">Guide with correct government schemes</p> 
							<p class="hm_p">Help with raising the deb</p>
						</div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 martop25 two"><img class="sideimg" width="460" height="321" src="<?php echo base_url(); ?>asset/images/cohorts/4-01.png" alt="Start-up Kit" title="Start-up Kit"/></div>
			</div>
		</div>
		
		<div class="container martop10 signupbox">
			<div class="row martop10 midsignup text-center">
				<div class="col-md-7"><h4><strong>WE GOT</br>YOU COVERED</strong></h4></div>
				<div class="col-md-5 desktop-view">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-sm-8"><input class="emailtosign2 comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
						<div class="col-sm-4 midbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit2">Free Signup</button></div>
					</form>	
				</div>	
				<div class="col-md-5 mobile-view">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-md-12 imageSide">
							<input class="emailtosign2 comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
							<button type="submit" id="partner-btn" class="btn tnp-submit head-submit2 topbtnch">FREE SIGNUP</button>
						</div>
					</form>	
				</div>
			</div>	
			<!--<div class="row martop25 head-row mobile-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-md-12 imageSide">
						<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
						<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
					</div>
				</form>	
			</div>-->	
		</div>
		
		<div class="container text-center freeresources">
			<div class="row">
			<div class="col-md-12">
				<h4 class="freeh4">FREE RESOURCES FOR OUR USERS</h4>
			</div>
			</div>
		</div>	
		
		<div class="row trending-video-row lazy-background martop50">
		<div class="col-md-12 trend-12">
		<h2 class="trend-h2 text-center"><label class="trending">Innovator Interviews</h2>
		<div id="inspCarousel" class="carousel slide desk-carousel trend-carousel" data-ride="inspcarousel">
			<div class="carousel-inner">
				<?php foreach($interviews as $keyVid=>$vid){ if($keyVid < 4){?>
				<div class="carousel-item mar-left--25 <?php if($keyVid == 1){?>active<?php } ?>">
					<div class="row mar-top-3">
						<div class="col-md-3"></div>
						<div class="col-md-6 text-center">
							<div class="trending-video-1">
								<div class="video-container most-trend">
									<?php if($vid->interview_url !=""){ ?>
										<a  href="<?php echo base_url();?>index/view_interviews/<?=$vid->url?>">
							    			<video style="width:100%; height:100%; border-radius:10px; object-fit:fill; margin-right: 0px;" class="" src="<?=$vid->video."/".$vid->filename?>" poster="<?=$vid->cover?>" data-src="#" frameborder="0"></video>
											<!-- <iframe class="explainer" src="<?php //echo $vid->interview_url; ?>?loop=1&autoplay=0&rel=0&showinfo=0&color=white&mute=0&controls=1&amp;start=0" style="border-radius: 15px; height: 380px !important; margin: 1% 0 0 0; width: 100%; border: 1px solid #000;" width="100%" height="380" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
										</a>	 
						    			<?php }else{ ?>
											<a  href="<?php echo base_url();?>index/view_interviews/<?=$vid->url?>">
							    				<video id="viewvid" style="height: 380px !important; width: 100% !important; border-radius: 10px; border:1px solid #ccc;"  class="responsive-iframe img-responsive" width="100%" height="380" controls autoplay allowfullscreen><source src="<?php echo $vid[0]->video; ?>/<?php echo $vid[0]->filename; ?>"></video>
											</a>	
										<?php } ?>
									</a>	
									<!-- <a href=""><video class="carausel-picks trendVideocell" src="<?php //echo $vid->video; ?>/<?php //echo $vid->filename; ?>" poster="<?php //echo $vid->cover; ?>" frameborder="0" controls></video>
									</a> -->
									<hr class="intvideo"></hr>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			
			
				
				<?php } }?>
			</div>
			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#inspCarousel" data-slide="prev">
				<span class="carousel-control-prev-icon"><!--img src="<?php echo base_url(); ?>assets/images/sm_icons/left-arrow - 30.png" width="3ss0" height="30" alt="Left Arrow"--></span>
			</a>

			<a class="carousel-control-next" href="#inspCarousel" data-slide="next">
				<span class="carousel-control-next-icon"><!--img src="<?php echo base_url(); ?>assets/images/sm_icons/right-arrow - 30.png" width="30" height="30" alt="Right Arrow"--></span>
			</a>
		</div>
		</div>
		
		<div class="col-xl-12 mbTrending">	
			<div class="col-xl-12">
				<div class="col-xl-12 trending-other-video">
					<div class="row">

						<?php foreach($interviews as $keyVid=>$vid) { if($keyVid < 3){ 
                            $likes=$this->Adminmaster_model->selectData('interview_likes', '*', array('interview_id'=>$vid->id,'interview_like'=>1),'id','DESC');
							$countLikes=count($likes);
							if($countLikes >=0 && $countLikes<=1000){
								$subNo=$countLikes;
								$number=$subNo;
							}else if($countLikes >=1000 && $countLikes<=100000){
								$subNo=$countLikes/1000;
								$number=$subNo."K";
							}else{
								$subNo=$countLikes/100000;
								$number=$subNo."L";
							} 
							$views1=$this->Adminmaster_model->selectData('interview_views', '*', array('interview_id'=>$vid->id),'id','DESC');  
							$countViews=count($views1);
							if($countViews >=0 && $countViews<=1000){
								$subNoV=$countViews;
								$numberView=$subNoV;
							}else if($countViews >=1000 && $countViews<=100000){
								$subNoV=$countViews/1000;
								$numberView=$subNoV."K";
							}else{
								$subNoV=$countViews/100000;
								$numberView=$subNoV."L";
							}
							?>
						<div class="col-xl-4 text-cente videorw">
							<div class="row wrapper">
								<?php if($vid->interview_url !=""){ ?>
									<a  href="<?php echo base_url();?>index/view_interviews/<?=$vid->url?>">
										<video style="width:100%; height:100%; border-radius:10px; object-fit:fill; margin-right: 0px;" class="" src="<?=$vid->video."/".$vid->filename?>" poster="<?=$vid->cover?>" data-src="#" frameborder="0"></video>
										<!-- <iframe class="explainer" src="<?php //echo $vid->interview_url; ?>?loop=1&autoplay=0&rel=0&showinfo=0&color=white&mute=0&controls=1&amp;start=0" style="border-radius: 15px; height: 200px !important; margin: 1% 0 0 0; width: 100%; border: 1px solid #000;" width="100%" height="200" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
									</a>	 
						    	<?php }else{ ?>
									<a  href="<?php echo base_url();?>index/view_interviews/<?=$vid->url?>">
							    		<video id="viewvid" style="height: 200px !important; width: 100% !important; border-radius: 10px; border:1px solid #ccc;"  class="responsive-iframe img-responsive" width="100%" height="200" controls autoplay allowfullscreen><source src="<?php echo $vid[0]->video; ?>/<?php echo $vid[0]->filename; ?>"></video>
									</a>	
								<?php } ?>
								<!-- <a href="">
									<video class="otherVideo youtube editor-vl" src="<?php //echo $vid->video; ?>/<?php //echo $vid->filename; ?>" poster="<?php //echo $vid->cover; ?>" data-src="" frameborder="0" controls></video>
									<div class="playpause"></div>		
								</a>	 -->
							</div>	
							<div class="row trend-desc">
								<a class="colorWhite" href="">
									<!--<p class="video--cat"><label class="trend-cat"></label> | <span class="video--views"><?=$numberView?> Views</span> | <span class="video--likes"> <?=$number?> <img class="icon-like" width="25" height="25" src="<?php echo base_url();?>asset/images/cohorts/thumb.png" alt="Likes" title="Likes" loading="lazy"></span></p> <?php echo base_url(); ?>interviews/<?=$vid->url?>-->
									<p class="trending-desc"><?=$vid->title?></p>
								</a>
							</div>
						</div>
					<?php //} ?>
					
					
					
						
						
					<?php }} ?>
						
					</div>	
				</div>
			</div>
		</div>
		
		</div>
		</div>
		
		<div class="container text-center mediaiconbar desktop-view">
			<div class="row">
			<div class="col-md-12">
				<h4 class="freeh4">JOIN OUR SOCIAL MEDIA</h4>
			</div>
			<div class="col-md-2 tw_col">
				<a href="#" target="_blank"><img width="105" height="107" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/twitter.png" alt="BV Twitter" title="BV Twitter" loading="lazy"></a>
			</div>
			<div class="col-md-2 link_col">
				<a href="#" target="_blank"><img width="115" height="107" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/linkdin.png" alt="BV LinkedIn" title="BV LinkedIn" loading="lazy"></a>
			</div>
			<div class="col-md-2 fb_col">
				<a href="#" target="_blank"><img width="110" height="107" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/fb.png" alt="BV Facebook" title="BV Facebook" loading="lazy"></a>
			</div>
			<div class="col-md-2 in_col">
				<a href="#" target="_blank"><img width="105" height="107" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/insta.png" alt="BV Instagram" title="BV Instagram" loading="lazy"></a>
			</div>
			<div class="col-md-2 yt_col">
				<a href="#" target="_blank"><img width="105" height="107" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/youtube.png" alt="BV YouTube" title="BV YouTube" loading="lazy"></a>
			</div>
			</div>
		</div>	
		
		<div class="container text-center mediaiconbar mobile-view">
			<div class="row">
			<div class="col-md-12">
				<h4 class="freeh4">JOIN OUR SOCIAL MEDIA</h4>
			</div>
			<div class="col-md-12 smicon mb-tw">
				<a href="#" target="_blank"><img width="40" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/twitter.png" alt="BV Twitter" title="BV Twitter" loading="lazy"></a>
			</div>
			<div class="col-md-12 smicon mb-link">			
				<a href="#" target="_blank"><img width="40" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/linkdin.png" alt="BV LinkedIn" title="BV LinkedIn" loading="lazy"></a>
			</div>
			<div class="col-md-12 smicon mb-fb">			
				<a href="#" target="_blank"><img width="40" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/fb.png" alt="BV Facebook" title="BV Facebook" loading="lazy"></a>
			</div>
			<div class="col-md-12 smicon mb-in">	
				<a href="#" target="_blank"><img width="40" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/insta.png" alt="BV Instagram" title="BV Instagram" loading="lazy"></a>
			</div>
			<div class="col-md-12 smicon mb-yt">	
				<a href="#" target="_blank"><img width="40" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/youtube.png" alt="BV YouTube" title="BV YouTube" loading="lazy"></a>
			</div>
			</div>
		</div>	
				
		<div class="container clienttrust lazy-background">
			<!--<div class="row">
				<div class="col-md-12">
					<h2 class="client-trust">What our innovators have to say</h2>
				</div>
			</div>-->
			<div class="row bvSlide">
				
				<main class="hm-gradient">
						<div class="container mt-4">
						<div id="xbcarousel" class="carousel slide carousel-fade mb-5" data-ride="carousel">
							<!--<p class="carousal_us"><hr class="carousal_hr"></p>-->
							<!--<a class="carousel-control-prev" href="#xbcarousel" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#xbcarousel" role="button" data-slide="next">
								<span class="carousel-control-next-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>-->
							<!--<ol class="carousel-indicators">
								<li data-target="#xbcarousel" data-slide-to="0" class="active"></li>
								<li data-target="#xbcarousel" data-slide-to="1"></li>
								<li data-target="#xbcarousel" data-slide-to="2"></li>
							</ol> height280 -->
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item xb-item active">
								<div class="comp-info half-circle"><img width="150" height="150" src="<?php echo base_url(); ?>asset/images/cohorts/anshupriya.png" alt="Anshu Priya" title="Anshu Priya" loading="lazy"></div>
								<div class="middlebv"><p class="inno_image text-center"></p>
								<p class="comp-info text-center whcolor"><span class="founder1">Dr. Anshu Priya</span></p>
								<p class="founder-desc text-center yco whcolor">Researcher</p>
								<h4 class="text-center tosay">WHAT OUR INNOVATORS HAVE TO SAY</h4>
								<p class="companyName text-center">City University of Hong Kong</p>
								<!-- scrollbar srl_y bvScroll-->
								<p class="companyDesc scrollbar srl_y bvScroll">Bharat Vaani is a very nice and novel concept materialized for the Indian scientific community; such kind of initiative was much needed in India. It provides a platform to the Indian scientists and researchers to spread awareness about science and their innovations. This digital platform is unique in itself and is actively involved in dissemination of knowledge to the scientific community, general public, entrepreneurs, and industrial setups. In the wake of digitisation of science, Bharat Vaani has contributed to the dissemination of knowledge beyond conventional academic publishing, conferences, seminars and workshops. The social marketing of the new idea and innovation through Bharat Vaani is not only facilitating the development of a connection between science and society but is also promoting the transformation of innovation into business. Bharat Vaani has potential to bring scientific transformation and will be very helpful in strengthening the Make in India initiative of the Government of India. I wish this initiative reaches great heights of success.</p>
								</div>
							</div>
							<div class="carousel-item xb-item">
								<div class="comp-info half-circle"><img width="150" height="150" src="<?php echo base_url(); ?>asset/images/cohorts/jayaraman.png" alt="Chandrasekaran Jayaraman" title="Chandrasekaran Jayaraman" loading="lazy"></div>
								<div class="middlebv">
								<p class="inno_image text-center"></p>	
								<p class="comp-info text-center whcolor"><span class="founder1">Chandrasekaran Jayaraman</span></p>
								<p class="founder-desc yco text-center whcolor">Founder</p>
								<h4 class="text-center tosay">WHAT OUR INNOVATORS HAVE TO SAY</h4>
								<p class="companyName text-center">Watsan Envirotech Private Limited</p>
								
								<p class="companyDesc bvScroll">It is a good platform for rural Innovators and Social Enterprises like ours, that Bharat Vaani has exposed to the common public through their portal the video interview. I wish it comes in all vernacular languages and reaches out to the last mile rural public so that the benefits of such innovations reach all the needy and poor. We wish Bharat Vaani to continue this journey in more years to come</p>
								</div>
							</div>
							<div class="carousel-item xb-item">
								<div class="comp-info half-circle"><img width="150" height="150" src="<?php echo base_url(); ?>asset/images/cohorts/manish.png" alt="Manish Kothari" title="Manish Kothari" loading="lazy"></div>
								<div class="middlebv">
								<p class="inno_image text-center"></p>	
								<p class="comp-info text-center whcolor"><span class="founder1">Manish Kothari</span></p>
								<p class="founder-desc yco text-center whcolor">Owner and Managing Director</p>
								<h4 class="text-center tosay">WHAT OUR INNOVATORS HAVE TO SAY</h4>
								<p class="companyName text-center">Rhino Machines Private Limited</p>
								
								<p class="companyDesc bvScroll scrollbar srl_y">I wish to acknowledge the work being done by Bharat Vaani in capturing the essence of innovators across India, and having conversations. I was happy to find that the format was open, and the interview evolved as we kept discussing. Being able to share my perspective without any fixed pre-decided framework is probably a differentiator which I did like. At the same time, the young team are open to listening of ideas and moving their activities to the next step, and look forward to their next steps. I have spoken to Erica and Sapna - found them very comfortable and amenable to discuss, and do hope they are able to continue this journey to take the innovations forward.</p>
								</div>
							</div>
							<div class="carousel-item xb-item">
								<div class="comp-info half-circle"><img width="150" height="150" src="<?php echo base_url(); ?>asset/images/cohorts/naman_gupta.jpg" alt="Naman Gupta" title="Naman Gupta" loading="lazy"></div>
								<div class="middlebv">
								<p class="inno_image text-center"></p>	
								<p class="comp-info text-center whcolor"><span class="founder1">Naman Gupta</span></p>
								<p class="founder-desc yco text-center whcolor">Founder</p>
								<h4 class="text-center tosay">WHAT OUR INNOVATORS HAVE TO SAY</h4>
								<p class="companyName text-center">Code Effort Private Limited</p>
								
								<p class="companyDesc">A dynamic platform showcasing deep-rooted and resilient innovations is the key to mass growth and sustainability. Bharat Vaani has done it by building this strong community. Bharat Vaani is a revolution in its space.</p>
								</div>
							</div>
						</div>
						<div class="prnext">
							<div class="col-md-12 sliderbv">
								<a class="carousel-control-prev" href="#xbcarousel" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon lazy-background visible" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#xbcarousel" role="button" data-slide="next">
									<span class="carousel-control-next-icon lazy-background visible" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>	
						</div>	
						</div>
						</div>      
					</main>
			</div>
		</div>
		
		<div class="container martop25">
			<div class="row martop10 midsignup text-center desktop-view">
				<form class="desk-sign-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-sm-8"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
					<div class="col-sm-4 topbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">FREE SIGNUP</button></div>
				</form>	
			</div>	
			<div class="row martop25 head-row mobile-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-md-12">
						<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
						<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
					</div>
				</form>	
			</div>
		</div>
		</br></br>
		
	</section>
</body>
<?php include('footer.php'); ?>
</html>
