<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/contact_partner_us.css">
<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<title>Contact us for more information</title>
<meta name="description" content="Contact Us - One Cohort">
<?php include('header.php'); ?>
<style type="text/css">
	.contactFrom .input-container{ margin-bottom:22px; }
	.contactFrom #contact-btn{ width:85px !important; }
	.contactFrom .error {
		font-family: Arial;
		color: red;
		margin-top: 20px;
		margin-bottom: -15px;
		text-align: center;
		width: 60%;
		padding: 10px 0;
	}
	
	.contactFrom .success {
		font-family: Arial;
		color: green;
		margin-top: 20px;
		margin-bottom: -15px;
		text-align: center;
		width: 35%;
		padding: 10px 0;
		color: #fff;
	}

	.text-danger {
		margin: 5px 0 5px 0;
		float: left;
	}
	.a-link{ padding:1.2% 0px 0px 30px; }
	.contact{ padding-top:4% !important; }
	.bvfoot{ margin-top:5% !important; }
	.contact-header {
		text-align: center;
		font-family: Arial;
		font-size: 20px;
		padding: 40px 0;
		color: grey;
		margin-top: -25px !important;
		margin-bottom: -60px !important;
	}
	
	.fa{
		padding: 10px 0;
		font-size: 41px;
	}
	::placeholder{ color:#000; }
	input:focus::placeholder, textarea:focus::placeholder{
		color: transparent;
	}
	.contactFrom .input-field{ height:40px; }
	.contactFrom .input-field {
		width: 65%;
		padding: 10px;
		outline: none;
		border: 1.5px solid #ccc;
		border-radius: 10px;
		font-size:15px;
	}
	.contactFrom textarea { width: 98%; margin: 0 0 0 4px; padding: 10px 13px; font-size:15px; }
	.g-recaptcha { margin:2% 0 0% 24%; }
	.bvcontactform{ max-width: 500px; margin-left: 29.5%; padding:1% 1% 3% 6%; }
	.sub-btn{ padding: 8px 28px 8px 0px; margin-left: 67%; }
	@media only screen and (max-width: 600px) {
		.contactFrom .bvcontactform{ max-width: 500px; margin: 0 6% 0 3%; padding:1% 1% 3% 6%; }
		.contactFrom .contact-header h1{ margin:8% 0 0 0; }
		.contactFrom .g-recaptcha { margin:2% 0 0% 2%; }
		.contactFrom .sub-btn{ padding: 8px 14px 8px 0px; margin-left: 67%; }
	}
</style>

<div class="contactFrom">
  <div class="contact-header contact">
    <h1 style="font-weight: bold; font-size: 30px;">Contact Us</h1>
    <br>
    <p>We would love to hear from you. While filling the forms below,<br> it would be great if you could specify your purpose of contacting us</p>
  </div>

  <center>
    <p style="color: green; font-family: Arial;">
      <?php if($msg = $this->session->flashdata('msg')): ?>
      <?php echo $msg; ?>
      <?php endif; ?>
    </p>

    <!-- Display the status message -->
    <p style="margin:12px ​0 -10px 0;">
      <?php if(!empty($status)){ ?>
        <div class="status <?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
      <?php } ?>
    </p>
  </center>

  <form class="bvcontactform" action="<?php echo base_url();?>index/contactus_save" method="post">

    <div class="error col-sm-10">
        <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-user icon"></i>-->
      <input class="input-field" type="text" placeholder="NAME" name="name">
    </div>
    
    <div class="error col-sm-10">
      <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-envelope icon"></i>-->
      <input class="input-field" type="text" placeholder="EMAIL" name="email">
    </div>

    <div class="error col-sm-10">
      <?php echo form_error('organisation', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-building icon"></i>-->
      <input class="input-field" type="text" placeholder="ORGANISATION" name="organisation">
    </div>

    <div class="error col-sm-10">
      <?php echo form_error('purpose', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
		<!--<i style="padding: 10px 0; font-size: 41px; width:8%;" class="fa fa-comments icon"></i>
		<input class="input-field" type="text" placeholder="PURPOSE" name="purpose">-->
		<select class="input-field" type="text" name="purpose" aria-required="true" aria-invalid="false" style="background-color:transparent;">
			<option value="">PURPOSE</option>
			<option value="Press Queries">Press Queries</option>
			<option value="Speaking Engagements">Speaking Engagements</option>
			<option value="Partner with Us">Partner with Us</option>
			<option value="Feedback">Feedback</option>
		</select><!--span class="colored">*</span-->
    </div>
    
    <div class="col-75">
      <textarea name="message" placeholder="MESSAGE" style="height:200px"></textarea>
      <?php echo form_error('message', '<div class="text-danger" style="font-family: Arial; font-size: 17px;">', '</div>'); ?>
    </div>
	
	<div class="col-75">
		<div class="g-recaptcha" data-sitekey="6Lf6zdkZAAAAAHIlA8Jjz1keIIlqRnesZI2m7323"></div>
		<span class="input-contact-captcha"></span>	
	</div>

    <div class="btn sub-btn">
      <input id="contact-btn" type="submit" name="contactSubmit" placeholder="SUBMIT">
    </div>

  </form>

  <br><br>
</div>

<div class=" contactFrom" hidden>
  <div class="contact-header">
    <h2 style="font-weight: bold; font-size: 20px;">Contact Us</h2>
    <br>
    <p style="line-height: 1.5;">We would love to hear from you. While filling <br>the forms below, it would be great if you could specify your purpose of contacting us</p>
  </div>

  <center>
    <!--
    <p style="color: green; font-family: Arial;">
      <?php if($msg = $this->session->flashdata('msg')): ?>
      <?php echo $msg; ?>
      <?php endif; ?>
    </p>
    -->

    <!-- Display the status message -->
    <p>
      <?php if(!empty($status)){ ?>
        <div class="status <?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
      <?php } ?>
    </p>
  </center>

  <form class="bvcontactform" action="<?php echo base_url();?>index/contactus_save" method="post" style="max-width: 500px; padding:3% 1% 3% 6%;">

    <div class="error col-sm-10">
        <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <i style="padding: 10px 0; font-size: 41px; width:12%;" class="fa fa-user icon"></i>
      <input style="width:80%;" class="input-field" type="text" placeholder="NAME" name="name">
    </div>
    
    <div class="error col-sm-10">
      <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <i style="padding: 10px 0; font-size: 41px; width:12%;" class="fa fa-envelope icon"></i>
      <input style="width:80%;" class="input-field" type="text" placeholder="EMAIL" name="email">
    </div>

    <div class="error col-sm-10">
      <?php echo form_error('organisation', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <i style="padding: 10px 0; font-size: 41px; width:12%;" class="fa fa-building icon"></i>
      <input style="width:80%;" class="input-field" type="text" placeholder="ORGANISATION" name="organisation">
    </div>

    <div class="error col-sm-10">
      <?php echo form_error('purpose', '<div class="text-danger">', '</div>'); ?>
    </div>
    <div class="input-container">
      <i style="padding: 10px 0; font-size: 41px; width:12%;" class="fa fa-comments icon"></i>
      <input style="width:80%;" class="input-field" type="text" placeholder="PURPOSE" name="purpose">
    </div>
    
    <div class="col-75">
      <textarea name="message" placeholder="MESSAGE" style="height:200px; width: 92%; margin: 0 0 0 5px;"></textarea>
      <?php echo form_error('message', '<div class="text-danger" style="font-family: Arial; font-size: 17px;">', '</div>'); ?>
    </div>
	
	<div class="col-75">
		<div class="g-recaptcha" data-sitekey="6Lf6zdkZAAAAAHIlA8Jjz1keIIlqRnesZI2m7323"></div>
		<span class="input-contact-captcha"></span>	
	</div>

    <div class="btn sub-btn" style="padding: 8px 20px 8px 0px; margin-left: 65.5%;">
      <input id="contact-btn" type="submit" name="contactSubmit" placeholder="SUBMIT">
    </div>

  </form>

  <br><br>
</div>
<?php include('footer.php'); ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>