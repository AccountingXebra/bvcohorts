<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Know more about Bharat's innovation destination</title>
	<meta name="description" content="Know more about Bharat's innovation destination">
	<style>
		.heading{
			padding: 2% 2% !important;
			text-align: center ! important;
			margin-bottom:5px !important;
		}
		.heading h1{ font-size:30px; }
		.pp-row{
			width:80% !important;
			font-size:18px;
		}
		.container.pp-row{ padding:0 !important; }
		.pp-row p{ text-align:justify; }
	</style>
</head>
<body>
	<div class="container pp-row">
		<div class="heading">
			<h1>Our Story</h1>
		</div>
		<p>India is ranked among the top 50 innovative countries globally, and it is no surprise that India's youth is considered one of the most well-informed individuals. Our landscape has shown remarkable growth in innovations. With good schemes and the adoption of new technologies, India now has the third-largest start-up ecosystem in the world.</p>

		<p>Having said that, there is still a considerable hardship that innovators have to face when they start-up. Issues range from prototyping centres to funding, from mentoring to sales and marketing, from hiring a founding team to accessing new-age business software. </p>

		<p>These gaps have led us to start Bharat Vaani – Bharat’s Innovation Destination. We are Bharat’s first digital community for inventors and innovators. It’s a place to draw inspiration, build connections, generate growth, avail mentoring and get funding for your innovation</p>

		<p>We would love to hear from you. If you are an inventor yourself or are aware of one, suggest Bharat Vaani to them.  For more ideas and suggestions, reach out at <a class="mail-text red" href="mailto:contactus@bharatvaani.in"><strong>contactus(at)bharatvaani.in</strong></a></p>
		
		<div class="container-fluid text-center">
		</br>
		<div class="desktop-view">
			<!--<iframe width="1060" height="515" src="https://www.youtube.com/embed/a_VRWqEh1gk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
			<img src="<?php echo base_url();?>asset/images/cohorts/value.png" title="Our Values" alt="Our Values" width="100%" height="460">
			<br><br><br><br>
		</div>
		<div class="mobile-view" style="margin-left: 13px;">
			<!--<iframe style="border-radius: 20px;" width="320" height="250" src="https://www.youtube.com/embed/a_VRWqEh1gk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
			<img style="margin-left: -20%;" src="<?php echo base_url();?>asset/images/cohorts/value.png" title="Our Values" alt="Our Values" width="350" height="180">
		</div>
		</div>
		
		<div class="container-fluid text-center desktop-view">
		<h3 style="font-family: Arial; font-size: 35px; font-weight: bold; color: grey">OUR TEAM</h3>
		<br><br>
		<div class="row teamates" style="font-family: Arial; color: grey; font-size: 17px; font-weight: bold;">
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/nimesh.png" width="100%" height="150" alt="Nimesh Shah" title="Nimesh Shah">
				Nimesh Shah
				<div class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/sapana.png" width="100%" height="150" alt="Sapna Bakshi" title="Sapna Bakshi">
				Sapna Bakshi
				<div class="head-margin"></div>
			</div>
			<!--<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>assets/images/team/akansha.png" width="60%" height="150" alt="Akansha Negi" title="Akansha Negi">
				<br><br>
				Akansha Negi 
				<div class="head-margin"></div>
			</div>-->
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/erica.png" width="92%" height="150" alt="Erica Fernandes" title="Erica Fernandes">
				Erica Fernandes
				<div class="head-margin"></div>
			</div>
			<!--<div class="col-sm-2 width15">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>assets/images/team/pratik.png" width="100%" height="150" alt="Pratik Sawant" title="Pratik Sawant">
				Pratik Sawant
				<div class="head-margin"></div>
			</div>-->
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/sachin_kumar.jpg" width="92%" height="150" alt="Sachin Kumar" title="Sachin Kumar">
				Sachin Kumar
				<div class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/manoj.png" width="100%" height="150"alt="Manoj Bhoir" title="Manoj Bhoir">
				Manoj Bhoir
				<div class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="margin:0 0 15px 0; border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/nitesh.png" width="100%" height="150" alt="Nitesh Chaughule" title="Nitesh Chaughule">
				Nitesh Chaughule
				<div class="head-margin"></div>
			</div>
		</div>		
	</div>

	<div class="container-fluid text-center mobile-view">
		<h3 style="font-family: Arial; font-size: 35px; font-weight: bold; color: grey">OUR TEAM</h3>
		<br><br>
		<div class="row" style="font-family: Arial; color: grey; font-size: 17px; font-weight: bold;">
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/nimesh.png" width="60%" height="150">
				<br><br>
				Nimesh Shah
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/sapana.png" width="60%" height="150">
				<br><br>
				Sapna Bakshi
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
			<!--<div class="col-sm-2">
				<img style="border-radius: 10px" src="<?php echo base_url();?>assets/images/team/akansha.png" width="60%" height="150">
				<br><br>
				Akansha Negi
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>-->
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/erica.png" width="60%" height="150" alt="Erica Fernandes" title="Erica Fernandes">
				<br><br>
				Erica Fernandes
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
			<!--<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>assets/images/team/pratik.png" width="60%" height="150">
				<br><br>
				Pratik Sawant
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>-->
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/sachin_kumar.jpg" width="60%" height="150" alt="Sachin Kumar" title="Sachin Kumar">
				<br><br>
				Sachin Kumar
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/manoj.png" width="60%" height="150">
				<br><br>
				Manoj Bhoir
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
			<div class="col-sm-2">
				<img style="border-radius: 10px; object-fit:contain;" src="<?php echo base_url();?>asset/images/cohorts/team/nitesh.png" width="60%" height="150">
				<br><br>
				Nitesh Chaughule
				<div style="margin-left: 85px; margin-bottom: 20px;" class="head-margin"></div>
			</div>
		</div>
	</div>
	
	<!--<div class="desktop-view">
		<hr>
		<div class="container text-center">
			<div class="row">
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://www.facebook.com/bharatvaani.in/" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 50.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://www.facebook.com/bharatvaani.in/" target="_blank">Facebook</a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://youtube.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 50.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://youtube.com/bharatvaani" target="_blank">Youtube</a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://www.instagram.com/bharatvaani/" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 50.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://www.instagram.com/bharatvaani/" target="_blank">Instagram</a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://twitter.com/bharatvaaniin/" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 50.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://twitter.com/bharatvaaniin/" target="_blank">Twitter</a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 50.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://www.linkedin.com/company/bharatvaani/" target="_blank">Linkedin</a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="ab-icon">
						<a href="https://www.kooapp.com/profile/BharatVaani" target="_blank"><img width="50" height="50" src="<?php echo base_url(); ?>assets/images/sm_icons/koo.png" alt="Bharat Vaani Koo" title="Bharat Vaani Koo"></a>
						<br><br>
						<a style="color: grey; font-family: Arial;" href="https://www.kooapp.com/profile/BharatVaani" target="_blank">Koo</a>
					</div>
				</div>
			</div>
		</div>
	</div>-->
		
		<p class="desktop-view">&nbsp;</p>
		<p class="desktop-view">&nbsp;</p>
	</div>
	<?php include('footer.php'); ?>
</body>
</html>