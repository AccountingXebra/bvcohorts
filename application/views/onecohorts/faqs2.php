<?php include('header.php'); ?>
<title>FAQs on Bharat Vaani Community platform for Inventors, Innovators & Scientists </title>
<meta name="description" content="FAQs on Bharat Vaani’s community that help innovators grow their business, network with other innovators, get offers and resources and funding from the investors">
<style>
	#content{ padding: 0% 0 3% 0; border-bottom: 1px solid #f7f7f7; }
	.faqs-content .faq-header{ background-color:#bc343a; padding:2.5% 0; }
	.faqs-content .faq-header h1{ color:#fff; font-size:32px; }
	.faqs-content .faq-div{ padding: 3% 0 0 0 !important; }
	.faqs-content .card-header{ background-color:transparent !important; border-bottom: none !important; height:60px; }
	.faqs-content .card-body{ line-height:30px; }
	.faqs-content .card-header h5{ color: #bc343a !important; font-weight:400; font-size:18px; }
	.faqs-content .card{ border:none !important; border-bottom: 1px solid #f7f7f7 !important; margin-bottom:10px; }
	.faqs-content .collapse{ margin-top:-20px;  }
	.faqs-content .fa-caret-down { text-align: right; float: right; font-size: 22px; color: #595959; }
	.faqs-content .card11 p { padding: 5px 0; } 
	.a-link{ padding:1% 0px 0px 30px; }
	.bvfoot{ margin-top:5% !important; }
	@media only screen and (max-width: 600px) { 
		.faqs-con { padding-right: 10px !important; padding-left: 10px !important; }
	}
</style>
<div class="wrapper">
    <!-- Page Content -->
	<div class="container-fluid">
	<div id="content" class="faqs-content">
		<div class="row">
			<div class="col-xl-12 faq-header">
				<h1 class="text-center">Frequently Asked Questions</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<div class="container faqs-con my-4">
			<!--Accordion wrapper-->
			<div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
				<!-- Card 1 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne1">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
							<h5 class="mb-0">What is Bharat Vaani? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne1" class="collapse" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
						<div class="card-body">
							India’s first digital community for inventors and innovators to draw inspiration, build connections, generate growth, avail mentoring, and get funding for your innovation. You can set up a marketplace to do business and list out events, deals, challenges, and resources, learning through webinars and coaches. 
						</div>
					</div>
				</div>
				
				<!-- Card 2 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne2">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
							<h5 class="mb-0">How can Bharat Vaani help me grow my business? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne2" class="collapse" role="tabpanel" aria-labelledby="headingOne2" data-parent="#accordionEx">
						<div class="card-body">
							You can build connections, network in the marketplace, list out deals, get funding for your innovation to grow and generate more business for your firm.
						</div>
					</div>
				</div>
				
				<!-- Card 3 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne3">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne3">
							<h5 class="mb-0">Is there any hidden cost in creating a profile? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne3" class="collapse" role="tabpanel" aria-labelledby="headingOne3" data-parent="#accordionEx">
						<div class="card-body">
							No, the community and its features are free of cost. There are certain services like raising funding and customised mentoring which will be paid
						</div>
					</div>
				</div>
				
				<!-- Card 4 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne4">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne4">
							<h5 class="mb-0">Do I have to be an Inventor/Innovator/Scientist to create a profile on Bharat Vaani? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne4" class="collapse" role="tabpanel" aria-labelledby="headingOne4" data-parent="#accordionEx">
						<div class="card-body">
							Yes, only Inventor/Innovator/Scientist, Tech enthusiasts, and Corporates can create a profile. You can set up a marketplace to do business and list out events, deals, challenges, and resources to seek funding for your innovation and learning through webinars and coaches. 
						</div>
					</div>
				</div>
				
				<!-- Card 5 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne5">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne5" aria-expanded="true" aria-controls="collapseOne5">
							<h5 class="mb-0">Why should I create my profile on Bharat Vaani? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne5" class="collapse" role="tabpane1" aria-labelledby="headingOne5" data-parent="#accordionEx">
						<div class="card-body">
							Bharat Vaani is a community platform that bridges the gap between Inventors/Innovators/Scientists with people of similar interests. By signing up on Bharat Vaani, you will be able to connect with valuable people that can help you grow your business. 
						</div>
					</div>
				</div>
				
				<!-- Card 6 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne6">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne6" aria-expanded="true" aria-controls="collapseOne6">
							<h5 class="mb-0">How do I add or remove details from my company’s profile page? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne6" class="collapse" role="tabpanel" aria-labelledby="headingOne6" data-parent="#accordionEx">
						<div class="card-body">
							You can manage your profile by clicking on the Profile dropdown on the top right corner of the page
						</div>
					</div>
				</div>
				
				<!-- Card 7 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne7">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne7" aria-expanded="true" aria-controls="collapseOne7">
							<h5 class="mb-0">Can I add an event that my company is organizing and invite fellow Cohort members? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne7" class="collapse" role="tabpanel" aria-labelledby="headingOne7" data-parent="#accordionEx">
						<div class="card-body">
							Yes, you can add your event details that fellow members can see. However, you can send any personalized invites to them. They can choose to attend if they so like.
						</div>
					</div>
				</div>
				
				<!-- Card 8 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne8">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne8" aria-expanded="true" aria-controls="collapseOne8">
							<h5 class="mb-0">Can the invention or creation be in any category? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne8" class="collapse" role="tabpanel" aria-labelledby="headingOne8" data-parent="#accordionEx">
						<div class="card-body">
							Yes, inventions in all categories are allowed.
						</div>
					</div>
				</div>
				
				<!-- Card 9 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne9">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne9" aria-expanded="true" aria-controls="collapseOne9">
							<h5 class="mb-0"> Do I have to pay to access the deals offered in Bharat Vaani? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne9" class="collapse" role="tabpanel" aria-labelledby="headingOne9" data-parent="#accordionEx">
						<div class="card-body">
							No. These deals are made available to all the members of the Bharat Vaani community. 
						</div>
					</div>
				</div>
				
				<!-- Card 10 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne10">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne10" aria-expanded="true" aria-controls="collapseOne10">
							<h5 class="mb-0">Can I offer deals on my service to Bharat Vaani community members? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne10" class="collapse" role="tabpanel" aria-labelledby="headingOne10" data-parent="#accordionEx">
						<div class="card-body">
							Yes. You need to fill in the details of the deals correctly and generate more business by offering them to fellow members 
						</div>
					</div>
				</div>
				
				<!-- Card 11 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne11">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne11" aria-expanded="true" aria-controls="collapseOne11">
							<h5 class="mb-0">Can I opt-out of the program at any stage <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne11" class="collapse" role="tabpanel" aria-labelledby="headingOne11" data-parent="#accordionEx">
						<div class="card-body">
							Yes. We have ensured that this program doesn’t bind you in any manner
						</div>
					</div>
				</div>
				
				<!-- Card 12 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne12">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne12" aria-expanded="true" aria-controls="collapseOne12">
							<h5 class="mb-0">How can Bharat Vaani help me avail funds for my Innovations? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne12" class="collapse" role="tabpanel" aria-labelledby="headingOne12" data-parent="#accordionEx">
						<div class="card-body">
							Yes, Bharat Vaani will help you identify the right investors and help you prepare the documentation and approach strategy to raise the funds
						</div>
					</div>
				</div>
				
				<!-- Card 13 -->
				<div class="card">
					<div class="card-header" role="tab" id="headingOne13">
						<a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne13" aria-expanded="true" aria-controls="collapseOne13">
							<h5 class="mb-0">How do I send you a suggestion or questions? <i class="fa fa-caret-down rotate-icon"></i></h5>
						</a>
					</div>
					<div id="collapseOne13" class="collapse" role="tabpanel" aria-labelledby="headingOne13" data-parent="#accordionEx">
						<div class="card-body">
							We would love to hear from you! Please contact us at <a class="red mail-text" href="mailto:contactus@bharatvaani.in"><strong>contactus(at)bharatvaani.in</strong></a>contactus@bharatvaani.in and submit your comments, questions or report an issue.
						</div>
					</div>
				</div>
			
			</div>
			<!-- Accordion wrapper -->
			</div>
			</div>
		</div>
		
		<div class="container clienttrust">
			<div class="row">
				<div class="col-md-12">
					<h2 class="client-trust">What our innovators have to say</h2>
				</div>
			</div>
		<div class="row">
				<main class="hm-gradient">
						<div class="container mt-4">
						<div id="xbcarousel" class="carousel slide carousel-fade mb-5" data-ride="carousel">
							<p class="carousal_us">Check what others are saying about us<hr class="carousal_hr"></p>
							<a class="carousel-control-prev" href="#xbcarousel" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#xbcarousel" role="button" data-slide="next">
								<span class="carousel-control-next-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
							<!--<ol class="carousel-indicators">
								<li data-target="#xbcarousel" data-slide-to="0" class="active"></li>
								<li data-target="#xbcarousel" data-slide-to="1"></li>
								<li data-target="#xbcarousel" data-slide-to="2"></li>
							</ol>-->
							<div class="carousel-inner" role="listbox" style="height:280px;">
							<div class="carousel-item xb-item active">
								<p class="companyName">City University of Hong Kong</p>
								
								<p class="companyDesc bvScroll scrollbar" style="overflow-y:scroll;">Bharat Vaani is a very nice and novel concept materialized for the Indian scientific community; such kind of initiative was much needed in India. It provides a platform to the Indian scientists and researchers to spread awareness about science and their innovations. This digital platform is unique in itself and is actively involved in dissemination of knowledge to the scientific community, general public, entrepreneurs, and industrial setups. In the wake of digitisation of science, Bharat Vaani has contributed to the dissemination of knowledge beyond conventional academic publishing, conferences, seminars and workshops. The social marketing of the new idea and innovation through Bharat Vaani is not only facilitating the development of a connection between science and society but is also promoting the transformation of innovation into business. Bharat Vaani has potential to bring scientific transformation and will be very helpful in strengthening the Make in India initiative of the Government of India. I wish this initiative reaches great heights of success.</p>
								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/anshupriya.png" alt="Ganesh Gadakh" title="Ganesh Gadakh" loading="lazy"><span class="founder1">Dr. Anshu Priya</span></p>
								<p class="founder-desc">Researcher</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Watsan Envirotech Private Limited</p>
								
								<p class="companyDesc">It is a good platform for rural Innovators and Social Enterprises like ours, that Bharat Vaani has exposed to the common public through their portal the video interview. I wish it comes in all vernacular languages and reaches out to the last mile rural public so that the benefits of such innovations reach all the needy and poor. We wish Bharat Vaani to continue this journey in more years to come</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/jayaraman.png" alt="Poornima Poojari" title="Poornima Poojari" loading="lazy"><span class="founder1">Chandrasekaran Jayaraman</span></p>	
								<p class="founder-desc">Founder</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Rhino Machines Private Limited</p>
								
								<p class="companyDesc bvScroll scrollbar" style="overflow-y:scroll;">I wish to acknowledge the work being done by Bharat Vaani in capturing the essence of innovators across India, and having conversations. I was happy to find that the format was open, and the interview evolved as we kept discussing. Being able to share my perspective without any fixed pre-decided framework is probably a differentiator which I did like. At the same time, the young team are open to listening of ideas and moving their activities to the next step, and look forward to their next steps. I have spoken to Erica and Sapna - found them very comfortable and amenable to discuss, and do hope they are able to continue this journey to take the innovations forward.</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/manish.png" alt="Vimal Kutmutia" title="Vimal Kutmutia" loading="lazy"><span class="founder1">Manish Kothari</span></p>
								<p class="founder-desc">Owner and Managing Director</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Code Effort Private Limited</p>
								
								<p class="companyDesc">A dynamic platform showcasing deep-rooted and resilient innovations is the key to mass growth and sustainability. Bharat Vaani has done it by building this strong community. Bharat Vaani is a revolution in its space.</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/naman_gupta.jpg" alt="Vimal Kutmutia" title="Vimal Kutmutia" loading="lazy"><span class="founder1">Naman Gupta</span></p>
								<p class="founder-desc">Founder</p>
							</div>
						</div>
						</div>
						</div>      
					</main>
			</div>
		</div>
	</div>
	</div>
</div>
<?php include('footer.php'); ?>