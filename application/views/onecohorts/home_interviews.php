<?php include 'header.php';?>
<title>Interview of Indian Innovators</title>
<meta name="description" content="Interviews">
<style>
	#content{ padding: 0% 0 3% 0; border-bottom: 1px solid #f7f7f7; }
	.interview_row .pp-header{ padding:2.5% 0; }
	.interview_row .pp-header h1{ color:#595959; font-size:32px; }
	.interview_row .pp{ padding: 5px 0; line-height:25px; font-size:18px; text-align: justify; } 
	.interview_row .tc-info{ padding: 0 7%; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.blog-disp .blog-title{
		width:250px;
		margin:15px 0 15px 0%;
		text-align:left;
		line-height:25px;
	}
	.blog-disp .brief-content{
		width:250px;
		margin:0 0 6% 0%;
		text-align:left;
	}
	.playpause {
		background-image:url("<?=base_url('assets/images/overlay.png');?>");
		background-repeat:no-repeat;
		background-repeat: no-repeat;
		width: 40%;
		height: 15%;
		position: absolute;
		left: -18%;
		right: 0%;
		top: -30%;
		bottom: 0%;
		margin: auto;
		background-size: contain;
		background-position: center;
		cursor:pointer;
	}
	.blog-disp{ margin:0 325px 25% 0; }
	@media screen and (-webkit-min-device-pixel-ratio:0){
		::i-block-chrome,.footer-list{ border-top:none !important; } 
	}	
</style>
<div class="wrapper">
    <!-- Page Content -->
	<div class="container-fluid">
	<div id="content" class="interview_row">
		<div class="row">
			<div class="col-xl-12 pp-header">
				<h1 class="text-center">&nbsp;Interviews</h1>
			</div>
		</div>
		<div class="row tc-info">
			<div class="row blog-content">
				<?php 
					if(count($interviews)>0){
						foreach($interviews as $inws){ ?>
						<div class="col-sm-3">
							<div class="blog-disp"><!-- <?php echo base_url();?>interviews/<?=$inws->url?> -->
								<a href="<?php echo base_url();?>login"><video style="width:275px; height:175px; border-radius:10px; object-fit:fill; margin-right: 0px;" class="" src="<?=$inws->video."/".$inws->filename?>" poster="<?=$inws->cover?>" data-src="#" frameborder="0"></video>
						</a>
								<a href="<?php echo base_url();?>login"><!--<div class="playpause"></div>--></a>
						<p class="blog-title"></p>
						<p class="blog-title"><strong>Interview of</strong> - <?=$inws->name?></p>
						<p class="brief-content"><strong>Conducted by</strong> - <?=$inws->conduct?></p>
							</div>	
						</div>
					<?php } }else{ echo "No Interviews Available";} ?>
			</div>	
		</div>
	</div>
	</div>
</div>
<?php include 'footer.php';?>		      