<?php include('header.php'); ?>
<title>Interested to work with us?  Apply Now</title>
<meta name="description" content="Apply - One Cohort">
<script src="https://www.google.com/recaptcha/api.js"></script>
<style type="text/css">

  .applyTo input[type=text], select {
    width: 70%;
    border-radius: 0px;
    padding: 10px 10px;
    margin: 10px 0;
    margin-left: 10px;
    font-size: 14px !important;
    display: inline-block;
    border: 1.5px solid #adadad;
  }

  .applyTo .mobile-view input[type=text], select {
    width: 70%;
    border-radius: 0px;
    padding: 10px 10px;
    margin: 10px 0;
    margin-left: 10px;
    font-size: 14px !important;
    display: inline-block;
    border: 1.5px solid #adadad;
  }

  ::placeholder{ color:#ccc; }

  .applyTo textarea {
    width:70%;
	  height:170px;
    border-radius: 0px;
    padding: 10px 10px;
    margin: 10px 0 10px 10px;
    display: block;
    border: 1.5px solid #adadad;
  }

  .applyTo .mobile-view textarea {
    width: 70%;
    border-radius: 0px;
    padding: 10px 10px;
    margin: 10px 0;
    display: inline-block;
    border: 1.5px solid #adadad;
  }

  .applyTo input[type=submit] {
    border: 0px;
    border-radius: 10px;
    background-color: #bf4b3d;
    color: white;
    font-family: Arial;
    width: 100px !important;
    height: 40px;
  }
  
  .applyTo .job-label{
	  padding:22px 0;
	  text-align:right !important;
  }
  
	.a-link{ padding:1.2% 0px 0px 30px; }
	.applyTo{ padding-top:2% !important; }
	.bvfoot{ margin-top:5% !important; }
	.contact-header {
		text-align: center;
		font-family: Arial;
		font-size: 20px;
		padding: 40px 0;
		color: grey;
		margin-top: -5px;
		margin-bottom: -35px;
	}
	input:focus::placeholder, textarea:focus::placeholder{
		color: transparent;
	}
	.applyTo .heading{ margin-bottom:10px; }
	.red{ color:red; }
	.design{ margin:0 0 0 -6%; }
	.g-recaptcha { margin:0 0 5% 24%; }
	.head-margin h1{ font-size:30px; }
	@media only screen and (max-width: 600px) {
		.mobile-view.applyTo input[type=text], select{ width:100%; }
		.mobile-view.applyTo textarea{ width:100%; margin-left:3%; }
	}
  .upload-error{ color:red; margin-bottom:-5%; font-size: 17px; }
  #resume{ margin-left:11px; }
</style>
<script>
  $(document).ready(function(){
      $(".upload-error").hide();
      $(document).on("change","#resume",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["application/pdf"];
				if($.inArray(fileType, validImageTypes) < 0) {
					$(".upload-error").show();
					$("#resume").val('');
				}else{
          $(".upload-error").hide();
        }
			}
		});
  });
</script>
	<div class="desktop-view applyTo">
		<div style="font-size: 32px; font-family: Arial;" class="head-margin">
			<h1><center>Apply Now</center></h1>
		</div>
		<div class="row martop25">
			<div class="col-sm-12 text-center" style="font-family: Arial; font-size: 20px; color: grey;">
				Thank you for showing interest in Bharat Vaani as your future career partner! </br>You need to fill in the following details for our records and for us to get in touch with you.
			</div>
		</div>
		<center>
			<p style="color: green; font-family: Arial;">
			<?php if($msg = $this->session->flashdata('msg')): ?>
			<?php echo $msg; ?>
			<?php endif; ?>
			</p>
		</center>
		<br>
		<div class="container" style="color: grey;">
		<div class="col-md-12">
			<form method="post" action="<?php echo base_url();?>index/apply_save" enctype="multipart/form-data">
        
        <!--Personal Details -->
        <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Personal Details
          </div>
        </div>
		<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-2">
				<div class="design job-label">
					<label>Your Full Name<span class="red">*</span>:</label>&nbsp
				</div>
			</div>
			<div class="col-sm-5">
				<div class="design">
					<input type="text" name="full_name" placeholder="FULL NAME" value="<?php if(isset($apply['full_name'])){ ?> <?php echo $apply['full_name'] ?> <?php } ?>">
					<?php echo form_error('full_name', '<div class="text-danger">', '</div>');?>
				</div>
			</div>
			<div class="col-sm-2">&nbsp;</div>
		</div>
		</div>
		
		<div class="col-sm-12">
        <div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-2">
				<div class="design job-label">
					<label>Contact Number<span class="red">*</span>:</label>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="design">
					<input type="text" name="contact_number" placeholder="CONTACT NUMBER" value="<?php if(isset($apply['contact_number'])){ ?><?php echo $apply['contact_number']; ?> <?php } ?>">
					<?php echo form_error('contact_number', '<div class="text-danger">', '</div>'); ?>
				</div>
			</div>
			<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-3">&nbsp;</div>
          <div class="col-sm-2">
  			<div class="design job-label">
  				<label>Email ID<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="email" placeholder="EMAIL ID" value="<?php if(isset($apply['email'])){ ?><?php echo $apply['email'] ?><?php } ?>">
              <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>
        <!-- End of Personal Details -->

        </br><hr></br>

        <!--Online Presence -->
        <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Online Presence
          </div>
        </div>
        <br>
		<div class="col-sm-12">
        <div class="row">
  		<div class="col-sm-3">&nbsp;</div>
          <div class="col-sm-2">
  			<div class="design job-label">
  				<label>Your Blog Link<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="blog" placeholder="BLOG LINK" value="<?php if(isset($apply['blog'])){ ?><?php echo $apply['blog'] ?><?php } ?>">
              <?php echo form_error('blog', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Your Linkedin Profile URL<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="linkedin" placeholder="LINKEDIN PROFILE URL" value="<?php if(isset($apply['linkedin'])){ ?><?php echo $apply['linkedin'] ?><?php } ?>">
              <?php echo form_error('linkedin', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <br><br>

		<div class="row heading text-center" style="font-family: Arial; font-size: 20px; margin-bottom:20px;">
          <div class="col-sm-12">
  			<p class="text-center" style="font-size: 17px;"> In case you don't have a Linkedin Profile or have an incomplete one, <br>then upload your CV here (.pdf) </p>
          </div>
        </div>
  		
		<div class="col-sm-12">
        <div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Resume:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design" style="margin-top: 19px !important;">
              <input type="file" name="resume" id="resume" accept="file_extension">
              <?php echo form_error('resume', '<div class="text-danger">', '</div>'); ?>
              <br><br>
              <p class="upload-error">File format is incorrect.</p>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <br><br>

		<div class="col-sm-12">
        <div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Your Facebook Profile URL<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="facebook" placeholder="FACEBOOK PROFILE URL"  value="<?php if(isset($apply['facebook'])){ ?><?php echo $apply['facebook'] ?><?php } ?>">
              <?php echo form_error('facebook', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Your Twitter Profile URL<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="twitter" placeholder="TWITTER PROFILE URL" value="<?php if(isset($apply['twitter'])){ ?><?php echo $apply['twitter'] ?><?php } ?>">
              <?php echo form_error('twitter', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>
        <!-- End of Online Presence -->
        
        <br><hr><br>
      
        <!-- Professional Details -->
        <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Professional Details
          </div>
        </div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Which function would you like to work in? <span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <select name="work_function" style="color: grey">
                  <option value="">SELECT FUNCTION</option>
				  <option value="Digital Marketing" <?php if(isset($apply['work_function']) && $apply['work_function'] =="Digital Marketing"){ ?>selected<?php } ?>>Digital Marketing</option>		
				  <option value="Copywriting" <?php if(isset($apply['work_function']) && $apply['work_function'] =="Copywriting"){ ?>selected<?php } ?>>Copywriting</option>		
				  <option value="Video Editor" <?php if(isset($apply['work_function']) && $apply['work_function'] =="Video Editor"){ ?>selected<?php } ?>>Video Editor</option>		
				  
                  <!--<?php  //if(count($getFun)):?>
                      <?php //foreach($getFun as $fun):?>
                          <option value="<?php //echo $fun->name;?>"><?php //echo $fun->name;?></option>
                      <?php //endforeach;?> 
                  <?php //else:?>

                  <?php //endif;?>-->
              </select>

              <?php echo form_error('work_function', '<div class="text-danger">', '</div>'); ?>
            
            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Preferred work location<span class="red">*</span>:</label>
              </div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <select name="location" style="color: grey">
                <option value="">SELECT LOCATION</option>
				<option value="Mumbai" <?php if(isset($apply['location']) && $apply['location'] =="Mumbai"){ ?>selected<?php } ?>>Mumbai</option>
				<option value="Gurgaon" <?php if(isset($apply['location']) && $apply['location'] =="Gurgaon"){ ?>selected<?php } ?>>Gurgaon</option>
				<option value="Banglore" <?php if(isset($apply['location']) && $apply['location'] =="Banglore"){ ?>selected<?php } ?>>Banglore</option>

                <!--<?php //if(count($getLocation)):?>
                    <?php //foreach($getLocation as $location):?>
                        <option value="<?php //echo $location->location;?>"><?php //echo $location->location;?></option>
                    <?php //endforeach;?>
                <?php //else:?>

                <?php //endif;?>-->
            </select>
            
            <?php echo form_error('location', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>Number of years of relevant experience<span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <select name="experience" style="color: grey">
                <option value="">SELECT EXPERIENCE</option>
				<option value="2" <?php if(isset($apply['experience']) && $apply['experience'] =="2"){ ?>selected<?php } ?>>0-2 Years</option>
				<option value="4" <?php if(isset($apply['experience']) && $apply['experience'] =="4"){ ?>selected<?php } ?>>2-4 Years</option>
				<option value="7" <?php if(isset($apply['experience']) && $apply['experience'] =="7"){ ?>selected<?php } ?>>4-7 Years</option>
				<option value="10" <?php if(isset($apply['experience']) && $apply['experience'] =="10"){ ?>selected<?php } ?>>7-10 Years</option>
				<option value="15" <?php if(isset($apply['experience']) && $apply['experience'] =="15"){ ?>selected<?php } ?>>10-15 Years</option>
            </select>

            <?php echo form_error('experience', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

    <div class="col-sm-12">
		  <div class="row text-center">
        <div class="col-sm-2">&nbsp;</div>
  		  <div class="col-sm-3">
  			  <div class="design job-label">
  				  <label>Tell us how will you be the right person for Bharat Vaani<span class="red">*</span>:</label>
  			  </div>
  		  </div>       
          <div class="col-sm-5">
            <div class="design">
              <textarea rows="10" name="about_self_on_bv" style="color: grey" placeholder="Tell us how will you be the right person for Bharat Vaani..."><?php if(isset($apply['about_self_on_bv'])){ ?><?php echo $apply['about_self_on_bv'] ?><?php } ?></textarea>
              <?php echo form_error('about_self_on_bv', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>
		</div>

        <div class="col-sm-12">
		<div class="row">
  		<div class="col-sm-2">&nbsp;</div>
  		<div class="col-sm-3">
  			<div class="design job-label">
  				<label>So, where did you hear of Bharat Vaani? <span class="red">*</span>:</label>
  			</div>
  		</div>
          <div class="col-sm-5">
            <div class="design">
              <select name="knowbv" style="color: grey">
                <option value="">SELECT OPTION</option>
                <option value="Searched Online" <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Searched Online"){ ?>selected<?php } ?>>Searched Online</option>
				<option value="Read on a website"  <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Read on a website"){ ?>selected<?php } ?>>Read on a website</option>
				<option value="Through a friend" <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Through a friend"){ ?>selected<?php } ?>>Through a friend</option>
				<option value="Read about us" <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Read about us"){ ?>selected<?php } ?>>Read about us</option>
				<option value="Social Media Platform" <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Social Media Platform"){ ?>selected<?php } ?>>Social Media Platform</option>
				<option value="Others" <?php if(isset($apply['knowbv']) && $apply['knowbv'] =="Others"){ ?>selected<?php } ?>>Others</option>
                <!--<?php //if(count($getKnowbv)):?>
                    <?php //foreach($getKnowbv as $knowbv):?>
                        <option value="<?php //echo $knowbv->how;?>"><?php //echo $knowbv->how;?></option>
                    <?php //endforeach;?>
                <?php //else:?>

                <?php //endif;?>-->
            </select>

            <?php echo form_error('knowbv', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
  		<div class="col-sm-2">&nbsp;</div>
        </div>
		</div>

    <div class="col-sm-12">
		  <div class="row text-center">
        <div class="col-sm-2">&nbsp;</div>
  		    <div class="col-sm-3">
  			    <div class="design job-label">
  				    <label>Why do you want to join Bharat Vaani?<span class="red">*</span>:</label>
  			    </div>
  		    </div>       
          <div class="col-sm-5">
            <div class="design">
              <textarea rows="10" name="reason_to_join" style="color: grey" placeholder="Why do you want to join Bharat Vaani?"><?php if(isset($apply['reason_to_join'])){ ?><?php echo $apply['reason_to_join'] ?><?php } ?></textarea>
              <?php echo form_error('reason_to_join', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>
		</div>
        <!-- End of Professional Details -->
		<div class="row">
			<div class="col-sm-5">&nbsp;</div>
			<div class="col-sm-4" style="text-align: right; margin-left: -3.5%;">
				<div class="g-recaptcha brochure__form__captcha" data-sitekey="6Lf6zdkZAAAAAHIlA8Jjz1keIIlqRnesZI2m7323"></div>
				<input type="submit" name="submit" value="SUBMIT">
			</div>
			<div class="col-sm-3">&nbsp;</div>
		</div>

      <br><br>
      </form>
    </div> <!-- Line 60 end -->
  </div> <!--Line 59 end -->
</div>


<div class="mobile-view applyTo">
  <div style="font-size: 40px; font-family: Arial;" class="head-margin">
      <center>Apply Now</center>
  </div>

  <br><br>

  <div class="row">
    <div class="col-sm-12 text-center" style="font-family: Arial; font-size: 20px; color: grey;">
      Thank you for showing interest in <br>Bharat Vaani as your future career partner! <br><br>You need to fill in the following details <br>for our records and for us to get in touch with you.
    </div>
  </div>

  <center>
    <br>
    <p style="color: green; font-family: Arial;">
      <?php if($msg = $this->session->flashdata('msg')): ?>
      <?php echo $msg; ?>
      <?php endif; ?>
    </p>
  </center>

  <hr>
  <br>

  <div class="container" style="color: grey;">
    <div class="col-md-12">
      <form method="post" action="<?php echo base_url();?>index/apply_save" enctype="multipart/form-data">
        
        <!--Personal Details -->
          <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Personal Details
          </div>
        </div>
        
        <br>

        <div class="row">
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="full_name" placeholder="FULL NAME">
              <?php echo form_error('full_name', '<div class="text-danger">', '</div>');?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="contact_number" placeholder="CONTACT NUMBER">
              <?php echo form_error('contact_number', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="email" placeholder="EMAIL ID">
              <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>
        <!-- End of Personal Details -->

<hr><br>

        <!--Online Presence -->
        <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Online Presence
          </div>
        </div>
        
        <br>

        <div class="row">
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="blog" placeholder="BLOG LINK">
              <?php echo form_error('blog', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>

        <div class="row">
      
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="linkedin" placeholder="LINKEDIN PROFILE URL">
              <?php echo form_error('linkedin', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
      
        </div>

      <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
        <div class="col-sm-12">
          <p class="text-center" style="font-size: 16px;"> In case you don't have a Linkedin Profile or have an incomplete one, <br>then upload your CV here (.pdf) </p>
        </div>
      </div>
      
      <div class="row text-center">
        <div class="col-sm-5">
          <div class="design" style="margin-top: 20px !important;">
            <input style="margin-left: 17px;" type="file" name="resume" id="resume" accept="file_extension">
            <?php echo form_error('resume', '<div class="text-danger">', '</div>'); ?>
            <br><br>
            <p style="font-size: 16px;">Please upload only pdf files</p>
          </div>
        </div>
      </div>

        <br>

        <div class="row">
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="facebook" placeholder="FACEBOOK PROFILE URL">
              <?php echo form_error('facebook', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>

        <div class="row">
      
          <div class="col-sm-5">
            <div class="design">
              <input type="text" name="twitter" placeholder="TWITTER PROFILE URL">
              <?php echo form_error('twitter', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
      
        </div>
        <!-- End of Online Presence -->
        
      <hr><br>
      
        <!-- Professional Details -->
        <div class="row heading text-center" style="font-family: Arial; font-size: 20px;">
          <div class="col-sm-12">
            Professional Details
          </div>
        </div>
        
        <br>

        <div class="row">
      
          <div class="col-sm-5">
            <div class="design">
              <select name="work_function" style="color: grey">
                  <option value="">SELECT FUNCTION</option>

                  <?php  if(count($getFun)):?>
                      <?php foreach($getFun as $fun):?>
                          <option value="<?php echo $fun->name;?>"><?php echo $fun->name;?></option>
                      <?php endforeach;?> 
                  <?php else:?>

                  <?php endif;?>
              </select>

              <?php echo form_error('work_function', '<div class="text-danger">', '</div>'); ?>
            
            </div>
          </div>
      
        </div>

        <div class="row">
     
          <div class="col-sm-5">
            <div class="design">
              <select name="location" style="color: grey">
                <option value="">SELECT LOCATION</option>

                <?php if(count($getLocation)):?>
                    <?php foreach($getLocation as $location):?>
                        <option value="<?php echo $location->location;?>"><?php echo $location->location;?></option>
                    <?php endforeach;?>
                <?php else:?>

                <?php endif;?>
            </select>
            
            <?php echo form_error('location', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
      
        </div>

        <div class="row">
      
          <div class="col-sm-5">
            <div class="design">
              <select name="experience" style="color: grey">
                <option value="">SELECT EXPERIENCE</option>

                <?php if(count($getExperience)):?>
                    <?php foreach($getExperience as $experience):?>
                        <option value="<?php echo $experience->years?>"><?php echo $experience->years;?></option>
                    <?php endforeach;?>
                <?php else:?>

                <?php endif;?>
            </select>

            <?php echo form_error('experience', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
      
        </div>

        <div class="row text-center">
          <div class="col-sm-12">
            <div class="design">
              <textarea rows="10" name="about_self_on_bv" style="color: grey" placeholder="Tell us how will you be the right person for Bharat Vaani..."><?php if(isset($apply['about_self_on_bv'])){ ?><?php echo $apply['about_self_on_bv'] ?><?php } ?></textarea>
              <?php echo form_error('about_self_on_bv', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>

        <div class="row">
      <div class="col-sm-3">
        <div class="design job-label">
          <label style="font-size: 16px;">Where did you hear of Bharat Vaani?</label>
        </div>
      </div>
          <div class="col-sm-5">
            <div class="design">
              <select name="knowbv" style="color: grey">
                <option value="">SELECT OPTION</option>
                
                <?php if(count($getKnowbv)):?>
                    <?php foreach($getKnowbv as $knowbv):?>
                        <option value="<?php echo $knowbv->how;?>"><?php echo $knowbv->how;?></option>
                    <?php endforeach;?>
                <?php else:?>

                <?php endif;?>
            </select>

            <?php echo form_error('knowbv', '<div class="text-danger">', '</div>'); ?>

            </div>
          </div>
        </div>

        <div class="row text-center">
          <div class="col-sm-12">
            <div class="design">
              <textarea rows="10" name="reason_to_join" style="color: grey" placeholder="Why do you want to join Bharat Vaani?"><?php if(isset($apply['reason_to_join'])){ ?><?php echo $apply['reason_to_join'] ?><?php } ?></textarea>
              <?php echo form_error('reason_to_join', '<div class="text-danger">', '</div>'); ?>
            </div>
          </div>
        </div>
        <!-- End of Professional Details -->
      
      <div class="row">
      <div class="col-sm-5"></div>
      <div class="col-sm-4" style="text-align: right; margin-left: -15px;">
        <input type="submit" name="submit" value="SUBMIT">
      </div>
      <div class="col-sm-3"></div>
    </div>

      <br><br>
      </form>
    </div> <!-- Line 60 end -->
  </div> <!--Line 59 end -->    
</div>
<?php include('footer.php'); ?>