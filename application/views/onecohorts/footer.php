<section>
	<div class="row footer-list">
		<div class="col-sm-12 pt-3">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-3 logo-footer">
					<img src="<?php echo base_url(); ?>asset/images/cohorts/finalbv.png" alt="Bharat Vaani" title="Bharat Vaani" width="282" height="74" class="ftLogo">
					<p class="built">Built with <span class="builtWith">❤️</span> in India</p>
					<p class="f-img startUpIndia"><img loading="lazy" class="size-medium wp-image-2356 alignleft si-img indistart" title="Startup India" src="<?php echo base_url(); ?>asset/images/cohorts/startup.png" alt="Startup India" width="160" height="50"><!--img loading="lazy" class="alignnone size-medium wp-image-2356 tenthousand-img" title="10000 start ups" src="<?php echo base_url(); ?>asset/images/cohorts/startup10000.png" alt="10000 start ups" width="150" height="65"--></p>
				</div>
				<div class="col-sm-2 ft-2">
					<h6 class="text-ft ftheadh6"><a class="mail-text normallink" href="" target="_blank"><strong>Features</strong></a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Showcase Profile</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Market Place</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Challenges</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Corporate Deals</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Connections</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Events & Webinars</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Industry News</a></h6>
				</div>
				<div class="col-sm-2 ft-2">
					<h6 class="text-ft ftheadh6"><a class="mail-text normallink" href="" target="_blank"><strong>Company</strong></a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>about-us" target="_blank">Our Story</a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>contactus" target="_blank">Contact Us</a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>openings" target="_blank">Current Openings</a></h6>
					<!--<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>" target="_blank">Data Security</a></h6>-->
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>partner-with-us" target="_blank">Partner With Us</a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>faqs" target="_blank">FAQs</a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>terms-and-conditions" target="_blank">Terms & Conditions</a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>privacy-policy" target="_blank">Privacy policy</a></h6>
					<!--<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>apply" target="_blank">Apply To Us</a></h6>-->
				</div>
				<!--<div class="col-sm-2">
					<h6 class="text-ft"><a class="mail-text" href="" target="_blank"><strong>Partners</strong></a></h6>
					<h6 class="text-ft"><a class="mail-text" href="<?php echo base_url();?>" target="_blank">Charteres Accountants</a></h6>
					
				</div>-->
				<div class="col-sm-2 ft-2">
					<h6 class="text-ft ftheadh6"><a class="mail-text normallink" href="" target="_blank"><strong>Resources</strong></a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Start-up Kit</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>govt-scheme" target="_blank" rel="nofollow" >Government Schemes</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>incubator" target="_blank" rel="nofollow" >Incubators List</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>" target="_blank">Mentoring</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>index/interviews" target="_blank" rel="nofollow" >Videos & Interviews</a></h6>
					<h6 class="text-ft"><a class="mail-text normallink" href="<?php echo base_url();?>login" target="_blank">Learning</a></h6>
				</div>
				<div class="col-sm-3 sm-icon">
					<h6><a class="mail-text normallink" href="" target="_blank"><strong>Social Media</strong></a></h6>
					</br>
					<a class="padleft10" href="https://youtube.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>asset/images/cohorts/icon_youtube - 27.png" width="30" height="30" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp;&nbsp;&nbsp;
					<a class="padleft10" href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>asset/images/cohorts/icon_instagram - 27.png" width="30" height="30" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp;&nbsp;&nbsp;
					<a class="padleft10" href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>asset/images/cohorts/icon_facebook - 27.png" width="30" height="30" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp;&nbsp;&nbsp;
					<a class="padleft10" href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>asset/images/cohorts/icon_twitter - 27.png" width="30" height="30" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp;&nbsp;&nbsp;
					<a class="padleft10" href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>asset/images/cohorts/icon_linkedin - 27.png" width="30" height="30" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					<!--<a href="https://www.kooapp.com/profile/BharatVaani" target="_blank" class="ftkoo"><img src="<?php echo base_url(); ?>asset/images/cohorts/koo.png" width="30" height="30" alt="Bharat Vaani Koo" title="Bharat Vaani Koo"></a>-->
				</div>
            </div>
			</div>
        </div>
    </div>
	<div class="copyright row">
		<div class="col-md-12 pt-3">
			<p class="copy-right"><strong>&copy;Copyright <?php echo date("Y"); ?> Bharat Vaani. All Rights Reserved</strong></p>
        </div>
    </div>
	<!-- Whatsapp API -->
	<div class="row">
		<div class="fixed-wa p-4">
			<a href="https://api.whatsapp.com/send?phone=+918928299834"><img width="4%" height="4%" src="<?php echo base_url(); ?>asset/images/cohorts/sm_icons/whatsapp.png" alt="whatsapp" title="whatsapp" class="img-wa"></a>
		</div>
	</div>
	<!-- Whatsapp API Close -->
</section>
<!-- Google Analytics -->
<!--<script async type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/local-ga.js"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-149366484-1');
</script>-->
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-RK1PKQQ7JK"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-RK1PKQQ7JK');
</script>
<script type="text/javascript">
	function base_path(){
		return '<?=base_path()?>';
	}
</script>
<script>
	function myFunction() {
	  var x = document.getElementById("myLinks");
	  if (x.style.display === "block") {
		x.style.display = "none";
	  } else {
		x.style.display = "block";
	  }
	}
	
	document.addEventListener("DOMContentLoaded", function() {
	var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

	if ("IntersectionObserver" in window) {
		let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
			entries.forEach(function(entry) {
				if (entry.isIntersecting) {
					entry.target.classList.add("visible");
					lazyBackgroundObserver.unobserve(entry.target);
				}
			});
		});

		lazyBackgrounds.forEach(function(lazyBackground) {
			lazyBackgroundObserver.observe(lazyBackground);
		});
	}
	});
</script>
