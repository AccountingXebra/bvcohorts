<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--<link rel="preload" as="font" href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900&display=swap' rel='stylesheet' type='text/css'>-->
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/cohort_bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/cohort_home.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/cohort_style.css">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>asset/images/cohorts/title.png"/>
	<script> var base_url = '<?php echo base_url(); ?>';</script>
	<script> var page_url = "<?php echo @$_SERVER['HTTP_REFERER']; ?>";</script>
	<!--<script type='application/ld+json'> 
	{
		"@context": "http://www.schema.org",
		"@type": "Organization",
		"name": "Bharat Vaani",
		"brand": "Bharat Vaani",
		"legalName": "Bharat Vaani",
		"url": "https://bharatvaani.in/",
		"logo": "https://bharatvaani.in/asset/images/cohorts/finalbv.png",
		"description": "Innovator’s community for Connections, Marketplace, Events, Deals, Learning, Resources"
		"potentialAction": {
			"@type": "SearchAction",
			"target": " https://bharatvaani.in/{search_term_string}",
			"query-input": "required name=search_term_string"
		}
	}
	</script>-->
</head>
<body>
	<div class="header desktop-view">
		<a href="<?php echo base_url();?>" class="logo"><img width="282" height="74" alt="Bharat Vaani" title="Bharat Vaani" src="<?php echo base_url(); ?>asset/images/cohorts/finalbv.png"></a>
		<div class="header-right">
			<a target="_blank" class="loginBtn" href="<?php echo base_url(); ?>login">
				LOGIN
			</a>
			<a target="_blank" class="active login-btn" href="<?php echo base_url(); ?>signup">SIGN UP</a>
		</div>
	</div>
	<div class="topnav mobile-view">
		<!-- width="281" height="74" a href="#home" class="active">Logo</a-->
		<a href="<?php echo base_url();?>" class="logo"><img width="282" height="74" alt="Bharat Vaani" title="Bharat Vaani" src="<?php echo base_url(); ?>asset/images/cohorts/finalbv.png"></a>
		<div id="myLinks" class="co-links">
			<a target="_blank" href="<?php echo base_url(); ?>login">LOGIN</a>
			<a target="_blank" href="<?php echo base_url(); ?>signup">SIGN UP</a>
		</div>
		<a href="javascript:void(0);" class="icon" onclick="myFunction()">
			<span class="fa fa-bars"></span>
		</a>
	</div>
</body>
<script type="text/JavaScript" src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/JavaScript" src="<?php echo base_url(); ?>asset/js/popper.js"></script>
<script type="text/JavaScript" src="<?php echo base_url(); ?>asset/js/bootstrap.js"></script>
<script type="text/JavaScript" src="<?php echo base_url(); ?>asset/js/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</html>
