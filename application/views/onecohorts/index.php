<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Innovator’s community for Connections, Marketplace, Events, Deals, Learning, Resources</title>
<meta name="description" content="Bharat Vaani is a community for innovators. It also acts as a marketplace to do business and lists out their events, deals, challenges and resources, learning through webinars and coaches">
<head>
</head>
<?php include('header.php'); ?>
<style>
	.topvideo {
		width: 450px;
		height: 350px;
		object-fit: cover;
		margin: 14% 0 0 0;
		border: 1px solid #ccc;
		border-radius: 10px;
	}
	.topvideo_mb{
		width: 320px;
		height: 250px;
		object-fit: cover;
		margin: 5% 0 0 0;
		border: 1px solid #ccc;
		border-radius: 10px;
	}
</style>
<body>
	<section class="homesection">
		<div class="desktop-view">
		<div class="row image">
			<div class="col-md-6 head-row">
				<div>
					<h1 class="co-h1">An innovator’s journey is a long and lonely one!</h1>
					<p class="co-desc"><strong>Bharat Vaani</strong> is a platform for innovators to leverage the power of a community. It enables the innovators by increasing product visibility, a community to discuss ideas, a marketplace to showcase and sell, mentoring from domain experts and funding from investors..</p>
				</div>	
				<div class="row martop10">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-sm-6"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
						<div class="col-sm-3 topbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">FREE SIGNUP</button></div>
					</form>	
				</div>	
			</div>
			<div class="col-md-6">
				<!--<img class="sideimg" width="660" height="550" src="<?php echo base_url(); ?>asset/images/cohorts/bvcover.png" alt="Start-up Kit" title="Start-up Kit"/>-->
				<video class="topvideo" src="<?php echo base_url(); ?>asset/images/cohorts/team/home_video.mp4" poster="" frameborder="0" controls></video>
			</div>
		</div>
		</div>
		<div class="row mobile-view">
			<div class="col-sm-6 head-row">
				<div>
					<p class="co-h1">An innovator’s journey is a long and lonely one!</p>
					<p class="co-desc">Bharat Vaani is a platform for innovators to leverage the power of a community. It enables the innovators by increasing product visibility, a community to discuss ideas, a marketplace to showcase and sell, mentoring from domain experts and funding from investors</p>
				</div>	
				<div class="col-sm-6 head-row">
					<video class="topvideo_mb" src="<?php echo base_url(); ?>asset/images/cohorts/team/home_video.mp4" poster="" frameborder="0" controls></video>
				</div>	
				<div class="row martop25">
					<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
						<div class="col-md-12">
							<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
							<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
						</div>
					</form>	
				</div>	
			</div>
		</div>
		<div class="container martop5">
			<div class="row">
				<div class="col-md-5 martop25"><img class="sideimg" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage01.png" alt="Start-up Kit" title="Start-up Kit"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div martop50">
					<h4 class="sec-title">Start-up Kit</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="Start-up Kit" title="Start-up Kit" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture9.PNG"></div>
						<div class="col-md-11"><p class="hm_p martp5">Get access to Finance, Legal and HR document formats and templates for free</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Start-up Kit" title="Start-up Kit" width="35" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture10.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Free subscription to Xebra MSME Business Suite, which integrates Business Intelligence, Sales Invoicing, Expense Recording, Asset Tracking, Purchase & Inventory, Accounts Receivable & Payable, Payroll, HRMS, NeoBank, Tax Calculation & Automated Accounting </p></div>	
					</div>
					<!--<div class="row">
						<div class="col-md-1"><img width="35" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Download document formats and templates of Finance, Legal and HR</p></div>	
					</div>-->
				</div>
			</div>
		</div>
		
		<div class="container martop25 lazy-background">
			<div class="row width100">
				<div class="col-md-6 marleft4 feature-div martop50 one">
					<h4 class="sec-title">Funding and Govt Schemes</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="Govt Schemes" title="Govt Schemes" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture7.PNG"></div>
						<div class="col-md-11"><p class="hm_p martop10">We work with you to help raise funds for your innovation</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Govt Schemes" title="Govt Schemes" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="hm_p p-padd"> Organise webinars and mentorship from investors</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Govt Schemes" title="Govt Schemes" width="30" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture22.PNG"></div>
						<div class="col-md-11"><p class="hm_p"> Take advantage of government schemes and policies listed out for your start-up</p></div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 two marleft4"><img class="sideimg" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage02.png" alt="Govt Schemes" title="Govt Schemes"/></div>
			</div>
		</div>
		
		<div class="container martop25 lazy-background">
			<div class="row">
				<div class="col-md-5 marleft4"><img class="sideimg markimg" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage04.png" alt="News & Videos" title="News & Videos"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div martop50">
					<h4 class="sec-title">News & Videos</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="News & Videos" title="News & Videos" width="30" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture22.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Stories of individuals that inspire us</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="News & Videos" title="News & Videos" width="30" height="35" src="<?php echo base_url(); ?>asset/images/cohorts/Capture24.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Highlights of important policies, industry events news through the day</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="News & Videos" title="News & Videos" width="30" height="35" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Watch innovative products & processes that solve real, tangible problems</p></div>	
					</div>
				</div>
			</div>
		</div>
		
		<div class="row trending-video-row lazy-background">
		<div class="col-md-12 trend-12">
		<h2 class="trend-h2 text-center"><label class="trending">Innovator Interviews</h2>
		<div id="inspCarousel" class="carousel slide desk-carousel trend-carousel" data-ride="inspcarousel">
			<div class="carousel-inner">
				<?php foreach($interviews as $keyVid=>$vid){ if($keyVid < 4){?>
				<div class="carousel-item mar-left--25 <?php if($keyVid == 1){?>active<?php } ?>">
					<div class="row mar-top-3">
						<div class="col-md-3"></div>
						<div class="col-md-6 text-center">
							<div class="trending-video-1">
								<div class="video-container most-trend">
									<a href=""><video class="carausel-picks trendVideocell" src="<?php echo $vid->video; ?>/<?php echo $vid->filename; ?>" poster="<?php echo $vid->cover; ?>" frameborder="0" controls></video>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			
			
				
				<?php } }?>
			</div>
			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#inspCarousel" data-slide="prev">
				<span class="carousel-control-prev-icon"><!--img src="<?php echo base_url(); ?>assets/images/sm_icons/left-arrow - 30.png" width="3ss0" height="30" alt="Left Arrow"--></span>
			</a>

			<a class="carousel-control-next" href="#inspCarousel" data-slide="next">
				<span class="carousel-control-next-icon"><!--img src="<?php echo base_url(); ?>assets/images/sm_icons/right-arrow - 30.png" width="30" height="30" alt="Right Arrow"--></span>
			</a>
		</div>
		</div>
		
		<div class="col-xl-12 mbTrending">	
			<div class="col-xl-12">
				<div class="col-xl-12 trending-other-video">
					<div class="row">

						<?php foreach($interviews as $keyVid=>$vid) { if($keyVid < 4){ 
                            $likes=$this->Adminmaster_model->selectData('interview_likes', '*', array('interview_id'=>$vid->id,'interview_like'=>1),'id','DESC');
							$countLikes=count($likes);
							if($countLikes >=0 && $countLikes<=1000){
								$subNo=$countLikes;
								$number=$subNo;
							}else if($countLikes >=1000 && $countLikes<=100000){
								$subNo=$countLikes/1000;
								$number=$subNo."K";
							}else{
								$subNo=$countLikes/100000;
								$number=$subNo."L";
							} 
							$views1=$this->Adminmaster_model->selectData('interview_views', '*', array('interview_id'=>$vid->id),'id','DESC');  
							$countViews=count($views1);
							if($countViews >=0 && $countViews<=1000){
								$subNoV=$countViews;
								$numberView=$subNoV;
							}else if($countViews >=1000 && $countViews<=100000){
								$subNoV=$countViews/1000;
								$numberView=$subNoV."K";
							}else{
								$subNoV=$countViews/100000;
								$numberView=$subNoV."L";
							}
							?>
						<div class="col-xl-3 text-cente videorw">
							<div class="row wrapper">
								<a href="">
									<video class="otherVideo youtube editor-vl" src="<?php echo $vid->video; ?>/<?php echo $vid->filename; ?>" poster="<?php echo $vid->cover; ?>" data-src="" frameborder="0" controls></video>
									<div class="playpause"></div>		
								</a>	
							</div>	
							<div class="row trend-desc">
								<a class="colorBlack" href="">
									<!--<p class="video--cat"><label class="trend-cat"></label> | <span class="video--views"><?=$numberView?> Views</span> | <span class="video--likes"> <?=$number?> <img class="icon-like" width="25" height="25" src="<?php echo base_url();?>asset/images/cohorts/thumb.png" alt="Likes" title="Likes" loading="lazy"></span></p> <?php echo base_url(); ?>interviews/<?=$vid->url?>-->
									<p class="trending-desc"><?=$vid->title?></p>
								</a>
							</div>
						</div>
					<?php //} ?>
					
					
					
						
						
					<?php }} ?>
						
					</div>	
				</div>
			</div>
		</div>
		
		</div>
		
		<div class="row social-row SoMedia lazy-background">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3 connect">
						<p class="text-center commonSign font600">Join Our Social Media</p>
					</div>
					<div class="col-md-9 socialConnect">
						<p><a href="https://twitter.com/bharatvaaniin"><span class="connectimg1"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/icon_twitter.png" alt="BV Twitter" title="BV Twitter" loading="lazy"></span></br><label class="textsocial textsocial1">Twitter</label></a></p>
						
						<p><a href="https://www.linkedin.com/company/bharatvaani"><span class="connectimg1"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/icon_linkedin.png" alt="BV Linkedin" title="BV Linkedin" loading="lazy"></span></br><label class="textsocial textsocial2">LinkedIn</label></a></p>	
						
						<p><a href="https://www.facebook.com/bharatvaani.in"><span class="connectimg3"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/icon_facebook.png" alt="BV FB" title="BV FB" loading="lazy"></span></br><label class="textsocial textsocial3">Facebook</label></a></p>
						
						<p class="insta"><a href="https://www.instagram.com/bharatvaani"><span class="connectimg4"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/icon_instagram.png" alt="BV Instagram" title="BV Instagram" loading="lazy"></span></br><label class="textsocial textsocial3">Instagram</label></a></p>
						
						<p class="mbyt"><a href="https://youtube.com/bharatvaani"><span class="connectimg5"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/icon_youtube.png" alt="BV Youtube" title="BV Youtube" loading="lazy"></span></br><label class="textsocial textsocial3">YouTube</label></a></p>
						
						<p class="kooMar"><a href="https://www.kooapp.com/profile/BharatVaani"><span class="connectimg5"><img width="45" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/koo.png" alt="BV Koo" title="BV Koo" loading="lazy"></span></br><label class="textsocial textsocial5">Koo</label></a></p>
					</div>
				</div>	
			</div>
		</div>

		<div class="container martop25 lazy-background">
			<div class="row">
				<div class="col-md-5 marleft4 martop25"><img class="sideimg lazy-background" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage05.png" alt="Showcase profile" title="Showcase profile"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div marleft4 martop50">
					<h4 class="sec-title">Showcase Profile & Challenges</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="Showcase profile" title="Showcase profile" width="25" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture5.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Create your profile page and showcase your innovation, clients and awards</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="Showcase profile" title="Showcase profile" width="30" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture6.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Show your presence across cities and any events or deals that you are offering</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Showcase profile" title="Showcase profile" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="hm_p">A powerful profile that leads to more connections and funding</p></div>	
					</div>
				</div>
			</div>
		</div>

		<div class="container martop50 lazy-background">
			<div class="row">
				<div class="col-md-6 marleft4 feature-div one martop25">
					<h4 class="sec-title">Innovator’s Community Network</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="Community Network" title="Community Network" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture16.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Network with fellow inventors and build your reach</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="Community Network" title="Community Network" width="25" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture18.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p martop10">Connect your business with that of your clients and vendors</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Community Network" title="Community Network" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p martop10">Use your connections as a sounding board for your ideas</p></div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 two"><img class="sideimg lazy-background" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage03.png" alt="Community Network" title="Community Network"/></div>
			</div>
		</div>		
		
		<div class="container martop25 lazy-background">
			<div class="row">
				<div class="col-md-5 marleft4"><img class="sideimg lazy-background" width="450" height="450" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage06.png" alt="Accelerators & Challenges" title="Accelerators & Challenges"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div martop25">
					<h4 class="sec-title">Accelerators & Challenges</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="Accelerators & Challenges" title="Accelerators & Challenges" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture16.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Get a comprehensive list of incubators and accelerators in India</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Accelerators & Challenges" title="Accelerators & Challenges" width="25" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture18.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Participate in new accelerator challenges and competitions listed out</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Accelerators & Challenges" title="Accelerators & Challenges" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Incubation opportunities across the countries</p></div>	
					</div>
				</div>
			</div>
		</div>
		
		<div class="container martop25 lazy-background">
			<div class="row">
				<div class="col-md-6 marleft4 feature-div martop10 one">
					<h4 class="sec-title">Events</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="Events" title="Events" width="30" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture25.PNG"></div>
						<div class="col-md-11"><p class="hm_p">Participate or create in events, conferences and workshops taking place</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Events" title="Events" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture7.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Organise webinars and promote them within Cohorts for better participation</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Events" title="Events" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture18.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Network with mentors and industry veterans to gain better insights</p></div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 two"><img class="sideimg lazy-background" width="450" height="400" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage09.png" alt="Events" title="Events"/></div>
			</div>
		</div>
		
		<div class="container martop10">
			<div class="row martop10 midsignup text-center desktop-view">
				<div class="col-sm-12"><h4 class="text-center"><strong>Sign Up Now</strong></h4></div>
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-sm-8"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
					<div class="col-sm-4 midbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">FREE SIGNUP</button></div>
				</form>	
			</div>	
			<div class="row martop25 head-row mobile-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-md-12 imageSide">
						<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
						<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
					</div>
				</form>	
			</div>	
		</div>
		
		<div class="container martop50 lazy-background">
			<div class="row">
				<div class="col-md-6 feature-div marleft4 martop50 one">
					<h4 class="sec-title">Marketplace</h4>
					<div class="row">
						<div class="col-md-1"><img alt="Marketplace" title="Marketplace" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture7.PNG"></div>
						<div class="col-md-11"><p class="hm_p martp5">Connect with potential clients, service providers and suppliers</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Marketplace" title="Marketplace" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture11.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Generate leads and reviews for your products & services</p></div>	
					</div>
				</div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-5 two"><img class="markimg sideimg lazy-background" width="450" height="400" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage08.png" alt="Marketplace" title="Marketplace"/></div>
			</div>
		</div>
		
		<!-- Corporate Deals -->
		<div class="container martop50 lazy-background">
			<div class="row">
				<div class="col-md-5 marleft4"><img class="sideimg lazy-background" width="450" height="360" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage10.png" alt="Corporate Deals" title="Corporate Deals"/></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-6 feature-div">
					<h4 class="sec-title">Corporate Deals</h4>
					<div class="row">
						<div class="col-md-1"><img alt="Corporate Deals" title="Corporate Deals" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture12.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Increase trials and revenue by offering deals on our products or services</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img class="lazy-background" alt="Corporate Deals" title="Corporate Deals" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture15.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Cut expenses by taking advantage of the deals provided on the website</p></div>	
					</div>
				</div>
			</div>
		</div>
		
		<div class="container1 martop50">
			<div class="col-sm-12"><h4 class="text-center"><strong>Sign Up Now</strong></h4></div>
			<div class="row martop10 midsignup text-center desktop-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-sm-8"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
					<div class="col-sm-4 midbtnch1"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">FREE SIGNUP</button></div>
				</form>	
			</div>	
			<div class="row martop25 head-row mobile-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-md-12">
						<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
						<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
					</div>
				</form>	
			</div>
		</div>
		
		<div class="container martop50 learn lazy-background">
			<div class="row">
				<div class="col-md-6 feature-div martop50 marleft4 one">
					<h4 class="sec-title">Learning</h4>
					<!--div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div-->
					<div class="row">
						<div class="col-md-1"><img alt="Learninge" title="Learning" width="30" height="30" src="<?php echo base_url(); ?>asset/images/cohorts/Capture25.PNG"></div>
						<div class="col-md-11"><p class="hm_p"> Webinars and interviews with fellow innovators and investors</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Learninge" title="Learning" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture7.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Interpret financial, accounting & business terms to interpret your business better</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img alt="Learninge" title="Learning" width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture18.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Mentoring from industry and domain experts</p></div>	
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5 two martop50"><img class="sideimg lazy-background" width="450" height="400" src="<?php echo base_url(); ?>asset/images/cohorts/bvhpage07.png" alt="Learninge" title="Learning"/></div>
			</div>
		</div>
		
		<!--<div class="container martop50">
			<div class="row">
				<div class="col-md-6 feature-div marleft4 martop25">
					<h4 class="sec-title">Knowledge & Articles</h4>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture12.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Interviews with entrepreneurs that map out their success stories</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>asset/images/cohorts/Capture15.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Interpret financial, accounting & business terms to interpret your business better</p></div>	
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5"><img width="500" height="400" src="<?php echo base_url(); ?>asset/images/cohorts/Capture13.PNG" alt="Discussions" title="Discussions"/></div>
			</div>
		</div>-->
		
		<!--<div class="container martop50">
			<div class="row">
				<div class="col-md-6 feature-div marleft4 martop25">
					<h4 class="sec-title">Discussions</h4>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture12.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Join your industry cohort and discuss the nuances about your sector</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture15.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Shares your success and more importantly failures for all to learn</p></div>	
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5"><img width="500" height="400" src="<?php echo base_url(); ?>assets/images/cohorts/Capture13.PNG" alt="Discussions" title="Discussions"/></div>
			</div>
		</div>
		
		<div class="container martop50">
			<div class="row">
				<div class="col-md-5 marleft4"><img width="500" height="400" src="<?php echo base_url(); ?>assets/images/cohorts/Capture14.PNG" alt="Events" title="Events"/></div>
				<div class="col-md-1"></div>
				<div class="col-md-6 feature-div">
					<h4 class="sec-title">Events</h4>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture16.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Participate or create in events, conferences and workshops taking place</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img width="25" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture18.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Organise webinars and promote it within Cohorts for better participation</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img width="30" height="35" src="<?php echo base_url(); ?>assets/images/cohorts/Capture5.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Network with mentors and industry veterans</p></div>	
					</div>
				</div>
			</div>
		</div>
		
		<div class="container martop50">
			<div class="row">
				<div class="col-md-6 feature-div marleft4">
					<h4 class="sec-title">Business Community Network Module</h4>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture19.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Connect your business with that of your clients and vendors</p></div>	
					</div>
					<div class="row">
						<div class="col-md-1"><img width="30" height="40" src="<?php echo base_url(); ?>assets/images/cohorts/Capture21.PNG"></div>
						<div class="col-md-11"><p class="p-padd hm_p">Convert your vendor’s invoices into expenses with a click</p></div>	
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5"><img width="450" height="350" src="<?php echo base_url(); ?>assets/images/cohorts/Capture17.PNG" alt="Business Community Network Moduleussions" title="Business Community Network Moduleussions"/></div>
			</div>
		</div>-->
		
			<div class="container clienttrust lazy-background">
			<div class="row">
				<div class="col-md-12">
					<h2 class="client-trust">What our innovators have to say</h2>
				</div>
			</div>
			<div class="row">
				
				<main class="hm-gradient">
						<div class="container mt-4">
						<div id="xbcarousel" class="carousel slide carousel-fade mb-5" data-ride="carousel">
							<p class="carousal_us"><hr class="carousal_hr"></p>
							<a class="carousel-control-prev" href="#xbcarousel" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#xbcarousel" role="button" data-slide="next">
								<span class="carousel-control-next-icon lazy-background visible" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
							<!--<ol class="carousel-indicators">
								<li data-target="#xbcarousel" data-slide-to="0" class="active"></li>
								<li data-target="#xbcarousel" data-slide-to="1"></li>
								<li data-target="#xbcarousel" data-slide-to="2"></li>
							</ol>-->
						<div class="carousel-inner height280" role="listbox">
							<div class="carousel-item xb-item active">
								<p class="companyName">City University of Hong Kong</p>
								
								<p class="companyDesc bvScroll scrollbar srl_y">Bharat Vaani is a very nice and novel concept materialized for the Indian scientific community; such kind of initiative was much needed in India. It provides a platform to the Indian scientists and researchers to spread awareness about science and their innovations. This digital platform is unique in itself and is actively involved in dissemination of knowledge to the scientific community, general public, entrepreneurs, and industrial setups. In the wake of digitisation of science, Bharat Vaani has contributed to the dissemination of knowledge beyond conventional academic publishing, conferences, seminars and workshops. The social marketing of the new idea and innovation through Bharat Vaani is not only facilitating the development of a connection between science and society but is also promoting the transformation of innovation into business. Bharat Vaani has potential to bring scientific transformation and will be very helpful in strengthening the Make in India initiative of the Government of India. I wish this initiative reaches great heights of success.</p>
								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/anshupriya.png" alt="Anshu Priya" title="Anshu Priya" loading="lazy"><span class="founder1">Dr. Anshu Priya</span></p>
								<p class="founder-desc">Researcher</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Watsan Envirotech Private Limited</p>
								
								<p class="companyDesc bvScroll scrollbar srl_y">It is a good platform for rural Innovators and Social Enterprises like ours, that Bharat Vaani has exposed to the common public through their portal the video interview. I wish it comes in all vernacular languages and reaches out to the last mile rural public so that the benefits of such innovations reach all the needy and poor. We wish Bharat Vaani to continue this journey in more years to come</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/jayaraman.png" alt="Chandrasekaran Jayaraman" title="Chandrasekaran Jayaraman" loading="lazy"><span class="founder1">Chandrasekaran Jayaraman</span></p>	
								<p class="founder-desc">Founder</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Rhino Machines Private Limited</p>
								
								<p class="companyDesc bvScroll scrollbar srl_y">I wish to acknowledge the work being done by Bharat Vaani in capturing the essence of innovators across India, and having conversations. I was happy to find that the format was open, and the interview evolved as we kept discussing. Being able to share my perspective without any fixed pre-decided framework is probably a differentiator which I did like. At the same time, the young team are open to listening of ideas and moving their activities to the next step, and look forward to their next steps. I have spoken to Erica and Sapna - found them very comfortable and amenable to discuss, and do hope they are able to continue this journey to take the innovations forward.</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/manish.png" alt="Manish Kothari" title="Manish Kothari" loading="lazy"><span class="founder1">Manish Kothari</span></p>
								<p class="founder-desc">Owner and Managing Director</p>
							</div>
							<div class="carousel-item xb-item">
								<p class="companyName">Code Effort Private Limited</p>
								
								<p class="companyDesc">A dynamic platform showcasing deep-rooted and resilient innovations is the key to mass growth and sustainability. Bharat Vaani has done it by building this strong community. Bharat Vaani is a revolution in its space.</p>

								<p class="comp-info"><img width="70" height="70" src="<?php echo base_url(); ?>asset/images/cohorts/naman_gupta.jpg" alt="Naman Gupta" title="Naman Gupta" loading="lazy"><span class="founder1">Naman Gupta</span></p>
								<p class="founder-desc">Founder</p>
							</div>
						</div>
						</div>
						</div>      
					</main>
			</div>
		</div>
		
		<div class="container martop25">
			<div class="col-sm-12"><h4 class="text-center"><strong>Sign Up Now</strong></h4></div>
			<div class="row martop10 midsignup text-center desktop-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-sm-8"><input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required></div>
					<div class="col-sm-4 topbtnch"><button type="submit" id="partner-btn" class="btn tnp-submit head-submit">FREE SIGNUP</button></div>
				</form>	
			</div>	
			<div class="row martop25 head-row mobile-view">
				<form class="desk-signup-form imageSide" action="https://bharatvaani.in/signup" method="GET">
					<div class="col-md-12">
						<input class="emailtosign comp-email tnp-email" type="email" placeholder="Your company email" id="tnp-email" name="ne" required>
						<button type="submit" id="partner-btn" class="btn tnp-submit head-submit topbtnch">FREE SIGNUP</button>
					</div>
				</form>	
			</div>
		</div>
		</br></br>
		<!--<div class="container inthenews martop50">
			<div class="row newssection">				
				<div class="col-md-4 opa-image for-img text-center">
					<img width="200" height="45" src="<?php echo base_url(); ?>asset/images/cohorts/forbes-logo.png" alt="">
				</div>
				<div class="col-md-4 opa-image fn text-center">
					<img width="300" height="20" src="<?php echo base_url(); ?>asset/images/cohorts/The-Financial-Express.png" alt="">
				</div>
				<div class="col-md-4 opa-image faq-img text-center">
					<img width="180" height="60" src="<?php echo base_url(); ?>asset/images/cohorts/Afaqs-logo.png" alt="">
				</div>
				<div class="col-md-4 opa-image ad-img text-center">
					<img width="180" height="60" src="<?php echo base_url(); ?>asset/images/cohorts/adgully.png" alt="">
				</div>
				<div class="col-md-4 opa-image ar-img text-center">
					<img width="200" height="65" src="<?php echo base_url(); ?>asset/images/cohorts/Agency-reporter.png" alt="">
				</div>
				<div class="col-md-4 opa-image medico-img text-center">
					<img width="250" height="65" src="<?php echo base_url(); ?>asset/images/cohorts/medico.png" alt="">
				</div>
			</div>
		</div>-->
		
	</section>
</body>
<?php include('footer.php'); ?>
</html>
