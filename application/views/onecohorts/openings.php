<?php include('header.php'); ?>
<title>Interested to work with us? </title>
<meta name="description" content="Join Our Team - One Cohort">
<style type="text/css">

    .head-margin {
      padding: 15px 30px;
	  margin:2% 0 0 0;
      background-color: #bf4b3d;
      color: white;
    }
	
	.transbox .container{ padding:0px !important; }

    .transbox {
      width: 70%;
      margin: 5px 220px;
      background-color: white;
      border-bottom: 2px dotted #ccc ;
      color: grey;
      font-family: Arial;
    }

    .mobile-view .transbox {
    width: 90%;
    margin: 40px 20px;
    background-color: white;
    border: 1px solid white;
    color: grey;
    box-shadow: 0px 5px 16px 0px rgba(0,0,0,0.3);
    border-radius: 10px;
    font-family: Arial;
}

    input[type=submit] {
		background-color: #bf4b3d;
		color: white;
		width: 40%;
		border-radius: 0px;
		padding: 8px 10px;
		border: none;
		cursor: pointer;
		margin: 0px 200px;
		font-size: 18px;
		box-shadow: 0px 6px 7px 0px rgb(0 0 0 / 30%);
    }

    .mobile-view input[type=submit] {
      background-color: #bf4b3d;
      color: white;
      width: 45%;
      border-radius: 10px;
      padding: 12px 14px;
      border: none;
      cursor: pointer;
      margin: 20px 70px; 
      font-size: 20px;
      box-shadow: 0px 4px 10px 0px rgba(0,0,0,0.3);
    }

    .job-short{
        padding:12px 0px;
    }

    .extend-btn{
      margin-top: 20px;    
      background-color:#fff !important;
      color: grey !important;
      font-size: 20px !important;
      margin-left: 7px;
      font-weight: 500;
      border: 0px;
    }

    .btn:focus, .btn.focus {
      outline: 0;
      box-shadow: none !important;
    }

    .job-mindesc{
        padding-left:27px !important;
        font-size: 17px;
    }

    #job-desc, #job-desc-2, #job-desc-3, #job-desc-4{
      padding-left:10px !important;
      font-size: 17px;
    }
	
	.a-link{ padding:1.2% 0px 0px 30px; }
	.bvfoot{ margin-top:5% !important; }
	.collapse{ margin-top:0px; }
	
	@media only screen and (max-width: 600px) {
		.about .transbox{ margin: 5px 30px; } 	
	}
	
</style>

<div class="about">
    <div style="font-size: 40px; font-family: Arial;" class="head-margin">
        <h1><center>Join Our Team</center></h1>
    </div>

	</br>
    <div class="transbox">
        <div class="container">
            <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc">Senior Content Writer</button>
                
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                   <!-- Support the roll-out of our organization-wide strategic priorities and projects.-->
                </div>
                <div class="col-sm-5 sbtn" style="margin: -5% 0 10px 0;">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a> 
                </div>
            </div>

            <div id="job-desc" class="collapse">
                <strong>Location</strong>: Mumbai, India
                <br><br>
                <strong>Experience</strong>: 4 to 7 years of experience in social media agency is a must.
                <br><br>
                <strong>About the role</strong>:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                <strong>Skills</strong>:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                <strong>Qualification</strong>:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                <strong>Reporting</strong>:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
            <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-2">Video Editor</button>
            
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    <!--Support the roll-out of our organization-wide strategic priorities and projects.-->
                </div>
                <div class="col-sm-5 sbtn" style="margin: -5% 0 10px 0;">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>

            <div id="job-desc-2" class="collapse">
                <strong>Location</strong>: Mumbai, India
                <br><br>
                <strong>Experience</strong>: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                <strong>About the role</strong>:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                <strong>Skills</strong>:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                <strong>Qualification</strong>:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                <strong>Reporting</strong>:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
        <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-3">Social Media Manager</button>
            
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    <!--Support the roll-out of our organization-wide strategic priorities and projects.-->
                </div>
                <div class="col-sm-5 sbtn" style="margin: -5% 0 10px 0;">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>

            <div id="job-desc-3" class="collapse">
                <strong>Location</strong>: Mumbai, India
                <br><br>
                <strong>Experience</strong>: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                <strong>About the role</strong>:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                <strong>Skills</strong>:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                <strong>Qualification</strong>:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                <strong>Reporting</strong>:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
        <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-4">Brand Manager</button>
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    <!--Support the roll-out of our organization-wide strategic priorities and projects.-->
                </div>
                <div class="col-sm-5 sbtn" style="margin: -5% 0 10px 0;">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>
            <div id="job-desc-4" class="collapse">
                <strong>Location</strong>: Mumbai, India
                <br><br>
                <strong>Experience</strong>: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                <strong>About the role</strong>:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                <strong>Skills</strong>:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                <strong>Qualification</strong>:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                <strong>Reporting</strong>:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>
	</br>
</div>


<div class="about" hidden>
    <div style="font-size: 40px; font-family: Arial;" class="head-margin">
        <center>Join Our Team</center>
    </div>


    <div class="transbox">
        <div class="container">
            <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc">Senior Content Writer</button>
                
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    Support the roll-out of our organization-wide strategic priorities and projects.
                </div>
                <div class="col-sm-5">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a> 
                </div>
            </div>

            <div id="job-desc" class="collapse">
                Location: Mumbai, India
                <br><br>
                Experience: 4 to 7 years of experience in social media agency is a must.
                <br><br>
                About the role:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                Skills:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                Qualification:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                Reporting:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
            <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-2">Video Editor</button>
            
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    Support the roll-out of our organization-wide strategic priorities and projects.
                </div>
                <div class="col-sm-5">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>

            <div id="job-desc-2" class="collapse">
                Location: Mumbai, India
                <br><br>
                Experience: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                About the role:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                Skills:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                Qualification:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                Reporting:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
        <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-3">Social Media Manager</button>
            
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    Support the roll-out of our organization-wide strategic priorities and projects.
                </div>
                <div class="col-sm-5">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>

            <div id="job-desc-3" class="collapse">
                Location: Mumbai, India
                <br><br>
                Experience: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                About the role:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                Skills:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                Qualification:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                Reporting:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>


    <div class="transbox">
        <div class="container">
        <button type="button" class="extend-btn" data-toggle="collapse" data-target="#job-desc-4">Brand Manager</button>
            <div class="row job-short">
                <div class="col-sm-7 job-mindesc">
                    Support the roll-out of our organization-wide strategic priorities and projects.
                </div>
                <div class="col-sm-5">
                    <a href="<?php echo base_url();?>index/apply"><input type="submit" name="btn" value="APPLY"></a>
                </div>
            </div>
            <div id="job-desc-4" class="collapse">
                Location: Mumbai, India
                <br><br>
                Experience: 4 to 7 years of experience in social media agency is a must.
                    <br><br>
                About the role:
                <br>
                To think strategically for each and every brand. This should lead to conceptualizing and formulating the exact brand message/campaign for assignments.
                <br>
                Creative/out of the box thinking.
                <br>
                Develop platform specific content for clients and Windchimes Communications.
                <br>
                Creating interesting & engaging content (Articles, Snippets, Contents, Polls, Quizzes, etc).
                <br>
                Work in a team or individually to develop client's content route.
                <br>
                Brainstorming with the team on creative avenues for project execution.
                <br>
                Provide support to the agency by sharing and developing social media research.
                <br>
                Create Case Study of International Campaign.
                <br><br>
                Skills:
                <br>
                Expert in writing skills, editing and proof reading abilities.
                <br>
                Ability to write concise, attention grabbing and hard-hitting copy that puts the message across.
                <br>
                Ability to deliver within short time frames and meet tight deadlines.
                <br>
                Good communication & writing skills in respect to social media platforms.
                <br>
                Writing and developing content for videos, newsletters, campaigns, online ads etc.
                <br>
                Through knowledge of Social Media Platforms.
                <br>
                Ability to work with a team and individually.
                <br><br>
                Qualification:
                <br>
                Candidates with B.A/M.A in English Literature, Mass Communication, Journalism.
                <br>
                Need to be proficient and worked on Social Platforms.
                <br>
                Good knowledge of MS-Office bundle.
                <br><br>
                Reporting:
                <br>
                Head Maven
                <br><br>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>