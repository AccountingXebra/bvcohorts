<?php include 'header.php';?>
<title>List of Bharat's Incubator Centers</title>
<meta name="description" content="Incubation Centers">

	<link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
	#content{ padding: 1% 0 3% 0; border-bottom: 1px solid #f7f7f7; }
	.incubator-content .faq-header{ background-color:#fff; padding:2.5% 0; }
	.incubator-content .faq-header h1{ color:#fff; font-size:32px; }
	.incubator-content .faq-div{ padding: 3% 0 0 0 !important; }
	.incubator-content .card-header{ background-color:transparent !important; border-bottom: none !important; height:60px; }
	.incubator-content .card-body{ line-height:30px; }
	.incubator-content .card-header h5{ color: #bc343a !important; font-weight:400; font-size:18px; }
	.incubator-content .card{ border:none !important; border-bottom: 1px solid #f7f7f7 !important; margin-bottom:10px; }
	.incubator-content .collapse{ margin-top:-20px;  }
	.incubator-content .fa-caret-down { text-align: right; float: right; font-size: 22px; color: #595959; } 
	#incubators_table.dataTable tbody tr { background-color: #fff !important; }
	#incubators_table_length{
		border:1px solid #B0B7CA !important;
		height:35px;
		border-radius:4px;
		width:125px;
		margin-top:10px;
		margin-left:48%;
		background-color:#f4f4f4;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field{border:none; border-bottom:1px solid #595959;}
	.select2-container--open .select2-dropdown--below{ width:166px !important; }
	.select2-search--dropdown{ padding:0px !important; }
	#incubators_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:5px !important;
	}

	#incubators_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-55% !important;
	}
	
	#incubators_table_length select{
		border: none;
		background-color: transparent;
		padding: 8px 8px;
		font-size: 14px;
	}
	
	table.dataTable.display tbody tr td:last-child {
		border: 1px solid #70aaab !important;
	}
	
	table thead{ display:none !important; }
	
	a.showmore {
		color: #ff7d9a !important;
		text-decoration: underline;
		text-align: center;
		font-size: 12px !important;
		padding-top: 15px;
	}	
	.dataTables_scrollHead{
		height:0px !important;
		display:none;
	}
	.select-wrapper {
		margin-right: 5px !important;
	}
	#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 25px 0 10px 0 !important;
		}
	.dataTables_scrollBody{
		height:auto !important;	
	}
	#incubator_table_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:90px;
		margin-top:5px;
		margin-left:52%;
	}

	#incubator_table_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#incubator_table_length .dropdown-content {
		min-width: 90px;
		margin-top:-50% !important;
	}

	#incubator_table_length .select-wrapper span.caret {
		margin: 17px 5px 0 0;
	}
	table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{ background-color:transparent !important; }
	table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{ background-color:transparent !important; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.dataTables_scrollBody{ overflow:hidden !important; }
	.title-name{ text-align: left; padding: 1.5% 0 4% 15.5%; font-size: 25px; }
	
	.incubator-content .dropdown-toggle::after{ border-top: 0.5em solid #2da5a6; }
	.incubator-content .dropdown-menu.show{ background-color: #bfdee0; min-width: 12rem; margin-left: 5%; }
	.incubator-content .dropdown-item:hover{ background-color: #bfdee0; color:#2fa5a7; }
	.incubator-content  .dropdown-item:active{ background-color: #bfdee0; color:#2fa5a7; }
	.incubator-content a.dropdown-item { height: auto; padding: 0% 5% !important; color: #010000; font-size: 13px !important; font-weight: 600; text-align:left;}
	.incubator-content .select2-selection--single{ width: 95%; padding: 8px 5px; border-radius: 6px; font-size:14px; }
	.incubator-content .select2-container--default .select2-selection--single{ border:1px solid #ccc !important; height:35px; }
	.incubator-content .select2-container--default .select2-selection--single .select2-selection__rendered{ line-height:18px; }
	.select2-container{ width:175px !important; }
	#incu_country.select2-dropdown { margin: -13px 0 0 -1px !important; }
	#incu_city{ margin: 0 2% 0 1%;}
	#incu_country{ margin: 0 0% 0 1%; }
	.incubator-content .select2-container--default .select2-selection--single .select2-selection__arrow b{ top:60%; left:10%; }
	.midaddress{ margin:-20px 0 0 0; }
	.city-inc{ margin-left: -12px; }
	.contactInc{ margin-left:2.8%; }
	@media only screen and (max-width: 600px) { 
		.midaddress{ margin:0px 0 0 0 !important; }
		.city-inc{ margin: 0 0 5% 0px !important; }
		.contactInc{ margin:15px 0 0 -15px !important; }
		#incubators_table_wrapper{ margin: 15px -15px !important; }
		.selectincu{ margin-left:16px; }
		.dataTables_wrapper .dataTables_paginate .paginate_button { padding: 0.5em 0.5em !important; }
		#incubators_table_length { margin-left:34% !important; }
		.title-name{ text-align:center !important; }
		.selectincu .resetRow{ margin:0 6% -5px 0; }
	}
	.resetRow{ float:right; margin:0 2% -5px 0; }
	.resetRow a{ color:#bc343a; font-size:15px; text-decoration:underline; }
	#incubators_table p{ font-size:14px !important; margin-bottom:0px !important;	}
	.inTable{ margin: 0 0 0 3.9%; width: 95.9%; }
	.bulk{ border-radius:10px; }
	.city{ margin: 3.2% 0; }
	.addr{ line-height: 25px; margin:6% 0 4% 0; }
	.offer-time{ margin:-10px 0 0 7px; background-color:#50A3A4; border-radius:5px; width:97%; padding:15px; }
	.dataTables_scrollBody{ max-height:100% !important; }
	@media screen and (-webkit-min-device-pixel-ratio:0){
		::i-block-chrome,.incubator-content select{ width:20%; }
		::i-block-chrome,.selectincu{ margin:3% 0% 1% 0; }
		::i-block-chrome,.resetRow{ margin:-3% 2% 0 0; }
		::i-block-chrome,.city{ margin: 1% 0; }
		::i-block-chrome,.addr{ line-height: 25px; margin:1% 0 4% 0; }
		::i-block-chrome,.offer-time{ margin:9% 0 0 7px; background-color:#50A3A4; border-radius:5px; width:97%; padding:15px; height:45px; }
		::i-block-chrome,.dataTables_scrollBody{ max-height:100% !important; }
	}
</style>
<div class="wrapper">
    <!-- Page Content -->
	<div class="container-fluid">
	<div id="content" class="incubator-content">
		<div class="row">
			<div class="col-lg-4"> <h1 class="title-name">Incubation Centers</h1> </div>
			<div class="col-lg-8 selectincu" style="float:right;">
				<div class="row">
					<div class="col-lg-12"> 
						<div class="row resetRow">
							<a href="javascript:void(0);" class="addmorelink right" onclick="reset_incubationfilter();" title="Reset all">Reset</a>
						</div>
					</div>
				</div>
				<div class="row" style="float:right; margin:5px 1% 0 0;">
				<select id="incu_name" name="incu_name" class="incu_select">
					<option value="">NAME</option>
					<?php foreach($name as $na){?>
                      <option value="<?=$na->name?>"><?=$na->name?></option>
					<?php } ?>
				</select>
				<select id="incu_country" name="incu_country" class="incu_select">
					<option value="">COUNTRY</option>
					<option value="">ALL</option>
					<?php foreach($countries as $country){
                           $countryname=$this->Adminmaster_model->selectData('countries', '*',array('country_id'=>$country->country));
					 ?>
						<option value="<?=$country->country?>"><?=$countryname[0]->country_name;?></option>
					<?php } ?>
				</select>
				<select id="incu_city" name="incu_city" class="incu_select">
					<option value="">ALL</option>
					<?php foreach($cities as $city){
                           $cityname=$this->Adminmaster_model->selectData('cities', '*',array('city_id'=>$city->city));
					 ?>
						<option value="<?=$city->city?>"><?=$cityname[0]->name;?></option>
					<?php } ?>
				</select>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-12 inTable">
					<table id="incubators_table" class="responsive-table display table-type1" cellspacing="0">
						<thead id="fixedHeader" style="display:none;">
							<tr>
								<!--th style="width:15%;"></th>
								<th style="width:80%;"></th-->
								<th style="width:100%;"></th>
							</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/list.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>	
<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<script>
	$(document).ready(function(){
		$("#incu_name").select2({  });	
		$("#incu_country").select2({  });	
		$("#incu_city").select2({  });	
	});
	function reset_incubationfilter(){
		$('.incu_select').val('').trigger("change");
		myincubatortable(base_path()+'index/get_incubators/','incubators_table');
		$('select').material_select();
	}
	function showmorelist(id){
		if($("#showmorelist"+id).hasClass('active')){
			$("#offer-time"+id).hide();
			$("#showmorelist"+id).removeClass('active');
			$('#less'+id).show();
			$('#showmorelist'+id).hide();
			$('#morelist'+id).show();
			$('#morelist1'+id).show();
		}else{
			$("#offer-time"+id).show();
			$("#showmorelist"+id).addClass('active');
			$('#showmorelist'+id).show();
			$('#less'+id).hide();
			$('#morelist'+id).hide();
			$('#morelist1'+id).hide();
		}
	}
</script>
<?php include 'footer.php';?>		      