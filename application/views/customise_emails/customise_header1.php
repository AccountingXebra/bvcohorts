<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-tap-highlight" content="no">

	<meta name="description" content="">

	<meta name="keywords" content="">

	<title>Xebra</title>

	<!-- Favicons-->

	<!-- <link rel="icon" href="../../images/favicon/favicon-32x32.png" sizes="32x32"> -->

	<!-- Favicons-->

	<!-- <link rel="apple-touch-icon-precomposed" href="../../images/favicon/apple-touch-icon-152x152.png"> -->

	<!-- For iPhone -->

	<meta name="msapplication-TileColor" content="#00bcd4">

	<!-- <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png"> -->

	<!-- For Windows Phone -->

	<!-- CORE CSS-->

	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">

	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />
    
    


    <?php /*?><link href="<?php echo base_url();?>asset/css/css/style-form.css" type="text/css" rel="stylesheet"><?php */?>

    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">
    
  <!--Import Google Icon Font-->

  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">

 

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    

    <link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">
  
        <script>var base_url = '<?php echo base_url(); ?>';</script>

		<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <style type="text/css">
    .loading_data {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
        background-size: 6%;
		opacity: .9;
    }
    .dropdown-select-wrapper {
        margin-left: 160px;
    }
    nav .nav-wrapper {
      border-bottom: 1px solid rgb(229,235,247);
    }
    </style>
</head>

<body class="layout-semi-dark white">
<div class="loading_data"></div>
    <!-- Start Page Loading -->

   <!-- <div id="loader-wrapper">

		<div id="loader"></div>

		<div class="loader-section section-left"></div>

		<div class="loader-section section-right"></div>

    </div> -->

		<?php
      	  //$current_user =  $this->session->userdata('aduserData');
        ?>

    <header id="header" class="page-topbar">

      <!-- start header nav-->

      <div class="navbar-fixed">

        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">

          <div class="nav-wrapper">

            <div class="dropdown-select-wrapper">

              <div class="company_dropdown">

                <select class="company_dropdown_select companyname" id="header_company_profiles" name="header_company_profiles">
               		  <!-- <option value="" >Entire Company<option> -->
        					<?php if($this->session->userdata['user_session']['reg_id']) {

           
        						$company_profiles = $this->Adminmaster_model->selectData('businesslist','*',array('reg_id'=>$this->session->userdata['user_session']['reg_id'],'status'=>'Active'),'bus_id','ASC');

        						  foreach($company_profiles as $company) {
        						  ?>

                  <option value="<?php echo $company->bus_id; ?>" <?php echo ($company->bus_id == $this->session->userdata['user_session']['bus_id'])?'selected':''; ?>><?php echo $company->bus_company_name; ?></option>

                  <?php } }  ?>

                </select>

              </div>

              <div class="gst_dropdown">

                <select class="gst_dropdown_select companyname" id="branch_gst_list" name="branch_gst_list">

                  
                  <?php if($this->session->userdata['user_session']['bus_id']) {

                    
                    $gst_number = $this->Adminmaster_model->selectData('gst_number','*',array('bus_id'=>$this->session->userdata['user_session']['bus_id']));
                    if($gst_number!=false){
                      foreach($gst_number as $gst) {
                       /*$result = mb_substr($gst->gst_no, 0, 7).'...';*/
                      ?>

                  <option value="<?php echo $gst->gst_id; ?>" ><?php echo "GSTIN: ".$gst->gst_no.' - '.$gst->place; ?></option>

                  <?php } ?>
                     ?> <option value="Combine">Combine</option>
                  <?php } }  ?>
                    
                </select>

              </div>

              <div class="header-search-wrapper hide sideNav-lock">

                <i class="material-icons">search</i>

                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search" />

              </div>

            </div>

            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <!--<?php // if($current_user['aduserprofilephoto'] != '') {?>
                      <img src="<?php echo base_url();?>uploads/profile_images/<?php // echo $current_user['aduserprofilephoto']; ?>" alt="">
                    <?php //} ?>-->
				        	 <img src="<?php echo base_url();?>asset/images/user.png" alt="user">
              
                  </span>
                  <span class="user-name">Hi <span class="user-display-name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?><?php // if(strlen($current_user['aduser_nm']) < 8){ echo $current_user['aduser_nm']; }else{ echo mb_substr($current_user['aduser_nm'], 0, 7); } ?></span></span>
                  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light set notification-button" data-activates="notifications-dropdown">
                  <i class="notifications-icons"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light setting-button" data-activates="setting-dropdown">
                  <i class="settings-icons"></i>
                </a>
              </li>
              <!-- <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light community-button">
                  <i class="community-icons"></i>
                  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>
              <li> -->
                <a href="javascript:void(0);" onclick="showSearch()" class="waves-effect waves-block waves-light search-button">
                  <i class="search-icons"></i>
                </a>
              </li>
            </ul>

            <!-- translation-button -->

            <ul id="translation-dropdown" class="dropdown-content">

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-gb"></i> English</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-fr"></i> French</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-cn"></i> Chinese</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-de"></i> German</a>

              </li>

            </ul>
			<!-- setting-dropdown -->
            
            <ul id="setting-dropdown" class="dropdown-content">

               <li>

                <a href="<?php echo  base_url();?>My_alerts">
                  <i class="menu-round-icon"></i>
                  <span>My Alerts</span>
                </a>

              </li>
              
            </ul>
            
            <!-- --->
            <!-- notifications-dropdown -->

            <ul id="notifications-dropdown" class="dropdown-content">

              <li>

                <h6>Notifications

                  <span class="new badge">1</span>

                </h6>

              </li>

              <li class="divider"></li>
              
              <?php 
			  
			 /*  $admin_user = $this->Common_model->get_single('EI_users',array('id'=>$current_user['aduser_id'],'cmpid'=>$current_user['aduser_cmpid'],'admintype !='=>3,'is_trial'=>1));
		  	$today=date('Y-m-d H:i:s');
		  	if($admin_user != '') {
			 $created_date = $admin_user['created_date'];
			 $date_diff=strtotime($today) - strtotime(date("Y-m-d H:i:s", strtotime($created_date)));
			 $days=floor($date_diff / (60 * 60 * 24));
			  if($days<15){
				$left_time = 15-$days; */
			 ?>
              <li>

                <a href="<?php echo base_url();?>My_subscription" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle yellow small">warning</span>You have 15<?php //echo $left_time; ?> days left for trial period to end. UPGRADE NOW! </a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>

              </li>
             <?php// } else if($days=15){  ?> 
             
            <!-- <li>

                <a href="<?php echo base_url();?>My_subscription" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle yellow small">warning</span>Your trial period expired today. Upgrade Your account to continue with us. </a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>

              </li>-->
             
			<?php// } }?>
            
              <li>

                <a href="#!" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> A new order has been placed!</a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle red small">stars</span> Completed the task</a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle teal small">settings</span> Settings updated</a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-2">

                  <span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>

                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>

              </li>

            </ul>
			<ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>
                  <li>
                    <a href="<?php echo base_url(); ?>profile/personal-profile"">
                      My Personal Profile
                    </a>
                  </li>
                  <li>
                      <a href="<?php echo base_url();?>profile/company-profile">
                        My Company Profile
                          </a>
                        <ul class="userprofile-submenu">
                        <li>
                            <a href="<?php echo base_url(); ?>profile/add-company-profile">
                            <i class="menu-round-icon"></i>
                            <span>Add new business</span>
                          </a>
                      </li>
                      </ul>
                  </li>
					
                  
                  <li>
                    <a href="<?= base_url(); ?>dashboard/document-locker">
                      Document Locker
                    </a>
                  </li>
                  <!-- <li>
                    <a href="#">
                      My Products / Services
                    </a>
                  </li> -->
                  <li>
                    <a href="#">
                      My Account
                    </a>
                    <ul class="userprofile-submenu">
                      <li>
                        <a href="#">
                          <i class="menu-round-icon"></i>
                          <span>My Subscription & Billing</span>
                        </a>
                      </li>
                       <li>
                        <a href="#">
                          <i class="menu-round-icon"></i>
                          <span>Referrals</span>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="menu-round-icon"></i>
                          <span>My Activity History</span>
                        </a>
                      </li>
                      <li>
                        <a class=" Password-change modal-trigger" href="#password-modal">
                          <i class="menu-round-icon"></i>
                          <span>Change Password</span>
                        </a>
                      </li>
                      <!-- <li>
                        <a class=" deactivate modal-trigger" href="<?= base_url(); ?>dashboard/document-locker">
                          <i class="menu-round-icon"></i>
                          <span>Document Locker</span>
                        </a>
                      </li> -->
                      <li>
                        <a class=" deactivate modal-trigger" href="#deactive_myaccount_modal">
                          <i class="menu-round-icon"></i>
                          <span>Deactivate Account</span>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#feedback_modal" class="modal-trigger">
                      My Feedback
                    </a>
                  </li>
                  <li class="divider"></li>
                  <li class="signout-link">
                    <a href="#logout_myacc_modal" class="singout modal-trigger">
                      Sign out
                    </a>
                  </li>
                </ul>
              </li>
            </ul>

          </div>

        </nav>

      </div>

    </header>

    <!-- END HEADER -->