<?php $this->load->view('customise_emails/customise_header'); ?>
    <!-- Include Editor style. -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
     
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/js/froala_editor.min.js'></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  type="text/css" rel="stylesheet" media="screen">
    <!-- <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
 -->
 <script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
<style type="text/css">
  #main {
    padding-left: 360px;
  }
  .outerdiv {
    display: flex;
    padding: 25px;
    background: #fff;
    border-bottom: 1px solid #e5ebf7;
  }
  span.paper-sapn-le {
    margin-top: 4px !important;
    float: none;
    color: #666;
    font-weight: 500;
    font-size: 15px;
  }
  .pdng-0 {
    padding: 0 !important;
    background: #fff;
   /* height: 1280px !important;*/
  }
  .radio-options {
    display: flex;
  }
  #breadcrumbs-wrapper {
    border-bottom: none;
  }
  form p {
    margin-bottom: 5px;
    font-size: 14px;
    font-weight: 500;
  }
  .bill1 {
    padding-top: 0 !important;
  }
  .fr-box.fr-basic .fr-element {
    min-height: 200px;
  }
  .fr-toolbar {
    border-top: 1px solid #d4d4d4;
  }
  .last-sec {
    z-index: 9999;
  }

</style>




<div id="main">
   <div class="wrapper">
      <?php $this->load->view('template/sidebar.php');?>
      <?php $this->load->view('customise_emails/customise_sidebar'); ?>

<form name="company-customise_frm" id="company-customise_frm" method="post" enctype="multipart/form-data">

<div class="">
  <div class="row" style="background-color: #fff;">
      <div class="col s12 m12 l2 pdng-0">
        <div class="outerdiv">
                <a href="javascript:void(0);" class="radio-options"><input type="radio" id="contact-person-2" name="ce_type"  value="company" checked>
                <label class="fill-green-radio" for="contact-person-2"></label><span class="paper-sapn-le">My Company</span></a>
        </div>
 
      </div>
      <div class="col s12 m12 l10" style="background-color: #f8f9fd;">
           <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="" style="margin-bottom: 40px;">
                  <h5 class="breadcrumbs-title">Customise Emails</h5>
                  <ol class="breadcrumbs">
                    <li class="active"><a href="">Customise Emails / Company</a></li>
                  </ol>
                </div>
                <div class="formsection">
                 
                    <div class="reference">
                        <p>Subject</p>
                        <input id="ce_id" type="hidden" name="ce_id" value="<?= @$customise_emails[0]->ce_id; ?>">
                        <input placeholder="ENTER SUBJECT" id="ce_subject" type="text" name="ce_subject" class="bill-box bill1" style="width: 45% !important;" autocomplete="off" value="<?= @$customise_emails[0]->ce_subject; ?>">
                        <label for="ce_subject"></label>
                  </div>
                  <div class="reference" style="margin-bottom: 80px;">
                        <p>Message</p>
                        <textarea id="email-editor" name="ce_message" placeholder="Enter your message here"><?= @$customise_emails[0]->ce_message; ?></textarea>
                          
                  </div>


                  
                </div>
            </div>
          </div>
        </div>     
      </div>
  </div>
  <section class="last-sec">
  <div class="row  pt-2">
    <div class="col s12 m12 l12 save-btn" style="width:26% !important;">    
    <a href="javascript:window.history.go(-1);" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large pays " type="button">CANCEL</a>
    <input type="submit" name="submit" value="SAVE" class="btn-flat theme-primary-btn theme-btn theme-btn-large pays  modal-trigger modal-close">
    </div>
  </div>
</section>
</div>

</form>
      
   </div>
</div>



<script type="text/javascript">

   /*$(function() {
    $('#froala-editor').froalaEditor({
      placeholderText: 'Enter your message here.'
    })
  });*/

$(document).ready(function() {
setTimeout(function(){ 
  $(location).attr('href');
    var pathname = window.location.pathname;
    if(pathname.indexOf("/my_company")) {
        $('#mycompany').addClass('active');      
    } 

 }, 500);

CKEDITOR.replace( 'email-editor', { toolbar : 'Basic' });
$('input[type=radio][name=ce_type]').change(function() {
 var ce_type=$(this).val();
 $('#ce_subject,#ce_id').val('');
  CKupdate();
  $.ajax({
          url:base_url+'customise_emails/get_customise_email',
          type:"POST",
          data:{"ce_type":ce_type},
          success:function(res){
                  var data = JSON.parse(res);
                  if(data != false)
                  {
                    $('#ce_subject').val(data[0].ce_subject);
                    $('#ce_id').val(data[0].ce_id);
                    CKEDITOR.instances['email-editor'].setData(data[0].ce_message);
                  }
             },
    }); 
  });
});
function CKupdate(){
    for ( instance in CKEDITOR.instances ){
        CKEDITOR.instances[instance].updateElement();
        CKEDITOR.instances[instance].setData('');
    }
}



</script>
 
 
   
 

<?php $this->load->view('template/footer'); ?>