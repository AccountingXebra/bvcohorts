<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-tap-highlight" content="no">

	<meta name="description" content="">

	<meta name="keywords" content="">

	<title>Xebra</title>

	<!-- Favicons-->

	<!-- <link rel="icon" href="../../images/favicon/favicon-32x32.png" sizes="32x32"> -->

	<!-- Favicons-->

	<!-- <link rel="apple-touch-icon-precomposed" href="../../images/favicon/apple-touch-icon-152x152.png"> -->

	<!-- For iPhone -->

	<meta name="msapplication-TileColor" content="#00bcd4">

	<!-- <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png"> -->

	<!-- For Windows Phone -->

	<!-- CORE CSS-->

	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">

	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />
    
    


    <?php /*?><link href="<?php echo base_url();?>asset/css/css/style-form.css" type="text/css" rel="stylesheet"><?php */?>

    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">
    
  <!--Import Google Icon Font-->

  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">

 

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    

    <link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">
  
        <script>var base_url = '<?php echo base_url(); ?>';</script>

		<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <style type="text/css">
      .loading_data {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
    background-size: 6%;
	opacity: .9;
}
.notification-msg{
      font-size: 13px;
    font-weight: 300;
    color: #616161 !important;
}
.notifi-dropdown {
  white-space: normal !important;
  max-width: 400px !important;
}
.notisec {
    display: flex !important;
}
.notisec span {
   margin-right: 8px;
}

	#logout_for_new_bus_modal{
		left:150px;
		width:41% !important;
	}
	
	#logout_for_new_bus_modal .modal-body{
		margin: 0px;
	}
	
	#logout_for_new_bus_modal .modal-content {
		padding:0px;
	}
	
	.notify{
		padding-top:0px !important;
		margin-bottom:-15px !important;
	}
	
	.notify h6{ 
		font-size:11px !important;
	}
	
	#setting-dropdown li{
		background:  #7864e9 !important;
	}
	
	.gst_dropdown_select li{
		background:  #7864e9 !important;
	}
	
	.gst_dropdown_select span {
		color:#D0D3D4 !important;
	}
	
	.gst_dropdown_select .dropdown-content li {
		background: #7864e9 !important;
		border-bottom: none  !important;
		overflow: hidden;
	}
	
	.gst_dropdown_select span:hover {
		width: 295px !important;
		background:  #7864e9 !important;
		color:white !important;
		border-left:3px solid #50f1ae !important;
	}
	
	.main_menu_options, .sub_menu_opt{
		color:#D0D3D4 !important;
	}

	.main_menu_options:hover{
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}
	
	/*After active Set Alert Dropdown*/
	.main_menu_options.active{
		background-color: rgba(0,0,0,0.12);
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}
	
	.sub_menu_opt:hover{
		color:white !important;
	}
	
	.company_dropdown span.caret {
		display:none;
	}
    </style>
</head>

<body class="layout-semi-dark white">
<div class="loading_data"></div>


    <?php 

        $reg_id=$this->session->userdata['user_session']['reg_id'];
        $bus_id=$this->session->userdata['user_session']['bus_id'];

        if($reg_id) {

        $notifications = $this->Adminmaster_model->selectData('notifications','*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active'));
        $newNotif = $this->Adminmaster_model->selectData('notifications','ref_id',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active','noti_read_status'=>0),'noti_id','DESC');

      } 

    ?>

    <!-- Start Page Loading -->

   <!-- <div id="loader-wrapper">

		<div id="loader"></div>

		<div class="loader-section section-left"></div>

		<div class="loader-section section-right"></div>

    </div> -->

		<?php
      	  //$current_user =  $this->session->userdata('aduserData');
        ?>

    <header id="header" class="page-topbar">

      <!-- start header nav-->

      <div class="navbar-fixed">

        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">

          <div class="nav-wrapper">

            <div class="dropdown-select-wrapper">

              <div class="company_dropdown">

                <select class="company_dropdown_select companyname" id="header_company_profiles" name="header_company_profiles" disabled="true">
               		  <!-- <option value="" >Entire Company<option> -->
        					<?php if($this->session->userdata['user_session']['reg_id']) {

           
        						$company_profiles = $this->Adminmaster_model->selectData('businesslist','*',array('bus_id'=>$this->session->userdata['user_session']['bus_id'],'status'=>'Active'),'bus_id','ASC');

        						  foreach($company_profiles as $company) {
        						  ?>

                  <option value="<?php echo $company->bus_id; ?>" <?php echo ($company->bus_id == $this->session->userdata['user_session']['bus_id'])?'selected':''; ?>><?php echo $company->bus_company_name; ?></option>

                  <?php } }  ?>

                </select>

              </div>

              <div class="gst_dropdown">

                <select class="gst_dropdown_select companyname" id="branch_gst_list" name="branch_gst_list" onchange="change_gst_header();">

                  
                  <?php if($this->session->userdata['user_session']['bus_id']) {

                    
                    $gst_number = $this->Adminmaster_model->selectData('gst_number','*',array('bus_id'=>$this->session->userdata['user_session']['bus_id'],'type'=>'business'),'gst_id','ASC');
                    if($gst_number!=false){
                      foreach($gst_number as $gst) {
                       /*$result = mb_substr($gst->gst_no, 0, 7).'...';*/
                      ?>

                  <option value="<?php echo $gst->gst_id; ?>" <?= ($this->session->userdata['user_session']['ei_gst']==$gst->gst_id)?'selected':''; ?>><?php echo "GSTIN: ".$gst->gst_no.' - '.$gst->place; ?></option>

                  <?php } ?>
                    <option value="0" <?= ($this->session->userdata['user_session']['ei_gst']==0)?'selected':''; ?>>Combine</option>
                  <?php } }  ?>
                    
                </select>

              </div>

              <div class="header-search-wrapper hide sideNav-lock">

                <i class="material-icons">search</i>

                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search" />

              </div>

            </div>

            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <!--<?php // if($current_user['aduserprofilephoto'] != '') {?>
                      <img src="<?php echo base_url();?>uploads/profile_images/<?php // echo $current_user['aduserprofilephoto']; ?>" alt="">
                    <?php //} ?>-->
                  
                     <?php if($this->session->userdata['user_session']['user_photo']==""){ 
                         
                      ?>
  				        	 <img src="<?php echo base_url();?>asset/css/img/icons/client.png" alt="client-pic">
                    <?php }else{ 
                         $imgUrl=base_url().'public/upload/personal_images/'.$this->session->userdata['user_session']['reg_id'].'/'.$this->session->userdata['user_session']['user_photo'];
                      ?>
                     <img src="<?php echo $imgUrl;?>" alt="client-pic">
                   <?php } ?>
              
                  </span>
                 
                  <span class="user-name">Hi 
					<span class="user-display-name" style="width:2px;"><?php echo ucfirst(substr($this->session->userdata['user_session']['ei_username'],0,8)); ?>
					</span>
				  </span>
				  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>
              <li>
                <?php
                    if(count($newNotif) == 0)
                    {
                      ?>
                      <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
                      <?php
                    }else{
                      ?>
                      <a href="javascript:void(0);" class="waves-effect waves-block waves-light set notification-button" data-activates="notifications-dropdown">
                      <?php
                    }

                    ?>
                
                  <i class="notifications-icons"></i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light setting-button" data-activates="setting-dropdown">
                  <i class="settings-icons"></i>
                </a>
              </li>
              <!-- <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light community-button">
                  <i class="community-icons"></i>
                  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>-->
              <li>
                <!--<a href="javascript:void(0);" onclick="showSearch()" class="waves-effect waves-block waves-light search-button">
                  <i class="search-icons"></i>
                </a>-->
              </li>
            </ul>

            <!-- translation-button -->

            <ul id="translation-dropdown" class="dropdown-content">

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-gb"></i> English</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-fr"></i> French</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-cn"></i> Chinese</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-de"></i> German</a>

              </li>

            </ul>


          <!-- notifications-dropdown -->

          <ul id="notifications-dropdown" class="dropdown-content notifi-dropdown">

            <li class="notify">

              <h6>NOTIFICATIONS
                 
                    <?php
                    if(count($newNotif) == 0)
                    {
                      ?>
                      
                      <?php
                    }else{
                      ?>
                        <span class="new badge"><?= ($newNotif!='')?count($newNotif):''; ?></span>
                      <?php
                    }

                    ?>
                   
                

              </h6>

            </li>

            <li class="divider"></li>
            
            <?php            
              // Remaining Time period
                if($this->session->userdata['user_session']['reg_istrial'] == 1)
                {
                  $now = time(); 
                  $created_date = strtotime($this->session->userdata['user_session']['createdat']); 
                  $datediff = $now - $created_date; 
                  $remaining_day = round($datediff / (60 * 60 * 24));
                  if($remaining_day>15){
                    // Activation Part ditails Come here
                    //echo 'trial_period_expired';
                  }
                  else
                  {
                    ?>
                    <li style="background-color: #f9e8e8 !important;">
                      <a href="<?php echo base_url();?>subscription/my_subscription" class="grey-text text-darken-2 notisec">

                        <div><span class="material-icons icon-bg-circle yellow">warning</span></div><div style="word-break: break-word;">You have <?php echo 15-$remaining_day; ?> days left for trial period to end. UPGRADE NOW! </div>
                      </a>

                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"></time>

                    </li>
                    <?php
                  }
                }

      			?>
            
            <?php
              // Show Notification of user
              if($notifications!=''){
              foreach($notifications as $notif) {
                  $today=date('Y-m-d');
                  $created_date = $notif->createdat;
                  $date_diff=strtotime($today) - strtotime(date("Y-m-d", strtotime($created_date)));
                  $days=floor($date_diff / (60 * 60 * 24));
                  $left_time='';

                  if($notif->noti_read_status==1)
                    {

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>">
                      <?php

                    }else{

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>" style="background-color: #f9e8e8 !important;">
                      <?php

                    }

                    ?>
                          <a class="grey-text text-darken-2 notisec">
                            <input type="hidden" name="link" id="link" value="<?=$notif->noti_url; ?>">
                            <div><span class="material-icons icon-bg-circle red">event_available</span></div><div style="word-break: break-word;"> <?=  wordwrap($notif->noti_message,60); ?>    
                            </div>
                          </a>

                          <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"><?= $days; ?> days ago</time>

                        </li>
                    <?php

                  }
                } 
              ?>

          </ul>

          <!-- Profile Dropdown -->
			    <ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1) 
                  { ?>
                  <li>
                    <a href="<?php echo base_url(); ?>profile/personal-profile"">
                      My Personal Profile
                    </a>
                  </li>
                  <?php }?>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) 
                  { ?>

                  <li class='my-comp'>
                      <a href="<?php echo base_url();?>profile/company-profile">
                        My Company Profile
                          </a>
                        <ul class="userprofile-submenu">
                        <li id="CheckForExistingBus">
                            <a data-target="#myModal">
                            <i class="menu-round-icon"></i>
                            <span>Add new business</span>
                          </a>
                      </li>
                      </ul>
                  </li>

					        <?php }?>
                  
                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) 
                  { ?>

                  <li>
                    <a href="<?= base_url(); ?>dashboard/document-locker">
                      Document Locker
                    </a>
                  </li>
                  <!-- <li>
                    <a href="#">
                      My Products / Services
                    </a>
                  </li> -->
                  <li>
                    <a href="#" id="ac-menu" class="open">
                      My Account
                    </a>
                    <ul class="userprofile-submenu my_ac">
                      <li>
						<a class="renew_sub modal-trigger modal-close" href="#subscription_modal">
                        <!--<a href="<?php //echo base_url(); ?>subscription">-->
                          <i class="menu-round-icon"></i>
                          <span>Renew Subscription & <br>&nbsp&nbsp&nbsp&nbspBuy Add-Ons</span>
                        </a>
                      </li>
                      <li>
            <!--<a class="modal-trigger modal-close" href="#subscription_modal">-->
                        <a href="<?php echo base_url(); ?>subscription">
                          <i class="menu-round-icon"></i>
                          <span>My Billing History</span>
                        </a>
                      </li>
                      <!-- <li>
                        <a href="#">
                          <i class="menu-round-icon"></i>
                          <span>Referrals</span>
                        </a>
                      </li>-->
                      <li>
                        <a href="<?php echo base_url(); ?>module_tracker">
                          <i class="menu-round-icon"></i>
                          <span>My Activity History</span>
                        </a>
                      </li>
                      <li>
                        <a class=" Password-change modal-trigger" href="#password-modal">
                          <i class="menu-round-icon"></i>
                          <span>Change Password</span>
                        </a>
                      </li>
                      <!-- <li>
                        <a class=" deactivate modal-trigger" href="<?= base_url(); ?>dashboard/document-locker">
                          <i class="menu-round-icon"></i>
                          <span>Document Locker</span>
                        </a>
                      </li> -->
                      <li>
                        <a class=" deactivate modal-trigger" href="#deactive_myaccount_modal">
                          <i class="menu-round-icon"></i>
                          <span>Deactivate Account</span>
                        </a>
                      </li>
                    </ul>
                  </li>

                  <?php }?>

                  <li>
                    <a href="#feedback_modal" class="modal-trigger">
                      My Feedback
                    </a>
                  </li>
                  <li class="divider"></li>
                  <li class="signout-link">
                    <a href="#logout_myacc_modal" class="singout modal-trigger">
                      Sign out
                    </a>
                  </li>
                </ul>
              </li>
          </ul>

          <!-- Settings Dropdown -->
          <ul id="setting-dropdown" class="dropdown-content user-profile-down">

              <li class="user-profile-fix">
                <ul>
                  <li>
                    <a class="main_menu_options" href="<?php echo base_url(); ?>customise-emails">
                      Customize Emails
                    </a>
                  </li>
                  
                  <li>
                    <a  class="main_menu_options open" id="set-alert" href="#">
                      Set Alert
                    </a>
                    <ul class="setalert-submenu">
                      <li>
                        <a href="<?= base_url(); ?>settings">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Clients</span>
                        </a>
                      </li>
                       <li>
                        <a href="<?= base_url(); ?>settings/item-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Items</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?= base_url(); ?>settings/credit-period-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Credit Period</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?= base_url(); ?>settings/birthday-anniversary-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Birthday & Anniversary</span>
                        </a>
                      </li>
                    </ul>
                  </li>

                </ul>
              </li>
          </ul>


          </div>

        </nav>

      </div>

      <!-- ----- Start Logout For New Company ----------- -->

      <div id="logout_for_new_bus_modal" class="modal " aria-hidden="true">
         <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
         <div class="modal-content">
            <div class="modal-header">
               <h4>Add New Company</h4>
               <a class="modal-close close-pop log_close"><img style="margin-left:10px;" src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
            </div>
         </div>
         <div class="modal-body confirma">
            <p class="ml-20">You will be signed out from your existing account and taking to Subscription Page to make payment</p>
         </div>
         <div class="modal-content">
            <div class="modal-footer">
               <div class="row">
                  <!--   <div class="col l4 s12 m12"></div> text-right-->
                  <div class="col l12 s12 m12 cancel-deactiv" style='text-align:right;'>
                     <a class="close modal-close btn-flat theme-primary-btn theme-btn theme-btn-large model-cancel " type="button" data-dismiss="modal" >CANCEL</a>
					 <a style='margin-top:0px; margin-right:5px;' class="btn-flat theme-primary-btn theme-btn theme-btn-large dea modal-trigger modal-close logout_my_account centertext">CONFIRM</a>
                    
                  </div>
               </div>
            </div>
         </div>
      </div>

    </header>

	<script type="text/javascript">
			$(document).ready(function() {
				$('#ac-menu').click(function(){
					if($(this).hasClass('open')){
						$('.my_ac').css('visibility','visible').css('opacity','1').css('height','auto');	
						$('#ac-menu').removeClass("open");
						$('#ac-menu').addClass("close");
						//profile-button.active .up
						//$(this).parent().toggleClass('open');
						//$('#profile-dropdown').css('display','block');
						return false;	
					}else if($(this).hasClass('close')){
						$('.my_ac').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#ac-menu').removeClass("close");
						$('#ac-menu').addClass("open");
						return false;	
					}
				});

				//Hide and Show for 'SET ALERTS'
				$('#set-alert').click(function(){
					if($(this).hasClass('open')){
						$('.setalert-submenu').css('visibility','visible').css('opacity','1').css('height','auto');	
						$('#set-alert').removeClass("open");
						$('#set-alert').addClass("active");
						$('#set-alert').addClass("close");
						//$('#profile-dropdown').css('');	
						return false;
					}else if($(this).hasClass('close')){
						$('.setalert-submenu').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#set-alert').removeClass("close");
						$('#set-alert').removeClass("active");
						$('#set-alert').addClass("open");
						return false;
					}
				});	
			});
	</script>
    <!-- END HEADER -->