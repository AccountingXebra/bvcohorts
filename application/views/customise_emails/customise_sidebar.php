<style type="text/css">
  #left-sidebar-nav {
    width: 100px;
    top: 68px;
  }
  .nav-collapsed .side-nav .nav-text {
    display: block;
    position: absolute;
    margin: 30px -10px;
    font-size: 10px;
  }
  /*#slide-out > li > ul.collapsible > li > a {
    line-height: 92px;
    height: 90px;
    
  }*/
  .nav-collapsed .side-nav {
    width: 70px;
  }
  /*.side-nav li > a {
        padding: 0 27px !important;
  }*/
  #slide-out > li > ul.collapsible > li:hover > a {
        border-left: 3px solid transparent;
        background-color: #fff;
        color: #000 !important;
        border-right: 1px solid #e5ebf7;
  }
  .menu.sales-icon {
        background: url('<?= base_url(); ?>asset/css/img/icons/menu/normal/my-gst.png') no-repeat center !important;
  }
  body.layout-semi-dark #main .side-nav li:hover .menu.sales-icon, body.layout-semi-dark #main .side-nav li.active .menu.sales-icon {
      background: url('<?= base_url(); ?>asset/css/img/icons/menu/normal/my-gst.png') no-repeat center !important;
      
  }
  body.layout-semi-dark #main .side-nav li:hover .menu.gst-icon, body.layout-semi-dark #main .side-nav li.active .menu.gst-icon {
     background: url('<?= base_url(); ?>asset/css/img/icons/menu/normal/my-gst.png') no-repeat center !important;
  }
  
  #slide-out > li > ul.collapsible > li.active > a { 
        border-left: 3px solid transparent;
        background-color: #fff;
        color: #000 !important;
        border-right: 1px solid #e5ebf7;
  }
  #slide-out .menu {
    height: 73%;
  }
  
  #slide-out > li > ul.collapsible > li.active {
	  border-left:3px solid #50e3c2 !important;
  }
</style>
<aside id="left-sidebar-nav" class="customise_email nav-collapsible nav-collapsed" style="left:253px !important; /*background:black !important; overflow:scroll; height:90% !important;*/">


    <ul id="slide-out" class="slide_email side-nav fixed leftside-navigation" style="left:253px !important; top: 68px; height: 93% !important;width: 107px; overflow:scroll !important;">

      <!--<li class="no-padding" style="display:block; background: #fff;height: 68px;border-right: 1px solid #e5ebf7;border-bottom: 1px solid #e5ebf7;width: 100px;">

            <ul class="" data-collapsible="accordion">

                <li class="bold">
                    <a class="">
                        <i class="menu-icon open" style="margin-left: 4px;width: 22px;margin-top: 10px;"></i>
                    </a>
                </li>
            </ul>
        </li>-->



        <li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold" id="mysales">

                    <a href="<?php echo base_url();?>customise-emails/my-sales" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu sales-icon"></i>

                        <span class="nav-text">MY SALES</span>

                    </a>

                </li>

            </ul>

        </li>

        <li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="mytax">

                    <a href="<?php echo base_url();?>customise-emails/my-tax" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text">MY TAX</span>

                    </a>

                </li>

            </ul>

        </li>
	
		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="myreceipts">

                    <a href="<?php echo base_url();?>customise-emails/my_receipt" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text" style="margin: 30px -18px !important;">MY RECEIPTS</span>

                    </a>

                </li>

            </ul>

        </li>
        
		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="mycompany">

                    <a href="<?php echo base_url();?>customise-emails/my_company" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text" style="margin: 30px -18px !important;">MY COMPANY</span>

                    </a>

                </li>

            </ul>

        </li>
          
		  
		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="mydoclocker">

                    <a href="<?php echo base_url();?>customise-emails/document_locker" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text" style="margin: 30px -18px !important;">DOC. LOCKER</span>

                    </a>

                </li>

            </ul>

        </li>

		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="mysubscription">

                    <a href="<?php echo base_url();?>customise-emails/subscription_billing" style="line-height: 92px !important; height: 90px !important; padding: 0 27px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text" style="margin: 30px -21px !important;">SUBSCRIPTION</span>

                    </a>

                </li>

            </ul>

        </li>
		
		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold " id="myalert">

                    <a href="<?php echo base_url();?>customise-emails/my_alert" style="line-height: 92px !important; height: 90px !important;" class="collapsible-header waves-effect waves-cyan">

                        <i class="menu gst-icon"></i>

                        <span class="nav-text" style="margin: 30px -2px !important;">ALERT</span>

                    </a>

                </li>

            </ul>

        </li>
		
    </ul>

    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">

        <i class="material-icons">menu</i>

    </a>

</aside>