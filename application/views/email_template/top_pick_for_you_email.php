<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Top picks just for you</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
		
		.playpause {
			background-image:url("<?=base_url('assets/images/overlay.png');?>");
			background-repeat:no-repeat;
			background-repeat: no-repeat;
			width: 9%;
			height: 9%;
			position: absolute;
			left: -33%;
			right: 0%;
			top: 116%;
			bottom: 0%;
			margin: auto;
			background-size: contain;
			background-position: center;
		}
		
		.playpause1 {
			background-image:url("<?=base_url('assets/images/overlay.png');?>");
			background-repeat:no-repeat;
			background-repeat: no-repeat;
			width: 9%;
			height: 9%;
			position: absolute;
			left: 0%;
			right: 0%;
			top: 116%;
			bottom: 0%;
			margin: auto;
			background-size: contain;
			background-position: center;
		}
		.playpause2 {
			background-image:url("<?=base_url('assets/images/overlay.png');?>");
			background-repeat:no-repeat;
			background-repeat: no-repeat;
			width: 9%;
			height: 9%;
			position: absolute;
			left: 33%;
			right: 0%;
			top: 116%;
			bottom: 0%;
			margin: auto;
			background-size: contain;
			background-position: center;
		}
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
	<tbody style="border:1px solid #ccc;">   
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
				<tr><!-- style="background-color:#e6e6ff;" -->
					<td><a href="#" target="_blank"><img style="margin:10px !important; width:180px; height:55px;" height="55" width="180" class="columnImage" src="<?php echo base_url();?>assets/images/logo/logo11.png" border="0"/></a></td>    
					<td><img style="margin:-11% 0 0 49%;" class="columnImage" src="<?php echo base_url(); ?>assets/images/Email Verification Circles Close Up Top.png" border="0"/></td>
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #bc343a;"><h3 style="margin: 40px 0px 30px 20px; font-size: 20px;">Top picks just for you!</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #bc343a;"><img height="225" class="columnImage" src="<?php echo base_url(); ?>assets/images/Toppickspic.png" border="0"/></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500; padding:40px 0 30px 0;">
            <p style="font-size: 15px">Having a boaring week? Why not catch up with your</br>favourite people and see what they are up to!. We</br>know they surely missed you. Hop on and take a look </br>at what you have missed.</p>
        </td>
    </tr>
	<!--tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="margin: 40px;">
                <a href="#" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); background: #bc343a;margin-top: 20px;padding: 10px 15px;color: #fff; border-radius: 6px; text-decoration: none;font-size: 15px;">GET YOUR SWAG NOW</a><!-- for this page is created in index folder -->
            <!--/p>
        </td>
    </tr-->
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500; background-color:#eff7f9; padding:0px 0 20px 0; margin:20px 20px; border-radius:20px;">
            <hr style="width: 60%; border: 1px solid #2da5a6; margin: 0 0 25px 0;">
			<p style="font-size:22px;">New Videos Added</p>
			<p style="font-size:15px;">
				<?php foreach($videolatest as $keyVid=>$vidLat){ if($keyVid < 2){ ?>
				<div style="width:50%; float:left;">
					<p style="font-size:15px;">
						<a href="<?php echo base_url();?>home/view_comment/<?=$vidLat->id?>" target="_blank"><img style="border: 5px solid #fff; border-radius: 18px; box-shadow: 0 0 25px #ccc; margin:10px !important; width:200px; height:180px;" height="55" width="180" class="columnImage" src="<?=$vidLat->thumbnail?>" border="0"/></a>
						<!--video style="border-radius:10px; width:65%; margin-left:20%; height:190px; object-fit: fill; border:5px solid #fff; box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);" class="youtube editor-vl" src="" poster="<?//=$vidLat->thumbnail?>" data-embed="" data-src="" frameborder="0" controls></video-->	
					</p>
					<!--div class="playpause"></div-->
				</div>
			<?php }} ?>
			
			</p>
        </td>
	</tr>	
	<tr>
		<td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500; background-color:#fff9ef; padding:0px 0 20px 0; border-radius:20px; margin:0 20px; border-radius:20px;">
            <hr style="border:1px solid #fcaf39; width: 60%; border: 1px solid #fcaf39; margin: 0 0 25px 0;">
			<p style="font-size:22px;">This Weeks Feature</p>
			<p style="font-size:15px;">
				<?php foreach($video as $keyVid=>$vid){ if($keyVid < 2){ ?>
				<div style="width:50%; float:left;">
					<p style="font-size:15px;">
						<a href="<?php echo base_url();?>home/view_comment/<?=$vid->id?>" target="_blank"><img style="border: 5px solid #fff; border-radius: 18px; box-shadow: 0 0 25px #ccc; margin:10px !important; width:200px; height:180px;" height="55" width="180" class="columnImage" src="<?=$vid->thumbnail?>" border="0"/></a>
						<!--video style="border-radius:10px; width:65%; margin-left:20%; height:190px; object-fit: fill; border:5px solid #fff; box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);" class="youtube editor-vl" src="" poster="<?//=$vid->thumbnail?>" data-embed="" data-src="" frameborder="0" controls></video-->	
					</p>
					<!--div class="playpause"></div-->
				</div>
				<?php }} ?>
			</p>
        </td>
    </tr>
	<tr>
		<td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500; background-color:#f1f0ee; padding:0px 0 20px 0; border-radius:20px; margin:0 20px; border-radius:20px;">
            <hr style=" width: 60%; border: 1px solid #4f342b; margin: 0 0 25px 0;">
			<p style="font-size:22px;">Bharat Vaani's Trending VIdeos</p>
			<p style="font-size:15px;">
				<?php foreach($videotrend as $keyVid=>$vid){ if($keyVid < 2){ ?>
				<div style="width:50%; float:left;">
					<p style="font-size:15px;">
						<a href="<?php echo base_url();?>home/view_comment/<?=$vid->id?>" target="_blank"><img style="border: 5px solid #fff; border-radius: 18px; box-shadow: 0 0 25px #ccc; margin:10px !important; width:200px; height:180px;" height="55" width="180" class="columnImage" src="<?=$vid->thumbnail?>" border="0"/></a>	
						<!--video style="border-radius:10px; width:65%; height:190px; object-fit: fill; border:5px solid #fff; box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); margin-left:20%;" class="youtube editor-vl" src="" poster="<?//=$vid->thumbnail?>" data-embed="" data-src="" frameborder="0" controls></video-->	
					</p>
				</div>
			<?php }} ?>
			</p>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;">
			<p style="font-size: 15px; margin-top:4%; color:#595959; font-weight:500;">Regards,</p>
			<p style="font-size: 15px; margin-top:-10px; color:#595959; font-weight:500;">Team <a style="text-decoration:none; color:#bc343a; font-size: 16px;" href="https://bharatvaani.in/"><strong>Bharat Vaani.</strong></a></p>
        </td>
    </tr>
	<tr><td><img class="columnImage" src="<?php echo base_url(); ?>/assets/images/Email Verification Circles Close Up.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #50A3A4; background:#50A3A4;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/BharatVaaniIn" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/showcase/bharatvaani" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/bharatvaani/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCoVVXCXAPIEtfQnpwFpnRqQ" target="_blank"><img style="width:40px; height:30px; padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<!--tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr-->
	</tbody>
</table>
</body>
</html>
