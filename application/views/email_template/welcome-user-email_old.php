<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Welcome to Bharat Vaani</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
		@media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
                <!--<tr>
                    <td><a href="#" target="_blank"><img width="200" height="50" class="columnImage" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" border="0"/></a></td>
                    <td><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0"/></td>
                </tr>-->
				<tr style="background-color:#e6e6ff;">
					<td><a href="#" target="_blank"><img style="margin:10px !important;" width="185" height="75" class="columnImage" src="<?php echo base_url(); ?>public/images/xebracohort-mail.png" border="0"/></a></td>  
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><h3 style="margin: 40px 0px 20px 0;font-size: 20px;">Thank you for signing up</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage"src="<?php echo base_url(); ?>asset/images/email/help-hand.gif" border="0"/></td>
    </tr>
    <tr><?php ?>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">Hey <span style="color: #503cb7"> <?php echo $name;?></span>,</p>
            <p style="font-size: 20px">Welcome to Bharat Vaani!</p>
            <p style="font-size: 15px">It’s a platform for you to leverage the power of a group, a Cohort! A cohort that you can use as a sounding board or share your success, failure, emotions and grind that went behind building your venture. Use it as a platform to talk about your emotional health and wellbeing as well as growth and revenues.</p>
			<p style="font-size: 15px">The irony of entrepreneurship is that while all of us go through a relatively similar set of successes and failures, the journey ends up being lonely for most times. We, too, felt it ourselves and decided to bring as many entrepreneurs together on a single platform to leverage the power of a group,</p>
			<p style="font-size: 15px">We have a marketplace where you can list your company and generate business from the eco-system. We keep updating it with deals that you encash and leverage for your business.  We also will be sharing details about industry-led events taking place and how to participate in it. </p>
			<p style="font-size: 15px">We would love to hear about your experiences with Bharat Vaani and how we can improvise it. Do send in your ideas and suggestions to <a style="color:#7864e9;" href="mailto:contactus@bharatvaani.in">contactus(at)bharatvaani.in</p>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;">
			<img style="height: 85px; width: 68px; border-radius:50%;" class="" src="<?php echo base_url(); ?>public/images/sapna.jpg" border="0" alt="ceo"/>
			<p style="float:right; margin: 2.5% 27% 0 -40%; color:#000; font-size:15px;"><label><b><a class="direct" style="color: #000; margin: 0 0 0 -96%;" href="https://www.linkedin.com/in/nimesh9" target="_blank" rel="noopener noreferrer"><strong>Sapna Bakshi</strong></a></b></label></p>
			<p style="float:right; margin: 5.5% 17% 0 -40%; color:#000; font-size:15px;"><label style="float:right; color:#000;">Co-Founder - Bharat Vaani</label></p>
		</td>
    </tr>
    <tr><td><img class="columnImage" src="<?php echo base_url(); ?>/asset/images/off.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/bharatvaaniin" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/bharatvaani" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/instagram.png" border="0"/></a>&nbsp;<a href="https://youtube.com/bharatvaani" target="_blank"><img style="width:40px; height:30px; padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
                <!--tr>
                    <td align="center" style="color: #ffffff;font-family: 'Roboto', sans-serif;">
                        <p style="font-size: 12px;">Copyright &copy; Xebra. All Rights Reserved.</p>
                        <p style="font-size: 12px;">Want to change how you receive these emails?<br>
                        You can <a href="#" style="color: #ffffff">update your preferences</a> or <a href="#" style="color: #ffffff">unsubscribe from this list</a></p>
                    </td>
                </tr-->
            </table>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr>
</table>
</body>
</html>
