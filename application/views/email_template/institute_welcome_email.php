<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Institute Welcome</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
	<tbody style="border:1px solid #ccc;">   
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
				<tr><!-- style="background-color:#e6e6ff;" -->
					<td><a href="#" target="_blank"><img style="margin:10px !important; width:180px; height:55px;" height="55" width="180" class="columnImage" src="<?php echo base_url();?>assets/images/logo/logo11.png" border="0"/></a></td>   
					<td><img style="margin:-11% 0 0 49%;" class="columnImage" src="<?php echo base_url(); ?>assets/images/Email Verification Circles Close Up Top.png" border="0"/></td>
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #bc343a;"><h3 style="margin: 40px 0px 20px 0;font-size: 20px;">Welcome to our Community</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #bc343a;"><img height="225" class="columnImage" src="<?php echo base_url(); ?>assets/images/institutepic.png" border="0"/></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px; margin-top:3%;">Hi <span style="color: #bc343a"> First name</span>,</p>
            <p style="font-size: 15px">It's great to have to on board with us.</p>
			<p style="font-size: 15px">Let's get to know each other, by first introducing ourselves.</br>Bharat Vaani is an exclusive community platform for indian</br>innovators and innovators to share inspiring videos of indian</br>inventions, innovations and creations. </p>
			<p style="font-size: 15px">The idea is to come together and build a community that shares </br>knowledge and inspire creativity.</p>
			<p style="font-size: 15px">We have industry experts, thought leaders, publications visit our </br> website to get authentic information on all the new innovations</br>which would be a great apportunity to showcase your ideas.</p>
			<p style="font-size: 15px"><a href="https://bharatvaani.in/" target="_blank" style="color: #bc343a !important; border-radius: 6px; text-decoration: none;font-size: 15px; text-decoration:underline;">So let's get started?</a></p>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;">
			<p style="font-size: 15px; margin-top:4%; color:#595959; font-weight:500;">Regards,</p>
			<p style="font-size: 15px; margin-top:-10px; color:#595959; font-weight:500;">Team <a style="text-decoration:none; color:#bc343a; font-size: 16px;" href="https://bharatvaani.in/"><strong>Bharat Vaani.</strong></a></p>
        </td>
    </tr>
	<tr><td><img class="columnImage" src="<?php echo base_url(); ?>/assets/images/Email Verification Circles Close Up.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #50A3A4; background:#50A3A4;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/BharatVaaniIn" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/showcase/bharatvaani" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/bharatvaani/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCoVVXCXAPIEtfQnpwFpnRqQ" target="_blank"><img style="width:40px; height:30px; padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<!--tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr-->
	</tbody>
</table>
</body>
</html>
