<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>News</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff; background-image:url(<?php echo base_url();?>asset/images/cohorts/news_email.jpg); background-size: 701px;">
	<tbody style="border:1px solid #ccc;">   
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
				<tr>
					<td style="float:right;"><a href="#" target="_blank"><img style="margin:18px !important; width:180px; height:55px;" height="55" width="180" class="columnImage" src="<?php echo base_url();?>assets/images/logo/logo11.png" border="0"/></a></td>   
				</tr>
            </table>
        </td>
    </tr>
	<tr height="420px"><td><hr style="border:1px solid transparent; width: 60%; border: 1px solid transparent; margin: 0 0 25px 0;"></td></tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; padding:0 10px 20px 50px;">
			<div style="float:left; margin:0 0 0 10px; width:30%; margin:0 0 0 -2px; ">
				<a style="text-decoration:none;" href="#" target="_blank">
					<img style="border:1px solid #fcad37; margin:10px !important; width:100%; height:110px; object-fit:contain; box-shadow: 0 8px 15px 0 rgb(138 155 165 / 15%);" class="columnImage" src="<?php echo base_url();?>asset/images/cohorts/video1001.jpg" border="0"/>
				</a>
			</div>
			<div style="float:left; width:50%; margin-top:10px; padding:0 0 0 50px; text-align:left;">
				<p style="color:#fcad37; margin: 0 0 15px 0; font-size:16px; font-weight:500;">Innovation Ranking</p>
				<p style="margin: 0 0 15px 0; font-size:14px;">Atal innovation Ranking 7 IITs </p>
				<a href="#" target="_blank" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); background: #bc343a;margin-top: 20px;padding: 5px 25px;color: #fff; border-radius: 40px; text-decoration: none;font-size: 15px;">Read More</a>
			</div>
		</td>
	</tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; padding:0 10px 20px 50px;">
			<div style="float:left; margin:0 0 0 10px; width:30%; margin:0 0 0 -2px; ">
				<a style="text-decoration:none;" href="#" target="_blank">
					<img style="border:1px solid #fcad37; margin:10px !important; width:100%; height:110px; object-fit:contain; box-shadow: 0 8px 15px 0 rgb(138 155 165 / 15%);" class="columnImage" src="<?php echo base_url();?>asset/images/cohorts/video1001.jpg" border="0"/>
				</a>
			</div>
			<div style="float:left; width:50%; margin-top:10px; padding:0 0 0 50px; text-align:left;">
				<p style="color:#fcad37; margin: 0 0 15px 0; font-size:16px; font-weight:500;">Technologies</p>
				<p style="margin: 0 0 15px 0; font-size:14px;">Detect Technologies enters into global agreement with shell</p>
				<a href="#" target="_blank" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); background: #bc343a;margin-top: 20px;padding: 5px 25px;color: #fff; border-radius: 40px; text-decoration: none;font-size: 15px;">Read More</a>
			</div>
		</td>
	</tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; padding:0 10px 20px 50px;">	
			<div style="float:left; margin:0 0 0 10px; width:30%; margin:0 0 0 -2px; ">
				<a style="text-decoration:none;" href="#" target="_blank">
					<img style="border:1px solid #fcad37; margin:10px !important; width:100%; height:110px; object-fit:contain; box-shadow: 0 8px 15px 0 rgb(138 155 165 / 15%);" class="columnImage" src="<?php echo base_url();?>asset/images/cohorts/video1001.jpg" border="0"/>
				</a>
			</div>
			<div style="float:left; width:50%; margin-top:10px; padding:0 0 0 50px; text-align:left;">
				<p style="color:#fcad37; margin: 0 0 15px 0; font-size:16px; font-weight:500;">Startups</p>
				<p style="margin: 0 0 15px 0; font-size:14px;">Indian startups raised $8.4bn in Q1, 2022 India ranks 4th globally with 96 unicorns</p>
				<a href="#" target="_blank" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); background: #bc343a;margin-top: 20px;padding: 5px 25px;color: #fff; border-radius: 40px; text-decoration: none;font-size: 15px;">Read More</a>
			</div>
		</td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; padding:0 10px 20px 50px;">	
			<div style="float:left; margin:0 0 0 10px; width:30%; margin:0 0 0 -2px; ">
				<a style="text-decoration:none;" href="#" target="_blank">
					<img style="border:1px solid #fcad37; margin:10px !important; width:100%; height:110px; object-fit:contain; box-shadow: 0 8px 15px 0 rgb(138 155 165 / 15%);" class="columnImage" src="<?php echo base_url();?>asset/images/cohorts/video1001.jpg" border="0"/>
				</a>
			</div>
			<div style="float:left; width:50%; margin-top:10px; padding:0 0 0 50px; text-align:left;">
				<p style="color:#fcad37; margin: 0 0 15px 0; font-size:16px; font-weight:500;">Govt Schemes</p>
				<p style="margin: 0 0 15px 0; font-size:14px;">Govt schemes accelerating pace of innovation to create holistic Science-Tech ecosystem</p>
				<a href="#" target="_blank" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75); background: #bc343a;margin-top: 20px;padding: 5px 25px;color: #fff; border-radius: 40px; text-decoration: none;font-size: 15px;">Read More</a>
			</div>
		</td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse; margin-top:20px;"><!-- background:#50A3A4; border: 1px solid #50A3A4;-->
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid transparent;"><a href="https://www.youtube.com/channel/UCoVVXCXAPIEtfQnpwFpnRqQ" target="_blank"><img style="width:40px; height:29px; padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/youtube.png" width="40" height="30" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/bharatvaani/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/instagram.png" border="0"/></a>&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/face.png" border="0"/></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/BharatVaaniIn" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/tweet.png" border="0"/></a>&nbsp;&nbsp;&nbsp;<a href="https://www.linkedin.com/showcase/bharatvaani" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/assets/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<!--tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr-->
	</tbody>
</table>
</body>
</html>
