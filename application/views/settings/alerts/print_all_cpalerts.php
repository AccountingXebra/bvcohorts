<style type="text/css">
	@page {
        size: auto; 
        margin: 0mm;  
    }
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="9"><h2 style="margin: 0">Credit Period Alerts</h2></th></tr>
	<tr>
	<th>ALERT NO.</th>
	<th>ALERT DATE</th>
	<th>CLIENT NAME</th>
	<th>REMINDER INTERVAL</th>
	<th>ALERT NOTIFICATION</th>
	<th>ALERT MOBILE</th>
	<th>ALERT EMAIL</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($result as $key => $value) { ?>
	<tr>
	
	<td><?=$value['alert_number']?></td>
	<td><?=date('d-m-Y',strtotime($value['alert_date']))?></td>
	<td><?=$value['cust_name']?></td>
	<td><?=date('d-m-Y',strtotime($value['alert_date']))?></td>
	<?php if($value['alert_notification']==1) { ?>
	<td>Yes</td>
	<?php }else{ ?>
		<td>No</td>
	<?php  } ?>
	<?php if($value['alert_msg']==1) { ?>
	<td><?php echo $value['alert_mobile']; ?></td>
	<?php }else{ ?>
		<td> </td>
	<?php  } ?>
	<?php if($value['alert_mail']==1) { ?>
		<td><?php echo $value['alert_email']; ?></td>
	<?php }else{ ?>
		<td> </td>
	<?php  } ?>
<?php } ?>

</tbody>
</table>
<script type="text/javascript">
	window.print();
</script>