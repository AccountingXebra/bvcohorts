<?php $this->load->view('template/header.php');?>
<!-- START MAIN -->
<style type="text/css">
.dataTables_scrollBody{
			overflow:hidden !important;
			height:100% !important;
		}

		.dataTables_scrollHead{
			margin-bottom:-24px !important;
		}

		table.dataTable thead .sorting {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_asc {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_desc {
		  background-position: 110px 15px !important;
		}
table.dataTable thead .sorting {
  background-position: 125px 17px !important;
}
 .btn-stated {
  max-width: 76px !important;
  background-position: 94px center !important;

}
.btn-date {
  margin: 0 0 0 -4px !important;
}
.btn-dropdown-select > input.select-dropdown{
  max-width: 100px;
}
.ml-3px{
  margin-left: 3px;
}
.btn-search{
  margin: 0 0 0 10px !important;
}
a.filter-search.btn-search.btn.active {
  margin-right: -35px !important;
}
.icon-img-noti {
  width: 20px;
  margin: 0 5px 0 2px;
}
.addnew.btn-dropdown-select span.caret {
  color: #FFFF;
}
.addnew.btn-dropdown-select > input.select-dropdown{
 color: #FFFF !important;
/* padding: 0 28px 0 18px !important;*/
 max-width: 100%;
 width: 152px;
}
.addnew.dropdown-content li > span{
  color: #000!important;
}
.btn-welcome {
  padding: 0px 0 !important;
}
.welcome-button {
  float: left;
}
.action-btn-wapper span.caret {
    margin: 16px 13px 0 0;
    color:#666;
}
/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 200px !important;
  margin-left: 3px;

}

.dataTables_length {

    margin-left: 500px;
}

#my-ba-alerts_length{
    border:1px solid #B0B7CA;
    height:38px;
    border-radius:4px;
    width:88px;
    margin-top:5px;
    margin-left:52%;
  }

  #my-ba-alerts_length .select-wrapper input.select-dropdown {
    margin-top:-3px !important;
    margin-left:10px !important;
  }

	#my-ba-alerts_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}

	#my-ba-alerts_length .dropdown-content {
		min-width: 95px;
		margin-top:-50% !important;
	}

/*----------END SEARCH DROPDOWN CSS--------*/
input[type=text]:not(.browser-default) {
  font-size: 12px !important;
}
.dropdown-content li > span {
  font-size: 12px;
  color: #666;
}
	.balerts_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.sticky {
			position: fixed;
			top: 70px;
			width: 77.6%;
			z-index:999;
			background: white;
			color: black;

		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}

	.ex{
		margin:-28px 0 0 48px !important;
	}
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
         <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-theme-gray alerts-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Birthday & Anniversary Alerts <small class="grey-text">(<?php echo $alerts; ?> Total)</small></h5>

                  <ol class="breadcrumbs">

                    <li class="active">MY SETTINGS / BIRTHDAY & ANNIVERSARY ALERTS</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
                  <a class="btn btn-theme btn-large right add-new-profile-btn service" href="<?php echo base_url(); ?>settings/add-birthday-anniversary-alert">ADD NEW ALERT</a>
                  <!-- <select name="settings_alerts" id="settings_alerts" class='btn-theme right border-radius-6 btn-dropdown-select border-split-form select-like-dropdown by-statys addnew'>
                  <option value=''>ADD NEW ALERT FOR</option>
                  <option value="add-client-alert">CLIENTS</option>
                  <option value="add-item-alert">ITEMS</option> -->
                  <!-- <option value="add-jv-alert">JOURNAL VOUCHER</option> -->
                  <!-- <option value="add-credit-period-alert">CREDIT PERIOD</option>
                  <option value="add-birthday-anniversary-alert">BIRTHDAY & ANNIVERSARY</option> -->
                </select>
              </div>
            </div>
          </div>
        </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12 ">
                 <a href="javascript:void(0);" class="addmorelink right" onclick="reset_alertsfilter('balert');" title="Reset all">Reset</a>
                </div>
                <div class="col l5 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk4'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown_bulk4' class='dropdown-content'>

                    <li><a id="email_multiple_balerts"><i class="material-icons">email</i> Email</a></li>
                    <li><a id="download_multiple_balerts"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 15px;" alt="export"/><p class="ex">Export</p></a></li>
                    <li><a id="print_multiple_balerts"><i class="material-icons">print</i>Print</a></li>
                    <li><a id="deactive_multiple_balerts" data-multi_balerts="0"><i class="material-icons">delete</i>Delete</a></li>
                  </ul>
                  <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_balerts" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>
                </div>
                 <div class="col l7 s12 m12 right">
                  <div class="action-btn-wapper right sort">

                  <select class='ml-3px border-radius-6 btn-dropdown-select border-split-form select-like-dropdown select-cmpy' id="balerts_occasion" name="balerts_occasion">
                   <option value="">BY OCCASION</option>
                   <option value="">ALL</option>
                   <option value="Birthday">BIRTHDAY</option>
                   <option value="Anniversary">ANNIVERSARY</option>
                  </select>

                  <!-- <select class='ml-3px border-radius-6 btn-dropdown-select border-split-form select-like-dropdown  by-statys' id="balerts_customer" name="balerts_customer"> -->
                  <select class="js-example-basic-single .ml-3px" name="balerts_customer" id="balerts_customer">
					<option value="">SELECT STAKEHOLDER</option>
                    <option value="">ALL</option>
					<option value="client">CLIENT</option>
					<option value="vendor">VENDOR</option>
					<option value="employee">EMPLOYEE</option>
                    <!--<?php //if($customers != '') {
                      //foreach($customers as $customer)  { ?>
                        <option value="<?php //echo $customer->cust_id; ?>"><?php //echo strtoupper($customer->cust_name); ?></option>
                        <?php //}}?>-->
                  </select>

                    <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="balerts_start_date" name="balerts_start_date" readonly="readonly">
                    <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="balerts_end_date" name="balerts_end_date" readonly="readonly">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12" id="print_services_tbl">
                  <table id="my-ba-alerts" class="responsive-table display table-type1 mb-2" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:10px">
                          <input type="checkbox" id="balerts_bulk" name="balerts_bulk" class="filled-in purple" />
                          <label for="balerts_bulk"></label>
                        </th>
                        <th style="width:50px;">ALERT NO.</th>
                        <th style="width:70px;  background-position: 95px 17px !important;">ALERT DATE</th>
                        <th style="width:100px; background-position: 150px 17px !important;">CONTACT NAME</th>
                        <th style="width:100px; background-position: 110px 17px !important;">STAKEHOLDER</th>
                        <th style="width:70px; background-position: 85px 17px !important;">OCCASION</th>
                        <th style="width:70px; background-position: 120px 17px !important;">REMINDER DATE</th>
                        <th style="width:50px">ALERT TYPE</th>
                        <th style="width:5px;">Actions</th>
                      </tr>
                    </thead>
                    <tbody class="scrollbody">

                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
<div id="deactivate_balerts" class="modal modal-set">

    <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content">

      <div class="modal-header">

        <h4> Remove Birthday/Anniversary Alert </h4>

        <input type="hidden" id="remove_balert_id" name="remove_balert_id" value="" />

        <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

      </div></div>

      <div class="modal-body confirma">

        <p>Are you sure you want to remove this birthday/anniversary Alert?</p>

      </div>

      <div class="modal-content">

        <div class="modal-footer">

          <div class="row">



            <div class="col l12 s12 m12 cancel-deactiv">

              <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">CANCEL</a>


              <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close delete_balerts">REMOVE</button>

            </div>

          </div>

        </div>

      </div>

    </div>

<div id="email_popup_ba_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
    <?php $this->load->view('settings/alerts/email_popup_ba'); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
  });
</script>
<?php $this->load->view('template/footer.php'); ?>
