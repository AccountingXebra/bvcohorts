 <?php $this->load->view('template/header.php'); ?>
<link href="<?php echo base_url();?>asset/css/style-form.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url();?>asset/css/mystyle.css" type="text/css" rel="stylesheet">
  
  <style type="text/css">
    .icon-img-noti {
      width: 20px;
      margin: 5px 5px 0 2px;
    }
    .cre-pre-row {
     border-bottom:none !important;
    }
  </style>
  <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar.php'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="">
          <div class="container">
            <div class="list-top">
              <div class="list-left">
                <div class="top-barl">
                  <h4><a href="<?php echo base_url();?>settings/credit-period-alerts"><span>Back to My Alerts</span></a></h4>
                </div> <!-- top-bar END -->
              </div> <!-- list-left END -->
               <?php /*<div class="list-right">
               <div class="right-2">
                <?php $encrypted_id = $halerts[0]->id; 
                if($halerts[0]->alt_status == 'Active') { ?>
                  <a class="cur active_deactive_calerts" data-calerts_id="<?php echo $encrypted_id; ?>" data-cstatus="Inactive" data-alert_type="credit_history">Delete this Alert</a>
                 <?php } else { ?>
                 <a class="cur active_deactive_calerts" data-calerts_id="<?php echo $encrypted_id; ?>"  data-cstatus="Active" data-alert_type="credit_history">Activate this Alert</a>
                 <?php } ?> 
                </div> <!-- right-2 end -->
                <div class="right-1">
                  <a href="<?php echo base_url(); ?>settings/print-alert/<?= $encrypted_id; ?>" target="_blank"><i class="icon print"></i></a>
                </div><!-- right-1 end -->
              </div> <!-- list-right end -->*/?>
			  <?php $encrypted_id = $halerts[0]->id; ?>
            </div> <!-- list-top END -->
            <div class="row">
              <img src="<?php echo base_url();?>asset/css/img/icons/greencrl.png" alt="green" class="off-color">
              <div class="col l12 s12 m12">
                <div class="col l12 s12 m12">
                  <div class="first-cover">
                    <div class="row first-rows">
                      <div class="col s12 m12 l12 wind no-margin-row wind-my bg-img-green">
                        <div class="row">
                          <div class="col l12 s12 m12 Communications">
                            <div class="" style="margin: 15px 0;">
                                <div class="uploader-placeholder" style="box-shadow: 0px 0px 0px 2px #50e3c2; border:none; width: 35px; height: 35px; border-radius: 50%;">
                                </div>
                            </div>
                            <h5><?php echo $halerts[0]->cust_name; ?></h5>
                            <div class="first-right">
                              <a href="<?php echo base_url(); ?>settings/edit-credit-period-alert/<?php echo $encrypted_id; ?>"><i class="material-icons" style="color: #fff; font-size: 20px;">edit</i></a>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col l6 s12 m12">
                            <div class="col l1 s1 m1"></div>
                            <!-- <div class="col l1 s1 m1">
                                <i class="material-icons"  style="color: #51e2c5;">location_on</i>
                            </div> -->
                            <div class="col s10 m10 l10">
                              <div class="row">
                                <div class="col s12 m12 l12">
                                  <h4 class="wind-label"></h4>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col s12 m12 l6">
                                  <h4 class="wind-label">ALERT NO.</h4>
                            
                                  <p><?= @$halerts[0]->alert_number; ?></p> 
                                </div>
                                <div class="col s12 m12 l6">
                                  <h4 class="wind-label">ALERT DATE</h4>
                                  <?php $alt_date='';
                                  if($halerts[0]->alert_date!='')
                                  {
                                    $dateTime = date("d-m-Y",  strtotime($halerts[0]->alert_date));
                                    $alt_date = str_replace("-",".",$dateTime);
                                  }
                                  $rem_date='';
                                  if($halerts[0]->alert_reminder!='')
                                  {
                                    $dateTime = date("d-m-Y",  strtotime($halerts[0]->alert_reminder));
                                    $rem_date = str_replace("-",".",$dateTime);
                                  } ?>
                                  <p><?php echo ($halerts[0]->alert_date != '' && $halerts[0]->alert_date != '0000-00-00')?$alt_date:''; ?></p> 
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <img src="<?php echo base_url();?>asset/css/img/icons/rose.png" alt="rose" class="of-circle-cus"> 

            <div class="documents-section my-cus-doc bg-gray oter-info-fooer">
              <div class="documents-color"> </div>
              <div class="container">
                <div class="row ">
                  <div class="col s12 m12 l12">
                    <div class="documents">
                      <p>Alert Info</p>
                    </div>
                  </div>
                </div>
                <div class="row cre-pre-row">
                  <div class="col s12 m12 l3">
                    <div class="row">
                      <div class="col s12 m12 l2">
                        <img src="<?php echo base_url();?>asset/images/circle.png" alt="circle" class="circle-icon">
                      </div>
                      <div class="col s12 m12 l8 credit-p borr-r">
                        <P>REMINDER DATE</P><span><?php echo ($halerts[0]->alert_reminder != '' && $halerts[0]->alert_reminder != '0000-00-00')?$rem_date:''; ?></span>
                      </div>
                     
                    </div>
                  </div>
                  <div class="col s12 m12 l3">
                    <div class="row ">
                      <div class="col s12 m12 l2">
                        <img src="<?php echo base_url();?>asset/images/circle.png" alt="circle" class="circle-icon">
                      </div>
                      <div class="col s12 m12 l8 credit-p borr-r">
                        <P>TYPES OF ALERT</P>
                          <?php 
                            $bell='bell-icon.png';$msg='msg-gicon.png';$mail='mail-icon.png';
                            if($halerts[0]->alert_notification==0){
                              $bell='notifications.png';
                            }
                            if($halerts[0]->alert_msg==0){
                              $msg='msg-icon.png';
                            }
                            if($halerts[0]->alert_mail==0){
                              $mail='nodi.png';
                            }
                           ?>
                        <span><img class="icon-img-noti" src="<?= base_url(); ?>asset/css/img/icons/<?= $bell; ?>"><img class="icon-img-noti" src="<?= base_url(); ?>asset/css/img/icons/<?= $msg; ?>"><img class="icon-img-noti" src="<?= base_url(); ?>asset/css/img/icons/<?= $mail; ?>" alt="noti"></span>
                      </div>
                      <div class="col s12 m12 l2"></div>
                    </div>
                  </div>
                    <div class="col s12 m12 l2">
                    <div class="row">
                      <div class="col s12 m12 l2">
                        <img src="<?php echo base_url(); ?>asset/images/circle.png" alt="circle" class="circle-icon">
                      </div>
                      <div class="col s12 m12 l10 credit-p borr-r">
                        <p>MOBILE NUMBER</p>
                        <span><?= $halerts[0]->alert_mobile; ?></span>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m12 l4">
                    <div class="row">
                      <div class="col s12 m12 l2">
                        <img src="<?php echo base_url();?>asset/images/circle.png" alt="circle" class="circle-icon">
                      </div>
                      <div class="col s12 m12 l10 credit-p">
                        <p>EMAIL ADDRESS</p>
                        <span>
                          <?= $halerts[0]->alert_email; ?>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
            <img src="<?php echo base_url();?>asset/css/img/icons/off-cir-ic.png" alt="off-cir" class="off2-cir-ic">
            <div class="row">
              <div class="col l12 s12 m12 pb-1"></div>
            </div>
          </div>
        </section>


        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
<div id="view_deactivate_calerts" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4><span class="cstatus"></span> Credit Period Alert</h4>

      <input type="hidden" id="remove_calert_id" name="remove_calert_id" value="" />

      <input type="hidden" id="alt_status" name="alt_status" value="" />
      <input type="hidden" id="alt_type" name="alt_type" value="" />
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to <span class="cstatus"></span> this credit period alert?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv text-center">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close  active_deactive_calerts_yes cstatus" style="color: #fff !important;">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>


<?php $this->load->view('template/footer.php');?>