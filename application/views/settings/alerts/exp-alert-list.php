 <?php $this->load->view('template/header.php');?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
     
   <!-- START MAIN -->
<style type="text/css">
table.dataTable thead .sorting ,table.dataTable thead .sorting_asc,table.dataTable thead .sorting_desc {
  background-position: 125px 17px !important;
}
.btn-date {
  margin: 0 0 0 3px !important;
}
.btn-stated {
  max-width: 73px !important;
}
.icon-img-noti {
  width: 22px;
  margin: 0 5px 0 2px;
}
.addnew.btn-dropdown-select span.caret {
  color: #FFFF;
}
.addnew.btn-dropdown-select > input.select-dropdown{
 color: #FFFF !important;
/* padding: 0 28px 0 18px !important;*/
  max-width: 100%;
  width: 152px;
}
.addnew.dropdown-content li > span{
  color: #000!important;
}
.btn-welcome {
  padding: 0px 0 !important;
}
.welcome-button {
  float: left;
}

 .dataTables_length {
  
    margin-left: 500px;
}

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;
  
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 165px !important;
  margin-left: 3px;

}

.dataTables_length {
  
    margin-left: 500px;
}

#my_exp_alert_length{
    border:1px solid #B0B7CA;
    height:38px;
    border-radius:4px;
    width:88px;
    margin-top:5px;
    margin-left:780px;
  }
  
  #my_exp_alert_length .select-wrapper input.select-dropdown {
    margin-top:-3px !important;
    margin-left:10px !important;
  }
  
  #my_exp_alert_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}
	
	#my_exp_alert_length .dropdown-content {
		min-width: 95px;
	}

/*----------END SEARCH DROPDOWN CSS--------*/
input[type=text]:not(.browser-default) {
  font-size: 12px !important;
}
.dropdown-content li > span {
  font-size: 12px;
  color: #666;
}
.icon-img{
    margin: 0 30px 0 2px;
}
	.calerts_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}
	
	::placeholder{
		color:#000000 !important;			
		font-size:12px !important;
	}
	
	.ex{
		margin:-28px 0 0 48px !important;
	}
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
         <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-theme-gray alerts-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Expense Alerts <small class="grey-text">(8 Total)</small></h5>
                  <ol class="breadcrumbs">
                    <li class="active">MY SETTINGS / Expense ALERTS</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
                  <a class="btn btn-theme btn-large right add-new-profile-btn service" href="<?php echo base_url(); ?>settings/Expense_alert_create">ADD NEW ALERT</a>
                  <!-- <a class="btn btn-theme btn-large right add-new-profile-btn service" href="<?php echo base_url(); ?>services/add-service">ADD NEW ALERT FOR</a>
                  --> <!-- <select name="settings_alerts" id="settings_alerts" class='btn-theme right border-radius-6 btn-dropdown-select border-split-form select-like-dropdown by-statys addnew'>
                  <option value=''>ADD NEW ALERT FOR</option>
                  <option value="add-client-alert">CLIENTS</option>
                  <option value="add-item-alert">ITEMS</option> -->
                  <!-- <option value="add-jv-alert">JOURNAL VOUCHER</option> -->
                  <!-- <option value="add-credit-period-alert">CREDIT PERIOD</option>
                  <option value="add-birthday-anniversary-alert">BIRTHDAY & ANNIVERSARY</option>
                </select> -->
              </div>
            </div>
          </div>
        </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12 ">
                 <a href="javascript:void(0);" class="addmorelink right" onclick="reset_alertsfilter('calert');" title="Reset all">Reset</a>
                </div>
                <div class="col l5 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk4'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown_bulk4' class='dropdown-content'>
                  
                    <li><a id="email_multiple_calerts"><i class="material-icons">email</i> Email</a></li>
                    <li><a id="download_multiple_calerts" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" alt="export" style="width: 15px;height: 22px;"><p class="ex">Export</p></a></li>
                    <li><a id="print_multiple_calerts"><i class="material-icons">print</i>Print</a></li>
                    <li><a id="deactive_multiple_calerts" data-multi_calerts="0"><i class="material-icons">delete</i>Delete</a></li>
                  </ul>
                  <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_ealerts" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>
                </div>
                 <div class="col l6 s12 m12 right">
                  <div class="action-btn-wapper right sort">
                    <!-- <select class='ml-3px border-radius-6 btn-dropdown-select border-split-form select-like-dropdown select-cmpy' id="calerts_customer" name="calerts_customer">  -->
                    <select class="js-example-basic-single .ml-3px" name="ealerts_customer" id="ealerts_customer">
                    <option value="">FOR EXPENSE</option>
                    <option value=''>ALL</option>
                    </select>
                    <input type="text" placeholder="Start Date" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="ealerts_start_date" name="ealerts_start_date" readonly="readonly">
                    <input type="text" placeholder="End Date" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="ealerts_end_date" name="ealerts_end_date" readonly="readonly">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12" id="print_services_tbl">
                <form>
                  <table id="my_exp_alert" class="responsive-table display table-type1 mb-2" cellspacing="0">
                    <thead>
                      <tr>
                        <th >
                          <input type="checkbox" id="calerts_bulk" name="calerts_bulk" class="filled-in purple" />
                          <label for="calerts_bulk"></label>
                        </th>
                        <th style="width:100px;" >ALERT NO.</th>
                        <th style="width:100px;" >ALERT DATE</th>
                        <th style="width:130px;">Expanse NAME</th>
                        <th style="width:130px;">ALERT PARAMETER </th>
                        <th style="width: 100px">TARGET</th>
                        <th style="width: 100px">BY WHEN</th>
                        <th style="width: 100px">ALERT TYPE</th>
                        <th style="width:5px;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                   
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>           
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
<div id="deactivate_calerts" class="modal modal-set">

    <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content">

      <div class="modal-header">

        <h4> Remove Customer Alert </h4>
        
        <input type="hidden" id="remove_calert_id" name="remove_calert_id" value="" />

        <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

      </div></div>

      <div class="modal-body confirma">

        <p>Are you sure you want to remove this Customer Alert?</p>

      </div>

      <div class="modal-content">

        <div class="modal-footer">

          <div class="row">

            

            <div class="col l12 s12 m12 cancel-deactiv">

              <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>


              <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close delete_calerts">REMOVE</button>

            </div>

          </div>

        </div>

      </div>

    </div>

<div id="email_popup_client_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
    <?php $this->load->view('settings/alerts/email_popup_client'); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
  }); 
</script>
<?php $this->load->view('template/footer.php'); ?>