<?php $this->load->view('template/header'); ?>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
<style type="text/css">
.addnew.btn-dropdown-select span.caret {

  color: #FFFF;
}
.addnew.btn-dropdown-select > input.select-dropdown{
 color: #FFFF !important;
 padding: 0 28px 0 18px !important;
 max-width: 100%;
}
.addnew.dropdown-content li > span{
  color: #000!important;
}
.btn-welcome {
  padding: 0px 0 !important;
}
.welcome-button {
  float: left;
  width: 90%;
}
.addnew {
 width: 60% !important; 
 margin-right: 50px;
}
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <div class="page-header">
            <div class="container">
              <h2 class="page-title">Set Alerts</h2>
            </div>
          </div>

          <div class="page-content">
            <div class="container row">
              <div class="welcom-container">
                <div class="Welcome-image">
                  <img src="<?php echo base_url(); ?>asset/css/img/icons/blank-stage/ei-blank.gif" class="gifblank" alt="ei-blank">
                </div>
                <div class="welcom-content">
                  <p class="welcom-title">Hey <?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?>!</p>
                  <p class="welcom-text">You are currently have no alerts listed.<br/>Let's get started by adding one below.</p>
     <div class="col s12 m12 l12">
           <div class="col s12 m4 l4">
           </div>
           <div class="col s12 m4 l4">       
                  <p class="welcome-button" style="">
                   <select name="settings_alerts" id="settings_alerts" class='btn-theme right border-radius-6 btn-dropdown-select border-split-form select-like-dropdown by-statys addnew'>
                  <option value=''>SET NEW ALERTS FOR</option>
                  <option value="add-client-alert">CLIENTS</option>
                  <option value="add-item-alert">ITEMS</option>
                  <option value="add-credit-period-alert">CREDIT PERIOD</option>
                  <option value="add-birthday-anniversary-alert">BIRTHDAY & ANNIVERSARY</option>
                </select>
                     <!-- <a class="btn btn-welcome add-new-cus" href="add-sales-invoice">CREATE NEW INVOICE</a> -->
                  </p>
            </div>
             <div class="col s12 m4 l4">
           </div> 
    </div>         
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->

      <!-- ================================================
    Scripts
    ================================================ -->
      <?php $this->load->view('template/footer'); ?>
