<?php $this->load->view('my-community/Cohorts-header');?>
   <!-- START MAIN -->
<style type="text/css">
		.dataTables_scrollBody{
			overflow:hidden !important;
			height:100% !important;
		}

		.dataTables_scrollHead{
			margin-bottom:-24px !important;
		}

    .btn-stated {
    background-position: 91px center !important;
}

		table.dataTable thead .sorting {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_asc {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_desc {
		  background-position: 110px 15px !important;
		}
table.dataTable thead .sorting ,table.dataTable thead .sorting_asc,table.dataTable thead .sorting_desc {
  background-position: 125px 17px !important;
}
.btn-date {
  margin: 0 0 0 3px !important;
}
.btn-stated {
  max-width: 80px !important;
	background-position: 95px center !important;
}
.icon-img-noti {
  width: 22px;
  margin: 0 5px 0 2px;
}
.addnew.btn-dropdown-select span.caret {
  color: #FFFF;
}
.addnew.btn-dropdown-select > input.select-dropdown{
 color: #FFFF !important;
/* padding: 0 28px 0 18px !important;*/
  max-width: 100%;
  width: 152px;
}
.addnew.dropdown-content li > span{
  color: #000!important;
}
.btn-welcome {
  padding: 0px 0 !important;
}
.welcome-button {
  float: left;
}

 .dataTables_length {

    margin-left: 500px;
}

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 250px !important;
  margin-left: 3px;

}

.dataTables_length {

    margin-left: 500px;
}

#my-generic-alerts_length{
    border:1px solid #B0B7CA;
    height:38px;
    border-radius:4px;
    width:88px;
    margin-top:5px;
    margin-left:52%;
  }

  #my-generic-alerts_length .select-wrapper input.select-dropdown {
    margin-top:-3px !important;
    margin-left:10px !important;
  }

  #my-generic-alerts_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}

	#my-generic-alerts_length .dropdown-content {
		min-width: 95px;
		margin-top:-55% !important;
	}

/*----------END SEARCH DROPDOWN CSS--------*/
input[type=text]:not(.browser-default) {
  font-size: 12px !important;
}
.dropdown-content li > span {
  font-size: 12px;
  color: #666;
}
.icon-img{
    margin: 0 30px 0 2px;
}
	.calerts_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	::placeholder{
    font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.sticky {
		position: fixed;
		top: 70px;
		min-width: 77.6%;
		max-width: 80%;
		z-index:999;
		background: white;
		color: black;
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}

	.ex{
		margin:-28px 0 0 48px !important;
	}

  [type="checkbox"].filled-in:not(:checked) + label:after {
    border: 2px solid transparent;
    cursor: default !important;
  }
  .table-type1 [type="checkbox"] + label {
    cursor: default;
}
</style>
    <div id="main" style="padding:10px 0 0 0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
         <?php //$this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-theme-gray alerts-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
            <div class="col l12 s12 m12 ">
                 <a style="padding-top:0px !important;" href="javascript:void(0);" class="addmorelink right" onclick="reset_alertsfilter('aalert');" title="Reset all">Reset</a>
                </div>
			<div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Activity Alerts <small class="grey-text">(<span id="alert_count"></span>Total <?php echo count($alerts);?>)</small></h5>
                  <ol class="breadcrumbs">
						<li class="active">MY SETTINGS / ALERTS / ACTIVITY ALERTS</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
				<div class="action-btn-wapper right sort">
					<input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="aalerts_start_date" name="galerts_start_date" readonly="readonly">
                    <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="aalerts_end_date" name="galerts_end_date" readonly="readonly">
                  <!--a class="btn btn-theme btn-large right add-new-profile-btn service" href="<?php //echo base_url(); ?>settings/add-activity-alert">ADD NEW ALERT</a-->
				 </div> 
              </div>
            </div>
          </div>
        </div>

          <!--div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l5 s12 m12" style="margin: 0 0 0 -20px;">
                  <!--
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk4'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown_bulk4' class='dropdown-content'>
                    <li><a id="email_multiple_calerts"><i class="material-icons">email</i> Email</a></li>
                    <li><a id="download_multiple_calerts" style="display: flex;">
                      <img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" alt="export" style="width: 15px;height: 22px;">Export
                    </a>
                    </li>
                    <li><a id="print_multiple_calerts"><i class="material-icons">print</i>Print</a></li>
                    <li><a id="deactive_multiple_calerts" data-multi_calerts="0"><i class="material-icons">delete</i>Delete</a></li>
                  </ul>
                  -->
                  <!--a class="filter-search btn-search btn" style="display: none">
                    <input type="text" name="search" id="search_aalerts" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>

                </div>
                 <div class="col l6 s12 m12 right">
                  <div class="action-btn-wapper right sort">
                    <!--select class="js-example-basic-single .ml-3px" name="calerts_customer" id="calerts_customer">
                    <option value="">FOR CLIENTS / VENDORS</option>
                    <option value=''>ALL</option>
                    <?php //if($customers != '') {
                      //foreach($customers as $customer)  { ?>
                        <option value="<?php //echo $customer['id']; ?>"><?php //echo strtoupper($customer['code']." - ". $customer['name']); ?></option>
                        <?php // } }?>
                    </select-->
                    
                  <!--/div>
                </div>
              </div>
            </div>
          </div-->
          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12" id="print_services_tbl">
                  <table id="my-generic-alerts" class="responsive-table display table-type1" cellspacing="0" style="margin-bottom:15px;">
                    <thead id="fixedHeader">
                      <tr>
                        <!--th style="width:1%;">
                          <input type="checkbox" id="aalerts_bulk" name="aalerts_bulk" class="filled-in purple" />
                          <label for="aalerts_bulk"></label>
                        </th-->
                        <th style="width:15%;">ALERT NO. <br> & DATE</th>
                        <th style="width:25%;">ACTIVITY TITLE</th>
                        <th style="width:15%;">DATE</th>
                        <th style="width:15%;">TIME</th>
                        <th style="width:15%">BY WHEN</th>
                        <th style="width:20%">ALERT TYPE</th>
                        <th style="width:5%;">ACTIONS</th>
                      </tr>
                    </thead>

                    <tbody class="scrollbody">

                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->

      <div id="deactivate_aalerts" class="modal modal-set">
        <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
        <div class="modal-content">
          <div class="modal-header">
            <h4> Delete Activity Alert </h4>
            <input type="hidden" id="remove_aalert_id" name="remove_aalert_id" value=""/>
            <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
          </div>
        </div>

        <div class="modal-body confirma">
          <p>Are you sure you want to delete this alert?</p>
        </div>

        <div class="modal-content">
          <div class="modal-footer">
            <div class="row">
              <div class="col l12 s12 m12 cancel-deactiv">
                <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close delete_aalerts">DELETE</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="email_popup_activity_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
          <?php $this->load->view('settings/alerts/email_popup_activity'); ?>
      </div>

      <script type="text/javascript">
          $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
      </script>

      <?php $this->load->view('template/footer.php'); ?>
