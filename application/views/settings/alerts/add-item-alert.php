<?php $this->load->view('template/header.php'); ?>
    <!-- START MAIN -->
    <style>
    input.valid:not([type]), input.valid:not([type]):focus, input[type=text].valid:not(.browser-default) {
          /*border-bottom: 0px !important;*/
    }
    .border-split-form .select-wrapper{
       padding: 7px 0 2px 0 !important;
    }
     /*.fl-r img.green-bel {
          margin: 0px -16px 0 19px;
          width: 20px;
      }*/
      .full-bg{
        border-bottom: none !important;
        padding-top: 10px !important;
   		display: inline-flex;
      }
      img.green-bel {
      	width: 21px !important;
   		 margin: 12px 0 0 13px;
      }
      .no-type{
  	    color: #666;
		    font-size: 12px;
		    margin-top: 17px !important;
		    font-weight: normal;
		    text-transform: uppercase;
      }
      .w-25{
            width: 25px !important;
      }
      .select-wrapper label.error:not(.active) {
         margin: -30px 0 0 -11px;
      }
      .selected_notification {
          border:none !important;
      }
      a.info-ref.tooltipped.info-tooltipped {
          position: absolute;
      }
/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 35px;
  color: #666;
  font-weight: 400;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
  right: 14px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
  width: 98%;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 53px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 91%;
    max-width: 100%;
    background: #fff;
}
.select2-container--open .select2-dropdown--below {
  margin-top: -15px;
}
/*----------END SEARCH DROPDOWN CSS--------*/
/*---Dropdown error message format---*/
.select-wrapper + label.error{
 margin: 18px 0 0 -10px;

} 
select.error + label.error:not(.active){

 margin: -20px 0 0 -10px; 
}
select + label.error.active{
  margin-left: -10px;
}
/*---End dropdown error message format---*/
      
  </style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar.php'); ?> 
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
         <?php 
         $email= $this->user_session['email'] ;
         $mobile= $this->user_session['mobile']; ?>
         <input id="email" name="email" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $email; ?>" readonly="readonly">
           <input id="mobile" name="mobile" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $mobile; ?>" readonly="readonly">
        <form class="create-company-form border-split-form" id="add_item_alert" name="add_item_alert" method="post">
          <section id="content">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12">
                  <a class="go-back underline" href="<?php echo base_url(); ?>settings/item-alerts">Back to My Alerts</a> 
                  </div>
                  <div class="col l6 s12 m6">
                  </div>
                </div>
              </div>
              <div class="page-content">
                <div class="row">
                  <div class="col s12 m12 l3"></div>
                  <div class="col s12 m12 l6">
                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
                      <div class="box-header">
                        <h3 class="box-title">New Alert for Item / Expense</h3>
                      </div>
                      <div class="box-body">
                      	<div class="row ">
                             <div class="col s12 m12 l12">
                                <div class="input-field col s12 m12 l6 padd-n">
                                    <label for="alert_number" class="full-bg-label">Alert No.</label>
                                   <input id="alert_number" name="alert_number" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= $alertNo; ?>" readonly="readonly">
                            
                                </div>
                                <div class="input-field col s12 m12 l6 padd-n">
                                  <label for="alert_date" class="full-bg-label">Alert Date</label>
                                  <input id="alert_date" name="alert_date" class="readonly-bg-grey full-bg adjust-width border-top-none valid" type="text" value="<?= date('d/m/Y'); ?>" readonly="readonly">
                                </div>
                              </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 padd-n">
                                <div class="row">
                                  <div class="input-field">
                                     <a class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-html="true" data-tooltip="Select a suitable name for future reference.<br/>For example: If you want an alert for revenue <br/>crossing 10 lakhs from this item, you can call <br/>this Alert as Revenue - 10L"></a>
                                      <label for="alert_name" class="full-bg-label">Alert Name</label>
                                      <input id="alert_name" name="alert_name" class="full-bg adjust-width border-top-none border-right" type="text">
                                  </div>
                                </div>
                              </div>
                              
                              <div class="col s12 m12 l6 padd-n border-bottom">
                                <div class="input-field" style="margin-top:3px !important;">
                                  <label for="parent_name" class="full-bg-label select-label">SELECT ITEM / EXPENSE</label>
                                        <select class="js-example-basic-single" name="parent_name" id="parent_name">
                                        <option value="">SELECT ITEM / EXPENSE</option>
                                        
                                        <option value="item">ITEM</option>
                                        <option value="expense">EXPENSE</option>
                                       
                                    </select>
                                </div>
                              </div>
                              
                            </div>
						 </div>
						 
						 <div class="row">
                            <div class="col s12 m12 l12">
								<div class="col s12 m12 l12 padd-n">
                                <div class="input-field">
                                  <label for="parent_id" class="full-bg-label select-label">SELECT ITEM</label>
                                        <select class="js-example-basic-single" name="parent_id" id="parent_id">
                                        <option value="">SELECT ITEM *</option>
										<?php 
										
										foreach($services as $ser){ ?>
											<option value="<?=$ser->service_id?>"><?=$ser->service_name?></option>
										<?php }?>
                                        
                                      </select>
                                </div>
								</div>
							</div>
						 </div>
                         </div>
                         </div>
                         <div class="step22">
	                      <h4 class="box-inner-title">Alert Info</h4>
	                      <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="a4791c04-e6b5-9640-532d-84ec1e9ac670"></a>
	                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6">
	                        <div class="box-body">
                          <div class="row pl-2 pr-2">
                            <div class="col l6 s12 m6 fieldset">
                              <div class="input-field ">
                                <label class="full-bg-label">By When<span class="required_field"> *</span></label>
                                <input name="alert_reminder" id="alert_reminder" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off" readonly="readonly">
                            </div>
                            </div>
                              <div class="col s12 m12 l6 input-set">
                                  <div class="input-field">
                                   <label class="full-bg-label select-label" for="alert_condition">SET ALERT PARAMETER</label>
                               			<select id="alert_condition" name="alert_condition" class="country-dropdown check-label" >
                                      <option value=""></option>
                                      <option value="Revenue Exceeds Target">Rev / Exp Exceeds Target</option>
                                      <option value="Revenue Below Target">Rev / Exp Below Target</option>
                                
                                		</select>
                                  </div>
                              </div>
                          </div>
			                <div class="row pl-2 pr-2">

			                  <div class="col l6 s12 m6 fieldset">
			                    <div class="input-field">
			                      <label class="full-bg-label">SET TARGET<span class="required_field">*</span></label>
			                      <input name="alert_target" id="alert_target" class="full-bg adjust-width gstin border-bottom-none" type="text">
			                    </div>
			                  </div>

			                  <div class="col l6 s12 m6 full-bg">
			                  	<span id="notification_error"></span>
			                      <input type="hidden" name="alert_notification" id="alert_notification" value=""/>
			                      <input type="hidden" name="alert_msg" id="alert_msg" value="" />
			                      <input type="hidden" name="alert_mail" id="alert_mail" value="" />
			                     
			                      <span class="no-type">TYPES OF ALERT<span class="required_field">*</span></span> 
			                      <a class="cur noti_type" data-noti_type="1"><img src="<?php echo base_url(); ?>asset/css/img/icons/bell-grey.png" alt="bel" class="green-bel" title="Notification"></a>
			                      <a class="cur noti_type" data-noti_type="2"><img src="<?php echo base_url(); ?>asset/css/img/icons/msg-grey.png" alt="msg" class="green-bel msg" title="Message"></a>
			                      <a class="cur noti_type" data-noti_type="3"><img src="<?php echo base_url(); ?>asset/css/img/icons/mail-grey.png" alt="email" class="email green-bel w-25" title="Email"></a>
			                  
			                </div>
			                
			                  	
			                </div>
			                 <div class="row pl-2 pr-2">
			                  <div class="col l6 s12 m12 fieldset noti_mobile">
			                    <div class="input-field">
			                      <label class="full-bg-label">ENTER MOBILE NUMBER</label>
			                      <input name="alert_mobile" id="alert_mobile" class="full-bg adjust-width gstin border-bottom-none numeric_number" type="text" readonly="readonly">
			                    </div>
			                  </div>
			                  <div class="col l6 s12 m12 fieldset noti_email">
			                    <div class="input-field">
			                      <label class="full-bg-label">ENTER EMAIL ADDRESS</label>
			                      <input id="alert_email" name="alert_email" class="full-bg adjust-width border-bottom-none" type="email" readonly="readonly">
			                    </div>
			                  </div>
			                </div>
                          
                                </div>
                            </div>
                          </div>

                    <div class="step1 footer-btns">

                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="form-botom-divider"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s12 m12 l6 right">
                          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" type="submit">Save</button>
                          <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right" type="button" onclick="location.href = '<?php echo base_url(); ?>settings/item-alerts';">Cancel</button>
                          <?php //} ?>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m12 l3"></div>
                </div>
              </div>
            </div>           
          </section>
        </form>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <script type="text/javascript">
        $(document).ready(function() {
          $('.js-example-basic-single').select2();
            $('.select2-selection__rendered').each(function () {
             $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
          });
          $("select").change(function () {
            if($(this).val()!=''){
              $(this).valid();
              $(this).closest('.input-field').find('.error').remove();
              
            }
           });
        });
      </script>
<?php $this->load->view('template/footer.php'); ?>