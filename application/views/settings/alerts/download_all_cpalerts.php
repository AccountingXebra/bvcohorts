<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('Credit Period Alerts');
$this->excel->getActiveSheet()->setCellValue('A1', 'Credit Period Alerts');
$this->excel->getActiveSheet()->mergeCells('A1:G1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('A3', 'ALERT NO.');
$this->excel->getActiveSheet()->setCellValue('B3', 'ALERT DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'CLIENT NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'REMINDER DATE');


$this->excel->getActiveSheet()->setCellValue('E3', 'ALERT NOTIFICATION');
$this->excel->getActiveSheet()->setCellValue('F3', 'ALERT MOBILE');
$this->excel->getActiveSheet()->setCellValue('G3', 'ALERT EMAIL');


$this->excel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['alert_number']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, date('d-m-Y',strtotime($value['alert_date'])));
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['cust_name']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, date('d-m-Y',strtotime($value['alert_date'])));
	if($value['alert_notification']==1){
		$this->excel->getActiveSheet()->setCellValue('E'.$count, 'Yes');
	}
	if($value['alert_msg']==1){
		$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['alert_mobile']);
	}
	if($value['alert_mail']==1){
		$this->excel->getActiveSheet()->setCellValue('G'.$count, $value['alert_email']);
	}
	
	$count++;
}

$filename='Credit Period Alerts '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
ob_end_clean();	
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>