<?php $this->load->view('template/header.php'); ?>
    <!-- START MAIN -->
<style>
    input.valid:not([type]), input.valid:not([type]):focus, input[type=text].valid:not(.browser-default) {
          /*border-bottom: 0px !important;*/
    }
    .border-split-form .select-wrapper{
       padding: 7px 0 2px 0 !important;
    }
     /*.fl-r img.green-bel {
          margin: 0px -16px 0 19px;
          width: 20px;
      }*/
      .full-bg{
        border-bottom: none !important;
        padding-top: 10px !important;
   		display: inline-flex;
      }
      img.green-bel {
      	width: 21px !important;
   		 margin: 12px 0 0 13px;
      }
      .no-type{
      	    color: #666;
		    font-size: 12px;
		    margin-top: 17px !important;
		    font-weight: normal;
		    text-transform: uppercase;
      }
      .w-25{
            width: 25px !important;
      }
      .select-wrapper label.error:not(.active) {
         margin: -30px 0 0 -11px;
      }
      .selected_notification {
          border:none !important;
      }

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container {
  width: 100% !important;
}
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 35px;
  color: #666;
  font-weight: 400;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 53px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
  border-top-left-radius: 0;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
   /* border: none;*/
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93.5%;
    max-width: 100%;
    background: #fff;
    border-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
.select2-container--open .select2-dropdown--below {
  margin-top: -15px;
/*  border-top: 1px solid #aaa;
  border-radius: 5px !important;*/
}
ul#select2-currency-results {
  width: 97%;
}

/*.select2-container--open .select2-dropdown--above{
    border-top: 1px solid #aaa;
    border-radius: 5px !important;
}*/

/*----------END SEARCH DROPDOWN CSS--------*/



/*------------- START Aniket CSS-----------------------*/

#select2-currency-container {
    font-size: 14px;
    color: #000;
    font-weight: 500;
}
/*------------- END Aniket CSS-----------------------*/  
/*---Dropdown error message format---*/
.select-wrapper + label.error{
 margin: 18px 0 0 -10px;

} 
select.error + label.error:not(.active){

 margin: -20px 0 0 -10px; 
}
select + label.error.active{
  margin-left: -10px;
}
/*---End dropdown error message format---*/    

#add_ba_alert .input-set ul.dropdown-content {
    width: 104.5% !important;
    border-radius: 0 0 8px 8px;
    margin:  35px 0 0 -10px !important;
}
</style>

<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar.php'); ?> 
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
         <?php 
         $email= $this->user_session['email'] ;
         $mobile= $this->user_session['mobile']; ?>
         <input id="email" name="email" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $email; ?>" readonly="readonly">
           <input id="mobile" name="mobile" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $mobile; ?>" readonly="readonly">
        <form class="create-company-form border-split-form" id="add_ba_alert" name="add_ba_alert" method="post">
          <section id="content">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12">
                  <a class="go-back underline" href="<?php echo base_url(); ?>settings/birthday-anniversary-alerts">Back to My Alerts</a> 
                  </div>
                  <div class="col l6 s12 m6">
                  </div>
                </div>
              </div>
              <div class="page-content">
                <div class="row">
                  <div class="col s12 m12 l3"></div>
                  <div class="col s12 m12 l6">
                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
                      <div class="box-header">
                        <h3 class="box-title">New Alert for Birthday & Anniversary</h3>
                      </div>
                      <div class="box-body">
                      	<div class="row ">
                         <div class="col s12 m12 l12">
                            <div class="input-field col s12 m12 l6 padd-n">
                                <label for="alert_number" class="full-bg-label">Alert No.</label>
                               <input id="alert_number" name="alert_number" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= $alertNo; ?>" readonly="readonly">
                        
                            </div>
                            <div class="input-field col s12 m12 l6 padd-n">
                              <label for="alert_date" class="full-bg-label">Alert Date</label>
                              <input id="alert_date" name="alert_date" class="readonly-bg-grey full-bg adjust-width border-top-none valid" type="text" value="<?= date('d-M'); ?>" readonly="readonly">
                            </div>
                          </div>
                        </div>
                        <div class="row pl-2 pr-2">
                              <div class="col s12 m12 l6 input-set">
                                  <div class="input-field">
                                    <label for="alert_occasion" class="full-bg-label select-label">Select Occasion</label>
                                       <select id="alert_occasion" name="alert_occasion" class="country-dropdown check-label">
                                            <option value=""></option>
                                            <option value="Birthday">Birthday</option>
                                            <option value="Anniversary">Anniversary</option>
                                       </select>
                                  </div>
                              </div>
                              <div class="col s12 m12 l6 input-set" style="padding:0px !important;">
                                    <div class="input-field">
                                      <label class="full-bg-label">Occasion Date<span class="required_field"> *</span></label>
                                      <input name="occasion_date" id="occasion_date" class="full-bg adjust-width border-top-none birthdatepicker icon-calendar-gray" type="text" autocomplete="off">
                                    </div>
                              </div>
  
                          </div>
                          <div class="row ">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set border-field">
                                  <div class="input-field label-active">
                                   <label class="full-bg-label select-label" for="alert_stakeholder">Select Stakeholder<span class="required_field"> *</span></label>
                                    <select id="alert_stakeholder" name="alert_stakeholder" class="country-dropdown check-label" >
                                          <option value="">Select</option>
                                          <option value="employee">Employee</option>
                                          <option value="client" >Client</option>
                                           <option value="vendor">Vendor</option> 
                                    </select>
                                  </div>
                              </div>
                              <div class="col s12 m12 l6 input-set border-field" id="company_show_client" hidden>
                                <div class="input-field label-active">
                                   <label class="full-bg-label select-label" for="parent_id">Client Name<span class="required_field"> *</span></label>
                                      <select class="country-dropdown check-label" name="parent_id" id="parent_id">
                                        <option value="">Select Client</option>
                                         <?php foreach($customers as $cust){ ?>
                                        <option value="<?= $cust->cust_id; ?>"><?= $cust->cust_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set border-field" id="company_show_comp" hidden>
                                <div class="input-field label-active">
                                   <label class="full-bg-label select-label" for="alert_condition">Company Name<span class="required_field"> *</span></label>
                                      <select class="country-dropdown check-label" name="parent_id1" id="parent_id1">
                                       <option value="<?= $bus_data[0]->bus_id; ?>" selected><?= $bus_data[0]->bus_company_name; ?> </option>
                                         
                                    </select>
                                </div>
                              </div>

                                  <div class="col s12 m12 l6 input-set border-field" id="company_show_vendor" hidden>
                                <div class="input-field label-active">
                                   <label class="full-bg-label select-label" for="alert_condition">Vendor Name<span class="required_field"> *</span></label>
                                     <select class="country-dropdown check-label" name="parent_id2" id="parent_id2">
                                        <option value="">Select Vendor</option>
                                         <?php foreach($vendors as $vend){ ?>
                                        <option value="<?= $vend->vendor_id; ?>"><?= $vend->vendor_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                              </div>
                            
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12" id="code_of_employee" hidden>
							<div class="col s12 m12 l12 input-set border-field">
                                <div class="input-field">
                                   <label for="emp_code" class="full-bg-label select-label">Employee ID & Name<span class="required_field"> *</span></label>
                                    <select class="country-dropdown check-label" name="emp_code" id="emp_code">
										
										<option value=""></option>
										<?php foreach($employee as $emp){ ?>
										<option value="<?= $emp->emp_id; ?>"><?=$emp->employee_id."-".$emp->first_name." ".$emp->last_name; ?></option>
										<?php } ?>
									</select>
                                </div>
                            </div>
							</div>
							<div class="col s12 m12 l12" id="contact_person" hidden>
							<div class="col s12 m12 l12 input-set border-field">
                                <div class="input-field">
                                   <label for="contact_per" class="full-bg-label select-label">Contact Person Name<span class="required_field"> *</span></label>
                                    <select class="country-dropdown check-label" name="contact_per" id="contact_per">
									
										<option value=""></option>
										<?php //foreach($customers as $cust){ ?>
										<option value="<?//= $cust->cust_id; ?>"><?//= $cust->cust_name; ?></option>
										<?php //} ?>
									</select>
                                </div>
                              </div>
							</div>
							<!--div class="col s12 m12 l12">
                                  <div class="input-field">
                                      <label for="zipcode" class="full-bg-label">Employee Code & Name<span class="required_field"> *</span></label>
                                      <input id="alert_name" name="alert_name" class="full-bg adjust-width" type="text" autocomplete="off">
                                  </div>         
                            </div-->
                          </div>


                            </div>
                         </div>
                         <div class="step22">
	                      <h4 class="box-inner-title">Alert Info</h4>
	                     <!-- <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="a4791c04-e6b5-9640-532d-84ec1e9ac670"></a>-->
	                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6">
	                        <div class="box-body">
                          
			                <div class="row pl-2 pr-2">

			                   <div class="col l6 s12 m6 fieldset">
                              <div class="input-field ">
                                <label class="full-bg-label">Reminder Date<span class="required_field"> *</span></label>
                                <input name="alert_reminder" id="alert_reminder" class="full-bg adjust-width border-top-none birthdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">
                              </div>
                          </div>

			                  <div class="col l6 s12 m6 full-bg">
			                  	 <span id="notification_error"></span>
			                      <input type="hidden" name="alert_notification" id="alert_notification" value=""/>
			                      <input type="hidden" name="alert_msg" id="alert_msg" value="" />
			                      <input type="hidden" name="alert_mail" id="alert_mail" value="" />
			                     
			                      <span class="no-type">TYPES OF ALERT<span class="required_field">*</span></span> 
			                      <a class="cur noti_type" data-noti_type="1"><img src="<?php echo base_url(); ?>asset/css/img/icons/bell-grey.png" alt="bel" class="green-bel" title="Notification"></a>
			                      <a class="cur noti_type" data-noti_type="2"><img src="<?php echo base_url(); ?>asset/css/img/icons/msg-grey.png" alt="msg" class="green-bel msg" title="Message"></a>
			                      <a class="cur noti_type" data-noti_type="3"><img src="<?php echo base_url(); ?>asset/css/img/icons/mail-grey.png" alt="noti" class="green-bel w-25 email" title="Email"></a>
			                  
			                </div>
			               
			                  	
			                </div>
			                 <div class="row pl-2 pr-2">
			                  <div class="col l6 s12 m12 fieldset noti_mobile">
			                    <div class="input-field">
			                      <label class="full-bg-label">ENTER MOBILE NUMBER</label>
			                      <input name="alert_mobile" id="alert_mobile" class="full-bg adjust-width gstin border-bottom-none border-bottom-none numeric_number" type="text" readonly="readonly">
			                    </div>
			                  </div>
			                  <div class="col l6 s12 m12 fieldset noti_email">
			                    <div class="input-field">
			                      <label class="full-bg-label">ENTER EMAIL ADDRESS</label>
			                      <input id="alert_email" name="alert_email" class="full-bg adjust-width border-bottom-none border-bottom-none" type="email" readonly="readonly">
			                    </div>
			                  </div>
			                </div>
                          
                                </div>
                            </div>
                          </div>

                    <div class="step1 footer-btns">

                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="form-botom-divider"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s12 m12 l6 right">
                       <!--  <?php //if($this->session->userdata['user_session']['user_business'] == '2') {?>
                          <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right ml-5" type="button" onclick="location.href = '<?php echo base_url(); ?>My_services/manage_services_for_manufacturing';">Cancel</button>
                          <?php //} else { ?> -->
                          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" type="submit">Save</button>
                          <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right" type="button" onclick="location.href = '<?php echo base_url(); ?>settings/birthday-anniversary-alerts';">Cancel</button>
                          <?php //} ?>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m12 l3"></div>
                </div>
              </div>
            </div>           
          </section>
        </form>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <script type="text/javascript">
        $(document).ready(function() {

			  $("#code_of_employee").hide();	
              $('.js-example-basic-single').select2();
              $('.select2-selection__rendered').each(function () {
                $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
              });
          $("select").change(function () {
            if($(this).val()!=''){
              $(this).valid();
              $(this).closest('.input-field').find('.error').remove();
            }
           });

          $("#alert_stakeholder").change(function () {

            var id = $(this).val();

            if(id == "employee")
            {
              $("#company_show_client").hide();
               $("#company_show_vendor").hide();
              $("#company_show_comp").show();
			  $("#code_of_employee").show();	
			  $("#contact_person").hide();	
            }
             else if(id == "vendor")
            {
              $("#company_show_vendor").show();
              $("#company_show_client").hide();
              $("#company_show_comp").hide();
        $("#code_of_employee").hide();  
        $("#contact_person").show();  
            }else{
              $("#company_show_client").show();
              $("#company_show_comp").hide();
               $("#company_show_vendor").hide();
			  $("#code_of_employee").hide();	
			  $("#contact_person").show();	
            }


            });


          $("#parent_id ,#parent_id2").change(function () {
    var id = $(this).val();
   var stakeholder= $("#alert_stakeholder").val();

    $.ajax({

    url:base_url+'settings/get_contact_persons',

    type:"POST",

    data:{"id":id,"stakeholder":stakeholder},

    success:function(result){
      var data=JSON.parse(result);
       
        var opt='<option value=""></option>';
          
        for(i=0;i<data.length;i++){
         
          opt+='<option value='+data[i].cp_id+'>'+data[i].cp_name+'</option>';
        }
     $("select#contact_per").html(opt);
      $("select#contact_per").material_select();


      },

    });  
 });

        });


        

           


      </script>


<?php $this->load->view('template/footer.php'); ?>