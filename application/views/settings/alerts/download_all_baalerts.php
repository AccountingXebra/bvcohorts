<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('Birthday Anniversary Alerts');
$this->excel->getActiveSheet()->setCellValue('A1', 'Birthday/Anniversary Alerts');
$this->excel->getActiveSheet()->mergeCells('A1:H1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('A3', 'ALERT NO.');
$this->excel->getActiveSheet()->setCellValue('B3', 'ALERT DATE');
$this->excel->getActiveSheet()->setCellValue('C3', 'OCCASION');
$this->excel->getActiveSheet()->setCellValue('D3', 'OCCASION DATE');
$this->excel->getActiveSheet()->setCellValue('E3', 'STAKEHOLDER');
$this->excel->getActiveSheet()->setCellValue('F3', 'CLIENT / VENDOR NAME');
$this->excel->getActiveSheet()->setCellValue('G3', 'CONTACT PERSON NAME');
$this->excel->getActiveSheet()->setCellValue('H3', 'REMINDER DATE');
$this->excel->getActiveSheet()->setCellValue('I3', 'ALERT NOTIFICATION');
$this->excel->getActiveSheet()->setCellValue('J3', 'ALERT MOBILE');
$this->excel->getActiveSheet()->setCellValue('K3', 'ALERT EMAIL');


$this->excel->getActiveSheet()->getStyle("A3:K3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	$contact_person = $this->Settings_model->selectData('sales_customer_contacts', '*',array('cust_id'=>$value['cust_id'],'status'=>'Active','cp_is_admin'=>1));
	$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['alert_number']);
	$this->excel->getActiveSheet()->setCellValue('B'.$count, date('d-m-Y',strtotime($value['alert_date'])));
	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['alert_occasion']);
	$this->excel->getActiveSheet()->setCellValue('D'.$count, date('d-m-Y',strtotime($value['occasion_date'])));
	$this->excel->getActiveSheet()->setCellValue('E'.$count, $value['alert_stakeholder']);
	$this->excel->getActiveSheet()->setCellValue('F'.$count, $value['cust_name']);
	$this->excel->getActiveSheet()->setCellValue('G'.$count, $contact_person[0]->cp_name);
	$this->excel->getActiveSheet()->setCellValue('H'.$count, date('d-m-Y',strtotime($value['alert_reminder'])));
	if($value['alert_notification']==1){
		$this->excel->getActiveSheet()->setCellValue('I'.$count, 'Yes');
	}
	if($value['alert_msg']==1){
		$this->excel->getActiveSheet()->setCellValue('J'.$count, $value['alert_mobile']);
	}
	if($value['alert_mail']==1){
		$this->excel->getActiveSheet()->setCellValue('K'.$count, $value['alert_email']);
	}
	$count++;
}

$filename='Birthday Anniversary Alerts '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
ob_end_clean();	
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>