<?php $this->load->view('template/header'); ?>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <div class="page-header">
            <div class="container">
              <h2 class="page-title">My Module Activity</h2>
            </div>
          </div>

          <div class="page-content">
            <div class="container">
              <div class="welcom-container">
                <div class="Welcome-image">
                  <img width="200" height="200" src="<?php echo base_url(); ?>asset/css/img/icons/blank-stage/ei-blank.gif" class="gifblank" alt="blank">
                </div>
                <div class="welcom-content">
                  <p class="welcom-title">Hey <?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?>!</p>
                  <p class="welcom-text">You currently have no Activity listed.<br/>
                  </p>
                  <p class="welcome-button">
                    
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- ================================================
    Scripts
    ================================================ -->
    <?php $this->load->view('template/footer'); ?>