https://www.xebra.in/xebra/index/xebra_signup
<?php $this->load->view('template/header'); ?>
<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<style type="text/css">
.dataTables_scrollBody{
			overflow:hidden !important;
			height:100% !important;
		}

		.dataTables_scrollHead{
			margin-bottom:-24px !important;
		}

		table.dataTable thead .sorting {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_asc {
		  background-position: 110px 15px !important;
		}
		table.dataTable thead .sorting_desc {
		  background-position: 110px 15px !important;
		}
	.btn-stated {
		max-width: 80px !important;
		background-position: 90px center !important;
}
.btn-date {
  margin: 0 0 0 3px !important;
  padding: 0 24px 0 10px !important;
}
.btn-dropdown-select > input.select-dropdown{
  max-width: 124px;
}

.expense-edit {
 margin: 0 0 0 0 !important;
}
table.dataTable thead .sorting,table.dataTable thead .sorting_asc,table.dataTable thead .sorting_desc {
  background-position: 100px 17px !important;
}

.btn-search{
  margin: 0 0 0 15px !important;
}
a.filter-search.btn-search.btn.active {
  margin-right: -35px !important;
}

.action-btn-wapper span.caret {
    margin: 16px 13px 0 0;
    color:#000;
}
.btn-dropdown-action {
  min-width: 134px;
}
@media only screen and (max-width: 1300px){
  .btn-dropdown-action {
    min-width: 125;
  }
  .btn-dropdown-select > input.select-dropdown{
    max-width: 122px;
    padding-right: 0;
    padding-top: 10px;
    padding-left: 6px !important;
  }
  .searchbtn {
    margin-left: 0 !important;

  }
  .btn-search{
    margin: 0 0 0 24px !important;
  }
}
.dropdown-menu {
  min-width: 170px !important;
}
.dropdown-menu td.day {
  height: 30px;
}
.bildocdrop.btn-dropdown-select ul.select-dropdown {
    width: 190px !important;
}

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 165px !important;
  margin-left: 0px;

}
/*----------END SEARCH DROPDOWN CSS--------*/
input[type=text]:not(.browser-default) {
  font-size: 12px !important;
}
.dropdown-content li > span {
  font-size: 12px;
  color: #666;
}
.icon-img{
    margin: 0 30px 0 2px;
  }
 .dataTables_length {

    margin-left: 500px;
}

	#myactivity_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:96px;
		margin-top:5px;
		margin-left:52%;
	}

	#myactivity_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#myactivity_length .dropdown-content {
		min-width: 94px;
		margin-top:-50% !important;
	}

	.activity_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.sticky {
		position: fixed;
		top: 70px;
		min-width: 75.2%;
		max-width: 80%;
		z-index:999;
		background: white;
		color: black;

	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}
</style>
<!-- START MAIN -->
<div id="main">
  <!-- START WRAPPER -->
  <div class="wrapper">
    <!-- START LEFT SIDEBAR NAV-->
    <?php $this->load->view('template/sidebar'); ?>
    <!-- END LEFT SIDEBAR NAV-->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <section id="content" class="bg-cp ctds-search">
      <div id="breadcrumbs-wrapper">
        <div class="container">
          <div class="row">
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title my-ex">My Activity History<small class="grey-text">(<?= count($activity); ?> Total)</small></h5>

              <ol class="breadcrumbs">
                <li><a href="">MY ACCOUNT / MY ACTIVITY HISTORY</a>
                </ol>
              </div>
            </div>
          </div>
        </div>
          <div class="container custom">
            <div class="row">
			  <div class="col s12 m12 l12" style="margin-top:0px;">
              <div class="col l12 s12 m12">
                 <a href="javascript:void(0);" class="addmorelink right" onclick="reset_actfilter();" title="Reset all" style='padding-top:5px !important;'>Reset</a>
              </div>
              <div class="col l5 s12 m12">
                <div class="col l4 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' style="min-width:148px !important;" href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown004' class='dropdown-content'>
                   <li><a id="email_multiple_activity"><i class="dropdwon-icon icon email"></i>Email</a></li>
                    <!-- <li><a><i class="dropdwon-icon icon email"></i>Export</a></li> -->
                    <li><a id="download_multiple_activity" data-multi_download="0" style="display:flex;"><i class="dropdwon-icon icon download"></i>Download</a></li>
                    <li><a id="print_multiple_activity"><i class="material-icons">print</i>Print</a></li>
                  </ul>
                </div>
                <div class="col l4 s12 m12 searchbtn">
                  <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_activity" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>
                </div>
              </div>
              <div class="col l7 s12 m12">
                <div class="action-btn-wapper right"><!-- <p class="stort-by">STORT BY :</p> -->
					<select class="js-example-basic-single" name="person_name" id="person_name">
                        <option value="">SELECT PERSON</option>
                        <option value="">ALL</option>

                        <?php
                        if($users)
                        {
                          for($i=0; $i<count($users);$i++)
                          {
                            ?><option value="<?php echo $users[$i]->reg_username; ?>"><?php echo strtoupper($users[$i]->reg_username); ?></option><?php
                          }
                        }
                        ?>

                    </select>
                    <select class="js-example-basic-single" name="module_name" id="module_name">
						<option value="">SELECT MODULE</option>
						<option value="">ALL</option>
						<option value="My Company Profile">MY COMPANY PROFILE</option>
						<option value="My Personal Profile">MY PERSONAL PROFILE</option>
						<option value="Document Locker">DOCUMENT LOCKER</option>
						<option value="customer">CLIENT MASTER</option>
						<option value="item">ITEM MASTER</option>
						<option value="billing_doc">BILLING DOCUMENTS</option>
						<option value="credit_note">CREDIT NOTES</option>
						<option value="expense_voucher">EXPENSE VOUCHERS</option>
						<option value="vendor">VENDOR MASTER</option>
						<option value="expense">EXPENSE MASTER</option>
						<option value="purchase_order">PURCHASE ORDER</option>
						<option value="com_exp">COMPANY EXPENSE</option>
						<option value="salary_exp">SALARY EXPENSE</option>
						<option value="debit">DEBIT NOTES</option>
						<option value="pettycash">PETTY CASH</option>
						<option value="asset_register">VENDOR & ASSET MASTER</option>
						<option value="asset_purchase">ASSET PURCHASE</option>
						<option value="asset_sales">ASSET SALES</option>
						<option value="asset_tracker">ASSET TRACKER</option>
						<option value="depre_sche">DEPRECIATION SCHEDULE</option>
						<option value="sales_receipt">SALES RECEIPT</option>
						<option value="asset_sales_receipt">ASSET SALES RECEIPT</option>
						<option value="expense_pay">EXPENSE PAYMENT</option>
						<option value="asset_pay">ASSET PAYMENT</option>
						<option value="salary_pay">SALARY PAYMENT</option>
						<option value="salary_pay">SALARY PAYMENT</option>
						<option value="gst">GST</option>
						<option value="tds">TDS</option>
						<option value="gst">EQUALISATION LEVY</option>
						<option value="prof_tax">PROFESSIONAL TAX</option>
						<option value="emp_master">EMPLOYEE MASTER</option>
						<option value="appraisal">APPRAISAL</option>
						<option value="cash_stat">CASH STATEMENT</option>
						<option value="bank_stat">BANK STATEMENT</option>
						<option value="My TAX Summery">MY TAX SUMMARY</option>
						<option value="My Account">STATEMENT OF ACCOUNT</option>
						<option value="My Resource Center">MY RESOURCE CENTER</option>
				    </select>
					<input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="act_start_date" name="act_start_date" readonly="readonly">
					<input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="act_end_date" name="act_end_date" readonly="readonly">
                </div>
              </div>
            </div>
          </div>
		  </div>
        </div>


          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12" id="print_activity_tbl">
               <!-- <form>-->
                  <table id="myactivity" class="responsive-table display table-type1 mb-2" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:35px;">
                          <input type="checkbox" id="activity_bulk" name="activity_bulk" class="filled-in purple" />
                          <label for="activity_bulk"></label>
                        </th>
                        <th style="width:120px;">DATE</th>
                        <th style="width:130px;">PERSON NAME</th>
                        <th style="width:100px;">LOG IN TIME</th>
                        <th style="width:100px;">LOG OUT TIME</th>
                        <th style="width:200px; text-align:center;">MODULE NAME</th>
                        <th style="width:100px;">IP ADDRESS</th>
                        <th style="width:5px;">ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody class="scrollbody">

                    </tbody>
                  </table>
                <!--</form>-->
              </div>
            </div>
          </div>



      </section>
      <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
  </div>
  <!-- END MAIN -->


<div id="customer_tds_status" class="modal modal-set">
  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
  <div class="modal-content">
    <div class="modal-header">
      <h4><span class="tstatus_type ei-title"></span> Client</h4>

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
    </div></div>
    <div class="modal-body confirma">
      <p>Are you sure you want to change the Client status to <span class="tstatus_type"></span>?</p>
    </div>
    <div class="modal-content">
    <div class="modal-footer">
      <div class="row">
         <input type="hidden" id="ctds_id" name="ctds_id" value="" />
         <input type="hidden" id="ctds_status" name="ctds_status" value="" />
        <div class="col l4 s12 m12"></div>
        <div class="col l12 s12 m12 cancel-deactiv text-center">
          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel no_tdsstatus_change" type="button">CANCEL</a>
          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close tds_cstatus_yes"><span class="tstatus_type" id="tstatus_type"></span></button>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="send_email_activity_modal" class="modal modal-md ps-active-y">
    <?php $this->load->view('module_activity/activity_email_popup'); ?>
</div>


<script type="text/javascript">
    $(document).ready(function() {
      $('.js-example-basic-single').select2();

		var bulk_activity = [];
		$("input[id='activity_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".activity_bulk_action").prop('checked', true);
				$(".activity_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',bulk_cmp_profiles);
			}
			else {
				$(".activity_bulk_action").prop('checked',false);
				bulk_activity = [];
				//$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
			}
		});
  });
</script>

<script type="text/javascript">
	function reset_actfilter(){
		$('.action-btn-wapper').find('select').prop('selectedIndex',0);
		$('.btn-date,.search-hide-show').val('');
		$('.js-example-basic-single').trigger('change.select2');
		$('select').material_select();
		$('#myactivity tr').find('input[type="checkbox"]').each(function() {
			$(this).prop('checked', false);
		});
		myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		$('select').material_select();
	}
</script>

    <!-- ================================================
    Scripts
    ================================================ -->
<?php $this->load->view('template/footer'); ?>
