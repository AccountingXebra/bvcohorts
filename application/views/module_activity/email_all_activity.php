<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="6"><h2 style="margin: 0">Login Activity</h2></th></tr>
	<tr>
	 <th>Date</th>
	 <th>Person Name</th>
	 <th>Login Time</th>
	 <th>Log Out Time</th>
	 <th>Module Name</th>
	 <th>IP Address</th>
	</tr>
</thead>
<tbody>

	<?php
	if(count($userdata)>0){ foreach ($userdata as $key => $value) { ?>
	<tr>
		<?php
		$value = (array) $value;
		if($value['created_at'] == $value['updated_at'])
			{
				$log_out = "--";
			}else{

				$log_out = date('h:ia',strtotime($value['updated_at'])); 
			}
		?>
		<td><?=date('d.m.Y',strtotime($value['created_at']))?></td>
		<td><?=$value['reg_username']?></td>
		<td><?=date('h:ia',strtotime($value['created_at']))?></td>
		<td><?=$log_out?></td>
		<td><?=wordwrap($value['module_data'],20,"<br>\n")?></td>
		<td><?=$value['ip_address']?></td>
	</tr>	
<?php } } ?>

</tbody>
</table>
