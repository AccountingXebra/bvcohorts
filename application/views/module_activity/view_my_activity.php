<?php $this->load->view('template/header.php');?>
<style type="text/css">
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after {
      width: 270px;
   }
   
   .act_details{
	   font-size:11px !important;
	   color:white !important;
   }
   
   .act_data{
	   font-size:13px !important;
	   color:white !important;
   }
   
   #my_activity th{
	   color:#808080 !important;
	   font-size:13px;
   }
   
   #my_activity td{
	   font-size:12px;
	   padding: 15px 12px !important;
   }
   
   #my_activity thead{
	   border-bottom:none !important;
   }
   
   #my_activity{
	    border-collapse:separate;
		border-spacing:0px 5px;
   }
</style>
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12 m6">
                    <a class="go-back underline" href="<?php echo base_url();?>module_tracker/view_activity">Back to My Activity History</a>
                  </div>
                  <div class="col l6 s12 m6">
                    <div class="right-2">
					  <!--<?php $id=$company_profile[0]->bus_id; ?>-->
                      
                     
                    </div>
                    <div class="right-1">
                      <a href=""><i class="fa fa-print" style="font-size:48px;color:black"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php //print_r($activity); ?>
            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12 view-box blue-box before-green-bg green-bg-right">
                  <div class="box-title-info">
                    <div class="row">
                      <div class="col l12 s12 m12">
						<div class="col l12 s12 m12">
							<div class="col l12 s12 m12">
								<div class="col l12 s12 m12">
									<div class="col l12 s12 m12">
										<h4 class="view-box-title"><?php echo $bus_name[0]->bus_company_name ?></h4>
									</div>
								</div>
							</div>
						</div>
                      </div>
                    </div>
                  </div>
			
				  <div class="row">
					<div class="col s12 m12 l12">
						<div class="col s12 m12 l12">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<div class="col s12 m12 l3">
										<label class="act_details">DATE<label><br>
										<label class="act_data"><?php echo date("d.m.Y", strtotime('+5 hour +30 minutes',strtotime($login_record[0]->created_at))); ?></label>

									</div>
									<div class="col s12 m12 l3">
										<label class="act_details">NAME<label><br>
										<label class="act_data"><?php echo $user_Details[0]->reg_username ?></label>
									</div>
									<div class="col s12 m12 l3">
										<label class="act_details">ACCESS<label><br>
										<label class="act_data"><?php echo $access[0]->access ?></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				  </div>
			
				  <div class="row" style="margin-top:20px;"></div>
				  
				  <div class="row">
					<div class="col s12 m12 l12">
						<div class="col s12 m12 l12">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<div class="col s12 m12 l3">
										<label class="act_details">LOG IN<label><br>
										<label class="act_data"><?php echo date("h.i a", strtotime($login_record[0]->created_at)); ?></label><label class="act_data"></label>
									</div>
									<div class="col s12 m12 l3">
										<label class="act_details">LOG OUT<label><br>
										<?php if($login_record[0]->created_at == $login_record[0]->updated_at)
										{
											?> <label class="act_data"><?php echo $log_out = "NA" ?></label> <?php
										}else{
											?> <label class="act_data"><?php echo $log_out = date("h.i a",strtotime($login_record[0]->updated_at)); ?></label> <?php

										}	
										?>
										<label class="act_data"></label>
									</div>
									<div class="col s12 m12 l3">
										<label class="act_details">TIME SPENT<label><br>
										<?php if($login_record[0]->created_at == $login_record[0]->updated_at || $log_out == 'NA')
										{
											?> <label class="act_data"> -- : -- : --</label> <?php
										}else{
											?> <label class="act_data"> <?php echo gmdate("H:i:s",(strtotime($login_record[0]->updated_at) - strtotime($login_record[0]->created_at))) ; ?> </label> <?php
										}	
										?>
										
									</div>
									<div class="col s12 m12 l3">
										<label class="act_details">IP ADDRESS<label><br>
										<label class="act_data"><?php echo $login_record[0]->ip_address; ?></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				  </div>
            </div>
			
			<div class="row">
                <div class="col s12 m12 l12">
					<div class="col l12 m12 s12 view-box white-box before-red-bg red-bg-right">
						<div class="row">
							<div class="col l12 m12 s12">
								<h6 class="white-box-title" style="padding: 11px 34px !important">Details</h6>
							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="col l12 s12 m12">
									<div class="col s12 m12 l12">
										<div class="col l12 s12 m12" id="activity_list_tbl">
											<table id="my_activity" class="responsive-table display mb-2" cellspacing="0" style="margin-left:0px;">
												<thead>
													<tr>
														<th style="width:200px;">START TIME</th>
														<th style="width:200px;">END TIME</th>
														<th style="width:200px;">MODULE/SUB MODULE</th>
														<th style="width:200px;">ACTIVITY DETAILS</th>
													</tr>
												</thead>
												<tbody>
													<?php

													for($i=0; $i<count($activity); $i++)
													{
														?>
														<tr class="white-box">
															<td><?php echo date('h:i:s a',strtotime($activity[$i]->createdat)); ?></td>
															<?php
																if($i == count($activity)-1)
																{
																	?> <td><?php echo $log_out; ?></td> <?php
																}else{
																	?> <td><?php echo date('h:i:s a',strtotime($activity[$i+1]->createdat));
                                                                          
																	 ?></td> <?php
																}
															 ?>
															
															<td><?php echo $activity[$i]->module_name; ?></td>
															<td><?php echo $activity[$i]->action_taken.' '.$activity[$i]->reference; ?></td>
														</tr>
														<?php
													}
													?>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
          </section>
      </div>
    </div>
<?php $this->load->view('template/footer.php');?>