<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://pathli.com/app/assets/build/style/select2_material.css"/>
  <link rel="stylesheet" href="https://pathli.com/app/assets/build/style/materialize.css" />
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
  <link href="https://cdn.rawgit.com/novus/nvd3/master/build/nv.d3.css" rel="stylesheet" type="text/css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
  <script src="https://cdn.rawgit.com/novus/nvd3/master/build/nv.d3.js"></script>
</head>
<style>
body, html {
  background: #212121 !important;
  font-family:'Roboto',sans-serif;
}
.card {
  background-color: #303030 !important
}
.card-panel.dark{
  background-color:#424242;
}
.node {
  cursor: pointer;
}


.node circle {
  fill: #fff;
  stroke: rgba(128, 128, 128,0.9);
  stroke-width:3px;
}

text {
  font-size:1rem;
  fill: rgba(0, 0, 0, 0.87);
  font-weight: 300;
  margin: 0 0 0 15px;
}

.node:hover > text {
  fill: rgba(0, 0, 255, 0.27);
  font-weight: 600;
  font-size: 1.2rem;
}

.link {
  fill: none;
  stroke:rgba(0, 0, 0, 0.27);
  stroke-width: 1.5px;
}

.link:hover {
  stroke-width:4px;
  stroke:rgba(0,0,255,0.27);
}

.found {
  stroke-opacity: 0.7;
  font-weight:100
}

#tree_chart {
  overflow:auto;
  margin-top:-5rem;
  margin-bottom:-1rem;
}
.search {
  float: left;
  font: 10px sans-serif;
  width: 100%;
}

::-webkit-scrollbar {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-button {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-thumb {
  background: rgba(0, 0, 0, 0.54);
  border: 3px none rgba(0, 0, 0, 0.54);
  border-radius: 1px;
}

::-webkit-scrollbar-thumb:hover {
  background: rgba(0, 0, 0, 0.64);
}

::-webkit-scrollbar-thumb:active {
  background: rgba(0, 0, 0, 0.54);
}

::-webkit-scrollbar-track {
  background: #fff;
  border: 2px none #fff;
  border-radius: 1px;
}

::-webkit-scrollbar-track:hover {
  background: #fff;
}

::-webkit-scrollbar-track:active {
  background: #fff;
}

::-webkit-scrollbar-corner {
  background: transparent;
}
.card-panel.no-pad {
  padding:0 !important;
  margin:1px
}
svg {
  overflow:auto !important;
}
/*!
 * Materialize v0.97.8 (http://materializecss.com)
 * Copyright 2014-2015 Materialize
 * MIT License (https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE)
 */
.materialize-red {
    background-color: #e51c23 !important
}

.materialize-red-text {
    color: #e51c23 !important
}

.materialize-red.lighten-5 {
    background-color: #fdeaeb !important
}

.materialize-red-text.text-lighten-5 {
    color: #fdeaeb !important
}

.materialize-red.lighten-4 {
    background-color: #f8c1c3 !important
}

.materialize-red-text.text-lighten-4 {
    color: #f8c1c3 !important
}

.materialize-red.lighten-3 {
    background-color: #f3989b !important
}

.materialize-red-text.text-lighten-3 {
    color: #f3989b !important
}

.materialize-red.lighten-2 {
    background-color: #ee6e73 !important
}

.materialize-red-text.text-lighten-2 {
    color: #ee6e73 !important
}

.materialize-red.lighten-1 {
    background-color: #ea454b !important
}

.materialize-red-text.text-lighten-1 {
    color: #ea454b !important
}

.materialize-red.darken-1 {
    background-color: #d0181e !important
}

.materialize-red-text.text-darken-1 {
    color: #d0181e !important
}

.materialize-red.darken-2 {
    background-color: #b9151b !important
}

.materialize-red-text.text-darken-2 {
    color: #b9151b !important
}

.materialize-red.darken-3 {
    background-color: #a21318 !important
}

.materialize-red-text.text-darken-3 {
    color: #a21318 !important
}

.materialize-red.darken-4 {
    background-color: #8b1014 !important
}

.materialize-red-text.text-darken-4 {
    color: #8b1014 !important
}

.red {
    background-color: #F44336 !important
}

.red-text {
    color: #F44336 !important
}

.red.lighten-5 {
    background-color: #FFEBEE !important
}

.red-text.text-lighten-5 {
    color: #FFEBEE !important
}

.red.lighten-4 {
    background-color: #FFCDD2 !important
}

.red-text.text-lighten-4 {
    color: #FFCDD2 !important
}

.red.lighten-3 {
    background-color: #EF9A9A !important
}

.red-text.text-lighten-3 {
    color: #EF9A9A !important
}

.red.lighten-2 {
    background-color: #E57373 !important
}

.red-text.text-lighten-2 {
    color: #E57373 !important
}

.red.lighten-1 {
    background-color: #EF5350 !important
}

.red-text.text-lighten-1 {
    color: #EF5350 !important
}

.red.darken-1 {
    background-color: #E53935 !important
}

.red-text.text-darken-1 {
    color: #E53935 !important
}

.red.darken-2 {
    background-color: #D32F2F !important
}

.red-text.text-darken-2 {
    color: #D32F2F !important
}

.red.darken-3 {
    background-color: #C62828 !important
}

.red-text.text-darken-3 {
    color: #C62828 !important
}

.red.darken-4 {
    background-color: #B71C1C !important
}

.red-text.text-darken-4 {
    color: #B71C1C !important
}

.red.accent-1 {
    background-color: #FF8A80 !important
}

.red-text.text-accent-1 {
    color: #FF8A80 !important
}

.red.accent-2 {
    background-color: #FF5252 !important
}

.red-text.text-accent-2 {
    color: #FF5252 !important
}

.red.accent-3 {
    background-color: #FF1744 !important
}

.red-text.text-accent-3 {
    color: #FF1744 !important
}

.red.accent-4 {
    background-color: #D50000 !important
}

.red-text.text-accent-4 {
    color: #D50000 !important
}

.pink {
    background-color: #e91e63 !important
}

.pink-text {
    color: #e91e63 !important
}

.pink.lighten-5 {
    background-color: #fce4ec !important
}

.pink-text.text-lighten-5 {
    color: #fce4ec !important
}

.pink.lighten-4 {
    background-color: #f8bbd0 !important
}

.pink-text.text-lighten-4 {
    color: #f8bbd0 !important
}

.pink.lighten-3 {
    background-color: #f48fb1 !important
}

.pink-text.text-lighten-3 {
    color: #f48fb1 !important
}

.pink.lighten-2 {
    background-color: #f06292 !important
}

.pink-text.text-lighten-2 {
    color: #f06292 !important
}

.pink.lighten-1 {
    background-color: #ec407a !important
}

.pink-text.text-lighten-1 {
    color: #ec407a !important
}

.pink.darken-1 {
    background-color: #d81b60 !important
}

.pink-text.text-darken-1 {
    color: #d81b60 !important
}

.pink.darken-2 {
    background-color: #c2185b !important
}

.pink-text.text-darken-2 {
    color: #c2185b !important
}

.pink.darken-3 {
    background-color: #ad1457 !important
}

.pink-text.text-darken-3 {
    color: #ad1457 !important
}

.pink.darken-4 {
    background-color: #880e4f !important
}

.pink-text.text-darken-4 {
    color: #880e4f !important
}

.pink.accent-1 {
    background-color: #ff80ab !important
}

.pink-text.text-accent-1 {
    color: #ff80ab !important
}

.pink.accent-2 {
    background-color: #ff4081 !important
}

.pink-text.text-accent-2 {
    color: #ff4081 !important
}

.pink.accent-3 {
    background-color: #f50057 !important
}

.pink-text.text-accent-3 {
    color: #f50057 !important
}

.pink.accent-4 {
    background-color: #c51162 !important
}

.pink-text.text-accent-4 {
    color: #c51162 !important
}

.purple {
    background-color: #9c27b0 !important
}

.purple-text {
    color: #9c27b0 !important
}

.purple.lighten-5 {
    background-color: #f3e5f5 !important
}

.purple-text.text-lighten-5 {
    color: #f3e5f5 !important
}

.purple.lighten-4 {
    background-color: #e1bee7 !important
}

.purple-text.text-lighten-4 {
    color: #e1bee7 !important
}

.purple.lighten-3 {
    background-color: #ce93d8 !important
}

.purple-text.text-lighten-3 {
    color: #ce93d8 !important
}

.purple.lighten-2 {
    background-color: #ba68c8 !important
}

.purple-text.text-lighten-2 {
    color: #ba68c8 !important
}

.purple.lighten-1 {
    background-color: #ab47bc !important
}

.purple-text.text-lighten-1 {
    color: #ab47bc !important
}

.purple.darken-1 {
    background-color: #8e24aa !important
}

.purple-text.text-darken-1 {
    color: #8e24aa !important
}

.purple.darken-2 {
    background-color: #7b1fa2 !important
}

.purple-text.text-darken-2 {
    color: #7b1fa2 !important
}

.purple.darken-3 {
    background-color: #6a1b9a !important
}

.purple-text.text-darken-3 {
    color: #6a1b9a !important
}

.purple.darken-4 {
    background-color: #4a148c !important
}

.purple-text.text-darken-4 {
    color: #4a148c !important
}

.purple.accent-1 {
    background-color: #ea80fc !important
}

.purple-text.text-accent-1 {
    color: #ea80fc !important
}

.purple.accent-2 {
    background-color: #e040fb !important
}

.purple-text.text-accent-2 {
    color: #e040fb !important
}

.purple.accent-3 {
    background-color: #d500f9 !important
}

.purple-text.text-accent-3 {
    color: #d500f9 !important
}

.purple.accent-4 {
    background-color: #a0f !important
}

.purple-text.text-accent-4 {
    color: #a0f !important
}

.deep-purple {
    background-color: #673ab7 !important
}

.deep-purple-text {
    color: #673ab7 !important
}

.deep-purple.lighten-5 {
    background-color: #ede7f6 !important
}

.deep-purple-text.text-lighten-5 {
    color: #ede7f6 !important
}

.deep-purple.lighten-4 {
    background-color: #d1c4e9 !important
}

.deep-purple-text.text-lighten-4 {
    color: #d1c4e9 !important
}

.deep-purple.lighten-3 {
    background-color: #b39ddb !important
}

.deep-purple-text.text-lighten-3 {
    color: #b39ddb !important
}

.deep-purple.lighten-2 {
    background-color: #9575cd !important
}

.deep-purple-text.text-lighten-2 {
    color: #9575cd !important
}

.deep-purple.lighten-1 {
    background-color: #7e57c2 !important
}

.deep-purple-text.text-lighten-1 {
    color: #7e57c2 !important
}

.deep-purple.darken-1 {
    background-color: #5e35b1 !important
}

.deep-purple-text.text-darken-1 {
    color: #5e35b1 !important
}

.deep-purple.darken-2 {
    background-color: #512da8 !important
}

.deep-purple-text.text-darken-2 {
    color: #512da8 !important
}

.deep-purple.darken-3 {
    background-color: #4527a0 !important
}

.deep-purple-text.text-darken-3 {
    color: #4527a0 !important
}

.deep-purple.darken-4 {
    background-color: #311b92 !important
}

.deep-purple-text.text-darken-4 {
    color: #311b92 !important
}

.deep-purple.accent-1 {
    background-color: #b388ff !important
}

.deep-purple-text.text-accent-1 {
    color: #b388ff !important
}

.deep-purple.accent-2 {
    background-color: #7c4dff !important
}

.deep-purple-text.text-accent-2 {
    color: #7c4dff !important
}

.deep-purple.accent-3 {
    background-color: #651fff !important
}

.deep-purple-text.text-accent-3 {
    color: #651fff !important
}

.deep-purple.accent-4 {
    background-color: #6200ea !important
}

.deep-purple-text.text-accent-4 {
    color: #6200ea !important
}

.indigo {
    background-color: #3f51b5 !important
}

.indigo-text {
    color: #3f51b5 !important
}

.indigo.lighten-5 {
    background-color: #e8eaf6 !important
}

.indigo-text.text-lighten-5 {
    color: #e8eaf6 !important
}

.indigo.lighten-4 {
    background-color: #c5cae9 !important
}

.indigo-text.text-lighten-4 {
    color: #c5cae9 !important
}

.indigo.lighten-3 {
    background-color: #9fa8da !important
}

.indigo-text.text-lighten-3 {
    color: #9fa8da !important
}

.indigo.lighten-2 {
    background-color: #7986cb !important
}

.indigo-text.text-lighten-2 {
    color: #7986cb !important
}

.indigo.lighten-1 {
    background-color: #5c6bc0 !important
}

.indigo-text.text-lighten-1 {
    color: #5c6bc0 !important
}

.indigo.darken-1 {
    background-color: #3949ab !important
}

.indigo-text.text-darken-1 {
    color: #3949ab !important
}

.indigo.darken-2 {
    background-color: #303f9f !important
}

.indigo-text.text-darken-2 {
    color: #303f9f !important
}

.indigo.darken-3 {
    background-color: #283593 !important
}

.indigo-text.text-darken-3 {
    color: #283593 !important
}

.indigo.darken-4 {
    background-color: #1a237e !important
}

.indigo-text.text-darken-4 {
    color: #1a237e !important
}

.indigo.accent-1 {
    background-color: #8c9eff !important
}

.indigo-text.text-accent-1 {
    color: #8c9eff !important
}

.indigo.accent-2 {
    background-color: #536dfe !important
}

.indigo-text.text-accent-2 {
    color: #536dfe !important
}

.indigo.accent-3 {
    background-color: #3d5afe !important
}

.indigo-text.text-accent-3 {
    color: #3d5afe !important
}

.indigo.accent-4 {
    background-color: #304ffe !important
}

.indigo-text.text-accent-4 {
    color: #304ffe !important
}

.blue {
    background-color: #2196F3 !important
}

.blue-text {
    color: #2196F3 !important
}

.blue.lighten-5 {
    background-color: #E3F2FD !important
}

.blue-text.text-lighten-5 {
    color: #E3F2FD !important
}

.blue.lighten-4 {
    background-color: #BBDEFB !important
}

.blue-text.text-lighten-4 {
    color: #BBDEFB !important
}

.blue.lighten-3 {
    background-color: #90CAF9 !important
}

.blue-text.text-lighten-3 {
    color: #90CAF9 !important
}

.blue.lighten-2 {
    background-color: #64B5F6 !important
}

.blue-text.text-lighten-2 {
    color: #64B5F6 !important
}

.blue.lighten-1 {
    background-color: #42A5F5 !important
}

.blue-text.text-lighten-1 {
    color: #42A5F5 !important
}

.blue.darken-1 {
    background-color: #1E88E5 !important
}

.blue-text.text-darken-1 {
    color: #1E88E5 !important
}

.blue.darken-2 {
    background-color: #1976D2 !important
}

.blue-text.text-darken-2 {
    color: #1976D2 !important
}

.blue.darken-3 {
    background-color: #1565C0 !important
}

.blue-text.text-darken-3 {
    color: #1565C0 !important
}

.blue.darken-4 {
    background-color: #0D47A1 !important
}

.blue-text.text-darken-4 {
    color: #0D47A1 !important
}

.blue.accent-1 {
    background-color: #82B1FF !important
}

.blue-text.text-accent-1 {
    color: #82B1FF !important
}

.blue.accent-2 {
    background-color: #448AFF !important
}

.blue-text.text-accent-2 {
    color: #448AFF !important
}

.blue.accent-3 {
    background-color: #2979FF !important
}

.blue-text.text-accent-3 {
    color: #2979FF !important
}

.blue.accent-4 {
    background-color: #2962FF !important
}

.blue-text.text-accent-4 {
    color: #2962FF !important
}

.light-blue {
    background-color: #03a9f4 !important
}

.light-blue-text {
    color: #03a9f4 !important
}

.light-blue.lighten-5 {
    background-color: #e1f5fe !important
}

.light-blue-text.text-lighten-5 {
    color: #e1f5fe !important
}

.light-blue.lighten-4 {
    background-color: #b3e5fc !important
}

.light-blue-text.text-lighten-4 {
    color: #b3e5fc !important
}

.light-blue.lighten-3 {
    background-color: #81d4fa !important
}

.light-blue-text.text-lighten-3 {
    color: #81d4fa !important
}

.light-blue.lighten-2 {
    background-color: #4fc3f7 !important
}

.light-blue-text.text-lighten-2 {
    color: #4fc3f7 !important
}

.light-blue.lighten-1 {
    background-color: #29b6f6 !important
}

.light-blue-text.text-lighten-1 {
    color: #29b6f6 !important
}

.light-blue.darken-1 {
    background-color: #039be5 !important
}

.light-blue-text.text-darken-1 {
    color: #039be5 !important
}

.light-blue.darken-2 {
    background-color: #0288d1 !important
}

.light-blue-text.text-darken-2 {
    color: #0288d1 !important
}

.light-blue.darken-3 {
    background-color: #0277bd !important
}

.light-blue-text.text-darken-3 {
    color: #0277bd !important
}

.light-blue.darken-4 {
    background-color: #01579b !important
}

.light-blue-text.text-darken-4 {
    color: #01579b !important
}

.light-blue.accent-1 {
    background-color: #80d8ff !important
}

.light-blue-text.text-accent-1 {
    color: #80d8ff !important
}

.light-blue.accent-2 {
    background-color: #40c4ff !important
}

.light-blue-text.text-accent-2 {
    color: #40c4ff !important
}

.light-blue.accent-3 {
    background-color: #00b0ff !important
}

.light-blue-text.text-accent-3 {
    color: #00b0ff !important
}

.light-blue.accent-4 {
    background-color: #0091ea !important
}

.light-blue-text.text-accent-4 {
    color: #0091ea !important
}

.cyan {
    background-color: #00bcd4 !important
}

.cyan-text {
    color: #00bcd4 !important
}

.cyan.lighten-5 {
    background-color: #e0f7fa !important
}

.cyan-text.text-lighten-5 {
    color: #e0f7fa !important
}

.cyan.lighten-4 {
    background-color: #b2ebf2 !important
}

.cyan-text.text-lighten-4 {
    color: #b2ebf2 !important
}

.cyan.lighten-3 {
    background-color: #80deea !important
}

.cyan-text.text-lighten-3 {
    color: #80deea !important
}

.cyan.lighten-2 {
    background-color: #4dd0e1 !important
}

.cyan-text.text-lighten-2 {
    color: #4dd0e1 !important
}

.cyan.lighten-1 {
    background-color: #26c6da !important
}

.cyan-text.text-lighten-1 {
    color: #26c6da !important
}

.cyan.darken-1 {
    background-color: #00acc1 !important
}

.cyan-text.text-darken-1 {
    color: #00acc1 !important
}

.cyan.darken-2 {
    background-color: #0097a7 !important
}

.cyan-text.text-darken-2 {
    color: #0097a7 !important
}

.cyan.darken-3 {
    background-color: #00838f !important
}

.cyan-text.text-darken-3 {
    color: #00838f !important
}

.cyan.darken-4 {
    background-color: #006064 !important
}

.cyan-text.text-darken-4 {
    color: #006064 !important
}

.cyan.accent-1 {
    background-color: #84ffff !important
}

.cyan-text.text-accent-1 {
    color: #84ffff !important
}

.cyan.accent-2 {
    background-color: #18ffff !important
}

.cyan-text.text-accent-2 {
    color: #18ffff !important
}

.cyan.accent-3 {
    background-color: #00e5ff !important
}

.cyan-text.text-accent-3 {
    color: #00e5ff !important
}

.cyan.accent-4 {
    background-color: #00b8d4 !important
}

.cyan-text.text-accent-4 {
    color: #00b8d4 !important
}

.teal {
    background-color: #009688 !important
}

.teal-text {
    color: #009688 !important
}

.teal.lighten-5 {
    background-color: #e0f2f1 !important
}

.teal-text.text-lighten-5 {
    color: #e0f2f1 !important
}

.teal.lighten-4 {
    background-color: #b2dfdb !important
}

.teal-text.text-lighten-4 {
    color: #b2dfdb !important
}

.teal.lighten-3 {
    background-color: #80cbc4 !important
}

.teal-text.text-lighten-3 {
    color: #80cbc4 !important
}

.teal.lighten-2 {
    background-color: #4db6ac !important
}

.teal-text.text-lighten-2 {
    color: #4db6ac !important
}

.teal.lighten-1 {
    background-color: #40c4ff !important
}

.teal-text.text-lighten-1 {
    color: #40c4ff !important
}

.teal.darken-1 {
    background-color: #00897b !important
}

.teal-text.text-darken-1 {
    color: #00897b !important
}

.teal.darken-2 {
    background-color: #00796b !important
}

.teal-text.text-darken-2 {
    color: #00796b !important
}

.teal.darken-3 {
    background-color: #00695c !important
}

.teal-text.text-darken-3 {
    color: #00695c !important
}

.teal.darken-4 {
    background-color: #004d40 !important
}

.teal-text.text-darken-4 {
    color: #004d40 !important
}

.teal.accent-1 {
    background-color: #a7ffeb !important
}

.teal-text.text-accent-1 {
    color: #a7ffeb !important
}

.teal.accent-2 {
    background-color: #64ffda !important
}

.teal-text.text-accent-2 {
    color: #64ffda !important
}

.teal.accent-3 {
    background-color: #1de9b6 !important
}

.teal-text.text-accent-3 {
    color: #1de9b6 !important
}

.teal.accent-4 {
    background-color: #00bfa5 !important
}

.teal-text.text-accent-4 {
    color: #00bfa5 !important
}

.green {
    background-color: #4CAF50 !important
}

.green-text {
    color: #4CAF50 !important
}

.green.lighten-5 {
    background-color: #E8F5E9 !important
}

.green-text.text-lighten-5 {
    color: #E8F5E9 !important
}

.green.lighten-4 {
    background-color: #C8E6C9 !important
}

.green-text.text-lighten-4 {
    color: #C8E6C9 !important
}

.green.lighten-3 {
    background-color: #A5D6A7 !important
}

.green-text.text-lighten-3 {
    color: #A5D6A7 !important
}

.green.lighten-2 {
    background-color: #81C784 !important
}

.green-text.text-lighten-2 {
    color: #81C784 !important
}

.green.lighten-1 {
    background-color: #66BB6A !important
}

.green-text.text-lighten-1 {
    color: #66BB6A !important
}

.green.darken-1 {
    background-color: #43A047 !important
}

.green-text.text-darken-1 {
    color: #43A047 !important
}

.green.darken-2 {
    background-color: #388E3C !important
}

.green-text.text-darken-2 {
    color: #388E3C !important
}

.green.darken-3 {
    background-color: #2E7D32 !important
}

.green-text.text-darken-3 {
    color: #2E7D32 !important
}

.green.darken-4 {
    background-color: #1B5E20 !important
}

.green-text.text-darken-4 {
    color: #1B5E20 !important
}

.green.accent-1 {
    background-color: #B9F6CA !important
}

.green-text.text-accent-1 {
    color: #B9F6CA !important
}

.green.accent-2 {
    background-color: #69F0AE !important
}

.green-text.text-accent-2 {
    color: #69F0AE !important
}

.green.accent-3 {
    background-color: #00E676 !important
}

.green-text.text-accent-3 {
    color: #00E676 !important
}

.green.accent-4 {
    background-color: #00C853 !important
}

.green-text.text-accent-4 {
    color: #00C853 !important
}

.light-green {
    background-color: #8bc34a !important
}

.light-green-text {
    color: #8bc34a !important
}

.light-green.lighten-5 {
    background-color: #f1f8e9 !important
}

.light-green-text.text-lighten-5 {
    color: #f1f8e9 !important
}

.light-green.lighten-4 {
    background-color: #dcedc8 !important
}

.light-green-text.text-lighten-4 {
    color: #dcedc8 !important
}

.light-green.lighten-3 {
    background-color: #c5e1a5 !important
}

.light-green-text.text-lighten-3 {
    color: #c5e1a5 !important
}

.light-green.lighten-2 {
    background-color: #aed581 !important
}

.light-green-text.text-lighten-2 {
    color: #aed581 !important
}

.light-green.lighten-1 {
    background-color: #9ccc65 !important
}

.light-green-text.text-lighten-1 {
    color: #9ccc65 !important
}

.light-green.darken-1 {
    background-color: #7cb342 !important
}

.light-green-text.text-darken-1 {
    color: #7cb342 !important
}

.light-green.darken-2 {
    background-color: #689f38 !important
}

.light-green-text.text-darken-2 {
    color: #689f38 !important
}

.light-green.darken-3 {
    background-color: #558b2f !important
}

.light-green-text.text-darken-3 {
    color: #558b2f !important
}

.light-green.darken-4 {
    background-color: #33691e !important
}

.light-green-text.text-darken-4 {
    color: #33691e !important
}

.light-green.accent-1 {
    background-color: #ccff90 !important
}

.light-green-text.text-accent-1 {
    color: #ccff90 !important
}

.light-green.accent-2 {
    background-color: #b2ff59 !important
}

.light-green-text.text-accent-2 {
    color: #b2ff59 !important
}

.light-green.accent-3 {
    background-color: #76ff03 !important
}

.light-green-text.text-accent-3 {
    color: #76ff03 !important
}

.light-green.accent-4 {
    background-color: #64dd17 !important
}

.light-green-text.text-accent-4 {
    color: #64dd17 !important
}

.lime {
    background-color: #cddc39 !important
}

.lime-text {
    color: #cddc39 !important
}

.lime.lighten-5 {
    background-color: #f9fbe7 !important
}

.lime-text.text-lighten-5 {
    color: #f9fbe7 !important
}

.lime.lighten-4 {
    background-color: #f0f4c3 !important
}

.lime-text.text-lighten-4 {
    color: #f0f4c3 !important
}

.lime.lighten-3 {
    background-color: #e6ee9c !important
}

.lime-text.text-lighten-3 {
    color: #e6ee9c !important
}

.lime.lighten-2 {
    background-color: #dce775 !important
}

.lime-text.text-lighten-2 {
    color: #dce775 !important
}

.lime.lighten-1 {
    background-color: #d4e157 !important
}

.lime-text.text-lighten-1 {
    color: #d4e157 !important
}

.lime.darken-1 {
    background-color: #c0ca33 !important
}

.lime-text.text-darken-1 {
    color: #c0ca33 !important
}

.lime.darken-2 {
    background-color: #afb42b !important
}

.lime-text.text-darken-2 {
    color: #afb42b !important
}

.lime.darken-3 {
    background-color: #9e9d24 !important
}

.lime-text.text-darken-3 {
    color: #9e9d24 !important
}

.lime.darken-4 {
    background-color: #827717 !important
}

.lime-text.text-darken-4 {
    color: #827717 !important
}

.lime.accent-1 {
    background-color: #f4ff81 !important
}

.lime-text.text-accent-1 {
    color: #f4ff81 !important
}

.lime.accent-2 {
    background-color: #eeff41 !important
}

.lime-text.text-accent-2 {
    color: #eeff41 !important
}

.lime.accent-3 {
    background-color: #c6ff00 !important
}

.lime-text.text-accent-3 {
    color: #c6ff00 !important
}

.lime.accent-4 {
    background-color: #aeea00 !important
}

.lime-text.text-accent-4 {
    color: #aeea00 !important
}

.yellow {
    background-color: #ffeb3b !important
}

.yellow-text {
    color: #ffeb3b !important
}

.yellow.lighten-5 {
    background-color: #fffde7 !important
}

.yellow-text.text-lighten-5 {
    color: #fffde7 !important
}

.yellow.lighten-4 {
    background-color: #fff9c4 !important
}

.yellow-text.text-lighten-4 {
    color: #fff9c4 !important
}

.yellow.lighten-3 {
    background-color: #fff59d !important
}

.yellow-text.text-lighten-3 {
    color: #fff59d !important
}

.yellow.lighten-2 {
    background-color: #fff176 !important
}

.yellow-text.text-lighten-2 {
    color: #fff176 !important
}

.yellow.lighten-1 {
    background-color: #ffee58 !important
}

.yellow-text.text-lighten-1 {
    color: #ffee58 !important
}

.yellow.darken-1 {
    background-color: #fdd835 !important
}

.yellow-text.text-darken-1 {
    color: #fdd835 !important
}

.yellow.darken-2 {
    background-color: #fbc02d !important
}

.yellow-text.text-darken-2 {
    color: #fbc02d !important
}

.yellow.darken-3 {
    background-color: #f9a825 !important
}

.yellow-text.text-darken-3 {
    color: #f9a825 !important
}

.yellow.darken-4 {
    background-color: #f57f17 !important
}

.yellow-text.text-darken-4 {
    color: #f57f17 !important
}

.yellow.accent-1 {
    background-color: #ffff8d !important
}

.yellow-text.text-accent-1 {
    color: #ffff8d !important
}

.yellow.accent-2 {
    background-color: #ff0 !important
}

.yellow-text.text-accent-2 {
    color: #ff0 !important
}

.yellow.accent-3 {
    background-color: #ffea00 !important
}

.yellow-text.text-accent-3 {
    color: #ffea00 !important
}

.yellow.accent-4 {
    background-color: #ffd600 !important
}

.yellow-text.text-accent-4 {
    color: #ffd600 !important
}

.amber {
    background-color: #ffc107 !important
}

.amber-text {
    color: #ffc107 !important
}

.amber.lighten-5 {
    background-color: #fff8e1 !important
}

.amber-text.text-lighten-5 {
    color: #fff8e1 !important
}

.amber.lighten-4 {
    background-color: #ffecb3 !important
}

.amber-text.text-lighten-4 {
    color: #ffecb3 !important
}

.amber.lighten-3 {
    background-color: #ffe082 !important
}

.amber-text.text-lighten-3 {
    color: #ffe082 !important
}

.amber.lighten-2 {
    background-color: #ffd54f !important
}

.amber-text.text-lighten-2 {
    color: #ffd54f !important
}

.amber.lighten-1 {
    background-color: #ffca28 !important
}

.amber-text.text-lighten-1 {
    color: #ffca28 !important
}

.amber.darken-1 {
    background-color: #ffb300 !important
}

.amber-text.text-darken-1 {
    color: #ffb300 !important
}

.amber.darken-2 {
    background-color: #ffa000 !important
}

.amber-text.text-darken-2 {
    color: #ffa000 !important
}

.amber.darken-3 {
    background-color: #ff8f00 !important
}

.amber-text.text-darken-3 {
    color: #ff8f00 !important
}

.amber.darken-4 {
    background-color: #ff6f00 !important
}

.amber-text.text-darken-4 {
    color: #ff6f00 !important
}

.amber.accent-1 {
    background-color: #ffe57f !important
}

.amber-text.text-accent-1 {
    color: #ffe57f !important
}

.amber.accent-2 {
    background-color: #ffd740 !important
}

.amber-text.text-accent-2 {
    color: #ffd740 !important
}

.amber.accent-3 {
    background-color: #ffc400 !important
}

.amber-text.text-accent-3 {
    color: #ffc400 !important
}

.amber.accent-4 {
    background-color: #ffab00 !important
}

.amber-text.text-accent-4 {
    color: #ffab00 !important
}

.orange {
    background-color: #ff9800 !important
}

.orange-text {
    color: #ff9800 !important
}

.orange.lighten-5 {
    background-color: #fff3e0 !important
}

.orange-text.text-lighten-5 {
    color: #fff3e0 !important
}

.orange.lighten-4 {
    background-color: #ffe0b2 !important
}

.orange-text.text-lighten-4 {
    color: #ffe0b2 !important
}

.orange.lighten-3 {
    background-color: #ffcc80 !important
}

.orange-text.text-lighten-3 {
    color: #ffcc80 !important
}

.orange.lighten-2 {
    background-color: #ffb74d !important
}

.orange-text.text-lighten-2 {
    color: #ffb74d !important
}

.orange.lighten-1 {
    background-color: #ffa726 !important
}

.orange-text.text-lighten-1 {
    color: #ffa726 !important
}

.orange.darken-1 {
    background-color: #fb8c00 !important
}

.orange-text.text-darken-1 {
    color: #fb8c00 !important
}

.orange.darken-2 {
    background-color: #f57c00 !important
}

.orange-text.text-darken-2 {
    color: #f57c00 !important
}

.orange.darken-3 {
    background-color: #ef6c00 !important
}

.orange-text.text-darken-3 {
    color: #ef6c00 !important
}

.orange.darken-4 {
    background-color: #e65100 !important
}

.orange-text.text-darken-4 {
    color: #e65100 !important
}

.orange.accent-1 {
    background-color: #ffd180 !important
}

.orange-text.text-accent-1 {
    color: #ffd180 !important
}

.orange.accent-2 {
    background-color: #ffab40 !important
}

.orange-text.text-accent-2 {
    color: #ffab40 !important
}

.orange.accent-3 {
    background-color: #ff9100 !important
}

.orange-text.text-accent-3 {
    color: #ff9100 !important
}

.orange.accent-4 {
    background-color: #ff6d00 !important
}

.orange-text.text-accent-4 {
    color: #ff6d00 !important
}

.deep-orange {
    background-color: #ff5722 !important
}

.deep-orange-text {
    color: #ff5722 !important
}

.deep-orange.lighten-5 {
    background-color: #fbe9e7 !important
}

.deep-orange-text.text-lighten-5 {
    color: #fbe9e7 !important
}

.deep-orange.lighten-4 {
    background-color: #ffccbc !important
}

.deep-orange-text.text-lighten-4 {
    color: #ffccbc !important
}

.deep-orange.lighten-3 {
    background-color: #ffab91 !important
}

.deep-orange-text.text-lighten-3 {
    color: #ffab91 !important
}

.deep-orange.lighten-2 {
    background-color: #ff8a65 !important
}

.deep-orange-text.text-lighten-2 {
    color: #ff8a65 !important
}

.deep-orange.lighten-1 {
    background-color: #ff7043 !important
}

.deep-orange-text.text-lighten-1 {
    color: #ff7043 !important
}

.deep-orange.darken-1 {
    background-color: #f4511e !important
}

.deep-orange-text.text-darken-1 {
    color: #f4511e !important
}

.deep-orange.darken-2 {
    background-color: #e64a19 !important
}

.deep-orange-text.text-darken-2 {
    color: #e64a19 !important
}

.deep-orange.darken-3 {
    background-color: #d84315 !important
}

.deep-orange-text.text-darken-3 {
    color: #d84315 !important
}

.deep-orange.darken-4 {
    background-color: #bf360c !important
}

.deep-orange-text.text-darken-4 {
    color: #bf360c !important
}

.deep-orange.accent-1 {
    background-color: #ff9e80 !important
}

.deep-orange-text.text-accent-1 {
    color: #ff9e80 !important
}

.deep-orange.accent-2 {
    background-color: #ff6e40 !important
}

.deep-orange-text.text-accent-2 {
    color: #ff6e40 !important
}

.deep-orange.accent-3 {
    background-color: #ff3d00 !important
}

.deep-orange-text.text-accent-3 {
    color: #ff3d00 !important
}

.deep-orange.accent-4 {
    background-color: #dd2c00 !important
}

.deep-orange-text.text-accent-4 {
    color: #dd2c00 !important
}

.brown {
    background-color: #795548 !important
}

.brown-text {
    color: #795548 !important
}

.brown.lighten-5 {
    background-color: #efebe9 !important
}

.brown-text.text-lighten-5 {
    color: #efebe9 !important
}

.brown.lighten-4 {
    background-color: #d7ccc8 !important
}

.brown-text.text-lighten-4 {
    color: #d7ccc8 !important
}

.brown.lighten-3 {
    background-color: #bcaaa4 !important
}

.brown-text.text-lighten-3 {
    color: #bcaaa4 !important
}

.brown.lighten-2 {
    background-color: #a1887f !important
}

.brown-text.text-lighten-2 {
    color: #a1887f !important
}

.brown.lighten-1 {
    background-color: #8d6e63 !important
}

.brown-text.text-lighten-1 {
    color: #8d6e63 !important
}

.brown.darken-1 {
    background-color: #6d4c41 !important
}

.brown-text.text-darken-1 {
    color: #6d4c41 !important
}

.brown.darken-2 {
    background-color: #5d4037 !important
}

.brown-text.text-darken-2 {
    color: #5d4037 !important
}

.brown.darken-3 {
    background-color: #4e342e !important
}

.brown-text.text-darken-3 {
    color: #4e342e !important
}

.brown.darken-4 {
    background-color: #3e2723 !important
}

.brown-text.text-darken-4 {
    color: #3e2723 !important
}

.blue-grey {
    background-color: #607d8b !important
}

.blue-grey-text {
    color: #607d8b !important
}

.blue-grey.lighten-5 {
    background-color: #eceff1 !important
}

.blue-grey-text.text-lighten-5 {
    color: #eceff1 !important
}

.blue-grey.lighten-4 {
    background-color: #cfd8dc !important
}

.blue-grey-text.text-lighten-4 {
    color: #cfd8dc !important
}

.blue-grey.lighten-3 {
    background-color: #b0bec5 !important
}

.blue-grey-text.text-lighten-3 {
    color: #b0bec5 !important
}

.blue-grey.lighten-2 {
    background-color: #90a4ae !important
}

.blue-grey-text.text-lighten-2 {
    color: #90a4ae !important
}

.blue-grey.lighten-1 {
    background-color: #78909c !important
}

.blue-grey-text.text-lighten-1 {
    color: #78909c !important
}

.blue-grey.darken-1 {
    background-color: #546e7a !important
}

.blue-grey-text.text-darken-1 {
    color: #546e7a !important
}

.blue-grey.darken-2 {
    background-color: #455a64 !important
}

.blue-grey-text.text-darken-2 {
    color: #455a64 !important
}

.blue-grey.darken-3 {
    background-color: #37474f !important
}

.blue-grey-text.text-darken-3 {
    color: #37474f !important
}

.blue-grey.darken-4 {
    background-color: #263238 !important
}

.blue-grey-text.text-darken-4 {
    color: #263238 !important
}

.grey {
    background-color: #9e9e9e !important
}

.grey-text {
    color: #9e9e9e !important
}

.grey.lighten-5 {
    background-color: #fafafa !important
}

.grey-text.text-lighten-5 {
    color: #fafafa !important
}

.grey.lighten-4 {
    background-color: #f5f5f5 !important
}

.grey-text.text-lighten-4 {
    color: #f5f5f5 !important
}

.grey.lighten-3 {
    background-color: #eee !important
}

.grey-text.text-lighten-3 {
    color: #eee !important
}

.grey.lighten-2 {
    background-color: #e0e0e0 !important
}

.grey-text.text-lighten-2 {
    color: #e0e0e0 !important
}

.grey.lighten-1 {
    background-color: #bdbdbd !important
}

.grey-text.text-lighten-1 {
    color: #bdbdbd !important
}

.grey.darken-1 {
    background-color: #757575 !important
}

.grey-text.text-darken-1 {
    color: #757575 !important
}

.grey.darken-2 {
    background-color: #616161 !important
}

.grey-text.text-darken-2 {
    color: #616161 !important
}

.grey.darken-3 {
    background-color: #424242 !important
}

.grey-text.text-darken-3 {
    color: #424242 !important
}

.grey.darken-4 {
    background-color: #212121 !important
}

.grey-text.text-darken-4 {
    color: #212121 !important
}

.black {
    background-color: #000 !important
}

.black-text {
    color: #000 !important
}

.white {
    background-color: #fff !important
}

.white-text {
    color: #fff !important
}

.transparent {
    background-color: transparent !important
}

.transparent-text {
    color: transparent !important
}

/*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
html {
    font-family: sans-serif;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%
}

body {
    margin: 0
}

article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary {
    display: block
}

audio,canvas,progress,video {
    display: inline-block;
    vertical-align: baseline
}

audio:not([controls]) {
    display: none;
    height: 0
}

[hidden],template {
    display: none
}

a {
    background-color: transparent
}

a:active,a:hover {
    outline: 0
}

abbr[title] {
    border-bottom: 1px dotted
}

b,strong {
    font-weight: bold
}

dfn {
    font-style: italic
}

h1 {
    font-size: 2em;
    margin: 0.67em 0
}

mark {
    background: #ff0;
    color: #000
}

small {
    font-size: 80%
}

sub,sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline
}

sup {
    top: -0.5em
}

sub {
    bottom: -0.25em
}

img {
    border: 0
}

svg:not(:root) {
    overflow: hidden
}

figure {
    margin: 1em 40px
}

hr {
    box-sizing: content-box;
    height: 0
}

pre {
    overflow: auto
}

code,kbd,pre,samp {
    font-family: monospace, monospace;
    font-size: 1em
}

button,input,optgroup,select,textarea {
    color: inherit;
    font: inherit;
    margin: 0
}

button {
    overflow: visible
}

button,select {
    text-transform: none
}

button,html input[type="button"],input[type="reset"],input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer
}

button[disabled],html input[disabled] {
    cursor: default
}

button::-moz-focus-inner,input::-moz-focus-inner {
    border: 0;
    padding: 0
}

input {
    line-height: normal
}

input[type="checkbox"],input[type="radio"] {
    box-sizing: border-box;
    padding: 0
}

input[type="number"]::-webkit-inner-spin-button,input[type="number"]::-webkit-outer-spin-button {
    height: auto
}

input[type="search"] {
    -webkit-appearance: textfield;
    box-sizing: content-box
}

input[type="search"]::-webkit-search-cancel-button,input[type="search"]::-webkit-search-decoration {
    -webkit-appearance: none
}

fieldset {
    border: 1px solid #c0c0c0;
    margin: 0 2px;
    padding: 0.35em 0.625em 0.75em
}

legend {
    border: 0;
    padding: 0
}

textarea {
    overflow: auto
}

optgroup {
    font-weight: bold
}

table {
    border-collapse: collapse;
    border-spacing: 0
}

td,th {
    padding: 0
}

html {
    box-sizing: border-box
}

*,*:before,*:after {
    box-sizing: inherit
}

ul:not(.browser-default) {
    padding-left: 0;
    list-style-type: none
}

ul:not(.browser-default) li {
    list-style-type: none
}

a {
    color: #039be5;
    text-decoration: none;
    -webkit-tap-highlight-color: transparent
}

.valign-wrapper {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center
}

.valign-wrapper .valign {
    display: block
}

.clearfix {
    clear: both
}

.z-depth-0 {
    box-shadow: none !important
}

.z-depth-1,nav,.card-panel,.card,.toast,.btn,.btn-large,.btn-floating,.dropdown-content,.collapsible,.side-nav {
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14),0 1px 5px 0 rgba(0,0,0,0.12),0 3px 1px -2px rgba(0,0,0,0.2)
}

.z-depth-1-half,.btn:hover,.btn-large:hover,.btn-floating:hover {
    box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14),0 1px 7px 0 rgba(0,0,0,0.12),0 3px 1px -1px rgba(0,0,0,0.2)
}

.z-depth-2 {
    box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14),0 1px 10px 0 rgba(0,0,0,0.12),0 2px 4px -1px rgba(0,0,0,0.3)
}

.z-depth-3 {
    box-shadow: 0 6px 10px 0 rgba(0,0,0,0.14),0 1px 18px 0 rgba(0,0,0,0.12),0 3px 5px -1px rgba(0,0,0,0.3)
}

.z-depth-4,.modal {
    box-shadow: 0 8px 10px 1px rgba(0,0,0,0.14),0 3px 14px 2px rgba(0,0,0,0.12),0 5px 5px -3px rgba(0,0,0,0.3)
}

.z-depth-5 {
    box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14),0 6px 30px 5px rgba(0,0,0,0.12),0 8px 10px -5px rgba(0,0,0,0.3)
}

.hoverable {
    transition: box-shadow .25s;
    box-shadow: 0
}

.hoverable:hover {
    transition: box-shadow .25s;
    box-shadow: 0 8px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)
}

.divider {
    height: 1px;
    overflow: hidden;
    background-color: #e0e0e0
}

blockquote {
    margin: 20px 0;
    padding-left: 1.5rem;
    border-left: 5px solid #ee6e73
}

i {
    line-height: inherit
}

i.left {
    float: left;
    margin-right: 15px
}

i.right {
    float: right;
    margin-left: 15px
}

i.tiny {
    font-size: 1rem
}

i.small {
    font-size: 2rem
}

i.medium {
    font-size: 4rem
}

i.large {
    font-size: 6rem
}

img.responsive-img,video.responsive-video {
    max-width: 100%;
    height: auto
}

.pagination li {
    display: inline-block;
    border-radius: 2px;
    text-align: center;
    vertical-align: top;
    height: 30px
}

.pagination li a {
    color: #444;
    display: inline-block;
    font-size: 1.2rem;
    padding: 0 10px;
    line-height: 30px
}

.pagination li.active a {
    color: #fff
}

.pagination li.active {
    background-color: #ee6e73
}

.pagination li.disabled a {
    cursor: default;
    color: #999
}

.pagination li i {
    font-size: 2rem
}

.pagination li.pages ul li {
    display: inline-block;
    float: none
}

@media only screen and (max-width: 992px) {
    .pagination {
        width:100%
    }

    .pagination li.prev,.pagination li.next {
        width: 10%
    }

    .pagination li.pages {
        width: 80%;
        overflow: hidden;
        white-space: nowrap
    }
}

.breadcrumb {
    font-size: 18px;
    color: rgba(255,255,255,0.7)
}

.breadcrumb i,.breadcrumb [class^="mdi-"],.breadcrumb [class*="mdi-"],.breadcrumb i.material-icons {
    display: inline-block;
    float: left;
    font-size: 24px
}

.breadcrumb:before {
    content: '\E5CC';
    color: rgba(255,255,255,0.7);
    vertical-align: top;
    display: inline-block;
    font-family: 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 25px;
    margin: 0 10px 0 8px;
    -webkit-font-smoothing: antialiased
}

.breadcrumb:first-child:before {
    display: none
}

.breadcrumb:last-child {
    color: #fff
}

.parallax-container {
    position: relative;
    overflow: hidden;
    height: 500px
}

.parallax {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: -1
}

.parallax img {
    display: none;
    position: absolute;
    left: 50%;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%)
}

.pin-top,.pin-bottom {
    position: relative
}

.pinned {
    position: fixed !important
}

ul.staggered-list li {
    opacity: 0
}

.fade-in {
    opacity: 0;
    -webkit-transform-origin: 0 50%;
    transform-origin: 0 50%
}

@media only screen and (max-width: 600px) {
    .hide-on-small-only,.hide-on-small-and-down {
        display:none !important
    }
}

@media only screen and (max-width: 992px) {
    .hide-on-med-and-down {
        display:none !important
    }
}

@media only screen and (min-width: 601px) {
    .hide-on-med-and-up {
        display:none !important
    }
}

@media only screen and (min-width: 600px) and (max-width: 992px) {
    .hide-on-med-only {
        display:none !important
    }
}

@media only screen and (min-width: 993px) {
    .hide-on-large-only {
        display:none !important
    }
}

@media only screen and (min-width: 993px) {
    .show-on-large {
        display:block !important
    }
}

@media only screen and (min-width: 600px) and (max-width: 992px) {
    .show-on-medium {
        display:block !important
    }
}

@media only screen and (max-width: 600px) {
    .show-on-small {
        display:block !important
    }
}

@media only screen and (min-width: 601px) {
    .show-on-medium-and-up {
        display:block !important
    }
}

@media only screen and (max-width: 992px) {
    .show-on-medium-and-down {
        display:block !important
    }
}

@media only screen and (max-width: 600px) {
    .center-on-small-only {
        text-align:center
    }
}

footer.page-footer {
    margin-top: 20px;
    padding-top: 20px;
    background-color: #ee6e73
}

footer.page-footer .footer-copyright {
    overflow: hidden;
    height: 50px;
    line-height: 50px;
    color: rgba(255,255,255,0.8);
    background-color: rgba(51,51,51,0.08)
}

table,th,td {
    border: none
}

table {
    width: 100%;
    display: table
}

table.bordered>thead>tr,table.bordered>tbody>tr {
    border-bottom: 1px solid #d0d0d0
}

table.striped>tbody>tr:nth-child(odd) {
    background-color: #f2f2f2
}

table.striped>tbody>tr>td {
    border-radius: 0
}

table.highlight>tbody>tr {
    transition: background-color .25s ease
}

table.highlight>tbody>tr:hover {
    background-color: #f2f2f2
}

table.centered thead tr th,table.centered tbody tr td {
    text-align: center
}

thead {
    border-bottom: 1px solid #d0d0d0
}

td,th {
    padding: 15px 5px;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
    border-radius: 2px
}

@media only screen and (max-width: 992px) {
    table.responsive-table {
        width:100%;
        border-collapse: collapse;
        border-spacing: 0;
        display: block;
        position: relative
    }

    table.responsive-table td:empty:before {
        content: '\00a0'
    }

    table.responsive-table th,table.responsive-table td {
        margin: 0;
        vertical-align: top
    }

    table.responsive-table th {
        text-align: left
    }

    table.responsive-table thead {
        display: block;
        float: left
    }

    table.responsive-table thead tr {
        display: block;
        padding: 0 10px 0 0
    }

    table.responsive-table thead tr th::before {
        content: "\00a0"
    }

    table.responsive-table tbody {
        display: block;
        width: auto;
        position: relative;
        overflow-x: auto;
        white-space: nowrap
    }

    table.responsive-table tbody tr {
        display: inline-block;
        vertical-align: top
    }

    table.responsive-table th {
        display: block;
        text-align: right
    }

    table.responsive-table td {
        display: block;
        min-height: 1.25em;
        text-align: left
    }

    table.responsive-table tr {
        padding: 0 10px
    }

    table.responsive-table thead {
        border: 0;
        border-right: 1px solid #d0d0d0
    }

    table.responsive-table.bordered th {
        border-bottom: 0;
        border-left: 0
    }

    table.responsive-table.bordered td {
        border-left: 0;
        border-right: 0;
        border-bottom: 0
    }

    table.responsive-table.bordered tr {
        border: 0
    }

    table.responsive-table.bordered tbody tr {
        border-right: 1px solid #d0d0d0
    }
}

.collection {
    margin: 0.5rem 0 1rem 0;
    border: 1px solid #e0e0e0;
    border-radius: 2px;
    overflow: hidden;
    position: relative
}

.collection .collection-item {
    background-color: #fff;
    line-height: 1.5rem;
    padding: 10px 20px;
    margin: 0;
    border-bottom: 1px solid #e0e0e0
}

.collection .collection-item.avatar {
    min-height: 84px;
    padding-left: 72px;
    position: relative
}

.collection .collection-item.avatar .circle {
    position: absolute;
    width: 42px;
    height: 42px;
    overflow: hidden;
    left: 15px;
    display: inline-block;
    vertical-align: middle
}

.collection .collection-item.avatar i.circle {
    font-size: 18px;
    line-height: 42px;
    color: #fff;
    background-color: #999;
    text-align: center
}

.collection .collection-item.avatar .title {
    font-size: 16px
}

.collection .collection-item.avatar p {
    margin: 0
}

.collection .collection-item.avatar .secondary-content {
    position: absolute;
    top: 16px;
    right: 16px
}

.collection .collection-item:last-child {
    border-bottom: none
}

.collection .collection-item.active {
    background-color: #40c4ff;
    color: #eafaf9
}

.collection .collection-item.active .secondary-content {
    color: #fff
}

.collection a.collection-item {
    display: block;
    transition: .25s;
    color: #40c4ff
}

.collection a.collection-item:not(.active):hover {
    background-color: #ddd
}

.collection.with-header .collection-header {
    background-color: #fff;
    border-bottom: 1px solid #e0e0e0;
    padding: 10px 20px
}

.collection.with-header .collection-item {
    padding-left: 30px
}

.collection.with-header .collection-item.avatar {
    padding-left: 72px
}

.secondary-content {
    float: right;
    color: #40c4ff
}

.collapsible .collection {
    margin: 0;
    border: none
}

span.badge {
    min-width: 3rem;
    padding: 0 6px;
    margin-left: 14px;
    text-align: center;
    font-size: 1rem;
    line-height: inherit;
    color: #757575;
    float: right;
    box-sizing: border-box
}

span.badge.new {
    font-weight: 300;
    font-size: 0.8rem;
    color: #fff;
    background-color: #40c4ff;
    border-radius: 2px
}

span.badge.new:after {
    content: " new"
}

span.badge[data-badge-caption]::after {
    content: " " attr(data-badge-caption)
}

nav ul a span.badge {
    display: inline-block;
    float: none;
    margin-left: 4px;
    line-height: 22px;
    height: 22px
}

.side-nav span.badge.new,.collapsible span.badge.new {
    position: relative;
    background-color: transparent
}

.side-nav span.badge.new::before,.collapsible span.badge.new::before {
    content: '';
    position: absolute;
    top: 10px;
    right: 0;
    bottom: 10px;
    left: 0;
    background-color: #40c4ff;
    border-radius: 2px;
    z-index: -1
}

.collapsible span.badge.new {
    z-index: 1
}

.video-container {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden
}

.video-container iframe,.video-container object,.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%
}

.progress {
    position: relative;
    height: 4px;
    display: block;
    width: 100%;
    background-color: #acece6;
    border-radius: 2px;
    margin: 0.5rem 0 1rem 0;
    overflow: hidden
}

.progress .determinate {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    background-color: #40c4ff;
    transition: width .3s linear
}

.progress .indeterminate {
    background-color: #40c4ff
}

.progress .indeterminate:before {
    content: '';
    position: absolute;
    background-color: inherit;
    top: 0;
    left: 0;
    bottom: 0;
    will-change: left, right;
    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
    animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite
}

.progress .indeterminate:after {
    content: '';
    position: absolute;
    background-color: inherit;
    top: 0;
    left: 0;
    bottom: 0;
    will-change: left, right;
    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
    animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
    -webkit-animation-delay: 1.15s;
    animation-delay: 1.15s
}

@-webkit-keyframes indeterminate {
    0% {
        left: -35%;
        right: 100%
    }

    60% {
        left: 100%;
        right: -90%
    }

    100% {
        left: 100%;
        right: -90%
    }
}

@keyframes indeterminate {
    0% {
        left: -35%;
        right: 100%
    }

    60% {
        left: 100%;
        right: -90%
    }

    100% {
        left: 100%;
        right: -90%
    }
}

@-webkit-keyframes indeterminate-short {
    0% {
        left: -200%;
        right: 100%
    }

    60% {
        left: 107%;
        right: -8%
    }

    100% {
        left: 107%;
        right: -8%
    }
}

@keyframes indeterminate-short {
    0% {
        left: -200%;
        right: 100%
    }

    60% {
        left: 107%;
        right: -8%
    }

    100% {
        left: 107%;
        right: -8%
    }
}

.hide {
    display: none !important
}

.left-align {
    text-align: left
}

.right-align {
    text-align: right
}

.center,.center-align {
    text-align: center
}

.left {
    float: left !important
}

.right {
    float: right !important
}

.no-select,input[type=range],input[type=range]+.thumb {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none
}

.circle {
    border-radius: 50%
}

.center-block {
    display: block;
    margin-left: auto;
    margin-right: auto
}

.truncate {
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis
}

.no-padding {
    padding: 0 !important
}

.material-icons {
    text-rendering: optimizeLegibility;
    -webkit-font-feature-settings: 'liga';
    -moz-font-feature-settings: 'liga';
    font-feature-settings: 'liga'
}

.container {
    margin: 0 auto;
    max-width: 1280px;
    width: 90%
}

@media only screen and (min-width: 601px) {
    .container {
        width:85%
    }
}

@media only screen and (min-width: 993px) {
    .container {
        width:70%
    }
}

.container .row {
    margin-left: -0.75rem;
    margin-right: -0.75rem
}

.section {
    padding-top: 1rem;
    padding-bottom: 1rem
}

.section.no-pad {
    padding: 0
}

.section.no-pad-bot {
    padding-bottom: 0
}

.section.no-pad-top {
    padding-top: 0
}

.row {
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 20px
}

.row:after {
    content: "";
    display: table;
    clear: both
}

.row .col {
    float: left;
    box-sizing: border-box;
    padding: 0 0.75rem;
    min-height: 1px
}

.row .col[class*="push-"],.row .col[class*="pull-"] {
    position: relative
}

.row .col.s1 {
    width: 8.3333333333%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s2 {
    width: 16.6666666667%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s3 {
    width: 25%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s4 {
    width: 33.3333333333%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s5 {
    width: 41.6666666667%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s6 {
    width: 50%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s7 {
    width: 58.3333333333%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s8 {
    width: 66.6666666667%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s9 {
    width: 75%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s10 {
    width: 83.3333333333%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s11 {
    width: 91.6666666667%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.s12 {
    width: 100%;
    margin-left: auto;
    left: auto;
    right: auto
}

.row .col.offset-s1 {
    margin-left: 8.3333333333%
}

.row .col.pull-s1 {
    right: 8.3333333333%
}

.row .col.push-s1 {
    left: 8.3333333333%
}

.row .col.offset-s2 {
    margin-left: 16.6666666667%
}

.row .col.pull-s2 {
    right: 16.6666666667%
}

.row .col.push-s2 {
    left: 16.6666666667%
}

.row .col.offset-s3 {
    margin-left: 25%
}

.row .col.pull-s3 {
    right: 25%
}

.row .col.push-s3 {
    left: 25%
}

.row .col.offset-s4 {
    margin-left: 33.3333333333%
}

.row .col.pull-s4 {
    right: 33.3333333333%
}

.row .col.push-s4 {
    left: 33.3333333333%
}

.row .col.offset-s5 {
    margin-left: 41.6666666667%
}

.row .col.pull-s5 {
    right: 41.6666666667%
}

.row .col.push-s5 {
    left: 41.6666666667%
}

.row .col.offset-s6 {
    margin-left: 50%
}

.row .col.pull-s6 {
    right: 50%
}

.row .col.push-s6 {
    left: 50%
}

.row .col.offset-s7 {
    margin-left: 58.3333333333%
}

.row .col.pull-s7 {
    right: 58.3333333333%
}

.row .col.push-s7 {
    left: 58.3333333333%
}

.row .col.offset-s8 {
    margin-left: 66.6666666667%
}

.row .col.pull-s8 {
    right: 66.6666666667%
}

.row .col.push-s8 {
    left: 66.6666666667%
}

.row .col.offset-s9 {
    margin-left: 75%
}

.row .col.pull-s9 {
    right: 75%
}

.row .col.push-s9 {
    left: 75%
}

.row .col.offset-s10 {
    margin-left: 83.3333333333%
}

.row .col.pull-s10 {
    right: 83.3333333333%
}

.row .col.push-s10 {
    left: 83.3333333333%
}

.row .col.offset-s11 {
    margin-left: 91.6666666667%
}

.row .col.pull-s11 {
    right: 91.6666666667%
}

.row .col.push-s11 {
    left: 91.6666666667%
}

.row .col.offset-s12 {
    margin-left: 100%
}

.row .col.pull-s12 {
    right: 100%
}

.row .col.push-s12 {
    left: 100%
}

@media only screen and (min-width: 601px) {
    .row .col.m1 {
        width:8.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m2 {
        width: 16.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m3 {
        width: 25%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m4 {
        width: 33.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m5 {
        width: 41.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m6 {
        width: 50%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m7 {
        width: 58.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m8 {
        width: 66.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m9 {
        width: 75%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m10 {
        width: 83.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m11 {
        width: 91.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.m12 {
        width: 100%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.offset-m1 {
        margin-left: 8.3333333333%
    }

    .row .col.pull-m1 {
        right: 8.3333333333%
    }

    .row .col.push-m1 {
        left: 8.3333333333%
    }

    .row .col.offset-m2 {
        margin-left: 16.6666666667%
    }

    .row .col.pull-m2 {
        right: 16.6666666667%
    }

    .row .col.push-m2 {
        left: 16.6666666667%
    }

    .row .col.offset-m3 {
        margin-left: 25%
    }

    .row .col.pull-m3 {
        right: 25%
    }

    .row .col.push-m3 {
        left: 25%
    }

    .row .col.offset-m4 {
        margin-left: 33.3333333333%
    }

    .row .col.pull-m4 {
        right: 33.3333333333%
    }

    .row .col.push-m4 {
        left: 33.3333333333%
    }

    .row .col.offset-m5 {
        margin-left: 41.6666666667%
    }

    .row .col.pull-m5 {
        right: 41.6666666667%
    }

    .row .col.push-m5 {
        left: 41.6666666667%
    }

    .row .col.offset-m6 {
        margin-left: 50%
    }

    .row .col.pull-m6 {
        right: 50%
    }

    .row .col.push-m6 {
        left: 50%
    }

    .row .col.offset-m7 {
        margin-left: 58.3333333333%
    }

    .row .col.pull-m7 {
        right: 58.3333333333%
    }

    .row .col.push-m7 {
        left: 58.3333333333%
    }

    .row .col.offset-m8 {
        margin-left: 66.6666666667%
    }

    .row .col.pull-m8 {
        right: 66.6666666667%
    }

    .row .col.push-m8 {
        left: 66.6666666667%
    }

    .row .col.offset-m9 {
        margin-left: 75%
    }

    .row .col.pull-m9 {
        right: 75%
    }

    .row .col.push-m9 {
        left: 75%
    }

    .row .col.offset-m10 {
        margin-left: 83.3333333333%
    }

    .row .col.pull-m10 {
        right: 83.3333333333%
    }

    .row .col.push-m10 {
        left: 83.3333333333%
    }

    .row .col.offset-m11 {
        margin-left: 91.6666666667%
    }

    .row .col.pull-m11 {
        right: 91.6666666667%
    }

    .row .col.push-m11 {
        left: 91.6666666667%
    }

    .row .col.offset-m12 {
        margin-left: 100%
    }

    .row .col.pull-m12 {
        right: 100%
    }

    .row .col.push-m12 {
        left: 100%
    }
}

@media only screen and (min-width: 993px) {
    .row .col.l1 {
        width:8.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l2 {
        width: 16.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l3 {
        width: 25%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l4 {
        width: 33.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l5 {
        width: 41.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l6 {
        width: 50%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l7 {
        width: 58.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l8 {
        width: 66.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l9 {
        width: 75%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l10 {
        width: 83.3333333333%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l11 {
        width: 91.6666666667%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.l12 {
        width: 100%;
        margin-left: auto;
        left: auto;
        right: auto
    }

    .row .col.offset-l1 {
        margin-left: 8.3333333333%
    }

    .row .col.pull-l1 {
        right: 8.3333333333%
    }

    .row .col.push-l1 {
        left: 8.3333333333%
    }

    .row .col.offset-l2 {
        margin-left: 16.6666666667%
    }

    .row .col.pull-l2 {
        right: 16.6666666667%
    }

    .row .col.push-l2 {
        left: 16.6666666667%
    }

    .row .col.offset-l3 {
        margin-left: 25%
    }

    .row .col.pull-l3 {
        right: 25%
    }

    .row .col.push-l3 {
        left: 25%
    }

    .row .col.offset-l4 {
        margin-left: 33.3333333333%
    }

    .row .col.pull-l4 {
        right: 33.3333333333%
    }

    .row .col.push-l4 {
        left: 33.3333333333%
    }

    .row .col.offset-l5 {
        margin-left: 41.6666666667%
    }

    .row .col.pull-l5 {
        right: 41.6666666667%
    }

    .row .col.push-l5 {
        left: 41.6666666667%
    }

    .row .col.offset-l6 {
        margin-left: 50%
    }

    .row .col.pull-l6 {
        right: 50%
    }

    .row .col.push-l6 {
        left: 50%
    }

    .row .col.offset-l7 {
        margin-left: 58.3333333333%
    }

    .row .col.pull-l7 {
        right: 58.3333333333%
    }

    .row .col.push-l7 {
        left: 58.3333333333%
    }

    .row .col.offset-l8 {
        margin-left: 66.6666666667%
    }

    .row .col.pull-l8 {
        right: 66.6666666667%
    }

    .row .col.push-l8 {
        left: 66.6666666667%
    }

    .row .col.offset-l9 {
        margin-left: 75%
    }

    .row .col.pull-l9 {
        right: 75%
    }

    .row .col.push-l9 {
        left: 75%
    }

    .row .col.offset-l10 {
        margin-left: 83.3333333333%
    }

    .row .col.pull-l10 {
        right: 83.3333333333%
    }

    .row .col.push-l10 {
        left: 83.3333333333%
    }

    .row .col.offset-l11 {
        margin-left: 91.6666666667%
    }

    .row .col.pull-l11 {
        right: 91.6666666667%
    }

    .row .col.push-l11 {
        left: 91.6666666667%
    }

    .row .col.offset-l12 {
        margin-left: 100%
    }

    .row .col.pull-l12 {
        right: 100%
    }

    .row .col.push-l12 {
        left: 100%
    }
}

nav {
    color: #fff;
    background-color: #ee6e73;
    width: 100%;
    height: 56px;
    line-height: 56px
}

nav.nav-extended {
    height: auto
}

nav.nav-extended .nav-wrapper {
    height: auto
}

nav a {
    color: #fff
}

nav i,nav [class^="mdi-"],nav [class*="mdi-"],nav i.material-icons {
    display: block;
    font-size: 24px;
    height: 56px;
    line-height: 56px
}

nav .nav-wrapper {
    position: relative;
    height: 100%
}

@media only screen and (min-width: 993px) {
    nav a.button-collapse {
        display:none
    }
}

nav .button-collapse {
    float: left;
    position: relative;
    z-index: 1;
    height: 56px;
    margin: 0 18px
}

nav .button-collapse i {
    height: 56px;
    line-height: 56px
}

nav .brand-logo {
    position: absolute;
    color: #fff;
    display: inline-block;
    font-size: 2.1rem;
    padding: 0;
    white-space: nowrap
}

nav .brand-logo.center {
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%)
}

@media only screen and (max-width: 992px) {
    nav .brand-logo {
        left:50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%)
    }

    nav .brand-logo.left,nav .brand-logo.right {
        padding: 0;
        -webkit-transform: none;
        transform: none
    }

    nav .brand-logo.left {
        left: 0.5rem
    }

    nav .brand-logo.right {
        right: 0.5rem;
        left: auto
    }
}

nav .brand-logo.right {
    right: 0.5rem;
    padding: 0
}

nav .brand-logo i,nav .brand-logo [class^="mdi-"],nav .brand-logo [class*="mdi-"],nav .brand-logo i.material-icons {
    float: left;
    margin-right: 15px
}

nav ul {
    margin: 0
}

nav ul li {
    transition: background-color .3s;
    float: left;
    padding: 0
}

nav ul li.active {
    background-color: rgba(0,0,0,0.1)
}

nav ul a {
    transition: background-color .3s;
    font-size: 1rem;
    color: #fff;
    display: block;
    padding: 0 15px;
    cursor: pointer
}

nav ul a.btn,nav ul a.btn-large,nav ul a.btn-large,nav ul a.btn-flat,nav ul a.btn-floating {
    margin-top: -2px;
    margin-left: 15px;
    margin-right: 15px
}

nav ul a:hover {
    background-color: rgba(0,0,0,0.1)
}

nav ul.left {
    float: left
}

nav form {
    height: 100%
}

nav .input-field {
    margin: 0;
    height: 100%
}

nav .input-field input {
    height: 100%;
    font-size: 1.2rem;
    border: none;
    padding-left: 2rem
}

nav .input-field input:focus,nav .input-field input[type=text]:valid,nav .input-field input[type=password]:valid,nav .input-field input[type=email]:valid,nav .input-field input[type=url]:valid,nav .input-field input[type=date]:valid {
    border: none;
    box-shadow: none
}

nav .input-field label {
    top: 0;
    left: 0
}

nav .input-field label i {
    color: rgba(255,255,255,0.7);
    transition: color .3s
}

nav .input-field label.active i {
    color: #fff
}

nav .input-field label.active {
    -webkit-transform: translateY(0);
    transform: translateY(0)
}

.navbar-fixed {
    position: relative;
    height: 56px;
    z-index: 997
}

.navbar-fixed nav {
    position: fixed
}

@media only screen and (min-width: 601px) {
    nav,nav .nav-wrapper i,nav a.button-collapse,nav a.button-collapse i {
        height:64px;
        line-height: 64px
    }

    .navbar-fixed {
        height: 64px
    }
}

@font-face {
    font-family: "Roboto";
    src: local(Roboto Thin),url("../fonts/roboto/Roboto-Thin.eot");
    src: url("../fonts/roboto/Roboto-Thin.eot?#iefix") format("embedded-opentype"),url("../fonts/roboto/Roboto-Thin.woff2") format("woff2"),url("../fonts/roboto/Roboto-Thin.woff") format("woff"),url("../fonts/roboto/Roboto-Thin.ttf") format("truetype");
    font-weight: 200
}

@font-face {
    font-family: "Roboto";
    src: local(Roboto Light),url("../fonts/roboto/Roboto-Light.eot");
    src: url("../fonts/roboto/Roboto-Light.eot?#iefix") format("embedded-opentype"),url("../fonts/roboto/Roboto-Light.woff2") format("woff2"),url("../fonts/roboto/Roboto-Light.woff") format("woff"),url("../fonts/roboto/Roboto-Light.ttf") format("truetype");
    font-weight: 300
}

@font-face {
    font-family: "Roboto";
    src: local(Roboto Regular),url("../fonts/roboto/Roboto-Regular.eot");
    src: url("../fonts/roboto/Roboto-Regular.eot?#iefix") format("embedded-opentype"),url("../fonts/roboto/Roboto-Regular.woff2") format("woff2"),url("../fonts/roboto/Roboto-Regular.woff") format("woff"),url("../fonts/roboto/Roboto-Regular.ttf") format("truetype");
    font-weight: 400
}

@font-face {
    font-family: "Roboto";
    src: url("../fonts/roboto/Roboto-Medium.eot");
    src: url("../fonts/roboto/Roboto-Medium.eot?#iefix") format("embedded-opentype"),url("../fonts/roboto/Roboto-Medium.woff2") format("woff2"),url("../fonts/roboto/Roboto-Medium.woff") format("woff"),url("../fonts/roboto/Roboto-Medium.ttf") format("truetype");
    font-weight: 500
}

@font-face {
    font-family: "Roboto";
    src: url("../fonts/roboto/Roboto-Bold.eot");
    src: url("../fonts/roboto/Roboto-Bold.eot?#iefix") format("embedded-opentype"),url("../fonts/roboto/Roboto-Bold.woff2") format("woff2"),url("../fonts/roboto/Roboto-Bold.woff") format("woff"),url("../fonts/roboto/Roboto-Bold.ttf") format("truetype");
    font-weight: 700
}

a {
    text-decoration: none
}

html {
    line-height: 1.5;
    font-family: "Roboto", sans-serif;
    font-weight: normal;
    color: rgba(0,0,0,0.87)
}

@media only screen and (min-width: 0) {
    html {
        font-size:14px
    }
}

@media only screen and (min-width: 992px) {
    html {
        font-size:14.5px
    }
}

@media only screen and (min-width: 1200px) {
    html {
        font-size:15px
    }
}

h1,h2,h3,h4,h5,h6 {
    font-weight: 400;
    line-height: 1.1
}

h1 a,h2 a,h3 a,h4 a,h5 a,h6 a {
    font-weight: inherit
}

h1 {
    font-size: 4.2rem;
    line-height: 110%;
    margin: 2.1rem 0 1.68rem 0
}

h2 {
    font-size: 3.56rem;
    line-height: 110%;
    margin: 1.78rem 0 1.424rem 0
}

h3 {
    font-size: 2.92rem;
    line-height: 110%;
    margin: 1.46rem 0 1.168rem 0
}

h4 {
    font-size: 2.28rem;
    line-height: 110%;
    margin: 1.14rem 0 0.912rem 0
}

h5 {
    font-size: 1.64rem;
    line-height: 110%;
    margin: 0.82rem 0 0.656rem 0
}

h6 {
    font-size: 1rem;
    line-height: 110%;
    margin: 0.5rem 0 0.4rem 0
}

em {
    font-style: italic
}

strong {
    font-weight: 500
}

small {
    font-size: 75%
}

.light,footer.page-footer .footer-copyright {
    font-weight: 300
}

.thin {
    font-weight: 200
}

.flow-text {
    font-weight: 300
}

@media only screen and (min-width: 360px) {
    .flow-text {
        font-size:1.2rem
    }
}

@media only screen and (min-width: 390px) {
    .flow-text {
        font-size:1.224rem
    }
}

@media only screen and (min-width: 420px) {
    .flow-text {
        font-size:1.248rem
    }
}

@media only screen and (min-width: 450px) {
    .flow-text {
        font-size:1.272rem
    }
}

@media only screen and (min-width: 480px) {
    .flow-text {
        font-size:1.296rem
    }
}

@media only screen and (min-width: 510px) {
    .flow-text {
        font-size:1.32rem
    }
}

@media only screen and (min-width: 540px) {
    .flow-text {
        font-size:1.344rem
    }
}

@media only screen and (min-width: 570px) {
    .flow-text {
        font-size:1.368rem
    }
}

@media only screen and (min-width: 600px) {
    .flow-text {
        font-size:1.392rem
    }
}

@media only screen and (min-width: 630px) {
    .flow-text {
        font-size:1.416rem
    }
}

@media only screen and (min-width: 660px) {
    .flow-text {
        font-size:1.44rem
    }
}

@media only screen and (min-width: 690px) {
    .flow-text {
        font-size:1.464rem
    }
}

@media only screen and (min-width: 720px) {
    .flow-text {
        font-size:1.488rem
    }
}

@media only screen and (min-width: 750px) {
    .flow-text {
        font-size:1.512rem
    }
}

@media only screen and (min-width: 780px) {
    .flow-text {
        font-size:1.536rem
    }
}

@media only screen and (min-width: 810px) {
    .flow-text {
        font-size:1.56rem
    }
}

@media only screen and (min-width: 840px) {
    .flow-text {
        font-size:1.584rem
    }
}

@media only screen and (min-width: 870px) {
    .flow-text {
        font-size:1.608rem
    }
}

@media only screen and (min-width: 900px) {
    .flow-text {
        font-size:1.632rem
    }
}

@media only screen and (min-width: 930px) {
    .flow-text {
        font-size:1.656rem
    }
}

@media only screen and (min-width: 960px) {
    .flow-text {
        font-size:1.68rem
    }
}

@media only screen and (max-width: 360px) {
    .flow-text {
        font-size:1.2rem
    }
}

.card-panel {
    transition: box-shadow .25s;
    padding: 20px;
    margin: 0.5rem 0 1rem 0;
    border-radius: 2px;
    background-color: #fff
}

.card {
    position: relative;
    margin: 0.5rem 0 1rem 0;
    background-color: #fff;
    transition: box-shadow .25s;
    border-radius: 2px
}

.card .card-title {
    font-size: 24px;
    font-weight: 300
}

.card .card-title.activator {
    cursor: pointer
}

.card.small,.card.medium,.card.large {
    position: relative
}

.card.small .card-image,.card.medium .card-image,.card.large .card-image {
    max-height: 60%;
    overflow: hidden
}

.card.small .card-image+.card-content,.card.medium .card-image+.card-content,.card.large .card-image+.card-content {
    max-height: 40%
}

.card.small .card-content,.card.medium .card-content,.card.large .card-content {
    max-height: 100%;
    overflow: hidden
}

.card.small .card-action,.card.medium .card-action,.card.large .card-action {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0
}

.card.small {
    height: 300px
}

.card.medium {
    height: 400px
}

.card.large {
    height: 500px
}

.card.horizontal {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex
}

.card.horizontal.small .card-image,.card.horizontal.medium .card-image,.card.horizontal.large .card-image {
    height: 100%;
    max-height: none;
    overflow: visible
}

.card.horizontal.small .card-image img,.card.horizontal.medium .card-image img,.card.horizontal.large .card-image img {
    height: 100%
}

.card.horizontal .card-image {
    max-width: 50%
}

.card.horizontal .card-image img {
    border-radius: 2px 0 0 2px;
    max-width: 100%;
    width: auto
}

.card.horizontal .card-stacked {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-flex: 1;
    -ms-flex: 1;
    flex: 1;
    position: relative
}

.card.horizontal .card-stacked .card-content {
    -webkit-flex-grow: 1;
    -ms-flex-positive: 1;
    flex-grow: 1
}

.card.sticky-action .card-action {
    z-index: 2
}

.card.sticky-action .card-reveal {
    z-index: 1;
    padding-bottom: 64px
}

.card .card-image {
    position: relative
}

.card .card-image img {
    display: block;
    border-radius: 2px 2px 0 0;
    position: relative;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    width: 100%
}

.card .card-image .card-title {
    color: #fff;
    position: absolute;
    bottom: 0;
    left: 0;
    padding: 20px
}

.card .card-content {
    padding: 20px;
    border-radius: 0 0 2px 2px
}

.card .card-content p {
    margin: 0;
    color: inherit
}

.card .card-content .card-title {
    line-height: 48px
}

.card .card-action {
    position: relative;
    background-color: inherit;
    border-top: 1px solid rgba(160,160,160,0.2);
    padding: 20px
}

.card .card-action a:not(.btn):not(.btn-large):not(.btn-floating) {
    color: #ffab40;
    margin-right: 20px;
    transition: color .3s ease;
    text-transform: uppercase
}

.card .card-action a:not(.btn):not(.btn-large):not(.btn-floating):hover {
    color: #ffd8a6
}

.card .card-reveal {
    padding: 20px;
    position: absolute;
    background-color: #fff;
    width: 100%;
    overflow-y: auto;
    left: 0;
    top: 100%;
    height: 100%;
    z-index: 3;
    display: none
}

.card .card-reveal .card-title {
    cursor: pointer;
    display: block
}

#toast-container {
    display: block;
    position: fixed;
    z-index: 10000
}

@media only screen and (max-width: 600px) {
    #toast-container {
        min-width:100%;
        bottom: 0%
    }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
    #toast-container {
        left:5%;
        bottom: 7%;
        max-width: 90%
    }
}

@media only screen and (min-width: 993px) {
    #toast-container {
        top:10%;
        right: 7%;
        max-width: 86%
    }
}

.toast {
    border-radius: 2px;
    top: 0;
    width: auto;
    clear: both;
    margin-top: 10px;
    position: relative;
    max-width: 100%;
    height: auto;
    min-height: 48px;
    line-height: 1.5em;
    word-break: break-all;
    background-color: #323232;
    padding: 10px 25px;
    font-size: 1.1rem;
    font-weight: 300;
    color: #fff;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between
}

.toast .btn,.toast .btn-large,.toast .btn-flat {
    margin: 0;
    margin-left: 3rem
}

.toast.rounded {
    border-radius: 24px
}

@media only screen and (max-width: 600px) {
    .toast {
        width:100%;
        border-radius: 0
    }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
    .toast {
        float:left
    }
}

@media only screen and (min-width: 993px) {
    .toast {
        float:right
    }
}

.tabs {
    position: relative;
    overflow-x: auto;
    overflow-y: hidden;
    height: 48px;
    width: 100%;
    background-color: #fff;
    margin: 0 auto;
    white-space: nowrap
}

.tabs.tabs-transparent {
    background-color: transparent
}

.tabs.tabs-transparent .tab a,.tabs.tabs-transparent .tab.disabled a,.tabs.tabs-transparent .tab.disabled a:hover {
    color: rgba(255,255,255,0.7)
}

.tabs.tabs-transparent .tab a:hover,.tabs.tabs-transparent .tab a.active {
    color: #fff
}

.tabs.tabs-transparent .indicator {
    background-color: #fff
}

.tabs.tabs-fixed-width {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex
}

.tabs.tabs-fixed-width .tab {
    -webkit-box-flex: 1;
    -webkit-flex-grow: 1;
    -ms-flex-positive: 1;
    flex-grow: 1
}

.tabs .tab {
    display: inline-block;
    text-align: center;
    line-height: 48px;
    height: 48px;
    padding: 0;
    margin: 0;
    text-transform: uppercase
}

.tabs .tab a {
    color: rgba(238,110,115,0.7);
    display: block;
    width: 100%;
    height: 100%;
    padding: 0 24px;
    font-size: 14px;
    text-overflow: ellipsis;
    overflow: hidden;
    transition: color .28s ease
}

.tabs .tab a:hover,.tabs .tab a.active {
    background-color: transparent;
    color: #ee6e73
}

.tabs .tab.disabled a,.tabs .tab.disabled a:hover {
    color: rgba(238,110,115,0.7);
    cursor: default
}

.tabs .indicator {
    position: absolute;
    bottom: 0;
    height: 2px;
    background-color: #f6b2b5;
    will-change: left, right
}

@media only screen and (max-width: 992px) {
    .tabs {
        display:-webkit-flex;
        display: -ms-flexbox;
        display: flex
    }

    .tabs .tab {
        -webkit-box-flex: 1;
        -webkit-flex-grow: 1;
        -ms-flex-positive: 1;
        flex-grow: 1
    }

    .tabs .tab a {
        padding: 0 12px
    }
}

.material-tooltip {
    padding: 10px 8px;
    font-size: 1rem;
    z-index: 2000;
    background-color: transparent;
    border-radius: 2px;
    color: #fff;
    min-height: 36px;
    line-height: 120%;
    opacity: 0;
    display: none;
    position: absolute;
    text-align: center;
    max-width: calc(100% - 4px);
    overflow: hidden;
    left: 0;
    top: 0;
    pointer-events: none
}

.backdrop {
    position: absolute;
    opacity: 0;
    display: none;
    height: 7px;
    width: 14px;
    border-radius: 0 0 50% 50%;
    background-color: #323232;
    z-index: -1;
    -webkit-transform-origin: 50% 0%;
    transform-origin: 50% 0%;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0)
}

.btn,.btn-large,.btn-flat {
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 36px;
    line-height: 36px;
    padding: 0 2rem;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent
}

.btn.disabled,.disabled.btn-large,.btn-floating.disabled,.btn-large.disabled,.btn-flat.disabled,.btn:disabled,.btn-large:disabled,.btn-floating:disabled,.btn-large:disabled,.btn-flat:disabled,.btn[disabled],[disabled].btn-large,.btn-floating[disabled],.btn-large[disabled],.btn-flat[disabled] {
    pointer-events: none;
    background-color: #DFDFDF !important;
    box-shadow: none;
    color: #9F9F9F !important;
    cursor: default
}

.btn.disabled:hover,.disabled.btn-large:hover,.btn-floating.disabled:hover,.btn-large.disabled:hover,.btn-flat.disabled:hover,.btn:disabled:hover,.btn-large:disabled:hover,.btn-floating:disabled:hover,.btn-large:disabled:hover,.btn-flat:disabled:hover,.btn[disabled]:hover,[disabled].btn-large:hover,.btn-floating[disabled]:hover,.btn-large[disabled]:hover,.btn-flat[disabled]:hover {
    background-color: #DFDFDF !important;
    color: #9F9F9F !important
}

.btn,.btn-large,.btn-floating,.btn-large,.btn-flat {
    outline: 0
}

.btn i,.btn-large i,.btn-floating i,.btn-large i,.btn-flat i {
    font-size: 1.3rem;
    line-height: inherit
}

.btn:focus,.btn-large:focus,.btn-floating:focus {
    background-color: #1d7d74
}

.btn,.btn-large {
    text-decoration: none;
    color: #fff;
    background-color: #40c4ff;
    text-align: center;
    letter-spacing: .5px;
    transition: .2s ease-out;
    cursor: pointer
}

.btn:hover,.btn-large:hover {
    background-color: #2bbbad
}

.btn-floating {
    display: inline-block;
    color: #fff;
    position: relative;
    overflow: hidden;
    z-index: 1;
    width: 40px;
    height: 40px;
    line-height: 40px;
    padding: 0;
    background-color: #40c4ff;
    border-radius: 50%;
    transition: .3s;
    cursor: pointer;
    vertical-align: middle
}

.btn-floating i {
    width: inherit;
    display: inline-block;
    text-align: center;
    color: #fff;
    font-size: 1.6rem;
    line-height: 40px
}

.btn-floating:hover {
    background-color: #40c4ff
}

.btn-floating:before {
    border-radius: 0
}

.btn-floating.btn-large {
    width: 56px;
    height: 56px
}

.btn-floating.btn-large i {
    line-height: 56px
}

button.btn-floating {
    border: none
}

.fixed-action-btn {
    position: fixed;
    right: 23px;
    bottom: 23px;
    padding-top: 15px;
    margin-bottom: 0;
    z-index: 998
}

.fixed-action-btn.active ul {
    visibility: visible
}

.fixed-action-btn.horizontal {
    padding: 0 0 0 15px
}

.fixed-action-btn.horizontal ul {
    text-align: right;
    right: 64px;
    top: 50%;
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);
    height: 100%;
    left: auto;
    width: 500px
}

.fixed-action-btn.horizontal ul li {
    display: inline-block;
    margin: 15px 15px 0 0
}

.fixed-action-btn.toolbar {
    padding: 0;
    height: 56px
}

.fixed-action-btn.toolbar.active>a i {
    opacity: 0
}

.fixed-action-btn.toolbar ul {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    top: 0;
    bottom: 0
}

.fixed-action-btn.toolbar ul li {
    -webkit-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: inline-block;
    margin: 0;
    height: 100%;
    transition: none
}

.fixed-action-btn.toolbar ul li a {
    display: block;
    overflow: hidden;
    position: relative;
    width: 100%;
    height: 100%;
    background-color: transparent;
    box-shadow: none;
    color: #fff;
    line-height: 56px;
    z-index: 1
}

.fixed-action-btn.toolbar ul li a i {
    line-height: inherit
}

.fixed-action-btn ul {
    left: 0;
    right: 0;
    text-align: center;
    position: absolute;
    bottom: 64px;
    margin: 0;
    visibility: hidden
}

.fixed-action-btn ul li {
    margin-bottom: 15px
}

.fixed-action-btn ul a.btn-floating {
    opacity: 0
}

.fixed-action-btn .fab-backdrop {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    width: 40px;
    height: 40px;
    background-color: #40c4ff;
    border-radius: 50%;
    -webkit-transform: scale(0);
    transform: scale(0)
}

.btn-flat {
    box-shadow: none;
    background-color: transparent;
    color: #343434;
    cursor: pointer;
    transition: background-color .2s
}

.btn-flat:focus,.btn-flat:active {
    background-color: transparent
}

.btn-flat:focus,.btn-flat:hover {
    background-color: rgba(0,0,0,0.1);
    box-shadow: none
}

.btn-flat:active {
    background-color: rgba(0,0,0,0.2)
}

.btn-flat.disabled {
    background-color: transparent !important;
    color: #b3b3b3 !important;
    cursor: default
}

.btn-large {
    height: 54px;
    line-height: 54px
}

.btn-large i {
    font-size: 1.6rem
}

.btn-block {
    display: block
}

.dropdown-content {
    background-color: #fff;
    margin: 0;
    display: none;
    min-width: 100px;
    max-height: 650px;
    overflow-y: auto;
    opacity: 0;
    position: absolute;
    z-index: 999;
    will-change: width, height
}

.dropdown-content li {
    clear: both;
    color: rgba(0,0,0,0.87);
    cursor: pointer;
    min-height: 50px;
    line-height: 1.5rem;
    width: 100%;
    text-align: left;
    text-transform: none
}

.dropdown-content li:hover,.dropdown-content li.active,.dropdown-content li.selected {
    background-color: #eee
}

.dropdown-content li.active.selected {
    background-color: #e1e1e1
}

.dropdown-content li.divider {
    min-height: 0;
    height: 1px
}

.dropdown-content li>a,.dropdown-content li>span {
    font-size: 16px;
    color: #40c4ff;
    display: block;
    line-height: 22px;
    padding: 14px 16px
}

.dropdown-content li>span>label {
    top: 1px;
    left: 0;
    height: 18px
}

.dropdown-content li>a>i {
    height: inherit;
    line-height: inherit
}

.input-field.col .dropdown-content [type="checkbox"]+label {
    top: 1px;
    left: 0;
    height: 18px
}

/*!
 * Waves v0.6.0
 * http://fian.my.id/Waves
 *
 * Copyright 2014 Alfiana E. Sibuea and other contributors
 * Released under the MIT license
 * https://github.com/fians/Waves/blob/master/LICENSE
 */
.waves-effect {
    position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    will-change: opacity, transform;
    transition: .3s ease-out
}

.waves-effect .waves-ripple {
    position: absolute;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    margin-top: -10px;
    margin-left: -10px;
    opacity: 0;
    background: rgba(0,0,0,0.2);
    transition: all 0.7s ease-out;
    transition-property: opacity, -webkit-transform;
    transition-property: transform, opacity;
    transition-property: transform, opacity, -webkit-transform;
    -webkit-transform: scale(0);
    transform: scale(0);
    pointer-events: none
}

.waves-effect.waves-light .waves-ripple {
    background-color: rgba(255,255,255,0.45)
}

.waves-effect.waves-red .waves-ripple {
    background-color: rgba(244,67,54,0.7)
}

.waves-effect.waves-yellow .waves-ripple {
    background-color: rgba(255,235,59,0.7)
}

.waves-effect.waves-orange .waves-ripple {
    background-color: rgba(255,152,0,0.7)
}

.waves-effect.waves-purple .waves-ripple {
    background-color: rgba(156,39,176,0.7)
}

.waves-effect.waves-green .waves-ripple {
    background-color: rgba(76,175,80,0.7)
}

.waves-effect.waves-teal .waves-ripple {
    background-color: rgba(0,150,136,0.7)
}

.waves-effect input[type="button"],.waves-effect input[type="reset"],.waves-effect input[type="submit"] {
    border: 0;
    font-style: normal;
    font-size: inherit;
    text-transform: inherit;
    background: none
}

.waves-effect img {
    position: relative;
    z-index: -1
}

.waves-notransition {
    transition: none !important
}

.waves-circle {
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-mask-image: -webkit-radial-gradient(circle, #fff 100%, #000 100%)
}

.waves-input-wrapper {
    border-radius: 0.2em;
    vertical-align: bottom
}

.waves-input-wrapper .waves-button-input {
    position: relative;
    top: 0;
    left: 0;
    z-index: 1
}

.waves-circle {
    text-align: center;
    width: 2.5em;
    height: 2.5em;
    line-height: 2.5em;
    border-radius: 50%;
    -webkit-mask-image: none
}

.waves-block {
    display: block
}

.waves-effect .waves-ripple {
    z-index: -1
}

.modal {
    display: none;
    position: fixed;
    left: 0;
    right: 0;
    background-color: #fafafa;
    padding: 0;
    max-height: 70%;
    width: 55%;
    margin: auto;
    overflow-y: auto;
    border-radius: 2px;
    will-change: top, opacity
}

@media only screen and (max-width: 992px) {
    .modal {
        width:80%
    }
}

.modal h1,.modal h2,.modal h3,.modal h4 {
    margin-top: 0
}

.modal .modal-content {
    padding: 24px
}

.modal .modal-close {
    cursor: pointer
}

.modal .modal-footer {
    border-radius: 0 0 2px 2px;
    background-color: #fafafa;
    padding: 4px 6px;
    height: 56px;
    width: 100%
}

.modal .modal-footer .btn,.modal .modal-footer .btn-large,.modal .modal-footer .btn-flat {
    float: right;
    margin: 6px 0
}

.modal-overlay {
    position: fixed;
    z-index: 999;
    top: -100px;
    left: 0;
    bottom: 0;
    right: 0;
    height: 125%;
    width: 100%;
    background: #000;
    display: none;
    will-change: opacity
}

.modal.modal-fixed-footer {
    padding: 0;
    height: 70%
}

.modal.modal-fixed-footer .modal-content {
    position: absolute;
    height: calc(100% - 56px);
    max-height: 100%;
    width: 100%;
    overflow-y: auto
}

.modal.modal-fixed-footer .modal-footer {
    border-top: 1px solid rgba(0,0,0,0.1);
    position: absolute;
    bottom: 0
}

.modal.bottom-sheet {
    top: auto;
    bottom: -100%;
    margin: 0;
    width: 100%;
    max-height: 45%;
    border-radius: 0;
    will-change: bottom, opacity
}

.collapsible {
    border-top: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-left: 1px solid #ddd;
    margin: 0.5rem 0 1rem 0
}

.collapsible-header {
    display: block;
    cursor: pointer;
    min-height: 3rem;
    line-height: 3rem;
    padding: 0 1rem;
    background-color: #fff;
    border-bottom: 1px solid #ddd
}

.collapsible-header i {
    width: 2rem;
    font-size: 1.6rem;
    line-height: 3rem;
    display: block;
    float: left;
    text-align: center;
    margin-right: 1rem
}

.collapsible-body {
    display: none;
    border-bottom: 1px solid #ddd;
    box-sizing: border-box
}

.collapsible-body p {
    margin: 0;
    padding: 2rem
}

.side-nav .collapsible,.side-nav.fixed .collapsible {
    border: none;
    box-shadow: none
}

.side-nav .collapsible li,.side-nav.fixed .collapsible li {
    padding: 0
}

.side-nav .collapsible-header,.side-nav.fixed .collapsible-header {
    background-color: transparent;
    border: none;
    line-height: inherit;
    height: inherit;
    padding: 0 16px
}

.side-nav .collapsible-header:hover,.side-nav.fixed .collapsible-header:hover {
    background-color: rgba(0,0,0,0.05)
}

.side-nav .collapsible-header i,.side-nav.fixed .collapsible-header i {
    line-height: inherit
}

.side-nav .collapsible-body,.side-nav.fixed .collapsible-body {
    border: 0;
    background-color: #fff
}

.side-nav .collapsible-body li a,.side-nav.fixed .collapsible-body li a {
    padding: 0 23.5px 0 31px
}

.collapsible.popout {
    border: none;
    box-shadow: none
}

.collapsible.popout>li {
    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
    margin: 0 24px;
    transition: margin 0.35s cubic-bezier(0.25, 0.46, 0.45, 0.94)
}

.collapsible.popout>li.active {
    box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
    margin: 16px 0
}

.chip {
    display: inline-block;
    height: 32px;
    font-size: 13px;
    font-weight: 500;
    color: rgba(0,0,0,0.6);
    line-height: 32px;
    padding: 0 12px;
    border-radius: 16px;
    background-color: #e4e4e4;
    margin-bottom: 5px;
    margin-right: 5px
}

.chip img {
    float: left;
    margin: 0 8px 0 -12px;
    height: 32px;
    width: 32px;
    border-radius: 50%
}

.chip .close {
    cursor: pointer;
    float: right;
    font-size: 16px;
    line-height: 32px;
    padding-left: 8px
}

.chips {
    border: none;
    border-bottom: 1px solid #9e9e9e;
    box-shadow: none;
    margin: 0 0 20px 0;
    min-height: 45px;
    outline: none;
    transition: all .3s
}

.chips.focus {
    border-bottom: 1px solid #40c4ff;
    box-shadow: 0 1px 0 0 #40c4ff
}

.chips:hover {
    cursor: text
}

.chips .chip.selected {
    background-color: #40c4ff;
    color: #fff
}

.chips .input {
    background: none;
    border: 0;
    color: rgba(0,0,0,0.6);
    display: inline-block;
    font-size: 1rem;
    height: 3rem;
    line-height: 32px;
    outline: 0;
    margin: 0;
    padding: 0 !important;
    width: 120px !important
}

.chips .input:focus {
    border: 0 !important;
    box-shadow: none !important
}

.prefix ~ .chips {
    margin-left: 3rem;
    width: 92%;
    width: calc(100% - 3rem)
}

.chips:empty ~ label {
    font-size: 0.8rem;
    -webkit-transform: translateY(-140%);
    transform: translateY(-140%)
}

.materialboxed {
    display: block;
    cursor: -webkit-zoom-in;
    cursor: zoom-in;
    position: relative;
    transition: opacity .4s
}

.materialboxed:hover {
    will-change: left, top, width, height
}

.materialboxed:hover:not(.active) {
    opacity: .8
}

.materialboxed.active {
    cursor: -webkit-zoom-out;
    cursor: zoom-out
}

#materialbox-overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #292929;
    z-index: 1000;
    will-change: opacity
}

.materialbox-caption {
    position: fixed;
    display: none;
    color: #fff;
    line-height: 50px;
    bottom: 0;
    width: 100%;
    text-align: center;
    padding: 0% 15%;
    height: 50px;
    z-index: 1000;
    -webkit-font-smoothing: antialiased
}

select:focus {
    outline: 1px solid #c9f3ef
}

button:focus {
    outline: none;
    background-color: #2ab7a9
}

label {
    font-size: 0.8rem;
    color: #9e9e9e
}

::-webkit-input-placeholder {
    color: #d1d1d1
}

:-moz-placeholder {
    color: #d1d1d1
}

::-moz-placeholder {
    color: #d1d1d1
}

:-ms-input-placeholder {
    color: #d1d1d1
}

input:not([type]),input[type=text],input[type=password],input[type=email],input[type=url],input[type=time],input[type=date],input[type=datetime],input[type=datetime-local],input[type=tel],input[type=number],input[type=search],textarea.materialize-textarea {
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #9e9e9e;
    border-radius: 0;
    outline: none;
    height: 3rem;
    width: 100%;
    font-size: 1rem;
    margin: 0 0 6px 0;
    padding: 0;
    box-shadow: none;
    box-sizing: content-box;
    transition: all 0.3s
}

input:not([type]):disabled,input:not([type])[readonly="readonly"],input[type=text]:disabled,input[type=text][readonly="readonly"],input[type=password]:disabled,input[type=password][readonly="readonly"],input[type=email]:disabled,input[type=email][readonly="readonly"],input[type=url]:disabled,input[type=url][readonly="readonly"],input[type=time]:disabled,input[type=time][readonly="readonly"],input[type=date]:disabled,input[type=date][readonly="readonly"],input[type=datetime]:disabled,input[type=datetime][readonly="readonly"],input[type=datetime-local]:disabled,input[type=datetime-local][readonly="readonly"],input[type=tel]:disabled,input[type=tel][readonly="readonly"],input[type=number]:disabled,input[type=number][readonly="readonly"],input[type=search]:disabled,input[type=search][readonly="readonly"],textarea.materialize-textarea:disabled,textarea.materialize-textarea[readonly="readonly"] {
    color: rgba(0,0,0,0.26);
    border-bottom: 1px dotted rgba(0,0,0,0.26)
}

input:not([type]):disabled+label,input:not([type])[readonly="readonly"]+label,input[type=text]:disabled+label,input[type=text][readonly="readonly"]+label,input[type=password]:disabled+label,input[type=password][readonly="readonly"]+label,input[type=email]:disabled+label,input[type=email][readonly="readonly"]+label,input[type=url]:disabled+label,input[type=url][readonly="readonly"]+label,input[type=time]:disabled+label,input[type=time][readonly="readonly"]+label,input[type=date]:disabled+label,input[type=date][readonly="readonly"]+label,input[type=datetime]:disabled+label,input[type=datetime][readonly="readonly"]+label,input[type=datetime-local]:disabled+label,input[type=datetime-local][readonly="readonly"]+label,input[type=tel]:disabled+label,input[type=tel][readonly="readonly"]+label,input[type=number]:disabled+label,input[type=number][readonly="readonly"]+label,input[type=search]:disabled+label,input[type=search][readonly="readonly"]+label,textarea.materialize-textarea:disabled+label,textarea.materialize-textarea[readonly="readonly"]+label {
    color: rgba(0,0,0,0.26)
}

input:not([type]):focus:not([readonly]),input[type=text]:focus:not([readonly]),input[type=password]:focus:not([readonly]),input[type=email]:focus:not([readonly]),input[type=url]:focus:not([readonly]),input[type=time]:focus:not([readonly]),input[type=date]:focus:not([readonly]),input[type=datetime]:focus:not([readonly]),input[type=datetime-local]:focus:not([readonly]),input[type=tel]:focus:not([readonly]),input[type=number]:focus:not([readonly]),input[type=search]:focus:not([readonly]),textarea.materialize-textarea:focus:not([readonly]) {
    border-bottom: 1px solid #40c4ff;
    box-shadow: 0 1px 0 0 #40c4ff
}

input:not([type]):focus:not([readonly])+label,input[type=text]:focus:not([readonly])+label,input[type=password]:focus:not([readonly])+label,input[type=email]:focus:not([readonly])+label,input[type=url]:focus:not([readonly])+label,input[type=time]:focus:not([readonly])+label,input[type=date]:focus:not([readonly])+label,input[type=datetime]:focus:not([readonly])+label,input[type=datetime-local]:focus:not([readonly])+label,input[type=tel]:focus:not([readonly])+label,input[type=number]:focus:not([readonly])+label,input[type=search]:focus:not([readonly])+label,textarea.materialize-textarea:focus:not([readonly])+label {
    color: #40c4ff
}

input:not([type]).valid,input:not([type]):focus.valid,input[type=text].valid,input[type=text]:focus.valid,input[type=password].valid,input[type=password]:focus.valid,input[type=email].valid,input[type=email]:focus.valid,input[type=url].valid,input[type=url]:focus.valid,input[type=time].valid,input[type=time]:focus.valid,input[type=date].valid,input[type=date]:focus.valid,input[type=datetime].valid,input[type=datetime]:focus.valid,input[type=datetime-local].valid,input[type=datetime-local]:focus.valid,input[type=tel].valid,input[type=tel]:focus.valid,input[type=number].valid,input[type=number]:focus.valid,input[type=search].valid,input[type=search]:focus.valid,textarea.materialize-textarea.valid,textarea.materialize-textarea:focus.valid {
    border-bottom: 1px solid #4CAF50;
    box-shadow: 0 1px 0 0 #4CAF50
}

input:not([type]).valid+label:after,input:not([type]):focus.valid+label:after,input[type=text].valid+label:after,input[type=text]:focus.valid+label:after,input[type=password].valid+label:after,input[type=password]:focus.valid+label:after,input[type=email].valid+label:after,input[type=email]:focus.valid+label:after,input[type=url].valid+label:after,input[type=url]:focus.valid+label:after,input[type=time].valid+label:after,input[type=time]:focus.valid+label:after,input[type=date].valid+label:after,input[type=date]:focus.valid+label:after,input[type=datetime].valid+label:after,input[type=datetime]:focus.valid+label:after,input[type=datetime-local].valid+label:after,input[type=datetime-local]:focus.valid+label:after,input[type=tel].valid+label:after,input[type=tel]:focus.valid+label:after,input[type=number].valid+label:after,input[type=number]:focus.valid+label:after,input[type=search].valid+label:after,input[type=search]:focus.valid+label:after,textarea.materialize-textarea.valid+label:after,textarea.materialize-textarea:focus.valid+label:after {
    content: attr(data-success);
    color: #4CAF50;
    opacity: 1
}

input:not([type]).invalid,input:not([type]):focus.invalid,input[type=text].invalid,input[type=text]:focus.invalid,input[type=password].invalid,input[type=password]:focus.invalid,input[type=email].invalid,input[type=email]:focus.invalid,input[type=url].invalid,input[type=url]:focus.invalid,input[type=time].invalid,input[type=time]:focus.invalid,input[type=date].invalid,input[type=date]:focus.invalid,input[type=datetime].invalid,input[type=datetime]:focus.invalid,input[type=datetime-local].invalid,input[type=datetime-local]:focus.invalid,input[type=tel].invalid,input[type=tel]:focus.invalid,input[type=number].invalid,input[type=number]:focus.invalid,input[type=search].invalid,input[type=search]:focus.invalid,textarea.materialize-textarea.invalid,textarea.materialize-textarea:focus.invalid {
    border-bottom: 1px solid #F44336;
    box-shadow: 0 1px 0 0 #F44336
}

input:not([type]).invalid+label:after,input:not([type]):focus.invalid+label:after,input[type=text].invalid+label:after,input[type=text]:focus.invalid+label:after,input[type=password].invalid+label:after,input[type=password]:focus.invalid+label:after,input[type=email].invalid+label:after,input[type=email]:focus.invalid+label:after,input[type=url].invalid+label:after,input[type=url]:focus.invalid+label:after,input[type=time].invalid+label:after,input[type=time]:focus.invalid+label:after,input[type=date].invalid+label:after,input[type=date]:focus.invalid+label:after,input[type=datetime].invalid+label:after,input[type=datetime]:focus.invalid+label:after,input[type=datetime-local].invalid+label:after,input[type=datetime-local]:focus.invalid+label:after,input[type=tel].invalid+label:after,input[type=tel]:focus.invalid+label:after,input[type=number].invalid+label:after,input[type=number]:focus.invalid+label:after,input[type=search].invalid+label:after,input[type=search]:focus.invalid+label:after,textarea.materialize-textarea.invalid+label:after,textarea.materialize-textarea:focus.invalid+label:after {
    content: attr(data-error);
    color: #F44336;
    opacity: 1
}

input:not([type]).validate+label,input[type=text].validate+label,input[type=password].validate+label,input[type=email].validate+label,input[type=url].validate+label,input[type=time].validate+label,input[type=date].validate+label,input[type=datetime].validate+label,input[type=datetime-local].validate+label,input[type=tel].validate+label,input[type=number].validate+label,input[type=search].validate+label,textarea.materialize-textarea.validate+label {
    width: 100%;
    pointer-events: none
}

input:not([type])+label:after,input[type=text]+label:after,input[type=password]+label:after,input[type=email]+label:after,input[type=url]+label:after,input[type=time]+label:after,input[type=date]+label:after,input[type=datetime]+label:after,input[type=datetime-local]+label:after,input[type=tel]+label:after,input[type=number]+label:after,input[type=search]+label:after,textarea.materialize-textarea+label:after {
    display: block;
    content: "";
    position: absolute;
    top: 60px;
    opacity: 0;
    transition: .2s opacity ease-out, .2s color ease-out
}

.input-field {
    position: relative;
    margin-top: 1rem
}

.input-field.inline {
    display: inline-block;
    vertical-align: middle;
    margin-left: 5px
}

.input-field.inline input,.input-field.inline .select-dropdown {
    margin-bottom: 1rem
}

.input-field.col label {
    left: 0.75rem
}

.input-field.col .prefix ~ label,.input-field.col .prefix ~ .validate ~ label {
    width: calc(100% - 3rem - 1.5rem)
}

.input-field label {
    color: #9e9e9e;
    position: absolute;
    top: 0.8rem;
    left: 0;
    font-size: 1rem;
    cursor: text;
    transition: .2s ease-out
}

.input-field label.active {
    font-size: 0.8rem;
    -webkit-transform: translateY(-140%);
    transform: translateY(-140%)
}

.input-field .prefix {
    position: absolute;
    width: 3rem;
    font-size: 2rem;
    transition: color .2s
}

.input-field .prefix.active {
    color: #40c4ff
}

.input-field .prefix ~ input,.input-field .prefix ~ textarea,.input-field .prefix ~ label,.input-field .prefix ~ .validate ~ label,.input-field .prefix ~ .autocomplete-content {
    margin-left: 3rem;
    width: 92%;
    width: calc(100% - 3rem)
}

.input-field .prefix ~ label {
    margin-left: 3rem
}

@media only screen and (max-width: 992px) {
    .input-field .prefix ~ input {
        width:86%;
        width: calc(100% - 3rem)
    }
}

@media only screen and (max-width: 600px) {
    .input-field .prefix ~ input {
        width:80%;
        width: calc(100% - 3rem)
    }
}

.input-field input[type=search] {
    display: block;
    line-height: inherit;
    padding-left: 4rem;
    width: calc(100% - 4rem)
}

.input-field input[type=search]:focus {
    background-color: #fff;
    border: 0;
    box-shadow: none;
    color: #444
}

.input-field input[type=search]:focus+label i,.input-field input[type=search]:focus ~ .mdi-navigation-close,.input-field input[type=search]:focus ~ .material-icons {
    color: #444
}

.input-field input[type=search]+label {
    left: 1rem
}

.input-field input[type=search] ~ .mdi-navigation-close,.input-field input[type=search] ~ .material-icons {
    position: absolute;
    top: 0;
    right: 1rem;
    color: transparent;
    cursor: pointer;
    font-size: 2rem;
    transition: .3s color
}

textarea {
    width: 100%;
    height: 3rem;
    background-color: transparent
}

textarea.materialize-textarea {
    overflow-y: hidden;
    padding: .8rem 0 1.6rem 0;
    resize: none;
    min-height: 3rem
}

.hiddendiv {
    display: none;
    white-space: pre-wrap;
    word-wrap: break-word;
    overflow-wrap: break-word;
    padding-top: 1.2rem
}

.autocomplete-content {
    margin-top: -15px;
    display: block;
    opacity: 1;
    position: static
}

.autocomplete-content li .highlight {
    color: #444
}

.autocomplete-content li img {
    height: 40px;
    width: 40px;
    margin: 5px 15px
}

[type="radio"]:not(:checked),[type="radio"]:checked {
    position: absolute;
    left: -9999px;
    opacity: 0
}

[type="radio"]:not(:checked)+label,[type="radio"]:checked+label {
    position: relative;
    padding-left: 35px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none
}

[type="radio"]+label:before,[type="radio"]+label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px;
    width: 16px;
    height: 16px;
    z-index: 0;
    transition: .28s ease
}

[type="radio"]:not(:checked)+label:before,[type="radio"]:not(:checked)+label:after,[type="radio"]:checked+label:before,[type="radio"]:checked+label:after,[type="radio"].with-gap:checked+label:before,[type="radio"].with-gap:checked+label:after {
    border-radius: 50%
}

[type="radio"]:not(:checked)+label:before,[type="radio"]:not(:checked)+label:after {
    border: 2px solid #5a5a5a
}

[type="radio"]:not(:checked)+label:after {
    -webkit-transform: scale(0);
    transform: scale(0)
}

[type="radio"]:checked+label:before {
    border: 2px solid transparent
}

[type="radio"]:checked+label:after,[type="radio"].with-gap:checked+label:before,[type="radio"].with-gap:checked+label:after {
    border: 2px solid #40c4ff
}

[type="radio"]:checked+label:after,[type="radio"].with-gap:checked+label:after {
    background-color: #40c4ff
}

[type="radio"]:checked+label:after {
    -webkit-transform: scale(1.02);
    transform: scale(1.02)
}

[type="radio"].with-gap:checked+label:after {
    -webkit-transform: scale(0.5);
    transform: scale(0.5)
}

[type="radio"].tabbed:focus+label:before {
    box-shadow: 0 0 0 10px rgba(0,0,0,0.1)
}

[type="radio"].with-gap:disabled:checked+label:before {
    border: 2px solid rgba(0,0,0,0.26)
}

[type="radio"].with-gap:disabled:checked+label:after {
    border: none;
    background-color: rgba(0,0,0,0.26)
}

[type="radio"]:disabled:not(:checked)+label:before,[type="radio"]:disabled:checked+label:before {
    background-color: transparent;
    border-color: rgba(0,0,0,0.26)
}

[type="radio"]:disabled+label {
    color: rgba(0,0,0,0.26)
}

[type="radio"]:disabled:not(:checked)+label:before {
    border-color: rgba(0,0,0,0.26)
}

[type="radio"]:disabled:checked+label:after {
    background-color: rgba(0,0,0,0.26);
    border-color: #BDBDBD
}

form p {
    margin-bottom: 10px;
    text-align: left
}

form p:last-child {
    margin-bottom: 0
}

[type="checkbox"]:not(:checked),[type="checkbox"]:checked {
    position: absolute;
    left: -9999px;
    opacity: 0
}

[type="checkbox"]+label {
    position: relative;
    padding-left: 35px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-user-select: none;
    -moz-user-select: none;
    -khtml-user-select: none;
    -ms-user-select: none
}

[type="checkbox"]+label:before,[type="checkbox"]:not(.filled-in)+label:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 18px;
    height: 18px;
    z-index: 0;
    border: 2px solid #5a5a5a;
    border-radius: 1px;
    margin-top: 2px;
    transition: .2s
}

[type="checkbox"]:not(.filled-in)+label:after {
    border: 0;
    -webkit-transform: scale(0);
    transform: scale(0)
}

[type="checkbox"]:not(:checked):disabled+label:before {
    border: none;
    background-color: rgba(0,0,0,0.26)
}

[type="checkbox"].tabbed:focus+label:after {
    -webkit-transform: scale(1);
    transform: scale(1);
    border: 0;
    border-radius: 50%;
    box-shadow: 0 0 0 10px rgba(0,0,0,0.1);
    background-color: rgba(0,0,0,0.1)
}

[type="checkbox"]:checked+label:before {
    top: -4px;
    left: -5px;
    width: 12px;
    height: 22px;
    border-top: 2px solid transparent;
    border-left: 2px solid transparent;
    border-right: 2px solid #40c4ff;
    border-bottom: 2px solid #40c4ff;
    -webkit-transform: rotate(40deg);
    transform: rotate(40deg);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transform-origin: 100% 100%;
    transform-origin: 100% 100%
}

[type="checkbox"]:checked:disabled+label:before {
    border-right: 2px solid rgba(0,0,0,0.26);
    border-bottom: 2px solid rgba(0,0,0,0.26)
}

[type="checkbox"]:indeterminate+label:before {
    top: -11px;
    left: -12px;
    width: 10px;
    height: 22px;
    border-top: none;
    border-left: none;
    border-right: 2px solid #40c4ff;
    border-bottom: none;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transform-origin: 100% 100%;
    transform-origin: 100% 100%
}

[type="checkbox"]:indeterminate:disabled+label:before {
    border-right: 2px solid rgba(0,0,0,0.26);
    background-color: transparent
}

[type="checkbox"].filled-in+label:after {
    border-radius: 2px
}

[type="checkbox"].filled-in+label:before,[type="checkbox"].filled-in+label:after {
    content: '';
    left: 0;
    position: absolute;
    transition: border .25s, background-color .25s, width .20s .1s, height .20s .1s, top .20s .1s, left .20s .1s;
    z-index: 1
}

[type="checkbox"].filled-in:not(:checked)+label:before {
    width: 0;
    height: 0;
    border: 3px solid transparent;
    left: 6px;
    top: 10px;
    -webkit-transform: rotateZ(37deg);
    transform: rotateZ(37deg);
    -webkit-transform-origin: 20% 40%;
    transform-origin: 100% 100%
}

[type="checkbox"].filled-in:not(:checked)+label:after {
    height: 20px;
    width: 20px;
    background-color: transparent;
    border: 2px solid #5a5a5a;
    top: 0px;
    z-index: 0
}

[type="checkbox"].filled-in:checked+label:before {
    top: 0;
    left: 1px;
    width: 8px;
    height: 13px;
    border-top: 2px solid transparent;
    border-left: 2px solid transparent;
    border-right: 2px solid #fff;
    border-bottom: 2px solid #fff;
    -webkit-transform: rotateZ(37deg);
    transform: rotateZ(37deg);
    -webkit-transform-origin: 100% 100%;
    transform-origin: 100% 100%
}

[type="checkbox"].filled-in:checked+label:after {
    top: 0;
    width: 20px;
    height: 20px;
    border: 2px solid #40c4ff;
    background-color: #40c4ff;
    z-index: 0
}

[type="checkbox"].filled-in.tabbed:focus+label:after {
    border-radius: 2px;
    border-color: #5a5a5a;
    background-color: rgba(0,0,0,0.1)
}

[type="checkbox"].filled-in.tabbed:checked:focus+label:after {
    border-radius: 2px;
    background-color: #40c4ff;
    border-color: #40c4ff
}

[type="checkbox"].filled-in:disabled:not(:checked)+label:before {
    background-color: transparent;
    border: 2px solid transparent
}

[type="checkbox"].filled-in:disabled:not(:checked)+label:after {
    border-color: transparent;
    background-color: #BDBDBD
}

[type="checkbox"].filled-in:disabled:checked+label:before {
    background-color: transparent
}

[type="checkbox"].filled-in:disabled:checked+label:after {
    background-color: #BDBDBD;
    border-color: #BDBDBD
}

.switch,.switch * {
    -webkit-user-select: none;
    -moz-user-select: none;
    -khtml-user-select: none;
    -ms-user-select: none
}

.switch label {
    cursor: pointer
}

.switch label input[type=checkbox] {
    opacity: 0;
    width: 0;
    height: 0
}

.switch label input[type=checkbox]:checked+.lever {
    background-color: #84c7c1
}

.switch label input[type=checkbox]:checked+.lever:after {
    background-color: #40c4ff;
    left: 24px
}

.switch label .lever {
    content: "";
    display: inline-block;
    position: relative;
    width: 40px;
    height: 15px;
    background-color: #818181;
    border-radius: 15px;
    margin-right: 10px;
    transition: background 0.3s ease;
    vertical-align: middle;
    margin: 0 16px
}

.switch label .lever:after {
    content: "";
    position: absolute;
    display: inline-block;
    width: 21px;
    height: 21px;
    background-color: #F1F1F1;
    border-radius: 21px;
    box-shadow: 0 1px 3px 1px rgba(0,0,0,0.4);
    left: -5px;
    top: -3px;
    transition: left 0.3s ease, background .3s ease, box-shadow 0.1s ease
}

input[type=checkbox]:checked:not(:disabled) ~ .lever:active::after,input[type=checkbox]:checked:not(:disabled).tabbed:focus ~ .lever::after {
    box-shadow: 0 1px 3px 1px rgba(0,0,0,0.4),0 0 0 15px rgba(38,166,154,0.1)
}

input[type=checkbox]:not(:disabled) ~ .lever:active:after,input[type=checkbox]:not(:disabled).tabbed:focus ~ .lever::after {
    box-shadow: 0 1px 3px 1px rgba(0,0,0,0.4),0 0 0 15px rgba(0,0,0,0.08)
}

.switch input[type=checkbox][disabled]+.lever {
    cursor: default
}

.switch label input[type=checkbox][disabled]+.lever:after,.switch label input[type=checkbox][disabled]:checked+.lever:after {
    background-color: #BDBDBD
}

select {
    display: none
}

select.browser-default {
    display: block
}

select {
    background-color: rgba(255,255,255,0.9);
    width: 100%;
    padding: 5px;
    border: 1px solid #f2f2f2;
    border-radius: 2px;
    height: 3rem
}

.select-label {
    position: absolute
}

.select-wrapper {
    position: relative
}

.select-wrapper input.select-dropdown {
    position: relative;
    cursor: pointer;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #9e9e9e;
    outline: none;
    height: 3rem;
    line-height: 3rem;
    width: 100%;
    font-size: 1rem;
    margin: 0 0 20px 0;
    padding: 0;
    display: block
}

.select-wrapper span.caret {
    color: initial;
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    height: 10px;
    margin: auto 0;
    font-size: 10px;
    line-height: 10px
}

.select-wrapper span.caret.disabled {
    color: rgba(0,0,0,0.26)
}

.select-wrapper+label {
    position: absolute;
    top: -14px;
    font-size: 0.8rem
}

select:disabled {
    color: rgba(0,0,0,0.3)
}

.select-wrapper input.select-dropdown:disabled {
    color: rgba(0,0,0,0.3);
    cursor: default;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    border-bottom: 1px solid rgba(0,0,0,0.3)
}

.select-wrapper i {
    color: rgba(0,0,0,0.3)
}

.select-dropdown li.disabled,.select-dropdown li.disabled>span,.select-dropdown li.optgroup {
    color: rgba(0,0,0,0.3);
    background-color: transparent
}

.prefix ~ .select-wrapper {
    margin-left: 3rem;
    width: 92%;
    width: calc(100% - 3rem)
}

.prefix ~ label {
    margin-left: 3rem
}

.select-dropdown li img {
    height: 40px;
    width: 40px;
    margin: 5px 15px;
    float: right
}

.select-dropdown li.optgroup {
    border-top: 1px solid #eee
}

.select-dropdown li.optgroup.selected>span {
    color: rgba(0,0,0,0.7)
}

.select-dropdown li.optgroup>span {
    color: rgba(0,0,0,0.4)
}

.select-dropdown li.optgroup ~ li.optgroup-option {
    padding-left: 1rem
}

.file-field {
    position: relative
}

.file-field .file-path-wrapper {
    overflow: hidden;
    padding-left: 10px
}

.file-field input.file-path {
    width: 100%
}

.file-field .btn,.file-field .btn-large {
    float: left;
    height: 3rem;
    line-height: 3rem
}

.file-field span {
    cursor: pointer
}

.file-field input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    width: 100%;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0)
}

.range-field {
    position: relative
}

input[type=range],input[type=range]+.thumb {
    cursor: pointer
}

input[type=range] {
    position: relative;
    background-color: transparent;
    border: none;
    outline: none;
    width: 100%;
    margin: 15px 0;
    padding: 0
}

input[type=range]:focus {
    outline: none
}

input[type=range]+.thumb {
    position: absolute;
    border: none;
    height: 0;
    width: 0;
    border-radius: 50%;
    background-color: #40c4ff;
    top: 10px;
    margin-left: -6px;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg)
}

input[type=range]+.thumb .value {
    display: block;
    width: 30px;
    text-align: center;
    color: #40c4ff;
    font-size: 0;
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg)
}

input[type=range]+.thumb.active {
    border-radius: 50% 50% 50% 0
}

input[type=range]+.thumb.active .value {
    color: #fff;
    margin-left: -1px;
    margin-top: 8px;
    font-size: 10px
}

input[type=range] {
    -webkit-appearance: none
}

input[type=range]::-webkit-slider-runnable-track {
    height: 3px;
    background: #c2c0c2;
    border: none
}

input[type=range]::-webkit-slider-thumb {
    -webkit-appearance: none;
    border: none;
    height: 14px;
    width: 14px;
    border-radius: 50%;
    background-color: #40c4ff;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
    margin: -5px 0 0 0;
    transition: .3s
}

input[type=range]:focus::-webkit-slider-runnable-track {
    background: #ccc
}

input[type=range] {
    border: 1px solid white
}

input[type=range]::-moz-range-track {
    height: 3px;
    background: #ddd;
    border: none
}

input[type=range]::-moz-range-thumb {
    border: none;
    height: 14px;
    width: 14px;
    border-radius: 50%;
    background: #40c4ff;
    margin-top: -5px
}

input[type=range]:-moz-focusring {
    outline: 1px solid #fff;
    outline-offset: -1px
}

input[type=range]:focus::-moz-range-track {
    background: #ccc
}

input[type=range]::-ms-track {
    height: 3px;
    background: transparent;
    border-color: transparent;
    border-width: 6px 0;
    color: transparent
}

input[type=range]::-ms-fill-lower {
    background: #777
}

input[type=range]::-ms-fill-upper {
    background: #ddd
}

input[type=range]::-ms-thumb {
    border: none;
    height: 14px;
    width: 14px;
    border-radius: 50%;
    background: #40c4ff
}

input[type=range]:focus::-ms-fill-lower {
    background: #888
}

input[type=range]:focus::-ms-fill-upper {
    background: #ccc
}

.table-of-contents.fixed {
    position: fixed
}

.table-of-contents li {
    padding: 2px 0
}

.table-of-contents a {
    display: inline-block;
    font-weight: 300;
    color: #757575;
    padding-left: 20px;
    height: 1.5rem;
    line-height: 1.5rem;
    letter-spacing: .4;
    display: inline-block
}

.table-of-contents a:hover {
    color: #a8a8a8;
    padding-left: 19px;
    border-left: 1px solid #ea4a4f
}

.table-of-contents a.active {
    font-weight: 500;
    padding-left: 18px;
    border-left: 2px solid #ea4a4f
}

.side-nav {
    position: fixed;
    width: 300px;
    left: 0;
    top: 0;
    margin: 0;
    -webkit-transform: translateX(-100%);
    transform: translateX(-100%);
    height: 100%;
    height: calc(100% + 60px);
    height: -moz-calc(100%);
    padding-bottom: 60px;
    background-color: #fff;
    z-index: 999;
    overflow-y: auto;
    will-change: transform;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transform: translateX(-105%);
    transform: translateX(-105%)
}

.side-nav.right-aligned {
    right: 0;
    -webkit-transform: translateX(105%);
    transform: translateX(105%);
    left: auto;
    -webkit-transform: translateX(100%);
    transform: translateX(100%)
}

.side-nav .collapsible {
    margin: 0
}

.side-nav li {
    float: none;
    line-height: 48px
}

.side-nav li.active {
    background-color: rgba(0,0,0,0.05)
}

.side-nav a {
    color: rgba(0,0,0,0.87);
    display: block;
    font-size: 14px;
    font-weight: 500;
    height: 48px;
    line-height: 48px;
    padding: 0 32px
}

.side-nav a:hover {
    background-color: rgba(0,0,0,0.05)
}

.side-nav a.btn,.side-nav a.btn-large,.side-nav a.btn-large,.side-nav a.btn-flat,.side-nav a.btn-floating {
    margin: 10px 15px
}

.side-nav a.btn,.side-nav a.btn-large,.side-nav a.btn-large,.side-nav a.btn-floating {
    color: #fff
}

.side-nav a.btn-flat {
    color: #343434
}

.side-nav a.btn:hover,.side-nav a.btn-large:hover,.side-nav a.btn-large:hover {
    background-color: #2bbbad
}

.side-nav a.btn-floating:hover {
    background-color: #40c4ff
}

.side-nav li>a>i,.side-nav li>a>[class^="mdi-"],.side-nav li>a>[class*="mdi-"],.side-nav li>a>i.material-icons {
    float: left;
    height: 48px;
    line-height: 48px;
    margin: 0 32px 0 0;
    width: 24px;
    color: rgba(0,0,0,0.54)
}

.side-nav .divider {
    margin: 8px 0 0 0
}

.side-nav .subheader {
    cursor: initial;
    pointer-events: none;
    color: rgba(0,0,0,0.54);
    font-size: 14px;
    font-weight: 500;
    line-height: 48px
}

.side-nav .subheader:hover {
    background-color: transparent
}

.side-nav .userView {
    position: relative;
    padding: 32px 32px 0;
    margin-bottom: 8px
}

.side-nav .userView>a {
    height: auto;
    padding: 0
}

.side-nav .userView>a:hover {
    background-color: transparent
}

.side-nav .userView .background {
    overflow: hidden;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -1
}

.side-nav .userView .circle,.side-nav .userView .name,.side-nav .userView .email {
    display: block
}

.side-nav .userView .circle {
    height: 64px;
    width: 64px
}

.side-nav .userView .name,.side-nav .userView .email {
    font-size: 14px;
    line-height: 24px
}

.side-nav .userView .name {
    margin-top: 16px;
    font-weight: 500
}

.side-nav .userView .email {
    padding-bottom: 16px;
    font-weight: 400
}

.drag-target {
    height: 100%;
    width: 10px;
    position: fixed;
    top: 0;
    z-index: 998
}

.side-nav.fixed {
    left: 0;
    -webkit-transform: translateX(0);
    transform: translateX(0);
    position: fixed
}

.side-nav.fixed.right-aligned {
    right: 0;
    left: auto
}

@media only screen and (max-width: 992px) {
    .side-nav.fixed {
        -webkit-transform:translateX(-105%);
        transform: translateX(-105%)
    }

    .side-nav.fixed.right-aligned {
        -webkit-transform: translateX(105%);
        transform: translateX(105%)
    }

    .side-nav a {
        padding: 0 16px
    }

    .side-nav .userView {
        padding: 16px 16px 0
    }
}

.side-nav .collapsible-body>ul:not(.collapsible)>li.active,.side-nav.fixed .collapsible-body>ul:not(.collapsible)>li.active {
    background-color: #ee6e73
}

.side-nav .collapsible-body>ul:not(.collapsible)>li.active a,.side-nav.fixed .collapsible-body>ul:not(.collapsible)>li.active a {
    color: #fff
}

#sidenav-overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    height: 120vh;
    background-color: rgba(0,0,0,0.5);
    z-index: 997;
    will-change: opacity
}

.preloader-wrapper {
    display: inline-block;
    position: relative;
    width: 48px;
    height: 48px
}

.preloader-wrapper.small {
    width: 36px;
    height: 36px
}

.preloader-wrapper.big {
    width: 64px;
    height: 64px
}

.preloader-wrapper.active {
    -webkit-animation: container-rotate 1568ms linear infinite;
    animation: container-rotate 1568ms linear infinite
}

@-webkit-keyframes container-rotate {
    to {
        -webkit-transform: rotate(360deg)
    }
}

@keyframes container-rotate {
    to {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg)
    }
}

.spinner-layer {
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
    border-color: #40c4ff
}

.spinner-blue,.spinner-blue-only {
    border-color: #4285f4
}

.spinner-red,.spinner-red-only {
    border-color: #db4437
}

.spinner-yellow,.spinner-yellow-only {
    border-color: #f4b400
}

.spinner-green,.spinner-green-only {
    border-color: #0f9d58
}

.active .spinner-layer.spinner-blue {
    -webkit-animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,blue-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,blue-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

.active .spinner-layer.spinner-red {
    -webkit-animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,red-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,red-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

.active .spinner-layer.spinner-yellow {
    -webkit-animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,yellow-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,yellow-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

.active .spinner-layer.spinner-green {
    -webkit-animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,green-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both,green-fade-in-out 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

.active .spinner-layer,.active .spinner-layer.spinner-blue-only,.active .spinner-layer.spinner-red-only,.active .spinner-layer.spinner-yellow-only,.active .spinner-layer.spinner-green-only {
    opacity: 1;
    -webkit-animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

@-webkit-keyframes fill-unfill-rotate {
    12.5% {
        -webkit-transform: rotate(135deg)
    }

    25% {
        -webkit-transform: rotate(270deg)
    }

    37.5% {
        -webkit-transform: rotate(405deg)
    }

    50% {
        -webkit-transform: rotate(540deg)
    }

    62.5% {
        -webkit-transform: rotate(675deg)
    }

    75% {
        -webkit-transform: rotate(810deg)
    }

    87.5% {
        -webkit-transform: rotate(945deg)
    }

    to {
        -webkit-transform: rotate(1080deg)
    }
}

@keyframes fill-unfill-rotate {
    12.5% {
        -webkit-transform: rotate(135deg);
        transform: rotate(135deg)
    }

    25% {
        -webkit-transform: rotate(270deg);
        transform: rotate(270deg)
    }

    37.5% {
        -webkit-transform: rotate(405deg);
        transform: rotate(405deg)
    }

    50% {
        -webkit-transform: rotate(540deg);
        transform: rotate(540deg)
    }

    62.5% {
        -webkit-transform: rotate(675deg);
        transform: rotate(675deg)
    }

    75% {
        -webkit-transform: rotate(810deg);
        transform: rotate(810deg)
    }

    87.5% {
        -webkit-transform: rotate(945deg);
        transform: rotate(945deg)
    }

    to {
        -webkit-transform: rotate(1080deg);
        transform: rotate(1080deg)
    }
}

@-webkit-keyframes blue-fade-in-out {
    from {
        opacity: 1
    }

    25% {
        opacity: 1
    }

    26% {
        opacity: 0
    }

    89% {
        opacity: 0
    }

    90% {
        opacity: 1
    }

    100% {
        opacity: 1
    }
}

@keyframes blue-fade-in-out {
    from {
        opacity: 1
    }

    25% {
        opacity: 1
    }

    26% {
        opacity: 0
    }

    89% {
        opacity: 0
    }

    90% {
        opacity: 1
    }

    100% {
        opacity: 1
    }
}

@-webkit-keyframes red-fade-in-out {
    from {
        opacity: 0
    }

    15% {
        opacity: 0
    }

    25% {
        opacity: 1
    }

    50% {
        opacity: 1
    }

    51% {
        opacity: 0
    }
}

@keyframes red-fade-in-out {
    from {
        opacity: 0
    }

    15% {
        opacity: 0
    }

    25% {
        opacity: 1
    }

    50% {
        opacity: 1
    }

    51% {
        opacity: 0
    }
}

@-webkit-keyframes yellow-fade-in-out {
    from {
        opacity: 0
    }

    40% {
        opacity: 0
    }

    50% {
        opacity: 1
    }

    75% {
        opacity: 1
    }

    76% {
        opacity: 0
    }
}

@keyframes yellow-fade-in-out {
    from {
        opacity: 0
    }

    40% {
        opacity: 0
    }

    50% {
        opacity: 1
    }

    75% {
        opacity: 1
    }

    76% {
        opacity: 0
    }
}

@-webkit-keyframes green-fade-in-out {
    from {
        opacity: 0
    }

    65% {
        opacity: 0
    }

    75% {
        opacity: 1
    }

    90% {
        opacity: 1
    }

    100% {
        opacity: 0
    }
}

@keyframes green-fade-in-out {
    from {
        opacity: 0
    }

    65% {
        opacity: 0
    }

    75% {
        opacity: 1
    }

    90% {
        opacity: 1
    }

    100% {
        opacity: 0
    }
}

.gap-patch {
    position: absolute;
    top: 0;
    left: 45%;
    width: 10%;
    height: 100%;
    overflow: hidden;
    border-color: inherit
}

.gap-patch .circle {
    width: 1000%;
    left: -450%
}

.circle-clipper {
    display: inline-block;
    position: relative;
    width: 50%;
    height: 100%;
    overflow: hidden;
    border-color: inherit
}

.circle-clipper .circle {
    width: 200%;
    height: 100%;
    border-width: 3px;
    border-style: solid;
    border-color: inherit;
    border-bottom-color: transparent !important;
    border-radius: 50%;
    -webkit-animation: none;
    animation: none;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0
}

.circle-clipper.left .circle {
    left: 0;
    border-right-color: transparent !important;
    -webkit-transform: rotate(129deg);
    transform: rotate(129deg)
}

.circle-clipper.right .circle {
    left: -100%;
    border-left-color: transparent !important;
    -webkit-transform: rotate(-129deg);
    transform: rotate(-129deg)
}

.active .circle-clipper.left .circle {
    -webkit-animation: left-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: left-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

.active .circle-clipper.right .circle {
    -webkit-animation: right-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    animation: right-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both
}

@-webkit-keyframes left-spin {
    from {
        -webkit-transform: rotate(130deg)
    }

    50% {
        -webkit-transform: rotate(-5deg)
    }

    to {
        -webkit-transform: rotate(130deg)
    }
}

@keyframes left-spin {
    from {
        -webkit-transform: rotate(130deg);
        transform: rotate(130deg)
    }

    50% {
        -webkit-transform: rotate(-5deg);
        transform: rotate(-5deg)
    }

    to {
        -webkit-transform: rotate(130deg);
        transform: rotate(130deg)
    }
}

@-webkit-keyframes right-spin {
    from {
        -webkit-transform: rotate(-130deg)
    }

    50% {
        -webkit-transform: rotate(5deg)
    }

    to {
        -webkit-transform: rotate(-130deg)
    }
}

@keyframes right-spin {
    from {
        -webkit-transform: rotate(-130deg);
        transform: rotate(-130deg)
    }

    50% {
        -webkit-transform: rotate(5deg);
        transform: rotate(5deg)
    }

    to {
        -webkit-transform: rotate(-130deg);
        transform: rotate(-130deg)
    }
}

#spinnerContainer.cooldown {
    -webkit-animation: container-rotate 1568ms linear infinite,fade-out 400ms cubic-bezier(0.4, 0, 0.2, 1);
    animation: container-rotate 1568ms linear infinite,fade-out 400ms cubic-bezier(0.4, 0, 0.2, 1)
}

@-webkit-keyframes fade-out {
    from {
        opacity: 1
    }

    to {
        opacity: 0
    }
}

@keyframes fade-out {
    from {
        opacity: 1
    }

    to {
        opacity: 0
    }
}

.slider {
    position: relative;
    height: 400px;
    width: 100%
}

.slider.fullscreen {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0
}

.slider.fullscreen ul.slides {
    height: 100%
}

.slider.fullscreen ul.indicators {
    z-index: 2;
    bottom: 30px
}

.slider .slides {
    background-color: #9e9e9e;
    margin: 0;
    height: 400px
}

.slider .slides li {
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
    width: 100%;
    height: inherit;
    overflow: hidden
}

.slider .slides li img {
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: center
}

.slider .slides li .caption {
    color: #fff;
    position: absolute;
    top: 15%;
    left: 15%;
    width: 70%;
    opacity: 0
}

.slider .slides li .caption p {
    color: #e0e0e0
}

.slider .slides li.active {
    z-index: 2
}

.slider .indicators {
    position: absolute;
    text-align: center;
    left: 0;
    right: 0;
    bottom: 0;
    margin: 0
}

.slider .indicators .indicator-item {
    display: inline-block;
    position: relative;
    cursor: pointer;
    height: 16px;
    width: 16px;
    margin: 0 12px;
    background-color: #e0e0e0;
    transition: background-color .3s;
    border-radius: 50%
}

.slider .indicators .indicator-item.active {
    background-color: #4CAF50
}

.carousel {
    overflow: hidden;
    position: relative;
    width: 100%;
    height: 400px;
    -webkit-perspective: 500px;
    perspective: 500px;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    -webkit-transform-origin: 0% 50%;
    transform-origin: 0% 50%
}

.carousel.carousel-slider {
    top: 0;
    left: 0;
    height: 0
}

.carousel.carousel-slider .carousel-fixed-item {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 20px;
    z-index: 1
}

.carousel.carousel-slider .carousel-fixed-item.with-indicators {
    bottom: 68px
}

.carousel.carousel-slider .carousel-item {
    width: 100%;
    height: 100%;
    min-height: 400px;
    position: absolute;
    top: 0;
    left: 0
}

.carousel.carousel-slider .carousel-item h2 {
    font-size: 24px;
    font-weight: 500;
    line-height: 32px
}

.carousel.carousel-slider .carousel-item p {
    font-size: 15px
}

.carousel .carousel-item {
    display: none;
    width: 200px;
    height: 400px;
    position: absolute;
    top: 0;
    left: 0
}

.carousel .carousel-item img {
    width: 100%
}

.carousel .indicators {
    position: absolute;
    text-align: center;
    left: 0;
    right: 0;
    bottom: 0;
    margin: 0
}

.carousel .indicators .indicator-item {
    display: inline-block;
    position: relative;
    cursor: pointer;
    height: 8px;
    width: 8px;
    margin: 24px 4px;
    background-color: rgba(255,255,255,0.5);
    transition: background-color .3s;
    border-radius: 50%
}

.carousel .indicators .indicator-item.active {
    background-color: #fff
}

.picker {
    font-size: 16px;
    text-align: left;
    line-height: 1.2;
    color: #000000;
    position: absolute;
    z-index: 10000;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none
}

.picker__input {
    cursor: default
}

.picker__input.picker__input--active {
    border-color: #0089ec
}

.picker__holder {
    width: 100%;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch
}

/*!
 * Default mobile-first, responsive styling for pickadate.js
 * Demo: https://amsul.github.io/pickadate.js
 */
.picker__holder,.picker__frame {
    bottom: 0;
    left: 0;
    right: 0;
    top: 100%
}

.picker__holder {
    position: fixed;
    transition: background 0.15s ease-out, top 0s 0.15s;
    -webkit-backface-visibility: hidden
}

.picker__frame {
    position: absolute;
    margin: 0 auto;
    min-width: 256px;
    width: 300px;
    max-height: 350px;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    filter: alpha(opacity=0);
    -moz-opacity: 0;
    opacity: 0;
    transition: all 0.15s ease-out
}

@media (min-height: 28.875em) {
    .picker__frame {
        overflow:visible;
        top: auto;
        bottom: -100%;
        max-height: 80%
    }
}

@media (min-height: 40.125em) {
    .picker__frame {
        margin-bottom:7.5%
    }
}

.picker__wrap {
    display: table;
    width: 100%;
    height: 100%
}

@media (min-height: 28.875em) {
    .picker__wrap {
        display:block
    }
}

.picker__box {
    background: #ffffff;
    display: table-cell;
    vertical-align: middle
}

@media (min-height: 28.875em) {
    .picker__box {
        display:block;
        border: 1px solid #777777;
        border-top-color: #898989;
        border-bottom-width: 0;
        border-radius: 5px 5px 0 0;
        box-shadow: 0 12px 36px 16px rgba(0,0,0,0.24)
    }
}

.picker--opened .picker__holder {
    top: 0;
    background: transparent;
    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#1E000000,endColorstr=#1E000000)";
    zoom:1;background: rgba(0,0,0,0.32);
    transition: background 0.15s ease-out
}

.picker--opened .picker__frame {
    top: 0;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    filter: alpha(opacity=100);
    -moz-opacity: 1;
    opacity: 1
}

@media (min-height: 35.875em) {
    .picker--opened .picker__frame {
        top:10%;
        bottom: auto
    }
}

.picker__input.picker__input--active {
    border-color: #E3F2FD
}

.picker__frame {
    margin: 0 auto;
    max-width: 325px
}

@media (min-height: 38.875em) {
    .picker--opened .picker__frame {
        top:10%;
        bottom: auto
    }
}

.picker__box {
    padding: 0 1em
}

.picker__header {
    text-align: center;
    position: relative;
    margin-top: .75em
}

.picker__month,.picker__year {
    display: inline-block;
    margin-left: .25em;
    margin-right: .25em
}

.picker__select--month,.picker__select--year {
    height: 2em;
    padding: 0;
    margin-left: .25em;
    margin-right: .25em
}

.picker__select--month.browser-default {
    display: inline;
    background-color: #FFFFFF;
    width: 40%
}

.picker__select--year.browser-default {
    display: inline;
    background-color: #FFFFFF;
    width: 26%
}

.picker__select--month:focus,.picker__select--year:focus {
    border-color: rgba(0,0,0,0.05)
}

.picker__nav--prev,.picker__nav--next {
    position: absolute;
    padding: .5em 1.25em;
    width: 1em;
    height: 1em;
    box-sizing: content-box;
    top: -0.25em
}

.picker__nav--prev {
    left: -1em;
    padding-right: 1.25em
}

.picker__nav--next {
    right: -1em;
    padding-left: 1.25em
}

.picker__nav--disabled,.picker__nav--disabled:hover,.picker__nav--disabled:before,.picker__nav--disabled:before:hover {
    cursor: default;
    background: none;
    border-right-color: #f5f5f5;
    border-left-color: #f5f5f5
}

.picker__table {
    text-align: center;
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    font-size: 1rem;
    width: 100%;
    margin-top: .75em;
    margin-bottom: .5em
}

.picker__table th,.picker__table td {
    text-align: center
}

.picker__table td {
    margin: 0;
    padding: 0
}

.picker__weekday {
    width: 14.285714286%;
    font-size: .75em;
    padding-bottom: .25em;
    color: #999999;
    font-weight: 500
}

@media (min-height: 33.875em) {
    .picker__weekday {
        padding-bottom:.5em
    }
}

.picker__day--today {
    position: relative;
    color: #595959;
    letter-spacing: -.3;
    padding: .75rem 0;
    font-weight: 400;
    border: 1px solid transparent
}

.picker__day--disabled:before {
    border-top-color: #aaaaaa
}

.picker__day--infocus:hover {
    cursor: pointer;
    color: #000;
    font-weight: 500
}

.picker__day--outfocus {
    display: none;
    padding: .75rem 0;
    color: #fff
}

.picker__day--outfocus:hover {
    cursor: pointer;
    color: #dddddd;
    font-weight: 500
}

.picker__day--highlighted:hover,.picker--focused .picker__day--highlighted {
    cursor: pointer
}

.picker__day--selected,.picker__day--selected:hover,.picker--focused .picker__day--selected {
    border-radius: 50%;
    -webkit-transform: scale(0.75);
    transform: scale(0.75);
    background: #0089ec;
    color: #ffffff
}

.picker__day--disabled,.picker__day--disabled:hover,.picker--focused .picker__day--disabled {
    background: #f5f5f5;
    border-color: #f5f5f5;
    color: #dddddd;
    cursor: default
}

.picker__day--highlighted.picker__day--disabled,.picker__day--highlighted.picker__day--disabled:hover {
    background: #bbbbbb
}

.picker__footer {
    text-align: center;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between
}

.picker__button--today,.picker__button--clear,.picker__button--close {
    border: 1px solid #ffffff;
    background: #ffffff;
    font-size: .8em;
    padding: .66em 0;
    font-weight: bold;
    width: 33%;
    display: inline-block;
    vertical-align: bottom
}

.picker__button--today:hover,.picker__button--clear:hover,.picker__button--close:hover {
    cursor: pointer;
    color: #000000;
    background: #b1dcfb;
    border-bottom-color: #b1dcfb
}

.picker__button--today:focus,.picker__button--clear:focus,.picker__button--close:focus {
    background: #b1dcfb;
    border-color: rgba(0,0,0,0.05);
    outline: none
}

.picker__button--today:before,.picker__button--clear:before,.picker__button--close:before {
    position: relative;
    display: inline-block;
    height: 0
}

.picker__button--today:before,.picker__button--clear:before {
    content: " ";
    margin-right: .45em
}

.picker__button--today:before {
    top: -0.05em;
    width: 0;
    border-top: 0.66em solid #0059bc;
    border-left: .66em solid transparent
}

.picker__button--clear:before {
    top: -0.25em;
    width: .66em;
    border-top: 3px solid #ee2200
}

.picker__button--close:before {
    content: "\D7";
    top: -0.1em;
    vertical-align: top;
    font-size: 1.1em;
    margin-right: .35em;
    color: #777777
}

.picker__button--today[disabled],.picker__button--today[disabled]:hover {
    background: #f5f5f5;
    border-color: #f5f5f5;
    color: #dddddd;
    cursor: default
}

.picker__button--today[disabled]:before {
    border-top-color: #aaaaaa
}

.picker__box {
    border-radius: 2px;
    overflow: hidden
}

.picker__date-display {
    text-align: center;
    background-color: #40c4ff;
    color: #fff;
    padding-bottom: 15px;
    font-weight: 300
}

.picker__nav--prev:hover,.picker__nav--next:hover {
    cursor: pointer;
    color: #000000;
    background: #a1ded8
}

.picker__weekday-display {
    background-color: #1f897f;
    padding: 10px;
    font-weight: 200;
    letter-spacing: .5;
    font-size: 1rem;
    margin-bottom: 15px
}

.picker__month-display {
    text-transform: uppercase;
    font-size: 2rem
}

.picker__day-display {
    font-size: 4.5rem;
    font-weight: 400
}

.picker__year-display {
    font-size: 1.8rem;
    color: rgba(255,255,255,0.4)
}

.picker__box {
    padding: 0
}

.picker__calendar-container {
    padding: 0 1rem
}

.picker__calendar-container thead {
    border: none
}

.picker__table {
    margin-top: 0;
    margin-bottom: .5em
}

.picker__day--infocus {
    color: #595959;
    letter-spacing: -.3;
    padding: .75rem 0;
    font-weight: 400;
    border: 1px solid transparent
}

.picker__day.picker__day--today {
    color: #40c4ff
}

.picker__day.picker__day--today.picker__day--selected {
    color: #fff
}

.picker__weekday {
    font-size: .9rem
}

.picker__day--selected,.picker__day--selected:hover,.picker--focused .picker__day--selected {
    border-radius: 50%;
    -webkit-transform: scale(0.9);
    transform: scale(0.9);
    background-color: #40c4ff;
    color: #ffffff
}

.picker__day--selected.picker__day--outfocus,.picker__day--selected:hover.picker__day--outfocus,.picker--focused .picker__day--selected.picker__day--outfocus {
    background-color: #a1ded8
}

.picker__footer {
    text-align: right;
    padding: 5px 10px
}

.picker__close,.picker__today {
    font-size: 1.1rem;
    padding: 0 1rem;
    color: #40c4ff
}

.picker__nav--prev:before,.picker__nav--next:before {
    content: " ";
    border-top: .5em solid transparent;
    border-bottom: .5em solid transparent;
    border-right: 0.75em solid #676767;
    width: 0;
    height: 0;
    display: block;
    margin: 0 auto
}

.picker__nav--next:before {
    border-right: 0;
    border-left: 0.75em solid #676767
}

button.picker__today:focus,button.picker__clear:focus,button.picker__close:focus {
    background-color: #a1ded8
}

.picker__list {
    list-style: none;
    padding: 0.75em 0 4.2em;
    margin: 0
}

.picker__list-item {
    border-bottom: 1px solid #dddddd;
    border-top: 1px solid #dddddd;
    margin-bottom: -1px;
    position: relative;
    background: #ffffff;
    padding: .75em 1.25em
}

@media (min-height: 46.75em) {
    .picker__list-item {
        padding:.5em 1em
    }
}

.picker__list-item:hover {
    cursor: pointer;
    color: #000000;
    background: #b1dcfb;
    border-color: #0089ec;
    z-index: 10
}

.picker__list-item--highlighted {
    border-color: #0089ec;
    z-index: 10
}

.picker__list-item--highlighted:hover,.picker--focused .picker__list-item--highlighted {
    cursor: pointer;
    color: #000000;
    background: #b1dcfb
}

.picker__list-item--selected,.picker__list-item--selected:hover,.picker--focused .picker__list-item--selected {
    background: #0089ec;
    color: #ffffff;
    z-index: 10
}

.picker__list-item--disabled,.picker__list-item--disabled:hover,.picker--focused .picker__list-item--disabled {
    background: #f5f5f5;
    border-color: #f5f5f5;
    color: #dddddd;
    cursor: default;
    border-color: #dddddd;
    z-index: auto
}

.picker--time .picker__button--clear {
    display: block;
    width: 80%;
    margin: 1em auto 0;
    padding: 1em 1.25em;
    background: none;
    border: 0;
    font-weight: 500;
    font-size: .67em;
    text-align: center;
    text-transform: uppercase;
    color: #666
}

.picker--time .picker__button--clear:hover,.picker--time .picker__button--clear:focus {
    color: #000000;
    background: #b1dcfb;
    background: #ee2200;
    border-color: #ee2200;
    cursor: pointer;
    color: #ffffff;
    outline: none
}

.picker--time .picker__button--clear:before {
    top: -0.25em;
    color: #666;
    font-size: 1.25em;
    font-weight: bold
}

.picker--time .picker__button--clear:hover:before,.picker--time .picker__button--clear:focus:before {
    color: #ffffff
}

.picker--time .picker__frame {
    min-width: 256px;
    max-width: 320px
}

.picker--time .picker__box {
    font-size: 1em;
    background: #f2f2f2;
    padding: 0
}

@media (min-height: 40.125em) {
    .picker--time .picker__box {
        margin-bottom:5em
    }
}



.select2-search input {
  margin:0 0 -.25rem 0 !important;
  background:#F5F5F5;
}

.select2-container .select2-choice {
  background-color: rgba(255,255,255,0.9);
    width: 100% !important;
    padding: 5px !important;
    border: 1px solid #f2f2f2 !important;
    border-radius: 2px !important;
    height: 3rem !important;
}

html,
body {
  font-family: 'Roboto', sans-serif;
  background: #E0E0E0;
  color: rgba(0, 0, 0, 0.87);
}
.nv-div {
  height: 500px;
  min-width: 100%;
  margin-left: auto;
  margin-right: auto
}

svg {
  display: block;
}
.no-pad {
  padding:0 !important;
}

html,
body,
svg {
  margin: 0px;
  padding: 0px;
  height: 100%;
  width: 100%;
  overflow:auto !important;
}

.dropdown-content {
  overflow: visible !important;
  background-color: #e5e5e5 !important;
  margin-top: -4px !important;
}

.dropdown-content.sub-menu {
  margin-top: -0.3rem;
}

.content {
  margin-top: 1rem;
  height: 100%;
  min-height: 860px;
  overflow: auto;
}

.navbar-fixed,
nav {
  color: #3f51b5 !important;
  background-color: #F5F5F5 !important;
  background: transparent;
  box-shadow: 0px 0px 1px 1px rgba(0, 0, 0, 0.14), 0px 0px 2px 2px rgba(0, 0, 0, 0.098), 0px 0px 5px 1px rgba(0, 0, 0, 0.084) !important;
  z-index: 999 !important;
}

nav a.button-collapse > i {
  font-size: 32px;
}

.side-nav a {
  margin: 0 !important;
  padding-bottom: 0 !important;
}

.side-nav a:hover {
  background-color: rgba(0, 0, 0, 0.23) !important;
}

.button-collapse {
  width: 64px;
  text-align: -webkit-center;
}

a.button-collapse:hover {
  background: rgba(0, 0, 0, 0.13);
  border-radius: 50%;
}

.side-nav .top-bg {
  background-image: url(https://pathli.com/app/assets/build/images/todo_icon.png);
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  height: 140px;
  width: 100%;
  margin-top: 1rem;
  margin-bottom: 1.2rem;
  background-color: rgba(0, 0, 0, 0);
}

.side-nav.sub-side-nav {
  z-index: 999 !important;
  top: 62px !important;
  background-color: #F5F5F5 !important;
}

.side-nav.sub-side-nav > li:nth-child(2) {
  margin-top: .5rem !important;
}

.side-nav .divider {
  margin: 0 !important;
}

.side-nav .subheader {
  cursor: initial;
  pointer-events: none;
  color: rgba(0, 0, 0, 0.54);
  font-size: 1rem !Important;
  font-weight: 500;
  line-height: 32px !important;
  padding: 0.5rem 1rem !important;
  height: 32px !important;
  margin-bottom: .5rem !important;
}

.drag-target {
  z-index: 997 !important;
}

.dark-1 {
  background-color: #000000;
}

.dark-2 {
  background-color: #212121;
}

.dark-3 {
  background-color: #303030;
}

.dark-4 {
  background-color: #424242;
}

.light-1 {
  background-color: #E0E0E0;
}

.light-2 {
  background-color: #F5F5F5;
}

.light-3 {
  background-color: #FAFAFA;
}

.light-4 {
  background-color: #FFFFFF;
}

.primary-500 {
  background-color: #cddc39;
}

.primary-50 {
  background-color: #f9fbe7;
}

.primary-100 {
  background-color: #f0f4c3;
}

.primary-700 {
  background-color: #afb42b;
}

.accent-50 {
  background-color: #e0f7fa;
}

.accent-A100 {
  background-color: #84ffff;
}

.accent-A200 {
  background-color: #18ffff;
}

.accent-A400 {
  background-color: #00e5ff;
}

.warn-500 {
  background-color: #ff5722;
}

.warn-100 {
  background-color: #ffccbc;
}

.warn-700 {
  background-color: #e64a19;
}

.material-icons.md-18 {
  font-size: 18px;
}

.material-icons.md-24 {
  font-size: 24px;
}

.material-icons.md-36 {
  font-size: 36px;
}

.material-icons.md-48 {
  font-size: 48px;
}

::-webkit-scrollbar {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-button {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-thumb {
  background: rgba(0, 0, 0, 0.54);
  border: 3px none rgba(0, 0, 0, 0.54);
  border-radius: 1px;
}

::-webkit-scrollbar-thumb:hover {
  background: rgba(0, 0, 0, 0.64);
}

::-webkit-scrollbar-thumb:active {
  background: rgba(0, 0, 0, 0.54);
}

::-webkit-scrollbar-track {
  background: #fff;
  border: 2px none #fff;
  border-radius: 1px;
}

::-webkit-scrollbar-track:hover {
  background: #fff;
}

::-webkit-scrollbar-track:active {
  background: #fff;
}

::-webkit-scrollbar-corner {
  background: transparent;
}

.material-icons.md-18 {
  font-size: 18px;
}

.material-icons.md-24 {
  font-size: 24px;
}

.material-icons.md-36 {
  font-size: 36px;
}

.material-icons.md-48 {
  font-size: 48px;
}


/* Rules for using icons as black on a light background. */

.material-icons.md-dark {
  color: rgba(0, 0, 0, 0.54);
}

.material-icons.md-dark.md-inactive {
  color: rgba(0, 0, 0, 0.26);
}


/* Rules for using icons as white on a dark background. */

.material-icons.md-light {
  color: rgba(255, 255, 255, 1);
}

.material-icons.md-light.md-inactive {
  color: rgba(255, 255, 255, 0.3);
}

.rotate{
    -moz-transition: all .25s linear;
    -webkit-transition: all .25s linear;
    transition: all .25s linear;
}#tbl-head-chk {
  display: none;
}


#tbl-head-chk:checked + .square {
  transform: rotate(180deg);
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}

html,body{
  background-color:#E0E0E0
}

.highlight-rows {
  background-color:#F5F5F5
}

.waves-effect.waves-blue .waves-ripple {
     /* The alpha value allows the text and background color
     of the button to still show through. */
      background-color: rgba(3, 169, 244,0.65);
    }
a.table-nav {
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 36px;
    line-height: 36px;
    outline: 0;
    padding: 0 1.3rem !important;
    text-transform: uppercase;
    vertical-align: middle;
    margin-left: 5px !important;
    -webkit-tap-highlight-color: transparent;
}
.table-nav {
    margin-top: 15px;
    color: rgba(0, 0, 0, 0.53);
    background-color: transparent !important;
    box-shadow: none !important;
    padding: none !important;
}

.tbl_card_header{
  min-height:75px;
  z-index:5000;
  margin-bottom:1px;
  background-color:#40c4ff;
  color:rgba(255,255,255,0.87) !important;
  
}
.tbl_head_swap {
  color:rgba(0,0,0,0.87);
  font-size:20px;
  font-family:Roboto Regular;
  z-index:500;
  padding:1px 5px;
  vertical-align:middle;
  height:100%;
  display:inline-block;
  position:relative;
  width:100%;
  margin-bottom:-25px;
}

.tbl_head_swap > i{
  float:right;
  color:rgba(0,0,0,0.54);
  font-size:24px;
}

th > label {
  margin-left:25%;
  color:transparent
}
.card-panel table{
  padding:1px !important;
}

thead{
  
  margin-top:5px;
  border-bottom:2px solid rgba(0,0,0,0.1) !important;
}
tbody{
  position:relative;
  display:table-flex;
  z-index:2;
  background-color:#F5F5F5
}
  

@media only screen and (max-width: 992px){
  .tbl_card_header{
    border-bottom:none !important;
  }
table.responsive-table thead {
  
    height:100%;
    top: 0;
    position: inherit;
    display: inline-table;
    margin-top: -1.45rem;
  overflow:hidden;
  z-index:1;
  border-right:1px solid transparent !important;
  padding-right:3%;text-align:center !important;
}
  input[type="checkbox"], input[type="radio"],[type="checkbox"]+label{
    padding-left:30px !important;height:25px !important; text-align:end !important;vertical-align:bottom !important;
  }
}


/*Utils*/

::-webkit-scrollbar {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-button {
  width: 3px;
  height: 2px;
}

::-webkit-scrollbar-thumb {
  background: rgba(0, 0, 0, 0.54);
  border: 3px none rgba(0, 0, 0, 0.54);
  border-radius: 1px;
}

::-webkit-scrollbar-thumb:hover {
  background: rgba(0, 0, 0, 0.64);
}

::-webkit-scrollbar-thumb:active {
  background: rgba(0, 0, 0, 0.54);
}

::-webkit-scrollbar-track {
  background: #fff;
  border: 2px none #fff;
  border-radius: 1px;
}

::-webkit-scrollbar-track:hover {
  background: #fff;
}

::-webkit-scrollbar-track:active {
  background: #fff;
}

::-webkit-scrollbar-corner {
  background: transparent;
}











.table-card {
   transition: box-shadow .25s;
  padding: 20px;
  margin: 0.5rem 0 1rem 0;
  border-radius: 2px;
  background-color: #fff;
}</style>
<body>
  <header>
    <!-- SIDENAV -->
    <ul id="slide-out" class="side-nav sub-side-nav z-depth-3">
      <li>
        <div class="top-bg"></div>
      </li>
      <li>
        <a href="#!" class="waves-effect">
          <i class="material-icons">cloud</i>
          First Link With Icon
        </a>
      </li>
      <li>
        <a href="#!" class="waves-effect">
          <i class="material-icons">email</i> 
          Second Link
        </a>
      </li>
      <li>
        <div class="divider"></div>
      </li>
      <li>
        <a class="subheader">Subheader</a>
      </li>
      <li>
        <a class="waves-effect" href="#!">
          Third Link With Waves
        </a>
      </li>
    </ul>

    <!-- NAV DROPDOWN -->
    <ul id="dropdown1" class="dropdown-content">
      <li><a href="#!">one</a></li>
      <li><a href="#!">two</a></li>
      <li class="divider"></li>
      <li><a href="#!">three</a></li>
      <li>
        <a class='dropdown-button2 d' 
           href='#!' 
           data-activates='dropdown2' 
           >
          Drop Me!
        </a>
    </li>
    </ul>
    
    <!-- Nav sub-menu -->
    <ul id='dropdown2'
        class='dropdown-content sub-menu z-depth-2'>
        <li><a href="#!">one</a></li>
        <li><a href="#!">two</a></li>
        <li class="divider"></li>
        <li><a href="#!">three</a></li>
    </ul>

    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper">
          <!-- sidenav btn -->
          <a href="#" data-activates="slide-out" 
             class="button-collapse show-on-large waves-effect waves-dark">
            <i class="material-icons">menu</i>
          </a>
          <ul class="right">
            <li>
              <a href="sass.html">Sass</a>
            </li>
            <li>
              <a href="badges.html">Components</a>
            </li>
            <!-- Dropdown Trigger -->
            <li>
              <a class="dropdown-button" 
                 href="#!" 
                 data-activates="dropdown1"
                 data-beloworigin="true"
                 data-gutter="3">
                Dropdown
                <i class="material-icons right">
                  arrow_drop_down</i>
              </a>
            </li>
            <li><a href="#">las</a></li>
          </ul>
        </div>
      </nav>
    </div>
  </header>
  
  <div class="content">
    <div class="row">
      <div class="col s12">
        <div class="card no-pad" style="height:100vh;overflow:auto;">
          <div class="card-content no-pad" >
            <div class="row">
            <div class="input-field col s3">
              <div id="searchName"></div>
            </div>
          </div>
            <div class="row" style="overflow:auto">
            <div class="col s12 no-pad" style="overflow:auto;text-align:center;">
              <div id="tree_chart" style="overflow:auto"></div>
            </div>
          </div>
          </div>
        </div>
      </div>
      <div class="col s6 l8 m8">
        <div class="card no-pad">
          <div class='card-content'>
            <div id="chart" class='with-3d-shadow with-transitions nv-div'>
              <svg height="auto"></svg>
            </div>
        </div>
      </div>
    </div>
        <div class="col s6 l4 m4">
    <div class="card-panel" style="padding:1px !important;height:600px !important;overflow:scroll">
      <div class="tbl_card_header">
        <div class="tbl_head_swap">
          <div class="col s3">
            <p>
              Table
            </p>
          </div>
          <div class="col s9">
            <a class="waves-effect waves-light btn-flat right table-nav">
              <i class="material-icons" style="font-size:24px;">filter_list</i>
            </a>
            <a class="waves-effect waves-light btn-flat right table-nav">
              <i class="material-icons" style="font-size:24px;">person_add</i>
            </a>
          </div>
        </div>
        <div style="display:none;background-color:#E3F2FD;" class="tbl_head_swap">
          <div class="col s3">
            <p class="table-title light-blue-text">hi
              <p>
          </div>
          <div class="col s9">
            <a class="btn-flat right table-nav waves-effect waves-dark">
              <i class="material-icons md-24">more_vert</i>
            </a>
            <a class="btn-flat right table-nav waves-effect waves-dark">
              <i class="material-icons md-24">delete</i>
            </a>
          </div>

        </div>
      </div>
      <div style="overflow:scroll">
        <table class="bordered highlight responsive-table">
          <thead>
            <tr style="z-index:499;">
              <th>
                <input type="checkbox" id="all_col" />
                <label for="all_col" style="padding-top:1%;"></label>

              </th>
              <th> 
               mpg

              </th>
              <th> cyl </th>
              <th> disp </th>
              <th> hp </th>
              <th> drat </th>
              <th> wt </th>
              <th>qsec </th>
              <th>vs </th>
              <th>am </th>
              <th> gear </th>
              <th> carb </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="text-align:center;">
                <input type="checkbox" id="mazda_rx4" /><label for="mazda_rx4" style="margin-right:px;"></label> </td>
              <td style="text-align:center;"> 21.0 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 160.0 </td>
              <td style="text-align:center;"> 110 </td>
              <td style="text-align:center;"> 3.90 </td>
              <td style="text-align:center;"> 2.620 </td>
              <td style="text-align:center;"> 16.46 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;">
                <input type="checkbox" id="mazda_rx4_wag" />
                <label for="mazda_rx4_wag"></label>
              </td>
              <td style="text-align:center;"> 21.0 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 160.0 </td>
              <td style="text-align:center;"> 110 </td>
              <td style="text-align:center;"> 3.90 </td>
              <td style="text-align:center;"> 2.875 </td>
              <td style="text-align:center;"> 17.02 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="datsun_710" /><label for="datsun_710"></label> </td>
              <td style="text-align:center;"> 22.8 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 108.0 </td>
              <td style="text-align:center;"> 93 </td>
              <td style="text-align:center;"> 3.85 </td>
              <td style="text-align:center;"> 2.320 </td>
              <td style="text-align:center;"> 18.61 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="hornet_4_drive" /><label for="hornet_4_drive"></label> </td>
              <td style="text-align:center;"> 21.4 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 258.0 </td>
              <td style="text-align:center;"> 110 </td>
              <td style="text-align:center;"> 3.08 </td>
              <td style="text-align:center;"> 3.215 </td>
              <td style="text-align:center;"> 19.44 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="hornet_sportabout" /><label for="hornet_sportabout"></label> </td>
              <td style="text-align:center;"> 18.7 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 360.0 </td>
              <td style="text-align:center;"> 175 </td>
              <td style="text-align:center;"> 3.15 </td>
              <td style="text-align:center;"> 3.440 </td>
              <td style="text-align:center;"> 17.02 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="valiant" /><label for="valiant"></label> </td>
              <td style="text-align:center;"> 18.1 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 225.0 </td>
              <td style="text-align:center;"> 105 </td>
              <td style="text-align:center;"> 2.76 </td>
              <td style="text-align:center;"> 3.460 </td>
              <td style="text-align:center;"> 20.22 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="duster_360" /><label for="duster_360"></label> </td>
              <td style="text-align:center;"> 14.3 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 360.0 </td>
              <td style="text-align:center;"> 245 </td>
              <td style="text-align:center;"> 3.21 </td>
              <td style="text-align:center;"> 3.570 </td>
              <td style="text-align:center;"> 15.84 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_240d" /><label for="merc_240d"></label> </td>
              <td style="text-align:center;"> 24.4 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 146.7 </td>
              <td style="text-align:center;"> 62 </td>
              <td style="text-align:center;"> 3.69 </td>
              <td style="text-align:center;"> 3.190 </td>
              <td style="text-align:center;"> 20.00 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_230" /><label for="merc_230"></label> </td>
              <td style="text-align:center;"> 22.8 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 140.8 </td>
              <td style="text-align:center;"> 95 </td>
              <td style="text-align:center;"> 3.92 </td>
              <td style="text-align:center;"> 3.150 </td>
              <td style="text-align:center;"> 22.90 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_280" /><label for="merc_280"></label> </td>
              <td style="text-align:center;"> 19.2 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 167.6 </td>
              <td style="text-align:center;"> 123 </td>
              <td style="text-align:center;"> 3.92 </td>
              <td style="text-align:center;"> 3.440 </td>
              <td style="text-align:center;"> 18.30 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_280c" /><label for="merc_280c"></label> </td>
              <td style="text-align:center;"> 17.8 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 167.6 </td>
              <td style="text-align:center;"> 123 </td>
              <td style="text-align:center;"> 3.92 </td>
              <td style="text-align:center;"> 3.440 </td>
              <td style="text-align:center;"> 18.90 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_450se" /><label for="merc_450se"></label> </td>
              <td style="text-align:center;"> 16.4 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 275.8 </td>
              <td style="text-align:center;"> 180 </td>
              <td style="text-align:center;"> 3.07 </td>
              <td style="text-align:center;"> 4.070 </td>
              <td style="text-align:center;"> 17.40 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 3 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_450sl" /><label for="merc_450sl"></label> </td>
              <td style="text-align:center;"> 17.3 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 275.8 </td>
              <td style="text-align:center;"> 180 </td>
              <td style="text-align:center;"> 3.07 </td>
              <td style="text-align:center;"> 3.730 </td>
              <td style="text-align:center;"> 17.60 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 3 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="merc_450slc" /><label for="merc_450slc"></label> </td>
              <td style="text-align:center;"> 15.2 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 275.8 </td>
              <td style="text-align:center;"> 180 </td>
              <td style="text-align:center;"> 3.07 </td>
              <td style="text-align:center;"> 3.780 </td>
              <td style="text-align:center;"> 18.00 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 3 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="cadillac_fleetwood" /><label for="cadillac_fleetwood"></label> </td>
              <td style="text-align:center;"> 10.4 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 472.0 </td>
              <td style="text-align:center;"> 205 </td>
              <td style="text-align:center;"> 2.93 </td>
              <td style="text-align:center;"> 5.250 </td>
              <td style="text-align:center;"> 17.98 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="lincoln_continental" /><label for="lincoln_continental"></label> </td>
              <td style="text-align:center;"> 10.4 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 460.0 </td>
              <td style="text-align:center;"> 215 </td>
              <td style="text-align:center;"> 3.00 </td>
              <td style="text-align:center;"> 5.424 </td>
              <td style="text-align:center;"> 17.82 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="chrysler_imperial" /><label for="chrysler_imperial"></label> </td>
              <td style="text-align:center;"> 14.7 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 440.0 </td>
              <td style="text-align:center;"> 230 </td>
              <td style="text-align:center;"> 3.23 </td>
              <td style="text-align:center;"> 5.345 </td>
              <td style="text-align:center;"> 17.42 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="fiat_128" /><label for="fiat_128"></label> </td>
              <td style="text-align:center;"> 32.4 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 78.7 </td>
              <td style="text-align:center;"> 66 </td>
              <td style="text-align:center;"> 4.08 </td>
              <td style="text-align:center;"> 2.200 </td>
              <td style="text-align:center;"> 19.47 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="honda_civic" /><label for="honda_civic"></label> </td>
              <td style="text-align:center;"> 30.4 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 75.7 </td>
              <td style="text-align:center;"> 52 </td>
              <td style="text-align:center;"> 4.93 </td>
              <td style="text-align:center;"> 1.615 </td>
              <td style="text-align:center;"> 18.52 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="toyota_corolla" /><label for="toyota_corolla"></label> </td>
              <td style="text-align:center;"> 33.9 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 71.1 </td>
              <td style="text-align:center;"> 65 </td>
              <td style="text-align:center;"> 4.22 </td>
              <td style="text-align:center;"> 1.835 </td>
              <td style="text-align:center;"> 19.90 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="toyota_corona" /><label for="toyota_corona"></label> </td>
              <td style="text-align:center;"> 21.5 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 120.1 </td>
              <td style="text-align:center;"> 97 </td>
              <td style="text-align:center;"> 3.70 </td>
              <td style="text-align:center;"> 2.465 </td>
              <td style="text-align:center;"> 20.01 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="dodge_challenger" /><label for="dodge_challenger"></label> </td>
              <td style="text-align:center;"> 15.5 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 318.0 </td>
              <td style="text-align:center;"> 150 </td>
              <td style="text-align:center;"> 2.76 </td>
              <td style="text-align:center;"> 3.520 </td>
              <td style="text-align:center;"> 16.87 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="amc_javelin" /><label for="amc_javelin"></label> </td>
              <td style="text-align:center;"> 15.2 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 304.0 </td>
              <td style="text-align:center;"> 150 </td>
              <td style="text-align:center;"> 3.15 </td>
              <td style="text-align:center;"> 3.435 </td>
              <td style="text-align:center;"> 17.30 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="camaro_z28" /><label for="camaro_z28"></label> </td>
              <td style="text-align:center;"> 13.3 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 350.0 </td>
              <td style="text-align:center;"> 245 </td>
              <td style="text-align:center;"> 3.73 </td>
              <td style="text-align:center;"> 3.840 </td>
              <td style="text-align:center;"> 15.41 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="pontiac_firebird" /><label for="pontiac_firebird"></label> </td>
              <td style="text-align:center;"> 19.2 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 400.0 </td>
              <td style="text-align:center;"> 175 </td>
              <td style="text-align:center;"> 3.08 </td>
              <td style="text-align:center;"> 3.845 </td>
              <td style="text-align:center;"> 17.05 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 3 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="fiat_x1-9" /><label for="fiat_x1-9"></label> </td>
              <td style="text-align:center;"> 27.3 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 79.0 </td>
              <td style="text-align:center;"> 66 </td>
              <td style="text-align:center;"> 4.08 </td>
              <td style="text-align:center;"> 1.935 </td>
              <td style="text-align:center;"> 18.90 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 1 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="porsche_914-2" /><label for="porsche_914-2"></label> </td>
              <td style="text-align:center;"> 26.0 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 120.3 </td>
              <td style="text-align:center;"> 91 </td>
              <td style="text-align:center;"> 4.43 </td>
              <td style="text-align:center;"> 2.140 </td>
              <td style="text-align:center;"> 16.70 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 5 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="lotus_europa" /><label for="lotus_europa"></label> </td>
              <td style="text-align:center;"> 30.4 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 95.1 </td>
              <td style="text-align:center;"> 113 </td>
              <td style="text-align:center;"> 3.77 </td>
              <td style="text-align:center;"> 1.513 </td>
              <td style="text-align:center;"> 16.90 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 5 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="ford_pantera_l" /><label for="ford_pantera_l"></label> </td>
              <td style="text-align:center;"> 15.8 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 351.0 </td>
              <td style="text-align:center;"> 264 </td>
              <td style="text-align:center;"> 4.22 </td>
              <td style="text-align:center;"> 3.170 </td>
              <td style="text-align:center;"> 14.50 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 5 </td>
              <td style="text-align:center;"> 4 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="ferrari_dino" /><label for="ferrari_dino"></label> </td>
              <td style="text-align:center;"> 19.7 </td>
              <td style="text-align:center;"> 6 </td>
              <td style="text-align:center;"> 145.0 </td>
              <td style="text-align:center;"> 175 </td>
              <td style="text-align:center;"> 3.62 </td>
              <td style="text-align:center;"> 2.770 </td>
              <td style="text-align:center;"> 15.50 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 5 </td>
              <td style="text-align:center;"> 6 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="maserati_bora" /><label for="maserati_bora"></label> </td>
              <td style="text-align:center;"> 15.0 </td>
              <td style="text-align:center;"> 8 </td>
              <td style="text-align:center;"> 301.0 </td>
              <td style="text-align:center;"> 335 </td>
              <td style="text-align:center;"> 3.54 </td>
              <td style="text-align:center;"> 3.570 </td>
              <td style="text-align:center;"> 14.60 </td>
              <td style="text-align:center;"> 0 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 5 </td>
              <td style="text-align:center;"> 8 </td>
            </tr>
            <tr>
              <td style="text-align:center;"> <input type="checkbox" id="volvo_142e" /><label for="volvo_142e"></label> </td>
              <td style="text-align:center;"> 21.4 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 121.0 </td>
              <td style="text-align:center;"> 109 </td>
              <td style="text-align:center;"> 4.11 </td>
              <td style="text-align:center;"> 2.780 </td>
              <td style="text-align:center;"> 18.60 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 1 </td>
              <td style="text-align:center;"> 4 </td>
              <td style="text-align:center;"> 2 </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
    </div>
  </div>
</body>
<script>
$(document).ready(function() {
  $('.rt-select').material_select();

  //===============================================
  function select2DataCollectName(d) {
    if (d.children)
      d.children.forEach(select2DataCollectName);
    else if (d._children)
      d._children.forEach(select2DataCollectName);
    select2Data.push(d.name);
  }

  //===============================================
  function searchTree(d) {
    if (d.children)
      d.children.forEach(searchTree);
    else if (d._children)
      d._children.forEach(searchTree);
    var searchFieldValue = eval(searchField);
    if (searchFieldValue && searchFieldValue.match(searchText)) {
      // Walk parent chain
      var ancestors = [];
      var parent = d;
      while (typeof(parent) !== "undefined") {
        ancestors.push(parent);
        //console.log(parent);
        parent.class = "found";
        parent = parent.parent;
      }
      //console.log(ancestors);
    }
  }

  //===============================================
  function clearAll(d) {
    d.class = "";
    if (d.children)
      d.children.forEach(clearAll);
    else if (d._children)
      d._children.forEach(clearAll);
  }

  //===============================================
  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }

  //===============================================
  function collapseAllNotFound(d) {
    if (d.children) {
      if (d.class !== "found") {
        d._children = d.children;
        d._children.forEach(collapseAllNotFound);
        d.children = null;
      } else
        d.children.forEach(collapseAllNotFound);
    }
  }

  //===============================================
  function expandAll(d) {
    if (d._children) {
      d.children = d._children;
      d.children.forEach(expandAll);
      d._children = null;
    } else if (d.children)
      d.children.forEach(expandAll);
  }

  //===============================================
  // Toggle children on click.
  function toggle(d) {
    if (d.children) {
      d._children = d.children;
      d.children = null;
    } else {
      d.children = d._children;
      d._children = null;
    }
    clearAll(root);
    update(d);
    $("#searchName").select2("val", "");
  }

  //===============================================
  $("#searchName").on("select2-selecting", function(e) {
    clearAll(root);
    expandAll(root);
    update(root);

    searchField = "d.name";
    searchText = e.object.text;
    searchTree(root);
    root.children.forEach(collapseAllNotFound);
    update(root);
  })

  //===============================================
  var margin = {
      top: 50,
      right: 10,
      bottom:20,
      left: 60
    },
    width = 1200 - margin.right - margin.left,
    height = 990 - margin.top - margin.bottom;

  var i = 0,
    duration = 750,
    root;

  var tree = d3.layout.tree()
    .size([height, width]);

  var diagonal = d3.svg.diagonal()
    .projection(function(d) {
      return [d.y, d.x];
    });

  var svg = d3.select("#tree_chart").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  d3.json("flare.json", function(error, flare) {
    function getData() {
      return {
        "name": "flare",
        "children": [{
          "name": "analytics",
          "icon": "",
          "children": [{
            "name": "cluster",
            "children": [{
              "name": "AgglomerativeCluster",
              "size": 3938
            }, {
              "name": "CommunityStructure",
              "size": 3812
            }, {
              "name": "MergeEdge",
              "size": 743
            }]
          }, {
            "name": "graph",
            "children": [{
              "name": "BetweennessCentrality",
              "size": 3534
            }, {
              "name": "LinkDistance",
              "size": 5731
            }]
          }, {
            "name": "optimization",
            "children": [{
              "name": "AspectRatioBanker",
              "size": 7074
            }]
          }]
        }, {
          "name": "animate",
          "children": [{
            "name": "Easing",
            "size": 17010
          }, {
            "name": "FunctionSequence",
            "size": 5842
          }, {
            "name": "interpolate",
            "children": [{
              "name": "ArrayInterpolator",
              "size": 1983
            }, {
              "name": "ColorInterpolator",
              "size": 2047
            }, {
              "name": "DateInterpolator",
              "size": 1375
            }, {
              "name": "Interpolator",
              "size": 8746
            }, {
              "name": "MatrixInterpolator",
              "size": 2202
            }, {
              "name": "NumberInterpolator",
              "size": 1382
            }, {
              "name": "ObjectInterpolator",
              "size": 1629
            }, {
              "name": "PointInterpolator",
              "size": 1675
            }, {
              "name": "RectangleInterpolator",
              "size": 2042
            }]
          }, {
            "name": "ISchedulable",
            "size": 1041
          }, {
            "name": "Parallel",
            "size": 5176
          }, {
            "name": "Pause",
            "size": 449
          }, {
            "name": "Scheduler",
            "size": 5593
          }, {
            "name": "Sequence",
            "size": 5534
          }, {
            "name": "Transition",
            "size": 9201
          }, {
            "name": "Transitioner",
            "size": 19975
          }, {
            "name": "TransitionEvent",
            "size": 1116
          }, {
            "name": "Tween",
            "size": 6006
          }]
        }]
      };
    }
    var myData = getData();
    root = myData;
    root.x0 = height / 2;
    root.y0 = 0;

    select2Data = [];
    select2DataCollectName(root);
    select2DataObject = [];
    select2Data.sort(function(a, b) {
        if (a > b) return 1; // sort
        if (a < b) return -1;
        return 0;
      })
      .filter(function(item, i, ar) {
        return ar.indexOf(item) === i;
      }) // remove duplicate items
      .filter(function(item, i, ar) {
        select2DataObject.push({
          "id": i,
          "text": item
        });
      });
    select2Data.sort(function(a, b) {
        if (a > b) return 1; // sort
        if (a < b) return -1;
        return 0;
      })
      .filter(function(item, i, ar) {
        return ar.indexOf(item) === i;
      }) // remove duplicate items
      .filter(function(item, i, ar) {
        select2DataObject.push({
          "id": i,
          "text": item
        });
      });
    $("#searchName").select2({
      data: select2DataObject,
      containerCssClass: "search",
      placeholder: "Search for a node..."
    });

    function collapse(d) {
      if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
      }
    }

    root.children.forEach(collapse);
    update(root);
  });

  d3.select(self.frameElement).style("height", "1100px").style("width", "1800px").style('margin', '0px');

  function update(source) {

    // Compute the new tree layout.
    var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach(function(d) {
      d.y = d.depth * 180;
    });

    // Update the nodes…
    var node = svg.selectAll("g.node")
      .data(nodes, function(d) {
        return d.id || (d.id = ++i);
      });

    // Enter any new nodes at the parent's previous position.
    var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) {
        return "translate(" + source.y0 + "," + source.x0 + ")";
      })
      .on("click", toggle);

    // Append Images
    nodeEnter.append("svg:image")
      .attr("xlink:href", function(d) {
        return d.icon;
      })
      .attr("x", function(d) {
        return -15;
      })
      .attr("y", function(d) {
        return -12;
      })
      .attr("height", 25)
      .attr("width", 25);
    
    nodeEnter.append("circle")
      .attr("r", 1e-6)
      .style("fill", function(d) {
        return d._children ? "transparent" : "#fff";
      });

    nodeEnter.append("text")
      .attr("x", function(d) {
        return d.children || d._children ? -10 : 10;
      })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) {
        return d.children || d._children ? "end" : "start";
      })
      .text(function(d) {
        return d.name;
      })
      .style("fill-opacity", 1e-6);

    // Transition nodes to their new position.
    var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) {
        return "translate(" + d.y + "," + d.x + ")";
      });

    nodeUpdate.select("circle")
      .attr("r", 4.5)
      .style("fill", function(d) {
        if (d.class === "found") {
          return "#4cff00"; //lime-green
        } else if (d._children) {
          return "orange";
        } else {
          return "#E91E63";
        }
      })
      .style("stroke", function(d) {
        if (d.class === "found") {
          return "#4cff00"; //lime-green
        } else {
          return "rgba(255, 255, 255, 0.03)"
        }
      })
      .style("stroke-width", function(d) {
        if (d.class === "found") {
          return "1px"; //lime-green
        } else {
          return "1px"
        }
      });

    nodeUpdate.select("text")
      .style("fill-opacity", 1);

    // Transition exiting nodes to the parent's new position.
    var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) {
        return "translate(" + source.y + "," + source.x + ")";
      })
      .remove();

    nodeExit.select("circle")
      .attr("r", 1e-6);

    nodeExit.select("text")
      .style("fill-opacity", 1e-6);

    // Update the links…
    var link = svg.selectAll("path.link")
      .data(links, function(d) {
        return d.target.id;
      });

    // Enter any new links at the parent's previous position.
    link.enter().insert("path", "g")
      .attr("class", "link")
      .style("stroke", function(d) {
        return d.target.level;
      }) // Handle the appending of level color from data to link.
      .attr("d", function(d) {
        var o = {
          x: source.x0,
          y: source.y0
        };
        return diagonal({
          source: o,
          target: o
        });
      });

    // Transition links to their new position.
    link.transition()
      .duration(duration)
      .attr("d", diagonal)
      .style("stroke", function(d) {
        if (d.target.class === "found") {
          return "#4cff00";
        }
      })
      .style("stroke-opacity", function(d) {
        if (d.target.class === "found") {
          return 0.27;
        }
      })
      .style("stroke-width", function(d) {
        if (d.target.class === "found") {
          return ".25rem";
        }
      })
      .style("fill-opacity", function(d) {
        if (d.target.class === "found") {
          return 0.57;
        }
      });

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {
          x: source.x,
          y: source.y
        };
        return diagonal({
          source: o,
          target: o
        });
      })
      .remove();

    // Stash the old positions for transition.
    nodes.forEach(function(d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });

  };
  
  
  
    var expandLegend = function() {
    var exp = chart.legend.expanded();
    chart.legend.expanded(!exp);
    chart.update();
  }

  var histcatexplong = [
    {
    "key": "Consumer Discretionary",
    "values": [
      [1138683600000, 27.38478809681],
      [1141102800000, 27.371377218208],
      [1143781200000, 26.309915460827],
      [1146369600000, 26.425199957521],
      [1149048000000, 26.823411519395],
      [1151640000000, 23.850443591584],
      [1154318400000, 23.158355444054],
      [1156996800000, 22.998689393694],
      [1159588800000, 27.977128511299],
      [1162270800000, 29.073672469721],
      [1164862800000, 28.587640408904],
      [1167541200000, 22.788453687638],
      [1170219600000, 22.429199073597],
      [1172638800000, 22.324103271051],
      [1175313600000, 17.558388444186],
      [1177905600000, 16.769518096208],
      [1180584000000, 16.214738201302],
      [1183176000000, 18.729632971228],
      [1185854400000, 18.814523318848],
      [1188532800000, 19.789986451358],
      [1191124800000, 17.070049054933],
      [1193803200000, 16.121349575715],
      [1196398800000, 15.141659430091],
      [1199077200000, 17.175388025298],
      [1201755600000, 17.286592443521],
      [1204261200000, 16.323141626569],
      [1206936000000, 19.231263773952],
      [1209528000000, 18.446256391094],
      [1212206400000, 17.822632399764],
      [1214798400000, 15.539366475979],
      [1217476800000, 15.255131790216],
      [1220155200000, 15.660963922593],
      [1222747200000, 13.254482273697],
      [1225425600000, 11.920796202299],
      [1228021200000, 12.122809090925],
      [1230699600000, 15.691026271393],
      [1233378000000, 14.720881635107],
      [1235797200000, 15.387939360044],
      [1238472000000, 13.765436672229],
      [1241064000000, 14.6314458648],
      [1243742400000, 14.292446536221],
      [1246334400000, 16.170071367016],
      [1249012800000, 15.948135554337],
      [1251691200000, 16.612872685134],
      [1254283200000, 18.778338719091],
      [1256961600000, 16.75602606542],
      [1259557200000, 19.385804443147],
      [1262235600000, 22.950590240168],
      [1264914000000, 23.61159018141],
      [1267333200000, 25.708586989581],
      [1270008000000, 26.883915999885],
      [1272600000000, 25.893486687065],
      [1275278400000, 24.678914263176],
      [1277870400000, 25.937275793023],
      [1280548800000, 29.46138169384],
      [1283227200000, 27.357322961862],
      [1285819200000, 29.057235285673],
      [1288497600000, 28.549434189386],
      [1291093200000, 28.506352379723],
      [1293771600000, 29.449241421597],
      [1296450000000, 25.796838168807],
      [1298869200000, 28.740145449189],
      [1301544000000, 22.091744141872],
      [1304136000000, 25.079662545409],
      [1306814400000, 23.674906973064],
      [1309406400000, 23.41800274293],
      [1312084800000, 23.243644138871],
      [1314763200000, 31.591854066817],
      [1317355200000, 31.497112374114],
      [1320033600000, 26.672380820431],
      [1322629200000, 27.297080015495],
      [1325307600000, 20.174315530051],
      [1327986000000, 19.631084213899],
      [1330491600000, 20.366462219462],
      [1333166400000, 17.429019937289],
      [1335758400000, 16.75543633539],
      [1338436800000, 16.182906906042]
    ]
  }, {
    "key": "Consumer Staples",
    "values": [
      [1138683600000, 7.2800122043237],
      [1141102800000, 7.1187787503354],
      [1143781200000, 8.351887016482],
      [1146369600000, 8.4156698763993],
      [1149048000000, 8.1673298604231],
      [1151640000000, 5.5132447126042],
      [1154318400000, 6.1152537710599],
      [1156996800000, 6.076765091942],
      [1159588800000, 4.6304473798646],
      [1162270800000, 4.6301068469402],
      [1164862800000, 4.3466656309389],
      [1167541200000, 6.830104897003],
      [1170219600000, 7.241633040029],
      [1172638800000, 7.1432372054153],
      [1175313600000, 10.608942063374],
      [1177905600000, 10.914964549494],
      [1180584000000, 10.933223880565],
      [1183176000000, 8.3457524851265],
      [1185854400000, 8.1078413081882],
      [1188532800000, 8.2697185922474],
      [1191124800000, 8.4742436475968],
      [1193803200000, 8.4994601179319],
      [1196398800000, 8.7387319683243],
      [1199077200000, 6.8829183612895],
      [1201755600000, 6.984133637885],
      [1204261200000, 7.0860136043287],
      [1206936000000, 4.3961787956053],
      [1209528000000, 3.8699674365231],
      [1212206400000, 3.6928925238305],
      [1214798400000, 6.7571718894253],
      [1217476800000, 6.4367313362344],
      [1220155200000, 6.4048441521454],
      [1222747200000, 5.4643833239669],
      [1225425600000, 5.3150786833374],
      [1228021200000, 5.3011272612576],
      [1230699600000, 4.1203601430809],
      [1233378000000, 4.0881783200525],
      [1235797200000, 4.1928665957189],
      [1238472000000, 7.0249415663205],
      [1241064000000, 7.006530880769],
      [1243742400000, 6.994835633224],
      [1246334400000, 6.1220222336254],
      [1249012800000, 6.1177436137653],
      [1251691200000, 6.1413396231981],
      [1254283200000, 4.8046006145874],
      [1256961600000, 4.6647600660544],
      [1259557200000, 4.544865006255],
      [1262235600000, 6.0488249316539],
      [1264914000000, 6.3188669540206],
      [1267333200000, 6.5873958262306],
      [1270008000000, 6.2281189839578],
      [1272600000000, 5.8948915746059],
      [1275278400000, 5.5967320482214],
      [1277870400000, 0.99784432084837],
      [1280548800000, 1.0950794175359],
      [1283227200000, 0.94479734407491],
      [1285819200000, 1.222093988688],
      [1288497600000, 1.335093106856],
      [1291093200000, 1.3302565104985],
      [1293771600000, 1.340824670897],
      [1296450000000, 0],
      [1298869200000, 0],
      [1301544000000, 0],
      [1304136000000, 0],
      [1306814400000, 0],
      [1309406400000, 0],
      [1312084800000, 0],
      [1314763200000, 0],
      [1317355200000, 4.4583692315],
      [1320033600000, 3.6493043348059],
      [1322629200000, 3.8610064091761],
      [1325307600000, 5.5144800685202],
      [1327986000000, 5.1750695220792],
      [1330491600000, 5.6710066952691],
      [1333166400000, 8.5658461590953],
      [1335758400000, 8.6135447714243],
      [1338436800000, 8.0231460925212]
    ]
  }, {
    "key": "Energy",
    "values": [
      [1138683600000, 1.544303464167],
      [1141102800000, 1.4387289432421],
      [1143781200000, 0],
      [1146369600000, 0],
      [1149048000000, 0],
      [1151640000000, 1.328626801128],
      [1154318400000, 1.2874050802627],
      [1156996800000, 1.0872743105593],
      [1159588800000, 0.96042562635813],
      [1162270800000, 0.93139372870616],
      [1164862800000, 0.94432167305385],
      [1167541200000, 1.277750166208],
      [1170219600000, 1.2204893886811],
      [1172638800000, 1.207489123122],
      [1175313600000, 1.2490651414113],
      [1177905600000, 1.2593129913052],
      [1180584000000, 1.373329808388],
      [1183176000000, 0],
      [1185854400000, 0],
      [1188532800000, 0],
      [1191124800000, 0],
      [1193803200000, 0],
      [1196398800000, 0],
      [1199077200000, 0],
      [1201755600000, 0],
      [1204261200000, 0],
      [1206936000000, 0],
      [1209528000000, 0],
      [1212206400000, 0],
      [1214798400000, 0],
      [1217476800000, 0],
      [1220155200000, 0],
      [1222747200000, 1.4516108933695],
      [1225425600000, 1.1856025268225],
      [1228021200000, 1.3430470355439],
      [1230699600000, 2.2752595354509],
      [1233378000000, 2.4031560010523],
      [1235797200000, 2.0822430731926],
      [1238472000000, 1.5640902826938],
      [1241064000000, 1.5812873972356],
      [1243742400000, 1.9462448548894],
      [1246334400000, 2.9464870223957],
      [1249012800000, 3.0744699383222],
      [1251691200000, 2.9422304628446],
      [1254283200000, 2.7503075599999],
      [1256961600000, 2.6506701800427],
      [1259557200000, 2.8005425319977],
      [1262235600000, 2.6816184971185],
      [1264914000000, 2.681206271327],
      [1267333200000, 2.8195488011259],
      [1270008000000, 0],
      [1272600000000, 0],
      [1275278400000, 0],
      [1277870400000, 1.0687057346382],
      [1280548800000, 1.2539400544134],
      [1283227200000, 1.1862969445955],
      [1285819200000, 0],
      [1288497600000, 0],
      [1291093200000, 0],
      [1293771600000, 0],
      [1296450000000, 1.941972859484],
      [1298869200000, 2.1142247697552],
      [1301544000000, 2.3788590206824],
      [1304136000000, 2.5337302877545],
      [1306814400000, 2.3163370395199],
      [1309406400000, 2.0645451843195],
      [1312084800000, 2.1004446672411],
      [1314763200000, 3.6301875804303],
      [1317355200000, 2.454204664652],
      [1320033600000, 2.196082370894],
      [1322629200000, 2.3358418255202],
      [1325307600000, 0],
      [1327986000000, 0],
      [1330491600000, 0],
      [1333166400000, 0.39001201038526],
      [1335758400000, 0.30945472725559],
      [1338436800000, 0.31062439305591]
    ]
  }, {
    "key": "Financials",
    "values": [
      [1138683600000, 13.356778764352],
      [1141102800000, 13.611196863271],
      [1143781200000, 6.895903006119],
      [1146369600000, 6.9939633271352],
      [1149048000000, 6.7241510257675],
      [1151640000000, 5.5611293669516],
      [1154318400000, 5.6086488714041],
      [1156996800000, 5.4962849907033],
      [1159588800000, 6.9193153169279],
      [1162270800000, 7.0016334389777],
      [1164862800000, 6.7865422443273],
      [1167541200000, 9.0006454225383],
      [1170219600000, 9.2233916171431],
      [1172638800000, 8.8929316009479],
      [1175313600000, 10.345937520404],
      [1177905600000, 10.075914677026],
      [1180584000000, 10.089006188111],
      [1183176000000, 10.598330295008],
      [1185854400000, 9.968954653301],
      [1188532800000, 9.7740580198146],
      [1191124800000, 10.558483060626],
      [1193803200000, 9.9314651823603],
      [1196398800000, 9.3997715873769],
      [1199077200000, 8.4086493387262],
      [1201755600000, 8.9698309085926],
      [1204261200000, 8.2778357995396],
      [1206936000000, 8.8585045600123],
      [1209528000000, 8.7013756413322],
      [1212206400000, 7.7933605469443],
      [1214798400000, 7.0236183483064],
      [1217476800000, 6.9873088186829],
      [1220155200000, 6.8031713070097],
      [1222747200000, 6.6869531315723],
      [1225425600000, 6.138256993963],
      [1228021200000, 5.6434994016354],
      [1230699600000, 5.495220262512],
      [1233378000000, 4.6885326869846],
      [1235797200000, 4.4524349883438],
      [1238472000000, 5.6766520778185],
      [1241064000000, 5.7675774480752],
      [1243742400000, 5.7882863168337],
      [1246334400000, 7.2666010034924],
      [1249012800000, 7.519182132226],
      [1251691200000, 7.849651451445],
      [1254283200000, 10.383992037985],
      [1256961600000, 9.0653691861818],
      [1259557200000, 9.6705248324159],
      [1262235600000, 10.856380561349],
      [1264914000000, 11.27452370892],
      [1267333200000, 11.754156529088],
      [1270008000000, 8.2870811422456],
      [1272600000000, 8.0210264360699],
      [1275278400000, 7.5375074474865],
      [1277870400000, 8.3419527338039],
      [1280548800000, 9.4197471818443],
      [1283227200000, 8.7321733185797],
      [1285819200000, 9.6627062648126],
      [1288497600000, 10.187962234549],
      [1291093200000, 9.8144201733476],
      [1293771600000, 10.275723361713],
      [1296450000000, 16.796066079353],
      [1298869200000, 17.543254984075],
      [1301544000000, 16.673660675084],
      [1304136000000, 17.963944353609],
      [1306814400000, 16.637740867211],
      [1309406400000, 15.84857094609],
      [1312084800000, 14.767303362182],
      [1314763200000, 24.778452182432],
      [1317355200000, 18.370353229999],
      [1320033600000, 15.2531374291],
      [1322629200000, 14.989600840649],
      [1325307600000, 16.052539160125],
      [1327986000000, 16.424390322793],
      [1330491600000, 17.884020741105],
      [1333166400000, 7.1424929577921],
      [1335758400000, 7.8076213051482],
      [1338436800000, 7.2462684949232]
    ]
  }, {
    "key": "Health Care",
    "values": [
      [1138683600000, 14.212410956029],
      [1141102800000, 13.973193618249],
      [1143781200000, 15.218233920665],
      [1146369600000, 14.38210972745],
      [1149048000000, 13.894310878491],
      [1151640000000, 15.593086090032],
      [1154318400000, 16.244839695188],
      [1156996800000, 16.017088850646],
      [1159588800000, 14.183951830055],
      [1162270800000, 14.148523245697],
      [1164862800000, 13.424326059972],
      [1167541200000, 12.974450435753],
      [1170219600000, 13.23247041802],
      [1172638800000, 13.318762655574],
      [1175313600000, 15.961407746104],
      [1177905600000, 16.287714639805],
      [1180584000000, 16.246590583889],
      [1183176000000, 17.564505594809],
      [1185854400000, 17.872725373165],
      [1188532800000, 18.018998508757],
      [1191124800000, 15.584518016603],
      [1193803200000, 15.480850647181],
      [1196398800000, 15.699120036984],
      [1199077200000, 19.184281817226],
      [1201755600000, 19.691226605207],
      [1204261200000, 18.982314051295],
      [1206936000000, 18.707820309008],
      [1209528000000, 17.459630929761],
      [1212206400000, 16.500616076782],
      [1214798400000, 18.086324003979],
      [1217476800000, 18.929464156258],
      [1220155200000, 18.233728682084],
      [1222747200000, 16.315776297325],
      [1225425600000, 14.63289219025],
      [1228021200000, 14.667835024478],
      [1230699600000, 13.946993947308],
      [1233378000000, 14.394304684397],
      [1235797200000, 13.724462792967],
      [1238472000000, 10.930879035806],
      [1241064000000, 9.8339915513708],
      [1243742400000, 10.053858541872],
      [1246334400000, 11.786998438287],
      [1249012800000, 11.780994901769],
      [1251691200000, 11.305889670276],
      [1254283200000, 10.918452290083],
      [1256961600000, 9.6811395055706],
      [1259557200000, 10.971529744038],
      [1262235600000, 13.330210480209],
      [1264914000000, 14.592637568961],
      [1267333200000, 14.605329141157],
      [1270008000000, 13.936853794037],
      [1272600000000, 12.189480759072],
      [1275278400000, 11.676151385046],
      [1277870400000, 13.058852800017],
      [1280548800000, 13.62891543203],
      [1283227200000, 13.811107569918],
      [1285819200000, 13.786494560787],
      [1288497600000, 14.04516285753],
      [1291093200000, 13.697412447288],
      [1293771600000, 13.677681376221],
      [1296450000000, 19.961511864531],
      [1298869200000, 21.049198298158],
      [1301544000000, 22.687631094008],
      [1304136000000, 25.469010617433],
      [1306814400000, 24.883799437121],
      [1309406400000, 24.203843814248],
      [1312084800000, 22.138760964038],
      [1314763200000, 16.034636966228],
      [1317355200000, 15.394958944556],
      [1320033600000, 12.625642461969],
      [1322629200000, 12.973735699739],
      [1325307600000, 15.786018336149],
      [1327986000000, 15.227368020134],
      [1330491600000, 15.899752650734],
      [1333166400000, 18.994731295388],
      [1335758400000, 18.450055817702],
      [1338436800000, 17.863719889669]
    ]
  }, {
    "key": "Industrials",
    "values": [
      [1138683600000, 7.1590087090398],
      [1141102800000, 7.1297210970108],
      [1143781200000, 5.5774588290586],
      [1146369600000, 5.4977254491156],
      [1149048000000, 5.5138153113634],
      [1151640000000, 4.3198084032122],
      [1154318400000, 3.9179295839125],
      [1156996800000, 3.8110093051479],
      [1159588800000, 5.5629020916939],
      [1162270800000, 5.7241673711336],
      [1164862800000, 5.4715049695004],
      [1167541200000, 4.9193763571618],
      [1170219600000, 5.136053947247],
      [1172638800000, 5.1327258759766],
      [1175313600000, 5.1888943925082],
      [1177905600000, 5.5191481293345],
      [1180584000000, 5.6093625614921],
      [1183176000000, 4.2706312987397],
      [1185854400000, 4.4453235132117],
      [1188532800000, 4.6228003109761],
      [1191124800000, 5.0645764756954],
      [1193803200000, 5.0723447230959],
      [1196398800000, 5.1457765818846],
      [1199077200000, 5.4067851597282],
      [1201755600000, 5.472241916816],
      [1204261200000, 5.3742740389688],
      [1206936000000, 6.251751933664],
      [1209528000000, 6.1406852153472],
      [1212206400000, 5.8164385627465],
      [1214798400000, 5.4255846656171],
      [1217476800000, 5.3738499417204],
      [1220155200000, 5.1815627753979],
      [1222747200000, 5.0305983235349],
      [1225425600000, 4.6823058607165],
      [1228021200000, 4.5941481589093],
      [1230699600000, 5.4669598474575],
      [1233378000000, 5.1249037357],
      [1235797200000, 4.3504421250742],
      [1238472000000, 4.6260881026002],
      [1241064000000, 5.0140402458946],
      [1243742400000, 4.7458462454774],
      [1246334400000, 6.0437019654564],
      [1249012800000, 6.4595216249754],
      [1251691200000, 6.6420468254155],
      [1254283200000, 5.8927271960913],
      [1256961600000, 5.4712108838003],
      [1259557200000, 6.1220254207747],
      [1262235600000, 5.5385935169255],
      [1264914000000, 5.7383377612639],
      [1267333200000, 6.1715976730415],
      [1270008000000, 4.0102262681174],
      [1272600000000, 3.769389679692],
      [1275278400000, 3.5301571031152],
      [1277870400000, 2.7660252652526],
      [1280548800000, 3.1409983385775],
      [1283227200000, 3.0528024863055],
      [1285819200000, 4.3126123157971],
      [1288497600000, 4.594654041683],
      [1291093200000, 4.5424126126793],
      [1293771600000, 4.7790043987302],
      [1296450000000, 7.4969154058289],
      [1298869200000, 7.9424751557821],
      [1301544000000, 7.1560736250547],
      [1304136000000, 7.9478117337855],
      [1306814400000, 7.4109214848895],
      [1309406400000, 7.5966457641101],
      [1312084800000, 7.165754444071],
      [1314763200000, 5.4816702524302],
      [1317355200000, 4.9893656089584],
      [1320033600000, 4.498385105327],
      [1322629200000, 4.6776090358151],
      [1325307600000, 8.1350814368063],
      [1327986000000, 8.0732769990652],
      [1330491600000, 8.5602340387277],
      [1333166400000, 5.1293714074325],
      [1335758400000, 5.2586794619016],
      [1338436800000, 5.1100853569977]
    ]
  }, {
    "key": "Information Technology",
    "values": [
      [1138683600000, 13.242301508051],
      [1141102800000, 12.863536342042],
      [1143781200000, 21.034044171629],
      [1146369600000, 21.419084618803],
      [1149048000000, 21.142678863691],
      [1151640000000, 26.568489677529],
      [1154318400000, 24.839144939905],
      [1156996800000, 25.456187462167],
      [1159588800000, 26.350164502826],
      [1162270800000, 26.47833320519],
      [1164862800000, 26.425979547847],
      [1167541200000, 28.191461582256],
      [1170219600000, 28.930307448808],
      [1172638800000, 29.521413891117],
      [1175313600000, 28.188285966466],
      [1177905600000, 27.704619625832],
      [1180584000000, 27.490862424829],
      [1183176000000, 28.770679721286],
      [1185854400000, 29.060480671449],
      [1188532800000, 28.240998844973],
      [1191124800000, 33.004893194127],
      [1193803200000, 34.075180359928],
      [1196398800000, 32.548560664833],
      [1199077200000, 30.629727432728],
      [1201755600000, 28.642858788159],
      [1204261200000, 27.973575227842],
      [1206936000000, 27.393351882726],
      [1209528000000, 28.476095288523],
      [1212206400000, 29.29667866426],
      [1214798400000, 29.222333802896],
      [1217476800000, 28.092966093843],
      [1220155200000, 28.107159262922],
      [1222747200000, 25.482974832098],
      [1225425600000, 21.208115993834],
      [1228021200000, 20.295043095268],
      [1230699600000, 15.925754618401],
      [1233378000000, 17.162864628346],
      [1235797200000, 17.084345773174],
      [1238472000000, 22.246007102281],
      [1241064000000, 24.530543998509],
      [1243742400000, 25.084184918242],
      [1246334400000, 16.606166527358],
      [1249012800000, 17.239620011628],
      [1251691200000, 17.336739127379],
      [1254283200000, 25.478492475753],
      [1256961600000, 23.017152085245],
      [1259557200000, 25.617745423683],
      [1262235600000, 24.061133998642],
      [1264914000000, 23.223933318644],
      [1267333200000, 24.425887263937],
      [1270008000000, 35.501471156693],
      [1272600000000, 33.775013878676],
      [1275278400000, 30.417993630285],
      [1277870400000, 30.023598978467],
      [1280548800000, 33.327519522436],
      [1283227200000, 31.963388450371],
      [1285819200000, 30.498967232092],
      [1288497600000, 32.403696817912],
      [1291093200000, 31.47736071922],
      [1293771600000, 31.53259666241],
      [1296450000000, 41.760282761548],
      [1298869200000, 45.605771243237],
      [1301544000000, 39.986557966215],
      [1304136000000, 43.846330510051],
      [1306814400000, 39.857316881857],
      [1309406400000, 37.675127768208],
      [1312084800000, 35.775077970313],
      [1314763200000, 48.631009702577],
      [1317355200000, 42.830831754505],
      [1320033600000, 35.611502589362],
      [1322629200000, 35.320136981738],
      [1325307600000, 31.564136901516],
      [1327986000000, 32.074407502433],
      [1330491600000, 35.053013769976],
      [1333166400000, 26.434568573937],
      [1335758400000, 25.305617871002],
      [1338436800000, 24.520919418236]
    ]
  }, {
    "key": "Materials",
    "values": [
      [1138683600000, 5.5806167415681],
      [1141102800000, 5.4539047069985],
      [1143781200000, 7.6728842432362],
      [1146369600000, 7.719946716654],
      [1149048000000, 8.0144619912942],
      [1151640000000, 7.942223133434],
      [1154318400000, 8.3998279827444],
      [1156996800000, 8.532324572605],
      [1159588800000, 4.7324285199763],
      [1162270800000, 4.7402397487697],
      [1164862800000, 4.9042069355168],
      [1167541200000, 5.9583963430882],
      [1170219600000, 6.3693899239171],
      [1172638800000, 6.261153903813],
      [1175313600000, 5.3443942184584],
      [1177905600000, 5.4932111235361],
      [1180584000000, 5.5747393101109],
      [1183176000000, 5.3833633060013],
      [1185854400000, 5.5125898831832],
      [1188532800000, 5.8116112661327],
      [1191124800000, 4.3962296939996],
      [1193803200000, 4.6967663605521],
      [1196398800000, 4.7963004350914],
      [1199077200000, 4.1817985183351],
      [1201755600000, 4.3797643870182],
      [1204261200000, 4.6966642197965],
      [1206936000000, 4.3609995132565],
      [1209528000000, 4.4736290996496],
      [1212206400000, 4.3749762738128],
      [1214798400000, 3.3274661194507],
      [1217476800000, 3.0316184691337],
      [1220155200000, 2.5718140204728],
      [1222747200000, 2.7034994044603],
      [1225425600000, 2.2033786591364],
      [1228021200000, 1.9850621240805],
      [1230699600000, 0],
      [1233378000000, 0],
      [1235797200000, 0],
      [1238472000000, 0],
      [1241064000000, 0],
      [1243742400000, 0],
      [1246334400000, 0],
      [1249012800000, 0],
      [1251691200000, 0],
      [1254283200000, 0.44495950017788],
      [1256961600000, 0.33945469262483],
      [1259557200000, 0.38348269455195],
      [1262235600000, 0],
      [1264914000000, 0],
      [1267333200000, 0],
      [1270008000000, 0],
      [1272600000000, 0],
      [1275278400000, 0],
      [1277870400000, 0],
      [1280548800000, 0],
      [1283227200000, 0],
      [1285819200000, 0],
      [1288497600000, 0],
      [1291093200000, 0],
      [1293771600000, 0],
      [1296450000000, 0.52216435716176],
      [1298869200000, 0.59275786698454],
      [1301544000000, 0],
      [1304136000000, 0],
      [1306814400000, 0],
      [1309406400000, 0],
      [1312084800000, 0],
      [1314763200000, 0],
      [1317355200000, 0],
      [1320033600000, 0],
      [1322629200000, 0],
      [1325307600000, 0],
      [1327986000000, 0],
      [1330491600000, 0],
      [1333166400000, 0],
      [1335758400000, 0],
      [1338436800000, 0]
    ]
  }, {
    "key": "Telecommunication Services",
    "values": [
      [1138683600000, 3.7056975170243],
      [1141102800000, 3.7561118692318],
      [1143781200000, 2.861913700854],
      [1146369600000, 2.9933744103381],
      [1149048000000, 2.7127537218463],
      [1151640000000, 3.1195497076283],
      [1154318400000, 3.4066964004508],
      [1156996800000, 3.3754571113569],
      [1159588800000, 2.2965579982924],
      [1162270800000, 2.4486818633018],
      [1164862800000, 2.4002308848517],
      [1167541200000, 1.9649579750349],
      [1170219600000, 1.9385263638056],
      [1172638800000, 1.9128975336387],
      [1175313600000, 2.3412869836298],
      [1177905600000, 2.4337870351445],
      [1180584000000, 2.62179703171],
      [1183176000000, 3.2642864957929],
      [1185854400000, 3.3200396223709],
      [1188532800000, 3.3934212707572],
      [1191124800000, 4.2822327088179],
      [1193803200000, 4.1474964228541],
      [1196398800000, 4.1477082879801],
      [1199077200000, 5.2947122916128],
      [1201755600000, 5.2919843508028],
      [1204261200000, 5.1989783050309],
      [1206936000000, 3.5603057673513],
      [1209528000000, 3.3009087690692],
      [1212206400000, 3.1784852603792],
      [1214798400000, 4.5889503538868],
      [1217476800000, 4.401779617494],
      [1220155200000, 4.2208301828278],
      [1222747200000, 3.89396671475],
      [1225425600000, 3.0423832241354],
      [1228021200000, 3.135520611578],
      [1230699600000, 1.9631418164089],
      [1233378000000, 1.8963543874958],
      [1235797200000, 1.8266636017025],
      [1238472000000, 0.93136635895188],
      [1241064000000, 0.92737801918888],
      [1243742400000, 0.97591889805002],
      [1246334400000, 2.6841193805515],
      [1249012800000, 2.5664341140531],
      [1251691200000, 2.3887523699873],
      [1254283200000, 1.1737801663681],
      [1256961600000, 1.0953582317281],
      [1259557200000, 1.2495674976653],
      [1262235600000, 0.36607452464754],
      [1264914000000, 0.3548719047291],
      [1267333200000, 0.36769242398939],
      [1270008000000, 0],
      [1272600000000, 0],
      [1275278400000, 0],
      [1277870400000, 0],
      [1280548800000, 0],
      [1283227200000, 0],
      [1285819200000, 0.85450741275337],
      [1288497600000, 0.91360317921637],
      [1291093200000, 0.89647678692269],
      [1293771600000, 0.87800687192639],
      [1296450000000, 0],
      [1298869200000, 0],
      [1301544000000, 0.43668720882994],
      [1304136000000, 0.4756523602692],
      [1306814400000, 0.46947368328469],
      [1309406400000, 0.45138896152316],
      [1312084800000, 0.43828726648117],
      [1314763200000, 2.0820861395316],
      [1317355200000, 0.9364411075395],
      [1320033600000, 0.60583907839773],
      [1322629200000, 0.61096950747437],
      [1325307600000, 0],
      [1327986000000, 0],
      [1330491600000, 0],
      [1333166400000, 0],
      [1335758400000, 0],
      [1338436800000, 0]
    ]
  }, {
    "key": "Utilities",
    "values": [
      [1138683600000, 0],
      [1141102800000, 0],
      [1143781200000, 0],
      [1146369600000, 0],
      [1149048000000, 0],
      [1151640000000, 0],
      [1154318400000, 0],
      [1156996800000, 0],
      [1159588800000, 0],
      [1162270800000, 0],
      [1164862800000, 0],
      [1167541200000, 0],
      [1170219600000, 0],
      [1172638800000, 0],
      [1175313600000, 0],
      [1177905600000, 0],
      [1180584000000, 0],
      [1183176000000, 0],
      [1185854400000, 0],
      [1188532800000, 0],
      [1191124800000, 0],
      [1193803200000, 0],
      [1196398800000, 0],
      [1199077200000, 0],
      [1201755600000, 0],
      [1204261200000, 0],
      [1206936000000, 0],
      [1209528000000, 0],
      [1212206400000, 0],
      [1214798400000, 0],
      [1217476800000, 0],
      [1220155200000, 0],
      [1222747200000, 0],
      [1225425600000, 0],
      [1228021200000, 0],
      [1230699600000, 0],
      [1233378000000, 0],
      [1235797200000, 0],
      [1238472000000, 0],
      [1241064000000, 0],
      [1243742400000, 0],
      [1246334400000, 0],
      [1249012800000, 0],
      [1251691200000, 0],
      [1254283200000, 0],
      [1256961600000, 0],
      [1259557200000, 0],
      [1262235600000, 0],
      [1264914000000, 0],
      [1267333200000, 0],
      [1270008000000, 0],
      [1272600000000, 0],
      [1275278400000, 0],
      [1277870400000, 0],
      [1280548800000, 0],
      [1283227200000, 0],
      [1285819200000, 0],
      [1288497600000, 0],
      [1291093200000, 0],
      [1293771600000, 0],
      [1296450000000, 0],
      [1298869200000, 0],
      [1301544000000, 0],
      [1304136000000, 0],
      [1306814400000, 0],
      [1309406400000, 0],
      [1312084800000, 0],
      [1314763200000, 0],
      [1317355200000, 0],
      [1320033600000, 0],
      [1322629200000, 0],
      [1325307600000, 0],
      [1327986000000, 0],
      [1330491600000, 0],
      [1333166400000, 0],
      [1335758400000, 0],
      [1338436800000, 0]
    ]
  }];
  var colors = d3.scale.category20();
  var legend = nv.models.legend().width(400)

  var chart;
  nv.addGraph(function() {
    chart = nv.models.stackedAreaWithFocusChart()
      .useInteractiveGuideline(true)
      .x(function(d) {
        return d[0]
      })
      .y(function(d) {
        return d[1]
      })
      .duration(300);
    chart.height(500);
    chart.margin({"left":50,"right":5,"top":5,"bottom":5});
    chart.controlOptions(["Stacked","Stream"]);
    chart.showLegend('true');
    chart.legendPosition('top');
    chart.brushExtent([1225425600000, 1285819200000]);
    chart.xAxis.tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });
    chart.x2Axis.tickFormat(function(d) {
      return d3.time.format('%x')(new Date(d))
    });
    chart.yAxis.tickFormat(d3.format(',.4f'));
    chart.y2Axis.tickFormat(d3.format(',.4f'));

    d3.select('#chart svg')
      .datum(histcatexplong)
      .transition().duration(300)
      .call(chart)  
      .call(legend)
      .each('start', function() {
        setTimeout(function() {
          d3.selectAll('#chart *').each(function() {
            if (this.__transition__)
              this.__transition__.duration = 1;
          })
        }, 0)
      });
    nv.utils.windowResize(chart.update);
    return chart;
  });

  
  

});
</script>
</html>