

    <!-- //////////////////////////////////////////////////////////////////////////// -->
	 <?php $this->load->view('template/header.php');?>
    <!-- START MAIN -->

    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->

        <section id="content">

          <div class="page-header">

            <div class="container">

              <h2 class="page-title">My Personal Profile</h2>

            </div>

          </div>



          <div class="page-content">

            <div class="container">

              <div class="welcom-container">

                <div class="Welcome-image">

                  <img src="<?php echo base_url();?>asset/images/my-customer-blank.png" alt="customer">

                </div>

                <div class="welcom-content">

                  <p class="welcom-title">Hey Admin<?php //echo $this->session->userdata['aduserData']['aduser_nm']; ?>!</p>

                  <p class="welcom-text">Welcome abroad! Let's have you fill all your personal details here :)<br>Let's get started by adding one below.</p>

                  <p class="welcome-button">

                    <a class="btn btn-welcome" href="<?php echo base_url();?>Personal_profile/manage_profile">ADD NEW PERSONAL PROFILE</a>

                  </p>

                </div>

              </div>

            </div>

          </div>

        </section>

        <!-- END CONTENT -->

        </div>

        <!-- END WRAPPER -->

      </div>

      <!-- END MAIN -->
	 <?php $this->load->view('template/footer.php');?>
 