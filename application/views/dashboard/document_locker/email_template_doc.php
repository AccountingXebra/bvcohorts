<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document Details</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
                <!--<tr>
                    <td><a href="#" target="_blank">
                        <img class="columnImage"
                            src="<?php echo base_url();?>public/email_template/images/myeasyinvoice.jpg"
                            border="0"/>
                        </a></td>
                    <td><img class="columnImage" src="<?php echo base_url();?>public/email_template/images/topslice.jpg" border="0"/></td>
                </tr>-->
				<tr>
                    <td><!--a href="#" target="_blank"><img width="200" height="50" class="columnImage" src="<?php //echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" border="0" alt="eazyinvoice-logo" /></a-->
					<a href="#" target="_blank"><img style="margin:10px !important;" width="205" height="55" class="columnImage" src="<?php echo base_url(); ?>public/images/xebra-logo.png" border="0"/></a>
					</td>
                    <td><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0" alt="topslice" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><h3
                style="margin: 40px 0px 20px 0;font-size: 20px;">Details of Document</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage" src="<?php echo base_url();?>public/email_template/images/37-Credit-Note-Details.gif" border="0" alt="credit-note"/></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">Hey <span style="color: #503cb7"> <?php echo ucwords($this->user_session['ei_username']); ?> </span>,</p>
            <p style="font-size: 15px">Attached with this email you will find a spreadsheet with the details of your Document.
                </p>
            <p style="font-size: 15px">For any queries write to us at <b> <?php echo $this->user_session['email']; ?> </b>.</p>

            
        </td>
    </tr>
    <!--<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;">
            <p style="margin: 21px;">
                <a href="#" style="-webkit-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);-moz-box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);box-shadow: 0px 3px 15px 0px rgba(0, 0, 0, 0.75);background: #503cb5;margin-top: 20px;padding: 10px 50px;color: #fff;border-radius: 20px;text-decoration: none;font-size: 18px;">
                    LET’S GET STARTED</a>
            </p>
        </td>
    </tr>-->
    <!--<tr><td><img class="columnImage" src="<?php echo base_url();?>public/email_template/images/bottomslice.jpg" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
                   style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68;">
                <tr>
                    <td align="center" >
                        <p style="margin: 10px 22px;
    border-bottom: 1.5px solid #ffffff;"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url();?>public/email_template/images/twitter.png" border="0"/></p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="color: #ffffff;font-family: 'Roboto', sans-serif;">
                        <p style="font-size: 12px;">Copyright &copy; MyEazyInvoices. All Rights Reserved.</p>
                        <p style="font-size: 12px;">Want to change how you receive these emails?<br>
                            You can <a href="#" style="color: #ffffff">update your preferences</a> or <a href="#" style="color: #ffffff">unsubscribe from this list</a></p>
                    </td>

                </tr>
            </table>
        </td>
    </tr>-->
</table>
</body>
</html>
