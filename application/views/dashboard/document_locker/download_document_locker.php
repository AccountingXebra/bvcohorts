<?php 
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('Document Locker');
$this->excel->getActiveSheet()->setCellValue('A1', 'Document Locker');
$this->excel->getActiveSheet()->mergeCells('A1:G1');
$this->excel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('A3', 'UPLOAD DATE');
$this->excel->getActiveSheet()->setCellValue('B3', 'DOC NAME');
$this->excel->getActiveSheet()->setCellValue('C3', 'MODULE NAME');
$this->excel->getActiveSheet()->setCellValue('D3', 'FILE SIZE');
$this->excel->getActiveSheet()->setCellValue('E3', 'DATE');
$this->excel->getActiveSheet()->setCellValue('F3', 'START DATE');
$this->excel->getActiveSheet()->setCellValue('G3', 'END DATE');

$this->excel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	
	$this->excel->getActiveSheet()->setCellValue('A'.$count, date('d-m-Y',strtotime($value['createdat'])));
	$this->excel->getActiveSheet()->setCellValue('B'.$count, $value['legal_document']);
	if($value['legal_type']=="customer"){
		$this->excel->getActiveSheet()->setCellValue('C'.$count, 'My Clients');
	}else{
		$this->excel->getActiveSheet()->setCellValue('C'.$count, 'Company Profile');
	}
	
	$this->excel->getActiveSheet()->setCellValue('D'.$count, $value['legal_document_size'].' Kb');
	if($value['legal_po_date'] != '0000-00-00'){
		$this->excel->getActiveSheet()->setCellValue('E'.$count, date('d-m-Y',strtotime($value['legal_po_date'])));
	}else {
		$this->excel->getActiveSheet()->setCellValue('E'.$count, '');
	}
	if($value['legal_start_date'] != '0000-00-00'){
		$this->excel->getActiveSheet()->setCellValue('F'.$count, date('d-m-Y',strtotime($value['legal_start_date'])));
	}else {
		$this->excel->getActiveSheet()->setCellValue('F'.$count, '');
	}
	if($value['legal_end_date'] != '0000-00-00'){ 
		$this->excel->getActiveSheet()->setCellValue('G'.$count, date('d-m-Y',strtotime($value['legal_end_date'])));
	}else {
		$this->excel->getActiveSheet()->setCellValue('G'.$count, '');
	}

	
	$count++;
}


$filename='Document Locker '.date("d-m-Y").'.xlsx'; //save our workbook as this file name
ob_end_clean();		
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
ob_end_clean();	
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>