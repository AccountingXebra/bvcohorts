<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
		width: 100%;
		border-spacing: 0;
	} 
	.tableizer-table td {
		/*padding: 4px;
		margin: 3px;*/
		text-align: center;
		border: 1px solid #CCC;
		padding: 10px;
	}
	.tableizer-table th {
		/*background-color: #104E8B; */
		font-weight: bold;
		border: 1px solid #CCC;
		padding: 10px;
	}
</style>
<table class="tableizer-table">
<thead>
	<tr style="text-align: center;"><th colspan="7"><h2 style="margin: 0">Document Locker</h2></th></tr>
	<tr>
	<th>UPLOAD DATE</th>
	<th>DOC NAME</th>
	<th>MODULE NAME</th>
	<th>FILE SIZE</th>
	<th>DATE</th>
	<th>START DATE</th>
	<th>END DATE</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($result as $key => $value) { ?>
	<tr>
	
	<td><?= date('d-m-Y',strtotime($value['createdat'])) ?></td>
	<td><?=	$value['legal_document'] ?></td>
	<?php  if($value['legal_type']=="customer"){ ?>
		<td>My Clients</td>
	<?php }else{ ?>	
		<td>Company Profile</td>
	<?php } ?>	
	<td><?=	$value['legal_document_size'] ?> Kb</td>
	<td><?php if($value['legal_po_date']!='' && $value['legal_po_date'] != '0000-00-00' ){ echo date('d-m-Y',strtotime($value['legal_po_date'])); }else { } ?></td>
	<td><?php if($value['legal_start_date']!='' && $value['legal_start_date'] != '0000-00-00' ){ echo date('d-m-Y',strtotime($value['legal_start_date'])); }else { } ?></td>
	<td><?php if($value['legal_end_date']!='' && $value['legal_end_date'] != '0000-00-00' ){ echo date('d-m-Y',strtotime($value['legal_end_date'])); }else { } ?></td>
	
<?php } ?>

</tbody>
</table>
<script type="text/javascript">
	window.print();
</script>