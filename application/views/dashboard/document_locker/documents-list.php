 <?php $this->load->view('template/header.php');?>

   <!-- START MAIN -->
   <style type="text/css">

  .dataTables_scrollBody{
      overflow:hidden !important;
      height:100% !important;
    }

    .dataTables_scrollHead{
      margin-bottom:-24px !important;
    }

    table.dataTable thead .sorting {
      background-position: 110px 15px !important;
    }
    table.dataTable thead .sorting_asc {
      background-position: 110px 15px !important;
    }
    table.dataTable thead .sorting_desc {
      background-position: 110px 15px !important;
    }
    table.dataTable thead .sorting {
    background-position: 125px 17px !important;
}
     .btn-date {
		margin: 0 0 0 3px !important;
		padding-top:2px;
    }
    .btn-stated {
        max-width: 80px !important;
		background-position: 94px center !important;
    }
    .icon-img-noti {
      width: 22px;
      margin: 0 5px 0 2px;
    }
    .addnew.btn-dropdown-select span.caret {

  color: #FFFF;
}
.addnew.btn-dropdown-select > input.select-dropdown{
 color: #FFFF !important;
 padding: 0 28px 0 18px !important;
 max-width: 100%;
}
.addnew.dropdown-content li > span{
  color: #000!important;
}
.btn-welcome {
  padding: 0px 0 !important;
}
.welcome-button {
  float: left;
}
@media only screen and (min-width: 993px){
.container {
    width: 93% !important;
}
}
.dataTables_length {

    margin-left: 500px;
    min-width: :90px;
}



  #documents-locker_length{
    border:1px solid #B0B7CA !important;
    height:38px;
    border-radius:4px;
    width:88px;
    margin-top:5px;
    margin-left:52%;
  }

  #documents-locker_length .select-wrapper input.select-dropdown {
    margin-top:-3px !important;
    margin-left:5px !important;
  }

  #documents-locker_length .dropdown-content {
    min-width: 90px;
    margin-top:-55% !important;
  }

  .docl_bulk_action.filled-in:not(:checked) + label:after {
    top:5px !important;
  }

  .select-cmpy{
    margin-left:120px !important;
    width:36% !important;
  }

  ::placeholder{
    font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
  }

  .sticky {
    position: fixed;
    top: 70px;
    width: 74.5%;
    z-index:999;
    background: white;
    color: black;

  }

  .sticky + .scrollbody {
    padding-top: 102px;
  }

  /*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 30px;
  color: #000;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 165px !important;
  margin-left: 0px;

}
</style>
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
         <?php $this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-theme-gray documents-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
              <div class="row">
                <?php $dc=count($legal);
                  //$dc=$dc+(count($other));
                ?>
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Document Locker <small class="grey-text">(<span id="doc_tot"></span> Total)</small></h5>

                  <ol class="breadcrumbs">

                    <li class="active">MY PROFILE / DOCUMENT LOCKER</li>
                  </ol>
                </div>

            </div>
          </div>
        </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
                <div class="col l12 s12 m12 ">
                 <a href="javascript:void(0);" class="addmorelink right" onclick="reset_doclfilter();" title="Reset all">Reset</a>
                </div>
                <div class="col l5 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk4'>Bulk Actions <i class="arrow-icon"></i></a>
                  <ul id='dropdown_bulk4' class='dropdown-content'>

                    <li><a id="email_multiple_docl"><i class="material-icons">email</i> Email</a></li>
                    <li><a id="download_multiple_docl" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 21px;height: 22px;" alt="export"></i>Download</a></li>
                    <!-- <li><a id="print_multiple_docl"><i class="material-icons">print</i>Print</a></li> -->
                    <li style="display:none;"><a id="deactive_multiple_docl" data-multi_docl="0"><i class="material-icons">delete</i>Delete</a></li>
                  </ul>
                  <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_docl" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
                  </a>
                </div>
                 <div class="col l7 s12 m12">
                  <div class="action-btn-wapper sort right">
                    <select style="min-width:100% !important;" class='js-example-basic-single' id="docl_type" name="docl_type">
                        <option value="">FOR MODULE</option>
                        <option value="company">MY COMPANY PROFILE</option>
                      <option value="customer">CLIENT MASTER</option>
            <!--option value="item">ITEM MASTER</option-->
            <option value="billing_doc">BILLING DOCUMENTS</option>
            <option value="credit_note">CREDIT NOTES</option>
            <option value="expense_voucher">EXPENSE VOUCHERS</option>
            <option value="vendor">VENDOR MASTER</option>
            <!--option value="expense">EXPENSE MASTER</option-->
            <!--option value="purchase_order">PURCHASE ORDER</option-->
            <option value="com_exp">COMPANY EXPENSE</option>
            <!--option value="salary_exp">SALARY EXPENSE</option-->
            <option value="debit">DEBIT NOTES</option>
            <!--option value="pettycash">PETTY CASH</option-->
            <option value="asset_register">VENDOR & ASSET MASTER</option>
            <option value="asset_purchase">ASSET PURCHASE</option>
            <!--option value="asset_sales">ASSET SALES</option-->
            <option value="asset_tracker">ASSET TRACKER</option>
            <!--option value="depre_sche">DEPRECIATION SCHEDULE</option-->
            <option value="sales_receipt">SALES RECEIPT</option>
            <!--option value="asset_sales_receipt">ASSET SALES RECEIPT</option-->
            <option value="expense_payments">EXPENSE PAYMENT</option>
            <!--option value="asset_pay">ASSET PAYMENT</option-->
            <!--option value="salary_pay">SALARY PAYMENT</option>
            <option value="salary_pay">SALARY PAYMENT</option-->
            <option value="gst">GST</option>
            <option value="tds_exp">TDS EXPENSE</option>
            <option value="tds_emp">TDS EMPLOYEE</option>
            <option value="gst">EQUALISATION LEVY</option>
            <option value="prof_tax">PROFESSIONAL TAX</option>
            <option value="emp_master">EMPLOYEE MASTER</option>
            <!-- option value="appraisal">APPRAISAL</option>
            <option value="cash_stat">CASH STATEMENT</option>
            <option value="bank_stat">BANK STATEMENT</option-->
                    </select>
                     <!--<select class='ml-3px border-radius-6 btn-dropdown-select border-split-form select-like-dropdown select-cmpy' id="cust_id" name="cust_id">
                        <option value="">Select Client</option>

                                             <?php if($clients != '') {
                                      foreach($clients as $client) { ?>
                                        <option class="icon-location" value="<?php echo $client->cust_id; ?>"><?php echo $client->cust_name; ?></option>
                                          <?php }  } ?>
                    </select>-->
                    <input type="text" style="height:38px !important; font-size:11px !important; margin-left:8px !important;" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="docl_start_date" name="docl_start_date" readonly="readonly">
                    <input type="text" style="height:38px !important; font-size:11px !important;" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="docl_end_date" name="docl_end_date" readonly="readonly">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12" id="print_services_tbl">
               <!-- <form>-->
                  <table id="documents-locker" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:5%;" >
                          <input type="checkbox" id="docl_bulk" name="docl_bulk" class="filled-in purple" />
                          <label for="docl_bulk"></label>
                        </th>
                        <th style="width:15%;" >UPLOAD DATE</th>
                        <th style="width:10%;" >DOC NAME</th>
                        <th style="width:20%;" >CLIENT</th>
                        <th style="width:20%;">MODULE NAME</th>

                        <th style="width:15%;">FILE SIZE</th>
                        <th style="width:20% text-align:center;">REMINDERS</th><!--
                        <th style="width: 150px"></th> -->
                        <th style="width:5%;">Actions</th>
                      </tr>
                        <tr>
                        <th style="width:5%;" >

                        </th>
                        <th style="width:15%;" ></th>
                        <th style="width:10%;" ></th>
                        <th style="width:20%;" ></th>
                        <th style="width:20%;"></th>

                        <th style="width:15%"></th>
                        <th style="width:20%; text-align:center;"></th><!--
                        <th style="width: 150px"></th> -->
                        <th style="width:5%;"></th>
                      </tr>

                    </thead>
                    <tbody class="scrollbody">

                    </tbody>
                  </table>
               <!-- </form>-->
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
<div id="deactivate_dcl" class="modal modal-set">

    <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

    <div class="modal-content">

      <div class="modal-header">

        <h4> Remove Document </h4>

        <input type="hidden" id="remove_dcl_id" name="remove_dcl_id" value="" />

        <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

      </div></div>

      <div class="modal-body confirma">

        <p>Are you sure you want to remove this document?</p>

      </div>

      <div class="modal-content">

        <div class="modal-footer">

          <div class="row">



            <div class="col l12 s12 m12 cancel-deactiv">

              <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>


              <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close delete_dcl">REMOVE</button>

            </div>

          </div>

        </div>

      </div>

    </div>


<div id="email_popup_doc_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
    <?php $this->load->view('dashboard/document_locker/email_popup_doc'); ?>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  //$('.tooltipped').tooltip();
  $('.js-example-basic-single').trigger('change.select2');
$("#docl_type").on('change',function(){
var module=$("#docl_type").val();
if(module=='company'){

}else{

}

})

});
function reset_doclfilter(){
  $('#docl_type').prop('selectedIndex',0);
$('.action-btn-wapper').find('select').prop('selectedIndex',0);
    $('.js-example-basic-single').trigger('change.select2');
    $('.btn-date, .search-hide-show').val('');
  //$('.js-example-basic-single').trigger('change.select2');
  $('.btn-date,.search-hide-show').val('');

  doclDatatable(base_url+'Dashboard/get_documents_locker/','documents-locker');
  $('select').material_select();
}
</script>

<?php $this->load->view('template/footer.php'); ?>
