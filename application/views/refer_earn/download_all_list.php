<?php
//load our new PHPExcel library
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
$this->excel->getActiveSheet()->setTitle('Refer & Earn');
$this->excel->getActiveSheet()->setCellValue('A1', 'Your Referal Code is ');
$this->excel->getActiveSheet()->mergeCells('A1:D1');
$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
$this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A3', 'Client Name');
$this->excel->getActiveSheet()->setCellValue('B3', 'Status');
$this->excel->getActiveSheet()->setCellValue('C3', 'Subscription');
$this->excel->getActiveSheet()->setCellValue('D3', 'Referal Fee & Status');
$this->excel->getActiveSheet()->setCellValue('E3', 'Signup');
$this->excel->getActiveSheet()->getStyle("A3:E3")->getFont()->setBold(true);
$count=4;
foreach ($result as $key => $value) {
	if($value['refer_list_email'] != '')
		$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['refer_list_email']);
	else
		$this->excel->getActiveSheet()->setCellValue('A'.$count, $value['refer_list_mobile']);

	if(!empty($value['createdat'])) {
		$status = $data['personal_profile'][0]->reg_istrial;
		if($status == 1)
			$this->excel->getActiveSheet()->setCellValue('B'.$count, 'Trial');
		else
			$this->excel->getActiveSheet()->setCellValue('B'.$count, 'Paid');
		if(!empty($value['subscription_plan']))
		 	$this->excel->getActiveSheet()->setCellValue('C'.$count, $value['subscription_plan']);
		else
			$this->excel->getActiveSheet()->setCellValue('C'.$count, 'not yet');
		$this->excel->getActiveSheet()->setCellValue('D'.$count, 'not yet');
		$this->excel->getActiveSheet()->setCellValue('E'.$count, $value["createdat"]);
	}
	else {
		$this->excel->getActiveSheet()->setCellValue('B'.$count, 'not yet');
		$this->excel->getActiveSheet()->setCellValue('C'.$count, 'not yet');
		$this->excel->getActiveSheet()->setCellValue('D'.$count, 'not yet');
		$this->excel->getActiveSheet()->setCellValue('E'.$count, 'not yet');
	}

	$count++;
}

$filename='ReferEarnList'.date("d-m-Y").'.xlsx'; //save our workbook as this file name
	ob_end_clean();
 header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
ob_end_clean();
$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');

$objWriter->save('php://output');
?>
