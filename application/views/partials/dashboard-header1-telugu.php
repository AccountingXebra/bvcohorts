<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->

    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">
		<a class="navbar-brand" href="<?php echo base_url();?>telugu/dashboard"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">కేటగిరీలు<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/dashboard-submit-stories">కథనాలను సమర్పించండి</a>
				</li>

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome <?php echo $user['first_name']; ?></a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard-edit-account">Edit Account &nbsp</a>
						<a class="dropdown-item" href="<?php echo base_url();?>home/logout">Logout</a>
					</div>
				</li>

				<li class="nav-item dropdown cat-drop-head">

					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">తెలుగు</a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>home/dashboard">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi/dashboard">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla/dashboard">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati/dashboard">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi/dashboard">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil/dashboard">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyFN1Jz3sR5KuHqVyAN-Vr1y" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 30.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 30.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 30.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 30.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 30.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#">కేటగిరీలు<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/dashboard-submit-stories">కథనాలను సమర్పించండి</a>
				</li>

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome <?php echo $user['first_name']; ?></a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard-edit-account">Edit Account &nbsp</a>
						<a class="dropdown-item" href="<?php echo base_url();?>home/logout">Logout</a>
					</div>
				</li>
				&nbsp&nbsp&nbsp&nbsp

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">తెలుగు &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>home/dashboard">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi/dashboard">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla/dashboard">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati/dashboard">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi/dashboard">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil/dashboard">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard">తెలుగు</a>
					</div>
				</li>

				&nbsp&nbsp&nbsp&nbsp
				<li class="nav-item social-item" style="text-align:right;">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyFN1Jz3sR5KuHqVyAN-Vr1y" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 30.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 30.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 30.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 30.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 30.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>

	</nav>
	<div class="categories-header1" style="position:absolute; background-color:#fff; width:100%; z-index:1;">
		<hr>
		<div class="row" style="margin-top:2%;">
			<div class="bv-catgr-art bv-catgr-at">
			<img src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50"></img>
			<br><br>
			<p class="cat-name-art">కళ, సంస్కృతి <br><br> & కళాకారులు</p>
		</div>

		<div class="bv-catgr bv-catgr-sci">
			<img src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
			<p class="cat-name-sci">సైన్స్ & టెక్నాలజీ</p>
		</div>

		<div class="bv-catgr bv-catgr-pets">
			<img src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
			<br><br>
			<p class="cat-name-pet">పెంపుడు జంతువులు <br><br> & పర్యావరణం</p>
		</div>

		<div class="bv-catgr bv-catgr-peo">
			<img src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
			<p class="cat-name-peo">పీపుల్</p>
		</div>

		<div class="bv-catgr bv-catgr-hel">
			<img src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
			<p class="cat-name-helt">ఆరోగ్య</p>
		</div>

		<div class="bv-catgr bv-catgr-life">
			<img src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
			<p class="cat-name-life">జీవనశైలి & ఆరుబయట</p>
		</div>

		<div class="bv-catgr bv-catgr-edu">
			<img src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
			<br><br>
			<p class="cat-name-edu">విద్య & అభివృద్ధి</p>
		</div>

		<div class="bv-catgr-child bv-catgr-chld">
			<img src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
			<p class="cat-name-child">పిల్లలు & వృద్ధులు</p>
		</div>
		</div>
		<div class="row cat-list">
			<div class="bv-catgr-art">
			<ul class="art-list">
				<?php if(count($catdance)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-classical-dance" target="_blank">భారత శాస్త్రీయ నృత్యము</a></li>
				<?php } ?>

				<?php if(count($catmusic)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-classical-music" target="_blank" target="_blank" target="_blank">భారతీయ శాస్త్రీయ సంగీతం</a></li>
				<?php } ?>

				<?php if(count($catinstrument)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-classical-instruments" target="_blank" target="_blank">భారత శాస్త్రీయ వాద్య పరికరములు</a></li>
				<?php } ?>

				<?php if(count($cattheatre)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-theatre" target="_blank">భారత కళాక్షేత్రం</a></li>
				<?php } ?>

				<?php if(count($catpainters)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-painters" target="_blank">భారత చిత్రకారులు</a></li>
				<?php } ?>

				<?php if(count($catcraftsmen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/indian-craftsmen" target="_blank">భారతీయ హస్తకళాకారులు</a></li>
				<?php } ?>

				<?php if(count($catbca)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/books-comics-and-authors" target="_blank">పుస్తకాలు, కామిక్స్ & రచయితలు</a></li>
				<?php } ?>
				
				<?php if(count($catfun)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/fun-festivals" target="_blank">సరదా పండుగలు</a></li>
				<?php } ?>

				<?php if(count($catarch)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/archaeology" target="_blank">పురాతత్వ శాస్త్రం</a></li>
				<?php } ?>

				<?php if(count($cathistory)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/history" target="_blank">చరిత్ర</a></li>
				<?php } ?>

				<?php if(count($catmyth)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/mythology" target="_blank">పురాణగాధలు</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="sci-list">
				<?php if(count($catengg)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/engineering-marvels" target="_blank">ఇంజనీరింగ్ మార్వెల్స్</a></li>
				<?php } ?>

				<?php if(count($catspace)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/space-and-astronomy" target="_blank">స్పేస్ & ఖగోళ శాస్త్రం</a></li>
				<?php } ?>

				<?php if(count($catconst)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/construction-techniques" target="_blank">నిర్మాణ పద్ధతులు</a></li>
				<?php } ?>
				
				<?php if(count($catpos)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/positive-gaming" target="_blank">పాజిటివ్ గేమింగ్</a></li>
				<?php } ?>

				<?php if(count($cattech)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/tech-that-cares" target="_blank">టెక్ దట్ కేర్స్</a></li>
				<?php } ?>

				<?php if(count($catscience)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/science-wonders" target="_blank">సైన్స్ అద్భుతాలు</a></li>
				<?php } ?>

				<?php if(count($cathow)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/how-to" target="_blank">ఎలా?</a></li>
				<?php } ?>

				<?php if(count($catfarm)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/farming-innovations" target="_blank">వ్యవసాయ ఆవిష్కరణలు</a></li>
				<?php } ?>

				<?php if(count($catdefence)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/defence" target="_blank">రక్షణ</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="pets-list">
				<?php if(count($catwater)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/drinking-water-solution" target="_blank">తాగునీటి పరిష్కారం</a></li>
				<?php } ?>

				<?php if(count($catenergy)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/energy-alternatives" target="_blank">శక్తి ప్రత్యామ్నాయాలు</a></li>
				<?php } ?>

				<?php if(count($catenv)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/environment-solutions" target="_blank">పర్యావరణ పరిష్కారాలు</a></li>
				<?php } ?>

				<?php if(count($catpollutn)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/pollution-remedies" target="_blank">కాలుష్య నివారణలు</a></li>
				<?php } ?>

				<?php if(count($catsani)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/sanitation-solution" target="_blank">పారిశుధ్య పరిష్కారాలు</a></li>
				<?php } ?>

				<?php if(count($catpets)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/pets" target="_blank">పెంపుడు జంతువులు</a></li>
				<?php } ?>

				<?php if(count($catanimal)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/animal-care" target="_blank">జంతు సంరక్షణ</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="peo-list">
				<?php if(count($catwomen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/women" target="_blank">మహిళ</a></li>
				<?php } ?>

				<?php if(count($catmen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/men" target="_blank">పురుషులు</a></li>
				<?php } ?>

				<?php if(count($catdiff)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/differently-abled" target="_blank">భిన్నంగా సామర్థ్యం</a></li>
				<?php } ?>

				<?php if(count($cattrans)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/transgender" target="_blank">హిజ్రాలు</a></li>
				<?php } ?>

				<?php if(count($catlgbt)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/lgbt" target="_blank">ఎల్  జి బి టి</a></li>
				<?php } ?>

				<?php if(count($catincre)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/incredible-indians" target="_blank">ఆశ్చర్యకరమయిన భారతదేశవాసి</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="hel-list">
				<?php if(count($catmind)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/mind-and-sleep" target="_blank" target="_blank">మైండ్ & స్లీప్</a></li>
				<?php } ?>

				<?php if(count($catmed)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/medical-breakthrough" target="_blank">వైద్య పురోగతి</a></li>
				<?php } ?>

				<?php if(count($catfit)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/fitness" target="_blank">ఫిట్నెస్</a></li>
				<?php } ?>

				<?php if(count($catenab)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/enabling-specially-abled" target="_blank">ప్రత్యేకంగా చేయగలదు</a></li>
				<?php } ?>

				<?php if(count($catyoga)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/yoga" target="_blank" target="_blank">యోగ</a></li>
				<?php } ?>

				<?php if(count($catcalm)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/calm-and-meditation" target="_blank">ప్రశాంతత & ధ్యానం</a></li>
				<?php } ?>

				<?php if(count($catdeac)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/de-activating-tech" target="_blank" target="_blank">డి-యాక్టివేట్ టెక్</a></li>
				<?php } ?>

				<?php if(count($catdrug)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/drugs-as-medicine" target="_blank">ఔషధం మందులు</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="life-list">
				<?php if(count($catnatu)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/natural-beauty" target="_blank">సహజ సౌందర్యం</a></li>
				<?php } ?>

				<?php if(count($catecof)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">పర్యావరణ స్నేహపూర్వక ఫ్యాషన్</a></li>
				<?php } ?>

				<?php if(count($catkhadi)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/khadi-and-handloom" target="_blank" target="_blank">ఖాదీ & చేనేత</a></li>
				<?php } ?>

				<?php if(count($catherbs)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/herbs-and-food" target="_blank">మూలికలు & ఆహారం</a></li>
				<?php } ?>

				<?php if(count($cathome)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/homecare-and-gardening" target="_blank">ఇంటి సంరక్షణ & తోటపని</a></li>
				<?php } ?>
				
				<?php if(count($catcars)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/cars-and-bikes" target="_blank">కార్లు & బైక్‌లు</a></li>
				<?php } ?>

				<?php if(count($catsports)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/sports-and-athletics" target="_blank">క్రీడలు & అథ్లెటిక్స్</a></li>
				<?php } ?>

				<?php if(count($cattravel)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/travel-and-adventure-tech" target="_blank">ట్రావెల్ & అడ్వెంచర్ టెక్</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="edu-list">
				<?php if(count($catsmart)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/smart-cities" target="_blank">స్మార్ట్ సిటీలు</a></li>
				<?php } ?>

				<?php if(count($catsafe)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/safety-tools" target="_blank" target="_blank">భద్రతా సాధనాలు</a></li>
				<?php } ?>

				<?php if(count($catrural)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/rural-initiatives" target="_blank">గ్రామీణ కార్యక్రమాలు</a></li>
				<?php } ?>

				<?php if(count($catdisaster)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/disaster-management" target="_blank">విపత్తూ నిర్వహణ</a></li>
				<?php } ?>

				<?php if(count($catcampus)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/campus-life" target="_blank">క్యాంపస్ లైఫ్</a></li>
				<?php } ?>

				<?php if(count($cateduc)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/education-solution" target="_blank">విద్య పరిష్కారాలు</a></li>
				<?php } ?>

				<?php if(count($catcareers)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/careers-and-mentorship" target="_blank">కెరీర్లు & గురువు</a></li>
				<?php } ?>

				<?php if(count($catfinan)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/financial-learnings" target="_blank">ఆర్థిక అభ్యాసం</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr-child">
			<ul class="child-list">
				<?php if(count($catlabor)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/child-labour" target="_blank">పిల్లలు & వృద్ధులు</a></li>
				<?php } ?>

				<?php if(count($cattraffic)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/child-trafficking" target="_blank">పిల్లల అక్రమ రవాణా</a></li>
				<?php } ?>

				<?php if(count($catparent)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/parenting" target="_blank">పేరెంటింగ్</a></li>
				<?php } ?>

				<?php if(count($catelder)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>telugu/elderly" target="_blank">వృద్ధ</a></li>
				<?php } ?>
			</ul>
		</div>
		</div>
	</div>

	<hr>

	<!-- End of Header -->

