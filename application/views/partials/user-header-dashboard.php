<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

    <!-- Title -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
    <title><?php echo $title; ?></title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <style>

    body {margin:0;margin-left:213px;font-family:Arial;line-height: 1.5;}

    .sidenav {
      height: 100%;
      width: 20 0px;
      position: fixed;
      z-index: 1;
      font-family: Arial;
      left: 0;
      background-color: white;
      overflow-x: hidden;
      border-right: 1px solid #ebebeb;
    }

    .sidenav a {
      padding: 6px 10px 6px 16px;
      text-decoration: none;
      font-size: 17px;
      color: #818181;
      display: block;
    }

    .sidenav-links {
      font-size: 20px;
      margin-left: 15px;
      line-height: 2;
    }

    .sidenav a:hover {
      color: black;
    }

    .main {
      margin-left: 160px; /* Same as the width of the sidenav */
      font-size: 28px; /* Increased text to enable scrolling */
      padding: 0px 10px;
    }

    .welcome {
        font-family: Arial;
        text-align: center;
        margin-top: 20px;
        margin-bottom: 20px;
    }

    @media screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }

    .topnav {
      overflow: hidden;
      background-color: #bf4b3d;
    }

    .topnav a {
      float: right;
      display: block;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav .icon {
      display: none;
    }

    .dropdown {
      float: right;
      overflow: hidden;
    }

    .dropdown .dropbtn {
      font-size: 17px;    
      border: none;
      outline: none;
      color: white;
      padding: 26px 16px;
      background-color: inherit;
      font-family: inherit;
      margin: 0;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }

    .dropdown-content a {
      float: none;
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
      text-align: left;
    }

    .dropdown-content a:hover {
      background-color: #ddd;
      color: black;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }

    .errormessage {
      margin-top: 6px;
      margin-bottom: -10px;
      margin-left: 440px;
      color: green;
    }

    @media screen and (max-width: 600px) {
      .topnav a:not(:first-child), .dropdown .dropbtn {
        display: none;
      }
      .topnav a.icon {
        float: right;
        display: block;
      }
    }

    @media screen and (max-width: 600px) {
      .topnav.responsive {position: relative;}
      .topnav.responsive .icon {
        position: absolute;
        right: 0;
        top: 0;
      }
      .topnav.responsive a {
        float: none;
        display: block;
        text-align: left;
      }
      .topnav.responsive .dropdown {float: none;}
      .topnav.responsive .dropdown-content {position: relative;}
      .topnav.responsive .dropdown .dropbtn {
        display: block;
        width: 100%;
        text-align: left;
      }
    }

      .table-active, .table-active > th, .table-active > td {
        background-color: white;
    }

    .languagedrop {
      margin-top: auto;
      float: right;
    }
    
    .btn-secondary {
    color: grey;
    background-color: white;
    border-color: grey;
  }

  </style>

  <script type="text/javascript">
    var subcategory = {
      Artists: ["Indian Classical Dance", "Indian Classical Music", "Indian Classical Instruments", "Indian Theatre", "Indian Painters", "Indian Craftsmen", "Books, Comics & Authors", "Fun Festivals", "Archaeology", "History", "Mythology"],
      Science: ["Engineering Marvels", "Space & Astronomy", "Construction Techniques", "Positive Gaming", "Tech That Cares", "Science Wonders", "How To?", "Farming Innovations", "Defence"],
      Pets: ["Drinking Water Solution", "Energy Alternatives", "Environment Solution", "Pollution Remedies", "Sanitation Solution", "Pets", "Animal Care"],
      People: ["Women", "Men", "Differently Abled", "Transgenders", "LGBT", "Incredible Indians"],
      Health: ["Mind & Sleep", "Medical Breakthrough", "Fitness", "Enabling Specially Abled", "Yoga", "Calm & Meditation", "De-activating Tech", "Drugs as Medicine"],
      Lifestyle: ["Natural Beauty", "Eco-Friendly Fashion", "Khadi & Handloom", "Herbs & Food", "Homecare & Gardening", "Cars & Bikes", "Sports & Athletics", "Travel & Adventure Tech"],
      Education: ["Smart Cities", "Safety Tools", "Rural Initiatives", "Disaster Management", "Campus Life", "Education Solution", "Careers & Mentorship", "Financial Learnings"],
      Child: ["Child Labour", "Child Trafficking", "Parenting", "Elderly"]     
    }

    function makeSubmenu(value) {
        if (value.length == 0) document.getElementById("categorySelect").innerHTML = "<option></option>";
        else {
            var citiesOptions = "";
            for (categoryId in subcategory[value]) {
                citiesOptions += "<option>" + subcategory[value][categoryId] + "</option>";
            }
            document.getElementById("categorySelect").innerHTML = citiesOptions;
        }
    }

    function displaySelected() {
        var country = document.getElementById("category").value;
        var city = document.getElementById("categorySelect").value;
        alert(country + "\n" + city);
    }

    function resetSelection() {
        document.getElementById("category").selectedIndex = 0;
        document.getElementById("categorySelect").selectedIndex = 0;
    }
  </script>

  <script type="text/javascript">
    var subcategory2 = {
      Artists: ["Indian Classical Dance", "Indian Classical Music", "Indian Classical Instruments", "Indian Theatre", "Indian Painters", "Indian Craftsmen", "Books, Comics & Authors", "Fun Festivals", "Archaeology", "History", "Mythology"],
      Science: ["Engineering Marvels", "Space & Astronomy", "Construction Techniques", "Positive Gaming", "Tech That Cares", "Science Wonders", "How To?", "Farming Innovations", "Defence"],
      Pets: ["Drinking Water Solution", "Energy Alternatives", "Environment Solution", "Pollution Remedies", "Sanitation Solution", "Pets", "Animal Care"],
      People: ["Women", "Men", "Differently Abled", "Transgenders", "LGBT", "Incredible Indians"],
      Health: ["Mind & Sleep", "Medical Breakthrough", "Fitness", "Enabling Specially Abled", "Yoga", "Calm & Meditation", "De-activating Tech", "Drugs as Medicine"],
      Lifestyle: ["Natural Beauty", "Eco-Friendly Fashion", "Khadi & Handloom", "Herbs & Food", "Homecare & Gardening", "Cars & Bikes", "Sports & Athletics", "Travel & Adventure Tech"],
      Education: ["Smart Cities", "Safety Tools", "Rural Initiatives", "Disaster Management", "Campus Life", "Education Solution", "Careers & Mentorship", "Financial Learnings"],
      Child: ["Child Labour", "Child Trafficking", "Parenting", "Elderly"]     
    }

    function makeSubmenu2(value) {
        if (value.length == 0) document.getElementById("categorySelect2").innerHTML = "<option></option>";
        else {
            var citiesOptions = "";
            for (categoryId in subcategory2[value]) {
                citiesOptions += "<option>" + subcategory2[value][categoryId] + "</option>";
            }
            document.getElementById("categorySelect2").innerHTML = citiesOptions;
        }
    }

    function displaySelected() {
        var country = document.getElementById("category2").value;
        var city = document.getElementById("categorySelect2").value;
        alert(country + "\n" + city);
    }

    function resetSelection() {
        document.getElementById("category2").selectedIndex = 0;
        document.getElementById("categorySelect2").selectedIndex = 0;
    }
  </script>

  <script type="text/javascript">
    var subcategory3 = {
      Artists: ["Indian Classical Dance", "Indian Classical Music", "Indian Classical Instruments", "Indian Theatre", "Indian Painters", "Indian Craftsmen", "Books, Comics & Authors", "Fun Festivals", "Archaeology", "History", "Mythology"],
      Science: ["Engineering Marvels", "Space & Astronomy", "Construction Techniques", "Positive Gaming", "Tech That Cares", "Science Wonders", "How To?", "Farming Innovations", "Defence"],
      Pets: ["Drinking Water Solution", "Energy Alternatives", "Environment Solution", "Pollution Remedies", "Sanitation Solution", "Pets", "Animal Care"],
      People: ["Women", "Men", "Differently Abled", "Transgenders", "LGBT", "Incredible Indians"],
      Health: ["Mind & Sleep", "Medical Breakthrough", "Fitness", "Enabling Specially Abled", "Yoga", "Calm & Meditation", "De-activating Tech", "Drugs as Medicine"],
      Lifestyle: ["Natural Beauty", "Eco-Friendly Fashion", "Khadi & Handloom", "Herbs & Food", "Homecare & Gardening", "Cars & Bikes", "Sports & Athletics", "Travel & Adventure Tech"],
      Education: ["Smart Cities", "Safety Tools", "Rural Initiatives", "Disaster Management", "Campus Life", "Education Solution", "Careers & Mentorship", "Financial Learnings"],
      Child: ["Child Labour", "Child Trafficking", "Parenting", "Elderly"]     
    }

    function makeSubmenu3(value) {
        if (value.length == 0) document.getElementById("categorySelect3").innerHTML = "<option></option>";
        else {
            var citiesOptions = "";
            for (categoryId in subcategory3[value]) {
                citiesOptions += "<option>" + subcategory3[value][categoryId] + "</option>";
            }
            document.getElementById("categorySelect3").innerHTML = citiesOptions;
        }
    }

    function displaySelected() {
        var country = document.getElementById("category3").value;
        var city = document.getElementById("categorySelect3").value;
        alert(country + "\n" + city);
    }

    function resetSelection() {
        document.getElementById("category3").selectedIndex = 0;
        document.getElementById("categorySelect3").selectedIndex = 0;
    }
  </script>

  <script type="text/javascript">
    var subcategory4 = {
      Artists: ["Indian Classical Dance", "Indian Classical Music", "Indian Classical Instruments", "Indian Theatre", "Indian Painters", "Indian Craftsmen", "Books, Comics & Authors", "Fun Festivals", "Archaeology", "History", "Mythology"],
      Science: ["Engineering Marvels", "Space & Astronomy", "Construction Techniques", "Positive Gaming", "Tech That Cares", "Science Wonders", "How To?", "Farming Innovations", "Defence"],
      Pets: ["Drinking Water Solution", "Energy Alternatives", "Environment Solution", "Pollution Remedies", "Sanitation Solution", "Pets", "Animal Care"],
      People: ["Women", "Men", "Differently Abled", "Transgenders", "LGBT", "Incredible Indians"],
      Health: ["Mind & Sleep", "Medical Breakthrough", "Fitness", "Enabling Specially Abled", "Yoga", "Calm & Meditation", "De-activating Tech", "Drugs as Medicine"],
      Lifestyle: ["Natural Beauty", "Eco-Friendly Fashion", "Khadi & Handloom", "Herbs & Food", "Homecare & Gardening", "Cars & Bikes", "Sports & Athletics", "Travel & Adventure Tech"],
      Education: ["Smart Cities", "Safety Tools", "Rural Initiatives", "Disaster Management", "Campus Life", "Education Solution", "Careers & Mentorship", "Financial Learnings"],
      Child: ["Child Labour", "Child Trafficking", "Parenting", "Elderly"]     
    }

    function makeSubmenu4(value) {
        if (value.length == 0) document.getElementById("categorySelect4").innerHTML = "<option></option>";
        else {
            var citiesOptions = "";
            for (categoryId in subcategory4[value]) {
                citiesOptions += "<option>" + subcategory4[value][categoryId] + "</option>";
            }
            document.getElementById("categorySelect4").innerHTML = citiesOptions;
        }
    }

    function displaySelected() {
        var country = document.getElementById("category4").value;
        var city = document.getElementById("categorySelect4").value;
        alert(country + "\n" + city);
    }

    function resetSelection() {
        document.getElementById("category4").selectedIndex = 0;
        document.getElementById("categorySelect4").selectedIndex = 0;
    }
  </script>

  <script type="text/javascript">
    var subcategory5 = {
      Artists: ["Indian Classical Dance", "Indian Classical Music", "Indian Classical Instruments", "Indian Theatre", "Indian Painters", "Indian Craftsmen", "Books, Comics & Authors", "Fun Festivals", "Archaeology", "History", "Mythology"],
      Science: ["Engineering Marvels", "Space & Astronomy", "Construction Techniques", "Positive Gaming", "Tech That Cares", "Science Wonders", "How To?", "Farming Innovations", "Defence"],
      Pets: ["Drinking Water Solution", "Energy Alternatives", "Environment Solution", "Pollution Remedies", "Sanitation Solution", "Pets", "Animal Care"],
      People: ["Women", "Men", "Differently Abled", "Transgenders", "LGBT", "Incredible Indians"],
      Health: ["Mind & Sleep", "Medical Breakthrough", "Fitness", "Enabling Specially Abled", "Yoga", "Calm & Meditation", "De-activating Tech", "Drugs as Medicine"],
      Lifestyle: ["Natural Beauty", "Eco-Friendly Fashion", "Khadi & Handloom", "Herbs & Food", "Homecare & Gardening", "Cars & Bikes", "Sports & Athletics", "Travel & Adventure Tech"],
      Education: ["Smart Cities", "Safety Tools", "Rural Initiatives", "Disaster Management", "Campus Life", "Education Solution", "Careers & Mentorship", "Financial Learnings"],
      Child: ["Child Labour", "Child Trafficking", "Parenting", "Elderly"]     
    }

    function makeSubmenu5(value) {
        if (value.length == 0) document.getElementById("categorySelect5").innerHTML = "<option></option>";
        else {
            var citiesOptions = "";
            for (categoryId in subcategory5[value]) {
                citiesOptions += "<option>" + subcategory5[value][categoryId] + "</option>";
            }
            document.getElementById("categorySelect5").innerHTML = citiesOptions;
        }
    }

    function displaySelected() {
        var country = document.getElementById("category5").value;
        var city = document.getElementById("categorySelect5").value;
        alert(country + "\n" + city);
    }

    function resetSelection() {
        document.getElementById("category5").selectedIndex = 0;
        document.getElementById("categorySelect5").selectedIndex = 0;
    }
  </script>

</head>

<body onload="resetSelection()">
    <!-- Header -->
    
    <div class="sidenav">
        <a class="navbar-brand" href="<?php echo base_url();?>user/dashboard"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="175px" style="margin-left: -5px; margin-bottom: -10px;"></a>
        <hr>
        <div class="welcome">
          <center>
            <img src="<?php echo base_url();?>assets/images/logo/id-card.png" width="60" height="60" style="border-radius: 50%;">
          </center>
            <h2>Welcome <?php echo $user['first_name']; ?></h2>
        </div>

        <div class="sidenav-links">Profile Settings</div>
        <a href="<?php echo base_url();?>user/edit-profile">Edit Profile</a>
        <a href="<?php echo base_url();?>user/change-password">Change Password</a>

        <div class="sidenav-links" style="margin-top: 10px;">Videos</div>
        <a href="<?php echo base_url();?>user/view-uploaded-video">View Uploaded Videos</a>
        <a href="<?php echo base_url();?>user/editors-pick">Editor's Pick</a>
        <a href="<?php echo base_url();?>user/inspiring-people">Inspiring People</a>
        <br>
    </div>

    <div class="topnav" id="myTopnav">
      <button style="float: right; background-color: #bf4b3d; border: none; padding: 12px;">
        <a href="<?php echo base_url();?>user/logout">LOGOUT</a>
      </button>
    </div>

    <script>
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
    </script>
    <!-- End of Header -->

