	<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

	<!--Footer-->
	
	<br>

	<div class="footer-margin"></div>

	<br><br>

	<div class="container-fluid ft-div">
		<div class="row">

	  		<div class="col-sm-3">
	  			<a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>assets/images/logo/logo1.png" alt="Bharat Vaani" title="Bharat Vaani" class="ft-logo"></a>
	  		</div>
	  		
	  		<div class="col-sm-2 for-mobile">
	  			<strong>Company</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/about-us" target="_blank">About Us</a></li>
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/contact-us" target="_blank">Contact Us</li>
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/partner-with-us" target="_blank">Partner With Us</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>Careers</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/openings" target="_blank">Openings</a></li>
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/apply" target="_blank">Apply</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>Policies</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/terms-and-conditions" target="_blank">Terms & Conditions</a></li>
					<br>
					<li><a class="color-black" href="<?php echo base_url();?>home/privacy-policy" target="_blank">Privacy Policy</a></li>
				</ul>
	  		</div>
	  		
	  		<div class="col-sm-3 desktop-view">
	  			<strong>Social Media</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</ul>
	  		</div>

	  		<div class="col-sm-3 mobile-view">
	  			<strong class="marleft14">Social Media</strong>
				<ul class="foo-list foo-firsttabs marleft25">
					<br>
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					&nbsp&nbsp
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					&nbsp&nbsp
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					&nbsp&nbsp
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					&nbsp&nbsp
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</ul>
	  		</div>

		</div>
	</div>

	<br>
	<hr>

	<div class="ft-copy">	
		<p><strong>&copy;Copyright <?php echo date("Y"); ?>. All Rights Reserved</strong></p>
	</div>
	<br>
	
	<!-- Logout Modal -->
	<div class="modal" id="bv_signout">
    <div class="modal-dialog">
		<img class="geen" height="140" src="<?php echo base_url();?>assets/images/halfside.png" style="margin-left:-5% !important; position:absolute; text-align:center; margin-top:2%;" alt="off-cir">
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title"><strong>Sign Out</strong></h2>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" id="logout_frm" name="logout_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
				<div class="row">
					<div class="col-lg-12 logout-bdy">
						<p>Are you sure you want to sign out?</p>
					</div>
					<div class="col-lg-12 logout-ftr">
						<input type="submit" class="btn can-bv" value="CANCEL" class="" data-dismiss="modal">
						<input type="submit" class="btn sign-bv" value="CONFIRM" name="resnd_email" id="resnd_email" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onMouseOver="this.style.border='1px solid ##bc343a',this.style.color='#fff'">
					</div>       
				</div>
				</form>
			</div>
		</div>
	</div>
	</div>
	
	<!-- Change Password Modal -->
	<div class="modal" id="bv_change_pass">
    <div class="modal-dialog">
		<img class="geen" height="140" src="<?php echo base_url();?>assets/images/Circle_Single_1.png" style="margin-left: -9% !important; position: absolute; text-align: center; width: 30% !important; margin-top: -8%;" alt="off-cir" loading="lazy">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="margin-top:-6%;">
				<div class="logo-sign text-center">
					<img src="<?php echo base_url(); ?>assets/images/logo/logo11.png" alt="bv_logo" class="logo_style_2"/>
				</div>
				<form action="" id="logoutfrm" name="logoutfrm" class="" method="post" accept-charset="utf-8" novalidate="true">
				<div class="row">
					<div class="col-lg-12 logout-bdy" style="margin: 8% 0px; padding: 0 5%;">
						<div class="col-sm-12 error_cls"><div id="error_popup" name="error_popup"></div></div>
                        <div class="col-lg-12 login_form_textbox">
							<div class="input-group pass-wrng" style="border:1px solid #e5e5e5; border-radius:4px; margin-bottom:4%;">
								<input style="max-width:70%; width:82%; border:none !important;" class="bv-input form-control password_strength show_password" name="old_password" id="old_password" type="password" placeholder="Enter Old Password" onblur="alphanumeric(document.signup_frm.password_2)"/>
							</div>
							<!--div class="req_star_textbx sign-star"><font style="color:red; padding-left:5px;">*</font></div-->
                        </div>
						<div class="col-lg-12 login_form_textbox">
							<div class="input-group pass-wrng" style="border:1px solid #e5e5e5; border-radius:4px;">
								<input style="max-width:70%; width:82%; border:none !important;" class="bv-input form-control password_strength show_password" name="new_password" id="new_password" type="password" placeholder="Enter New Password" onblur="alphanumeric(document.signup_frm.password_2)"/>
							</div>
							<!--div class="req_star_textbx sign-star"><font style="color:red; padding-left:5px;">*</font></div-->
							<div class="hide-show-newpass">
								<span style="position: relative; left: 40%; top: -28px; color: #ccc; cursor: pointer; font-size: 12px;">SHOW</span>
							</div>
                        </div>
						<!--div class="col-lg-12 login_form_textbox" style="margin-top:-10px; margin-bottom:-12px !important;">
						<label style="font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
                        </div-->
						<div class="col-lg-12"><!--span id="pwdMeter" class="neutral"></span--></div>
						<div class="col-lg-12 login_form_textbox">
							<div class="input-group con-re" style="border:1px solid #e5e5e5; border-radius:4px;">
                                <input style="max-width:70%; width:82%; border:none !important;" class="bv-input form-control" name="new_confirm_password" id="new_confirm_password" type="password" placeholder="Confirm New Password"/>
                            </div>
							<!--div class="req_star_textbx sign-star"><font style="color:red; padding-left:5px;"> *</font></div-->
							<div class="hide-show-newconf">
								<span style="position: relative; left: 40%; top: -28px; color: #ccc; cursor: pointer; font-size: 12px;">SHOW</span>
							</div>
                        </div>
					</div>
					<div class="col-lg-12 logout-ftr">
						<input class="btn can-bv" value="CANCEL" class="" data-dismiss="modal"/>
						<input type="submit" class="btn sign-bv" value="SAVE" name="chng_pass_btn" id="chng_pass_btn" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onMouseOver="this.style.border='1px solid ##bc343a',this.style.color='#fff'">
					</div>       
				</div>
				</form>
			</div>
		</div>
		<!--img class="geen" width="170" src="<?php echo base_url();?>assets/images/Circle_Single.png" style="margin-left: 50% !important; position: initial; text-align: center; margin-top: -17%;" alt="off-cir"-->
	</div>
	</div>
	
	<!-- Deactivate Account -->
	<div class="modal" id="bv_deactive">
    <div class="modal-dialog">
		<img class="geen" height="140" src="<?php echo base_url();?>assets/images/halfside.png" style="margin-left:-5% !important; position:absolute; text-align:center; margin-top:2%;" alt="off-cir">
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title"><strong>Deactivate Account</strong></h2>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" id="deact_frm" name="deact_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
				<div class="row">
					<div class="col-lg-12 logout-bdy">
						<p>Are you sure you want to deactivate your account?</p>
					</div>
					<div class="col-lg-12 logout-ftr">
						<input type="submit" class="btn can-bv" value="YES" class="" data-dismiss="modal">
						<input type="submit" class="btn sign-bv" value="CANCEL" name="resnd_email" id="resnd_email" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onMouseOver="this.style.border='1px solid ##bc343a',this.style.color='#fff'">
					</div>       
				</div>
				</form>
			</div>
		</div>
	</div>
	</div>
	
	<!-- Feedback Form -->
	<div class="modal" id="bv_feedback">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<img style="height: 120px; border-radius: 135px; margin:-12% 20% 0 38%;" src="<?php echo base_url();?>assets/images/temp.png" class="gifblank lazyloaded" alt="canter" data-ll-status="loaded">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" id="feedback_frm" name="feedback_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
				<div class="row">
					<div class="col-lg-12 logout-bdy">
						<p class="user--name"><strong>Spana Bakshi</strong></br><img style="" src="<?php echo base_url();?>assets/images/feed.png" class="gifblank" alt="feed"></p>
						<textarea id="bv_textarea" name="bv_textarea" placeholder="WRITE FEEDBACK HERE"></textarea>
					</div>
					<div class="col-lg-12 logout-ftr">
						<input type="submit" class="btn can-bv" value="CANCEL" class="" data-dismiss="modal">
						<input type="submit" class="btn sign-bv" value="SUBMIT" name="resnd_email" id="resnd_email" onmouseout="this.style.background-color='#bc343a',this.style.border='1px solid #bc343a',this.style.color='#fff'" onMouseOver="this.style.border='1px solid ##bc343a',this.style.color='#fff'">
					</div>       
				</div>
				</form>
			</div>
		</div>
	</div>
	</div>
	<!-- Feedback Form -->
	
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		
		$('.hide-show-newpass').show();
		$('.hide-show-newpass span').addClass('show')
					  
		$('.hide-show-newpass span').click(function(){
			if( $(this).hasClass('show') ) {
				//$(this).text('Hide');
				$(this).html('HIDE');
				$('#new_password').attr('type','text');
				$(this).removeClass('show');
			} else {
				$(this).html('SHOW');
				$('#new_password').attr('type','password');
				$(this).addClass('show');
			}
		});
		
		$('.hide-show-newconf').show();
		$('.hide-show-newconf span').addClass('show')
					  
		$('.hide-show-newconf span').click(function(){
			if( $(this).hasClass('show') ) {
				//$(this).text('Hide');
				$(this).html('HIDE');
				$('#new_confirm_password').attr('type','text');
				$(this).removeClass('show');
			} else {
				$(this).html('SHOW');
				$('#new_confirm_password').attr('type','password');
				$(this).addClass('show');
			}
		});
	});
	</script>
	<script type="text/javascript">
		function myFunction() {
			var myVar = setTimeout(showPage, 1000);
		}
		function showPage() {
		$("#popUpMain").css("display","block");
		}
		if(!localStorage.getItem("visted")){
			myFunction();
		   	localStorage.setItem("visted",true);
		}
	</script>
	
	<script>
		$(document).ready(function() {
			$(".categories-header1").hide();	
			$(".cat-name").click(function() {
			
				if($(this).hasClass("active")) {
					$(this).removeClass("active");
					$(".categories-header1").hide();
					//$(".search-contain").show();
				}
				else {
					$(".categories-header1").show();
					$(this).addClass("active");
					//$(".search-contain").hide();
				}
			});
			
			$(document).click(function (event) {
				var clickover = $(event.target);
				var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
				if (_opened === true && !clickover.hasClass("navbar-toggle")) {
					$("button.navbar-toggle").click();
				}
			});
		});
	</script>
	
	<script>
		$(document).ready(function(){

			$(".bv-catgr-at").hover(function(){
				$(this).css("border-bottom","5px solid #ff6600");
				$(".art-list li a").css("color","#ff6600","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".art-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-sci").hover(function(){
				$(this).css("border-bottom","5px solid #6600cc");
				$(".sci-list li a").css("color", "#6600cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".sci-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-pets").hover(function(){
				$(this).css("border-bottom","5px solid #33cc33");
				$(".pets-list li a").css("color", "#33cc33","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".pets-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-peo").hover(function(){
				$(this).css("border-bottom","5px solid #ff33cc");
				$(".peo-list li a").css("color", "#ff33cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".peo-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-hel").hover(function(){
				$(this).css("border-bottom","5px solid #333399");
				$(".hel-list li a").css("color", "#333399","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".hel-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-life").hover(function(){
				$(this).css("border-bottom","5px solid #990073");
				$(".life-list li a").css("color", "#990073","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".life-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-edu").hover(function(){
				$(this).css("border-bottom","5px solid #66ccff");
				$(".edu-list li a").css("color", "#66ccff","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".edu-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-chld").hover(function(){
				$(this).css("border-bottom","5px solid #ff9900");
				$(".child-list li a").css("color", "#ff9900","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".child-list li a").css("color", "#fff","!important");
			});	
		});
	</script>
	
	<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-149366484-1', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- End Google Analytics -->

	<script type="text/javascript">
		$(window).load(function() {$(".loader").fadeOut("slow");});
	</script>
	<!-- End of Footer -->
</body>
</html>