<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>bangla"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">বিভাগ<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>bangla/submit-stories">গল্প জমা দিন</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>bangla/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>bangla/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">বাংলা</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#">বিভাগ<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link sub-stories" href="<?php echo base_url();?>bangla/submit-stories">গল্প জমা দিন</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>bangla/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>bangla/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">বাংলা</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="categories-header1" style="margin-top: 7px; position: absolute; background-color: #fff; width: 99%; z-index: 1; display: none;">
		<hr style="margin-top: -8px !important;"> 
		<div class="row desktop-view" style="margin-top: 2%; line-height: 1.5;">

			<!--a href="<?php echo base_url();?>bangla/art-culture-and-artists">
				<div class="bv-catgr bv-catgr-at" style="margin-left: 30px;">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50" alt="art">
					<p style="margin-left: -20px !important;" class="cat-name-art">শিল্প, সংস্কৃতি এবং শিল্পী</p>
				</div>
			</a-->

			<a href="<?php echo base_url();?>bangla/science-and-tech">
				<div class="bv-catgr bv-catgr-sci">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
					<p style="margin-left: 15px !important;" class="cat-name-sci">বিজ্ঞান ও প্রযুক্তি</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/pets-and-environment">
				<div class="bv-catgr bv-catgr-pets">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
					<p class="cat-name-pet">পোষ‍্য এবং পরিবেশ</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/people">
				<div class="bv-catgr bv-catgr-peo">
					<img style="margin-left: 38px !important;" src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
					<p style="margin-left: 40px !important;" class="cat-name-peo">জনগণ</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/health">
				<div class="bv-catgr bv-catgr-hel">
					<img style="margin-left: 34px !important;" src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
					<p style="margin-left: 40px !important;" class="cat-name-helt">স্বাস্থ্য</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/lifestyle-and-outdoors">
				<div class="bv-catgr bv-catgr-life">
					<img style="margin-left: 32px !important;" src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
					<p style="margin-left: -5px !important;" class="cat-name-life">জীবনচর্যা এবং বাড়ির বাইরের জীবন</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/education-and-development">
				<div class="bv-catgr bv-catgr-edu">
					<img style="margin-left: 30px !important;" src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
					<p style="margin-left: 0px !important;" class="cat-name-edu">শিক্ষা এবং উন্নয়ন</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>bangla/child-and-elderly">
				<div class="bv-catgr-child bv-catgr-chld">
					<img style="margin-left: 28px !important;" src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
					<p style="margin-left: -5px !important;" class="cat-name-child">শিশু এবং বয়ষ্ক মানুষ</p>
				</div>
			</a>
		</div>

		<!--div class="row cat-list">
			<div class="bv-catgr-art">
				<ul class="art-list">
					<?php if(count($catdance)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-classical-dance" target="_blank">ভারতীয় শাস্ত্রীয় নৃত‍্য</a></li>
					<?php } ?>

					<?php if(count($catmusic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-classical-music" target="_blank" target="_blank" target="_blank">ভারতীয় শাস্ত্রীয় সঙ্গীত</a></li>
					<?php } ?>

					<?php if(count($catinstrument)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-classical-instruments" target="_blank" target="_blank">ভারতীয় শাস্ত্রীয় বাদ‍্যযন্ত্র</a></li>
					<?php } ?>

					<?php if(count($cattheatre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-theatre" target="_blank">ভারতীয় প্রেক্ষাগৃহ</a></li>
					<?php } ?>

					<?php if(count($catpainters)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-painters" target="_blank">ভারতীয় চিত্রশিল্পী</a></li>
					<?php } ?>

					<?php if(count($catcraftsmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/indian-craftsmen" target="_blank">ভারতীয় কারিগর</a></li>
					<?php } ?>

					<?php if(count($catbca)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/books-comics-and-authors" target="_blank">বই, কমিকস্ এবং লেখক</a></li>
					<?php } ?>

					<?php if(count($catfun)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/fun-festivals" target="_blank">আনন্দের উৎসব</a></li>
					<?php } ?>

					<?php if(count($catarch)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/archaeology" target="_blank">প্রত্নবিদ‍্যা</a></li>
					<?php } ?>

					<?php if(count($cathistory)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/history" target="_blank">ইতিহাস</a></li>
					<?php } ?>

					<?php if(count($catmyth)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/mythology" target="_blank">পুরাণ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="sci-list">
					<?php if(count($catengg)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/engineering-marvels" target="_blank">ইঞ্জিনিয়ারিঙের বিস্ময়কর সৃষ্টি</a></li>
					<?php } ?>

					<?php if(count($catspace)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/space-and-astronomy" target="_blank">মহাকাশ এবং জ‍্যোতির্বিদ‍্যা</a></li>
					<?php } ?>

					<?php if(count($catconst)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/construction-techniques" target="_blank">নির্মাণকৌশল</a></li>
					<?php } ?>

					<?php if(count($catpos)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/positive-gaming" target="_blank">প্রগতিশীল গেমিং</a></li>
					<?php } ?>

					<?php if(count($cattech)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/tech-that-cares" target="_blank">প্রযুক্তি যা আপনার ব‍্যাপারে সচেতন</a></li>
					<?php } ?>

					<?php if(count($catscience)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/science-wonders" target="_blank">বিজ্ঞানের বিস্ময়</a></li>
					<?php } ?>

					<?php if(count($cathow)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/how-to" target="_blank">কিভাবে করবেন?</a></li>
					<?php } ?>

					<?php if(count($catfarm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/farming-innovations" target="_blank">কৃষির উদ্ভাবন</a></li>
					<?php } ?>

					<?php if(count($catdefence)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/defence" target="_blank">প্রতিরোধ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="pets-list">
					<?php if(count($catwater)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/drinking-water-solution" target="_blank">পানীয় জল</a></li>
					<?php } ?>

					<?php if(count($catenergy)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/energy-alternatives" target="_blank">বিকল্প শক্তি</a></li>
					<?php } ?>

					<?php if(count($catenv)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/environment-solutions" target="_blank">পরিবেশগত সমস্যা সমাধান</a></li>
					<?php } ?>

					<?php if(count($catpollutn)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/pollution-remedies" target="_blank">দূষণের প্রতিকার</a></li>
					<?php } ?>

					<?php if(count($catsani)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/sanitation-solution" target="_blank">স্বাস্থ‍্যব‍্যবস্থার সমস্যা সমাধান</a></li>
					<?php } ?>

					<?php if(count($catpets)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/pets" target="_blank">পোষ‍্য</a></li>
					<?php } ?>

					<?php if(count($catanimal)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/animal-care" target="_blank">পশুর যত্</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="peo-list">
					<?php if(count($catwomen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/women" target="_blank">নারী</a></li>
					<?php } ?>

					<?php if(count($catmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/men" target="_blank">পুরুষ</a></li>
					<?php } ?>

					<?php if(count($catdiff)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/differently-abled" target="_blank">বিশেষভাবে সক্ষম</a></li>
					<?php } ?>

					<?php if(count($cattrans)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/transgender" target="_blank">তৃতীয় লিঙ্গ</a></li>
					<?php } ?>

					<?php if(count($catlgbt)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/lgbt" target="_blank">এলজিবিটি</a></li>
					<?php } ?>

					<?php if(count($catincre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/incredible-indians" target="_blank">অসাধারণ ভারতীয়রা</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="hel-list">
					<?php if(count($catmind)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/mind-and-sleep" target="_blank" target="_blank">মন এবং ঘুম</a></li>
					<?php } ?>

					<?php if(count($catmed)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/medical-breakthrough" target="_blank">চিকিৎসায় নতুন সাফল্য</a></li>
					<?php } ?>

					<?php if(count($catfit)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/fitness" target="_blank">ফিটনেস</a></li>
					<?php } ?>

					<?php if(count($catenab)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/enabling-specially-abled" target="_blank">বিশেষভাবে সক্ষমদের সহায়তা</a></li>
					<?php } ?>

					<?php if(count($catyoga)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/yoga" target="_blank" target="_blank">যোগ</a></li>
					<?php } ?>

					<?php if(count($catcalm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/calm-and-meditation" target="_blank">শান্তি এবং ধ‍্যান</a></li>
					<?php } ?>

					<?php if(count($catdeac)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/de-activating-tech" target="_blank" target="_blank">প্রযুক্তি থেকে বিরতি</a></li>
					<?php } ?>

					<?php if(count($catdrug)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/drugs-as-medicine" target="_blank">ওষুধরূপে ড্রাগ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="life-list">
					<?php if(count($catnatu)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/natural-beauty" target="_blank">প্রাকৃতিক সৌন্দর্য</a></li>
					<?php } ?>

					<?php if(count($catecof)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">পরিবেশবান্ধব ফ‍্যাশন</a></li>
					<?php } ?>

					<?php if(count($catkhadi)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/khadi-and-handloom" target="_blank" target="_blank">খাদি এবং তাঁত</a></li>
					<?php } ?>

					<?php if(count($catherbs)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/herbs-and-food" target="_blank">খাদ্য এবং গাছগাছড়া</a></li>
					<?php } ?>

					<?php if(count($cathome)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/homecare-and-gardening" target="_blank">বাড়ির যত্ন এবং উদ‍্যানচর্চা</a></li>
					<?php } ?>

					<?php if(count($catcars)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/cars-and-bikes" target="_blank">গাড়ি এবং বাইক</a></li>
					<?php } ?>

					<?php if(count($catsports)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/sports-and-athletics" target="_blank">ক্রীড়া এবং অ্যাথলেটিকস্</a></li>
					<?php } ?>

					<?php if(count($cattravel)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/travel-and-adventure-tech" target="_blank">ভ্রমণ এবং অভিযান প্রযুক্তি</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="edu-list">
					<?php if(count($catsmart)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/smart-cities" target="_blank">স্মার্ট শহর</a></li>
					<?php } ?>

					<?php if(count($catsafe)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/safety-tools" target="_blank" target="_blank">নিরাপত্তা-সহায়ক বস্তু</a></li>
					<?php } ?>

					<?php if(count($catrural)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/rural-initiatives" target="_blank">গ্রামীণ উদ্যোগ</a></li>
					<?php } ?>

					<?php if(count($catdisaster)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/disaster-management" target="_blank">বিপর্যয় ব‍্যবস্থাপনা</a></li>
					<?php } ?>

					<?php if(count($catcampus)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/campus-life" target="_blank">ক‍্যাম্পাসের জীবন</a></li>
					<?php } ?>

					<?php if(count($cateduc)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/education-solution" target="_blank">শিক্ষা-সংক্রান্ত সমস্যা সমাধান</a></li>
					<?php } ?>

					<?php if(count($catcareers)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/careers-and-mentorship" target="_blank">কেরিয়ার এবং পরামর্শ</a></li>
					<?php } ?>

					<?php if(count($catfinan)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/financial-learnings" target="_blank">আর্থিক শিক্ষা</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr-child">
				<ul class="child-list">
					<?php if(count($catlabor)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/child-labour" target="_blank">শিশুশ্রম</a></li>
					<?php } ?>

					<?php if(count($cattraffic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/child-trafficking" target="_blank">শিশু পাচার</a></li>
					<?php } ?>

					<?php if(count($catparent)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/parenting" target="_blank">বাবা-মার দায়িত্ব পালন</a></li>
					<?php } ?>

					<?php if(count($catelder)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>bangla/elderly" target="_blank">বয়ষ্ক মানুষ</a></li>
					<?php } ?>
				</ul>
			</div>
		</div-->

	</div>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>bangla/submit-stories">গল্প জমা দিন</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 145px !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">বাংলা</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>

	<!-- End of Header -->

