	<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

	<!--Footer-->
	
	<br>

	<div class="footer-margin"></div>

	<br><br>

	<div class="container-fluid" style="font-family: Arial">
		<div class="row">

	  		<div class="col-sm-3">
	  			<a href="<?php echo base_url();?>tamil"> <img src="<?php echo base_url();?>assets/images/logo/logo1.png" alt="Bharat Vaani" title="Bharat Vaani" style="width:240px; margin-left:5px; margin:-5px 0 15px 45px;"></a>
	  		</div>
	  		
	  		<div class="col-sm-2 for-mobile">
	  			<strong>கம்பெனி</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/about-us" target="_blank">எங்களைப் பற்றி</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/contact-us" target="_blank">எங்களை தொடர்பு கொள்ள</li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/partner-with-us" target="_blank">எங்களுடன் இணைந்து பணியாற்ற</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>வேலைவாய்ப்புகள்</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/openings" target="_blank">துளைகள்</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/apply" target="_blank">விண்ணப்பிக்கவும்</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>கொள்கைகள்</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/terms-and-conditions" target="_blank">விதிமுறைகளும் நிபந்தனைகளும்</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>tamil/privacy-policy" target="_blank">தனியுரிமைக் கொள்கை</a></li>
				</ul>
	  		</div>
	  		
	  		<div class="col-sm-3 desktop-view">
	  			<strong>சமூக ஊடகங்கள்</strong>
				<ul class="foo-list foo-firsttabs">
					<br>
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGgptb6PohYC-LmohttXsPM" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
					<a href="https://www.instagram.com/bharatvaanitamil" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
					<a href="https://www.facebook.com/bharatvaanitamil" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</ul>
	  		</div>

	  		<div class="col-sm-3 mobile-view">
	  			<strong style="margin-left: 14px;">Social Media</strong>
				<ul class="foo-list foo-firsttabs" style="margin-left: 25px;">
					<br>
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGgptb6PohYC-LmohttXsPM" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					&nbsp&nbsp
					<a href="https://www.instagram.com/bharatvaanitamil" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					&nbsp&nbsp
					<a href="https://www.facebook.com/bharatvaanitamil" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					&nbsp&nbsp
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					&nbsp&nbsp
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</ul>
	  		</div>

		</div>
	</div>

	<br>
	<hr>

	<div style="text-align: center; margin-top: 10px; font-family: Arial;">	
		<p style="font-family:Arial; font-size:15px; color: grey;"><strong>&copy;Copyright <?php echo date("Y"); ?>. All Rights Reserved</strong></p>
	</div>
	<br>
	<!--
	<script type="text/javascript">
		$(document).ready(function() {
			setTimeout(function() {
				$('#popUpMain').css('display', 'block') }, 5000);
		}); 

		$('.submit').click(function() {
			$('#popUpMain').css('display', 'none');
		});
	</script>
	-->
	<script>
		$(document).ready(function() {
			$(".categories-header1").hide();	
			$(".cat-name").click(function() {
			
				if($(this).hasClass("active")) {
					$(this).removeClass("active");
					$(".categories-header1").hide();
					//$(".search-contain").show();
				}
				else {
					$(".categories-header1").show();
					$(this).addClass("active");
					//$(".search-contain").hide();
				}
			});
			
			$(document).click(function (event) {
				var clickover = $(event.target);
				var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
				if (_opened === true && !clickover.hasClass("navbar-toggle")) {
					$("button.navbar-toggle").click();
				}
			});
		});
	</script>
	
	<script>
		$(document).ready(function(){

			$(".bv-catgr-at").hover(function(){
				$(this).css("border-bottom","5px solid #ff6600");
				$(".art-list li a").css("color","#ff6600","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".art-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-sci").hover(function(){
				$(this).css("border-bottom","5px solid #6600cc");
				$(".sci-list li a").css("color", "#6600cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".sci-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-pets").hover(function(){
				$(this).css("border-bottom","5px solid #33cc33");
				$(".pets-list li a").css("color", "#33cc33","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".pets-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-peo").hover(function(){
				$(this).css("border-bottom","5px solid #ff33cc");
				$(".peo-list li a").css("color", "#ff33cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".peo-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-hel").hover(function(){
				$(this).css("border-bottom","5px solid #333399");
				$(".hel-list li a").css("color", "#333399","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".hel-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-life").hover(function(){
				$(this).css("border-bottom","5px solid #990073");
				$(".life-list li a").css("color", "#990073","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".life-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-edu").hover(function(){
				$(this).css("border-bottom","5px solid #66ccff");
				$(".edu-list li a").css("color", "#66ccff","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".edu-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-chld").hover(function(){
				$(this).css("border-bottom","5px solid #ff9900");
				$(".child-list li a").css("color", "#ff9900","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".child-list li a").css("color", "#fff","!important");
			});	
		});
	</script>

	<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-149366484-1', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- End Google Analytics -->

	<script type="text/javascript">
		$(window).load(function() {$(".loader").fadeOut("slow");});
	</script>
	<!-- End of Footer -->

</body>
</html>