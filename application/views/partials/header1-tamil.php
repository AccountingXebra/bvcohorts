<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>tamil"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">பிரிவுகள்<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>tamil/submit-stories">கதைகளை அனுப்புங்கள்</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>tamil/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>tamil/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">தமிழ்</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#">பிரிவுகள்<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link sub-stories" href="<?php echo base_url();?>tamil/submit-stories">கதைகளை அனுப்புங்கள்</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>tamil/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>tamil/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">தமிழ்</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="categories-header1" style="margin-top: 7px; position: absolute; background-color: #fff; width: 99%; z-index: 1; display: none;">
		<hr style="margin-top: -8px !important;"> 
		<div class="row desktop-view" style="margin-top: 2%; line-height: 1.5;">

			<!--a href="<?php echo base_url();?>tamil/art-culture-and-artists">
				<div class="bv-catgr bv-catgr-at" style="margin-left: 30px;">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50" alt="art">
					<p style="margin-left: -10px !important;" class="cat-name-art">கலை, கலாச்சாரம்  கலைஞர்கள்</p>
				</div>
			</a-->

			<a href="<?php echo base_url();?>tamil/science-and-tech">
				<div class="bv-catgr bv-catgr-sci">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
					<p style="margin-left: -7px !important;" class="cat-name-sci">அறிவியல் மற்றும் தொழில்நுட்பம்</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/pets-and-environment">
				<div class="bv-catgr bv-catgr-pets">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
					<p style="margin-left: -17px !important;" class="cat-name-pet">செல்லப்பிராணிகள் மற்றும் சுற்றுச்சூழல்</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/people">
				<div class="bv-catgr bv-catgr-peo">
					<img style="margin-left: 38px !important;" src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
					<p style="margin-left: 38px !important;" style="margin-left: 40px !important;" class="cat-name-peo">மக்கள்</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/health">
				<div class="bv-catgr bv-catgr-hel">
					<img style="margin-left: 34px !important;" src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
					<p style="margin-left: 21px !important;" class="cat-name-helt">சுகாதாரம்</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/lifestyle-and-outdoors">
				<div class="bv-catgr bv-catgr-life">
					<img style="margin-left: 32px !important;" src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
					<p style="margin-left: -12px !important;" class="cat-name-life">வாழ்க்கை முறை & வெளிப்புறம்</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/education-and-development">
				<div class="bv-catgr bv-catgr-edu">
					<img style="margin-left: 30px !important;" src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
					<p style="margin-left: 5px !important;" class="cat-name-edu">கல்வி மற்றும் மேம்பாடு</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>tamil/child-and-elderly">
				<div class="bv-catgr-child bv-catgr-chld">
					<img style="margin-left: 28px !important;" src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
					<p style="margin-left: -22px !important;" class="cat-name-child">குழந்தைகள் மற்றும் முதியவர்கள்</p>
				</div>
			</a>
		</div>

		<!--div class="row cat-list">
			<div class="bv-catgr-art">
				<ul class="art-list">
					<?php if(count($catdance)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-classical-dance" target="_blank">இந்தியாவின் பண்டைய நாட்டியங்கள்</a></li>
					<?php } ?>

					<?php if(count($catmusic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-classical-music" target="_blank" target="_blank" target="_blank">இந்தியாவின் பண்டைய இசை</a></li>
					<?php } ?>

					<?php if(count($catinstrument)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-classical-instruments" target="_blank" target="_blank">இந்தியாவின் பண்டைய இசை கருவிகள்</a></li>
					<?php } ?>

					<?php if(count($cattheatre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-theatre" target="_blank">இந்தியாவின் நாடகக் கலை</a></li>
					<?php } ?>

					<?php if(count($catpainters)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-painters" target="_blank">இந்தியாவின் ஓவியர்கள்</a></li>
					<?php } ?>

					<?php if(count($catcraftsmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/indian-craftsmen" target="_blank">இந்தியாவின் கைவினைக் கலைஞர்கள்</a></li>
					<?php } ?>

					<?php if(count($catbca)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/books-comics-and-authors" target="_blank">நூல்கள், நகைச்சுவை சித்திரங்கள், எழுத்தாளர்கள்</a></li>
					<?php } ?>

					<?php if(count($catfun)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/fun-festivals" target="_blank">வேடிக்கை விநோத நிகழ்ச்சிகள்</a></li>
					<?php } ?>

					<?php if(count($catarch)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/archaeology" target="_blank">தொல்லியல் துறை</a></li>
					<?php } ?>

					<?php if(count($cathistory)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/history" target="_blank">வரலாறு</a></li>
					<?php } ?>

					<?php if(count($catmyth)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/mythology" target="_blank">புராணங்கள்</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="sci-list">
					<?php if(count($catengg)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/engineering-marvels" target="_blank">பொறியியல் அற்புதங்கள்</a></li>
					<?php } ?>

					<?php if(count($catspace)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/space-and-astronomy" target="_blank">விண்வெளி மற்றும் வானியல்</a></li>
					<?php } ?>

					<?php if(count($catconst)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/construction-techniques" target="_blank">கட்டுமான நுட்பங்கள்</a></li>
					<?php } ?>

					<?php if(count($catpos)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/positive-gaming" target="_blank">நேர்மறை விளையாட்டுக்கள்</a></li>
					<?php } ?>

					<?php if(count($cattech)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/tech-that-cares" target="_blank">அக்கறை கொண்ட தொழில்நுட்பங்கள்</a></li>
					<?php } ?>

					<?php if(count($catscience)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/science-wonders" target="_blank">அறிவியல் அதிசயங்கள்</a></li>
					<?php } ?>

					<?php if(count($cathow)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/how-to" target="_blank">எப்படி?</a></li>
					<?php } ?>

					<?php if(count($catfarm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/farming-innovations" target="_blank">விவசாய கண்டுபிடிப்புகள்</a></li>
					<?php } ?>

					<?php if(count($catdefence)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/defence" target="_blank">பாதுகாப்புத்துறை</a></li>	
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="pets-list">
					<?php if(count($catwater)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/drinking-water-solution" target="_blank">குடிநீர் தீர்வு</a></li>
					<?php } ?>

					<?php if(count($catenergy)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/energy-alternatives" target="_blank">எரிபொருள் மாற்று தீர்வுகள்</a></li>
					<?php } ?>

					<?php if(count($catenv)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/environment-solutions" target="_blank">சுற்றுச்சூழல் தீர்வுகள்</a></li>
					<?php } ?>

					<?php if(count($catpollutn)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/pollution-remedies" target="_blank">மாசு தீர்வுகள்</a></li>
					<?php } ?>

					<?php if(count($catsani)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/sanitation-solution" target="_blank">துப்புரவு தீர்வுகள்</a></li>
					<?php } ?>

					<?php if(count($catpets)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/pets" target="_blank">செல்லப்பிராணிகள்</a></li>
					<?php } ?>

					<?php if(count($catanimal)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/animal-care" target="_blank">விலங்கு பராமரிப்பு</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="peo-list">
					<?php if(count($catwomen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/women" target="_blank">பெண்கள்</a></li>
					<?php } ?>

					<?php if(count($catmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/men" target="_blank">ஆண்கள்</a></li>
					<?php } ?>

					<?php if(count($catdiff)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/differently-abled" target="_blank">மாற்றுத்திறனாளி</a></li>
					<?php } ?>

					<?php if(count($cattrans)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/transgender" target="_blank">வேற்று பாலினத்தவர்கள்</a></li>
					<?php } ?>

					<?php if(count($catlgbt)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/lgbt" target="_blank">(எல்.ஜி.பி.டி) LGBT</a></li>
					<?php } ?>

					<?php if(count($catincre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/incredible-indians" target="_blank">ஆச்சரியமூட்டும் இந்தியர்கள்</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="hel-list">
					<?php if(count($catmind)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/mind-and-sleep" target="_blank" target="_blank">மனம் & தூக்கம்</a></li>
					<?php } ?>

					<?php if(count($catmed)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/medical-breakthrough" target="_blank">மருத்துவ முன்னேற்றம்</a></li>
					<?php } ?>

					<?php if(count($catfit)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/fitness" target="_blank">உடற்பயிற்சி</a></li>
					<?php } ?>

					<?php if(count($catenab)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/enabling-specially-abled" target="_blank">மாற்றுத்திறனாளிகள் முன்னேற்றம்</a></li>
					<?php } ?>

					<?php if(count($catyoga)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/yoga" target="_blank" target="_blank">யோகா</a></li>
					<?php } ?>

					<?php if(count($catcalm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/calm-and-meditation" target="_blank">அமைதி & தியானம்</a></li>
					<?php } ?>

					<?php if(count($catdeac)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/de-activating-tech" target="_blank" target="_blank">தொழில்நுட்பத்தை செயலிழக்கச் செய்தல்</a></li>
					<?php } ?>

					<?php if(count($catdrug)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/drugs-as-medicine" target="_blank">மருந்தாக மருந்துகள்</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="life-list">
					<?php if(count($catnatu)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/natural-beauty" target="_blank">இயற்கை அழகு</a></li>
					<?php } ?>

					<?php if(count($catecof)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">சுற்றுச்சூழலுக்கு உகந்த உடைகள்</a></li>
					<?php } ?>

					<?php if(count($catkhadi)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/khadi-and-handloom" target="_blank" target="_blank">காதி & கைத்தறி</a></li>
					<?php } ?>

					<?php if(count($catherbs)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/herbs-and-food" target="_blank">மூலிகைகள் & உணவு</a></li>
					<?php } ?>

					<?php if(count($cathome)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/homecare-and-gardening" target="_blank">வீட்டு பராமரிப்பு மற்றும் தோட்டக்கலை</a></li>
					<?php } ?>

					<?php if(count($catcars)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/cars-and-bikes" target="_blank">கார்கள் & பைக்குகள்</a></li>
					<?php } ?>

					<?php if(count($catsports)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/sports-and-athletics" target="_blank">விளையாட்டு மற்றும் தடகளம்</a></li>
					<?php } ?>

					<?php if(count($cattravel)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/travel-and-adventure-tech" target="_blank">பயணம் & சாகச  நுட்பங்கள்</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="edu-list">
					<?php if(count($catsmart)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/smart-cities" target="_blank">ஸ்மார்ட் நகரங்கள்</a></li>
					<?php } ?>

					<?php if(count($catsafe)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/safety-tools" target="_blank" target="_blank">பாதுகாப்பு கருவிகள்</a></li>
					<?php } ?>

					<?php if(count($catrural)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/rural-initiatives" target="_blank">கிராமப்புற முயற்சிகள்</a></li>
					<?php } ?>

					<?php if(count($catdisaster)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/disaster-management" target="_blank">பேரிடர் மேலாண்மை</a></li>
					<?php } ?>

					<?php if(count($catcampus)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/campus-life" target="_blank">வளாக வாழ்க்கை</a></li>
					<?php } ?>

					<?php if(count($cateduc)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/education-solution" target="_blank">கல்வி தீர்வுகள்</a></li>
					<?php } ?>

					<?php if(count($catcareers)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/careers-and-mentorship" target="_blank">தொழில் மற்றும் வழிகாட்டல்</a></li>
					<?php } ?>

					<?php if(count($catfinan)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/financial-learnings" target="_blank">நிதியறிவு கற்றல்</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr-child">
				<ul class="child-list">
					<?php if(count($catlabor)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/child-labour" target="_blank">குழந்தை தொழிலாளி</a></li>
					<?php } ?>

					<?php if(count($cattraffic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/child-trafficking" target="_blank">சிறுவர் கடத்தல்</a></li>
					<?php } ?>

					<?php if(count($catparent)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/parenting" target="_blank">பெற்றோர்</a></li>
					<?php } ?>

					<?php if(count($catelder)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>tamil/elderly" target="_blank">முதியோர்</a></li>
					<?php } ?>
				</ul>
			</div>
		</div-->

	</div>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>tamil/submit-stories">கதைகளை அனுப்புங்கள்</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 139px !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">தமிழ்</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>

	<!-- End of Header -->

