<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>hindi"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">श्रेणियाँ<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>hindi/submit-stories">कहानियाँ भेजें</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>hindi/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>hindi/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">हिन्दी</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#">श्रेणियाँ<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link sub-stories" href="<?php echo base_url();?>hindi/submit-stories">कहानियाँ भेजें</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>hindi/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>hindi/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">हिन्दी</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="categories-header1" style="margin-top: 7px; position: absolute; background-color: #fff; width: 99%; z-index: 1; display: none;">
		
		<hr style="margin-top: -8px !important;"> 

		<div class="row desktop-view" style="margin-top: 2%; line-height: 1.5;">

			<!--a href="<?php echo base_url();?>hindi/art-culture-and-artists">
				<div class="bv-catgr bv-catgr-at" style="margin-left: 30px;">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50" alt="art">
					<p style="margin-left: -13px !important;" class="cat-name-art">कला, संस्कृति और कलाकार</p>
				</div>
			</a-->

			<a href="<?php echo base_url();?>hindi/science-and-tech">
				<div class="bv-catgr bv-catgr-sci">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
					<p style="margin-left: 7px !important;" class="cat-name-sci">विज्ञान और तकनीक</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/pets-and-environment">
				<div class="bv-catgr bv-catgr-pets">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
					<p style="margin-left: -15px !important;" class="cat-name-pet">पालतू जानवर और पर्यावरण</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/people">
				<div class="bv-catgr bv-catgr-peo">
					<img style="margin-left: 38px !important;" src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
					<p style="margin-left: 50px !important;" class="cat-name-peo">लोग</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/health">
				<div class="bv-catgr bv-catgr-hel">
					<img style="margin-left: 34px !important;" src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
					<p style="margin-left: 40px !important;" class="cat-name-helt">स्वास्थ्य</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/lifestyle-and-outdoors">
				<div class="bv-catgr bv-catgr-life">
					<img style="margin-left: 32px !important;" src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
					<p style="margin-left: 0px !important;" class="cat-name-life">जीवन शैली और बाहर</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/education-and-development">
				<div class="bv-catgr bv-catgr-edu">
					<img style="margin-left: 30px !important;" src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
					<p style="margin-left: 7px !important;" class="cat-name-edu">शिक्षा और विकास</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>hindi/child-and-elderly">
				<div class="bv-catgr-child bv-catgr-chld">
					<img style="margin-left: 28px !important;" src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
					<p style="margin-left: 16px !important;" class="cat-name-child">बच्चे और बुजुर्ग</p>
				</div>
			</a>
		</div>

		<!--div class="row cat-list">
			<div class="bv-catgr-art">
				<ul class="art-list">
					<?php if(count($catdance)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-classical-dance" target="_blank">भारतीय शास्त्रीय नृत्य</a></li>
					<?php } ?>

					<?php if(count($catmusic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-classical-music" target="_blank" target="_blank" target="_blank">भारतीय शास्त्रीय संगीत</a></li>
					<?php } ?>

					<?php if(count($catinstrument)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-classical-instruments" target="_blank" target="_blank">भारतीय शास्त्रीय वाद्ययंत्र</a></li>
					<?php } ?>

					<?php if(count($cattheatre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-theatre" target="_blank">भारतीय रंगमंच</a></li>
					<?php } ?>

					<?php if(count($catpainters)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-painters" target="_blank">भारतीय चित्रकार</a></li>	
					<?php } ?>

					<?php if(count($catcraftsmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/indian-craftsmen" target="_blank">भारतीय शिल्पकार</a></li>
					<?php } ?>

					<?php if(count($catbca)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/books-comics-and-authors" target="_blank">किताबें, कॉमिक्स और लेखक</a></li>
					<?php } ?>

					<?php if(count($catfun)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/fun-festivals" target="_blank">मजेदार त्योहार</a></li>
					<?php } ?>

					<?php if(count($catarch)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/archaeology" target="_blank">पुरातत्त्व</a></li>
					<?php } ?>

					<?php if(count($cathistory)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/history" target="_blank">इतिहास</a></li>
					<?php } ?>

					<?php if(count($catmyth)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/mythology" target="_blank">पौराणिक कथा</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="sci-list">
					<?php if(count($catengg)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/engineering-marvels" target="_blank">इंजीनियरिंग चमत्कार</a></li>
					<?php } ?>

					<?php if(count($catspace)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/space-and-astronomy" target="_blank">अंतरिक्ष और खगोल विज्ञान</a></li>
					<?php } ?>

					<?php if(count($catconst)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/construction-techniques" target="_blank">निर्माण तकनीक</a></li>
					<?php } ?>

					<?php if(count($catpos)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/positive-gaming" target="_blank">सकारात्मक गेमिंग</a></li>
					<?php } ?>

					<?php if(count($cattech)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/tech-that-cares" target="_blank">तकनीकी देखभाल</a></li>
					<?php } ?>

					<?php if(count($catscience)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/science-wonders" target="_blank">विज्ञान का चमत्कार</a></li>
					<?php } ?>

					<?php if(count($cathow)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/how-to" target="_blank">कैसे?</a></li>
					<?php } ?>

					<?php if(count($catfarm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/farming-innovations" target="_blank">खेती संबंधी आविष्कार</a></li>
					<?php } ?>

					<?php if(count($catdefence)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/defence" target="_blank">रक्षा</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="pets-list">
					<?php if(count($catwater)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/drinking-water-solution" target="_blank">पेयजल समाधान</a></li>
					<?php } ?>

					<?php if(count($catenergy)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/energy-alternatives" target="_blank">ऊर्जा विकल्प</a></li>
					<?php } ?>

					<?php if(count($catenv)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/environment-solutions" target="_blank">पर्यावरण समाधान</a></li>
					<?php } ?>

					<?php if(count($catpollutn)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/pollution-remedies" target="_blank">प्रदूषण के उपाय</a></li>
					<?php } ?>

					<?php if(count($catsani)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/sanitation-solution" target="_blank">स्वच्छता समाधान</a></li>
					<?php } ?>

					<?php if(count($catpets)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/pets" target="_blank">पालतू जानवर</a></li>
					<?php } ?>

					<?php if(count($catanimal)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/animal-care" target="_blank">पशु देखभाल</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="peo-list">
					<?php if(count($catwomen)>0){ ?> 
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/women" target="_blank">महिलाओं</a></li>
					<?php } ?>

					<?php if(count($catmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/men" target="_blank">पुरुषों</a></li>
					<?php } ?>

					<?php if(count($catdiff)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/differently-abled" target="_blank">निःशक्तजन</a></li>
					<?php } ?>

					<?php if(count($cattrans)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/transgender" target="_blank">विपरीतलिंगी</a></li>
					<?php } ?>

					<?php if(count($catlgbt)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/lgbt" target="_blank">एलजीबीटी</a></li>
					<?php } ?>

					<?php if(count($catincre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/incredible-indians" target="_blank">अविश्वसनीय भारतीय</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="hel-list">
					<?php if(count($catmind)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/mind-and-sleep" target="_blank" target="_blank">मन और नींद</a></li>
					<?php } ?>

					<?php if(count($catmed)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/medical-breakthrough" target="_blank">चिकित्सा सफलता</a></li>
					<?php } ?>

					<?php if(count($catfit)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/fitness" target="_blank">फिटनेस</a></li>
					<?php } ?>

					<?php if(count($catenab)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/enabling-specially-abled" target="_blank">विशेष रूप से सक्षम बनाना</a></li>
					<?php } ?>

					<?php if(count($catyoga)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/yoga" target="_blank" target="_blank">योग</a></li>	
					<?php } ?>

					<?php if(count($catcalm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/calm-and-meditation" target="_blank">शांत और ध्यान</a></li>
					<?php } ?>

					<?php if(count($catdeac)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/de-activating-tech" target="_blank" target="_blank">तकनीक को निष्क्रिय करना</a></li>
					<?php } ?>

					<?php if(count($catdrug)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/drugs-as-medicine" target="_blank">नशीले ड्रग्स जो दावा है</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="life-list">
					<?php if(count($catnatu)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/natural-beauty" target="_blank">प्राकृतिक सौंदर्य</a></li>
					<?php } ?>

					<?php if(count($catecof)>0){ ?> 	
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">पर्यावरण के अनुकूल फैशन</a></li>
					<?php } ?>

					<?php if(count($catkhadi)>0){ ?>
					<li><a href="<?php echo base_url();?>hindi/khadi-and-handloom" target="_blank" target="_blank">खादी और हथकरघा</a></li>
					<?php } ?>

					<?php if(count($catherbs)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/herbs-and-food" target="_blank">जड़ी बूटियों और भोजन</a></li>
					<?php } ?>

					<?php if(count($cathome)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/homecare-and-gardening" target="_blank">घर की देखभाल और बागवानी</a></li>
					<?php } ?>

					<?php if(count($catcars)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/cars-and-bikes" target="_blank">कार और बाइक</a></li>
					<?php } ?>

					<?php if(count($catsports)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/sports-and-athletics" target="_blank">खेल और एथलेटिक्स</a></li>
					<?php } ?>

					<?php if(count($cattravel)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/travel-and-adventure-tech" target="_blank">यात्रा और साहसिक तकनीक</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="edu-list">
					<?php if(count($catsmart)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/smart-cities" target="_blank">स्मार्ट सिटी</a></li>
					<?php } ?>

					<?php if(count($catsafe)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/safety-tools" target="_blank" target="_blank">सुरक्षा उपकरण</a></li>
					<?php } ?>

					<?php if(count($catrural)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/rural-initiatives" target="_blank">ग्रामीण पहल</a></li>
					<?php } ?>

					<?php if(count($catdisaster)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/disaster-management" target="_blank">आपदा प्रबंधन</a></li>
					<?php } ?>

					<?php if(count($catcampus)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/campus-life" target="_blank">कैंपस की ज़िंदगी</a></li>
					<?php } ?>

					<?php if(count($cateduc)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/education-solution" target="_blank">शिक्षा समाधान</a></li>
					<?php } ?>

					<?php if(count($catcareers)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/careers-and-mentorship" target="_blank">करियर और मेंटरशिप</a></li>
					<?php } ?>

					<?php if(count($catfinan)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/financial-learnings" target="_blank">वित्तीय शिक्षा</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr-child">
				<ul class="child-list">
					<?php if(count($catlabor)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/child-labour" target="_blank">बाल श्रम</a></li>
					<?php } ?>

					<?php if(count($cattraffic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/child-trafficking" target="_blank">बच्चों का अवैध व्यापार</a></li>
					<?php } ?>

					<?php if(count($catparent)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/parenting" target="_blank">परवरिश</a></li>
					<?php } ?>

					<?php if(count($catelder)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>hindi/elderly" target="_blank">बुज़ुर्ग</a></li>
					<?php } ?>
				</ul>
			</div>
		</div-->

	</div>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>hindi/submit-stories">कहानियाँ भेजें</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 145px !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">हिन्दी</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>

	<!-- End of Header -->

