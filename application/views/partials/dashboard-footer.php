	<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

	<!--Footer-->
	
	<br>

	<div class="footer-margin"></div>

	<br><br>

	<div class="container-fluid" style="font-family: Arial">
		<div class="row">

	  		<div class="col-sm-3">
	  			<a href="<?php echo base_url();?>home/dashboard"> <img src="<?php echo base_url();?>assets/images/logo/logo1.png" alt="Logo" style="width:240px; margin-left:5px; margin:-5px 0 15px 0;"></a>
	  		</div>
	  		
	  		<div class="col-sm-2 for-mobile">
	  			<strong>Company</strong>
				<ul class="foo-list foo-firsttabs">
					<br><br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-about-us" target="_blank">About Us</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-contact-us" target="_blank">Contact Us</li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-partner-with-us" target="_blank">Partner With Us</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>Careers</strong>
				<ul class="foo-list foo-firsttabs">
					<br><br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-openings" target="_blank">Openings</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-apply" target="_blank">Apply</a></li>
				</ul>
	  		</div>

	  		<div class="col-sm-2 for-mobile">
	  			<strong>Policies</strong>
				<ul class="foo-list foo-firsttabs">
					<br><br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-terms-and-conditions" target="_blank">Terms & Conditions</a></li>
					<br>
					<li><a style="color: black" href="<?php echo base_url();?>home/dashboard-privacy-policy" target="_blank">Privacy Policy</a></li>
				</ul>
	  		</div>
	  		
	  		<div class="col-sm-3 for-mobile">
	  			<strong>Social Media</strong>
				<ul class="foo-list foo-firsttabs">
					<br><br>
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 30.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 30.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 30.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 30.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 30.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</ul>
	  		</div>

		</div>
	</div>

	<br>
	<hr>

	<div style="text-align: center; margin-top: 10px; font-family: Arial;">	
		<p style="font-family:Arial; font-size:15px; color: grey;"><strong>&copy;Copyright 2019. All Rights Reserved</strong></p>
	</div>
	<br>
	
	<script type="text/javascript">
		function myFunction() {
		var myVar = setTimeout(showPage, 1000);
	}

		function showPage() {

		$("#popUpMain").css("display","block");
		}
		if(!localStorage.getItem("visted")){
		   myFunction();
		   localStorage.setItem("visted",true);
		}
		
	</script>
	
	<script>
		$(document).ready(function() {
			$(".categories-header1").hide();	
			$(".cat-name").click(function() {
			
				if($(this).hasClass("active")) {
					$(this).removeClass("active");
					$(".categories-header1").hide();
					//$(".search-contain").show();
				}
				else {
					$(".categories-header1").show();
					$(this).addClass("active");
					//$(".search-contain").hide();
				}
			});
			
			$(document).click(function (event) {
				var clickover = $(event.target);
				var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
				if (_opened === true && !clickover.hasClass("navbar-toggle")) {
					$("button.navbar-toggle").click();
				}
			});
		});
	</script>
	
	<script>
		$(document).ready(function(){

			$(".bv-catgr-at").hover(function(){
				$(this).css("border-bottom","5px solid #ff6600");
				$(".art-list li a").css("color","#ff6600","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".art-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-sci").hover(function(){
				$(this).css("border-bottom","5px solid #6600cc");
				$(".sci-list li a").css("color", "#6600cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".sci-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-pets").hover(function(){
				$(this).css("border-bottom","5px solid #33cc33");
				$(".pets-list li a").css("color", "#33cc33","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".pets-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-peo").hover(function(){
				$(this).css("border-bottom","5px solid #ff33cc");
				$(".peo-list li a").css("color", "#ff33cc","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".peo-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-hel").hover(function(){
				$(this).css("border-bottom","5px solid #333399");
				$(".hel-list li a").css("color", "#333399","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".hel-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-life").hover(function(){
				$(this).css("border-bottom","5px solid #990073");
				$(".life-list li a").css("color", "#990073","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".life-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-edu").hover(function(){
				$(this).css("border-bottom","5px solid #66ccff");
				$(".edu-list li a").css("color", "#66ccff","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".edu-list li a").css("color", "#fff","!important");
			});
			
			$(".bv-catgr-chld").hover(function(){
				$(this).css("border-bottom","5px solid #ff9900");
				$(".child-list li a").css("color", "#ff9900","!important");
			}, function(){
				$(this).css("border-bottom","none");
				$(".child-list li a").css("color", "#fff","!important");
			});	
		});
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!--script async src="https://www.googletagmanager.com/gtag/js?id=UA-149366484-1"></script-->
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/local-ga.js"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-149366484-1');
	</script>	
	<!-- End of Footer -->

</body>
</html>