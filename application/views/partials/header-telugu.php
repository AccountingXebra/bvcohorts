<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script async type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/embed.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<!--script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script-->
	<style type="text/css">
		.dropdown-toggle::after {
	    	margin-left: 40px !important;
		}

		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>telugu"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">కేటగిరీలు<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/submit-stories">కథనాలను సమర్పించండి</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">తెలుగు</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyFN1Jz3sR5KuHqVyAN-Vr1y" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#categories">కేటగిరీలు<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/submit-stories">కథనాలను సమర్పించండి</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>telugu/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">తెలుగు</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyFN1Jz3sR5KuHqVyAN-Vr1y" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaanitelugu" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>telugu/submit-stories">కథనాలను సమర్పించండి</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 145px !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">తెలుగు</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>
</div>

	<!-- End of Header -->

