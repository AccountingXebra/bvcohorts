<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->

    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">
		<a class="navbar-brand" href="<?php echo base_url();?>assamese/dashboard"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">শ্ৰেণীসমূহ<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>assamese/dashboard-submit-stories">লেখা প্ৰেৰণ</a>
				</li>

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome <?php echo $user['first_name']; ?></a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard-edit-account">Edit Account &nbsp</a>
						<a class="dropdown-item" href="<?php echo base_url();?>home/logout">Logout</a>
					</div>
				</li>

				<li class="nav-item dropdown cat-drop-head">

					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">অসমিয়া</a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>home/dashboard">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi/dashboard">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla/dashboard">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati/dashboard">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi/dashboard">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil/dashboard">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyG9s6vnnq0eUoN0rV9KTDku" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 30.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaaniassamese" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 30.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaaniassamese" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 30.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 30.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 30.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#">শ্ৰেণীসমূহ<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>assamese/dashboard-submit-stories">লেখা প্ৰেৰণ</a>
				</li>

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome <?php echo $user['first_name']; ?></a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard-edit-account">Edit Account &nbsp</a>
						<a class="dropdown-item" href="<?php echo base_url();?>home/logout">Logout</a>
					</div>
				</li>
				&nbsp&nbsp&nbsp&nbsp
				<li class="nav-item dropdown cat-drop-head">

					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">অসমিয়া &nbsp&nbsp&nbsp&nbsp&nbsp</a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>home/dashboard">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi/dashboard">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese/dashboard">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla/dashboard">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati/dashboard">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi/dashboard">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil/dashboard">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu/dashboard">తెలుగు</a>
					</div>
				</li>
				&nbsp&nbsp&nbsp&nbsp
				<li class="nav-item social-item" style="text-align:right;">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyG9s6vnnq0eUoN0rV9KTDku" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 30.png" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaaniassamese" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 30.png" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaaniassamese" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 30.png" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 30.png" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 30.png" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>

	</nav>
	<div class="categories-header1" style="position:absolute; background-color:#fff; width:100%; z-index:1;">
		<hr>
		<div class="row" style="margin-top:2%;">
			<div class="bv-catgr-art bv-catgr-at">
			<img src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50"></img>
			<br><br>
			<p class="cat-name-art">কলা, সংস্কৃতি, <br><br> শিল্পী</p>
		</div>

		<div class="bv-catgr bv-catgr-sci">
			<img src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
			<p class="cat-name-sci">বিজ্ঞান প্ৰযুক্তি</p>
		</div>

		<div class="bv-catgr bv-catgr-pets">
			<img src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
			<br><br>
			<p class="cat-name-pet">ঘৰচীয়া জীৱ <br><br> আৰু পৰিৱেশ</p>
		</div>

		<div class="bv-catgr bv-catgr-peo">
			<img src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
			<p class="cat-name-peo">ব্যক্তিত্ব</p>
		</div>

		<div class="bv-catgr bv-catgr-hel">
			<img src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
			<p class="cat-name-helt">স্বাস্থ্য</p>
		</div>

		<div class="bv-catgr bv-catgr-life">
			<img src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
			<p class="cat-name-life">জীৱ্নশৈলী</p>
		</div>

		<div class="bv-catgr bv-catgr-edu">
			<img src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
			<br><br>
			<p class="cat-name-edu">শিক্ষা আৰু বিকাশ</p>
		</div>

		<div class="bv-catgr-child bv-catgr-chld">
			<img src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
			<p class="cat-name-child">শিশু আৰু বৃদ্ধাৱস্থা</p>
		</div>
		</div>
		<div class="row cat-list">
			<div class="bv-catgr-art">
			<ul class="art-list">
				<?php if(count($catdance)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-classical-dance" target="_blank">ভাৰতীয় শাস্ত্ৰীয় নৃত্য</a></li>
				<?php } ?>

				<?php if(count($catmusic)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-classical-music" target="_blank" target="_blank" target="_blank">ভাৰতীয় শাস্ত্ৰীয় সংগীত</a></li>
				<?php } ?>

				<?php if(count($catinstrument)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-classical-instruments" target="_blank" target="_blank">ভাৰতীয় শাস্ত্ৰীয় বাদ্যযন্ত্ৰ</a></li>
				<?php } ?>

				<?php if(count($cattheatre)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-theatre" target="_blank">ভাৰতীয় থিয়েটাৰ</a></li>
				<?php } ?>

				<?php if(count($catpainters)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-painters" target="_blank">ভাৰতীয় চিত্ৰশিল্পী</a></li>
				<?php } ?>

				<?php if(count($catcraftsmen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/indian-craftsmen" target="_blank">ভাৰতীয় কাৰুশিল্পী</a></li>
				<?php } ?>

				<?php if(count($catbca)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/books-comics-and-authors" target="_blank">গ্ৰন্থ, কমিকচ আৰু লেখক</a></li>
				<?php } ?>

				<?php if(count($catfun)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/fun-festivals" target="_blank">আমোদ-প্ৰমোদ উৎসৱ</a></li>
				<?php } ?>

				<?php if(count($catarch)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/archaeology" target="_blank">পুৰাতত্ব</a></li>
				<?php } ?>

				<?php if(count($cathistory)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/history" target="_blank">বুৰঞ্জী</a></li>
				<?php } ?>

				<?php if(count($catmyth)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/mythology" target="_blank">আখ্যান</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="sci-list">
				<?php if(count($catengg)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/engineering-marvels" target="_blank">অভিযান্ত্ৰিক বিস্ময়</a></li>
				<?php } ?>

				<?php if(count($catspace)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/space-and-astronomy" target="_blank">মহাকাশ আৰু জ্যোতিৰ্বিদ্যা</a></li>
				<?php } ?>

				<?php if(count($catconst)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/construction-techniques" target="_blank">স্থাপত্য কৌশল</a></li>
				<?php } ?>

				<?php if(count($catpos)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/positive-gaming" target="_blank">পজিটিভ গেমিং</a></li>
				<?php } ?>

				<?php if(count($cattech)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/tech-that-cares" target="_blank">অনুকূল প্ৰযুক্তি</a></li>
				<?php } ?>

				<?php if(count($catscience)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/science-wonders" target="_blank">বিজ্ঞান বিস্ময়</a></li>
				<?php } ?>

				<?php if(count($cathow)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/how-to" target="_blank">কেনেদৰে?</a></li>
				<?php } ?>

				<?php if(count($catfarm)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/farming-innovations" target="_blank">কৃষিজাত উদ্ভাৱন</a></li>
				<?php } ?>

				<?php if(count($catdefence)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/defence" target="_blank">প্ৰতিৰক্ষা</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="pets-list">
				<?php if(count($catwater)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/drinking-water-solution" target="_blank">খোৱাপানীৰ সমস্যা সমাধান</a></li>
				<?php } ?>

				<?php if(count($catenergy)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/energy-alternatives" target="_blank">শক্তি বিকল্প</a></li>
				<?php } ?>

				<?php if(count($catenv)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/environment-solutions" target="_blank">পৰিৱেশ সমস্যা সমাধান</a></li>
				<?php } ?>

				<?php if(count($catpollutn)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/pollution-remedies" target="_blank">প্ৰদূষণ প্ৰতিবিধান</a></li>
				<?php } ?>

				<?php if(count($catsani)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/sanitation-solution" target="_blank">অনাময় সমাধান</a></li>
				<?php } ?>

				<?php if(count($catpets)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/pets" target="_blank">ঘৰচীয়া জীৱ</a></li>
				<?php } ?>

				<?php if(count($catanimal)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/animal-care" target="_blank">জীৱ-জন্তুৰ দায়িত্ব</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="peo-list">
				<?php if(count($catwomen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/women" target="_blank">মহিলা</a></li>
				<?php } ?>

				<?php if(count($catmen)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/men" target="_blank">পুৰুষ</a></li>
				<?php } ?>

				<?php if(count($catdiff)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/differently-abled" target="_blank">দিব্যাংগজন</a></li>
				<?php } ?>

				<?php if(count($cattrans)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/transgender" target="_blank">তৃতীয় লিংগ</a></li>
				<?php } ?>

				<?php if(count($catlgbt)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/lgbt" target="_blank">এল জি বি টি</a></li>
				<?php } ?>

				<?php if(count($catincre)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/incredible-indians" target="_blank">ভাৰতীয় বিস্ময়</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="hel-list">
				<?php if(count($catmind)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/mind-and-sleep" target="_blank" target="_blank">ভাৱনা আৰু নিদ্ৰা</a></li>
				<?php } ?>

				<?php if(count($catmed)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/medical-breakthrough" target="_blank">চিকিৎসা উদ্ভাৱন</a></li>
				<?php } ?>

				<?php if(count($catfit)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/fitness" target="_blank">ফিটনেচ</a></li>
				<?php } ?>

				<?php if(count($catenab)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/enabling-specially-abled" target="_blank">দিব্যাংগজন</a></li>
				<?php } ?>

				<?php if(count($catyoga)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/yoga" target="_blank" target="_blank">যোগ</a></li>
				<?php } ?>

				<?php if(count($catcalm)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/calm-and-meditation" target="_blank">মানসিক প্ৰশান্তি আৰু মেডিটেচন</a></li>
				<?php } ?>

				<?php if(count($catdeac)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/de-activating-tech" target="_blank" target="_blank">ডি এক্টিভেটিং প্ৰযুক্তি</a></li>
				<?php } ?>

				<?php if(count($catdrug)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/drugs-as-medicine" target="_blank">ঔষধ হিচাবে ড্ৰাগচ</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="life-list">
				<?php if(count($catnatu)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/natural-beauty" target="_blank">প্ৰাকৃতিক সৌন্দৰ্য</a></li>
				<?php } ?>

				<?php if(count($catecof)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">পৰিৱেশ অনুকুল সাজসজ্জা</a></li>
				<?php } ?>

				<?php if(count($catkhadi)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/khadi-and-handloom" target="_blank" target="_blank">খাদী আৰু হস্ততাঁত</a></li>
				<?php } ?>

				<?php if(count($catherbs)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/herbs-and-food" target="_blank">বনৌষধি আৰু খাদ্য</a></li>
				<?php } ?>

				<?php if(count($cathome)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/homecare-and-gardening" target="_blank">গৃহ যতন আৰু গাৰ্ডেনিং</a></li>
				<?php } ?>

				<?php if(count($catcars)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/cars-and-bikes" target="_blank">গাড়ী আৰু বাইক</a></li>
				<?php } ?>

				<?php if(count($catsports)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/sports-and-athletics" target="_blank">ক্ৰীড়া আৰু এথলেটিকছ</a></li>
				<?php } ?>

				<?php if(count($cattravel)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/travel-and-adventure-tech" target="_blank">ভ্ৰমণ আৰু অভিযান টেক</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr">
			<ul class="edu-list">
				<?php if(count($catsmart)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/smart-cities" target="_blank">স্মাৰ্ট চিটি</a></li>
				<?php } ?>

				<?php if(count($catsafe)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/safety-tools" target="_blank" target="_blank">সুৰক্ষা সঁজুলি</a></li>
				<?php } ?>

				<?php if(count($catrural)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/rural-initiatives" target="_blank">গ্ৰাম্য পদক্ষেপ</a></li>
				<?php } ?>

				<?php if(count($catdisaster)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/disaster-management" target="_blank">দুৰ্যোগ ব্যৱস্থাপনা</a></li>
				<?php } ?>

				<?php if(count($catcampus)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/campus-life" target="_blank">কেম্পাছ জীৱন</a></li>
				<?php } ?>

				<?php if(count($cateduc)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/education-solution" target="_blank">শিক্ষা সমিধান</a></li>
				<?php } ?>

				<?php if(count($catcareers)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/careers-and-mentorship" target="_blank">কেৰিয়াৰ আৰু পৰামৰ্শদান</a></li>
				<?php } ?>

				<?php if(count($catfinan)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/financial-learnings" target="_blank">বিত্তীয় শিক্ষা</a></li>
				<?php } ?>
			</ul>
		</div>

		<div class="bv-catgr-child">
			<ul class="child-list">
				<?php if(count($catlabor)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/child-labour" target="_blank">শিশু শ্ৰম</a></li>
				<?php } ?>

				<?php if(count($cattraffic)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/child-trafficking" target="_blank">শিশু সৰবৰাহ</a></li>
				<?php } ?>

				<?php if(count($catparent)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/parenting" target="_blank">পেৰেণ্টিং</a></li>
				<?php } ?>

				<?php if(count($catelder)>0){ ?>
				<li><a style="font-size: 15px;" href="<?php echo base_url();?>assamese/elderly" target="_blank">বৃদ্ধাৱস্থা</a></li>
				<?php } ?>
			</ul>
		</div>
		</div>
	</div>

	<hr>

	<!-- End of Header -->

