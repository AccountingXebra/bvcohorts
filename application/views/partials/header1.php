<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>"/>
	<meta name="keywords" content="<?php echo $keywords; ?>"/>
	<!--meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/-->

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin { padding: 30px; background-color: #bf4b3d; color: white; }
		.mobile-view .head-margin { padding: 15px; background-color: #bf4b3d; color: white; }
		.design { font-family: Arial; font-size: 18px; }
		#popUpMain{ display: none }
		#langHeading{ text-align: center; color: black; font-size: 17px; font-family: Arial; }
		.bor-rad{ border-radius: 10px; }
		.navbar-brand{ margin-left: -50px; padding:0 45px !important; }
		.mb-logo{ margin-left: 30px; }
		.hr-head{ margin: 3.7rem -32px -61px -380px !important; }
		.mb-head-bot{ border-top: 0.5px solid #ccc; margin-top: 20px; }
		.sub-story{ font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important; }
		.mb-lang-head{ font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 155px !important; font-weight: bold !important; }
		.mb-hr{ margin-top: 40px; }
		.text-right{ text-align:right; }
		.navbar{ font-family: Arial; }
		.ft-div{ font-family: Arial }
		.ft-logo{ width:240px; margin-left:5px; margin:-5px 0 15px 45px; }
		.color-black{ color:#000 !important; }
		.marleft14{ margin-left: 14px; }
		.marleft25{ margin-left: 25px !important; }
		.ft-copy{ text-align: center; margin-top: 10px; font-family: Arial; font-size:15px; color: grey; }
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px" alt="Bharat Vaani" title="Bharat Vaani"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">CATEGORIES<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>home/submit-stories">SUBMIT STORIES</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>home/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>home/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LANGUAGE</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#">CATEGORIES<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link sub-stories" href="<?php echo base_url();?>home/submit-stories">SUBMIT STORIES</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>home/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>home/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LANGUAGE</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="categories-header1" style="margin-top: 7px; position: absolute; background-color: #fff; width: 99%; z-index: 1; display: none;">

		<hr style="margin-top: -8px !important;"> 

		<div class="row desktop-view" style="margin-top:2%;">

			<!--a href="<?php echo base_url();?>home/art-culture-and-artists">
				<div class="bv-catgr bv-catgr-at" style="margin-left: 30px;">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50" alt="art">
					<p style="margin-left: -3px !important;" class="cat-name-art">Art, Culture & Artists</p>
				</div>
			</a-->

			<a href="<?php echo base_url();?>home/science-and-tech">
				<div class="bv-catgr bv-catgr-sci">
					<img style="margin-left: 20px !important;" src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
					<p style="margin-left: -6px !important;" class="cat-name-sci">Science & Tech</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/pets-and-environment">
				<div class="bv-catgr bv-catgr-pets">
					<img style="margin-left: 20px !important;" src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
					<p style="margin-left: -25px !important;" class="cat-name-pet">Pets & Environment</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/people">
				<div class="bv-catgr bv-catgr-peo">
					<img style="margin-left: 8px !important;" src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
					<p style="margin-left: 11px !important;" class="cat-name-peo">People</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/health">
				<div class="bv-catgr bv-catgr-hel">
					<img style="margin-left: 8px !important;" src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
					<p style="margin-left: 14px !important;" class="cat-name-helt">Health</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/lifestyle-and-outdoors">
				<div class="bv-catgr bv-catgr-life">
					<img style="margin-left: 8px !important;" src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
					<p style="margin-left: -28px !important;" class="cat-name-life">Lifestyle & Outdoors</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/education-and-development">
				<div class="bv-catgr bv-catgr-edu">
					<img style="margin-left: 5px !important;" src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
					<p style="margin-left: -47px !important;" class="cat-name-edu">Education & Development</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>home/child-and-elderly">
				<div class="bv-catgr-child bv-catgr-chld">
					<img style="margin-left: 28px !important;" src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
					<p style="margin-left: 10px !important;" class="cat-name-child">Child & Elderly</p>
				</div>
			</a>
			
		</div>

		<!--div class="row cat-list">
			<div class="bv-catgr-art">
				<ul class="art-list">
					<?php if(count($catdance)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-classical-dance" target="_blank">Indian Classical Dance</a></li>
					<?php } ?>

					<?php if(count($catmusic)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-classical-music" target="_blank">Indian Classical Music</a></li>
					<?php } ?>

					<?php if(count($catinstrument)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-classical-instruments" target="_blank">Indian Classical Instruments</a></li>
					<?php } ?>

					<?php if(count($cattheatre)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-theatre" target="_blank">Indian Theatre</a></li>
					<?php } ?>

					<?php if(count($catpainters)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-painters" target="_blank">Indian Painters</a></li>
					<?php } ?>

					<?php if(count($catcraftsmen)>0){ ?>
					<li><a href="<?php echo base_url();?>home/indian-craftsmen" target="_blank">Indian Craftsmen</a></li>
					<?php } ?>

					<?php if(count($catbca)>0){ ?>
					<li><a href="<?php echo base_url();?>home/books-comics-and-authors" target="_blank">Books, Comics & Authors</a></li>
					<?php } ?>

					<?php if(count($catfun)>0){ ?>
					<li><a href="<?php echo base_url();?>home/fun-festivals" target="_blank">Fun Festivals</a></li>
					<?php } ?>

					<?php if(count($catarch)>0){ ?>
					<li><a href="<?php echo base_url();?>home/archaeology" target="_blank">Archeaology</a></li>
					<?php } ?>

					<?php if(count($cathistory)>0){ ?>
					<li><a href="<?php echo base_url();?>home/history" target="_blank">History</a></li>
					<?php } ?>

					<?php if(count($catmyth)>0){ ?>
					<li><a href="<?php echo base_url();?>home/mythology" target="_blank">Mythology</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="sci-list">
					<?php if(count($catengg)>0){ ?>
					<li><a href="<?php echo base_url();?>home/engineering-marvels" target="_blank">Engineering Marvels</a></li>
					<?php } ?>

					<?php if(count($catspace)>0){ ?>
					<li><a href="<?php echo base_url();?>home/space-and-astronomy" target="_blank">Space & Astronomy</a></li>
					<?php } ?>

					<?php if(count($catconst)>0){ ?>
					<li><a href="<?php echo base_url();?>home/construction-techniques" target="_blank">Construction Techniques</a></li>
					<?php } ?>

					<?php if(count($catpos)>0){ ?>
					<li><a href="<?php echo base_url();?>home/positive-gaming" target="_blank">Positive Gaming</a></li>
					<?php } ?>

					<?php if(count($cattech)>0){ ?>
					<li><a href="<?php echo base_url();?>home/tech-that-cares" target="_blank">Tech That Cares</a></li>
					<?php } ?>

					<?php if(count($catscience)>0){ ?>
					<li><a href="<?php echo base_url();?>home/science-wonders" target="_blank">Science Wonders</a></li>
					<?php } ?>

					<?php if(count($cathow)>0){ ?>
					<li><a href="<?php echo base_url();?>home/how-to" target="_blank">How To?</a></li>
					<?php } ?>

					<?php if(count($catfarm)>0){ ?>
					<li><a href="<?php echo base_url();?>home/farming-innovations" target="_blank">Farming Innovations</a></li>
					<?php } ?>

					<?php if(count($catdefence)>0){ ?>
					<li><a href="<?php echo base_url();?>home/defence" target="_blank">Defence</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="pets-list">
					<?php if(count($catwater)>0){ ?>
					<li><a href="<?php echo base_url();?>home/drinking-water-solution" target="_blank">Drinking Water Solution</a></li>
					<?php } ?>

					<?php if(count($catenergy)>0){ ?>
					<li><a href="<?php echo base_url();?>home/energy-alternatives" target="_blank">Energy Alternatives</a></li>
					<?php } ?>

					<?php if(count($catenv)>0){ ?>
					<li><a href="<?php echo base_url();?>home/environment-solutions" target="_blank">Environment Solution</a></li>
					<?php } ?>

					<?php if(count($catpollutn)>0){ ?>
					<li><a href="<?php echo base_url();?>home/pollution-remedies" target="_blank">Pollution Remedies</a></li>
					<?php } ?>

					<?php if(count($catsani)>0){ ?>
					<li><a href="<?php echo base_url();?>home/sanitation-solution" target="_blank">Sanitation Solution</a></li>
					<?php } ?>

					<?php if(count($catpets)>0){ ?>
					<li><a href="<?php echo base_url();?>home/pets" target="_blank">Pets</a></li>
					<?php } ?>

					<?php if(count($catanimal)>0){ ?>
					<li><a href="<?php echo base_url();?>home/animal-care" target="_blank">Animal Care</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="peo-list">
					<?php if(count($catwomen)>0){ ?>
					<li><a href="<?php echo base_url();?>home/women" target="_blank">Women</a></li>
					<?php } ?>

					<?php if(count($catmen)>0){ ?>
					<li><a href="<?php echo base_url();?>home/men" target="_blank">Men</a></li>
					<?php } ?>

					<?php if(count($catdiff)>0){ ?>
					<li><a href="<?php echo base_url();?>home/differently-abled" target="_blank">Differently Abled</a></li>
					<?php } ?>

					<?php if(count($cattrans)>0){ ?>
					<li><a href="<?php echo base_url();?>home/transgender" target="_blank">Transgenders</a></li>
					<?php } ?>

					<?php if(count($catlgbt)>0){ ?>
					<li><a href="<?php echo base_url();?>home/lgbt" target="_blank">LGBT</a></li>
					<?php } ?>

					<?php if(count($catincre)>0){ ?>
					<li><a href="<?php echo base_url();?>home/incredible-indians" target="_blank">Incredible Indians</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="hel-list">
					<?php if(count($catmind)>0){ ?>
					<li><a href="<?php echo base_url();?>home/mind-and-sleep" target="_blank" target="_blank">Mind & Sleep</a></li>
					<?php } ?>

					<?php if(count($catmed)>0){ ?>
					<li><a href="<?php echo base_url();?>home/medical-breakthrough" target="_blank">Medical Breakthrough</a></li>
					<?php } ?>

					<?php if(count($catfit)>0){ ?>
					<li><a href="<?php echo base_url();?>home/fitness" target="_blank">Fitness</a></li>
					<?php } ?>

					<?php if(count($catenab)>0){ ?>
					<li><a href="<?php echo base_url();?>home/enabling-specially-abled" target="_blank">Enabling Differently Abled</a></li>
					<?php } ?>

					<?php if(count($catyoga)>0){ ?>
					<li><a href="<?php echo base_url();?>home/yoga" target="_blank" target="_blank">Yoga</a></li>
					<?php } ?>

					<?php if(count($catcalm)>0){ ?>
					<li><a href="<?php echo base_url();?>home/calm-and-meditation" target="_blank">Calm & Meditation</a></li>
					<?php } ?>

					<?php if(count($catdeac)>0){ ?>
					<li><a href="<?php echo base_url();?>home/de-activating-tech" target="_blank" target="_blank">De-activating Tech</a></li>
					<?php } ?>

					<?php if(count($catdrug)>0){ ?>
					<li><a href="<?php echo base_url();?>home/drugs-as-medicine" target="_blank">Drugs as Medicine</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="life-list">
					<?php if(count($catnatu)>0){ ?>
					<li><a href="<?php echo base_url();?>home/natural-beauty" target="_blank">Natural Beauty</a></li>
					<?php } ?>

					<?php if(count($catecof)>0){ ?>
					<li><a href="<?php echo base_url();?>home/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">Eco-Friendly Fashion</a></li>
					<?php } ?>

					<?php if(count($catkhadi)>0){ ?>
					<li><a href="<?php echo base_url();?>home/khadi-and-handloom" target="_blank" target="_blank">Khadi & Handloom</a></li>
					<?php } ?>

					<?php if(count($catherbs)>0){ ?>
					<li><a href="<?php echo base_url();?>home/herbs-and-food" target="_blank">Herbs & Food</a></li>
					<?php } ?>

					<?php if(count($cathome)>0){ ?>
					<li><a href="<?php echo base_url();?>home/homecare-and-gardening" target="_blank">Homecare & Gardening</a></li>
					<?php } ?>

					<?php if(count($catcars)>0){ ?>
					<li><a href="<?php echo base_url();?>home/cars-and-bikes" target="_blank">Cars & Bikes</a></li>
					<?php } ?>

					<?php if(count($catsports)>0){ ?>
					<li><a href="<?php echo base_url();?>home/sports-and-athletics" target="_blank">Sports & Athletics</a></li>
					<?php } ?>

					<?php if(count($cattravel)>0){ ?>
					<li><a href="<?php echo base_url();?>home/travel-and-adventure-tech" target="_blank">Travel & Adventure Tech</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="edu-list">
					<?php if(count($catsmart)>0){ ?>
					<li><a href="<?php echo base_url();?>home/smart-cities" target="_blank">Smart Cities</a></li>
					<?php } ?>

					<?php if(count($catsafe)>0){ ?>
					<li><a href="<?php echo base_url();?>home/safety-tools" target="_blank" target="_blank">Safety Tools</a></li>
					<?php } ?>

					<?php if(count($catrural)>0){ ?>
					<li><a href="<?php echo base_url();?>home/rural-initiatives" target="_blank">Rural Initiatives</a></li>
					<?php } ?>

					<?php if(count($catdisaster)>0){ ?>
					<li><a href="<?php echo base_url();?>home/disaster-management" target="_blank">Disaster Management</a></li>
					<?php } ?>

					<?php if(count($catcampus)>0){ ?>
					<li><a href="<?php echo base_url();?>home/campus-life" target="_blank">Campus Life</a></li>
					<?php } ?>

					<?php if(count($cateduc)>0){ ?>
					<li><a href="<?php echo base_url();?>home/education-solution" target="_blank">Education Solution</a></li>
					<?php } ?>

					<?php if(count($catcareers)>0){ ?>
					<li><a href="<?php echo base_url();?>home/careers-and-mentorship" target="_blank">Careers & Mentorship</a></li>
					<?php } ?>

					<?php if(count($catfinan)>0){ ?>
					<li><a href="<?php echo base_url();?>home/financial-learnings" target="_blank">Financial Learnings</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr-child">
				<ul class="child-list">
					<ul class="child-list">
					<?php if(count($catlabor)>0){ ?>
					<li><a href="<?php echo base_url();?>home/child-labour" target="_blank">Child Labor</a></li>
					<?php } ?>

					<?php if(count($cattraffic)>0){ ?>
					<li><a href="<?php echo base_url();?>home/child-trafficking" target="_blank">Child Trafficking</a></li>
					<?php } ?>

					<?php if(count($catparent)>0){ ?>
					<li><a href="<?php echo base_url();?>home/parenting" target="_blank">Parenting</a></li>
					<?php } ?>

					<?php if(count($catelder)>0){ ?>
					<li><a href="<?php echo base_url();?>home/elderly" target="_blank">Elderly</a></li>
					<?php } ?>
				</ul>
				</ul>
			</div>
		</div-->

	</div>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>home/submit-stories">SUBMIT STORIES</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 155px !important; font-weight: bold !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LANGUAGE</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>

	<!-- End of Header -->

