<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html>
<head>

	<!-- Title -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/title.png"/>
	<title><?php echo $title; ?></title>

	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name = "description" content ="Bharat Vaani is a global multilingual video platform for narrating inspiring and positive stories"/>
    <meta name="keywords" content="Bharat Vaani, Indian languages, Vernacular languages, Multilingual, Innovation, Ideas, Solutions, Science, Technology, Gadgets, People, Products, Process, Policy, Inspiration, Motivation, Creativity, Positivity, Society, Well-being, Health, Environment, Sustainability, Good Life, Influencer, Digital Channel, Technology Channel, Stories, Content, Start-up, Entrepreneurship, Video Platform, Bharat, India"/>

    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title=" Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy; Feed" href="https://bharatvaani.in/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Indian Language Video Platform |  Positive & Inspiring Stories | People | Product | Process | Policy  & raquo; Comments Feed" href="https://bharatvaani.in/comments/feed/"/>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/header_footer.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/terms_privacy.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contact_partner_us.css">
    <!--link rel="stylesheet" type="text/css" href="<?php //echo base_url(); ?>assets/css/job_form.css"-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/homepage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/categories.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/editors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sliding.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <!-- JavaScript -->
    <script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/popper.js"></script>
	<script type="text/JavaScript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<style>
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - makes the four columns stack on top of each other instead of next to each other */

		.pp-head{
			margin: 4% 0;
			color: grey;
		}

		.pp-head p{
			font-family: "Arial";
			text-align: center;
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.pri-pol{
			padding: 0 0 5% 0;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tc-head{
			margin: 4% 0;
			color: grey;
		}

		.tc-head p{
			text-align: center;
			font-family: "Arial";
			font-size: 30px;
			font-weight: bold;
			color:#595959;
		}

		.tr-cond{
			padding: 1% 0 5% 0;
			color: grey;
			font-family: Arial;
			color: grey;
			font-size: 17px;
		}

		.tr-cond p{
			justify-content: center;
		}
		
		<!-- job_form css -->
		
		.head-margin {
		  padding: 30px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.mobile-view .head-margin {
		  padding: 15px;
		  background-color: #bf4b3d;
		  color: white;
		}

		.design {
		  font-family: Arial;
		  font-size: 18px;
		}
	</style>
	
	<script>
		$(document).ready(function(){
		  $("a").on('click', function(event) {
		    if (this.hash !== "") {
		      event.preventDefault();
		      var hash = this.hash;
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		        window.location.hash = hash;
		      });
		    }
		  });
		});
	</script>

</head>

<body>

	<!-- Header -->
	<div class="loader"></div>
    <nav class="navbar navbar-expand-lg navbar-light" style="font-family: Arial;">

		<a style="padding:0 45px !important;" class="navbar-brand" href="<?php echo base_url();?>gujarati"><img src="<?php echo base_url();?>assets/images/logo/logo1.png" width="240px"></a>
  		
		<!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMobileContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button-->

		<!-- Mobile View -->
		<div class="collapse navbar-collapse mobile-view" id="navbarMobileContent">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="header-cat nav-link cat-name" href="#categories">શ્રેણીઓ<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>gujarati/submit-stories">વાર્તાઓ સમર્પિત કરો</a>
				</li>

				<!-- User login and sign up
				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>gujarati/login">LOGIN</a>
				</li>

				<li class="nav-item">
					<a class="nav-link cat-name" href="<?php echo base_url();?>gujarati/register">SIGN UP</a>
				</li>
				-->

				<li class="nav-item dropdown cat-drop-head">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ગુજરાતી</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
						<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
						<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
						<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
						<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
						<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
						<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
					</div>
				</li>
				<li class="nav-item social-item">
					<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>
					<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>
					<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>
					<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>
					<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
				</li>

			</ul>
		</div>
		
		<!-- Desktop View -->
		<div class="collapse navbar-collapse desktop-view" id="navbarSupportedContent">
			<div>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="header-cat nav-link cat-name" href="#">શ્રેણીઓ<span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link sub-stories" href="<?php echo base_url();?>gujarati/submit-stories">વાર્તાઓ સમર્પિત કરો</a>
					</li>

					<!-- User login and sign up
					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>gujarati/login">LOGIN</a>
					</li>

					<li class="nav-item">
						<a class="nav-link cat-name" href="<?php echo base_url();?>gujarati/register">SIGN UP</a>
					</li>
					-->

					<li class="nav-item dropdown cat-drop-head">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ગુજરાતી</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
							<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
							<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
							<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
							<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
							<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
							<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
						</div>
					</li>

					&nbsp&nbsp&nbsp&nbsp
					<li class="nav-item social-item" style="text-align:right;">
						<a href="https://www.youtube.com/playlist?list=PLwFpGGX13cyGrwHFez3_eEQYZwOBBuAMz" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube"></a>&nbsp
						<a href="https://www.instagram.com/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Bharat Vaani Instagram" title="Bharat Vaani Instagram"></a>&nbsp
						<a href="https://www.facebook.com/bharatvaani.in" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Bharat Vaani Facebook" title="Bharat Vaani Facebook"></a>&nbsp
						<a href="https://twitter.com/bharatvaaniin" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Bharat Vaani Twitter" title="Bharat Vaani Twitter"></a>&nbsp
						<a href="https://www.linkedin.com/company/bharatvaani" target="_blank"><img src="<?php echo base_url(); ?>assets/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Bharat Vaani Linkedin" title="Bharat Vaani Linkedin"></a>
					</li>
				</ul>
			</div>

			<div><hr style="margin: 3.7rem -20px -61px -365px !important;"></div>

		</div>
	</nav>

	<div class="categories-header1" style="margin-top: 7px; position: absolute; background-color: #fff; width: 99%; z-index: 1; display: none;">
		<hr style="margin-top: -8px !important;"> 
		<div class="row desktop-view" style="margin-top: 2%; line-height: 1.5;">

			<!--a href="<?php echo base_url();?>gujarati/art-culture-and-artists">
				<div class="bv-catgr bv-catgr-at" style="margin-left: 30px;">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/1.png" alt="art" width="50" height="50" alt="art">
					<p style="margin-left: -10px !important;" class="cat-name-art">કલા, સંસ્કૃતિ અને કલાકારો</p>
				</div>
			</a-->

			<a href="<?php echo base_url();?>gujarati/science-and-tech">
				<div class="bv-catgr bv-catgr-sci">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/8 - 50.png" width="50" height="50" alt="Science & Tech" title="Science & Tech"></img>
					<p style="margin-left: 5px !important;" class="cat-name-sci">વિજ્ઞાન અને તકનીકી</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/pets-and-environment">
				<div class="bv-catgr bv-catgr-pets">
					<img style="margin-left: 40px !important;" src="<?php echo base_url();?>assets/images/cat-icons/7 - 50.png" width="50" height="50" alt="Pets & Environment" title="Pets & Environment"></img>
					<p style="margin-left: -15px !important;" class="cat-name-pet">પાળતુ પ્રાણી અને પર્યાવરણ</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/people">
				<div class="bv-catgr bv-catgr-peo">
					<img style="margin-left: 38px !important;" src="<?php echo base_url();?>assets/images/cat-icons/3 - 50.png" width="50" height="50" alt="People" title="People"></img>
					<p style="margin-left: 50px !important;" class="cat-name-peo">લોકો</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/health">
				<div class="bv-catgr bv-catgr-hel">
					<img style="margin-left: 34px !important;" src="<?php echo base_url();?>assets/images/cat-icons/2 - 50.png" width="50" height="50" title="Health" alt="Health"></img>
					<p style="margin-left: 37px !important;" class="cat-name-helt">આરોગ્ય</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/lifestyle-and-outdoors">
				<div class="bv-catgr bv-catgr-life">
					<img style="margin-left: 32px !important;" src="<?php echo base_url();?>assets/images/cat-icons/6 - 50.png" width="50" height="50" alt="Lifestyle & Outdoors" title="Lifestyle & Outdoors"></img>
					<p style="margin-left: -5px !important;" class="cat-name-life">જીવનશૈલી અને બહાર પ્રવૃતિઓ</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/education-and-development">
				<div class="bv-catgr bv-catgr-edu">
					<img style="margin-left: 30px !important;" src="<?php echo base_url();?>assets/images/cat-icons/4 - 50.png" width="50" height="50" alt="Education & Development" alt="Education & Development"></img>
					<p style="margin-left: 0px !important;" class="cat-name-edu">શિક્ષણ અને વિકાસ</p>
				</div>
			</a>

			<a href="<?php echo base_url();?>gujarati/child-and-elderly">
				<div class="bv-catgr-child bv-catgr-chld">
					<img style="margin-left: 28px !important;" src="<?php echo base_url();?>assets/images/cat-icons/5 - 50.png" width="50" height="50" alt="Child & Elderly" title="Child & Elderly"></img>
					<p style="margin-left: 5px !important;" class="cat-name-child">બાળકો અને વરિષ્ઠો</p>
				</div>
			</a>
		</div>

		<!--div class="row cat-list">
			<div class="bv-catgr-art">
				<ul class="art-list">
					<?php if(count($catdance)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-classical-dance" target="_blank">ભારતીય ક્લાસિકલ નૃત્ય</a></li>
					<?php } ?>

					<?php if(count($catmusic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-classical-music" target="_blank" target="_blank" target="_blank">ભારતીય શાસ્ત્રીય સંગીત</a></li>
					<?php } ?>

					<?php if(count($catinstrument)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-classical-instruments" target="_blank" target="_blank">ભારતીય ક્લાસિકલ વાજિંત્રો</a></li>
					<?php } ?>

					<?php if(count($cattheatre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-theatre" target="_blank">ભારતીય થિયેટર</a></li>
					<?php } ?>

					<?php if(count($catpainters)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-painters" target="_blank">ભારતીય ચિત્રકારો</a></li>
					<?php } ?>

					<?php if(count($catcraftsmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/indian-craftsmen" target="_blank">ભારતીય કારીગરો</a></li>
					<?php } ?>

					<?php if(count($catbca)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/books-comics-and-authors" target="_blank">પુસ્તકો, કોમિક્સ અને લેખકો</a></li>
					<?php } ?>

					<?php if(count($catfun)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/fun-festivals" target="_blank">મનોરંજક તહેવારો</a></li>
					<?php } ?>

					<?php if(count($catarch)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/archaeology" target="_blank">પુરાતત્ત્વવિદ્યા</a></li>
					<?php } ?>

					<?php if(count($cathistory)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/history" target="_blank">ઇતિહાસ</a></li>
					<?php } ?>

					<?php if(count($catmyth)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/mythology" target="_blank">પૌરાણિક કથા</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="sci-list">
					<?php if(count($catengg)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/engineering-marvels" target="_blank">એન્જિનિયરિંગ અજાયબીઓ</a></li>
					<?php } ?>

					<?php if(count($catspace)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/space-and-astronomy" target="_blank">અવકાશ અને ખગોળશાસ્ત્ર</a></li>
					<?php } ?>

					<?php if(count($catconst)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/construction-techniques" target="_blank">બાંધકામ તકનીકીઓ</a></li>
					<?php } ?>

					<?php if(count($catpos)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/positive-gaming" target="_blank">સકારાત્મક રમત-ગમ્મત </a></li>
					<?php } ?>

					<?php if(count($cattech)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/tech-that-cares" target="_blank">તકનીક જે કાળજી લે </a></li>
					<?php } ?>

					<?php if(count($catscience)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/science-wonders" target="_blank">વિજ્ઞાનની અજાયબીઓ</a></li>
					<?php } ?>

					<?php if(count($cathow)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/how-to" target="_blank">કઈ રીતે?</a></li>
					<?php } ?>

					<?php if(count($catfarm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/farming-innovations" target="_blank">ખેતીની નવીનતાઓ</a></li>
					<?php } ?>

					<?php if(count($catdefence)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/defence" target="_blank">સંરક્ષણ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="pets-list">
					<?php if(count($catwater)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/drinking-water-solution" target="_blank">પીવાના પાણીના ઉકેલો</a></li>
					<?php } ?>

					<?php if(count($catenergy)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/energy-alternatives" target="_blank">ઉર્જાના વિકલ્પો</a></li>
					<?php } ?>

					<?php if(count($catenv)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/environment-solutions" target="_blank">પર્યાવરણના ઉકેલો</a></li>
					<?php } ?>

					<?php if(count($catpollutn)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/pollution-remedies" target="_blank">પ્રદૂષણના ઉપાય</a></li>
					<?php } ?>

					<?php if(count($catsani)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/sanitation-solution" target="_blank">સ્વચ્છતાના ઉકેલો</a></li>
					<?php } ?>

					<?php if(count($catpets)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/pets" target="_blank">પાળતુ પ્રાણી</a></li>
					<?php } ?>

					<?php if(count($catanimal)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/animal-care" target="_blank">પ્રાણીઓની સાર-સંભાળ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="peo-list">
					<?php if(count($catwomen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/women" target="_blank">સ્ત્રીઓ</a></li>
					<?php } ?>

					<?php if(count($catmen)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/men" target="_blank">પુરુષો</a></li>
					<?php } ?>

					<?php if(count($catdiff)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/differently-abled" target="_blank">ભિન્ન રીતે સક્ષમ</a></li>
					<?php } ?>

					<?php if(count($cattrans)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/transgender" target="_blank">ટ્રાન્સજેંડર્સ</a></li>
					<?php } ?>

					<?php if(count($catlgbt)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/lgbt" target="_blank">એલજીબીટી (LGBT)</a></li>
					<?php } ?>

					<?php if(count($catincre)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/incredible-indians" target="_blank">અતુલ્ય ભારતીયો</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="hel-list">
					<?php if(count($catmind)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/mind-and-sleep" target="_blank" target="_blank">મન અને ઊંઘ</a></li>
					<?php } ?>

					<?php if(count($catmed)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/medical-breakthrough" target="_blank">તબીબી ક્ષેત્રે પ્રગતિ</a></li>
					<?php } ?>

					<?php if(count($catfit)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/fitness" target="_blank">તંદુરસ્તી</a></li>
					<?php } ?>

					<?php if(count($catenab)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/enabling-specially-abled" target="_blank">ખાસ-સક્ષમને સમર્ચ કરવા</a></li>
					<?php } ?>

					<?php if(count($catyoga)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/yoga" target="_blank" target="_blank">યોગ-અભ્યાસ</a></li>
					<?php } ?>

					<?php if(count($catcalm)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/calm-and-meditation" target="_blank">શાંતિ અને ધ્યાન</a></li>
					<?php } ?>

					<?php if(count($catdeac)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/de-activating-tech" target="_blank" target="_blank">ટેક નિષ્ક્રિય કરો</a></li>
					<?php } ?>

					<?php if(count($catdrug)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/drugs-as-medicine" target="_blank">દવા તરીકે ડ્રગ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="life-list">
					<?php if(count($catnatu)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/natural-beauty" target="_blank">કુદરતી સૌંદર્ય</a></li>
					<?php } ?>

					<?php if(count($catecof)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/eco-friendly-fashion" target="_blank" target="_blank" target="_blank">ઇકો ફ્રેન્ડલી ફેશન</a></li>
					<?php } ?>

					<?php if(count($catkhadi)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/khadi-and-handloom" target="_blank" target="_blank">ખાદી અને હેન્ડલૂમ</a></li>
					<?php } ?>

					<?php if(count($catherbs)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/herbs-and-food" target="_blank">આયુર્વેદીક ઔષધિ અને ખોરાક</a></li>
					<?php } ?>

					<?php if(count($cathome)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/homecare-and-gardening" target="_blank">ઘરની સંભાળ અને બાગકામ</a></li>
					<?php } ?>

					<?php if(count($catcars)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/cars-and-bikes" target="_blank">કાર અને બાઇક</a></li>
					<?php } ?>

					<?php if(count($catsports)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/sports-and-athletics" target="_blank">રમતો અને એથલેટિક્સ</a></li>
					<?php } ?>

					<?php if(count($cattravel)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/travel-and-adventure-tech" target="_blank">મુસાફરી અને સાહસ ટેક</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr">
				<ul class="edu-list">
					<?php if(count($catsmart)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/smart-cities" target="_blank">સ્માર્ટ શહેરો</a></li>
					<?php } ?>

					<?php if(count($catsafe)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/safety-tools" target="_blank" target="_blank">સલામતી સાધનો</a></li>
					<?php } ?>

					<?php if(count($catrural)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/rural-initiatives" target="_blank">ગ્રામીણ પહેલ</a></li>
					<?php } ?>

					<?php if(count($catdisaster)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/disaster-management" target="_blank">ડિઝાસ્ટર મેનેજમેન્ટ</a></li>
					<?php } ?>

					<?php if(count($catcampus)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/campus-life" target="_blank">કેમ્પસનું જીવન</a></li>
					<?php } ?>

					<?php if(count($cateduc)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/education-solution" target="_blank">શિક્ષણના ઉકેલો</a></li>
					<?php } ?>

					<?php if(count($catcareers)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/careers-and-mentorship" target="_blank">કારકિર્દી અને માર્ગદર્શન</a></li>
					<?php } ?>

					<?php if(count($catfinan)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/financial-learnings" target="_blank">નાણાકીય શિક્ષણ</a></li>
					<?php } ?>
				</ul>
			</div>

			<div class="bv-catgr-child">
				<ul class="child-list">
					<?php if(count($catlabor)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/child-labour" target="_blank">બાળ મજુર</a></li>
					<?php } ?>

					<?php if(count($cattraffic)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/child-trafficking" target="_blank">બાળકોની હેરાફેરી</a></li>
					<?php } ?>

					<?php if(count($catparent)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/parenting" target="_blank">બાળ-ઉછેર</a></li>
					<?php } ?>

					<?php if(count($catelder)>0){ ?>
					<li><a style="font-size: 15px;" href="<?php echo base_url();?>gujarati/elderly" target="_blank">વરિષ્ઠોનું</a></li>
					<?php } ?>
				</ul>
			</div>
		</div-->

	</div>

	<div class="row mobile-view" style="border-top: 0.5px solid #ccc; margin-top: 20px;">

		<a style="font-family: Arial !important; margin-left: 15px !important; float: left; color: #595959 !important; font-size: 16px !important;" class="nav-link cat-name" href="<?php echo base_url();?>gujarati/submit-stories">વાર્તાઓ સમર્પિત કરો</a>

		<a style="font-family: Arial !important; float: right; color: #595959 !important; padding-top: 13px; width: 145px !important;" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ગુજરાતી</a>

		<div style="font-family: Arial !important;" class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="<?php echo base_url();?>">English</a>
			<a class="dropdown-item" href="<?php echo base_url();?>hindi">हिन्दी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>assamese">অসমিয়া</a>
			<a class="dropdown-item" href="<?php echo base_url();?>bangla">বাংলা</a>
			<a class="dropdown-item" href="<?php echo base_url();?>gujarati">ગુજરાતી</a>
			<a class="dropdown-item" href="<?php echo base_url();?>marathi">मराठी</a>
			<a class="dropdown-item" href="<?php echo base_url();?>tamil">தமிழ்</a>
			<a class="dropdown-item" href="<?php echo base_url();?>telugu">తెలుగు</a>
		</div>

		<div style="margin-top: 40px;"><hr></div>

	</div>

	<!-- End of Header -->

