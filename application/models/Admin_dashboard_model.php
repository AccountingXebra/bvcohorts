<?php
class Admin_dashboard_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			
			return  $id;

		}else{
			return false;
		}
	}

	 public function insert_item()
    {    
        $data = array(
            'reg_username' => $this->input->post('reg_username'),
            'reg_email' => $this->input->post('reg_email'),
            'reg_password' => $this->input->post('reg_password'),
            'contact_confirm_password' => $this->input->post('contact_confirm_password'),
            'reg_mobile' => $this->input->post('reg_mobile'),
            'reg_admin_type' => $this->input->post('reg_admin_type'),
            'reg_profile_image' => $this->input->post('reg_profile_image'),
             'status' => 'Active',

        );
        return $this->db->insert('admin_profile', $data);
    }

     
    public function get_itemCRUD(){
       
        $this->db->from("admin_profile");
		$this->db->order_by("id","DESC");
		$query = $this->db->get();
        return $query->result();
    }

	public function insertData_batch($table, $data)
	{
		

		$this->db->insert_batch($table, $data);
		return true;
	}

	 public function update_item($id) 
    {
        $data = array(
            'reg_username' => $this->input->post('reg_username'),
            'reg_email' => $this->input->post('reg_email'),
            'reg_password' => $this->input->post('reg_password'),
            'contact_confirm_password' => $this->input->post('contact_confirm_password'),
            'reg_mobile' => $this->input->post('reg_mobile'),
            'reg_admin_type' => $this->input->post('reg_admin_type'),
            'reg_profile_image' => $this->input->post('reg_profile_image'),

        );
        if($id==0){
            return $this->db->insert('admin_profile',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('admin_profile',$data);
        }        
    }


	 public function find_item($id)
    {
        return $this->db->get_where('admin_profile', array('id' => $id))->row();
    }
	
	 public function delete_item($id,$status)
    {
        return $this->db->update('admin_profile',array('status'=>$status), array('id' => $id));
    }
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/


	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}



	public function revenueFilter($Data,$sort_field,$orderBy,$c)
	{
		//print_r($Data);exit;
		$this->db->select("sub.*,bus.bus_company_name,bus.nature_of_bus,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('subscription as sub');
		$this->db->join('businesslist as bus', 'bus.bus_id = sub.bus_id' ,'left');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');

		// if(!empty($Data['search']))
		// {
		// 	$this->db->group_start();
		// 	$this->db->where("sub.name like ","%".$Data['search']."%");
		// 	$this->db->or_where("sub.amount like ","%".$Data['search']."%");
		// 	$this->db->or_where("sub.invoice_no like","%".$Data['search']."%");
		// 	$this->db->or_where("sub.payment_status like ","%".$Data['search']."%");
		// 	$this->db->or_where("sub.subscription_plan like ","%".$Data['search']."%");
		// 	$this->db->or_where("sub.email like ","%".$Data['search']."%");
		// 	$this->db->group_end();
		// }
		//$this->db->join('services_other_taxes as tax', 'tax.service_id = ser.service_id' ,'left');
		if(@$Data['rev_start_date']!=''){
			$this->db->where("sub.createdat >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['rev_start_date']))));
		}
		if(@$Data['rev_end_date']!=''){
			$this->db->where("sub.createdat <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['rev_end_date']))));
		}

		if(@$Data['search_by_package']!=''){
			$this->db->where("sub.subscription_plan like ","%".$Data['search_by_package']."%");
		}

		if(@$Data['search_by_subsc']!=''){
			$this->db->where("sub.subscription_type like ","%".$Data['search_by_subsc']."%");
		}

		if(@$Data['search_bycountry']!=''){
			$this->db->where("bus.bus_billing_country like ","%".$Data['search_bycountry']."%");
		}

		if(@$Data['search_by_city']!=''){
			$this->db->where("bus.bus_billing_city like ","%".$Data['search_by_city']."%");
		}

		if(@$Data['search_by_coupon']!=''){
			$this->db->where("sub.promocode like ","%".$Data['search_by_coupon']."%");
		}
		//$this->db->where("sub.bus_id",$Data['bus_id']);
		
		//$this->db->group_by("sub.subscription_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}


	public function revenueDownloadFilter($array)
	{
		//print_r($Data);exit;
		$this->db->select("sub.*,bus.bus_company_name,bus.nature_of_bus,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('subscription as sub');
		$this->db->join('businesslist as bus', 'bus.bus_id = sub.bus_id' ,'left');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');

		$this->db->where_in("sub.subscription_id",$array);


		$query = $this->db->get();
		$result= $query->result_array();
		
		return $result;			

	}


	public function SubscriptionFilter($Data,$sort_field,$orderBy,$c)
	{
		//print_r($Data);exit;

		$this->db->select("reg.*,bus.bus_company_name,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name,sub.promocode,sub.amount");
		$this->db->from('registration as reg');
		$this->db->join('businesslist as bus', 'bus.bus_id = reg.bus_id' ,'left');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->join('subscription as sub', 'sub.reg_id = reg.reg_id','left');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("reg.reg_username like ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_id like ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_email like","%".$Data['search']."%");
			$this->db->or_where("reg.reg_mobile like ","%".$Data['search']."%");
			$this->db->or_where("sub.promocode like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		//$this->db->join('services_other_taxes as tax', 'tax.service_id = ser.service_id' ,'left');
		if(@$Data['sub_start_date']!=''){
			$this->db->where("reg.createdat >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['sub_start_date']))));
		}
		if(@$Data['sub_end_date']!=''){
			$this->db->where("reg.createdat <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['sub_end_date']))));
		}

		// if(@$Data['search_by_package']!=''){
		// 	$this->db->where("sub.subscription_plan like ","%".$Data['search_by_package']."%");
		// }

		// if(@$Data['search_by_subsc']!=''){
		// 	$this->db->where("sub.subscription_type like ","%".$Data['search_by_subsc']."%");
		// }

		if(@$Data['search_bycountry_sub']!=''){
			$this->db->where("bus.bus_billing_country like ","%".$Data['search_bycountry_sub']."%");
		}

		if(@$Data['search_by_city_sub']!=''){
			$this->db->where("bus.bus_billing_city like ","%".$Data['search_by_city_sub']."%");
		}

		// if(@$Data['search_by_coupon']!=''){
		// 	$this->db->where("sub.promocode like ","%".$Data['search_by_coupon']."%");
		//}
		//$this->db->where("sub.bus_id",$Data['bus_id']);
		
		$this->db->group_by("reg.reg_id");
		$this->db->order_by("reg.reg_id" ,"DESC");

		if( $c == 1)
		{
			// if($Data['length']!= -1){		
			// $this->db->limit($Data['length'],$Data['start']);
			// }
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}


	}



	public function Subscriber_Data_download($array)
	{
		//print_r($Data);exit;

		$this->db->select("reg.*,bus.bus_company_name,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name,sub.promocode,sub.amount");
		$this->db->from('registration as reg');
		$this->db->join('businesslist as bus', 'bus.bus_id = reg.bus_id' ,'left');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->join('subscription as sub', 'sub.reg_id = reg.reg_id','left');

		$this->db->where_in("reg.reg_id",$array);
	
		
		$this->db->group_by("reg.reg_id");

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		
		return $result;	

	}
	public function adminprofileFilter($Data,$sort_field,$orderBy,$c)
	{
		// print_r($Data);exit;

		$this->db->select("ap.*");
		$this->db->from('admin_profile as ap');

		
		$this->db->group_by("ap.feedback_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			// if($Data['length']!= -1){		
			// $this->db->limit($Data['length'],$Data['start']);
			// }
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}


	}



	public function feedbackFilter($Data,$sort_field,$orderBy,$c)
	{
		// print_r($Data);exit;

		$this->db->select("fb.*");
		$this->db->from('feedback as fb');

		// $q = $this->db->select('reg_id, registration.*') //select what we need
  //                     ->join('feedback', 'reg_id = feedback.reg_id') //do join
  //                     ->where(['reg_id' => $id]) //where clause
  //                     ->get('reg_username'); 

       

		if(!empty($Data['search_feedback']))
		{
			$this->db->group_start();
			$this->db->where("fb.feedback like ","%".$Data['search_feedback']."%");			
			$this->db->group_end();
		}


		if(@$Data['feedback_start_date']!=''){
			$this->db->where("fb.createdat >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['feedback_start_date']))));
		}

		if(@$Data['feedback_end_date']!=''){
			$this->db->where("fb.createdat <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['feedback_end_date']))));
		}

	
		
		//$this->db->where("cp.status",'Active');
		
		$this->db->group_by("fb.feedback_id");
		$this->db->order_by("fb.feedback_id","DESC");

		if( $c == 1)
		{
			// if($Data['length']!= -1){		
			// $this->db->limit($Data['length'],$Data['start']);
			// }
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}


	}

	
	public function feedbackDataDownload($array)
	{
		//print_r($Data);exit;

		$this->db->select("fb.*");
		$this->db->from('feedback as fb');

		$this->db->where_in("fb.feedback_id",$array);

		//$this->db->where("cp.status",'Active');
		
		$this->db->group_by("fb.feedback_id");

			
		$query = $this->db->get();
		$result= $query->result_array();
		return $result;			
	


	}


	public function referFilter($Data,$sort_field,$orderBy,$c)
	{
		//print_r($Data);exit;

		$this->db->select("sub.*,ref.*");
		$this->db->from('subscription as sub');
		$this->db->join('refer_earn as ref', 'sub.promocode = ref.refer_code' ,'right');

		if(!empty($Data['search_refer']))
		{
			$this->db->group_start();
			$this->db->where("sub.promocode like ","%".$Data['search_refer']."%");			
			$this->db->group_end();
		}


		if(@$Data['refer_start_date']!=''){
			$this->db->where("sub.createdat >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['refer_start_date']))));
		}

		if(@$Data['refer_end_date']!=''){
			$this->db->where("sub.createdat <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['refer_end_date']))));
		}

	
		
		//$this->db->where("cp.status",'Active');
		
		$this->db->group_by("sub.subscription_id");
		$this->db->order_by("sub.subscription_id","DESC");

		if( $c == 1)
		{
			// if($Data['length']!= -1){		
			// $this->db->limit($Data['length'],$Data['start']);
			// }
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}


	}



	public function couponFilter($Data,$sort_field,$orderBy,$c)
	{
		//print_r($Data);exit;

		$this->db->select("cp.*");
		$this->db->from('coupon_promo as cp');

		if(!empty($Data['search_coupon']))
		{
			$this->db->group_start();
			$this->db->where("cp.coupon_code like ","%".$Data['search_coupon']."%");
			$this->db->or_where("cp.description like ","%".$Data['search_coupon']."%");
			$this->db->or_where("cp.discount like","%".$Data['search_coupon']."%");
			$this->db->or_where("cp.validity like ","%".$Data['search_coupon']."%");
			$this->db->group_end();
		}


		if(@$Data['coupon_start_date']!=''){
			$this->db->where("cp.code_date >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['coupon_start_date']))));
		}

		if(@$Data['coupon_end_date']!=''){
			$this->db->where("cp.code_date <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['coupon_end_date']))));
		}

	
		if(@$Data['search_by_naturec']!=''){
			$this->db->where("cp.nature_of_code like ","%".$Data['search_by_naturec']."%");
		}

		//$this->db->where("cp.status",'Active');
		
		$this->db->group_by("cp.promo_id");
		$this->db->order_by("cp.promo_id", "DESC");

		if( $c == 1)
		{
			// if($Data['length']!= -1){		
			// $this->db->limit($Data['length'],$Data['start']);
			// }
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}


	}


	public function couponDataDownload($array)
	{
		//print_r($Data);exit;

		$this->db->select("cp.*");
		$this->db->from('coupon_promo as cp');

		$this->db->where_in("cp.promo_id",$array);

		//$this->db->where("cp.status",'Active');
		
		$this->db->group_by("cp.promo_id");

			
		$query = $this->db->get();
		$result= $query->result_array();
		return $result;			
	


	}












	public function MarketFilter($data)
	{
		$this->db->select("bus.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');

		if(@$data['search'])
		{
			$this->db->where("bus.bus_company_name like ","%".$data['search']."%");
			$this->db->or_where("bus.nature_of_bus like ","%".$data['search']."%");
		}
		
		if(@$data['multi_loc'])
		{
			$string = implode(",",$data['multi_loc']);
			$this->db->or_where("bus.bus_billing_city IN (".$string.")");
		}
		

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function GetUserInfo($data)
	{
		$this->db->select("bus.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		//$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->where("bus.bus_id",$data['id']);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function GetBranchInfo($data)
	{
		$this->db->select("gst.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('gst_number as gst');
		$this->db->join('countries as c', 'c.country_id = gst.country' ,'left');
		$this->db->join('states as s', 's.state_id = gst.state_code','left');
		$this->db->join('cities as ci', 'ci.city_id = gst.city','left');
		//$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->where("gst.bus_id",$data['id']);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function GetEmailIds($data)
	{
		$this->db->select("reg.reg_email");
		$this->db->from('registration as reg');
		$this->db->where("reg.reg_admin_type",'3');
		$this->db->where("reg.bus_id",$data['bus_id']);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_states($data)
	{
		$this->db->select("s.state_id");
		$this->db->from('states as s');
		$this->db->where("s.country_id",$data['country_id']);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_cities($data)
	{
		$this->db->select("ci.*");
		$this->db->from('cities as ci');
		$this->db->where("ci.city_id IN (".$data.")");
		$this->db->order_by("ci.name","ASC");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_country($data)
	{
		$this->db->select("c.*");
		$this->db->from('countries as c');
		$this->db->where("c.country_id IN (".$data.")");
		$this->db->order_by("c.country_name","ASC");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_unique_cities()
	{
		$this->db->distinct();
		$this->db->select("bus.bus_billing_city");
		$this->db->from('businesslist as bus');
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_unique_country()
	{
		$this->db->distinct();
		$this->db->select("bus.bus_billing_country");
		$this->db->from('businesslist as bus');
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function fetchChallanges($Data,$sort_field,$orderBy,$c){

       $this->db->select("*");
        $this->db->from('challanges');
        

      /* if(!empty($Data['search']))
		{
			$this->db->group_start();
			//$this->db->where("challange_name like ","%".$Data['search']."%");
			//$this->db->or_where("challange_nature like ","%".$Data['search']."%");
			
			$this->db->group_end();
		}*/
      /*  if(@$Data['challange_nature']!=''){
			$this->db->where("challange_nature",$Data['challange_nature']);
		}

		if(@$Data['challange_start_date']!=''){
			$this->db->where("challange_date >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['challange_start_date']))));
		}

		if(@$Data['challange_end_date']!=''){
			$this->db->where("challange_date <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['challange_end_date']))));
		}*/

       
        
        $this->db->group_by("id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          // print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }

      public function getWorkspace($post,$c) {

		$this->db->from('coworking_space as ws');
	

		if(!empty($post['name'])) {
			$this->db->where("ws.name like ","%".$post['name']."%");
		}

		if(!empty($post['country'])) {
			$this->db->where("ws.country ",$post['country']);
		}

		if(!empty($post['city'])) {
			$this->db->where("ws.city ",$post['city']);
		}

		

		 if( $c == 1)
        {      
            if($post['length']!= -1){       
            $this->db->limit($post['length'],$post['start']);
            }
            
            $query = $this->db->get();
            $result= $query->result_array();
          // print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }
	}


	public function getIncubation($post,$c) {

		$this->db->from('incubation as ic');
	

		if(!empty($post['name'])) {
			$this->db->where("ic.name like ","%".$post['name']."%");
		}

		if(!empty($post['country'])) {
			$this->db->where("ic.country ",$post['country']);
		}

		if(!empty($post['city'])) {
			$this->db->where("ic.city ",$post['city']);
		}

		

		
		$this->db->order_by("id" ,"DESC");

		

        if( $c == 1)
        {      
            if($post['length']!= -1){       
            $this->db->limit($post['length'],$post['start']);
            }
            
            $query = $this->db->get();
            $result= $query->result_array();
          // print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }
	}



}
?>