<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Admin_model extends CI_Model{ 
  
  function __construct() { 
    // Set table name 
    $this->table = 'admin';
  } 
   
  /* 
   * Fetch user data from the database 
   * @param array filter data based on the passed parameters 


   */ 


/*
  | -------------------------------------------------------------------
  | check unique fields
  | -------------------------------------------------------------------
  |
  */
  public function isUnique($table, $field, $value,$id='')
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($field,$value);
    if($id!='')
    {
      $this->db->where("id != ",$id);
    }
    $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);exit();
    $data = $query->num_rows();
    return ($data > 0)?FALSE:TRUE;
  }
  

    /*
  | -------------------------------------------------------------------
  | Insert data
  | -------------------------------------------------------------------
  |
  | general function to insert data in table
  |
  */
  public function insertData($table, $data)
  {
    
    $result = $this->db->insert($table, $data);
    // echo "<pre>";
    // print_r($result);exit();
    if($result == 1){
      return $this->db->insert_id();
    }else{
      return false;
    }
  }
  
  
  /*
  | -------------------------------------------------------------------
  | Update data
  | -------------------------------------------------------------------
  |
  | general function to update data
  |
  */
  public function updateData($table, $data, $where)
  {
    $this->db->where($where);
    if($this->db->update($table, $data)){
      return 1;
    }else{
      return 0;
    }
  }
  
  
  
  /*
  | -------------------------------------------------------------------
  | Select data
  | -------------------------------------------------------------------
  |
  | general function to get result by passing nesessary parameters
  |
  */
  public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
  {
    $this->db->select($fields);
    $this->db->from($table);
    if ($where != "") {
      $this->db->where($where);
    }
    

    if ($order_by != '') {
      $this->db->order_by($order_by,$order_type);
    }

    if ($group_by != '') {
      $this->db->group_by($group_by);
    }

    if ($limit > 0 && $rows == "") {
      $this->db->limit($limit);
    }
    if ($rows > 0) {
      $this->db->limit($rows, $limit);
    }


    $query = $this->db->get();

    if ($type == "rowcount") {
      $data = $query->num_rows();
    }else{
      $data = $query->result();
    }

    #echo "<pre>"; print_r($this->db->queries); exit;
    $query->result();

    return $data;
  }

    /*
  | -------------------------------------------------------------------
  | Role List
  | -------------------------------------------------------------------
  |
  | general function to delete the records
  |
  */

  public function selectRoleData($table){
    $query = $this->db->get($table);
    if($query!=NULL){
      return $query->result();
    }
    else{
      return false;
    }
  }

  
    /*
  | -------------------------------------------------------------------
  | Delere data
  | -------------------------------------------------------------------
  |
  | general function to delete the records
  |
  */
  public function deleteData($table, $data)
  {


    if($this->db->delete($table, $data)){
      return 1;
    }else{
      return 0;
    }
  }

  function getRows($params = array()){ 
    $this->db->select('*'); 
    $this->db->from($this->table); 
     
    if(array_key_exists("conditions", $params)){ 
      foreach($params['conditions'] as $key => $val){ 
          $this->db->where($key, $val); 
      } 
    } 
     
    if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){ 
        $result = $this->db->count_all_results(); 
    }else{ 
        if(array_key_exists("id", $params) || $params['returnType'] == 'single'){ 
            if(!empty($params['id'])){ 
                $this->db->where('id', $params['id']); 
            } 
            $query = $this->db->get(); 
            $result = $query->row_array(); 
        }else{ 
            $this->db->order_by('id', 'desc'); 
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                $this->db->limit($params['limit'],$params['start']); 
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                $this->db->limit($params['limit']); 
            } 
             
            $query = $this->db->get(); 

            $result = ($query->num_rows() > 0)?$query->result_array():FALSE; 
        } 

    } 
     
    // Return fetched data 
    return $result; 
  } 
   
  /* 
   * Insert user data into the database 
   * @param $data data to be inserted 
   */ 
  public function insert($data = array()) { 
    if(!empty($data)){ 
      // Add created and modified date if not included 
      if(!array_key_exists("created", $data)){ 
          $data['created'] = date("Y-m-d H:i:s"); 
      } 
      if(!array_key_exists("modified", $data)){ 
          $data['modified'] = date("Y-m-d H:i:s"); 
      } 
       
      // Insert member data 
      $insert = $this->db->insert('users', $data); 
       
      // Return the status 
      return $insert?$this->db->insert_id():false; 
    } 
    return false; 
  }

  public function update($data = array()) { 

    if(!empty($data)){ 
      // Add created and modified date if not included  
      if(!array_key_exists("modified", $data)){ 
          $data['modified'] = date("Y-m-d H:i:s"); 
      } 
       
      // Update member data 
      $update = $this->db->update('admin', $data); 
       
      // Return the status 
      return $update?$this->db->insert_id():false; 
    } 
    return false; 
  }

  public function checkCurrentpassword($user_id, $old_password) {

    $this->db->where('id', $user_id);
    $this->db->where('password', $old_password);

    $query = $this->db->get('admin');

    if($query->num_rows() > 0) 
      return true;
    else
      return false;
  }

  public function getUsersdata() {
    $query = $this->db->get('users');

    return $query->result();
  }

  public function getClientsdata() {
    $query = $this->db->get('clients');

    return $query->result();
  }

  public function deleteClient($id) {
    return $this->db->delete('clients', ['id'=>$id]);
  }

  public function deleteUser($id) {
    return $this->db->delete('users', ['id'=>$id]);
  }

  public function updatePost($data, $id) {
    return $this->db->where('id', $id)
                ->update('admin', $data);
  }

  public function getAdmindata($id) {
    $this->db->from('admin');
    $this->db->where('id', $id);
    $query = $this->db->get();

    return $query->result();
  }

  function getUsers() {    
    $this->db->from('users');
    return $num_rows = $this->db->count_all_results();
  }

  function getClients() {    
    $this->db->from('clients');
    return $num_rows = $this->db->count_all_results();
  } 

  function getEditorspicktotal() {    
    $this->db->from('video');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspick() {    
    $this->db->from('video');
    $this->db->where('language', 'English');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspickassamese() {    
    $this->db->from('video');
    $this->db->where('language', 'Assamese');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspickbangla() {    
    $this->db->from('video');
    $this->db->where('language', 'Bangla');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspickhindi() {    
    $this->db->from('video');
    $this->db->where('language', 'Hindi');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspickgujarati() {    
    $this->db->from('video');
    $this->db->where('language', 'Gujarati');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspickmarathi() {    
    $this->db->from('video');
    $this->db->where('language', 'Marathi');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspicktamil() {    
    $this->db->from('video');
    $this->db->where('language', 'Tamil');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getEditorspicktelugu() {    
    $this->db->from('video');
    $this->db->where('language', 'Telugu');
    $this->db->where('particular', 'Editors_pick');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriestotal() {    
    $this->db->from('video');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategories() {    
    $this->db->from('video');
    $this->db->where('language', 'English');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriestelugu() {    
    $this->db->from('video');
    $this->db->where('language', 'Telugu');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriestamil() {    
    $this->db->from('video');
    $this->db->where('language', 'Tamil');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriesgujarati() {    
    $this->db->from('video');
    $this->db->where('language', 'Gujarati');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriesmarathi() {    
    $this->db->from('video');
    $this->db->where('language', 'Marathi');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategorieshindi() {    
    $this->db->from('video');
    $this->db->where('language', 'Hindi');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriesbangla() {    
    $this->db->from('video');
    $this->db->where('language', 'Bangla');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getCategoriesassamese() {    
    $this->db->from('video');
    $this->db->where('language', 'Assamese');
    $this->db->where('particular', 'Categories');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeople() {    
    $this->db->from('video');
    $this->db->where('language', 'English');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeopletotal() {    
    $this->db->from('video');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeopleassamese() {    
    $this->db->from('video');
    $this->db->where('language', 'Assamese');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeoplebangla() {    
    $this->db->from('video');
    $this->db->where('language', 'Bangla');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeoplehindi() {    
    $this->db->from('video');
    $this->db->where('language', 'Hindi');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeoplemarathi() {    
    $this->db->from('video');
    $this->db->where('language', 'Marathi');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeopletelugu() {    
    $this->db->from('video');
    $this->db->where('language', 'Telugu');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeopletamil() {    
    $this->db->from('video');
    $this->db->where('language', 'Tamil');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  function getInspiringpeoplegujarati() {    
    $this->db->from('video');
    $this->db->where('language', 'Gujarati');
    $this->db->where('particular', 'Inspiring_people');
    return $num_rows = $this->db->count_all_results();
  }

  public function email_exist($email) {

    $this->db->select('email');
    $this->db->from('admin');
    $this->db->where('email', $email);

    $query = $this->db->get();

    return $query->row_array();
  }

  public function updatePassword($email, $userData = array()) {

    if(!empty($userData)) {

      if(!array_key_exists("modified", $userData)){ 
        $userData['modified'] = date("Y-m-d H:i:s"); 
      }

      $update = $this->db->where('email', $email);
      $update = $this->db->update('admin', $userData);

      return $update?$this->db->insert_id():false; 
    } 
    
    return false; 
  }

    public function fetchUser($Data,$sort_field,$orderBy,$c){

        $this->db->select("user.*");
        $this->db->from('registration as user');
     

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/

         if($Data['city']!=''){
       $this->db->where("user.user_city",$Data['city']);
        }
        
      if($Data['status']!=''){
       $this->db->where("user.status",$Data['status']);
        }
        $this->db->group_by("user.reg_id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }
      }


        public function download_multiple_users($array)
    {
      $this->db->select("rg.*");
      $this->db->from('registration as rg');

      
      if(is_array($array)){
        $this->db->where_in("rg.reg_id",$array);
      } else {
        $this->db->where("rg.reg_id",$array);
      }
    

      $this->db->group_by("rg.reg_id");
      $query = $this->db->get();
      $result= $query->result_array();

      return $result;

    }


      public function fetchActiveUser($Data,$sort_field,$orderBy,$c){

        $this->db->select("user.*");
        $this->db->from('registration as user');
     

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/

        // if($Data['city']!=''){
       //$this->db->where("user.user_city",$Data['city']);
        //}
        
      //if($Data['status']!=''){
       $this->db->where("user.status","Active");
     //   }
        $this->db->group_by("user.reg_id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }
      }


      public function fetchUserVideo($Data,$sort_field,$orderBy,$c){

       $this->db->select("vid.*,user.*");
        $this->db->from('registration as user');
        $this->db->join('video as vid', 'user.reg_id = vid.reg_id','right');

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
        $this->db->where("vid.uploaded_user","User");
        
        $this->db->group_by("vid.id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


        public function fetchUserTopVideo($Data,$sort_field,$orderBy,$c){

       $this->db->select("vid.*,user.*,count(vi.video_id) as views");
        $this->db->from('registration as user');
        $this->db->join('video as vid', 'user.reg_id = vid.reg_id','right');
        $this->db->join('views as vi', 'vid.id = vi.video_id','left');

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
        $this->db->where("vid.uploaded_user","User");
        
        $this->db->group_by("vi.video_id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


       public function fetchInterview($Data,$sort_field,$orderBy,$c){

       $this->db->select("*");
        $this->db->from('interviews');
        

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
       
        
        $this->db->group_by("id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


        public function fetchBlogs($Data,$sort_field,$orderBy,$c){

       $this->db->select("*");
        $this->db->from('blogs');
        

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
       
        
        $this->db->group_by("id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


       public function fetchPodcast($Data,$sort_field,$orderBy,$c){

       $this->db->select("*");
        $this->db->from('podcast');
        

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
       
        
        $this->db->group_by("id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


         public function fetchQuizs($Data,$sort_field,$orderBy,$c){

       $this->db->select("cat.*");
        $this->db->from('quizs as cat');
      //  $this->db->join('video as vid', 'user.reg_id = vid.reg_id','right');

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
       
        
        $this->db->group_by("cat.id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }




      public function fetchCategory($Data,$sort_field,$orderBy,$c){

       $this->db->select("cat.*");
        $this->db->from('categories as cat');
      //  $this->db->join('video as vid', 'user.reg_id = vid.reg_id','right');

       /* if(!empty($Data['search']))
        {
            $this->db->group_start();
            
            
            $this->db->group_end();
        }
        
        if($Data['search_by_location']!=''){
            $this->db->where("pl.location",$Data['search_by_location']);
        }
        if($Data['search_by_name']!=''){
            $this->db->where("pl.policy_name",$Data['search_by_name']);
        }
        //if($Data['search_by_year']!=''){
        //  $this->db->where("pl.annual_year ",$Data['search_by_year']);
        //}*/
        
      //  $this->db->where("vid.reg_id",$Data['reg_id']);
      //  $this->db->where("vid.status","Active");
       
        
        $this->db->group_by("cat.id");
        $this->db->order_by("".$sort_field." ".$orderBy."");

        if( $c == 1)
        {
            if($Data['length']!= -1){       
            if($Data['length']!= -1){       
            $this->db->limit($Data['length'],$Data['start']);
            }
            }
            $query = $this->db->get();
            $result= $query->result_array();
          //  print $this->db->last_query();
            return $result;         
        }
        else
        {       
            
            $query = $this->db->get();
            $result['NumRecords']=$query->num_rows();       
            return $result;     
        }


      }


      public function get_videos_summary($data){

        
        $data['start_date']=str_replace('/', '-', $data['start_date']);
        $data['end_date']=str_replace('/', '-', $data['end_date']);

    if($data['option']=="videos"){
      $this->db->select("count(vid.id) As videonum,DATE_FORMAT(vid.created_at,'%b-%Y') AS createdDate ")      
    

    ->from('video as vid')   

   
    ->where("vid.status ",'Active');

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(vid.created_at, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(vid.created_at, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(vid.created_at, '%Y-%m')");

    }

    if($data['option']=="subscription"){
      $this->db->select("count(sub.id) As videonum,DATE_FORMAT(sub.createdat,'%b-%Y') AS createdDate ")      
    

    ->from('subscription as sub')   

   
    ->where("sub.status ",1);

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(sub.createdat, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(sub.createdat, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(sub.createdat, '%Y-%m')");

    }



    if($data['option']=="signup"){
      $this->db->select("count(reg.reg_id) As videonum,DATE_FORMAT(reg.createdat,'%b-%Y') AS createdDate ")      
    

    ->from('registration as reg')   

   
    ->where("reg.status ",'Active');

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(reg.createdat, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(reg.createdat, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(reg.createdat, '%Y-%m')");

    }


     if($data['option']=="interaction"){
      $this->db->select("count(li.id) As videonum,DATE_FORMAT(li.date_created,'%b-%Y') AS createdDate ")      
    

    ->from('likes as li');

   
    //->where("reg.status ",'Active');

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(li.date_created, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(li.date_created, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(li.date_created, '%Y-%m')");
     $this->db->get();
     $query1= $this->db->last_query();

      $this->db->select("count(com.id) As videonum,DATE_FORMAT(com.date_created,'%b-%Y') AS createdDate ")      
    

    ->from('comments as com');

   
    //->where("reg.status ",'Active');

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(com.date_created, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(com.date_created, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(com.date_created, '%Y-%m')");



     // $query=$this->db->get();

      $this->db->get();

      $query2= $this->db->last_query();

     
    $query= $this->db->query($query1. " UNION ALL ".  $query2);


 

   $result =$query->result();

    
     $data['summary']=[array(
     'like'=>$result[0]->videonum,
     'comment'=>$result[1]->videonum,
      'videonum'=>$result[0]->videonum + $result[1]->videonum,
           'createdDate'=>$result[0]->createdDate,
     )];


    return (array) $data;

    }



     if($data['option']=="views" || $data['option']=="visitors"){
      $this->db->select("count(vi.id) As videonum,DATE_FORMAT(vi.date_created,'%b-%Y') AS createdDate ")      
    

    ->from('views as vi');

   
   

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(vi.date_created, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(vi.date_created, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(vi.date_created, '%Y-%m')");

    }


     /*if($data['option']=="visitors"){
      $this->db->select("count(DISTINCT(vis.ip)) As videonum,DATE_FORMAT(vis.date_created,'%b-%Y') AS createdDate ")      
    

    ->from('visitors as vis') ;

   
  

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(vis.date_created, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(vis.date_created, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(vis.date_created, '%Y-%m')");

    }*/

     if($data['option']=="login"){
      $this->db->select("SUM(login.time_spend) As videonum,DATE_FORMAT(login.created_at,'%b-%Y') AS createdDate ")      
    

    ->from('login_record as login') ;

   
  

  
  if($data['start_date']!="" && $data['end_date']!=""){
    $this->db->where("DATE_FORMAT(login.created_at, '%Y-%m') >=",date("Y-m",strtotime($data['start_date'])))

    ->where("DATE_FORMAT(login.created_at, '%Y-%m') <=",date("Y-m",strtotime($data['end_date'])));

  }
   
        
     $this->db->group_by("DATE_FORMAT(login.created_at, '%Y-%m')");

    }

    $query=$this->db->get();

      

      //print $this->db->last_query();

     



    $data['summary']=$query->result();

    



    return (array) $data;

      }
 


}