<?php
class Refer_earn_model extends CI_Model{

	public function  __construct(){
		parent::__construct();
		$this->load->database();

	}


	/*
		| -------------------------------------------------------------------
		| check unique fields
		| -------------------------------------------------------------------
		|
		*/
		public function isUnique($table, $field, $value,$id='')
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($field,$value);
			if($id!='')
			{
				$this->db->where("id != ",$id);
			}
			$query = $this->db->get();
			// echo "<pre>";
			// print_r($query);exit();
			$data = $query->num_rows();
			return ($data > 0)?FALSE:TRUE;
		}

			/*
		| -------------------------------------------------------------------
		| Insert data
		| -------------------------------------------------------------------
		|
		| general function to insert data in table
		|
		*/
		public function insertData($table, $data)
		{

			$result = $this->db->insert($table, $data);

			if($result == 1){

				$id=$this->db->insert_id();
				return  $id;

			}else{
				return false;
			}
		}

		public function insertData_batch($table, $data)
		{

			$this->db->insert_batch($table, $data);
			return true;
		}


		/*
		| -------------------------------------------------------------------
		| Update data
		| -------------------------------------------------------------------
		|
		| general function to update data
		|
		*/

		public function updateData($table, $data, $where)
		{

			$this->db->where($where);
			if($this->db->update($table, $data)){

				return 1;

			}else{

				return 0;
			}
		}



		/*
		| -------------------------------------------------------------------
		| Select data
		| -------------------------------------------------------------------
		|
		| general function to get result by passing nesessary parameters
		|
		*/
		public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
		{
			$this->db->select($fields);
			$this->db->from($table);
			if ($where != "") {
				$this->db->where($where);
			}

			if ($order_by != '') {
				$this->db->order_by($order_by,$order_type);
			}

			if ($group_by != '') {
				$this->db->group_by($group_by);
			}

			if ($limit > 0 && $rows == "") {
				$this->db->limit($limit);
			}
			if ($rows > 0) {
				$this->db->limit($rows, $limit);
			}


			$query = $this->db->get();

			if ($type == "rowcount") {
				$data = $query->num_rows();
			}else{
				$data = $query->result();
			}

			#echo "<pre>"; print_r($this->db->queries); exit;
			$query->result();

			return $data;
		}

		public function searchData($keyword){
				$this->db->select('state_name');
				$this->db->from('states');
				$this->db->where('country_id',101);
				$this->db->like('state_name', $keyword, 'after');
				$query = $this->db->get();
				return $query->result();

		}

			/*
		}
		| -------------------------------------------------------------------
		| Role List
		| -------------------------------------------------------------------
		|
		| general function to delete the records
		|
		*/


		public function selectRoleData($table){
			$query = $this->db->get($table);
			if($query!=NULL){
				return $query->result();
			}
			else{
				return false;
			}
		}


			/*
		| -------------------------------------------------------------------
		| Delete data
		| -------------------------------------------------------------------
		|
		| general function to delete the records
		|
		*/
		public function deleteData($table, $data)
		{
			if($this->db->delete($table, $data)){

				return 1;
			}else{
				return 0;
			}
		}

        //  Referal Code List Filter
		public function referalListFilter($Data,$sort_field,$orderBy,$c,$type=0)
		{
			$this->db->select("rel.*,re.*,reg.*,sub.*");
			$this->db->from('refer_earn_list as rel');
			if(!empty($Data['search']))
			{
				$this->db->group_start();
				// $this->db->where("rel.signup_date like ","%".$Data['search']."%");
				$this->db->or_where("rel.refer_list_email like ","%".$Data['search']."%");
				// $this->db->or_where("rel.status like ","%".$Data['search']."%");
				// $this->db->or_where("rel.subscription like ","%".$Data['search']."%");
				// $this->db->or_where("rel.referal_fee like ","%".str_replace(" ", "_",$Data['search'])."%");
				$this->db->group_end();
			}

			$this->db->join('refer_earn as re', 're.refer_id = rel.refer_id','left');
			$this->db->join('registration as reg', 'reg.reg_email = rel.refer_list_email','left');
			$this->db->join('subscription as sub', 'sub.email = rel.refer_list_email','left');


			if(!empty($Data['referal_end_date']) && $Data['referal_start_date']!=''){
				$this->db->where("re.createdat>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['referal_start_date']))));
			}
			if(!empty($Data['referal_end_date']) && $Data['referal_end_date']!=''){
				$this->db->where("re.updatedat<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['referal_end_date']))));
			}

			$this->db->where("rel.reg_no",$Data['reg_id']);
			//$this->db->where("reg.reg_email IS NOT NULL", null, false);

			$this->db->order_by("rel.refer_list_id","DESC");

			if( $c == 1)
			{
				if(!empty($Data['length']) && $Data['length']!= -1){
				$this->db->limit($Data['length'],$Data['start']);
				}

				$query = $this->db->get();
				
				$result= $query->result_array();

				return $result;
			}
			else
			{

				$query = $this->db->get();
				//print_r($this->db->last_query());exit;
				$result['NumRecords']=$query->num_rows();
				return $result;
			}

		}

		public function download_multiple_referearn_list($array,$reg_id)
		{
			$this->db->select("rel.*,pp.*, ss.*");
			$this->db->from('refer_earn_list as rel');

			$this->db->join('registration as pp', 'pp.reg_email = rel.refer_list_email','left');
			$this->db->join('subscription as ss', 'ss.email = rel.refer_list_email','left');
			if(is_array($array)){
			$this->db->where_in("rel.refer_list_id",$array);
			} else {
			$this->db->where("rel.refer_list_id",$array);
			}

			$this->db->group_by("rel.refer_list_id");
			$query = $this->db->get();
			$result= $query->result_array();

			return $result;

		}

}
?>
