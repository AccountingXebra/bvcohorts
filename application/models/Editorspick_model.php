<?php
	class Editorspick_model extends CI_Model {

		function __construct() {
	        // Set table name
	        $this->table = 'video';
    	}

    	function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->table);
        //$this->db->where('particular', "Editors_pick");
        $this->db->where('language', "English");
        $this->db->where("show_edt", 1);
        $this->db->order_by("id","DESC");
		$this->db->limit(5);
        
        if(array_key_exists("conditions", $params)){
            foreach($params['conditions'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(!empty($params['searchKeyword'])){
            $search = $params['searchKeyword'];
            $likeArr = array('title' => $search);
            $this->db->like($likeArr);
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'DESC');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }

		public function getPosts() {

			$query = $this->db->get('video');
			
			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		public function getVideos() {
            $this->db->select("ep.*");
			$this->db->from("video as ep");
			$this->db->where("language", "English");
			//$this->db->where("particular", "Editors_pick");
			$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindivideos() {
            $this->db->select("ep.*");
			$this->db->from("video as ep");
			$this->db->where("language", "Hindi");
			//$this->db->where("particular", "Editors_pick");
          	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesevideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Assamese");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujarativideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Gujarati");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathivideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Marathi");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglavideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Bangla");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilvideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Tamil");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguvideos() {
           	$this->db->select("ep.*");
			$this->db->from("video as ep");
           	$this->db->where("language", "Telugu");
           	$this->db->where("show_edt", 1);
          	$this->db->order_by("ep.id","DESC");
          	$this->db->limit(5);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function addPost($data) {
			return $this->db->insert('video', $data);
		}

		public function getSinglePosts($id) {
			$query = $this->db->get_where('video', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

		public function updatePost($data, $id) {
			return $this->db->where('id', $id)
						->update('video', $data);
		}

		public function deletePosts($id) {
			return $this->db->delete('video', ['id'=>$id]);
		}

		public function getLanguage() {
			$query = $this->db->get('language');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		function getHindirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Hindi");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getAssameserows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Assamese");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getBanglarows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Bangla");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getMarathirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Marathi");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getGujaratirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Gujarati");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTamilrows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Tamil");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTelugurows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Editors_pick");
	        $this->db->where('language', "Telugu");
	        $this->db->where("show_edt", 1);
        	$this->db->order_by("id","DESC");
			$this->db->limit(5);
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	}