<?php
	class Jobapplication_model extends CI_Model {

		function __construct() {
	        // Set table name
	        $this->table = 'job_application';
    	}

    	function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("conditions", $params)){
            foreach($params['conditions'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(!empty($params['searchKeyword'])){
            $search = $params['searchKeyword'];
            $likeArr = array('full_name' => $search, 'email' => $search);
            $this->db->like($likeArr);
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'DESC');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }

		public function getJob() {

			$query = $this->db->get('job_application');
			
			return $query->result();
		}

		public function getSingleJob($id) {
			$query = $this->db->get_where('job_application', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

		public function deleteJob($id) {
			return $this->db->delete('job_application', ['id'=>$id]);
		}
	}