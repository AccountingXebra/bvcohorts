<?php
	class Uploadvideos_model extends CI_Model {

		function __construct() {
	        // Set table name
	        $this->table = 'video';
    	}

    	function getRows($params = array()){


	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	           // $this->db->or_like($likeArr);
	             $this->db->like($likeArr);
	            $this->db->where('particular', "Categories");
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	               
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    public function getVideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "English");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesevideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Assamese");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglavideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Bangla");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujarativideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Gujarati");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindivideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Hindi");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathivideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Marathi");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilvideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Tamil");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguvideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Telugu");
           	$this->db->where("particular", "Categories");
            $this->db->order_by("ins.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSinglePosts($id) {
			$query = $this->db->get_where('video', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

	    function getAssameserows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            //$this->db->or_like($likeArr);
	             $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getBanglarows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->or_like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getHindirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getGujaratirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getMarathirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	          //  $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	            $this->db->or_like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTamilrows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTelugurows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }
		
		//Fetching categories data from database
		public function getCategories() {
			$query = $this->db->get('categories');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		//Fetching language data from database
		public function getLanguage() {
			$query = $this->db->get('language');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		//Inserting into database
		public function addData($data) {
			return $this->db->insert('video', $data);
		}

		public function getPosts() {

			$query = $this->db->get('video');
			
			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		function artists($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artisthindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artistassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artistbangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artistgujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artistmarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artisttamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);

	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function artisttelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Artists");
	        $this->db->or_where('category2', "Artists");
	        $this->db->or_where('category3', "Artists");
	        $this->db->or_where('category4', "Artists");
	        $this->db->or_where('category5', "Artists");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);

	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function science($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);

	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function scienceassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

		function sciencebangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function sciencegujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }	    

	    function sciencehindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function sciencemarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function sciencetamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function sciencetelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Science");
	        $this->db->or_where('category2', "Science");
	        $this->db->or_where('category3', "Science");
	        $this->db->or_where('category4', "Science");
	        $this->db->or_where('category5', "Science");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function child($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childbangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childgujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childhindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childmarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childtamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function childtelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Child");
	        $this->db->or_where('category2', "Child");
	        $this->db->or_where('category3', "Child");
	        $this->db->or_where('category4', "Child");
	        $this->db->or_where('category5', "Child");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestyle($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestyleassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestylebangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestylegujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestylehindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestylemarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestyletamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function lifestyletelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Lifestyle");
	        $this->db->or_where('category2', "Lifestyle");
	        $this->db->or_where('category3', "Lifestyle");
	        $this->db->or_where('category4', "Lifestyle");
	        $this->db->or_where('category5', "Lifestyle");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function pets($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petsassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petsbangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petsgujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petshindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petsmarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petstamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function petstelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Pets");
	        $this->db->or_where('category2', "Pets");
	        $this->db->or_where('category3', "Pets");
	        $this->db->or_where('category4', "Pets");
	        $this->db->or_where('category5', "Pets");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function people($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peopleassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peoplebangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peoplegujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peoplehindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peoplemarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peopletamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function peopletelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "People");
	        $this->db->or_where('category2', "People");
	        $this->db->or_where('category3', "People");
	        $this->db->or_where('category4', "People");
	        $this->db->or_where('category5', "People");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function health($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthbangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthgujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthhindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthmarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthtamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function healthtelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Health");
	        $this->db->or_where('category2', "Health");
	        $this->db->or_where('category3', "Health");
	        $this->db->or_where('category4', "Health");
	        $this->db->or_where('category5', "Health");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function education($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "English");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationassamese($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Assamese");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationbangla($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Bangla");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationgujarati($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Gujarati");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	          //  $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationhindi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Hindi");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationmarathi($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Marathi");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	            //$this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationtamil($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Tamil");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function educationtelugu($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        $this->db->where('particular', "Categories");
	        $this->db->where('language', "Telugu");
	        $this->db->group_start();
	        $this->db->where('category', "Education");
	        $this->db->or_where('category2', "Education");
	        $this->db->or_where('category3', "Education");
	        $this->db->or_where('category4', "Education");
	        $this->db->or_where('category5', "Education");
	        $this->db->group_end();
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search, 'category' => $search, 'subcategory' => $search);
	           // $this->db->or_like($likeArr);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

		//English
		public function getDance() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			//$this->db->limit(4);
          	$this->db->order_by("vid.id","DESC");
			//print $this->db->last_query(); exit();
			//$query1 = $this->db->query("Select * from video as vid where language='English' AND subcategory='Indian Classical Dance' AND (subcategory2='Indian Classical Dance' OR subcategory3='Indian Classical Dance' )");
			$query = $this->db->get();
			//print $this->db->last_query(); exit();
			return $query->result();
		}

		public function getMusic() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function getInstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getPainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getCraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getFunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getArchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getHistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getMarvels() {
            
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$query = $this->db->get();
			return $query->result();
		}

		public function getAstronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getConstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getPositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getScience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getFarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming & Agri Tech");
			$this->db->or_where("subcategory2", "Farming & Agri Tech");
			$this->db->or_where("subcategory3", "Farming & Agri Tech");
			$this->db->or_where("subcategory4", "Farming & Agri Tech");
			$this->db->or_where("subcategory5", "Farming & Agri Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGadget() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Home Gadgets");
			$this->db->or_where("subcategory2", "Home Gadgets");
			$this->db->or_where("subcategory3", "Home Gadgets");
			$this->db->or_where("subcategory4", "Home Gadgets");
			$this->db->or_where("subcategory5", "Home Gadgets");
			$this->db->group_end();
			$this->db->limit(3);
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getDefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence Tech");
			$this->db->or_where("subcategory2", "Defence Tech");
			$this->db->or_where("subcategory3", "Defence Tech");
			$this->db->or_where("subcategory4", "Defence Tech");
			$this->db->or_where("subcategory5", "Defence Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getDrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getEnergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getEnvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getPollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getPets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Pet Tech");
			$this->db->or_where("subcategory2", "Pet Tech");
			$this->db->or_where("subcategory3", "Pet Tech");
			$this->db->or_where("subcategory4", "Pet Tech");
			$this->db->or_where("subcategory5", "Pet Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAnimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getWomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getDifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getLgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getIncredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getFitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness Tech");
			$this->db->or_where("subcategory2", "Fitness Tech");
			$this->db->or_where("subcategory3", "Fitness Tech");
			$this->db->or_where("subcategory4", "Fitness Tech");
			$this->db->or_where("subcategory5", "Fitness Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getEnabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getYoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getCalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getDeactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getDrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getNatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getEcofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Fashion Tech");
			$this->db->or_where("subcategory2", "Fashion Tech");
			$this->db->or_where("subcategory3", "Fashion Tech");
			$this->db->or_where("subcategory4", "Fashion Tech");
			$this->db->or_where("subcategory5", "Fashion Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getKhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHerbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Food Tech");
			$this->db->or_where("subcategory2", "Food Tech");
			$this->db->or_where("subcategory3", "Food Tech");
			$this->db->or_where("subcategory4", "Food Tech");
			$this->db->or_where("subcategory5", "Food Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getCars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Auto Tech");
			$this->db->or_where("subcategory2", "Auto Tech");
			$this->db->or_where("subcategory3", "Auto Tech");
			$this->db->or_where("subcategory4", "Auto Tech");
			$this->db->or_where("subcategory5", "Auto Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Gaming & Sports");
			$this->db->or_where("subcategory2", "Gaming & Sports");
			$this->db->or_where("subcategory3", "Gaming & Sports");
			$this->db->or_where("subcategory4", "Gaming & Sports");
			$this->db->or_where("subcategory5", "Gaming & Sports");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSmart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getRural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getDisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getCampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getEducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solutions");
			$this->db->or_where("subcategory2", "Education Solutions");
			$this->db->or_where("subcategory3", "Education Solutions");
			$this->db->or_where("subcategory4", "Education Solutions");
			$this->db->or_where("subcategory5", "Education Solutions");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getCareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getFinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getChild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getChildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getParenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getElderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "English");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Hindi
		public function getHindidance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "Hindi");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindimusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "Hindi");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "Hindi");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHinditheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->where("language", "Hindi");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getHindipainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getHindicraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindibooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindifunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiarchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getHindihistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindimythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getHindimarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindipositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHinditech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiscience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindihow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindifarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindidefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getHindidrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindienergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindienvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindipollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindisanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindipets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindianimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiwomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindimen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindidifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHinditransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindilgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindimind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindimedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindifitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindienabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindicalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindideactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindidrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindinatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindikhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindihomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindicars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindisports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHinditravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindismart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindisafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindirural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindidisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindicampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindieducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindicareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindifinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindichild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindichildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindiparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getHindielderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Assamese
		public function getAssamesedance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesemusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesetheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getAssamesepainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getAssamesecraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesebooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesefunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesearchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getAssamesehistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesemythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getAssamesemarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesepositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesetech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesescience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesehow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesefarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesedefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getAssamesedrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseenergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseenvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesepollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesesanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesepets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseanimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesewomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesemen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesedifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesetransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameselgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesemind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesemedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesefitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseenabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesecalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesedeactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesedrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesenatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesekhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesehomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesecars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesesports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesetravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesesmart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesesafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameserural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesedisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesecampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseeducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesecareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesefinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesechild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesechildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssameseelderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Assamese");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		
		//Bangla
		public function getBangladance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglamusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglainstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglatheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getBanglapainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getBanglacraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglabooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglafunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaarchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getBanglahistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglamythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getBanglamarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglapositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglatech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglascience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglahow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglafarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBangladefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getBangladrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaenergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaenvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglapollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglasanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglapets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaanimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglawomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglamen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBangladifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglatransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglalgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglamind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglamedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglafitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaenabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglayoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglacalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBangladeactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBangladrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglanatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglakhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglahomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglacars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglasports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglatravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglasmart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglasafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglarural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBangladisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglacampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaeducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglacareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglafinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglachild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglachildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglaelderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Bangla");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Gujarati
		public function getGujaratidance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratimusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratitheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getGujaratipainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getGujaraticraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratibooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratifunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiarchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getGujaratihistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Hindi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratimythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getGujaratimarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaraticonstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratipositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratitech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiscience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratihow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratifarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratidefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getGujaratidrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratienergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratienvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratipollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratisanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratipets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratianimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiwomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratimen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratidifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratitransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratilgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratimind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratimedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratifitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratienabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaraticalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratideactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratidrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratinatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratikhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratihomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaraticars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratisports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratitravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratismart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratisafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratirural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratidisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaraticampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratieducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaraticareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratifinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratichild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratichildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratiparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujaratielderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Gujarati");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Marathi
		public function getMarathidance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathimusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathitheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getMarathipainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getMarathicraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathibooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathifunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiarchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getMarathihistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathimythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getMarathimarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathipositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathitech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiscience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathihow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathifarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathidefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getMarathidrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathienergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathienvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathipollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathisanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathipets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathianimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiwomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathimen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathidifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathitransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathilgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathimind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathimedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathifitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathienabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathicalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathideactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathidrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathinatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathikhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathihomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathicars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathisports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathitravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathismart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathisafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathirural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathidisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathicampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathieducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathicareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathifinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathichild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathichildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathiparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathielderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Marathi");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Tamil
		public function getTamildance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilmusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Music");
			$this->db->or_where("subcategory3", "Indian Classical Music");
			$this->db->or_where("subcategory4", "Indian Classical Music");
			$this->db->or_where("subcategory5", "Indian Classical Music");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamiltheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getTamilpainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getTamilcraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilbooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilfunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilarchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getTamilhistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilmythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getTamilmarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilpositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamiltech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilscience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilhow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilfarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamildefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getTamildrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilenergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilenvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilpollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilsanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilpets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilanimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilwomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamildifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamiltransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamillgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilmind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilmedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilfitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilenabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilcalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamildeactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamildrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilnatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilkhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilhomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilcars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilsports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamiltravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilsmart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilsafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilrural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamildisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilcampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamileducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilcareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilfinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilchild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilchildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilelderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Tamil");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		//Telugu
		public function getTelugudance() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Dance");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugumusic() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Music");
			$this->db->or_where("subcategory2", "Indian Classical Dance");
			$this->db->or_where("subcategory3", "Indian Classical Dance");
			$this->db->or_where("subcategory4", "Indian Classical Dance");
			$this->db->or_where("subcategory5", "Indian Classical Dance");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguinstruments() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Classical Instruments");
			$this->db->or_where("subcategory2", "Indian Classical Instruments");
			$this->db->or_where("subcategory3", "Indian Classical Instruments");
			$this->db->or_where("subcategory4", "Indian Classical Instruments");
			$this->db->or_where("subcategory5", "Indian Classical Instruments");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugutheatre() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Theatre");
			$this->db->or_where("subcategory2", "Indian Theatre");
			$this->db->or_where("subcategory3", "Indian Theatre");
			$this->db->or_where("subcategory4", "Indian Theatre");
			$this->db->or_where("subcategory5", "Indian Theatre");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();	
		}

		public function getTelugupainters() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Painters");
			$this->db->or_where("subcategory2", "Indian Painters");
			$this->db->or_where("subcategory3", "Indian Painters");
			$this->db->or_where("subcategory4", "Indian Painters");
			$this->db->or_where("subcategory5", "Indian Painters");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();

			return $query->result();
		}

		public function getTelugucraftsmen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Indian Craftsmen");
			$this->db->or_where("subcategory2", "Indian Craftsmen");
			$this->db->or_where("subcategory3", "Indian Craftsmen");
			$this->db->or_where("subcategory4", "Indian Craftsmen");
			$this->db->or_where("subcategory5", "Indian Craftsmen");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugubooks() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Books, Comics & Authors");
			$this->db->or_where("subcategory2", "Books, Comics & Authors");
			$this->db->or_where("subcategory3", "Books, Comics & Authors");
			$this->db->or_where("subcategory4", "Books, Comics & Authors");
			$this->db->or_where("subcategory5", "Books, Comics & Authors");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugufunfest() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fun Festivals");
			$this->db->or_where("subcategory2", "Fun Festivals");
			$this->db->or_where("subcategory3", "Fun Festivals");
			$this->db->or_where("subcategory4", "Fun Festivals");
			$this->db->or_where("subcategory5", "Fun Festivals");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugurchaeology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Archaeology");
			$this->db->or_where("subcategory2", "Archaeology");
			$this->db->or_where("subcategory3", "Archaeology");
			$this->db->or_where("subcategory4", "Archaeology");
			$this->db->or_where("subcategory5", "Archaeology");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}		

		public function getTeluguhistory() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "History");
			$this->db->or_where("subcategory2", "History");
			$this->db->or_where("subcategory3", "History");
			$this->db->or_where("subcategory4", "History");
			$this->db->or_where("subcategory5", "History");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugumythology() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mythology");
			$this->db->or_where("subcategory2", "Mythology");
			$this->db->or_where("subcategory3", "Mythology");
			$this->db->or_where("subcategory4", "Mythology");
			$this->db->or_where("subcategory5", "Mythology");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getTelugumarvels() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Engineering Marvels");
			$this->db->or_where("subcategory2", "Engineering Marvels");
			$this->db->or_where("subcategory3", "Engineering Marvels");
			$this->db->or_where("subcategory4", "Engineering Marvels");
			$this->db->or_where("subcategory5", "Engineering Marvels");
          	$this->db->order_by("vid.id","DESC");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguastronomy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Space & Astronomy");
			$this->db->or_where("subcategory2", "Space & Astronomy");
			$this->db->or_where("subcategory3", "Space & Astronomy");
			$this->db->or_where("subcategory4", "Space & Astronomy");
			$this->db->or_where("subcategory5", "Space & Astronomy");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguconstruction() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Construction Techniques");
			$this->db->or_where("subcategory2", "Construction Techniques");
			$this->db->or_where("subcategory3", "Construction Techniques");
			$this->db->or_where("subcategory4", "Construction Techniques");
			$this->db->or_where("subcategory5", "Construction Techniques");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugupositive() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Positive Gaming");
			$this->db->or_where("subcategory2", "Positive Gaming");
			$this->db->or_where("subcategory3", "Positive Gaming");
			$this->db->or_where("subcategory4", "Positive Gaming");
			$this->db->or_where("subcategory5", "Positive Gaming");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugutech() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Tech That Cares");
			$this->db->or_where("subcategory2", "Tech That Cares");
			$this->db->or_where("subcategory3", "Tech That Cares");
			$this->db->or_where("subcategory4", "Tech That Cares");
			$this->db->or_where("subcategory5", "Tech That Cares");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguscience() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Science Wonders");
			$this->db->or_where("subcategory2", "Science Wonders");
			$this->db->or_where("subcategory3", "Science Wonders");
			$this->db->or_where("subcategory4", "Science Wonders");
			$this->db->or_where("subcategory5", "Science Wonders");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguhow() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "How To?");
			$this->db->or_where("subcategory2", "How To?");
			$this->db->or_where("subcategory3", "How To?");
			$this->db->or_where("subcategory4", "How To?");
			$this->db->or_where("subcategory5", "How To?");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugufarming() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Farming Innovations");
			$this->db->or_where("subcategory2", "Farming Innovations");
			$this->db->or_where("subcategory3", "Farming Innovations");
			$this->db->or_where("subcategory4", "Farming Innovations");
			$this->db->or_where("subcategory5", "Farming Innovations");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugudefence() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Defence");
			$this->db->or_where("subcategory2", "Defence");
			$this->db->or_where("subcategory3", "Defence");
			$this->db->or_where("subcategory4", "Defence");
			$this->db->or_where("subcategory5", "Defence");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}


		public function getTelugudrinking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drinking Water Solution");
			$this->db->or_where("subcategory2", "Drinking Water Solution");
			$this->db->or_where("subcategory3", "Drinking Water Solution");
			$this->db->or_where("subcategory4", "Drinking Water Solution");
			$this->db->or_where("subcategory5", "Drinking Water Solution");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguenergy() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Energy Alternatives");
			$this->db->or_where("subcategory2", "Energy Alternatives");
			$this->db->or_where("subcategory3", "Energy Alternatives");
			$this->db->or_where("subcategory4", "Energy Alternatives");
			$this->db->or_where("subcategory5", "Energy Alternatives");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguenvironment() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Environment Solution");
			$this->db->or_where("subcategory2", "Environment Solution");
			$this->db->or_where("subcategory3", "Environment Solution");
			$this->db->or_where("subcategory4", "Environment Solution");
			$this->db->or_where("subcategory5", "Environment Solution");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugupollution() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pollution Remedies");
			$this->db->or_where("subcategory2", "Pollution Remedies");
			$this->db->or_where("subcategory3", "Pollution Remedies");
			$this->db->or_where("subcategory4", "Pollution Remedies");
			$this->db->or_where("subcategory5", "Pollution Remedies");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugusanitation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sanitation Solution");
			$this->db->or_where("subcategory2", "Sanitation Solution");
			$this->db->or_where("subcategory3", "Sanitation Solution");
			$this->db->or_where("subcategory4", "Sanitation Solution");
			$this->db->or_where("subcategory5", "Sanitation Solution");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugupets() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Pets");
			$this->db->or_where("subcategory2", "Pets");
			$this->db->or_where("subcategory3", "Pets");
			$this->db->or_where("subcategory4", "Pets");
			$this->db->or_where("subcategory5", "Pets");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguanimal() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Animal Care");
			$this->db->or_where("subcategory2", "Animal Care");
			$this->db->or_where("subcategory3", "Animal Care");
			$this->db->or_where("subcategory4", "Animal Care");
			$this->db->or_where("subcategory5", "Animal Care");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguwomen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Women");
			$this->db->or_where("subcategory2", "Women");
			$this->db->or_where("subcategory3", "Women");
			$this->db->or_where("subcategory4", "Women");
			$this->db->or_where("subcategory5", "Women");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugumen() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Men");
			$this->db->or_where("subcategory2", "Men");
			$this->db->or_where("subcategory3", "Men");
			$this->db->or_where("subcategory4", "Men");
			$this->db->or_where("subcategory5", "Men");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugudifferentlyabled() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Differently Abled");
			$this->db->or_where("subcategory2", "Differently Abled");
			$this->db->or_where("subcategory3", "Differently Abled");
			$this->db->or_where("subcategory4", "Differently Abled");
			$this->db->or_where("subcategory5", "Differently Abled");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugutransgender() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Transgenders");
			$this->db->or_where("subcategory2", "Transgenders");
			$this->db->or_where("subcategory3", "Transgenders");
			$this->db->or_where("subcategory4", "Transgenders");
			$this->db->or_where("subcategory5", "Transgenders");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugulgbt() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "LGBT");
			$this->db->or_where("subcategory2", "LGBT");
			$this->db->or_where("subcategory3", "LGBT");
			$this->db->or_where("subcategory4", "LGBT");
			$this->db->or_where("subcategory5", "LGBT");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguincredible() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Incredible Indians");
			$this->db->or_where("subcategory2", "Incredible Indians");
			$this->db->or_where("subcategory3", "Incredible Indians");
			$this->db->or_where("subcategory4", "Incredible Indians");
			$this->db->or_where("subcategory5", "Incredible Indians");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugumind() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Mind & Sleep");
			$this->db->or_where("subcategory2", "Mind & Sleep");
			$this->db->or_where("subcategory3", "Mind & Sleep");
			$this->db->or_where("subcategory4", "Mind & Sleep");
			$this->db->or_where("subcategory5", "Mind & Sleep");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugumedical() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Medical Breakthrough");
			$this->db->or_where("subcategory2", "Medical Breakthrough");
			$this->db->or_where("subcategory3", "Medical Breakthrough");
			$this->db->or_where("subcategory4", "Medical Breakthrough");
			$this->db->or_where("subcategory5", "Medical Breakthrough");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugufitness() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Fitness");
			$this->db->or_where("subcategory2", "Fitness");
			$this->db->or_where("subcategory3", "Fitness");
			$this->db->or_where("subcategory4", "Fitness");
			$this->db->or_where("subcategory5", "Fitness");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguenabling() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Enabling Specially Abled");
			$this->db->or_where("subcategory2", "Enabling Specially Abled");
			$this->db->or_where("subcategory3", "Enabling Specially Abled");
			$this->db->or_where("subcategory4", "Enabling Specially Abled");
			$this->db->or_where("subcategory5", "Enabling Specially Abled");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguyoga() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Yoga");
			$this->db->or_where("subcategory2", "Yoga");
			$this->db->or_where("subcategory3", "Yoga");
			$this->db->or_where("subcategory4", "Yoga");
			$this->db->or_where("subcategory5", "Yoga");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugucalm() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Calm & Meditation");
			$this->db->or_where("subcategory2", "Calm & Meditation");
			$this->db->or_where("subcategory3", "Calm & Meditation");
			$this->db->or_where("subcategory4", "Calm & Meditation");
			$this->db->or_where("subcategory5", "Calm & Meditation");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugudeactivating() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "De-activating Tech");
			$this->db->or_where("subcategory2", "De-activating Tech");
			$this->db->or_where("subcategory3", "De-activating Tech");
			$this->db->or_where("subcategory4", "De-activating Tech");
			$this->db->or_where("subcategory5", "De-activating Tech");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugudrugs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Drugs as Medicine");
			$this->db->or_where("subcategory2", "Drugs as Medicine");
			$this->db->or_where("subcategory3", "Drugs as Medicine");
			$this->db->or_where("subcategory4", "Drugs as Medicine");
			$this->db->or_where("subcategory5", "Drugs as Medicine");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugunatural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Natural Beauty");
			$this->db->or_where("subcategory2", "Natural Beauty");
			$this->db->or_where("subcategory3", "Natural Beauty");
			$this->db->or_where("subcategory4", "Natural Beauty");
			$this->db->or_where("subcategory5", "Natural Beauty");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguecofriendly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory2", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory3", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory4", "Eco-Friendly Fashion");
			$this->db->or_where("subcategory5", "Eco-Friendly Fashion");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugukhadi() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Khadi & Handloom");
			$this->db->or_where("subcategory2", "Khadi & Handloom");
			$this->db->or_where("subcategory3", "Khadi & Handloom");
			$this->db->or_where("subcategory4", "Khadi & Handloom");
			$this->db->or_where("subcategory5", "Khadi & Handloom");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguherbs() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Herbs & Food");
			$this->db->or_where("subcategory2", "Herbs & Food");
			$this->db->or_where("subcategory3", "Herbs & Food");
			$this->db->or_where("subcategory4", "Herbs & Food");
			$this->db->or_where("subcategory5", "Herbs & Food");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguhomecare() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Homecare & Gardening");
			$this->db->or_where("subcategory2", "Homecare & Gardening");
			$this->db->or_where("subcategory3", "Homecare & Gardening");
			$this->db->or_where("subcategory4", "Homecare & Gardening");
			$this->db->or_where("subcategory5", "Homecare & Gardening");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugucars() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Cars & Bikes");
			$this->db->or_where("subcategory2", "Cars & Bikes");
			$this->db->or_where("subcategory3", "Cars & Bikes");
			$this->db->or_where("subcategory4", "Cars & Bikes");
			$this->db->or_where("subcategory5", "Cars & Bikes");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugusports() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Sports & Athletics");
			$this->db->or_where("subcategory2", "Sports & Athletics");
			$this->db->or_where("subcategory3", "Sports & Athletics");
			$this->db->or_where("subcategory4", "Sports & Athletics");
			$this->db->or_where("subcategory5", "Sports & Athletics");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugutravel() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Travel & Adventure Tech");
			$this->db->or_where("subcategory2", "Travel & Adventure Tech");
			$this->db->or_where("subcategory3", "Travel & Adventure Tech");
			$this->db->or_where("subcategory4", "Travel & Adventure Tech");
			$this->db->or_where("subcategory5", "Travel & Adventure Tech");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugusmart() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Smart Cities");
			$this->db->or_where("subcategory2", "Smart Cities");
			$this->db->or_where("subcategory3", "Smart Cities");
			$this->db->or_where("subcategory4", "Smart Cities");
			$this->db->or_where("subcategory5", "Smart Cities");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugusafety() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Safety Tools");
			$this->db->or_where("subcategory2", "Safety Tools");
			$this->db->or_where("subcategory3", "Safety Tools");
			$this->db->or_where("subcategory4", "Safety Tools");
			$this->db->or_where("subcategory5", "Safety Tools");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugurural() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Rural Initiatives");
			$this->db->or_where("subcategory2", "Rural Initiatives");
			$this->db->or_where("subcategory3", "Rural Initiatives");
			$this->db->or_where("subcategory4", "Rural Initiatives");
			$this->db->or_where("subcategory5", "Rural Initiatives");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugudisaster() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Disaster management");
			$this->db->or_where("subcategory2", "Disaster management");
			$this->db->or_where("subcategory3", "Disaster management");
			$this->db->or_where("subcategory4", "Disaster management");
			$this->db->or_where("subcategory5", "Disaster management");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugucampus() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Campus Life");
			$this->db->or_where("subcategory2", "Campus Life");
			$this->db->or_where("subcategory3", "Campus Life");
			$this->db->or_where("subcategory4", "Campus Life");
			$this->db->or_where("subcategory5", "Campus Life");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugueducation() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Education Solution");
			$this->db->or_where("subcategory2", "Education Solution");
			$this->db->or_where("subcategory3", "Education Solution");
			$this->db->or_where("subcategory4", "Education Solution");
			$this->db->or_where("subcategory5", "Education Solution");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugucareers() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Careers & Mentorship");
			$this->db->or_where("subcategory2", "Careers & Mentorship");
			$this->db->or_where("subcategory3", "Careers & Mentorship");
			$this->db->or_where("subcategory4", "Careers & Mentorship");
			$this->db->or_where("subcategory5", "Careers & Mentorship");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTelugufinancial() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Financial Learnings");
			$this->db->or_where("subcategory2", "Financial Learnings");
			$this->db->or_where("subcategory3", "Financial Learnings");
			$this->db->or_where("subcategory4", "Financial Learnings");
			$this->db->or_where("subcategory5", "Financial Learnings");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguchild() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Labour");
			$this->db->or_where("subcategory2", "Child Labour");
			$this->db->or_where("subcategory3", "Child Labour");
			$this->db->or_where("subcategory4", "Child Labour");
			$this->db->or_where("subcategory5", "Child Labour");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguchildtrafficking() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Child Trafficking");
			$this->db->or_where("subcategory2", "Child Trafficking");
			$this->db->or_where("subcategory3", "Child Trafficking");
			$this->db->or_where("subcategory4", "Child Trafficking");
			$this->db->or_where("subcategory5", "Child Trafficking");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguparenting() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Parenting");
			$this->db->or_where("subcategory2", "Parenting");
			$this->db->or_where("subcategory3", "Parenting");
			$this->db->or_where("subcategory4", "Parenting");
			$this->db->or_where("subcategory5", "Parenting");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguelderly() {
            $this->db->select("vid.*");
			$this->db->from("video as vid");
			$this->db->group_start();
			$this->db->where("subcategory", "Elderly");
			$this->db->or_where("subcategory2", "Elderly");
			$this->db->or_where("subcategory3", "Elderly");
			$this->db->or_where("subcategory4", "Elderly");
			$this->db->or_where("subcategory5", "Elderly");
			$this->db->group_end();
			$this->db->where("language", "Telugu");
          	$this->db->order_by("vid.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function categoryArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
			$this->db->where("language", "English");
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguArtists() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Artists");
			$this->db->or_where("category2", "Artists");
			$this->db->or_where("category3", "Artists");
			$this->db->or_where("category4", "Artists");
			$this->db->or_where("category5", "Artists");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguChild() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Child");
			$this->db->or_where("category2", "Child");
			$this->db->or_where("category3", "Child");
			$this->db->or_where("category4", "Child");
			$this->db->or_where("category5", "Child");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguScience() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Science");
			$this->db->or_where("category2", "Science");
			$this->db->or_where("category3", "Science");
			$this->db->or_where("category4", "Science");
			$this->db->or_where("category5", "Science");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssamesePets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguPets() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Pets");
			$this->db->or_where("category2", "Pets");
			$this->db->or_where("category3", "Pets");
			$this->db->or_where("category4", "Pets");
			$this->db->or_where("category5", "Pets");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssamesePeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguPeople() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "People");
			$this->db->or_where("category2", "People");
			$this->db->or_where("category3", "People");
			$this->db->or_where("category4", "People");
			$this->db->or_where("category5", "People");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguHealth() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Health");
			$this->db->or_where("category2", "Health");
			$this->db->or_where("category3", "Health");
			$this->db->or_where("category4", "Health");
			$this->db->or_where("category5", "Health");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguLifestyle() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Lifestyle");
			$this->db->or_where("category2", "Lifestyle");
			$this->db->or_where("category3", "Lifestyle");
			$this->db->or_where("category4", "Lifestyle");
			$this->db->or_where("category5", "Lifestyle");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "English");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryAssameseEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Assamese");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryBanglaEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Bangla");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryGujaratiEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Gujarati");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryHindiEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Hindi");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryMarathiEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Marathi");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTamilEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Tamil");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}

		public function categoryTeluguEducation() {
			$this->db->select("vid.*");
			$this->db->from("video as vid");
            $this->db->where("language", "Telugu");
            $this->db->group_start();
			$this->db->where("category", "Education");
			$this->db->or_where("category2", "Education");
			$this->db->or_where("category3", "Education");
			$this->db->or_where("category4", "Education");
			$this->db->or_where("category5", "Education");
			$this->db->group_end();
          	$this->db->order_by("vid.id","DESC");
          	//print $this->db->last_query(); exit();
			$query = $this->db->get();
			return $query->result();
		}
	}
