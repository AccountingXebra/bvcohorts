<?php
	class Viewvideos_model extends CI_Model {

		public function getPosts() {

			$query = $this->db->get('video');
			
			return $query->result();
		}

		public function getVideos() {
            $this->db->select("ep.*");
			$this->db->from("video as ep");
          	$this->db->order_by("ep.id","DESC");
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getSinglePosts($id) {
			$query = $this->db->get_where('video', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

		public function updatePost($data, $id) {
			return $this->db->where('id', $id)
						->update('video', $data);
		}

		public function deletePosts($id) {
			return $this->db->delete('video', ['id'=>$id]);
		}
	}