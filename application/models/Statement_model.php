<?php
class Statement_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}
	public function MyinvoicesFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode,scd.cd_grant_total,scd.cd_reason,scd.cd_note_date,scd.cd_note_no");
		$this->db->from('sales_invoices as si');
		$this->db->join('sales_credit_debit as scd', 'scd.cd_invoice_no = si.inv_invoice_no','left');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		
		if($Data['search_by_status']!=''){
			$this->db->where("si.inv_status",$Data['search_by_status']);
		}
		if($Data['inv_start_date']!=''){
			$this->db->where("si.inv_invoice_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}
		if($Data['inv_end_date']!=''){
			$this->db->where("si.inv_invoice_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}
			

		//$this->db->where("si.reg_id",$Data['reg_id']);
		$this->db->where("si.bus_id",$Data['bus_id']);
		$this->db->where("si.cust_id",$Data['cust_id']);
		
		$this->db->where("si.status","Active");
		
		$this->db->group_by("si.inv_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}

			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	public function MyinvoicesFilterSOA($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("si.inv_id as inv_id,si.inv_invoice_date as inv_invoice_date,si.inv_invoice_no as inv_invoice_no,si.inv_grant_total as inv_grant_total,si.status as status,si.cust_id as cust_id,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode,scd.cd_grant_total as cd_grant_total,scd.cd_reason as cd_reason ,scd.cd_note_date as cd_note_date,scd.cd_note_no as cd_note_no ");
		$this->db->from('sales_invoices as si');
		$this->db->join('sales_credit_debit as scd', 'scd.cd_invoice_no = si.inv_invoice_no','left');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		
		 if($Data['search_by_status']!=''){
		 	$this->db->where("si.inv_status",$Data['search_by_status']);
		 }
		 if($Data['inv_start_date']!=''){
		 	$this->db->where("si.inv_invoice_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		 }
		 if($Data['inv_end_date']!=''){
		 	$this->db->where("si.inv_invoice_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		 }
			
         
		//$this->db->where("si.reg_id",$Data['reg_id']);
		$this->db->where("si.bus_id",$Data['bus_id']);
		//$this->db->where("si.cust_id",$Data['cust_id']);
		$this->db->where("si.inv_document_type","Sales Invoice");
		$this->db->where("si.status","Active");

		
		$this->db->group_by("si.inv_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");
		
		 $this->db->get(); 
    	 $query1 = $this->db->last_query();

     /* if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}

			$query = $this->db->get();
			
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}*/
		//////////////////////////////////////////////////////


        $this->db->select("ce.exp_id as inv_id,ce.ce_date as inv_invoice_date,ce.ce_number as inv_invoice_no,ce.ce_grandtotal as inv_grant_total,ce.status as status,ce.ce_vendorname as cust_id,c.country_id,c.country_name,s.state_id,s.state_name,ven.vendor_name as cust_name,ven.vendor_billing_country as cust_billing_country,ven.vendor_billing_state as cust_billing_state,ven.vendor_currency as cust_currency,cur.currency_id,cur.currencycode,dbn.debit_grant_total as cd_grant_total,dbn.debit_reason as cd_reason,dbn.debit_note_date as cd_note_date,dbn.debit_note_no as cd_note_no");
		$this->db->from('company_expense as ce');
		$this->db->join('expense_debit_note as dbn', 'dbn.debit_invoice_no = ce.ce_number','left');
		$this->db->join('countries as c', 'c.country_id = ce.ce_supplyplace' ,'left');
		$this->db->join('states as s', 's.state_id = ce.ce_supplyplace','left');
		$this->db->join('expense_vendors as ven', 'ven.vendor_id = ce.ce_vendorname','left');
		$this->db->join('currency as cur', 'cur.currency_id = ven.vendor_currency','left');
		
		
		if($Data['inv_start_date']!=''){
			$this->db->where("ce.ce_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}
		if($Data['inv_end_date']!=''){
			$this->db->where("ce.ce_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}
			


		$this->db->where("ce.bus_id",$Data['bus_id']);
		//$this->db->where("ce.ce_vendorname",$Data['cust_id']);
		
		$this->db->where("ce.status","Active");
		
		$this->db->group_by("ce.exp_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");

        $this->db->get(); 
    	 $query2 = $this->db->last_query();


      $query = $this->db->query($query1." UNION ALL ".$query2 .' ORDER BY '.$sort_field.' '. $orderBy);



		/////////////////////////////////////////////////////////

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}

			//$query = $this->db->get();
			
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			//$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function printsoa($reg_id,$bus_id,$cust_id,$start_date='',$end_date='')
	{
		$this->db->select("si.inv_id as inv_id,si.inv_invoice_date as inv_invoice_date,si.inv_invoice_no as inv_invoice_no,si.inv_grant_total as inv_grant_total,si.status as status,si.cust_id as cust_id,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode,scd.cd_grant_total as cd_grant_total,scd.cd_reason as cd_reason ,scd.cd_note_date as cd_note_date,scd.cd_note_no as cd_note_no ");
		$this->db->from('sales_invoices as si');
		$this->db->join('sales_credit_debit as scd', 'scd.cd_invoice_no = si.inv_invoice_no','left');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		
		//$this->db->where("si.reg_id",$reg_id);
		$this->db->where("si.bus_id",$bus_id);
		//his->db->where("si.cust_id",$cust_id);
		$this->db->where("si.inv_document_type","Sales Invoice");
		$this->db->where("si.status","Active");
		
		$this->db->group_by("si.inv_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");

        
		 $this->db->get(); 
    	 $query1 = $this->db->last_query();
		  $this->db->select("ce.exp_id as inv_id,ce.ce_date as inv_invoice_date,ce.ce_number as inv_invoice_no,ce.ce_grandtotal as inv_grant_total,ce.status as status,ce.ce_vendorname as cust_id,c.country_id,c.country_name,s.state_id,s.state_name,ven.vendor_name as cust_name,ven.vendor_billing_country as cust_billing_country,ven.vendor_billing_state as cust_billing_state,ven.vendor_currency as cust_currency,cur.currency_id,cur.currencycode,dbn.debit_grant_total as cd_grant_total,dbn.debit_reason as cd_reason,dbn.debit_note_date as cd_note_date,dbn.debit_note_no as cd_note_no");
		$this->db->from('company_expense as ce');
		$this->db->join('expense_debit_note as dbn', 'dbn.debit_invoice_no = ce.ce_number','left');
		$this->db->join('countries as c', 'c.country_id = ce.ce_supplyplace' ,'left');
		$this->db->join('states as s', 's.state_id = ce.ce_supplyplace','left');
		$this->db->join('expense_vendors as ven', 'ven.vendor_id = ce.ce_vendorname','left');
		$this->db->join('currency as cur', 'cur.currency_id = ven.vendor_currency','left');
		
		
		/*if($Data['inv_start_date']!=''){
			$this->db->where("ce.ce_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}
		if($Data['inv_end_date']!=''){
			$this->db->where("ce.ce_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}*/
			


		$this->db->where("ce.bus_id",$bus_id);
		//$this->db->where("ce.ce_vendorname",$Data['cust_id']);
		
		$this->db->where("ce.status","Active");
		
		$this->db->group_by("ce.exp_id");
		//$this->db->order_by("".$sort_field." ".$orderBy."");

        $this->db->get(); 
    	 $query2 = $this->db->last_query();


      $query = $this->db->query($query1." UNION ALL ".$query2 .' ORDER BY inv_id DESC' );
		$userData = $query->result_array();
		
		//return $result;		
		$iTotalRecords = count($userData);
		$records = array();
	  	$records["data"] = array();
		$count=0;$balance_amt=0;
	  	foreach ($userData as $key => $value) {
	  		$count++;
	  		$check='<input type="checkbox" id="filled-in-box'.$value['inv_id'].'" name="filled-in-box'.$value['inv_id'].'" class="filled-in purple soa_bulk_action" data-type="inv" value="'.$value['inv_id'].'"/> <label for="filled-in-box'.$value['inv_id'].'"></label>';
	  			$cust_name=$value['cust_name'];
	  			
				$inv_invoice_no='';
				if($value['inv_invoice_no']!='')
				{
					
					$inv_invoice_no =$value['inv_invoice_no'];
					
				}
				$inv_date='';
				if($value['inv_invoice_date']!='')
				{
					
					$dateTime = date("d-m-Y",  strtotime($value['inv_invoice_date']));
					if($value['inv_invoice_date']=='0000-00-00'){
						$dateTime='';
					}
					$inv_date = str_replace("-","/",$dateTime);
				}
			

		
			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="active_sales_invoice" data-inv_id='.$value["inv_id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{
				
				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_sales_invoice" data-inv_id="'.$value["inv_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				$cls='active_record';
				$cls1='activate';
				
			}
			
			/*$str="edit-";
			$str.=strtolower(str_replace(" ","-",$value['inv_document_type']));
			//$str+=strtolower(str_replace("-"," ",$value['inv_document_type']));
			$conv_to_inv='';
			if($value['inv_document_type']=="Estimate Invoice"){
			$conv_to_inv='<li><a href="javascript:voice(0);"><img class="icon-img" src="'.base_url().'public/icons/convert_invoice.png">Convert to Invoice</a></li>';
			}
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["inv_id"]."'><i class='action-dot'></i></div>

			 	<ul id='dropdown".$value["inv_id"]."' class='dropdown-content'>
					".'
					<li><a href="javascript:void(0);" onclick="email_cinvoice('.$value["inv_id"].');"><i class="material-icons">email</i>Email</a></li>
                    <li><a href='.base_url().'client-portal/download-cinvoice/'.$value["inv_id"].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;height: 20px;">Export</a></li>
					<li><a href='.base_url().'client-portal/print-cinvoice/'.$value["inv_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>
				</ul>';*/
			$balance_amt=$balance_amt+$value['inv_grant_total'];
		
	  		$records["data"][] = array(
				//$check,
	  			$inv_date,
	  			$inv_invoice_no,
	  			'',
	  			round($value['inv_grant_total']),
	  			'NA',
	  			'NA',
	  			round($balance_amt),
	  			
	  			);

	  		
	  		// Creadit Note

	  		$cd_note = $this->Sales_model->selectData('sales_credit_debit', '*', array('cd_invoice_no'=>$value['inv_invoice_no'],'status'=>'Active','cust_id'=>$value['cust_id']));	


	  		for($i = 0; $i < count($cd_note); $i++)
	  		{

		  		if(count($cd_note)>0){
		  			
		  			// echo '<pre>';
		  			// print_r($cd_note);
		  			// echo '</pre>';
		  			// exit();
		  			
		  			$scheck='<input type="checkbox" id="filled-in-box'.$cd_note[$i]->cd_id.'" name="filled-in-box'.$cd_note[$i]->cd_id.'" data-type="cd_note" class="filled-in purple soa_bulk_action" value="'.$cd_note[$i]->cd_id.'"/> <label for="filled-in-box'.$cd_note[$i]->cd_id.'"></label>';

		  			$cd_note_date='';

		  			if($cd_note[$i]->cd_note_date !='')
					{
		
						$dateTime = date("d-m-Y",  strtotime($cd_note[$i]->cd_note_date));
						if($cd_note[$i]->cd_note_date=='0000-00-00'){
							$dateTime='';
						}
						$cd_note_date = str_replace("-","/",$dateTime);
			
					}
			
					$balance_amt=$balance_amt-$cd_note[$i]->cd_grant_total;
		  			$records["data"][] = array(
					//$scheck,
		  			$cd_note_date,
		  			$cd_note[$i]->cd_note_no,
		  			'CREDIT NOTE FOR '.$value['inv_invoice_no'],
		  			round($value['inv_grant_total']),
		  			round($cd_note[$i]->cd_grant_total),
		  			'',
		  			number_format((float)$balance_amt, 0, '.', ''),
		  				
		  			);
		  	
		  		}

	  		}
	  		// Sales Recipts
	  		
	  		$sreceipts=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value['inv_id'],'status'=>'Active'));
	  		for($i=0; $i< count($sreceipts); $i++)
	  		{	

		  		if(count($sreceipts)>0){
		  			
		  			$srec=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$sreceipts[$i]->srec_id,'status'=>'Active'));	
		  			
		  			if($srec)
		  			{

		  			$scheck='<input type="checkbox" id="filled-in-box'.$sreceipts[$i]->srecl_id.'" name="filled-in-box'.$sreceipts[0]->srecl_id.'" data-type="srec" class="filled-in purple soa_bulk_action" value="'.$sreceipts[$i]->srecl_id.'"/> <label for="filled-in-box'.$sreceipts[$i]->srecl_id.'"></label>';
		  			$srec_date='';

		  			if($srec[0]->srec_date !='')
					{
		
						$dateTime = date("d-m-Y",  strtotime($srec[0]->srec_date));
						if($srec[0]->srec_date=='0000-00-00'){
							$dateTime='';
						}
						$srec_date = str_replace("-","/",$dateTime);
			
					}


					if($srec[0]->srec_tds_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_tds_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'TDS FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_tds_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  			if($srec[0]->srec_baddebts_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_baddebts_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'BADDEBTS FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_baddebts_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}
			
					if($srec[0]->srec_amount>0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_amount;
				  			$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'SALES RECEIPT FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_amount),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}
		  			}
		  	
		  		}

		  	}



	  	
	  		//}
	  	}

	  	//----------------------------------------------------------------------------

		$res = $records["data"];
		

		function date_compare($a, $b)
		{
		    $t1 = strtotime(str_replace("/","-",$a[0]));
		    $t2 = strtotime(str_replace("/","-",$b[0]));
		    return $t1 - $t2;
		}    
		usort($res, 'date_compare');

		$bal = 0;
		for($i=0;$i<count($res);$i++)
		{
			
			if($res[$i][5] == 'NA' && $res[$i][4] == 'NA')
			{
				$res[$i][6] = $bal + $res[$i][3];
				$bal = $res[$i][6];
				$res[$i][5] = '';
				$res[$i][4] = '';

			}elseif ($res[$i][5] == '' && $res[$i][4] != '') {
				
				$res[$i][6] = $bal - $res[$i][4];
				$bal = $res[$i][6];

			}elseif ($res[$i][5] != '' && $res[$i][4] == '') {
				
				$res[$i][6] = $bal - $res[$i][5];
				$bal = $res[$i][6];
			}
		}

		//----------------------------------------------------------------------------

		$post['inv_start_date'] = $start_date;
		$post['inv_end_date'] = $end_date;
		$newres = array();
		if(@$post['inv_start_date'] != '' && @$post['inv_end_date'] != '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) >= strtotime(str_replace("/","-",$post['inv_start_date'])) && strtotime(str_replace("/","-",$res[$i][0])) <= strtotime(str_replace("/","-",$post['inv_end_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}elseif(@$post['inv_start_date'] != '' && @$post['inv_end_date'] == '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) >= strtotime(str_replace("/","-",$post['inv_start_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}elseif(@$post['inv_start_date'] == '' && @$post['inv_end_date'] != '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) <= strtotime(str_replace("/","-",$post['inv_end_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}
		


		//----------------------------------------------------------------------------
		if(@$post['inv_start_date'] != '' || @$post['inv_end_date'] != '')
		{
			$records["data"] = $newres;
		}else{
			$records["data"] = $res;
		}
        
		return $records;	

	}

	public function export_customer_invoice($array,$reg_id,$bus_id,$cus_id){

		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_invoices as si');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->where("si.reg_id",$reg_id);
		$this->db->where("si.bus_id",$bus_id);
		$this->db->where("si.cust_id",$cus_id);
		$this->db->where_in("si.inv_id",$array);
		$query = $this->db->get();
			//print_r($this->db->last_query());exit;
		$result= $query->result_array();
			
		return $result;			
	}
	public function paymentFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("sr.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cur.currency,cur.currency_id,cur.currencycode,srl.srecl_id,srl.srecl_inv_no,srl.srecl_inv_amt,inv.inv_id,inv.inv_invoice_no");
		$this->db->from('sales_receipts as sr');
		$this->db->join('sales_customers as cus', 'cus.cust_id = sr.cust_id','left');
		$this->db->join('countries as c', 'c.country_id = cus.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = cus.cust_billing_state','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('(SELECT * FROM sales_receipts_list ORDER BY srecl_id ASC LIMIT 10000000000000000000) srl', 'srl.srec_id = sr.srec_id','left');
		$this->db->join('sales_invoices as inv', 'inv.inv_id = srl.srecl_inv_no','left');
		if($Data['search_by_status']!=''){
			$this->db->where("sr.srec_status",$Data['search_by_status']);
		}
		
		if($Data['srec_start_date']!=''){
			$this->db->where("sr.srec_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['srec_start_date']))));
		}
		if($Data['srec_end_date']!=''){
			$this->db->where("sr.srec_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['srec_end_date']))));
		}
			
		$this->db->where("sr.reg_id",$Data['reg_id']);
		$this->db->where("sr.bus_id",$Data['bus_id']);
		$this->db->where("sr.cust_id",$Data['cust_id']);
		
		
		$this->db->where("sr.status","Active");
		
		$this->db->group_by("sr.srec_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}

			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	
}?>