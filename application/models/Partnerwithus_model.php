<?php
	class Partnerwithus_model extends CI_Model {

		function __construct() {
	        // Set table name
	        $this->table = 'partner_with_us';
    	}

    	function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("conditions", $params)){
            foreach($params['conditions'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(!empty($params['searchKeyword'])){
            $search = $params['searchKeyword'];
            $likeArr = array('name' => $search, 'email' => $search, 'organisation' => $search, 'purpose' => $search);
            $this->db->like($likeArr);
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'DESC');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }

		//Inserting into database
		public function addData($data) {
			return $this->db->insert('partner_with_us', $data);
		}

		public function getPartners() {
			$query = $this->db->get('partner_with_us');

			return $query->result();
		}

		public function getSinglePartner($id) {
			$query = $this->db->get_where('partner_with_us', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

		public function deletePartner($id) {
			return $this->db->delete('partner_with_us', ['id'=>$id]);
		}
	
	}
?>