<?php
	class Inspiringpeoples_model extends CI_Model {

		function __construct() {
	        $this->table = 'video';
    	}

    	function getRows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "English");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getAssameserows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Assamese");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getBanglarows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Bangla");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getHindirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Hindi");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getGujaratirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Gujarati");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getMarathirows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	       //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Marathi");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTamilrows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Tamil");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);

	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

	    function getTelugurows($params = array()){
	        $this->db->select('*');
	        $this->db->from($this->table);
	        //$this->db->where('particular', "Inspiring_people");
	        $this->db->where('language', "Telugu");
	        $this->db->where("show_insp",1);
            $this->db->order_by("id","DESC");
            $this->db->limit(9);
            
	        if(array_key_exists("conditions", $params)){
	            foreach($params['conditions'] as $key => $val){
	                $this->db->where($key, $val);
	            }
	        }
	        if(!empty($params['searchKeyword'])){
	            $search = $params['searchKeyword'];
	            $likeArr = array('title' => $search);
	            $this->db->like($likeArr);
	        }
	        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
	            $result = $this->db->count_all_results();
	        }else{
	            if(array_key_exists("id", $params)){
	                $this->db->where('id', $params['id']);
	                $query = $this->db->get();
	                $result = $query->row_array();
	            }else{
	                $this->db->order_by('id', 'DESC');
	                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit'],$params['start']);
	                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
	                    $this->db->limit($params['limit']);
	                }
	                
	                $query = $this->db->get();
	                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
	            }
	        }
	        return $result;
	    }

		public function getPosts() {

			$query = $this->db->get('video');
			
			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		public function getVideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "English");
           	//$this->db->where("particular", "Inspiring_people");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getLatestVideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "English");
           	//$this->db->where("particular", "Inspiring_people");
           	
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}


           public function getLatestVideosbyId($id) {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "English");
           	//$this->db->where("particular", "Inspiring_people");
           	$this->db->where("reg_id", $id);
           	
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}
		public function getTrendVideos() {
           	$this->db->select("ins.*,COUNT(li.vid_like)  AS likes,COUNT(com.video_id)  AS comments ,COUNT(vi.video_id)  AS views");
           	$this->db->from("video as ins");
           
           	  $this->db->join('likes as li', 'ins.id = li.video_id','left');
           	   $this->db->join('comments as com', 'ins.id = com.video_id','left');
           	    $this->db->join('views as vi', 'ins.id = vi.video_id','left');
           
           	$this->db->where("language", "English");
           	$this->db->where("type", "File");
            $this->db->group_by("ins.id");
          
           
            $this->db->order_by("views DESC,likes DESC, comments DESC");
          
            $this->db->limit(9);
			$query = $this->db->get();
			//print $this->db->last_query();
			return $query->result();
		}

		public function getHindivideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Hindi");
           	//$this->db->where("particular", "Inspiring_people");
            $this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getAssamesevideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Assamese");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getGujarativideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Gujarati");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getMarathivideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Marathi");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getBanglavideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Bangla");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTamilvideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Tamil");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function getTeluguvideos() {
           	$this->db->select("ins.*");
           	$this->db->from("video as ins");
           	$this->db->where("language", "Telugu");
           	$this->db->where("show_insp",1);
            $this->db->order_by("ins.id","DESC");
            $this->db->limit(9);
			$query = $this->db->get();
			
			return $query->result();
		}

		public function addPost($data) {
			return $this->db->insert('video', $data);
		}

		public function getSinglePosts($id) {
			$query = $this->db->get_where('video', array('id'=>$id));
			if($query->num_rows() > 0) {
				return $query->row();
			}		
		}

		public function updatePost($data, $id) {
			return $this->db->where('id', $id)
						->update('video', $data);
		}

		public function deletePosts($id) {
			return $this->db->delete('video', ['id'=>$id]);
		}

		public function getLanguage() {
			$query = $this->db->get('language');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}
	}