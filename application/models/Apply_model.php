<?php
	class Apply_model extends CI_Model {

		//Fetching work_function data from database
		public function getFun() {
			$query = $this->db->get('fun');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}


		//Fetching preferred_location data from database
		public function getLocation() {
			$query = $this->db->get('preferred_location');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}


		//Fetching experience data from database
		public function getExperience() {
			$query = $this->db->get('experience');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}


		//Fetching know_bv data from database
		public function getKnowbv() {
			$query = $this->db->get('know_bv');

			if($query->num_rows() > 0) {
				return $query->result();
			}
		}

		//Inserting into database
		public function addData($data) {
			return $this->db->insert('job_application', $data);
		}
	
	}
?>