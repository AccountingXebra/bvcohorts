<?php
	class Search_model extends CI_Model {

		public function getSearch($keyword) {

			$this->db->select('vd.id as id,vd.title as name,vd.title as username,vd.description as desc,vd.video as file,concat("video") as type,vd.thumbnail as thumbnail,vd.reg_id as userid,vd.created_at as date');
			$this->db->from('video as vd');
			$this->db->group_start();
			$this->db->like('vd.title', $keyword);
			$this->db->or_like('vd.description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'English');

			$this->db->get();
			 $query1 =  $this->db->last_query();

			$this->db->select('reg.reg_id as id,reg.reg_username as name,reg.reg_uniqname as username,reg.user_bio as desc,reg.reg_profile_image as file,concat("user") as type,reg.reg_profile_image as thumbnail,reg.reg_id as userid,reg.createdat as date');
			$this->db->from('registration as reg');
			$this->db->group_start();
			$this->db->like('reg.reg_username', $keyword);
			$this->db->or_like('reg.user_bio', $keyword);
			$this->db->group_end();
			

			

			 $this->db->get(); 
    $query2 =  $this->db->last_query();
    $query = $this->db->query($query1." UNION ".$query2);

    return $query->result();

			
		}

		public function getAssamesesearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Assamese');

			$query = $this->db->get();

			return $query->result();
		}

		public function getBanglasearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Bangla');

			$query = $this->db->get();

			return $query->result();
		}

		public function getGujaratisearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Gujarati');

			$query = $this->db->get();

			return $query->result();
		}

		public function getHindisearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Hindi');

			$query = $this->db->get();

			return $query->result();
		}

		public function getMarathisearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Marathi');

			$query = $this->db->get();

			return $query->result();
		}

		public function getTamilsearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Tamil');

			$query = $this->db->get();

			return $query->result();
		}

		public function getTeluguSearch($keyword) {

			$this->db->select('*');
			$this->db->from('video');
			$this->db->group_start();
			$this->db->like('title', $keyword);
			$this->db->or_like('description', $keyword);
			$this->db->group_end();
			$this->db->where('particular', 'Categories');
			$this->db->where('language', 'Telugu');

			$query = $this->db->get();

			return $query->result();
		}
	}
?>