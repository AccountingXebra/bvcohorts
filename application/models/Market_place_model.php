<?php
class Market_place_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			
			return  $id;

		}else{
			return false;
		}
	}

	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return true;
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/


	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}



	public function MarketFilter($data)
	{
		$this->db->select("bus.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->join('rating as r', 'r.review_id = bus.bus_id', 'left');

		//$this->db->where("bus.bus_company_name like ","%".$data['search']."%");
		//$this->db->or_where("bus.nature_of_bus like ","%".$data['search']."%");

		$string = implode(",",$data['multi_loc']);
		$this->db->or_where("bus.bus_billing_city IN (".$string.")");

		$this->db->or_where("bus.nature_of_bus",$data['nature_of_business']);

		//$this->db->or_where("r.rating", 5);

		$this->db->order_by('r.review_id',"ASC");
/*
		if(@$data['search'])
		{
			$this->db->where("bus.bus_company_name like ","%".$data['search']."%");
			$this->db->or_where("bus.nature_of_bus like ","%".$data['search']."%");
		}
		
		if(@$data['multi_loc'])
		{
			$string = implode(",",$data['multi_loc']);
			$this->db->or_where("bus.bus_billing_city IN (".$string.")");
		}

		if(@$data['nature_of_business'])
		{
			
			$this->db->or_where("bus.nature_of_bus",$data['nature_of_business']);
		}
		
*/
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;		

	}

	public function GetUserInfo($data)
	{
		$this->db->select('bus.bus_id as code, bus.bus_id as bus_id, bus.reg_id as reg_id, bus.bus_company_name as company_name, bus.nature_of_bus as nature, bus.bus_billing_city as city, bus.bus_company_logo as company_logo, bus.bus_facebook as facebook, bus.bus_linkedin as linkedin, bus.bus_twitter as twitter, bus.bus_googleplus as googleplus, bus.bus_incorporation_date as incorporation_date, bus.bus_company_size as company_size, bus.bus_company_revenue as company_revenue, bus.bus_billing_address as billing_address, bus.bus_billing_zipcode as billing_zipcode, bus.bus_services_keywords as services_keywords, bus.bus_website_url as website_url, bus.bus_case_study as case_study, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->where('bus.bus_id', $data['id']);
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select('ev.vendor_unique_code as code, ev.bus_id as bus_id, ev.reg_id as reg_id, ev.vendor_name as company_name, ev.nature_of_bus as nature, ev.vendor_billing_city as city, ev.vendor_shipping_address as company_logo, ev.vendor_shipping_address as facebook, ev.vendor_shipping_address as twitter, ev.vendor_shipping_address as linkedin, ev.vendor_shipping_address as googleplus, ev.vendor_shipping_address as incorporation_date, ev.vendor_shipping_address as company_size, ev.vendor_shipping_address as company_revenue, ev.vendor_billing_address as billing_address, ev.vendor_billing_pincode as billing_zipcode, ev.vendor_shipping_address as services_keywords, ev.vendor_shipping_address as website_url, ev.vendor_shipping_address as case_study, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('expense_vendors as ev');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city','left');
		$this->db->where('ev.vendor_unique_code', $data['id']);
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select('sc.cust_unique_code as code, sc.bus_id as bus_id, sc.reg_id as reg_id, sc.cust_name as company_name, sc.nature_of_bus as nature, sc.cust_billing_city as city, sc.cust_shipping_address as company_logo, sc.cust_shipping_address as facebook, sc.cust_shipping_address as twitter, sc.cust_shipping_address as linkedin, sc.cust_shipping_address as googleplus, sc.cust_shipping_address as incorporation_date, sc.cust_shipping_address as company_size, sc.cust_shipping_address as company_revenue, sc.cust_billing_address as billing_address, sc.cust_billing_zipcode as billing_zipcode, sc.cust_shipping_address as services_keywords, sc.cust_shipping_address as website_url, sc.cust_shipping_address as case_study,c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('sales_customers as sc');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city','left');
		$this->db->where('sc.cust_unique_code', $data['id']);
		$this->db->get();
		$query3 = $this->db->last_query();

		$query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);

		$result = $query->result_array();
		return $result;	

	}

	public function GetBranchInfo($data)
	{
		$this->db->select("gst.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('gst_number as gst');
		$this->db->join('countries as c', 'c.country_id = gst.country' ,'left');
		$this->db->join('states as s', 's.state_id = gst.state_code','left');
		$this->db->join('cities as ci', 'ci.city_id = gst.city','left');
		//$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->where("gst.bus_id",$data['id']);
		$this->db->where("gst.type","business");

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function GetEmailIds($data)
	{
		$this->db->select("reg.reg_email");
		$this->db->from('registration as reg');
		$this->db->where("reg.reg_admin_type",'3');
		$this->db->where("reg.bus_id",$data['bus_id']);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_states($data)
	{
		$this->db->select("s.state_id");
		$this->db->from('states as s');
		$this->db->where("s.country_id",$data['country_id']);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_cities($data)
	{
		$this->db->select("ci.*");
		$this->db->from('cities as ci');
		$this->db->where("ci.city_id IN (".$data.")");
		$this->db->order_by("ci.name","ASC");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_rating($data)
	{
		$this->db->distinct();
		$this->db->from('rating as rat');
		$this->db->select("rat.rate");
		//$this->db->where("rat.bus_id",$data['id']);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function Get_unique_cities()
	{
		$this->db->distinct();
		$this->db->select("bus.bus_billing_city");
		$this->db->from('businesslist as bus');
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function getCompanyname($data) {

		$this->db->distinct();
		$this->db->select("bus.bus_company_name, bus.bus_id");
		$this->db->from('businesslist as bus');
		$this->db->where('bus_id', $data['id']);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function getRegemail($data) {

		$this->db->distinct();
		$this->db->select("reg.reg_email, reg.reg_id");
		$this->db->from('registration as reg');
		$this->db->where('bus_id', $data['id']);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function getRegcontact($data) {

		$this->db->select("*");
		$this->db->from('registration');
		$this->db->where('bus_id', $data['id']);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


}
?>