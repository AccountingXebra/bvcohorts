<?php
class Adminmaster_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}
	

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);
		// echo "<pre>";
		// print_r($result);exit();
		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($table, $data, $where)
	{
		$this->db->where($where);
		if($this->db->update($table, $data)){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/

	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delere data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{


		if($this->db->delete($table, $data)){
			return 1;
		}else{
			return 0;
		}
	}


	public function insertActivityData($table,$data)
	{   
		$fields['login_id']= $this->session->userdata['user_session']['reg_login_id'];
		$fields['bus_id']= $this->session->userdata['user_session']['bus_id'];
		$fields['task_id']=$data['task_id'];
		$fields['activity']=$table;
		$fields['action']=$data['action'];
		$fields['createdat']=date('Y-m-d h:i:s');
		$fields['updatedat']=date('Y-m-d h:i:s');
		$result = $this->db->insert('activity_history', $fields);
		// echo "<pre>";
		// print_r($result);exit();
		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	

	public function insertActivity($data)
	{   
		$fields['login_id']= $this->session->userdata['user_session']['reg_login_id'];
		$fields['reg_id']= $this->session->userdata['user_session']['reg_id'];
		
		if(@$this->session->userdata['user_session']['bus_id']){

			$fields['bus_id'] = $this->session->userdata['user_session']['bus_id'];
			
		}elseif (@$data['bus_id']) {

			$fields['bus_id'] = $data['bus_id'];
		}
		else{
			$fields['bus_id'] = 0;
		}
		
		$fields['module_name']=$data['module_name'];
		$fields['action_taken']=$data['action_taken'];
		$fields['reference']=$data['reference'];
		$fields['createdat']=date('Y-m-d H:i:s');
		$fields['updatedat']=date('Y-m-d H:i:s');
		$result = $this->db->insert('activity_tracker', $fields);

		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}


	public function insertSmsActivity($data)
	{   
		$fields['login_id']= $this->session->userdata['user_session']['reg_login_id'];
		$fields['reg_id']= $this->session->userdata['user_session']['reg_id'];
		
		if(@$this->session->userdata['user_session']['bus_id']){

			$fields['bus_id'] = $this->session->userdata['user_session']['bus_id'];
			
		}elseif (@$data['bus_id']) {

			$fields['bus_id'] = $data['bus_id'];
		}
		else{
			$fields['bus_id'] = 0;
		}
		
		$fields['module_name']=$data['module_name'];
		$fields['action_taken']=$data['action_taken'];
		$fields['reference']=$data['reference'];
		$fields['createdat']=date('Y-m-d H:i:s');
		$fields['updatedat']=date('Y-m-d H:i:s');
		$result = $this->db->insert('sms_tracker', $fields);

		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}


	 public function instamojo_signup($user_id,$type){

		
        $reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data = $this->Adminmaster_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
		$userdata = $this->Adminmaster_model->selectData('registration', '*',array('bus_id'=>$bus_id));

          if($type=="vendor"){
          	$vendor = $this->Adminmaster_model->selectData('expense_vendors', '*',array('vendor_id'=>$user_id));

          	$vendor_contact = $this->Adminmaster_model->selectData('expense_vendor_contacts', '*',array('vendor_id'=>$user_id,'cp_is_admin'=>1));
            $table="expense_vendors";
            $id="vendor_id";

            
          	$name=@$vendor[0]->vendor_name;
          	$first_name=@$vendor[0]->vendor_name;
          	$last_name="";
		    $email=@$vendor_contact[0]->cp_email;
		    $phno=@$vendor_contact[0]->cp_mobile;
		    $country= $this->Adminmaster_model->selectData('countries','*',array("country_id"=>$vendor[0]->vendor_billing_country));
		    $location=@$country[0]->country_name;
		    $account=@$vendor[0]->bank_acc_no;
		    $ifsc=@$vendor[0]->bank_ifsc_code;
          }

           if($type=="employee"){
          	$employee = $this->Adminmaster_model->selectData('employee_master', '*',array('emp_id'=>$user_id));
            $table="employee_master";
            $id="emp_id";
          	$name=$employee[0]->first_name."".$employee[0]->last_name;
          	$first_name=$employee[0]->first_name;
          	$last_name=$employee[0]->last_name;
		    $email=$employee[0]->employee_email_id;
		    $phno=$employee[0]->employee_mobile_no;
		    $country= $this->Adminmaster_model->selectData('countries','*',array("country_id"=>$employee[0]->country));
		    $location=$country[0]->country_name;
		    $account=$employee[0]->bank_acc_no;
		    $ifsc=$employee[0]->bank_ifsc_code;
          }
		$token = curl_init();
                 
				curl_setopt($token, CURLOPT_URL, 'https://test.instamojo.com/oauth2/token/');  //This is the Test API endpoint
				
				curl_setopt($token, CURLOPT_HEADER, FALSE);               
				curl_setopt($token, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($token, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($token, CURLOPT_HTTPHEADER,array());

				$dataToken = Array(
				    
				    'grant_type'=> 'client_credentials',
      'client_id'=> 'test_Lu6dfumLzFjdFh5DncGD94s1DNWtxlfjJi8',
      'client_secret'=> 'test_j3ZSe0JxeZZPn8K1TpzpilUylusGMzPy3MBYqZ1sBjGzb2CLwYGN2dvxpcADyI8B6h4etdwl5iwMhKjkV86bqLJC1UNz7C0OSercZj2MtkabWqvuClFslum7V7A'
				    
				    
				);

				curl_setopt($token, CURLOPT_POST, true);
				curl_setopt($token, CURLOPT_POSTFIELDS, http_build_query($dataToken));
				$response = curl_exec($token);
				curl_close($token);
				$tokenArray = array(json_decode($response, true));

				

		if($tokenArray)
		{	
		
			
		   

			// $purpose="Invoice no : 00540";
			// $name="Nitesh";
		 //    $email="nitesh@windchimes.co.in";
		 //    $phno="8652465591";
		 //    $amount="25000";

		    if(!empty($tokenArray[0]['access_token']) ){		    
		    	$signup = curl_init();
                 
				curl_setopt($signup, CURLOPT_URL, 'https://test.instamojo.com/v2/users/');  //This is the Test API endpoint
				
				curl_setopt($signup, CURLOPT_HEADER, FALSE);               
				curl_setopt($signup, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($signup, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($signup, CURLOPT_HTTPHEADER,array("Authorization: Bearer ".$tokenArray[0]['access_token']));

				$signupData = Array(
				    
				    'email' => $email,
				    'password'=>'insta1234',
				    'phone' => $phno,
				    'referrer' => "manoj_941ac",
				  
				    
				    
				);

				curl_setopt($signup, CURLOPT_POST, true);
				curl_setopt($signup, CURLOPT_POSTFIELDS, http_build_query($signupData));
				$responseSignup = curl_exec($signup);
				curl_close($signup);
				$signupArray = array(json_decode($responseSignup, true));

				
				
				if(isset($signupArray[0]['id']) && $signupArray[0]['id']!="")
				{

                    $userToken = curl_init();
                 
				curl_setopt($userToken, CURLOPT_URL, 'https://test.instamojo.com/oauth2/token/');  //This is the Test API endpoint
				
				curl_setopt($userToken, CURLOPT_HEADER, FALSE);               
				curl_setopt($userToken, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($userToken, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($userToken, CURLOPT_HTTPHEADER,array());

				$dataToken = Array(
				    
				    'grant_type'=> 'password',
      'client_id'=> 'test_Lu6dfumLzFjdFh5DncGD94s1DNWtxlfjJi8',
      'client_secret'=> 'test_j3ZSe0JxeZZPn8K1TpzpilUylusGMzPy3MBYqZ1sBjGzb2CLwYGN2dvxpcADyI8B6h4etdwl5iwMhKjkV86bqLJC1UNz7C0OSercZj2MtkabWqvuClFslum7V7A',
              'username'=>$signupArray[0]['email'],
              'password'=>'insta1234',
				    
				    
				);

				curl_setopt($userToken, CURLOPT_POST, true);
				curl_setopt($userToken, CURLOPT_POSTFIELDS, http_build_query($dataToken));
				$responseUserToken = curl_exec($userToken);
				curl_close($userToken);
				$tokenUserArray = array(json_decode($responseUserToken, true));
                 
                    if(!empty($tokenUserArray[0]['access_token']) )
		    {
					$personalDetails = curl_init();
                 
				curl_setopt($personalDetails, CURLOPT_URL, 'https://test.instamojo.com/v2/users/'.$signupArray[0]['id'].'/');  //This is the Test API endpoint
				
				curl_setopt($personalDetails, CURLOPT_HEADER, FALSE);               
				curl_setopt($personalDetails, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($personalDetails, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($personalDetails, CURLOPT_HTTPHEADER,array("Authorization: Bearer ".$tokenUserArray[0]['access_token']));

				$personalData = Array(
				    
				    'first_name' => $first_name,
				    'last_name'=>$last_name,
				    'location'=>$location,
				    'phone' => $phno,
				   
				  
				    
				    
				);

				curl_setopt($personalDetails, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($personalDetails, CURLOPT_POSTFIELDS, http_build_query($personalData));
				$responseDetails = curl_exec($personalDetails);
				curl_close($personalDetails);
				$detailsArray = array(json_decode($responseDetails, true));

				

				$bankDetails = curl_init();
                 
				curl_setopt($bankDetails, CURLOPT_URL, 'https://test.instamojo.com/v2/users/'.$signupArray[0]['id'].'/inrbankaccount/');  //This is the Test API endpoint
				
				curl_setopt($bankDetails, CURLOPT_HEADER, FALSE);               
				curl_setopt($bankDetails, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($bankDetails, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($bankDetails, CURLOPT_HTTPHEADER,array("Authorization: Bearer ".$tokenUserArray[0]['access_token']));

				$bankData = Array(
				    
				    'account_holder_name' => $name,
				    'account_number'=>$account,
				    'ifsc_code'=>$ifsc,
				    
				   
				  
				    
				    
				);

				curl_setopt($bankDetails, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($bankDetails, CURLOPT_POSTFIELDS, http_build_query($bankData));
				$responseBankDetails = curl_exec($bankDetails);
				curl_close($bankDetails);
				$detailsBankArray = array(json_decode($responseBankDetails, true));

				
             
             }
			
                     $data=array(
                     	'submerchant_id'=>$signupArray[0]['id'],
                     );
		       	$flagSet=$this->Adminmaster_model->updateData($table,$data,array($id=>$user_id));
              return true;
	

	  } else{
	  	 return false;
	  }



     	}else{
     		return false;
     	}


     }else{
     	return false;
     }

   
    }


     public function expense_payment($data,$type){

     $this->session->unset_userdata('expense_payment_data');
     	if($type=="salary"){
     		$purpose="Salary";
     		$amount=$data[0]->net_earning;
            $data['type']="Salary";
            $data['amount']=$data[0]->net_earning;
             $data['id']=$data[0]->sal_exp_id;
             $employee=$this->Adminmaster_model->selectData('employee_master', '*',array('emp_id'=>$data[0]->emp_id));
         $data['submerchant_id']=$employee[0]->submerchant_id;
     	}

     	if($type=="expense"){
     		$purpose="Expense";
     		$amount=$data[0]->ce_grandtotal;
     		  $data['type']="Expense";
     		   $data['amount']=$data[0]->ce_grandtotal;
     		   $data['id']=$data[0]->exp_id;
     		    $vendor=$this->Adminmaster_model->selectData('expense_vendors', '*',array('vendor_id'=>$data[0]->ce_vendorname));
         $data['submerchant_id']=$vendor[0]->submerchant_id;

     	}


         
         $this->session->set_userdata('expense_payment_data',$data);
        
     	$business = $this->Adminmaster_model->selectData('businesslist', '*',array('bus_id'=>$data[0]->bus_id));
        
	 $token = curl_init();
                 
				curl_setopt($token, CURLOPT_URL, 'https://test.instamojo.com/oauth2/token/');  //This is the Test API endpoint
				
				curl_setopt($token, CURLOPT_HEADER, FALSE);               
				curl_setopt($token, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($token, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($token, CURLOPT_HTTPHEADER,array());

				$dataToken = Array(
				    
				    'grant_type'=> 'client_credentials',
      'client_id'=> 'test_Lu6dfumLzFjdFh5DncGD94s1DNWtxlfjJi8',
      'client_secret'=> 'test_j3ZSe0JxeZZPn8K1TpzpilUylusGMzPy3MBYqZ1sBjGzb2CLwYGN2dvxpcADyI8B6h4etdwl5iwMhKjkV86bqLJC1UNz7C0OSercZj2MtkabWqvuClFslum7V7A'
				    
				    
				);

				curl_setopt($token, CURLOPT_POST, true);
				curl_setopt($token, CURLOPT_POSTFIELDS, http_build_query($dataToken));
				$response = curl_exec($token);
				curl_close($token);
				$tokenArray = array(json_decode($response, true));


               if(!empty($tokenArray[0]['access_token'])){
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/v2/payment_requests/');  //This is the Live API endpoint
				curl_setopt($ch, CURLOPT_HEADER, FALSE);               
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization:Bearer ".$tokenArray[0]['access_token']));

				$payload = Array(
				    'purpose' => $purpose,
				    'amount' => $amount,
				    'phone' =>$business[0]->bus_company_contact,
				    'buyer_name' => $business[0]->bus_company_name,
				    'redirect_url' => 'https://'.$_SERVER['HTTP_HOST']."/xebra/expense/payment_sucess", 
				    'webhook' => '', 
				    'send_email' => false,
				    'send_sms' => false,
				    'email' => $business[0]->bus_company_email,
				    'allow_repeated_payments' => false
				);

				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
				$response = curl_exec($ch);
				curl_close($ch);
				$myArray = array(json_decode($response, true));
				//print_r($myArray);
				$longu = $myArray[0]["longurl"];   

					redirect($longu);

				}

	  }


}
?>
