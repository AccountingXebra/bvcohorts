<?php
class Userorderaddress_model extends CI_Model{
		var $table;
	public function  __construct(){
		parent::__construct();
		$this->load->database();
		$this->table ='user_order_address';
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($data)
	{
		$data['dtUpdatedAt'] = date("Y-m-d H:i:s");
		$result = $this->db->insert('user_order_address', $data);
		// echo "<pre>";
		// print_r($result);exit();
		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($data, $where)
	{
		$data['dtUpdatedAt'] = date("Y-m-d H:i:s");
		$this->db->where($where);
		if($this->db->update('user_order_address', $data)){
			return 1;
		}else{
			return 0;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delere data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){
			return 1;
		}else{
			return 0;
		}
	}


	public function fetchUserData($column , $value)
	{
		$this->db->select('*');
		$this->db->from('user_order_address');
		$this->db->where($column,$value);
		$this->db->where('is_deleted',0);
		
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = (array) $query->first_row();
		
		return $data;
	}

	public function fetchUserOrder()
	{
		$this->db->select('uoa.* , u.vName as user');
		$this->db->from('user_order_address as uoa');
		$this->db->join('user_master as u','u.iUserId = uoa.iUserId');
		$this->db->where('u.is_deleted',0);

		$query = $this->db->get();

		$data = $query->result_array();
		return $data;
	}


}
?>
