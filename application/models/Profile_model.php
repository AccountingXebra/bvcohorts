<?php
class Profile_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			return  $id;

		}else{
			return false;
		}
	}

	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return true;
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	
	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;

		}else{

			return 0;
		}
	}
	
	
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

	public function searchData($keyword){
			$this->db->select('state_name');
			$this->db->from('states');
			$this->db->where('country_id',101);
			$this->db->like('state_name', $keyword, 'after');
			$query = $this->db->get();
			return $query->result();

	}
	
		/*
	}
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	
	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delete data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	public function fetchUserFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("bus.*,c.country_id,c.country_name,s.state_id,s.state_name,ct.id,ct.company_type,cb.cbank_id,cb.cbank_name,cb.cbank_account_no,gst.gst_id,gst.gst_no,gst.place");
		$this->db->from('businesslist as bus');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("bus.bus_company_name like ","%".$Data['search']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']."%");
			$this->db->or_where("ct.company_type like ","%".$Data['search']."%");
			$this->db->or_where("cb.cbank_name like ","%".$Data['search']."%");
			$this->db->or_where("cb.cbank_account_no like ","%".$Data['search']."%");
			$this->db->or_where("gst.gst_no like ","%".$Data['search']."%");
			$this->db->or_where("gst.place like ","%".$Data['search']."%");
			$this->db->or_where("bus.status like ","%".$Data['search']."%");
			$this->db->group_end();
			
		}
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('company_type as ct', 'ct.id = bus.bus_company_type','left');
		$this->db->join('company_bank as cb', 'cb.bus_id = bus.bus_id','left');
		$this->db->join('gst_number as gst', 'gst.bus_id = bus.bus_id','left');
		if($Data['country']!=''){
			$this->db->where("bus.bus_billing_country",$Data['country']);
		}
		if($Data['state']!=''){
			$this->db->where("bus.bus_billing_state",$Data['state']);
		}
		$this->db->where("bus.bus_id",$Data['bus_id']);
		$this->db->group_by("bus.bus_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");
		
		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//echo $this->db->last_query(); exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	public function downloadMultiple($data,$reg_id){
		$this->db->select("bus.*,c.country_id,c.country_name,s.state_id,s.state_name,ct.id,ct.company_type,cb.cbank_id,cb.cbank_name,cb.cbank_account_no,gst.gst_id,gst.gst_no,gst.place,city.name as bcity");
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as city','city.city_id=bus.bus_billing_city');
		$this->db->join('company_type as ct', 'ct.id = bus.bus_company_type','left');
		$this->db->join('company_bank as cb', 'cb.bus_id = bus.bus_id','left');
		$this->db->join('gst_number as gst', 'gst.bus_id = bus.bus_id','left');
		//$this->db->where("bus.reg_id",$reg_id);
		$this->db->where_in("bus.bus_id",$data);
		$this->db->group_by("bus.bus_id");
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		$result= $query->result_array();
		return $result;			
		
	}

	public function downloadMultiPerosnalprofile($data,$reg_id){
		$this->db->select("reg.*,ap.id,ap.access");
		$this->db->from('registration as reg');
		$this->db->join('access_permissions as ap', 'ap.id = reg.reg_admin_type','left');
		$this->db->where_in("reg.reg_id",$data);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		$result= $query->result_array();
		return $result;			
		
	}

	public function fetchUserFilter1($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("reg.*,ap.id,ap.access");
		$this->db->from('registration as reg');
		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("reg.reg_username  ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_email like ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_mobile like ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_admin_type like ","%".$Data['search']."%");
			$this->db->or_where("ap.access like ","%".$Data['search']."%");
			if(strtolower(trim($Data['search']))=="a" || strtolower(trim($Data['search']))=="ad" || strtolower(trim($Data['search']))=="adm" || strtolower(trim($Data['search']))=="admi" || strtolower(trim($Data['search']))=="admin"){
			$this->db->or_where("reg.reg_admin_type like ","-1");	
			}
			$this->db->group_end();
		}
		$this->db->join('access_permissions as ap', 'ap.id = reg.reg_admin_type','left');
        if($Data['name'])
		{
		$this->db->where("reg.reg_username",$Data['name']);
	}
		//$this->db->where("reg.reg_createdby",$Data['reg_id']);
		if($Data['bus_id'])
		{
			$this->db->where("(reg.reg_createdby=".$Data['reg_id']." OR reg.bus_id=".$Data['bus_id'].")");
		}
		else
		{
			$this->db->where("(reg.reg_createdby=".$Data['reg_id']." OR reg.reg_id=".$Data['reg_id'].")");
		}

		//$this->db->where("reg.reg_admin_type !=",5);
		
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){

			$this->db->limit($Data['length'],$Data['start']);
			
			}
			$query = $this->db->get();

          
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		

		}
	}

	public function selectDataxls($id){
		$this->db->select('bus.*,con.country_name as bcountry,st.state_name as bstate,city.name as bcity,cons.country_name as scountry,sts.state_name as sstate,citys.name as scity,cur.currency,ct.company_type');
		$this->db->from('businesslist as bus');
		$this->db->join('countries as con','con.country_id=bus.bus_billing_country','left');
		$this->db->join('states as st','st.state_id=bus.bus_billing_state','left');
		$this->db->join('cities as city','city.city_id=bus.bus_billing_city','left');
		$this->db->join('countries as cons','cons.country_id=bus.bus_shipping_country','left');
		$this->db->join('states as sts','sts.state_id=bus.bus_shipping_state','left');
		$this->db->join('cities as citys','citys.city_id=bus.bus_shipping_city','left');
		$this->db->join('currency as cur','cur.currency_id=bus.bus_currency_format','left');
		$this->db->join('company_type as ct','ct.id=bus.bus_company_type','left');
		$this->db->where('bus.bus_id',$id);
		$query=$this->db->get();
		return $query->result();

	}


	public function get_company_bank_details($bus_id){
		$this->db->select("cb.*,c.country_id,c.country_name,c.sortname,c.phonecode,cr.currency_id,cr.currency,cr.currencycode,cr.symbol");
		$this->db->from('company_bank as cb');
		$this->db->join('countries as c', 'c.country_id = cb.cbank_country_code' ,'left');
		$this->db->join('currency as cr', 'cr.currency_id = cb.cbank_currency_code','left');
		$this->db->where_in("cb.bus_id",$bus_id);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
		$result= $query->result();
		return $result;			
		
	}
	

}
?>
