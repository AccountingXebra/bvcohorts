<?php
class Module_Tracker_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	}


		/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='',$nwhere='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}

		if ($nwhere != "") {
			$this->db->where_not_in($nwhere);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{

			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

	public function get_active_users($bus_id)
	{
		$where = array('log.bus_id'=>$bus_id);
		$this->db->distinct('log.reg_id');
		$this->db->select('reg.reg_username');
		$this->db->from('login_record as log');
		$this->db->join('registration as reg', 'reg.reg_id = log.reg_id' ,'left');
		//$this->db->from('activity_tracker');
		//$this->db->where("log.bus_id",$Data['bus_id']);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function ActivityFilter($Data,$sort_field,$orderBy,$c,$type=0)
	{
		$this->db->select("log.*,reg.reg_username,act.module_name");
		$this->db->from('login_record as log');
		$this->db->join('registration as reg', 'reg.reg_id = log.reg_id' ,'left');
		$this->db->join('activity_tracker as act', 'act.login_id = log.login_id' ,'left');
		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("log.login_id like ","%".$Data['search']."%");
			$this->db->or_where("log.ip_address like ","%".$Data['search']."%");
			$this->db->or_where("log.created_at ","%".$Data['search']."%");
			$this->db->or_where("reg.reg_username like ","%".$Data['search']."%");
			$this->db->or_where("act.module_name like ","%".$Data['search']."%");
			//$this->db->and_where("act.module_name like ","%".$Data['module_name']."%");
			$this->db->group_end();
		}

		if(!empty($Data['module_name']))
		{
			$this->db->group_start();
			
			$this->db->where("act.module_name like ","%".$Data['module_name']."%");
			$this->db->group_end();
		}

		if(!empty($Data['person_name']))
		{
			$this->db->group_start();
			
			$this->db->where("reg.reg_username like ","%".$Data['person_name']."%");
			$this->db->group_end();
		}

		
		if($Data['act_start_date']!=''){
			$this->db->where("log.created_at>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['act_start_date']))));
		}
		if($Data['act_end_date']!=''){
			$this->db->where("CAST(log.created_at as date)<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['act_end_date']))));
		}
		$this->db->where("log.bus_id",$Data['bus_id']);
		
		$this->db->group_by("log.login_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}




	public function unique_modules($login_id, $bus_id)
	{
		$where = array('login_id'=>$login_id,'bus_id'=>$bus_id);

		$this->db->distinct();
		$this->db->select('module_name');
		//$this->db->from('activity_tracker');
		$this->db->where($where);

		$query = $this->db->get('activity_tracker');
			//print_r($this->db->last_query());exit;
		return $query->result_array();
			
		//return $result;
	}


	public function print_multiple_activity($array,$bus_id)
	{
		$where = array('log.bus_id'=>$bus_id);
		
		$this->db->select('log.*,reg.reg_username');
		$this->db->from('login_record as log');
		$this->db->join('registration as reg', 'reg.reg_id = log.reg_id' ,'left');
		//$this->db->join('activity_tracker as act', 'act.login_id = log.login_id' ,'left');
		$this->db->where($where);
		$this->db->where_in("log.login_id",$array);
		$query = $this->db->get();
		$userData = $query->result();

		for($j=0;$j<count($userData);$j++)
		{	
	  		
            $module = $this->Module_Tracker_model->unique_modules($userData[$j]->login_id,$bus_id);
            if($module){
            	$new_arr = (array) $module;
            	$string = '';
            	for($i=0; $i<count($new_arr); $i++)
            	{
            		$string .= $new_arr[$i]['module_name'].', ';
            	}

            	$userData[$j]->module_data = rtrim($string,",");
            	//$module_data = substr($string,0,30).'....';
            	//print_r($new_arr);
            }else{
            	$userData[$j]->module_data = "";
            }
        }

        return $userData;

	}








/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
		
			return  $id;

		}else{
			return false;
		}
	}
	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return true;
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	
	
	


		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	
	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	public function deleteRecord($table,$srec_id, $data)
	{
		
		$this->db->where('parent_id',$srec_id);
		$this->db->where_not_in('srf_id',$data);
		//$this->db->delete($table);
		if($this->db->delete($table)){
			//echo $this->db->last_query();exit;
			return 1;
		}else{
			return 0;
		}
	}
		/*
	| -------------------------------------------------------------------
	| Delete data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	

	public function salesReceiptDetails($id,$reg_id,$bus_id,$gst_id='',$cust_id='')
	{
		$this->db->select('srec.*,srec.srec_id as sales_id,srl.*,srl.srec_id as srtrec_id,cus.cust_name,inv.inv_id,inv.inv_invoice_no,cur.currency,cur.currency_id,cur.currencycode,bank.cbank_id,bank.cbank_name');
		$this->db->from('sales_receipts as srec');
		$this->db->join('sales_customers as cus', 'cus.cust_id = srec.cust_id','left');
		$this->db->join('sales_receipts_list as srl', 'srl.srec_id = srec.srec_id','left');
		$this->db->join('sales_invoices as inv', 'inv.inv_id = srl.srecl_inv_no','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		
		$this->db->join('company_bank as bank', 'bank.cbank_id = srec.srec_bank_id','left');
		$this->db->where('srec.srec_id',$id);
		//$this->db->where('srec.reg_id',$reg_id);
		$this->db->where('srec.bus_id',$bus_id);
		if($gst_id!='0'){
		$this->db->where('srec.gst_id',$gst_id);
		}if($cust_id!=''){
		$this->db->where('srec.cust_id',$cust_id);
		}
		$this->db->where("srec.status","Active");
		//$this->db->group_by("srec.srec_id");
		$query = $this->db->get();
		return $query->result();
	}

	public function print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id,$singal='')
	{
		$this->db->select("sr.*,cus.cust_name,cur.currency,cur.currency_id,cur.currencycode,srl.srecl_id,srl.srecl_inv_no,srl.srecl_inv_amt,inv.inv_id,inv.inv_invoice_no");
		$this->db->from('sales_receipts as sr');
		$this->db->join('sales_customers as cus', 'cus.cust_id = sr.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('(SELECT * FROM sales_receipts_list ORDER BY srecl_id ASC LIMIT 10000000000000000000) srl', 'srl.srec_id = sr.srec_id','left');
		$this->db->join('sales_invoices as inv', 'inv.inv_id = srl.srecl_inv_no','left');
		//$this->db->where("sr.reg_id",$reg_id);
		$this->db->where("sr.bus_id",$bus_id);
		if($gst_id!='0'){
			$this->db->where("sr.gst_id",$gst_id);
		}
		if($singal==1){
			$this->db->where("sr.srec_id",$array);
		}else {
			$this->db->where_in("sr.srec_id",$array);
		}
		$this->db->where("sr.status","Active");
		$this->db->group_by("sr.srec_id");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;	
		
	}
	public function download_multiple_sreceipts($array,$reg_id,$bus_id,$gst_id)
	{
		$this->db->select("sr.*,cus.cust_name,cur.currency,cur.currency_id,cur.currencycode,srl.srecl_id,srl.srecl_inv_no,srl.srecl_inv_amt,inv.inv_id,inv.inv_invoice_no,inv.inv_invoice_date");
		$this->db->from('sales_receipts as sr');
		
		$this->db->join('sales_customers as cus', 'cus.cust_id = sr.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('(SELECT * FROM sales_receipts_list ORDER BY srecl_id ASC LIMIT 10000000000000000000) srl', 'srl.srec_id = sr.srec_id','left');
		$this->db->join('sales_invoices as inv', 'inv.inv_id = srl.srecl_inv_no','left');
		//$this->db->where("sr.reg_id",$reg_id);
		$this->db->where("sr.bus_id",$bus_id);
		if($gst_id!=''){
			$this->db->where("sr.gst_id",$gst_id);
		}
		if(is_array($array)){
		$this->db->where_in("sr.srec_id",$array);
		} else {
		$this->db->where("sr.srec_id",$array);	
		}
		$this->db->where("sr.status","Active");
		
		$this->db->group_by("sr.srec_id");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
			
		return $result;

	}

}
?>