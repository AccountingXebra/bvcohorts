<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Userslogin_model extends CI_Model{ 
    
    function __construct() { 
 
        $this->table = 'usersLogin';
    } 

    function getRows($params = array()){ 

        $this->db->select('*'); 
        $this->db->from($this->table); 
         
        if(array_key_exists("conditions", $params)){ 
            foreach($params['conditions'] as $key => $val){ 
                $this->db->where($key, $val); 
            } 
        } 
         
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){ 
            $result = $this->db->count_all_results(); 
        }else{ 
            if(array_key_exists("id", $params) || $params['returnType'] == 'single'){ 
                if(!empty($params['id'])){ 
                    $this->db->where('id', $params['id']); 
                } 
                $query = $this->db->get(); 
                $result = $query->row_array(); 
            }else{ 
                $this->db->order_by('id', 'desc'); 
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                    $this->db->limit($params['limit'],$params['start']); 
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
                    $this->db->limit($params['limit']); 
                } 
                 
                $query = $this->db->get(); 
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE; 
            } 
        } 
         
        // Return fetched data 
        return $result; 
    }

    public function updatePost($data, $id) {
        return $this->db->where('id', $id)
                    ->update('usersLogin', $data);
    }

    public function getUsersdata($id) {

        $query = $this->db->where('id',$id);
        $query = $this->db->get('usersLogin');
                

        return $query->result();
    }

    public function update($data = array(), $con) { 

        if(!empty($data)){ 
            // Add modified date if not included  
            if(!array_key_exists("modified", $data)){ 
            $data['modified'] = date("Y-m-d H:i:s"); 
        } 

        // Update member data 
        $update = $this->db->where('id', $con['id']);
        $update = $this->db->update('usersLogin', $data); 

        // Return the status 
        return $update?$this->db->insert_id():false; 
        } 
        return false; 
    }

    public function email_exist($email) {

        $this->db->select('email');
        $this->db->from('usersLogin');
        $this->db->where('email', $email);

        $query = $this->db->get();

        return $query->row_array();
    }

    public function updatePassword($email, $userData = array()) {

        if(!empty($userData)) {

          if(!array_key_exists("modified", $userData)){ 
            $userData['modified'] = date("Y-m-d H:i:s"); 
          }

          $update = $this->db->where('email', $email); 
          $update = $this->db->update('usersLogin', $userData);

          return $update?$this->db->insert_id():false; 
        } 
        
        return false; 
    } 

    public function insert($data = array()) { 
        if(!empty($data)){ 
            // Add created and modified date if not included 
            if(!array_key_exists("created", $data)){ 
                $data['created'] = date("Y-m-d H:i:s"); 
            } 
            if(!array_key_exists("modified", $data)){ 
                $data['modified'] = date("Y-m-d H:i:s"); 
            } 
             
            // Insert member data 
            $insert = $this->db->insert($this->table, $data); 
             
            // Return the status 
            return $insert?$this->db->insert_id():false; 
        } 
        return false; 
    } 

}