<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['logout'] = 'index/logout';
$route['logoff'] = 'index/logoff';
$route['signup'] = 'index/signup';
$route['signin'] = 'index/signin';

//$route['translate_uri_dashes'] = FALSE;
$route['translate_uri_dashes'] = TRUE;
$route['sales/my-clients'] = 'sales';
$route['sales/my-customers'] = 'sales';
$route['itemCRUDCreate']['post'] = "admin_dashboard/store";
$route['sales/add_credit_note/(:any)'] = 'sales/add_credit_note/$1';
$route['client'] = 'dashboard/client_login';
//$route['admin'] = 'admin-dashboard';
$route['admin'] = 'admin';
$route['itemCRUDCreate']['post'] = "admin_dashboard/store";
$route['vendor'] = 'dashboard/vendor_login';
$route['Public_prof/(:any)'] = 'Public_prof/index/$1';
/*$route['sales/sales-invoice'] = 'sales/sales_invoice/Invoice';
$route['sales/add-sales-invoice'] = 'sales/add_sales_invoice/Invoice';
$route['sales/estimate-invoice'] = 'sales/sales_invoice/Estimate';*/

$route['sitemap\.xml'] = "index/sitemap";
$route['news'] = 'community/news';
$route['about-us'] = 'index/about_us';
$route['contactus'] = 'index/contact_us';
$route['openings'] = 'index/openings';
$route['partner-with-us'] = 'index/partner_with_us';
$route['faqs'] = 'index/faqs';
$route['terms-and-conditions'] = 'index/terms_and_conditions';
$route['privacy-policy'] = 'index/privacy_policy';
$route['login'] = 'index/signin';
$route['govt-scheme'] = 'index/govt_scheme';
$route['interviews'] = 'community/interviews';
$route['interviews/(:any)'] = 'community/interviews/$1';

$route['blog/(:any)'] = 'community/interviews/$1';

$route['incubator'] = 'index/incubator';
$route['video/(:any)'] = 'community/video/$1';