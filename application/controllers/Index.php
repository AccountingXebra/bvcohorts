<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require ("common/Index_Controller.php");

class Index extends Index_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('Adminmaster_model');
        $this->load->model('Admin_dashboard_model');
        $this->load->model('User_model');
        $this->load->library('session');

        //MY_Output's disable_cache() method
        // $this->output->disable_cache();
    }

    public function no_internet() {

        $this->load->view('errors/html/no-internet.php');
    }

    public function index() {
        $session = $this->session->userdata('user_session');

        if (isset($session['reg_id'])) {

            $redirectId = "community/";

            redirect($redirectId);
        }



        if (@$_GET) {
            if (@$_GET['signup']) {
                $data['open_signup'] = 1;
            } else if (@$_GET['ne']) {
                $data['web_email'] = $_GET['ne'];
                $data['open_signup'] = 1;
            }
        } else {
            $data['web_email'] = '';
            $data['open_signup'] = "";
        }
        //$this->load->view('index/index',$data);

        $data['interviews'] = $this->Adminmaster_model->selectData("interviews", "*");
        $this->load->view('onecohorts/index_new', $data);
    }

    public function about_us() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/about_us');
    }

    public function contact_us() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/contact-us');
    }

    public function govt_scheme() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/govt_schemes');
    }

    public function interviews() {
        $session = $this->session->userdata('user_session');


        $data['title'] = "Interviews";
        $data['description'] = "Interviews";
        $data['keywords'] = "Interviews";
        $url = "";
        if ($url != "") {

            $data['video'] = $this->Adminmaster_model->selectData('interviews', '*', array('url' => $url));
            $data['interviews'] = $this->Adminmaster_model->selectData('interviews', '*');

            $data['comments'] = $this->Adminmaster_model->selectData('interview_comments', '*', array('interview_id' => $data['video'][0]->id), 'id', 'DESC');

            $data['likes'] = $this->Adminmaster_model->selectData('interview_likes', '*', array('interview_id' => $data['video'][0]->id, 'interview_like' => 1), 'id', 'DESC');
            $ip_address = $this->get_client_ip();
            $insert = array(
                'interview_id' => $data['video'][0]->id,
                'parent_id' => $data['video'][0]->profile_id,
                'ip' => $ip_address,
            );

            $this->Adminmaster_model->insertData('interview_views', $insert);


            $data['views'] = $this->Adminmaster_model->selectData('interview_views', '*', array('interview_id' => $data['video'][0]->id), 'id', 'DESC');
            if (isset($this->session->userdata['user_session']['reg_id'])) {
                $reg_id = $this->session->userdata['user_session']['reg_id'];
                $data['user'] = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $reg_id));
                //$this->load->view('my-community/interviews', $data);
                $this->load->view('onecohorts/home_interviews', $data);
            } else {
                $this->load->view('onecohorts/home_interviews', $data);
            }
        } else {

            if (isset($this->session->userdata['user_session']) && $this->session->userdata['user_session']['reg_id'] != "") {
                $reg_id = $this->session->userdata['user_session']['reg_id'];
                $data['user'] = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $reg_id));

                $data['interviews'] = $this->Adminmaster_model->selectData('interviews', '*');
                $this->load->view('onecohorts/home_interviews', $data);
            } else {
                $data['interviews'] = $this->Adminmaster_model->selectData('interviews', '*');
                $this->load->view('onecohorts/home_interviews', $data);
            }
        }
    }

    public function incubator() {
        $session = $this->session->userdata('user_session');
        $data['name'] = $this->Adminmaster_model->selectData('incubation ', 'distinct(name)');
        $data['countries'] = $this->Adminmaster_model->selectData('incubation', 'distinct(country)');
        $data['cities'] = $this->Adminmaster_model->selectData('incubation', 'distinct(city)');
        $this->load->view('onecohorts/incubators', $data);
    }

    public function get_incubators() {
        $post['length'] = -1;

        $records = array();
        $records["data"] = array();

        $post = $this->input->post();
        $incube = $this->User_model->getIncubation($post);
        //print_r($incube);exit();
        foreach ($incube as $key => $value) {
            if ($value['image'] != '') {
                $image = '<img src="/xebra/public/upload/incubator_image/' . $value['id'] . '/' . $value['image'] . '" height="auto" class="logo_style_2" width="130px">';
            } else {
                $image = '<p style="text-align:center;">No Image</p>';
            }
            $country = $this->Adminmaster_model->selectData('countries', "*", array('country_id' => $value['country']));

            $cities = $this->Adminmaster_model->selectData('cities', "*", array('city_id' => $value['city']));

            if (count($country) > 0) {
                $ctry = $country[0]->country_name;
            } else {
                $ctry = "";
            }

            if (count($cities) > 0) {
                $city = $cities[0]->name;
            } else {
                $city = "";
            }

            $middle_part = '<div class="row">
						<div class="col-md-12">
							<h5 style="font-weight:300; font-size:20px; color:#bc343a !important; margin:0 0 0 15px;"><strong>' . $value['name'] . '</strong></h5>
						</div>
						</div>
						<div class="row midaddress">
						<div class="col-md-7">
							<p class="offer_p addr"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS:</strong> ' . wordwrap($value['address'], 75, "<br>\n") . '</p>
						</div>
						<div class="col-md-3 city">
							<p class="offer_p"><i class="fas fa-globe"></i> <strong>COUNTRY:</strong> ' . $ctry . '</p>
						</div>
						<div class="col-md-2 city">
							<p class="offer_p city-inc"><i class="fas fa-map-marker-alt"></i> <strong>CITY: </strong> ' . $city . '</p>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist' . $value['id'] . '">
								<div class="col-md-7" style="margin-left:-15px;">
									<p class="offer_p eve-email"><a style="color:#000;" href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: </strong>' . $value['email_id'] . '</a></p>
								</div>
								<div class="col-md-4 contactInc">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: </strong>' . $value['contact_no'] . ' </p>
								</div>
						</div>

						<!--div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col-md-12">
								<a href="javascript:void(0);" id="showmorelist' . $value['id'] . '" class="more showmore active" title="Show More" onclick="showmorelist(' . $value['id'] . ')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less' . $value['id'] . '" class="less showmore" title="Show More" onclick="showmorelist(' . $value['id'] . ')" hidden>SHOW LESS</a>
							</div>
						</div-->';

            $records["data"][] = array(
                //$image,
                $middle_part,
                    //"",
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = count($incube);
        $records["recordsFiltered"] = count($incube);
        echo json_encode($records);
        exit;
    }

    public function contactus_save() {

        $data = $formData = array();

        // If contact request is submitted
        if ($this->input->post('contactSubmit')) {

            // Get the form data
            $formData = $this->input->post();
            // Form field validation rules
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('organisation', 'Organisation', 'required');
            $this->form_validation->set_rules('purpose', 'Purpose', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required');

            // Validate submitted form data
            if ($this->form_validation->run() == true) {

                // Define email data
                $mailData = array(
                    'name' => $formData['name'],
                    'email' => $formData['email'],
                    'organisation' => $formData['organisation'],
                    'purpose' => $formData['purpose'],
                    'message' => $formData['message']
                );

                // Send an email to the site admin

                $data = $this->input->post();

                $today = date('Y-m-d');
                $data['date_created'] = $today;

                unset($data['contactSubmit']);
                unset($data['g-recaptcha-response']);


                $this->load->model('Contactus_model');
                $this->Contactus_model->addData($data);

                $send = $this->sendEmail($mailData);

                // Check email sending status
                if ($send) {
                    // Unset form data
                    $formData = array();

                    $data['status'] = array(
                        'type' => 'success',
                        'msg' => 'Your contact request has been submitted successfully.'
                    );
                } else {
                    $data['status'] = array(
                        'type' => 'error',
                        'msg' => 'Some problems occured, please try again.'
                    );
                }
            }
        }

        // Pass POST data to view
        $data['postData'] = $formData;
        //echo '<pre>'; print_r($data); echo '</pre>';
        // Pass the data to view
        $data['title'] = "Contact Us";
        //header data
        $data['description'] = "Contact Us";
        $data['keywords'] = "Contact Us";

        if (isset($this->session->userdata['user_session']) && $this->session->userdata['user_session']['reg_id'] != "") {
            $reg_id = $this->session->userdata['user_session']['reg_id'];
            $data['user'] = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $reg_id));
        }
        $this->load->view('onecohorts/contact-us', $data);
        //$this->load->view('partials/header1', $data);
        //$this->load->view('partials/footer');
    }

    private function sendEmail($mailData) {
        // Load the email library
        $ci = & get_instance();
        $ci->load->library('email');

        // Mail config
        $to = 'contactus@bharatvaani.in';
        $from = 'From ' . $mailData['email'];
        $fromName = $mailData['name'];
        //$mailSubject = 'Contact Request Submitted by '.$mailData['name'];
        // Mail content
        $mailContent = '
            <h2>Contact Request Submitted</h2>
            <p>' . $mailData['name'] . '</p>
            <p>' . $mailData['email'] . '</p>
            <p><b>Organisation: </b>' . $mailData['organisation'] . '</p>
            <p><b>Purpose: </b>' . $mailData['purpose'] . '</p>
            <p><b>Message: </b>' . $mailData['message'] . '</p>
        ';

        $config['protocol'] = 'SMTP';
        $config['smtp_crypto'] = 'STARTTLS';
        $config['smtp_host'] = 'mail.bharatvaani.in';
        $config['smtp_port'] = 587;
        $config['smtp_username'] = 'contactus@bharatvaani.in';
        $config['smtp_password'] = 'bharat@vaani21';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");
        $ci->email->set_newline("\r\n");
        $ci->email->from($from, $fromName);

        $ci->email->to($to);
        $ci->email->cc("");
        $ci->email->bcc("");
        $ci->email->subject("Contact With Us");
        $ci->email->message($mailContent);


        //$this->email->to($to);
        //$this->email->from($from, $fromName);
        //$this->email->subject($mailSubject);
        //$this->email->message($mailContent);
        // Send email & return status
        return $ci->email->send() ? true : false;
    }

    public function partner_with_us() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/partner-with-us');
    }

    //Partner With Us saving to database controller
    /* 	public function partnerwithus_save() {
      $this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces');
      $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
      $this->form_validation->set_rules('organisation', 'Organisation', 'required');
      $this->form_validation->set_rules('purpose', 'Purpose', 'required');
      $this->form_validation->set_rules('message', 'Message', 'required');

      if ($this->form_validation->run()) {
      $data = $this->input->post();

      $today = date('Y-m-d');
      $data['date_created'] = $today;

      unset($data['submit']);

      $this->load->model('Partnerwithus_model');

      if ($this->Partnerwithus_model->addData($data)) {
      $this->session->set_flashdata('msg', 'Form Submitted Successfully!');
      }
      else {
      $this->session->set_flashdata('msg', 'Failed to Submit Form!');
      }
      redirect('home/partner-with-us');
      }
      else {

      $this->partner_with_us();
      }
      }
     */

    public function partnerwithus_save() {
        $data = $formData = array();

        // If contact request is submitted
        if ($this->input->post()) {

            // Get the form data
            $formData = $this->input->post();

            // Form field validation rules
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('organisation', 'Organisation', 'required');
            $this->form_validation->set_rules('purpose', 'Purpose', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required');

            // Validate submitted form data
            if ($this->form_validation->run() == true) {

                // Define email data
                $mailData2 = array(
                    'name' => $formData['name'],
                    'email' => $formData['email'],
                    'organisation' => $formData['organisation'],
                    'purpose' => $formData['purpose'],
                    'message' => $formData['message']
                );

                // Send an email to the site admin
                $data = $this->input->post();

                $today = date('Y-m-d');
                $data['date_created'] = $today;

                unset($data['submit']);
                unset($data['g-recaptcha-response']);

                $this->load->model('Partnerwithus_model');
                $this->Partnerwithus_model->addData($data);
                $send = $this->sendEmail2($mailData2);

                // Check email sending status
                if ($send) {
                    // Unset form data
                    $formData = array();

                    $data['status'] = array(
                        'type' => 'success',
                        'msg' => 'Your message has been sent.'
                    );
                } else {
                    $data['status'] = array(
                        'type' => 'error',
                        'msg' => 'Some problems occured, please try again.'
                    );
                }
            }
        }

        // Pass POST data to view
        $data['postData'] = $formData;
        //echo '<pre>'; print_r($data); echo '</pre>';
        // Pass the data to view
        $data['title'] = "Submit Stories";

        //header data
        $data['description'] = "Submit Stories";
        $data['keywords'] = "Submit Stories";
        if (isset($this->session->userdata['user_session']) && $this->session->userdata['user_session']['reg_id'] != "") {
            $reg_id = $this->session->userdata['user_session']['reg_id'];
            $data['user'] = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $reg_id));
        }
        $this->load->view('onecohorts/partner-with-us', $data);
        //$this->load->view('partials/header1', $data);
        //$this->load->view('partials/footer');
    }

    private function sendEmail2($mailData2) {
        // Load the email library
        $ci = & get_instance();
        $ci->load->library('email');

        // Mail config
        $to = 'contactus@bharatvaani.in';
        $from = 'From ' . $mailData2['email'];
        $fromName = $mailData2['name'];
        //$mailSubject = 'Contact Request Submitted by '.$mailData['name'];
        // Mail content
        $mailContent = '
            <h2>Contact Request Submitted</h2>
            <p>' . $mailData2['name'] . '</p>
            <p>' . $mailData2['email'] . '</p>
            <p><b>Organisation: </b>' . $mailData2['organisation'] . '</p>
            <p><b>Purpose: </b>' . $mailData2['purpose'] . '</p>
            <p><b>Message: </b>' . $mailData2['message'] . '</p>
        ';

        $config['protocol'] = 'SMTP';
        $config['smtp_crypto'] = 'STARTTLS';
        $config['smtp_host'] = 'mail.bharatvaani.in';
        $config['smtp_port'] = 587;
        $config['smtp_username'] = 'contactus@bharatvaani.in';
        $config['smtp_password'] = 'bharat@vaani21';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");
        $ci->email->set_newline("\r\n");
        $ci->email->from($from, $fromName);

        $ci->email->to($to);
        $ci->email->cc("");
        $ci->email->bcc("");
        $ci->email->subject("Partner With Us");
        $ci->email->message($mailContent);


        //$this->email->to($to);
        //$this->email->from($from, $fromName);
        //$this->email->subject($mailSubject);
        //$this->email->message($mailContent);
        // Send email & return status
        return $ci->email->send() ? true : false;
    }

    public function openings() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/openings');
    }

    public function apply() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/apply');
    }

    //Apply Now saving to database controller
    public function apply_save() {
        //Personal Details	
        $this->form_validation->set_rules('full_name', 'Full name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('contact_number', 'Contact number', 'required|is_natural|exact_length[10]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        //Online Presence
        $this->form_validation->set_rules('blog', 'Blog Link', 'required');
        $this->form_validation->set_rules('linkedin', 'Linkedin Link');

        if (empty($_FILES['resume']['name'])) {
            $this->form_validation->set_rules('resume', 'Document');
        }

        $this->form_validation->set_rules('facebook', 'Facebook Link', 'required');
        $this->form_validation->set_rules('twitter', 'Twitter Link', 'required');
        //Professional Details
        $this->form_validation->set_rules('work_function', 'Function', 'required');
        $this->form_validation->set_rules('location', 'Preferred Location', 'required');
        $this->form_validation->set_rules('experience', 'Experience', 'required');
        $this->form_validation->set_rules('knowbv', 'How do you hear Bharat Vaani', 'required');
        $this->form_validation->set_rules('about_self_on_bv', 'Description', 'required');
        $this->form_validation->set_rules('reason_to_join', 'Reason to join', 'required');

        if ($this->form_validation->run()) {
            $data = $this->input->post();
            $today = date('Y-m-d');
            $data['date_created'] = $today;

            $data['resume'] = base_url("./assets/resume/" . $_FILES['resume']['name']);
            $data['filename'] = $_FILES['resume']['name'];
            unset($data['submit']);

            $this->load->model('Apply_model');

            if ($this->Apply_model->addData($data)) {
                if (!empty($_FILES['resume']['name'])) {

                    $config['upload_path'] = './assets/resume';
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = 1024;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('resume')) {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('careers/apply', $error);
                    } else {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('careers/apply', $data);
                    }
                } else {
                    
                }
                $this->session->set_flashdata('msg', 'Applied Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Apply!');
            }
            redirect('index/apply');
        } else {
            $this->load->model('Apply_model');
            //$getFun = $this->Apply_model->getFun();
            //$getLocation = $this->Apply_model->getLocation();
            //$getExperience = $this->Apply_model->getExperience();
            ///$getKnowbv = $this->Apply_model->getKnowbv();
            //$apply['getFun']=$getFun;
            //$apply['getLocation']=$getLocation;
            //$apply['getExperience']=$getExperience;
            //$apply['getKnowbv']=$getKnowbv;
            $data['apply'] = $this->input->post();	
			$this->load->view('onecohorts/apply', $data);
            // $this->apply();
        }
    }

    public function faqs() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/faqs');
    }

    public function terms_and_conditions() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/terms-conditions');
    }

    public function privacy_policy() {
        $session = $this->session->userdata('user_session');
        $this->load->view('onecohorts/privacy-policy');
    }

    function logoff() {

        $re = '';
        if (@$this->session->userdata('client_session')) {
            $re = 'client-portal';
        } elseif (!@$this->session->userdata('user_session')) {
            $this->session->sess_destroy();

            redirect('https://' . $_SERVER['HTTP_HOST']);
        } else {
            date_default_timezone_set('Asia/Kolkata');
            $get_activity_data = $this->Adminmaster_model->selectData('login_record', '*', array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
            // echo strtotime($get_activity_data[0]->created_at);
            // echo "<br>";
            // echo time();
            // echo "<br>";
            // echo $date = date('m/d/Y h:i:s a', time());
            // echo "<br>";
            $time = time() - strtotime($get_activity_data[0]->created_at);
            $final_time = round(abs($time) / 60);
            $update = array(
                'time_spend' => $final_time
            );

            $update_status = $this->Adminmaster_model->updateData('login_record', $update, array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
        }

        $this->session->sess_destroy();

        //redirect('https://'.$_SERVER['HTTP_HOST']);
        //$this->load->view('index/logged-off');
        $this->load->view('index/login-back');
    }

    function success() {

        $session_data = $this->session->userdata('user_session');

        if ($session_data['reg_admin_type'] && $session_data['reg_id'] && $session_data['bus_id']) {
            $redirectId = $this->GetRedirectURL($session_data['reg_admin_type'], $session_data['reg_id'], $session_data['bus_id']);

            redirect('community/');
        } else {

            redirect('community/');
        }
    }

    public function get_gst_details() {

        $gstNo = $this->input->post("gst_id");
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://commonapi.mastersindia.co/oauth/access_token/');  //This is the Test API endpoint
        //curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');  //This is the Live API endpoint
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type:application/json"));


        $payload = array(
            'username' => "nimesh@socialplay.co.in",
            'password' => "Nimesh@123#",
            'client_id' => "rktDIDHKCoeReXBVpN",
            'client_secret' => "qyo7sw9RlHj9amXpaFNqFTN0",
            'grant_type' => "password"
        );

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $response = curl_exec($ch);
        curl_close($ch);
        $myArray = array(json_decode($response, true));


        if (isset($myArray[0]['access_token']) && $myArray[0]['access_token'] != "") {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://commonapi.mastersindia.co/commonapis/searchgstin?gstin=' . $gstNo);  //This is the Test API endpoint
            //curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');  //This is the Live API endpoint
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization:Bearer ' . $myArray[0]['access_token'], "client_id:rktDIDHKCoeReXBVpN"));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $result = curl_exec($ch);
            curl_close($ch);

            $gstin_details = array(json_decode($result, true));

            if ($gstin_details[0]["error"] == 0) {
                $state = $gstin_details[0]['data']['pradr']['addr']['stcd'];
                $city = $gstin_details[0]['data']['pradr']['addr']['city'];
                $states = $this->User_model->selectData('states', '*', array('state_name' => $state));
                $cities = $this->User_model->selectData('cities', '*', array('name' => $city));

                $state = $states[0]->state_id;
                $city = $cities[0]->city_id;
                $country = $states[0]->country_id;
                $gstin_details[0]['data']['info'] = array('country_id' => $country, 'state_id' => $state, 'city_id' => $city);
            }

            echo json_encode($gstin_details);
            exit;
        } else {

            echo "Missing Tokens";
        }
    }

    public function getpayment_link() {


        $bus_id = $_POST['bus_id'];

        $data = $this->Adminmaster_model->selectData('businesslist', 'api_key,auth_token', array('bus_id' => $bus_id));

        if ($data) {
            $purpose = $_POST['purpose'];
            $name = $_POST['name'];
            $email = $_POST['email'];
            $phno = $_POST['phno'];
            $amount = $_POST['amount'];

            // $purpose="Invoice no : 00540";
            // $name="Nitesh";
            //    $email="nitesh@windchimes.co.in";
            //    $phno="8652465591";
            //    $amount="25000";

            if (!empty($purpose) and ! empty($name) and ! empty($email) and ! empty($phno) and ! empty($amount) and ! empty($data[0]->api_key) and ! empty($data[0]->auth_token)) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');  //This is the Test API endpoint
                //curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');  //This is the Live API endpoint
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Api-Key:" . $data[0]->api_key . "", "X-Auth-Token:" . $data[0]->auth_token . ""));

                $payload = Array(
                    'purpose' => $purpose,
                    'amount' => $amount,
                    'phone' => $phno,
                    'buyer_name' => $name,
                    'redirect_url' => 'https://' . $_SERVER['HTTP_HOST'],
                    'webhook' => '',
                    'send_email' => true,
                    'send_sms' => true,
                    'email' => $email,
                    'allow_repeated_payments' => false
                );

                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
                $response = curl_exec($ch);
                curl_close($ch);
                $myArray = array(json_decode($response, true));
                //print_r($myArray);
                if ($myArray[0]['success']) {

                    $longu = $myArray[0]["payment_request"]["longurl"];

                    echo $longu;
                } else {
                    echo "Invalid_token";
                }
            } else {
                echo "Missing Parameters";
            }
        } else {

            echo "Missing Tokens";
        }
    }

    public function GetRedirectURL($type, $reg_id, $bus_id) {

        $redirectId = $this->findRedirectPath($type, $reg_id, $bus_id);
        if ($redirectId == '1') {
            return 'sales/billing-documents';
        } else if ($redirectId == '2') {
            return 'tax-summary';
        } else if ($redirectId == '4') {
            return 'employee/manage_employee_master';
        } else if ($redirectId == '5') {
            return 'sales/expense_voucher';
        } else if ($redirectId == '31') {
            return 'profile/add-personal-profile';
        } else if ($redirectId == '32') {
            return 'profile/add-company-profile';
        } else if ($redirectId == '33') {
            return 'sales/billing-documents';
        } else {
            return 'sales/manage-company-profile';
        }
    }

    /* function emailmsg(){
      $this->load->view('index/email-change-password');
      } */

    public function logout() {

        $re = '';
        if (@$this->session->userdata('client_session')) {
            $re = 'client-portal';
        } elseif (!@$this->session->userdata('user_session')) {
            $this->session->sess_destroy();

            redirect('https://' . $_SERVER['HTTP_HOST']);
        } else {
            date_default_timezone_set('Asia/Kolkata');
            $get_activity_data = $this->Adminmaster_model->selectData('login_record', '*', array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
            // echo strtotime($get_activity_data[0]->created_at);
            // echo "<br>";
            // echo time();
            // echo "<br>";
            // echo $date = date('m/d/Y h:i:s a', time());
            // echo "<br>";
            $time = time() - strtotime($get_activity_data[0]->created_at);
            $final_time = round(abs($time) / 60);
            $update = array(
                'time_spend' => $final_time
            );

            $update_status = $this->Adminmaster_model->updateData('login_record', $update, array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
        }

        $this->session->sess_destroy();

        redirect('https://' . $_SERVER['HTTP_HOST']);
    }

    public function logout_signup() {
        $re = '';
        if ($this->session->userdata('client_session')) {
            $re = 'client-portal';
        } else {
            date_default_timezone_set('Asia/Kolkata');
            $get_activity_data = $this->Adminmaster_model->selectData('login_record', '*', array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
            // echo strtotime($get_activity_data[0]->created_at);
            // echo "<br>";
            // echo time();
            // echo "<br>";
            // echo $date = date('m/d/Y h:i:s a', time());
            // echo "<br>";
            $time = time() - strtotime($get_activity_data[0]->created_at);
            $final_time = round(abs($time) / 60);
            $update = array(
                'time_spend' => $final_time
            );

            $update_status = $this->Adminmaster_model->updateData('login_record', $update, array('login_id' => $this->session->userdata['user_session']['reg_login_id']));
        }
        $this->session->sess_destroy();

        redirect(base_path() . $re);
    }

    public function get_states() {
        $country_id = $this->input->post('country_id');
        $states = $this->Adminmaster_model->selectData('states', '*', array('country_id' => $country_id));
        $opt = '';
        $opt.="<option value=''>Select State</option>";
        foreach ($states as $st) {
            $opt.="<option value=" . $st->state_id . " >" . $st->state_name . " - " . $st->state_gst_code . "</option>";
        }
        echo $opt;
    }

    public function get_cities() {
        $state_id = $this->input->post('state_id');
        $cities = $this->Adminmaster_model->selectData('cities', '*', array('state_id' => $state_id));
        $opt = '';
        $opt.="<option value=''>Select City</option>";
        foreach ($cities as $st) {
            $opt.="<option value=" . $st->city_id . " >" . $st->name . "</option>";
        }
        echo $opt;
    }

    /* public function resetPassword($token)
      {
      $data = array();
      if(!empty($token))
      {
      $userData = $this->User_model->fetchUserData("vToken" , $token);
      if(!empty($userData))
      {
      $data['status'] = "success";

      $post = $this->input->post();
      if ($post) {

      $updateUserData = array();
      $updateUserData['vPassword'] = md5($post['newpassword']);
      $updateUserData['vToken'] = md5($userData['vEmail'] . time());

      $this->User_model->updateData($updateUserData , array("iUserId" => $userData['iUserId']));

      $data['status'] = "confirm";
      $data['message'] = "Password Change Successfully.";

      }
      }
      else
      {
      $data['status'] = "error";
      $data['message'] = "Invalid link.";
      }
      }
      else
      {
      $data['status'] = "error";
      $data['message'] = "Invalid link.";
      }

      $this->load->view('index/resetPassword', $data);
      }
     */

    public function check_otp() {
        echo json_encode(true);
    }

    public function home_register() {
        $register_data = array();
        $ip_address = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }

        parse_str($_POST['frm_signup'], $register_data);
        $email = $register_data['email'];

        if (@$register_data['subscribe'] == '1') {
            $exist_email_newsletter = $this->Adminmaster_model->isUnique('newsletter', 'email', $register_data['email']);
            if ($exist_email_newsletter == TRUE) {
                $data = array(
                    'email' => $register_data['email'],
                );
                $this->Adminmaster_model->insertData('newsletter', $data);
            }
        } else {
            unset($register_data['subscribe']);
        }

        $exist_email = $this->Adminmaster_model->isUnique('registration', 'reg_email', $email);


        if ($exist_email == TRUE) {
            $array = array(
                'bus_company_name' => '',
            );
//            $get_company_id = $this->Adminmaster_model->insertData('businesslist', $array);
            $reg_otp = mt_rand(100000, 999999);
            $reg_mob_otp = mt_rand(100000, 999999);
            $data_to_be_insert = array(
//                'bus_id' => $get_company_id,
                //'reg_username'		=>	$register_data['email'],
                'reg_email' => $register_data['email'],
                'reg_password' => md5($register_data['password_2']),
                //'reg_mobile'		=>	$register_data['contact'],
                //'reg_istrial'		=>	1,
                'reg_ipaddress' => $ip_address,
                'reg_otp' => $reg_otp,
                //'reg_mob_otp'			=>	$reg_mob_otp,
                'reg_otp_verify' => 'No',
                'reg_admin_type' => 3,
                'status' => "Inactive",
            );
            $get_admin_id = $this->Adminmaster_model->insertData('registration', $data_to_be_insert);
            if ($get_admin_id > 0) {

                echo json_encode($get_admin_id);
                exit;
                /*   $name=explode(' ',$register_data['email']);
                  if(isset($name[1]) && $name[1]!=""){;
                  $last_name	=$name[1];
                  }else{
                  $last_name	="";
                  }

                  $from="Xebra Cohorts";
                  $dataa['user_name'] =  $register_data['email'];
                  $dataa['user_start_date'] = date("d M y");
                  $dataa['user_end_date'] = Date('d M y', strtotime("+14 days"));
                  $htmldataa = $this->load->view('email_template/trial_period_begins',$dataa,true);
                  $htmlWelcome = $this->load->view('email_template/welcome-user-email',$dataa,true);
                  sendEmail($from,$register_data['email'],'Welcome to Xebra Cohorts',$htmlWelcome);
                  //sendEmail($from,$register_data['email'],'Trial Period Begins',$htmldataa);


                  $user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id"=>$get_admin_id));
                  $mob_otp=$user[0]->reg_mob_otp;
                  $otp=$user[0]->reg_otp;
                  $message="The one-time password to register on Xebra Cohorts is $mob_otp.";
                  $msg="The one-time password to register on Xebra is $reg_otp.";
                  //send_sms($user[0]->reg_mobile,$message);
                  $from="Xebra Cohorts";
                  $subject="Hi! Here’s your OTP.";
                  $data['name'] = $register_data['email'];
                  $data['otp'] = $otp;
                  $content=$this->load->view('email_template/otp',$data,true);
                  sendEmail($from,$register_data['email'],$subject,$content);
                  echo json_encode($user);
                  exit();
                 */
            } else {
                echo "false";
                exit();
            }
        } else {
            echo 'notExits';
            exit();
        }
    }

    public function send_otp() {
        $post = $this->input->post();
        $user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $post['reg_id']));



        $mob_otp = $user[0]->reg_mob_otp;
        $otp = $user[0]->reg_otp;
        $message = "The one-time password to register on Xebra Cohorts is $mob_otp.";
        $msg = "The one-time password to register on Xebra Cohorts is $otp.";
        send_sms($user[0]->reg_mobile, $message);

        $from = "Xebra Cohorts";
        $subject = "Hi! Here’s your OTP.";
        $data['name'] = $user[0]->reg_username;
        $data['otp'] = $otp;
        $content = $this->load->view('email_template/otp', $data, true);
        sendEmail($from, $user[0]->reg_email, $subject, $content);


        exit;
    }

    public function send_login_otp() {
        $post = $this->input->post();
        $user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $post['reg_id']));



        $reg_otp = mt_rand(100000, 999999);
        $reg_mob_otp = mt_rand(100000, 999999);
        $this->Adminmaster_model->updateData('registration', array("reg_otp" => $reg_otp, "reg_mob_otp" => $reg_mob_otp), array("reg_id" => $user[0]->reg_id));

        $message = "The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
        $from = "Xebra Cohorts";
        $subject = "Hi! Here’s your OTP.";
        $data['name'] = $user[0]->reg_username;
        $data['otp'] = $reg_otp;
        $content = $this->load->view('email_template/otp', $data, true);
        sendEmail($from, $user[0]->reg_email, $subject, $content);

        send_sms($user[0]->reg_mobile, $message);

        exit;
    }

    public function reactive_user() {
        $session_data = $this->session->userdata('temp_user_session');

        $this->session->set_userdata('user_session', $session_data);
        $status = $this->session->userdata('user_session');

        if ($status) {
            $userid = $this->session->userdata['user_session']['reg_id'];

            $update = array(
                'status' => 'Active',
                'reg_otp' => '',
            );

            $update_status = $this->Adminmaster_model->updateData('registration', $update, array("reg_id" => $userid));

            // $get_client_list = $this->Adminmaster_model->selectData('sales_customers', 'cust_id', array("reg_id"=>$userid));
            // $get_client = json_decode( json_encode($get_client_list), true);
            // print_r($get_client);
            // $update = array(
            // 	'status'	=>	'InActive',
            // 	'cp_otp'	=>	'',
            // );
            // $update_status = $this->Adminmaster_model->updateData('sales_customer_contacts',$update,array("cust_id"=>$get_client));

            $update_clients_status = $this->Adminmaster_model->updateData('registration', $update, array("reg_createdby" => $userid));

            if ($update_status == true) {
                echo "success";
            } else {
                echo "cancel";
            }
        } else {
            echo "cancel";
        }

        exit();
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function login() {
        $where = array();
        parse_str($_POST['frm_login'], $where);
        $email = $where['email'];
        $password = $where['password'];
        $remember = $where;
        $data = array();
        if ($where) {
            $error = array();
            $e_flag = 0;
            if (trim($where['email']) == '') {
                $error['userid'] = 'Username cannot be blank.';
                $e_flag = 1;
            }
            if (trim($where['password']) == '') {
                $error['password'] = 'Password cannot be blank.';
                $e_flag = 1;
            }


            if ($e_flag == 0) {
                $where = array(
                    'reg_email' => $email,
                    'reg_password' => md5($password),
                );


                $chekUserEmail = $this->Adminmaster_model->selectData('registration', '*', array('reg_email' => $email));
                if (!$chekUserEmail) {
                    echo 'InvalidEmail';
                    exit();
                }

                $user = $this->Adminmaster_model->selectData('registration', '*', $where);
   
                if (count($user) > 0) {

                    //$gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id'=>$user[0]->bus_id,'type'=>'business'),'gst_id','ASC');
                    $gst_number = '';




                    $array['reg_id'] = $user[0]->reg_id;
                    $array['bus_id'] = $user[0]->bus_id;
                    $array['access'] = $user[0]->reg_admin_type;
                    $array['ip_address'] = $this->get_client_ip();

                    $activity_login_id = $this->Adminmaster_model->insertData('login_record', $array);

                    /* if($user[0]->reg_istrial==1){
                      $now = time();
                      $created_date = strtotime($user[0]->createdat);
                      $datediff = $now - $created_date;
                      if(round($datediff / (60 * 60 * 24))>15){

                      $session_data = array(
                      'reg_id' => $user[0]->reg_id,
                      'bus_id' => $user[0]->bus_id,
                      'email'	 => $user[0]->reg_email,
                      'mobile'	 => $user[0]->reg_mobile,
                      'ei_username' => $user[0]->reg_username,
                      'ei_gst' => $gst_number,
                      'user_photo'=>$user[0]->reg_profile_image,
                      'createdat'	 => $user[0]->createdat,
                      'reg_istrial'	 => $user[0]->reg_istrial,
                      'reg_admin_type'	 => $user[0]->reg_admin_type,
                      'reg_createdby'	 => $user[0]->reg_createdby,
                      'reg_trial_expire' => '1',
                      'reg_login_id'=> $activity_login_id,
                      );
                      $this->session->unset_userdata('user_session');
                      $this->session->unset_userdata('client_session');
                      $this->session->set_userdata('user_session',$session_data);

                      echo 'trial_period_expired';
                      exit();
                      }
                      } */
                      
                    

                    if ($user[0]->reg_otp_verify == 'No') {

                        echo json_encode($user[0]->reg_id);
                        exit();
                    }



                    /* if($user[0]->status == 'Inactive')
                      {
                      $session_data = array(
                      'reg_id' => $user[0]->reg_id,
                      'bus_id' => $user[0]->bus_id,
                      'email'	 => $user[0]->reg_email,
                      'mobile'	 => $user[0]->reg_mobile,
                      'ei_username' => $user[0]->reg_username,
                      'ei_gst' => $gst_number,
                      'user_photo'=>$user[0]->reg_profile_image,
                      'createdat'	 => $user[0]->createdat,
                      'reg_istrial'	 => $user[0]->reg_istrial,
                      'reg_admin_type'	 => $user[0]->reg_admin_type,
                      'reg_createdby'	 => $user[0]->reg_createdby,
                      'reg_login_id'=> $activity_login_id,

                      );
                      $this->session->unset_userdata('user_session');
                      $this->session->unset_userdata('client_session');
                      $this->session->set_userdata('temp_user_session',$session_data);
                      echo "error_account";
                      exit();
                      } */

                    $session_data = array(
                        'reg_id' => $user[0]->reg_id,
                        'bus_id' => $user[0]->bus_id,
                        'email' => $user[0]->reg_email,
                        'mobile' => $user[0]->reg_mobile,
                        'ei_username' => $user[0]->reg_username,
                        'reg_username' => $user[0]->reg_username,
                        'ei_gst' => $gst_number,
                        'user_photo' => $user[0]->reg_profile_image,
                        'createdat' => $user[0]->createdat,
                        'reg_istrial' => $user[0]->reg_istrial,
                        'reg_admin_type' => $user[0]->reg_admin_type,
                        'reg_createdby' => $user[0]->reg_createdby,
                        'reg_login_id' => $activity_login_id,
                        'reg_login_id' => $activity_login_id,
                        'status' => $user[0]->status,
                    );

                    $this->session->unset_userdata('user_session');
                    $this->session->unset_userdata('client_session');
                    $this->session->unset_userdata('vendor_session');
                    $this->session->set_userdata('user_session', $session_data);

                    if (isset($remember['remember_me']) && $remember['remember_me'] == "on") {

                        $year = time() + 31536000;
                        //setcookie('remember_me_u', $remember['email'], $year);
                        //setcookie('remember_me_pass', $remember['password'], $year);

                        $my_cookie = array(
                            'name' => 'remember_me_u',
                            'value' => $remember['email'],
                            'expire' => '30000000',
                            'secure' => TRUE
                        );
                        $this->input->set_cookie($my_cookie);

                        $my_cookie1 = array(
                            'name' => 'remember_me_pass',
                            'value' => $remember['password'],
                            'expire' => '30000000',
                            'secure' => TRUE
                        );
                        $this->input->set_cookie($my_cookie1);
                    }


                    $redirectId = "my-Community";
                    echo $redirectId;
                    exit;
                    // echo "success";
                    // exit();
                }
                echo "error_password";
                exit();
            }
        }
    }

    function findRedirectPath($reg_admin_type, $reg_id, $bus_id) {
        if ($reg_admin_type == 5) {
            return 5;
        } elseif ($reg_admin_type == 4) {
            return 4;
        } elseif ($reg_admin_type == 3 || $reg_admin_type == -1) {
            $user_data = $this->Adminmaster_model->selectData('registration', 'reg_username', array("reg_createdby" => $reg_id));
            $bus_data = $this->Adminmaster_model->selectData('businesslist', 'bus_company_name', array("bus_id" => $bus_id));
            if (!$user_data) {
                return 31;
            } elseif (!$bus_data) {

                return 32;
            } else {

                return 33;
            }
        } elseif ($reg_admin_type == 2) {

            return 2;
        } elseif ($reg_admin_type == 1) {

            return 1;
        } else {
            return base_url() . 'dashboard';
        }
    }

    public function new_index() {
        $this->load->view('index/new_index');
    }

    function deactive_my_account() {

        if ($this->session->userdata('user_session')) {
            $bus_id = $this->session->userdata['user_session']['bus_id'];
            $userid = $this->session->userdata['user_session']['reg_id'];

            $otp = rand(100000, 999999);
            $user_data = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $userid));
            if (count($user_data) > 0) {
                if ($this->input->post('mobile_no') == 1) {

                    $message = "Your OTP password for deactivate account in Xebra is:" . $otp . ". Please do not share with anyone.";
                    send_sms($user_data[0]->reg_mobile, $message);
                }

                if ($this->input->post('email_add') == 1) {
                    //$msg="The one-time password to reset your MyEasyInvoices password is :".$otp;
                    //$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);
                    //$this->email->set_mailtype("html");
                    //$this->email->from('admin@windchimes.co.in', "EasyInvoices");
                    //$this->email->to($userdata['emailid']);
                    //$this->email->subject("Adieu invoicing nightmares!");
                    //$from="admin@windchimes.co.in";
                    /* $to=$user_data[0]->reg_email;
                      $subject="Deactive Account - MyEasyInvoices";
                      $message = "<html><body>";
                      $message .= "<p style='text-align:center;'><img src='".base_url()."asset/images/logo.png' alt='Team Invoice Management'>       </p><h3 style='text-align:center;'>Password assistance</h3> ";
                      $message .= "<p style='text-align:center;text-transform: uppercase;'>Do not reply to this email. it is auto generated.</p>";
                      $message .= "<p>Here's your OTP</p>";
                      $message .= "<h4>".$otp."</h4>";
                      $message .= "<p>To deactive your account, we will need you to verify your account. It's really simple.</p>";
                      $message .= "<p>Simply enter ".$otp." as the OTP in the My Easy Invoices page and you will have complete access to your account.</p>";
                      $message .= "<h5>Happy Invoicing to you!</h5></body></html>"; */
                    //$this->email->message($message);
                    //$this->email->send();
                    $from = "Xebra cohorts";
                    $to = $user_data[0]->reg_email;
                    $msg = "deactive your account";
                    $data['name'] = ucwords($user_data[0]->reg_username);
                    $data['otp'] = $otp;
                    $data['msg'] = $msg;
                    $subject = "Hi! Here’s your OTP.";
                    $message = $this->load->view('email_template/otp', $data, true);
                    sendEmail($from, $to, $subject, $message);
                }

                $update = array(
                    'reg_otp' => $otp,
                );
                /* $update = array(
                  'status'	=>	'Inactive',
                  ); */
                $update_status = $this->Adminmaster_model->updateData('registration', $update, array("reg_id" => $userid, "bus_id" => $bus_id));
                if ($update_status != false) {
                    //$sess_array = $this->session->sess_destroy();
                    echo true;
                } else {
                    echo false;
                }
            } else {
                echo false;
            }
        } else {
            echo false;
        }
    }

    function match_acc_otp() {
        $otp_array = array();
        parse_str($_POST['frm'], $otp_array);
        if ($this->session->userdata('user_session')) {
            $bus_id = $this->session->userdata['user_session']['bus_id'];
            $userid = $this->session->userdata['user_session']['reg_id'];
            $user_data = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $userid, "bus_id" => $bus_id));

            $otp = intval(trim($otp_array['acc_otp'], ' '));
            if ($user_data[0]->reg_otp == $otp) {
                $update = array(
                    'status' => 'Inactive',
                    'reg_otp' => '',
                );

                $update_status = $this->Adminmaster_model->updateData('registration', $update, array("reg_id" => $userid, "bus_id" => $bus_id));

                $update_clients_status = $this->Adminmaster_model->updateData('registration', $update, array("reg_createdby" => $userid));
                if ($update_status != false) {
                    $sess_array = $this->session->sess_destroy();
                    echo json_encode(true);
                } else {
                    echo json_encode(false);
                }
            } else {
                echo json_encode(false);
            }
        } else {
            echo json_encode(false);
        }
    }

    public function change_password() {
        $change_password = array();
        parse_str($_POST['frm'], $change_password);
        $old_password = $change_password['old_password'];
        $bus_id = $this->session->userdata['user_session']['bus_id'];
        $reg_id = $this->session->userdata['user_session']['reg_id'];

        $filter = array(
            'reg_id' => $reg_id,
            'bus_id' => $bus_id,
            'reg_password' => md5($old_password),
        );
        $userdata = $this->Adminmaster_model->selectData('registration', '*', $filter);

        if (count($userdata) > 0) {
            $otp = rand(100000, 999999);
            // $otp_array = array(
            // 	'reg_otp' => $otp,
            // );
            $array_password = array(
                'reg_id' => $reg_id,
                'reg_otp' => $otp,
                'reg_password' => md5($change_password['new_password']),
                'reg_otp_verify' => "No",
            );
            //$this->session->unset_userdata('change_password_info');
            //$this->session->set_userdata('change_password_info',$array_password);

            $insertinfo = $this->Adminmaster_model->updateData('registration', $array_password, array('reg_id' => $reg_id));
            if ($insertinfo != false) {
                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            echo json_encode(false);
        }
    }

    function sent_change_pwdotp() {

        if ($this->session->userdata('user_session')) {
            $bus_id = $this->session->userdata['user_session']['bus_id'];
            $userid = $this->session->userdata['user_session']['reg_id'];

            $userdata = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $userid, "bus_id" => $bus_id));
            $otp = $this->session->userdata['change_password_info']['reg_otp'];

            if (count($userdata) > 0) {

                if ($this->input->post('mobile_no') == 1) {
                    $msg = "The OTP password to reset your Xebra account password is :" . $otp;
                    send_sms($userdata[0]->reg_mobile, $msg);

                    //$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);
                }
                if ($this->input->post('email_add') == 1) {
                    //$msg="The one-time password to reset your MyEasyInvoices password is :".$otp;
                    //$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);
                    //$this->email->set_mailtype("html");
                    //$this->email->from('admin@windchimes.co.in', "EasyInvoices");
                    //$this->email->to($userdata['emailid']);
                    //$this->email->subject("Adieu invoicing nightmares!");
                    //$from="admin@windchimes.co.in";
                    /* $to=$userdata[0]->reg_email;
                      $subject="Change Password - MyEasyInvoices";
                      $message = "<html><body>";
                      $message .= "<p style='text-align:center;'><img src='".base_url()."asset/images/logo.png' alt='Team Invoice Management'>       </p><h3 style='text-align:center;'>Password assistance</h3> ";
                      $message .= "<p style='text-align:center;text-transform: uppercase;'>Do not reply to this email. it is auto generated.</p>";
                      $message .= "<p>Here's your OTP</p>";
                      $message .= "<h4>".$otp."</h4>";
                      $message .= "<p>To change your password, we will need you to verify your account. It's really simple.</p>";
                      $message .= "<p>Simply enter ".$otp." as the OTP in the My Easy Invoices page and you will have complete access to your account.</p>";
                      $message .= "<h5>Happy Invoicing to you!</h5></body></html>"; */
                    $to = $userdata[0]->reg_email;
                    $from = "Xebra cohorts";
                    $msg = "change your account password";
                    $data['name'] = ucwords($userdata[0]->reg_username);
                    $data['otp'] = $otp;
                    $data['msg'] = $msg;
                    $subject = "Hi! Here’s your OTP.";
                    $message = $this->load->view('email_template/otp', $data, true);
                    //$this->email->message($message);
                    //$this->email->send();
                    sendEmail($from, $to, $subject, $message);
                }
                /* $update_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_id"=>$userid,"bus_id"=>$bus_id));
                  if($update_status != false)
                  {
                  //$sess_array = $this->session->sess_destroy();
                  echo true;
                  }
                  else {
                  echo false;
                  } */
                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            echo json_encode(false);
        }
    }

    public function check_otp_for_password() {
        $otp_array = array();
        parse_str($_POST['frm'], $otp_array);
        if ($this->session->userdata('user_session')) {
            $bus_id = $this->session->userdata['user_session']['bus_id'];
            $userid = $this->session->userdata['user_session']['reg_id'];
            $ei_username = $this->session->userdata['user_session']['ei_username'];


            $otp = intval(trim($otp_array['acc_pwdotp'], ' '));

            //$check = $otp_array['password_otp'];

            $id = $this->session->userdata['change_password_info']['reg_id'];
            $set_password = $this->session->userdata['change_password_info']['reg_password'];
            //$reg_otp = $this->session->userdata['change_password_info']['reg_otp'];
            $match = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $id, "reg_otp" => $otp));
            //$match = $this->Clientmodel->selectData('registration','*',array("reg_id"=>$userid,"bus_id"=>$bus_id));
            if (count($match) == 1) {
                $update_status = $this->Adminmaster_model->updateData('registration', array('reg_password' => md5($set_password), 'reg_otp' => ''), array("reg_id" => $userid, "bus_id" => $bus_id));
                $this->session->unset_userdata('change_password_info');
                $to = $this->session->userdata['user_session']['email'];
                $data['password'] = $set_password;
                $data['name'] = ucfirst($this->session->userdata['user_session']['ei_username']);
                $subject = "Here’s your new password";
                $message = $this->load->view('email_template/change-password', $data, true);
                $from = "Xebra cohorts";
                sendEmail($from, $to, $subject, $message);
                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            echo json_encode(false);
        }
    }

    public function feedback() {
        $feedbck = array();
        parse_str($_POST['frm'], $feedbck);
        if ($this->session->userdata('user_session')) {
            $bus_id = $this->session->userdata['user_session']['bus_id'];
            $reg_id = $this->session->userdata['user_session']['reg_id'];


            $data = array(
                'reg_id' => $reg_id,
                'bus_id' => $bus_id,
                'rate' => $feedbck['rate'],
                'feedback' => $feedbck['feedback'],
            );
            $update_status = $this->Adminmaster_model->insertData('feedback', $data);

            if ($update_status != false) {
                $to = $this->session->userdata['user_session']['email'];
                $data['name'] = ucfirst($this->session->userdata['user_session']['ei_username']);
                $subject = "We have received your feedback";
                $message = $this->load->view('email_template/feedback', $data, true);
                $from = "Bharatvaani cohorts";
                sendEmail($from, $to, $subject, $message);
                //////////////////////////////////////////////
                $tofb = "feedback@bharatvaani.in";
                $data['name'] = ucfirst($this->session->userdata['user_session']['ei_username']);
                $subjectfb = "Feedback from Bharatvaani user" . " " . ucfirst($this->session->userdata['user_session']['ei_username']);
                $messagefb = $this->load->view('email_template/feedback_client', $data, true);
                sendEmail($from, $tofb, $subjectfb, $messagefb);
                ///////////////////////////////////////////////////////


                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            echo json_encode(false);
        }
    }

    function match_signup_otp() {
        $post = array();

        parse_str($_POST['frm_otp'], $post);

        if ($post) {
            unset($post['submit']);
            $get_otp = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $post['reg_id']));
            if ($post['phone_otp'] == $get_otp[0]->reg_otp) {
                $this->Adminmaster_model->updateData('registration', array("reg_otp_verify" => "Yes"), array("reg_id" => $post['reg_id']));
                //	$gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id'=>$get_otp[0]->bus_id,'type'=>'business'),'gst_id','ASC');
                $gst_number = '';
                /* 	if(count($gst)>0){
                  $gst_number=$gst[0]->gst_id;
                  } */

                $get_admin_id = $get_otp[0]->reg_id;
                $from = "Bharatvaani Cohorts";
                $dataa['name'] = $get_otp[0]->reg_username;
                $dataa['user_start_date'] = date("d M y");
                $dataa['user_end_date'] = Date('d M y', strtotime("+14 days"));
                //$htmldataa = $this->load->view('email_template/trial_period_begins',$dataa,true);
                $htmlWelcome = $this->load->view('email_template/welcome-user-email', $dataa, true);

                //	sendEmail($from,$get_otp[0]->reg_email,'Trial Period Begins',$htmldataa);
                sendEmail($from, $get_otp[0]->reg_email, 'Welcome to Bharatvaani Cohorts', $htmlWelcome);


                // Activity Tracking

                $array['reg_id'] = $get_otp[0]->reg_id;
                $array['bus_id'] = $get_otp[0]->bus_id;
                $array['access'] = $get_otp[0]->reg_admin_type;
                $array['ip_address'] = $this->get_client_ip();

                $activity_login_id = $this->Adminmaster_model->insertData('login_record', $array);

                $session_data = array(
                    'reg_id' => $get_otp[0]->reg_id,
                    'bus_id' => $get_otp[0]->bus_id,
                    'email' => $get_otp[0]->reg_email,
                    'ei_username' => $get_otp[0]->reg_username,
                    'reg_username' => $get_otp[0]->reg_username,
                    'ei_gst' => $gst_number,
                    'user_photo' => $get_otp[0]->reg_profile_image,
                    'reg_admin_type' => $get_otp[0]->reg_admin_type,
                    'createdat' => $get_otp[0]->createdat,
                    'reg_istrial' => $get_otp[0]->reg_istrial,
                    'reg_createdby' => $get_otp[0]->reg_createdby,
                    'reg_login_id' => $activity_login_id,
                );

                $this->session->unset_userdata('user_session');
                $this->session->unset_userdata('client_session');
                $this->session->set_userdata('user_session', $session_data);
                echo json_encode(true);
                exit();
            } else {
                echo json_encode(false);
                exit();
            }
        } else {
            echo json_encode(false);
            exit();
        }
    }

    public function forgot_password() {

        $check_email = array();

        parse_str($_POST['frm'], $check_email);

        $emailid = $check_email['get_email'];

        $registration = $this->Adminmaster_model->selectData('registration', '*', array("reg_email" => $emailid));

        if (count($registration) > 0) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 12; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $this->session->set_userdata('forgot_email', $emailid);
            // $get_web_token=rand(100000,999999);
            // $get_mob_token=rand(100000,999999);

            $reset_array = array(
                'forget_pwd_token' => $randomString,
            );

            $insertotp = $this->Adminmaster_model->updateData('registration', $reset_array, array('reg_email' => $emailid));
            // $span = ucwords(@$registration[0]->reg_username);

            if ($insertotp != false) {



                $msg = base_url() . 'index/changemypassword?mytoken=' . $randomString . '&email=' . $emailid;
                //send_sms($registration[0]->reg_mobile,$msg);

                $to = $emailid;
                $data['name'] = ucwords($registration[0]->reg_username);
                //$msg="Change your account password";
                $from = "Bharatvaani Cohorts";
                $data['msg'] = $msg;
                $subject = "Trouble logging in? Reset your password";
                $message = $this->load->view('email_template/forget_link', $data, true);
                sendEmail($from, $to, $subject, $message);
            }

            echo json_encode(TRUE);
        } else {

            echo json_encode(FALSE);
        }
    }

    public function changemypassword() {

        $this->session->userdata('forgot_email');
        if ($_GET['email']) {
            if ($_GET['mytoken']) {
                $flag = $this->Adminmaster_model->selectData('registration', '*', array('reg_email' => $_GET['email'], 'forget_pwd_token' => $_GET['mytoken']));
                if ($flag) {
                    // $("#reset_password_model").modal('show');

                    $data['email_ei'] = $_GET['email'];
                    $data['token_ei'] = $_GET['mytoken'];
                    $this->load->view('index/email-change-password', $data);
                } else {

                    redirect('');
                }
            }
        }
    }

    public function update_user_password() {
        echo "fsdf";
        exit;

        $get_data = array();

        parse_str($_POST['frm'], $get_data);

        $emailid = $get_data['email'];

        $token = $get_data['token'];

        $registration = $this->Adminmaster_model->selectData('registration', '*', array("reg_email" => $emailid, 'forget_pwd_token' => $token));

        if (count($registration) > 0) {

            $reset_array = array(
                'reg_password' => md5($get_data['new_password']),
            );

            $insertotp = $this->Adminmaster_model->updateData('registration', $reset_array, array('reg_email' => $emailid, 'forget_pwd_token' => $token));
            // $span = ucwords(@$registration[0]->reg_username);

            if ($insertotp != false) {
                
            }
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }

    public function match_verification_code() {

        if ($this->session->userdata('forgot_email') != '') {

            $check_code = array();

            parse_str($_POST['frm3'], $check_code);

            $var_code = $check_code['verification_code'];

            $mbl_code = $check_code['mbl_code'];


            $flag = $this->Adminmaster_model->selectData('registration', '*', array('reg_email' => $this->session->userdata('forgot_email'), 'reg_otp' => $var_code, 'reg_mob_otp' => $mbl_code));

            if (count($flag) == 1) {
                $flag = $this->Adminmaster_model->updateData('registration', array('reg_otp_verify' => 'Yes'), array('reg_email' => $this->session->userdata('forgot_email')));
                echo json_encode(TRUE);
            } else {

                echo json_encode(FALSE);
            }
        }
    }

    public function reset_password() {

        $check_email = array();



        parse_str($_POST['frm4'], $check_email);



        $emailid = $check_email['email'];

        $registration = $this->Adminmaster_model->selectData('registration', '*', array("reg_email" => $emailid));

        if (count($registration) > 0) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 12; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $this->session->set_userdata('forgot_email', $emailid);
            // $get_web_token=rand(100000,999999);
            // $get_mob_token=rand(100000,999999);

            $array_password = array(
                'reg_password' => md5($check_email['new_password']),
            );

            $reset_array = array(
                'forget_pwd_token' => $randomString,
            );

            $insertotp = $this->Adminmaster_model->updateData('registration', $array_password, array('reg_email' => $emailid));
            // $span = ucwords(@$registration[0]->reg_username);

            /*  if($insertotp!=false){



              $msg=base_url().'index/changemypassword?mytoken='. $randomString.'&email='.$emailid;
              //send_sms($registration[0]->reg_mobile,$msg);

              $to=$emailid;
              $data['name']=ucwords($registration[0]->reg_username);

              //$msg="Change your account password";
              $from="Xebra";
              $data['msg']=$msg;
              $subject="Trouble logging in? Reset your password";
              $message=$this->load->view('email_template/forget_link',$data,true);
              sendEmail($from,$to,$subject,$message);


              } */



            echo json_encode(TRUE);
        } else {

            echo json_encode(FALSE);
        }
    }

    public function view_gift() {
        $this->load->view('index/activate-gift');
    }

    public function signin() {
        $session = $this->session->userdata('user_session');
        if (isset($session['reg_id']) && (isset($session['status']) && $session['status']=="Active")) {
			redirect('community');
		}
        $this->load->view('index/index');
    }

    public function signup() {
        $this->load->view('index/cohorts_signup');
    }

    public function resend_email_otp() {

        $post = $this->input->post();

        //$reg_email = $post['resend_email'];
        //$reg_password = $post['resend_password'];
        //$reg_email = 'vanlal@windchimes.co.in';
        //$reg_password = 'c8bdfd41d406005533290dff3b026c61';

        $user = $this->Anupdminmaster_model->selectData('registration', '*', array('reg_id' => $post['reg_id_otp']));


        $array['reg_id'] = $user[0]->reg_id;
        $array['bus_id'] = $user[0]->bus_id;
        $array['access'] = $user[0]->reg_admin_type;
        $array['ip_address'] = $this->get_client_ip();

        //	$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);
        //if($user[0]->reg_istrial==1){
        $reg_otp = mt_rand(100000, 999999);
        $reg_mob_otp = mt_rand(100000, 999999);
        $this->Adminmaster_model->updateData('registration', array("reg_otp" => $reg_otp), array("reg_id" => $user[0]->reg_id));
        //print $this->db->last_query(); exit();

        $message = "The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
        $from = "Xebra Cohorts";
        $subject = "Hi! Here’s your OTP.";
        $data['name'] = $user[0]->reg_username;
        $data['otp'] = $reg_otp;
        $content = $this->load->view('email_template/otp', $data, true);
        sendEmail($from, $user[0]->reg_email, $subject, $content);

        echo json_encode($user);
        exit();
        //}
    }

    public function resend_mob_otp() {

        $post = $this->input->post();

        //$reg_email = $post['resend_email'];
        //$reg_password = $post['resend_password'];
        //$reg_email = 'vanlal@windchimes.co.in';
        //$reg_password = 'c8bdfd41d406005533290dff3b026c61';

        $user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $post['reg_id_otp']));


        $array['reg_id'] = $user[0]->reg_id;
        $array['bus_id'] = $user[0]->bus_id;
        $array['access'] = $user[0]->reg_admin_type;
        $array['ip_address'] = $this->get_client_ip();

        //	$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);
        //if($user[0]->reg_istrial==1){
        $reg_otp = mt_rand(100000, 999999);
        $reg_mob_otp = mt_rand(100000, 999999);
        $this->Adminmaster_model->updateData('registration', array("reg_otp" => $reg_otp), array("reg_id" => $user[0]->reg_id));
        //print $this->db->last_query(); exit();

        $message = "The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
        $from = "Xebra";
        $subject = "Hi! Here’s your OTP.";
        $data['name'] = $user[0]->reg_username;
        $data['otp'] = $reg_otp;
        $content = $this->load->view('email_template/otp', $data, true);
        sendEmail($from, $user[0]->reg_email, $subject, $content);

        echo json_encode($user);
        exit();
        //}
    }

    public function send_mob_otp() {


        $post = $this->input->post();

        //$reg_email = $post['resend_email_mobile'];
        //$reg_password = $post['resend_password_mobile'];
        //$reg_email = 'vanlal@windchimes.co.in';
        //$reg_password = 'c8bdfd41d406005533290dff3b026c61';

        $user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $post['reg_id_otp']));

        /* $array['reg_id'] = $user[0]->reg_id;
          $array['bus_id'] = $user[0]->bus_id;
          $array['access'] = $user[0]->reg_admin_type;
          $array['ip_address'] = $this->get_client_ip();

          $activity_login_id = $this->Adminmaster_model->insertData('login_record',$array); */

        //if($user[0]->reg_istrial==1){
        $reg_otp = mt_rand(100000, 999999);
        $reg_mob_otp = mt_rand(100000, 999999);
        $this->Adminmaster_model->updateData('registration', array("reg_mob_otp" => $reg_mob_otp), array("reg_id" => $user[0]->reg_id));
        $message = "The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
        send_sms($user[0]->reg_mobile, $message);

        echo json_encode($user);
        exit();
        //}
    }

    public function verify_mob_otp() {

        $post = $this->input->post();
        $mbl_code = $post['otp'];
        $userid = $this->user_session['reg_id'];
        $user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $userid, 'reg_mob_otp' => $mbl_code));

        if (count($user) > 0) {

            $flag = $this->Adminmaster_model->updateData('registration', array('reg_otp_verify' => 'Yes', 'status' => 'Active'), array('reg_id' => $userid));

            $gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id' => $user[0]->bus_id, 'type' => 'business'), 'gst_id', 'ASC');
            $gst_number = '';
            if (count($gst) > 0) {
                $gst_number = $gst[0]->gst_id;
            }

            $array['reg_id'] = $user[0]->reg_id;
            $array['bus_id'] = $user[0]->bus_id;
            $array['access'] = $user[0]->reg_admin_type;
            $array['ip_address'] = $this->get_client_ip();

            $activity_login_id = $this->Adminmaster_model->insertData('login_record', $array);


            $session_data = array(
                'reg_id' => $user[0]->reg_id,
                'bus_id' => $user[0]->bus_id,
                'email' => $user[0]->reg_email,
                'mobile' => $user[0]->reg_mobile,
                'ei_username' => $user[0]->reg_username,
                'reg_username' => $get_otp[0]->reg_username,
                'ei_gst' => $gst_number,
                'user_photo' => $user[0]->reg_profile_image,
                'createdat' => $user[0]->createdat,
                'reg_istrial' => $user[0]->reg_istrial,
                'reg_admin_type' => $user[0]->reg_admin_type,
                'reg_createdby' => $user[0]->reg_createdby,
                'reg_login_id' => $activity_login_id,
                'reg_login_id' => $activity_login_id,
                'status' => $user[0]->status,
            );

            $this->session->unset_userdata('user_session');
            $this->session->set_userdata('user_session', $session_data);
            echo json_encode(TRUE);
            exit;
        } else {

            echo json_encode(FALSE);
            exit;
        }
    }

    public function verify_registration() {

        $post = $this->input->post();

        $userid = $this->user_session['reg_id'];
        $user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $userid, 'status' => 'Active'));

        if (count($user) > 0) {

            echo json_encode(TRUE);
            exit;
        } else {

            echo json_encode(FALSE);
            exit;
        }
    }

    public function send_verification() {

        $post = $this->input->post();
        $registration = $this->Adminmaster_model->selectData('registration', '*', array("reg_id" => $post['reg_id']));

        if (count($registration) > 0) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 12; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $emailid = $registration[0]->reg_email;

            $this->session->set_userdata('forgot_email', $emailid);
            // $get_web_token=rand(100000,999999);
            // $get_mob_token=rand(100000,999999);

            $reset_array = array(
                'forget_pwd_token' => $randomString,
            );

            $insertotp = $this->Adminmaster_model->updateData('registration', $reset_array, array('reg_email' => $emailid));
            // $span = ucwords(@$registration[0]->reg_username);

            if ($insertotp != false) {



                $msg = base_url() . 'index/verify?mytoken=' . $randomString . '&email=' . $emailid;
                //send_sms($registration[0]->reg_mobile,$msg);

                $to = $emailid;
                $data['name'] = ucwords($registration[0]->reg_username);
                //$msg="Change your account password";
                $from = "Bharat Vaani";
                $data['msg'] = $msg;
                $subject = "Verify your account";
                $message = $this->load->view('email_template/verification_email', $data, true);
                sendEmail($from, $to, $subject, $message);
            }

            echo json_encode(TRUE);
        } else {

            echo json_encode(FALSE);
        }
    }

    public function verify() {

        //$this->session->userdata('forgot_email');
        if ($_GET['email']) {
            if ($_GET['mytoken']) {
                $user = $this->Adminmaster_model->selectData('registration', '*', array('reg_email' => $_GET['email'], 'forget_pwd_token' => $_GET['mytoken']));

                //print $this->db->last_query();exit;

                if ($user) {


                    $array['reg_id'] = $user[0]->reg_id;
                    $array['bus_id'] = $user[0]->bus_id;
                    $array['access'] = $user[0]->reg_admin_type;
                    $array['ip_address'] = $this->get_client_ip();




                    $activity_login_id = $this->Adminmaster_model->insertData('login_record', $array);

                    $characters = '0123456789';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < 3; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    $referCode = "Bha" . $randomString;
                    $update = $this->Adminmaster_model->updateData('registration', array('reg_otp_verify' => 'yes', 'status' => 'Active', 'referal_code' => $referCode), array('reg_email' => $_GET['email'], 'forget_pwd_token' => $_GET['mytoken']));

                    $to = $user[0]->reg_email;
                    $data['name'] = ucwords($user[0]->reg_username);
                    //$msg="Change your account password";
                    $from = "Bharat Vaani";
                    //	$data['msg']=$msg;
                    $subject = "Welcome to Bharat Vaani";
                    $message = $this->load->view('email_template/welcome-user-email', $data, true);
                    sendEmail($from, $to, $subject, $message);



                    $session_data = array(
                        'reg_id' => $user[0]->reg_id,
                        'bus_id' => $user[0]->bus_id,
                        'email' => $user[0]->reg_email,
                        'mobile' => $user[0]->reg_mobile,
                        'reg_username' => $user[0]->reg_username,
                        'ei_username' => $user[0]->reg_username,
                        'user_photo' => $user[0]->reg_profile_image,
                        'createdat' => $user[0]->createdat,
                        'reg_istrial' => $user[0]->reg_istrial,
                        'reg_admin_type' => $user[0]->reg_admin_type,
                        'reg_createdby' => $user[0]->reg_createdby,
                        'reg_login_id' => $activity_login_id,
                        'status' => $user[0]->status,
                    );


                    $this->session->set_userdata('user_session', $session_data);

                    redirect('community');
                } else {

                    redirect('index');
                }
            }
        }
    }

    public function insert_blog() {
        $data = $this->input->post();


        $get_blog_id = $this->Adminmaster_model->insertData('blogs', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'blog_image/' . $get_blog_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('blogs', array('filename' => $_FILES['upload']['name'], 'image' => base_url("/public/upload/blog_image/" . $get_blog_id)), array('id' => $get_blog_id));
            }
        }

        exit;
    }

    public function insert_challange() {
        $data = $this->input->post();

        $get_challange_id = $this->Adminmaster_model->insertData('challanges', $data);

        //print $this->db->last_query();exit;

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'challange_image/' . $get_challange_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('challanges', array('challange_image' => $_FILES['upload']['name']), array('id' => $get_challange_id));
            }
        }

        exit;
    }

    public function insert_incubators() {
        //  print_r($_POST);
        //  print_r($_FILES);exit;
        //$data= (array) json_decode(file_get_contents("php://input"));
        $data = $this->input->post();

        // $image['file']=(array)$data['file'];
        // unset($data['file']);
        //   echo '<pre>'.print_r($data,1).'</pre>';

        $get_incube_id = $this->Adminmaster_model->insertData('incubation', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'incubator_image/' . $get_incube_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('incubation', array('image' => $_FILES['upload']['name']), array('id' => $get_incube_id));
            }
        }

        exit;
    }

    public function insert_coworking() {

        $data = $this->input->post();
        $get_cowork_id = $this->Adminmaster_model->insertData('coworking_space', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'co_working_image/' . $get_cowork_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('coworking_space', array('image' => $_FILES['upload']['name']), array('id' => $get_cowork_id));
            }
        }

        exit;
    }

    public function insert_interview() {

        $data = $this->input->post();


        $get_interview_id = $this->Adminmaster_model->insertData('interviews', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_video = $this->Common_model->upload_org_filename('upload', 'interview_video/' . $get_interview_id, $allowd = "mp4|mkv|mov", array('width' => 200, 'height' => 300));
            if ($user_video != false) {

                $get_update_id = $this->Adminmaster_model->updateData('interviews', array('filename' => $_FILES['upload']['name'], 'video' => base_url("/public/upload/interview_video/" . $get_interview_id)), array('id' => $get_interview_id));

                echo true;
            }
        }

        if (isset($_FILES['cover']) && $_FILES['cover']['name'] != '') {
            $_FILES['cover']['name'] = str_replace(" ", "_", $_FILES['cover']['name']);
            $user_cover = $this->Common_model->upload_org_filename('cover', 'interview_video/' . $get_interview_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
            if ($user_cover != false) {

                $get_updated_id = $this->Adminmaster_model->updateData('interviews', array('cover' => base_url("/public/upload/interview_video/" . $get_interview_id . "/thumbs/" . $_FILES['cover']['name'])), array('id' => $get_interview_id));
            }
        }


        exit;
    }

    public function insert_video() {

        $data = $this->input->post();


        $get_video_id = $this->Adminmaster_model->insertData('video', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_video = $this->Common_model->upload_org_filename('upload', 'video/' . $get_video_id, $allowd = "mp4|mkv|mov", array('width' => 200, 'height' => 300));
            if ($user_video != false) {

                $get_update_id = $this->Adminmaster_model->updateData('video', array('filename' => $_FILES['upload']['name'], 'video' => base_url("/public/upload/video/" . $get_video_id)), array('id' => $get_video_id));

                echo true;
            }
        }

        if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['name'] != '') {
            $_FILES['thumbnail']['name'] = str_replace(" ", "_", $_FILES['thumbnail']['name']);
            $user_cover = $this->Common_model->upload_org_filename('thumbnail', 'video/' . $get_video_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
            if ($user_cover != false) {

                $get_updated_id = $this->Adminmaster_model->updateData('video', array('thumbnail' => base_url("/public/upload/video/" . $get_video_id . "/thumbs/" . $_FILES['thumbnail']['name'])), array('id' => $get_video_id));
            }
        }




        exit;
    }

    public function insert_event() {

        $data = $this->input->post();

        $get_event_id = $this->Adminmaster_model->insertData('events', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'event_image/' . $get_event_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('events', array('event_image' => $_FILES['upload']['name']), array('id' => $get_event_id));
            }
        }

        exit;
    }

    public function insert_offer() {

        $data = $this->input->post();

        $get_offer_id = $this->Adminmaster_model->insertData('offers', $data);

        if ($_FILES['upload']['name'] != '') {
            $_FILES['upload']['name'] = str_replace(" ", "_", $_FILES['upload']['name']);
            $user_image = $this->Common_model->upload_org_filename('upload', 'offer_image/' . $get_offer_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
            if ($user_image != false) {

                $get_update_id = $this->Adminmaster_model->updateData('offers', array('offer_image' => $_FILES['upload']['name']), array('id' => $get_offer_id));
            }
        }

        exit;
    }

    public function sitemap() {

        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->load->view('sitemap');
    }

    public function view_interviews($url = "") {
        $data['title'] = "Interviews";
        $data['description'] = "Interviews";
        $data['keywords'] = "Interviews";
        if ($url != "") {
            $data['video'] = $this->Adminmaster_model->selectData('interviews', '*', array('url' => $url));
            $data['interviews'] = $this->Adminmaster_model->selectData('interviews', '*');
            $data['comments'] = $this->Adminmaster_model->selectData('interview_comments', '*', array('interview_id' => $data['video'][0]->id), 'id', 'DESC');
            $data['likes'] = $this->Adminmaster_model->selectData('interview_likes', '*', array('interview_id' => $data['video'][0]->id, 'interview_like' => 1), 'id', 'DESC');
            $ip_address = $this->get_client_ip();
            $insert = array(
                'interview_id' => $data['video'][0]->id,
                'parent_id' => $data['video'][0]->profile_id,
                'ip' => $ip_address,
            );
            $this->Adminmaster_model->insertData('interview_views', $insert);
            $data['views'] = $this->Adminmaster_model->selectData('interview_views', '*', array('interview_id' => $data['video'][0]->id), 'id', 'DESC');
            if (isset($this->session->userdata['user_session']['reg_id'])) {
                $reg_id = $this->session->userdata['user_session']['reg_id'];
                $data['user'] = $this->Adminmaster_model->selectData('registration', '*', array('reg_id' => $reg_id));
                $this->load->view('my-community/Cohorts-header');
                $this->load->view('my-community/view_interviews', $data);
                $this->load->view('template/footer');
            } else {
                $this->load->view('onecohorts/header');
                $this->load->view('my-community/view_interviews', $data);
                $this->load->view('onecohorts/footer');
            }
        } else { }
    }

}

?>