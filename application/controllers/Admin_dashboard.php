<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Admin_dashboard extends Index_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('Admin_dashboard_model');
		$this->load->model('User_model');
		$this->load->library('session');

	}
 
	public function index()
	{
		$session = $this->session->userdata('admin_session');
		//pr($session,1);
		if (isset($session['admin_id'])) {

			redirect(base_url().'admin-dashboard/revenue_list_view');
		}

		$this->load->view('admin_dashboard/dashboard-admin-login');
	}

	public function check_login()
	{
		$post=$this->input->post();
		if($this->session->userdata('admin_session')){
			redirect('admin_dashboard/revenue-list-view');
		}

		if($post){		

		$recaptcha = $_POST['g-recaptcha-response'];
			$res = $this->reCaptcha($recaptcha);
			//if(!$res['success']){
				// Error
				//$this->session->set_flashdata('error',"Something went wrong");
			//}else{
	
			
				$result=$this->Admin_dashboard_model->selectData('admin_profile', '*', array('status'=>'Active','reg_email'=>$post['ad_email'],'reg_password'=>$post['password']));

				if(count($result)>0){
					//$this->session->unset_userdata('user_session');
					$this->session->unset_userdata('admin_session');

						$session_data = array(	
							'admin_id' => $result[0]->id,
							'admin_name' => $result[0]->reg_username,
							'admin_email' => $result[0]->reg_email,	
                           'admin_type' => $result[0]->reg_admin_type,								
							);	
						$this->session->set_userdata('admin_session',$session_data);
						//if(array_key_exists("remeber_me",$post))
						//{
							if(array_key_exists("remeber_me",$post))
		{

			
	$year = time() + 31536000;
							//setcookie('remember_me_u', $post['cp_email'], $year);
							//setcookie('remember_me_pass', $post['password'], $year);
							
							$my_cookie= array(

							   'name'   => 'admin_email',
							   'value'  => $post['ad_email'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								
								//$this->input->set_cookie($my_cookie2);
								
								$my_cookie3= array(

							   'name'   => 'admin_password',
							   'value'  => $post['password'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								//$this->input->set_cookie($my_cookie3);
									$this->input->set_cookie('admin_email', $post['ad_email'], $year);
							$this->input->set_cookie('admin_password', $post['password'], $year);

							
							
		}
							
						//$this->session->set_flashdata('success',"Login Successfully");
                          if($result[0]->reg_admin_type=="Admin"){redirect(base_url().'admin-dashboard/revenue_list_view');}else{
                          	redirect(base_url().'admin-dashboard/subscriber_list');
                          }
						
						
				//}else{
					//$this->session->set_flashdata('error',"Your Information is Incorrect..!!");
					//echo "not Primary Contact";
				//}
			
		}

	}
		
		$this->load->view('admin_dashboard/dashboard-admin-login');

	}

	public function logout()
	{

		//$this->session->sess_destroy();
        $this->session->unset_userdata('admin_session');
		redirect(base_url().'admin-dashboard');
	}

	
	public function revenue_list_view()
	{
		is_login_admin_panel();
		$session = $this->session->userdata('admin_session');
		//print_r($session);
		//echo "Here is Revenue List View";

		$res_country_ids	=	$this->Admin_dashboard_model->Get_unique_country();
		for($i=0;$i<count($res_country_ids);$i++)
		{
			$arrc[$i] = $res_country_ids[$i]['bus_billing_country'];
		}
		$stringc = implode(",",$arrc);

		$res['country'] = $this->Admin_dashboard_model->Get_country($stringc);


		$res_city_ids	=	$this->Admin_dashboard_model->Get_unique_cities();
		for($i=0;$i<count($res_city_ids);$i++)
		{
			$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
		}
		$string = implode(",",$arr);

		$res['cities'] = $this->Admin_dashboard_model->Get_cities($string);

		$res['promo_data'] = $this->Admin_dashboard_model->selectData('coupon_promo', '*','','promo_id','ASC');
		// print_r($res['promo_data']);
		// exit;
		$this->load->view('admin_dashboard/revenue-list-view',$res);
	}

	public function add_new_offer() {

		$this->load->model('Community_model');

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];

		$data['company'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);

		$data['offers'] = $this->Community_model->selectData('offers', '*', array("bus_id" => $bus_id), 'id','DESC');

		if(count($data['offers']) > 0) {
			$last_offer_no = $data['offers'][0]->offer_number;
			$temp = str_replace("OF", "", $last_offer_no);
			$temp = intval($temp) + 1;
			$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
		} else {
			$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Community / Offers / Add Offers';
		$tracking_info['action_taken'] = 'Add';
		$tracking_info['reference'] = "Offers";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['alertNo']='OF'.$alno_ref;
		//print_r($data['alertNo']); exit();

		$this->load->view('admin_dashboard/add-new-offer', $data);

	}

	public function get_rev_details()
	{
		is_login_admin_panel();
		$post = $this->input->post();
		

		$field_pos=array("createdat"=>'0',"subscription_id"=>'1',"storage"=>'2',"payment_mode"=>'3',"validity"=>'4',"status"=>'5',"amount"=>'6');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Admin_dashboard_model->revenueFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Admin_dashboard_model->revenueFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

			
	  		$check='<input type="checkbox" id="filled-in-box'.$value['subscription_id'].'" name="filled-in-box'.$value['subscription_id'].'" class="filled-in purple revenue_bulk_action" value="'.$value['subscription_id'].'"/><label for="filled-in-box'.$value['subscription_id'].'"></label>';

	  		$purchase_date =  date("d-m-Y", strtotime($value['createdat']));

	  		if($value['promocode']=='FIRST20')
	  		{
	  			$discount_amount = $value['amount'];

	  		}else{
	  			$discount_amount = 0;
	  		}
			$cust_name=wordwrap($value['bus_company_name'],20,"<br>\n");
			if($cust_name==""){
             $cust_name="Yet to setup business";
			}

			$net_purchase_amt = $value['amount'] - $discount_amount;	

			$bus_info_xebra=$this->Admin_dashboard_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));
				$reg_id = $bus_info_xebra[0]->reg_id;
				$bus_id = $bus_info_xebra[0]->bus_id;

			$credit_note=$this->Admin_dashboard_model->selectData("sales_credit_debit","*",array('cd_invoice_no' =>$value['invoice_no'],'bus_id'=>$bus_id,'cust_id'=>$value['client_id']));	
			$cr_amt=0;
			foreach($credit_note as $value1){
				$cr_amt+=$value1->cd_grant_total;
			}	

						
	  		$records["data"][] = array(
				$check,
				$value['invoice_no'].'</br>'.$purchase_date,
				$cust_name,
	  			$value['name'].'</br>'.$value['country_name'],
	  			$value['nature_of_bus'],
	  			$value['subscription_type'],
	  			$value['subscription_plan'],
	  			$value['promocode'],
	  			round($value['amount']),
	  			round($discount_amount),
				round($cr_amt),
	  			round($net_purchase_amt-$discount_amount-$cr_amt),
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;		
	}


	public function download_multiple_rev(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->revenueDownloadFilter($array);
		// echo '<pre>';
		// print_r($data['result']);
		// echo '</pre>';
		// exit;
		$this->load->view('admin_dashboard/download_rev',$data);

	}

	public function download_multiple_rev_invoice(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$result = $this->Admin_dashboard_model->revenueDownloadFilter($array);
		// echo '<pre>';
		// print_r($data['result']);
		// echo '</pre>';
		// exit;

		for($i=0;$i<count($result);$i++)
		{

			$id=$result[$i]['subscription_id'];
			$reg_id=$result[$i]['reg_id'];
			$bus_id=$result[$i]['bus_id'];
			$gst_id=0;
			// $data['subscription'] = $this->Subscription_model->GetSubscritionData($id,$bus_id);
			$data['subscription'] = $this->Admin_dashboard_model->selectData('subscription', '*',array('subscription_id'=>$id,'bus_id'=>$bus_id));
			$data['bus_details'] = $this->Admin_dashboard_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
			$data['gst_num'] = $this->Admin_dashboard_model->selectData('gst_number', 'gst_no',array('bus_id'=>$bus_id,'gst_id'=>$gst_id));
			// $data['city'] = $this->Admin_dashboard_model->selectData('cities', '*',array('city_id'=>$data['bus_details'][0]->bus_billing_city));
			$data['country'] = $this->Admin_dashboard_model->selectData('countries', '*',array('country_id'=>$data['subscription'][0]->billing_country));
			$data['states'] = $this->Admin_dashboard_model->selectData('states', '*',array('state_id'=>$data['subscription'][0]->billing_state));
			$data['reg_details'] = $this->Admin_dashboard_model->selectData('registration', 'reg_mobile',array('reg_id'=>$reg_id));
			
			//subscription/export_subscription
			//$this->load->view('subscription/download_subscription',$data);
			$html_data=$this->load->view('subscription/new_download_subscription',$data,true);
			$inv_name=str_replace('/','-',$data['subscription'][0]->invoice_no);
           
			$this->pdf->create($html_data,$inv_name,'download_multi');

			$path=DOC_ROOT.'/public/pdfmail/'.$inv_name.'.pdf';
			$this->zip->read_file($path); 
			
			 $count++;
			 unlink($path);

		}

		$cdn = "Revenue".time().".zip";
         //$this->zip->archive(DOC_ROOT.'/public/pdfmail/'.$inv);
        $this->zip->download($cdn);
		//$this->load->view('admin_dashboard/download_rev',$data);

	}







	public function subscriber_list()
	{
		is_login_admin_panel();
		$res_country_ids	=	$this->Admin_dashboard_model->Get_unique_country();
		for($i=0;$i<count($res_country_ids);$i++)
		{
			$arrc[$i] = $res_country_ids[$i]['bus_billing_country'];
		}
		$stringc = implode(",",$arrc);

		$res['country'] = $this->Admin_dashboard_model->Get_country($stringc);


		$res_city_ids	=	$this->Admin_dashboard_model->Get_unique_cities();
		for($i=0;$i<count($res_city_ids);$i++)
		{
			$arr[$i] = $res_city_ids[$i]['bus_billing_city']; 
		}
		$string = implode(",",$arr);

		$res['cities'] = $this->Admin_dashboard_model->Get_cities($string);
		$this->load->view('admin_dashboard/subscriber-list-view',$res);
	}


	public function get_subscriber_details()
	{
		is_login_admin_panel();
		$post = $this->input->post();

		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"reg_username"=>'2',"reg_mobile"=>'3',"reg_istrial"=>'4',"status"=>'5',"reg_designation"=>'6',"amount"=>'7');	

		$sort_field=array_search(@$post['order'][0]['column'],$field_pos);

		if(@$post['order'][0]['dir']=='asc')
		{
	 		$orderBy="DESC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	


		$TotalRecord=$this->Admin_dashboard_model->SubscriptionFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Admin_dashboard_model->SubscriptionFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	// echo '<pre>';
	 	// print_r($userData);
	 	// echo '</pre>';
	 	// exit;
	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['reg_id'].'" name="filled-in-box'.$value['reg_id'].'" class="filled-in purple subscriber_bulk_action" value="'.$value['reg_id'].'"/><label for="filled-in-box'.$value['reg_id'].'"></label>';

	  		$created_at =  date("d-m-Y", strtotime($value['createdat']));

	  		if($value['reg_istrial']==0)
	  		{
	  			$reg_istrial = 'NO';
	  			

	  		}else{
	  			$reg_istrial = 'YES';
	  			$sub_status = 'NO';
	  		}



	  		if($value['promocode']!="" && $value['referalcode']!="")
	  		{
	  			$value['promocode'] = 'Promo &&</br> Referal';
	  			

	  		}else if($value['promocode']!="" && $value['referalcode']=="")
	  		{
	  			$value['promocode'] = 'Promo ';
	  			

	  		}else if($value['promocode']=="" && $value['referalcode']!="")
	  		{
	  			$value['promocode'] = 'Referal ';
	  			

	  		}else{
	  			$value['promocode'] = 'No any </br>code used ';
	  		}

	  		$sub_status = $value['sub_status'];
	  		if($sub_status==""){
	  			$sub_status = "Not </br>Subscribed";
	  		}

	  		if($value['amount']!=""){
	  			$value['amount']=$value['amount'];
	  		}else{
	  			$value['amount']=0;
	  		}
			
		$date1=date_create(date("Y-m-d", strtotime($value['createdat'])));
		$date2=date_create(date("Y-m-d"));
		$diff=date_diff($date1,$date2);
		$days = $diff->format("%a");


		$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$cls='deactive_record';
				$cls1='deactivate';

				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>	'";

				$status='<li class="activation"><a href="#" class="active_subscriber_profile" id="active_subscriber_profile"  data-profileid='.$value["reg_id"].'><i class="dropdwon-icon icon activate"></i>Re-Activate</a></li>';
				
			}else{

				$cls='active_record';
				$cls1='activate';
				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>'";
				$status='<li class="deactivation"><a href="#" class="deactive_subscriber_profile" id="deactive_subscriber_profile" data-profileid="'.$value["reg_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				
			}
			
				
			
			$action  .= $status;
			
			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

			 if(@$post['search_by_days'])
			 {
			 	if( $days <= 21 && $days >= 15 && $post['search_by_days'] == 15)
			 	{
			 		$records["data"][] = array(
					$check,
					'REG'.$value['reg_id'].'</br>'.$created_at,
		  			$value['reg_username'],
		  			$value['reg_email'],
		  			$value['reg_mobile'],
		  			$value['name'].'</br>'.$value['country_name'],
		  			'',
		  			$reg_istrial,
		  			$days,
		  			$sub_status,
		  			$value['promocode'],
		  			$value['amount'],
		  			$action,
		  			);


			 	}elseif ($days <= 30 && $days >= 22 && $post['search_by_days'] == 22) {

			 		$records["data"][] = array(
					$check,
					'REG'.$value['reg_id'].'</br>'.$created_at,
		  			$value['reg_username'],
		  			$value['reg_email'],
		  			$value['reg_mobile'],
		  			$value['name'].'</br>'.$value['country_name'],
		  			'',
		  			$reg_istrial,
		  			$days,
		  			$sub_status,
		  			$value['promocode'],
		  			$value['amount'],
		  			$action,
		  			);
			 		
			 	}elseif ($days >= 31 && $post['search_by_days'] == 31) {

			 		$records["data"][] = array(
					$check,
					'REG'.$value['reg_id'].'</br>'.$created_at,
		  			$value['reg_username'],
		  			$value['reg_email'],
		  			$value['reg_mobile'],
		  			$value['name'].'</br>'.$value['country_name'],
		  			$reg_istrial,
		  			$days,
		  			$sub_status,
		  			$value['promocode'],
		  			$value['amount'],
		  			$action,
		  			);
			 		
			 	}

			 }else{

			 	$records["data"][] = array(
					$check,
					'REG'.$value['reg_id'].'</br>'.$created_at,
		  			$value['reg_username'],
		  			$value['reg_email'],
		  			$value['reg_mobile'],
		  			$value['name'].'</br>'.wordwrap($value['country_name'],10,"<br>\n"),
		  			$reg_istrial,
		  			$days,
		  			$sub_status,
		  			$value['promocode'],
		  			$value['amount'],
		  			$action,
		  			);

			 }
							
	  		

	  	}

	  	 //  print_r($value['amount']);
			  // exit();

		
		//$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($records["data"]);
		$records["recordsFiltered"] = count($records["data"]);	
		echo json_encode($records);				  
		exit;		
	}


	public function email_subscriber(){
		// print_r($_POST);
		// exit();
		$data = $_POST;
		$id = $data['subscriber_id'];
		$ids = explode(',',$data['subscriber_id']);

		$data['result'] = $this->Admin_dashboard_model->Subscriber_Data_download($ids);
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$htmldata = $this->load->view('admin_dashboard/print_subscriber',$data,true);
		// $invoname ="Service";
		// $Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');

		$invoname=str_replace('/','-','Subscriber')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$all_emails =  $data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $data['email_message'];
		
		

		$subject = $data['email_subject'];

		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc); 
		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}
		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}

	}


	public function download_multiple_subscriber(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->Subscriber_Data_download($array);
		// echo '<pre>';
		// print_r($data['subscriber']);
		// echo '</pre>';
		// exit;
		$this->load->view('admin_dashboard/download_subscriber',$data);

	}

	public function deactive_reg_user()
	{	
		$reg_ids = $this->input->post('reg_ids');	
		$reg_ids_array = explode(",",$reg_ids);
		foreach($reg_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Inactive"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('registration',$deactive,array('reg_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}


	public function active_reg_user()
	{	
		$reg_ids = $this->input->post('reg_ids');	
		$reg_ids_array = explode(",",$reg_ids);
		foreach($reg_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Active"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('registration',$deactive,array('reg_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}


	public function get_services(){

	}





	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------


	//                            coupon generator
	

	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------





	public function coupon_generator(){
		 $post = $this->input->post();


		$field_pos=array("createdat"=>'0',"promo_id"=>'1',"coupon_code"=>'2',"nature_of_code"=>'3',"description"=>'4',"status"=>'5',"validity"=>'6');	
		$data['TotalRecord']=$this->Admin_dashboard_model->couponFilter($post,$field_pos,"",1);
		$this->load->view('admin_dashboard/coupon-list-view',$data);
		
	}

	public function add_coupon_code()
	{
		
		$post=$this->input->post();		
		if($post){
		
			
			$post['code_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_date'])));
			$post['start_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_start_date'])));
			$post['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_end_date'])));
			$post['createdat'] = date("Y-m-d H:i:s");
			$post['updatedat'] = date("Y-m-d H:i:s");
			$post['discount']=$post['coupon_discount'];
			unset($post['coupon_discount']);
			
			$post['target_group']=$post['coupon_tg'];
			unset($post['coupon_tg']);
			unset($post['coupon_date']);
			unset($post['coupon_start_date']);
			unset($post['coupon_end_date']);
			$post['validity']=$post['coupon_validity'];
			unset($post['coupon_validity']);
			$post['description']=$post['coupon_desc'];
			unset($post['coupon_desc']);
			$post['referrel']=$post['referal_name'];
			unset($post['referal_name']);			
			$post['nature_of_code']=$post['coupon_nature'];
			unset($post['coupon_nature']);
			
		
		

			$copn_id=$this->Admin_dashboard_model->insertData('coupon_promo',$post);
			
			$this->session->set_flashdata('success',"Coupon code has been created successfully");
			redirect('admin_dashboard/coupon_list_view',$data);
		}
		$data=array();
		$this->load->view('admin_dashboard/add-coupon-code',$data);
	}


	public function edit_coupon_code()
	{
		
		
		$post=$this->input->post();		
		if($post){
			$id = $post['id'];
			$post['code_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_date'])));
			$post['start_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_start_date'])));
			$post['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_end_date'])));
			$post['createdat'] = date("Y-m-d H:i:s");
			$post['updatedat'] = date("Y-m-d H:i:s");
			$post['discount']=$post['coupon_discount'];
			unset($post['coupon_discount']);
			
			$post['target_group']=$post['coupon_tg'];
			unset($post['id']);
			unset($post['coupon_tg']);
			unset($post['coupon_date']);
			unset($post['coupon_start_date']);
			unset($post['coupon_end_date']);
			$post['validity']=$post['coupon_validity'];
			unset($post['coupon_validity']);
			$post['description']=$post['coupon_desc'];
			unset($post['coupon_desc']);
			$post['referrel']=$post['referal_name'];
			unset($post['referal_name']);			
			$post['nature_of_code']=$post['coupon_nature'];
			unset($post['coupon_nature']);
			
			
			
			$copn_id=$this->Admin_dashboard_model->updateData('coupon_promo',$post,array("promo_id"=>$id));

			
		
			$this->session->set_flashdata('success',"Your Coupon code has been saved successfully");
			redirect('admin_dashboard/coupon_generator');
		}

		redirect('admin_dashboard/coupon_generator');
	}





	public function coupon_list_view(){
  $post = $this->input->post();


		$field_pos=array("createdat"=>'0',"promo_id"=>'1',"coupon_code"=>'2',"nature_of_code"=>'3',"description"=>'4',"status"=>'5',"validity"=>'6');	
		$data['TotalRecord']=$this->Admin_dashboard_model->couponFilter($post,$field_pos,"",1);
		$this->load->view('admin_dashboard/coupon-list-view',$data);
		
	}
	
	public function edit_coupon($id){

		$data['result']=$this->Admin_dashboard_model->selectData('coupon_promo', '*', array('promo_id'=>$id));
		//$data['TotalRecord']=$this->Admin_dashboard_model->couponFilter($post,$sort_field,$orderBy,0);
		$this->load->view('admin_dashboard/edit-coupon-code',$data);
		
	}
	
	public function get_cou_details(){
		
		print_r($_POST);
		exit;
		
	}
	
	public function get_coupon_details(){
		

		is_login_admin_panel();
		$post = $this->input->post();


		$field_pos=array("createdat"=>'0',"promo_id"=>'1',"coupon_code"=>'2',"nature_of_code"=>'3',"description"=>'4',"status"=>'5',"validity"=>'6');	

		$sort_field=array_search(@$post['order'][0]['column'],$field_pos);

		if(@$post['order'][0]['dir']=='asc')
		{
	 		$orderBy="DESC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Admin_dashboard_model->couponFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Admin_dashboard_model->couponFilter($post,$sort_field,$orderBy,1);
  //        print_r($userData);
		// exit();
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	// echo '<pre>';
	 	// print_r($userData);
	 	// echo '</pre>';
	 	// exit;
	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['promo_id'].'" name="filled-in-box'.$value['promo_id'].'" class="filled-in purple coupon_bulk_action" value="'.$value['promo_id'].'"/><label for="filled-in-box'.$value['promo_id'].'"></label>';

	  		$code_date =  date("d-m-Y", strtotime($value['code_date']));

	  		$start_date =  date("d-m-Y", strtotime($value['start_date']));
	  		$end_date =  date("d-m-Y", strtotime($value['end_date']));

	  		if($value['nature_of_code']==1)
	  		{
	  			$nature_of_code = 'Coupon Code';

	  		}else{

	  			$nature_of_code = 'Referal Code';
	  		}

	  		$code_and_date = $value['coupon_code'].'<br>'.$code_date;
			
		$date1=date_create(date("Y-m-d", strtotime($value['createdat'])));
		$date2=date_create(date("Y-m-d"));
		$diff=date_diff($date1,$date2);
		$days = $diff->format("%a");


		$action = '';$status='';$cls='';$cls1='';
			if($value['status']=="Active"){
				$cls='active_record';
				$cls1='activate';
				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["promo_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["promo_id"]."' class='dropdown-content'>'";

			 	$li="<li><a href=".base_url()."admin_dashboard/edit_coupon/".$value["promo_id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>';

				$action .= $li;

				$status='<li class="deactivation"><a href="#" class="deactive_coupon" id="deactive_coupon" data-profileid="'.$value["promo_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				
			}else{
				
				$cls='deactive_record';
				$cls1='deactivate';

				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["promo_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["promo_id"]."' class='dropdown-content'>	'";

				$status='<li class="activation"><a href="#" class="active_coupon" id="active_coupon"  data-profileid='.$value["promo_id"].'><i class="dropdwon-icon icon activate"></i>Re-Activate</a></li>';
				
			}
			
			
			
			$action  .= $status;
			
			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';
			


			 	$records["data"][] = array(
					$check,
					$code_and_date,
					$nature_of_code,
		  			$value['referrel'],
		  			$value['description'],
		  			$value['target_group'],
		  			$value['validity'],
		  			$start_date,
		  			$end_date,
		  			$value['discount'],
		  			$value["status"],
		  			$action,
		  			);	
	  		
	  	}

		
		//$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($records["data"]);
		$records["recordsFiltered"] = count($records["data"]);	
		echo json_encode($records);				  
		exit;		
		
		
	}
	

	public function email_coupon(){
		// print_r($_POST);
		// exit();
		$data = $_POST;
		$id = $data['coupon_id'];
		$ids = explode(',',$data['coupon_id']);

		$data['result'] = $this->Admin_dashboard_model->couponDataDownload($ids);
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$htmldata = $this->load->view('admin_dashboard/print_coupon',$data,true);
		// $invoname ="Service";
		// $Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');

		$invoname=str_replace('/','-','Coupon')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$all_emails =  $data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $data['email_message'];
		
		

		$subject = $data['email_subject'];

		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc); 
		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}
		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}

	}


	public function deactive_coupon()
	{	
		$coupon_ids = $this->input->post('coupon_ids');	
		$coupon_ids_array = explode(",",$coupon_ids);
		foreach($coupon_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Inactive"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('coupon_promo',$deactive,array('promo_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}


	public function active_coupon()
	{	
		$coupon_ids = $this->input->post('coupon_ids');	
		$coupon_ids_array = explode(",",$coupon_ids);
		foreach($coupon_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Active"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('coupon_promo',$deactive,array('promo_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}


	public function download_multiple_coupon(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->couponDataDownload($array);
		// echo '<pre>';
		// print_r($data['result']);
		// echo '</pre>';
		// exit;
		$this->load->view('admin_dashboard/download_coupon',$data);

	}




	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------


	//                            Profile


	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------

	public function add_profile_list(){

		$data['data'] = $this->Admin_dashboard_model->get_itemCRUD();
		$this->load->view('admin_dashboard/admin-portal-profile-list' , $data);
	}
   

	public function add_profile(){
		$this->load->view('admin_dashboard/admin-portal-add-profile');
	}

   /**
    * Store Data from this method.
    *
    * @return Response
   */
   public function store()
   {
        $this->form_validation->set_rules('reg_username', 'First & Last Name', 'required');
        $this->form_validation->set_rules('reg_email', 'Email ID', 'required');



        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());

            redirect(base_url('admin_dashboard/add_profile'));

        }else{
        	$exist_email = $this->Admin_dashboard_model->isUnique('admin_profile','reg_email',$this->input->post('reg_email'));
        	if($exist_email==TRUE)
			{
				 $image_info = array();
				$image_info = $_FILES;
				
           $id=$this->Admin_dashboard_model->insert_item();



				if(isset($image_info['reg_profile_image']) && $image_info['reg_profile_image']['name']!= "") {

					if($_FILES['reg_profile_image']['name'] != NULL) {

						$files = $_FILES['reg_profile_image'];

						$_FILES['reg_profile_image']['name']      = $files['name'];
						$_FILES['reg_profile_image']['type']      = $files['type'];
						$_FILES['reg_profile_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['co_working_image']['error']     = $files['error'];
						$_FILES['reg_profile_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('reg_profile_image','admin_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
						$this->Admin_dashboard_model->updateData('admin_profile',array('reg_profile_image'=>$files['name']),array('id'=>$id));
					}
				}


           redirect(base_url('admin_dashboard/add_profile_list'));
       }else{
               $this->session->set_flashdata('error',"Seems like this email address is already registered");
				redirect('admin_dashboard/add_profile');	
       }
        }
    }

    
     public function update($id)
   {
        $this->form_validation->set_rules('reg_username', 'First & Last Name', 'required');
        $this->form_validation->set_rules('reg_email', 'Email ID', 'required');

        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('admin_dashboard/edit_admin_profile/'.$id));
        }else{ 
          $this->Admin_dashboard_model->update_item($id);

           $image_info = array();
				$image_info = $_FILES;

            if(isset($image_info['reg_profile_image']) && $image_info['reg_profile_image']['name']!= "") {

					if($_FILES['reg_profile_image']['name'] != NULL) {

						$files = $_FILES['reg_profile_image'];

						$_FILES['reg_profile_image']['name']      = $files['name'];
						$_FILES['reg_profile_image']['type']      = $files['type'];
						$_FILES['reg_profile_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['co_working_image']['error']     = $files['error'];
						$_FILES['reg_profile_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('reg_profile_image','admin_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
						$this->Admin_dashboard_model->updateData('admin_profile',array('reg_profile_image'=>$files['name']),array('id'=>$id));
					}
				}


          redirect(base_url('admin_dashboard/add_profile_list'));
        }
   }

    public function edit_admin_profile($id)
	{
		$data['data']=$this->Admin_dashboard_model->find_item($id);
 
		$this->load->view('admin_dashboard/admin-portal-edit-profile',$data);

	}
   
   public function delete($id)
   {
  $status= $this->input->get('status');
    $profileInactive=$this->Admin_dashboard_model->selectData('admin_profile', '*', array('status'=>'Inactive'));
    $profile=$this->Admin_dashboard_model->selectData('admin_profile', '*');

    
    if(count($profile)-1==count($profileInactive)){
         $this->session->set_flashdata('error',"There should be at least 1 active admin profile");
       redirect(base_url('admin_dashboard/add_profile_list'));
    }else{
    	 $item = $this->Admin_dashboard_model->delete_item($id,$status);
       redirect(base_url('admin_dashboard/add_profile_list'));
    }

      
   }


	public function change_password()
	{
		
		$this->load->view('admin_dashboard/admin-change-password');
		
	}
	
	public function feedback_list(){
		
		$this->load->view('admin_dashboard/feedback_list/feedback-list');
		
	}
		
	public function get_feedback_details(){

        // Activity Tracker start
			 $tracking_info['module_name'] = 'Admin/ Feedback list';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Feedback list";
			// $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['gst_id']=$this->user_session['ei_gst'];
		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"feedback"=>'2');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Admin_dashboard_model->feedbackFilter($post,$sort_field,$orderBy,0,1);	
	 	$userData = $this->Admin_dashboard_model->feedbackFilter($post,$sort_field,$orderBy,1,1);


	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {
             
             
	  		$check='<input type="checkbox" id="filled-in-box'.$value['feedback_id'].'" name="filled-in-box'.$value['feedback_id'].'" class="filled-in purple feedb_bulk_action" value="'.$value['feedback_id'].'"/> <label for="filled-in-box'.$value['feedback_id'].'"></label>';
        $date=date('d-m-Y',strtotime($value['createdat']));

        $feedback=wordwrap($value['feedback'],40,"<br>");

         $user=$this->Admin_dashboard_model->selectData('registration', '*', array('reg_id'=>$value['reg_id']));
		
		// print_r($user);

		// exit();
         if(count($user)>0){
         	$username=$user[0]->reg_username;
         }else{
         	$username="-";
         }
		
		$act_taken='<input type="text" id="act-taken'.$value['feedback_id'].'" name="act-taken'.$value['feedback_id'].'" class="action-taken" value="'.$value['act_taken'].'"/>';
		$by_whom='<input type="text" id="by-whom'.$value['feedback_id'].'" name="by-whom'.$value['feedback_id'].'" class="by_whom" value="'.$value['by_whom'].'"/>';
		$act_date=($value['act_date'] != '' && $value['act_date'] != '0000-00-00')?str_replace("/"," ",date("d-m-Y", strtotime($value['act_date']))):'';
		$button='<a class="edit-info"  id="edit-info'.$value['feedback_id'].'" onclick="change_button('.$value['feedback_id'].');"><i class="material-icons" style="color: #000; font-size: 20px;">edit</i></a><a href="javascript:void(0)" class="save-info" id="save-info'.$value['feedback_id'].'" onclick="save_button('.$value['feedback_id'].');" hidden><i class="material-icons" style="color: #000; font-size: 20px;">save</i>';
		
		$records["data"][] = array(
				$check,
				$date,
	  			$username,
	  			$feedback,
	  			$act_taken,
	  			$by_whom,
	  			$act_date,
	  			$button,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
		
	}

	public function edit_feedback(){

   $post = $this->input->post();
   $data = array(
						'act_taken'	=> $post['act_taken'],
						'by_whom'	=> $post['by_whom'],
						'act_date'	=> date('Y-m-d'),

						);

			$update_id = $this->Admin_dashboard_model->updateData('feedback',$data,array('feedback_id'=>$post['id']));

			$data['success']=$update_id;

			echo json_encode($data);exit;
	}


	public function download_multiple_feedback(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->feedbackDataDownload($array);
		// echo '<pre>';
		// print_r($data['result']);
		// echo '</pre>';
		// exit;
		$this->load->view('admin_dashboard/feedback_list/download_feedback',$data);

	}
	
	public function referearn_list(){
		
		$data['countries']=$this->Admin_dashboard_model->selectData('countries', '*','','country_id','ASC');
		$this->load->view('admin_dashboard/refer_earn_list/refer_earn_list',$data);
		
	}
	
	public function get_refearn_details(){  
		
	
		
	 // Activity Tracker start
			 $tracking_info['module_name'] = 'Admin/ Referal list';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Referal list";
			// $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		
	//	$post['reg_id']=$this->user_session['reg_id'];
	//	$post['bus_id']=$this->user_session['bus_id'];
		//$post['gst_id']=$this->user_session['ei_gst'];
		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"feedback"=>'2');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="DESC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Admin_dashboard_model->referFilter($post,$sort_field,$orderBy,0,1);	
	 	$userData = $this->Admin_dashboard_model->referFilter($post,$sort_field,$orderBy,1,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {
            
			$cls='active_record';
			$cls1='activate';
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["subscription_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["subscription_id"]."' class='dropdown-content'>'";

			 	$li="<li><a class='refer_payment' id='refer_payment' href='javascript:void(0);'  data-refer_id=".$value["subscription_id"].'><img src="'.base_url().'public/icons/payment_rcv.png" style="width:21px; margin:0 24px 0 1px;"><p style="margin:-26px 24px 0 38%;">PAYMENT</p></a></li>';

				$action .= $li; 
             	
	  		$check='<input type="checkbox" id="filled-in-box'.$value['subscription_id'].'" name="filled-in-box'.$value['subscription_id'].'" class="filled-in purple refearn_bulk_action" value="'.$value['subscription_id'].'"/> <label for="filled-in-box'.$value['subscription_id'].'"></label>';
        $date=date('d-m-Y',strtotime($value['createdat']));
        $refcode=$value['referalcode'];
        $refname=$value['name'];
        $subtype=$value['subscription_type'];

         
		
		$records["data"][] = array(
				$check,
				$date,
	  			$refcode,
	  			$refname,
	  			$subtype,
	  			"10000",
	  			"Unpaid",
				$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;	
	}
	
	public function demo(){
		
		$this->load->view('admin_dashboard/demo');
		
	}
	
	public function create_coworking_space(){


		 $post=$this->input->post();
		 if($post)
		 {

              if(isset($post['co_name'])){

              	$post['name']=$post['co_name'];
              	unset($post['co_name']);
              }

              if(isset($post['co_address'])){

              	$post['address']=$post['co_address'];
              	unset($post['co_address']);
              }

               if(isset($post['co_country'])){

              	$post['country']=$post['co_country'];
              	unset($post['co_country']);
              }

              if(isset($post['co_state'])){

              	$post['state']=$post['co_state'];
              	unset($post['co_state']);
              }

              if(isset($post['co_city'])){

              	$post['city']=$post['co_city'];
              	unset($post['co_city']);
              }

               if(isset($post['co_emailid'])){

              	$post['email_id']=$post['co_emailid'];
              	unset($post['co_emailid']);
              }

                if(isset($post['co_contact'])){

              	$post['contact_no']=$post['co_contact'];
              	unset($post['co_contact']);
              }

              if(isset($post['submit'])){

              
              	unset($post['submit']);
              }
               unset($post['image_info']);
               if(isset($_FILES['co_working_image']) && $_FILES['co_working_image']['name']!= "") {

				if($_FILES['co_working_image']['name'] != NULL) {
					$post['image'] = $_FILES['co_working_image']['name'];
				}
			}

              $insert_working_space=$this->Admin_dashboard_model->insertData("coworking_space",$post);


              if($insert_working_space != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['co_working_image']) && $image_info['co_working_image']['name']!= "") {

					if($_FILES['co_working_image']['name'] != NULL) {

						$files = $_FILES['co_working_image'];

						$_FILES['co_working_image']['name']      = $files['name'];
						$_FILES['co_working_image']['type']      = $files['type'];
						$_FILES['co_working_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['co_working_image']['error']     = $files['error'];
						$_FILES['co_working_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('co_working_image','co_working_image/'.$insert_working_space, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}

                 	////////////////////////////////////Bvcohorts///////////////////////
						// API URL
						$post['file']=new CurlFile($_FILES['co_working_image']['tmp_name'], $_FILES['co_working_image']['type'], $_FILES['co_working_image']['name']);
						$url = 'https://xebra.in/index/insert_coworking';

						// Create a new cURL resource
						$ch = curl_init($url);

						// Setup request to send json via POST
						$data = $post;//array(
						//'username' => 'codexworld',
						//'password' => '123456'
						//);
						$payload = json_encode($data);

						//print_r($payload);

						// Attach encoded JSON string to the POST fields
						curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

						// Set the content type to application/json
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

						// Return response instead of outputting
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// Execute the POST request
						$result = curl_exec($ch);

						// Close cURL resource
						curl_close($ch);
						////////////////////////////////////Bvcohorts///////////////////////
						////////////////////////////////////Onecohorts///////////////////////

						$url = 'https://onecohort.in//index/insert_coworking';

						// Create a new cURL resource
						$ch = curl_init($url);

						// Setup request to send json via POST
						$data = $post;//array(
						//'username' => 'codexworld',
						//'password' => '123456'
						//);
						$payload = json_encode($data);



						// Attach encoded JSON string to the POST fields
						curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

						// Set the content type to application/json
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

						// Return response instead of outputting
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// Execute the POST request
						$result = curl_exec($ch);

						// Close cURL resource
						curl_close($ch);





						//////////////////////////////////////////////////////////////////////////
				$this->session->set_flashdata('success',"Your Data has been added successfully");
				
				redirect(base_url('admin_dashboard/coworking_list'));

			}


		 }
		$data['countries']	=	$this->Admin_dashboard_model->selectData('countries', '*','','country_id','ASC');
		$this->load->view('admin_dashboard/coworking_space',$data);
		
	}


	public function edit_coworking_space($id){


		 $post=$this->input->post();
		 if($post)
		 {

              if(isset($post['co_name'])){

              	$post['name']=$post['co_name'];
              	unset($post['co_name']);
              }

              if(isset($post['co_address'])){

              	$post['address']=$post['co_address'];
              	unset($post['co_address']);
              }

               if(isset($post['co_country'])){

              	$post['country']=$post['co_country'];
              	unset($post['co_country']);
              }

              if(isset($post['co_state'])){

              	$post['state']=$post['co_state'];
              	unset($post['co_state']);
              }

              if(isset($post['co_city'])){

              	$post['city']=$post['co_city'];
              	unset($post['co_city']);
              }

               if(isset($post['co_emailid'])){

              	$post['email_id']=$post['co_emailid'];
              	unset($post['co_emailid']);
              }

                if(isset($post['co_contact'])){

              	$post['contact_no']=$post['co_contact'];
              	unset($post['co_contact']);
              }

              if(isset($post['submit'])){

              
              	unset($post['submit']);
              }
               unset($post['image_info']);
               if(isset($_FILES['co_working_image']) && $_FILES['co_working_image']['name']!= "") {

				if($_FILES['co_working_image']['name'] != NULL) {
					$post['image'] = $_FILES['co_working_image']['name'];
				}
			}

              $insert_working_space=$this->Admin_dashboard_model->updateData("coworking_space",$post,array('id'=>$id));


              if($insert_working_space != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['co_working_image']) && $image_info['co_working_image']['name']!= "") {

					if($_FILES['co_working_image']['name'] != NULL) {

						$files = $_FILES['co_working_image'];

						$_FILES['co_working_image']['name']      = $files['name'];
						$_FILES['co_working_image']['type']      = $files['type'];
						$_FILES['co_working_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['co_working_image']['error']     = $files['error'];
						$_FILES['co_working_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('co_working_image','co_working_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}


				$this->session->set_flashdata('success',"Your Data has been edited successfully");
				redirect(base_url('admin_dashboard/coworking_list'));

			}


		 }

		$data['working_space']=$this->Admin_dashboard_model->selectData("coworking_space","*",array('id'=>$id));
		$data['countries']	=	$this->Admin_dashboard_model->selectData('countries', '*','','country_id','ASC');
		//print_r($data);exit();
		$this->load->view('admin_dashboard/edit_coworking_space',$data);
		
	}
	
	public function coworking_list(){
		$country=$this->Admin_dashboard_model->selectData('coworking_space',"*");
	
		$this->load->view('admin_dashboard/coworking_list');
		
	}
	public function get_coworking_details() {
         $post['length']=-1;
          

         $records = array();
         $records["data"] = array();

          $post=$this->input->post();
		 $working_space= $this->Admin_dashboard_model->getWorkspace($post);
		 foreach ($working_space as $key => $value) {
		if($value['image'] != '') {
			$image = '<img src="'.base_url().'public/upload/co_working_image/'.$value['id'].'/'.$value['image'].'" height="130px" class="logo_style_2" width="130px">';
		} else {
			$image = '<p style="text-align:center;">No Image</p>';				
		}


		$country=$this->Admin_dashboard_model->selectData('countries',"*",array('country_id'=>$value['country']));

		$cities=$this->Admin_dashboard_model->selectData('cities',"*",array('city_id'=>$value['city']));

		if(count($country)>0){
    $ctry=$country[0]->country_name;
		}else{
           $ctry="";
		}


		if(count($cities)>0){
    $city=$cities[0]->name;
		}else{
           $city="";
		}
		 
		 
		$middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'.$value['name'].'</strong></h5>
						</div>
						</div>
						<div class="row">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l6" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: '.wordwrap($value['address'],60,"<br>\n").' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p"><i class="fas fa-globe"></i> <strong>COUNTRY: '.$ctry.' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: '.$city.' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist'.$value["id"].'" hidden>
							<div class="col s12 m12 l12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col s12 m12 l4" style="margin-left:-12px;">
									<p class="offer_p eve-email"><a href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: '.$value['email_id'].' </strong></a></p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:-1.6%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: '.$value['contact_no'].' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$value["id"].'" class="more showmore active" title="Show More" onclick="showmorelist('.$value["id"].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'.$value["id"].'" class="less showmore" title="Show More" onclick="showmorelist('.$value["id"].')" hidden>SHOW LESS</a>
							</div>
						</div>';
					$vid=$value['id'];	
			$action  = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown$vid' class='dropdown-content'>
				<li><a href=".base_url()."admin_dashboard/edit_coworking_space/".$value['id'].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_space" data-cd_id="'.$value['id'].'"><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';		
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				$action,
	  		);
	  	 }
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($working_space);
		$records["recordsFiltered"] = count($working_space);	

		echo json_encode($records);			  
		exit; 	
	}

	public function delete_workspace() {

			$id = $this->input->post('workspace_id');

			
			$filter = array(
				'status' => $this->input->post('status'),
			);
        	
			$flag = $this->Admin_dashboard_model->deleteData('coworking_space', array('id'=>$id));
			//print $this->db->last_query(); exit();
			if($flag != false) {

				

				echo true;
			}
			else
			{
				echo false;
			}
		}
	
	public function create_incubator(){
		
		 $post=$this->input->post();
		 if($post)
		 {

              if(isset($post['incubator_name'])){

              	$post['name']=$post['incubator_name'];
              	unset($post['incubator_name']);
              }

              if(isset($post['incubator_address'])){

              	$post['address']=$post['incubator_address'];
              	unset($post['incubator_address']);
              }

               if(isset($post['incubator_country'])){

              	$post['country']=$post['incubator_country'];
              	unset($post['incubator_country']);
              }

              if(isset($post['incubator_state'])){

              	$post['state']=$post['incubator_state'];
              	unset($post['incubator_state']);
              }

              if(isset($post['incubator_city'])){

              	$post['city']=$post['incubator_city'];
              	unset($post['incubator_city']);
              }

               if(isset($post['incubator_emailid'])){

              	$post['email_id']=$post['incubator_emailid'];
              	unset($post['incubator_emailid']);
              }

                if(isset($post['incubator_contact'])){

              	$post['contact_no']=$post['incubator_contact'];
              	unset($post['incubator_contact']);
              }

              if(isset($post['submit'])){

              
              	unset($post['submit']);
              }
               unset($post['image_info']);
               if(isset($_FILES['incubator_image']) && $_FILES['incubator_image']['name']!= "") {

				if($_FILES['incubator_image']['name'] != NULL) {
					$post['image'] = $_FILES['incubator_image']['name'];
				}
			}

              $insert_incubator=$this->Admin_dashboard_model->insertData("incubation",$post);


              if($insert_incubator != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['incubator_image']) && $image_info['incubator_image']['name']!= "") {

					if($_FILES['incubator_image']['name'] != NULL) {

						$files = $_FILES['incubator_image'];

						$_FILES['incubator_image']['name']      = $files['name'];
						$_FILES['incubator_image']['type']      = $files['type'];
						$_FILES['incubator_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['incubator_image']['error']     = $files['error'];
						$_FILES['incubator_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('incubator_image','incubator_image/'.$insert_incubator, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}

				////////////////////////////////////Bvcohorts///////////////////////
						// API URL
						$post['file']=new CurlFile($_FILES['incubator_image']['tmp_name'], $_FILES['incubator_image']['type'], $_FILES['incubator_image']['name']);
						$url = 'https://xebra.in/index/insert_incubators';

						// Create a new cURL resource
						$ch = curl_init($url);

						// Setup request to send json via POST
						$data = $post;//array(
						//'username' => 'codexworld',
						//'password' => '123456'
						//);
						$payload = json_encode($data);

						//print_r($payload);

						// Attach encoded JSON string to the POST fields
						curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

						// Set the content type to application/json
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

						// Return response instead of outputting
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// Execute the POST request
						$result = curl_exec($ch);

						// Close cURL resource
						curl_close($ch);
						////////////////////////////////////Bvcohorts///////////////////////
						////////////////////////////////////Onecohorts///////////////////////

						$url = 'https://onecohort.in//index/insert_incubators';

						// Create a new cURL resource
						$ch = curl_init($url);

						// Setup request to send json via POST
						$data = $post;//array(
						//'username' => 'codexworld',
						//'password' => '123456'
						//);
						$payload = json_encode($data);



						// Attach encoded JSON string to the POST fields
						curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

						// Set the content type to application/json
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

						// Return response instead of outputting
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// Execute the POST request
						$result = curl_exec($ch);

						// Close cURL resource
						curl_close($ch);





						//////////////////////////////////////////////////////////////////////////



				$this->session->set_flashdata('success',"Your Data has been added successfully");
				
				redirect(base_url('admin_dashboard/incubator_list'));

			}


		 }
		$data['countries']	=	$this->Admin_dashboard_model->selectData('countries', '*','','country_id','ASC'); 
		$this->load->view('admin_dashboard/incubator',$data);
		
	}

	public function edit_incubator($id){

		$post=$this->input->post();
		 if($post)
		 {

              if(isset($post['incubator_name'])){

              	$post['name']=$post['incubator_name'];
              	unset($post['incubator_name']);
              }

              if(isset($post['incubator_address'])){

              	$post['address']=$post['incubator_address'];
              	unset($post['incubator_address']);
              }

               if(isset($post['incubator_country'])){

              	$post['country']=$post['incubator_country'];
              	unset($post['incubator_country']);
              }

              if(isset($post['incubator_state'])){

              	$post['state']=$post['incubator_state'];
              	unset($post['incubator_state']);
              }

              if(isset($post['incubator_city'])){

              	$post['city']=$post['incubator_city'];
              	unset($post['incubator_city']);
              }

               if(isset($post['incubator_emailid'])){

              	$post['email_id']=$post['incubator_emailid'];
              	unset($post['incubator_emailid']);
              }

                if(isset($post['incubator_contact'])){

              	$post['contact_no']=$post['incubator_contact'];
              	unset($post['incubator_contact']);
              }

              if(isset($post['submit'])){

              
              	unset($post['submit']);
              }
               unset($post['image_info']);
               if(isset($_FILES['incubator_image']) && $_FILES['incubator_image']['name']!= "") {

				if($_FILES['incubator_image']['name'] != NULL) {
					$post['image'] = $_FILES['incubator_image']['name'];
				}
			}

              $insert_incubator=$this->Admin_dashboard_model->updateData("incubation",$post,array('id'=>$id));


              if($insert_incubator != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['incubator_image']) && $image_info['incubator_image']['name']!= "") {

					if($_FILES['incubator_image']['name'] != NULL) {

						$files = $_FILES['incubator_image'];

						$_FILES['incubator_image']['name']      = $files['name'];
						$_FILES['incubator_image']['type']      = $files['type'];
						$_FILES['incubator_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['incubator_image']['error']     = $files['error'];
						$_FILES['incubator_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('incubator_image','incubator_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}


				$this->session->set_flashdata('success',"Your Data has been added successfully");
				
				redirect(base_url('admin_dashboard/incubator_list'));

			}


		 }
		$data['countries']	=	$this->Admin_dashboard_model->selectData('countries', '*','','country_id','ASC'); 
		$data['incubation']=$this->Admin_dashboard_model->selectData("incubation","*",array('id'=>$id));
		$this->load->view('admin_dashboard/edit_incubator',$data);
		
	}
	
	public function incubator_list(){
		
		$this->load->view('admin_dashboard/incubator_list');
		
	}
	
	public function get_incubator_details() {
		 $post['length']=-1;
          

         $records = array();
         $records["data"] = array();

          $post=$this->input->post();
		 $incube= $this->Admin_dashboard_model->getIncubation($post);
		 foreach ($incube as $key => $value) {
		if($value['image'] != '') {
			$image = '<img src="'.base_url().'public/upload/incubator_image/'.$value['id'].'/'.$value['image'].'" height="130px" class="logo_style_2" width="130px">';
		} else {
			$image = '<p style="text-align:center;">No Image</p>';				
		}


		$country=$this->Admin_dashboard_model->selectData('countries',"*",array('country_id'=>$value['country']));

		$cities=$this->Admin_dashboard_model->selectData('cities',"*",array('city_id'=>$value['city']));

		if(count($country)>0){
    $ctry=$country[0]->country_name;
		}else{
           $ctry="";
		}


		if(count($cities)>0){
    $city=$cities[0]->name;
		}else{
           $city="";
		}
		 
		 
		$middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'.$value['name'].'</strong></h5>
						</div>
						</div>
						<div class="row">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l6" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: '.wordwrap($value['address'],60,"<br>\n").' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p"><i class="fas fa-globe"></i> <strong>COUNTRY: '.$ctry.' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: '.$city.' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist'.$value["id"].'" hidden>
							<div class="col s12 m12 l12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col s12 m12 l4" style="margin-left:-12px;">
									<p class="offer_p eve-email"><a href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: '.$value['email_id'].' </strong></a></p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:-1.6%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: '.$value['contact_no'].' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$value["id"].'" class="more showmore active" title="Show More" onclick="showmorelist('.$value["id"].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'.$value["id"].'" class="less showmore" title="Show More" onclick="showmorelist('.$value["id"].')" hidden>SHOW LESS</a>
							</div>
						</div>';
			$vid=$value['id'];				
			$action  = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown$vid' class='dropdown-content'>
				<li><a href=".base_url()."admin_dashboard/edit_incubator/".$value['id'].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_incub" data-cd_id="'.$value['id'].'"><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';		
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				$action,
	  		);
	  	 }
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($incube);
		$records["recordsFiltered"] = count($incube);	

		echo json_encode($records);			  
		exit; 	
	}

	public function delete_incubation() {

			$id = $this->input->post('incube_id');

			
			$filter = array(
				'status' => $this->input->post('status'),
			);
        	
			$flag = $this->Admin_dashboard_model->deleteData('incubation', array('id'=>$id));
			//print $this->db->last_query(); exit();
			if($flag != false) {

				

				echo true;
			}
			else
			{
				echo false;
			}
	}
	
	public function add_config(){
      $post=$this->input->post();

      $data['config']=$this->Admin_dashboard_model->selectData('site_config','*');
		if($post){
			 
          if(count($data['config'])>0){
          	
          	for($i=0;$i<sizeof($post['plan_name']);$i++){
               $data=array(
                'plan_name'=>$post['plan_name'][$i],
                'plan_value'=>$post['plan_value'][$i],
                'discount'=>$post['discount'][$i],
                'referee'=>$post['referee'][$i],
                'referrer'=>$post['referrer'][$i],
                'updated_at'=>date('Y-m-d h:i:s'),
               );
             $this->Admin_dashboard_model->updateData('site_config',$data,array('plan_name' =>$post['plan_name'][$i] ));
         }
          }else{
          	for($i=0;$i<sizeof($post['plan_name']);$i++){
               $data=array(
                'plan_name'=>$post['plan_name'][$i],
                'plan_value'=>$post['plan_value'][$i],
                'discount'=>$post['discount'][$i],
                 'referee'=>$post['referee'][$i],
                'referrer'=>$post['referrer'][$i],
               );

          		 $this->Admin_dashboard_model->insertData('site_config',$data);

          	}
            
          	}
          
               $this->session->set_flashdata('success',"Plan details saved successfully.!!");
             redirect(base_url('admin_dashboard/add_config'));
		}else{
           
			$this->load->view('admin_dashboard/add-config',$data);

		}
		
		
		
	}


	public function get_code(){

		$post=$this->input->post();

		 $data['business']=$this->Adminmaster_model->selectData('coupon_promo','*',array('coupon_code'=>$post['code']));
		 if(count($data['business'])>0){
		 	echo "true";
		 }else{
		 	echo "false";
		 }
	}
	
	public function ca_partner(){
		
		$data['company'] = $this->Admin_dashboard_model->selectData('ca_registration', 'DISTINCT (company)');
		$data['business_type'] = $this->Admin_dashboard_model->selectData('ca_registration', 'DISTINCT(reg_bus_type)');
		$data['city'] = $this->Admin_dashboard_model->selectData('ca_registration', 'DISTINCT(city)');
		$this->load->view('admin_dashboard/ca_partner',$data);
		
	}
	
	public function get_capartner_details(){

      

		$post = $this->input->post();
		
		
		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"feedback"=>'2');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Admin_dashboard_model->capartnerFilter($post,$sort_field,$orderBy,0,1);	
	 	$userData = $this->Admin_dashboard_model->capartnerFilter($post,$sort_field,$orderBy,1,1);


	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {
             
	  	$check='<input type="checkbox" id="filled-in-box'.$value['reg_id'].'" name="filled-in-box'.$value['reg_id'].'" class="filled-in purple capartner_bulk_action" value="'.$value['reg_id'].'"/> <label for="filled-in-box'.$value['reg_id'].'"></label>';
        	
		$action = ''; $status=''; $cls=''; $cls1='';
		
		$disable='';
		if($value['status']!="Active"){
			$status='<li class="activation"><a href="'.base_url().'admin_dashboard/reactivate_ca/'.$value['reg_id'].'"  class="activate_ca_partner" data-profileid='.$value["reg_id"].'><i class="dropdwon-icon icon activate"></i>ACTIVATE</a></li>';
			$cls='deactive_record';
			$cls1='deactivate';

			$disable='disabled="disabled"';
		}else{
				
			$status='<li class="deactivation"><a href="'.base_url().'admin_dashboard/deactivate_ca/'.$value['reg_id'].'" class="deactive_ca_partner" data-profileid="'.$value["reg_id"].'"><i class="dropdwon-icon icon deactivate"></i>DEACTIVATE</a></li>';
			$cls='active_record';
			$cls1='activate';
				
		}
		
		$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['reg_id']."'><i class='action-dot'></i></div>
		<ul id='dropdown".$value['reg_id']."' class='dropdown-content'>'";
		//$li="<li class=".$disable.">";
			//$status='<li class="deactivation"><a href="'.base_url().'admin_dashboard/deactivate_ca/'.$value['reg_id'].'" class="deactive_ca_partner" id="deactive_ca_partner" data-profileid="'.$value['reg_id'].'"><i class="dropdwon-icon icon deactivate"></i>DEACTIVATE</a></li>';				
		if($value['status']=="Active"){
			//$action  .= $li;
		}
		
		$action  .= $status;
		
		$action  .= '</ul>

			<div class="status-action '.$cls1.'">

			<i class="action-status-icon"></i>

			</div>';

			$curr_date=date('Y-m-d');
			$validity_date=date('d-m-Y',strtotime($value['createdat']));     
			
			$date1 = date_create($curr_date);
			$date2 = date_create($validity_date);
			$diffr=$date1->diff($date2);
			$days = $diffr->days;

			$country=$this->Admin_dashboard_model->selectData("countries","*", array('country_id' => $value['country'] ));

			$cities=$this->Admin_dashboard_model->selectData("cities","*", array('city_id' => $value['city'] ));
		
		$records["data"][] = array(
			$check,
				"CA".$value['reg_id']."</br>".date('d-m-Y',strtotime($value['createdat'])),
				$value['company']."</br>".$value['reg_bus_type'],
	  			$value['reg_username'],
	  			$value['reg_email'],
	  			$value['reg_mobile'],
	  			$cities[0]->name."</br>".$country[0]->country_name,
	  			$days,
	  			"",
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	
	}

	public function deactivate_ca($id){

		$this->Admin_dashboard_model->updateData('ca_registration',array('status'=>'Inactive'),array('reg_id' =>$id  ));
		$this->Admin_dashboard_model->updateData('registration',array('status'=>'Inactive'),array('reg_id' =>$id  ));

		$this->session->set_flashdata('success'," Ca partner Deactivated Successfully.");

        redirect('admin_dashboard/ca_partner');
	}
	
	public function reactivate_ca($id){

		$this->Admin_dashboard_model->updateData('ca_registration',array('status'=>'Active'),array('reg_id' =>$id));
         $user= $this->Admin_dashboard_model->selectData('ca_registration','*',array("reg_id"=>$id));

		$this->Admin_dashboard_model->updateData('registration',array('status'=>'Active'),array('reg_email' =>$user[0]->reg_email));


       
				/*	 $to=$user[0]->reg_email;
	          			$data['name']=ucwords($user[0]->reg_username);
	          			$data['username']=ucwords($user[0]->reg_email);
						//$msg="Change your account password";
                          $from="Xebra";
						//$data['msg']=$msg;
						$subject="Credentials for Xebra";
						$message=$this->load->view('ca_portal/verify_ca_email',$data,true);
						sendEmail($from,$to,$subject,$message);
*/


		$this->session->set_flashdata('success'," Ca partner activated Successfully.");

        redirect('admin_dashboard/ca_partner');
	}
	
	public function download_multiple_capartner(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		//print_r($data); exit();
		$this->load->view('admin_dashboard/capartner_export',$data);

	}



	public function maintanance(){
      
        $post= $this->input->post();

       
         $data['start_date']=$post['start_date'];
         $data['end_date']=$post['end_date'];
         $data['start_time']=$post['start_time'];
         $data['end_time']=$post['end_time'];
         $data['email_parti']=$post['email_parti'];

         if($data['email_parti']=="General Maintenance"){
             $emailTemp="admin_dashboard/maintenance";
         }
          else if($data['email_parti']=="GST Verification Maintenance"){
              $emailTemp="admin_dashboard/gst_verify_maintenance";
         }else if($data['email_parti']=="E-invoice API Maintenance"){
             $emailTemp="admin_dashboard/api_maintenance";
         }else{
               $emailTemp="admin_dashboard/maintenance";
         }

		 $chekUser= $this->User_model->selectData('registration', '*', array('status'=>'Active'));

     foreach ($chekUser as $key => $value) {         

     	$data['cust_name']=$value->reg_username;		
     	
     

			
			$from = "Xebra";
			$to =  $value->reg_email;
			$subject = 'Xebra Downtime';
			$message=$this->load->view($emailTemp,$data,true);
			 sendEmail($from,$to,$subject,$message);
    
			
         
     	

     }

      $chekReferal= $this->User_model->selectData('refer_registration', '*', array('status'=>'Active'));

     foreach ($chekReferal as $key1 => $value1) {         

     	$data['cust_name']=$value1->reg_username;		
     	
     

			
			$from = "Xebra";
			$to =  $value1->reg_email;
			$subject = 'Xebra Downtime';
			$message=$this->load->view($emailTemp,$data,true);
			 sendEmail($from,$to,$subject,$message);
    
			
         
     	

     } 


      $chekCa= $this->User_model->selectData('ca_registration', '*', array('status'=>'Active'));

     foreach ($chekCa as $key2 => $value2) {         

     	$data['cust_name']=$value2->reg_username;		
     	
     

			
			$from = "Xebra";
			$to =  $value2->reg_email;
			$subject = 'Xebra Downtime';
			$message=$this->load->view($emailTemp,$data,true);
			 sendEmail($from,$to,$subject,$message);
    
			
         
     	

     }


	}
	
	public function blog_post_list(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$this->load->view('admin_dashboard/blog_post/blog-post-list',$data);

	}
	
	public function add_blog_post(){ 
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$this->load->view('admin_dashboard/blog_post/add_blog_post',$data);
	}
	
	public function submit_blog_post(){ 

		 $session=$this->session->userdata('admin_session');
		//pr($session,1);
		
		
        $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
           $post['reg_id']=$session['admin_id'];
		}

            $get_blog_id = $this->Admin_dashboard_model->insertData('blogs',$post);
                //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
                $image_info = array();
                $image_info = $_FILES;
                   if($get_blog_id){
                if($image_info['featured_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('featured_image','blog_image/'.$get_blog_id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('blogs',array('filename'=>$image_info['featured_image']['name'],'image'=>base_url("/public/upload/blog_image/".$get_blog_id)),array('id'=>$get_blog_id ));
                       
                     
                    }
                }


                /////////////////////////////////////////// curl post data///////////////
					

////////////////////////////////////Bvcohorts///////////////////////
					// API URL
                 //$post['file']=new CurlFile($_FILES['featured_image']['tmp_name'], $_FILES['featured_image']['type'], $_FILES['featured_image']['name']);
//$url = 'https://bharatvaani.in/index/insert_blog';

$url = 'https://xebra.in/index/insert_blog';


$targetFile=DOC_ROOT."/public/upload/blog_image/".$get_blog_id."/".$_FILES['featured_image']['name'];

$fp = fopen($targetFile, "w");
 $filename  = $_FILES['featured_image']['tmp_name'];
 $handle    = fopen($filename, "r");
 $uploadFile      = fread($handle, filesize($filename));
 $post['file'] = base64_encode($uploadFile);

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = $post;//array(
//'username' => 'codexworld',
//'password' => '123456'
//);
$payload = json_encode($data);

//print_r($payload);

// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_FILE, $fp);

curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);

 echo '<pre>'.print_r($result,1).'</pre>';exit;
////////////////////////////////////Bvcohorts///////////////////////
////////////////////////////////////Onecohorts///////////////////////

$url = 'https://onecohort.in/index/insert_blog';

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = $post;//array(
//'username' => 'codexworld',
//'password' => '123456'
//);
$payload = json_encode($data);

print_r($payload);

// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);





                //////////////////////////////////////////////////////////////////////////


				$subscriber=$this->Admin_dashboard_model->selectData('blog_subscribe',"*");
                $subject="New blog is uploaded ";
                $from ="Xebra";
                $data['title']=$post['post_name'];
                $data['url']=$post['url'];
                foreach($subscriber as $subscr){
					$to =$subscr->email;

					$data['username']=$subscr->name;
					$message=$this->load->view('admin_dashboard/blog_post/blog_notify_email',$data,true);
						//sendEmail($from ,$to,$subject,$message);	
                   }
                }
               redirect('admin_dashboard/blog_post_list');
        }
	}

	public function edit_admin_blog($id){
		
		         $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
           $post['reg_id']=$session['admin_id'];
		}

            $get_blog_id = $this->Admin_dashboard_model->updateData('blogs',$post,array("id"=>$id));

            //print $this->db->last_query();exit;
                //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
                $image_info = array();
                $image_info = $_FILES;
                   if($get_blog_id){
                if($image_info['featured_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('featured_image','blog_image/'.$id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('blogs',array('filename'=>$image_info['featured_image']['name'],'image'=>base_url("/public/upload/blog_image/".$id)),array('id'=>$id ));
                       
                     
                    }
                }

                   
                }
               redirect('admin_dashboard/blog_post_list');
        }
		$data['blogs'] = $this->Admin_dashboard_model->selectData("blogs","*",array('id'=>$id));
		$this->load->view('admin_dashboard/blog_post/edit_blog_post',$data);
	}

	public function delete_admin_blog($id){
		 $get_blog_id = $this->Admin_dashboard_model->deleteData('blogs',array('id'=>$id));
            redirect('admin_dashboard/blog_post_list');
	}
	
	public function get_post_details(){ 
		
	  $post = $this->input->post();
      
        
        $field_pos=array("id"=>'0');  

        $sort_field=array_search($post['order'][0]['column'],$field_pos);

        if($post['order'][0]['dir']=='asc')
        {
            $orderBy="ASC";
        }
        else
        {
            $orderBy="DESC";
        }   

        $TotalRecord=$this->Admin_dashboard_model->fetchBlogs($post,$sort_field,$orderBy,0);   
        $userData = $this->Admin_dashboard_model->fetchBlogs($post,$sort_field,$orderBy,1);
        
        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
        
        foreach ($userData as $key => $value) {
    
     $check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple post_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
            $action = '';$status='';$cls='';$cls1='';
            $action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['id']."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value['id']."' class='dropdown-content'>
                <a href=".base_url()."services/edit-service/".$value['id'].'>
            ';

            $action  .= $status;
            
              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';

              if($value['category']=='blog'){
                  $category="Product";
              }else{
              	$category=$value['category'];
              }
			$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_admin_blog/".$value['id']."'>Edit</a></br><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/delete_admin_blog/".$value['id']."'>Delete</a></div>";
         
            $records["data"][] = array(
            	$check,
				date("d-m-Y",strtotime($value['created_at'])) ,
				$value['post_name'],
				$category,
				$action_links,
                
            );
            
            }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);               
        exit;
	}

	public function icici_wallet_referral(){
 
   $data = $this->input->post();

        $bus_info_xebra=$this->Adminmaster_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));
       
		$reg_id = $bus_info_xebra[0]->reg_id;
		$bus_id =$bus_info_xebra[0]->bus_id;
  $data['reg_id']=$reg_id;
  $data['bus_id']=$bus_id;

  $expensedetails= $this->Adminmaster_model->selectData('subscription', '*',array('subscription_id'=>$data['exp_id']));
   $referdetails= $this->Adminmaster_model->selectData('refer_earn', '*',array('refer_code'=>$expensedetails[0]->referalcode));
   $data['amount']= "1000";//$expensedetails[0]->net_earning;
   $data['uniqid']=$this->Adminmaster_model->randomCode();

   if(count($referdetails)>0){

  $data['xebra_bank_account']=$referdetails[0]->ac_number;
   $data['nickname']="referaccount";
   $data['ifsc']=$referdetails[0]->ifsc_code;
           $strIFSC=substr($data['ifsc'], 0, 4);

           if($strIFSC=="ICIC"){
           	$data['payeeType']="W";
           }else{
           	$data['payeeType']="O";
           }

        }else{

        	   $data['xebra_bank_account']="";
   $data['nickname']="referaccount";
   $data['ifsc']="";
         
           	$data['payeeType']="";

           	$data['msg']= "No benefeciary bank details";
          

   }
  

		$checkBank= $this->Adminmaster_model->selectData('company_bank', '*',array('bus_id'=>$bus_id,'cbank_ifsc_code like'=>substr("ICIC", 0, 4)."%"));
	
		if(count($checkBank)<1){
            $data['msg']= "No bank";
		}else{
           
             
            $data['bank_details']=$checkBank;
              ////////////////////call ICICI API///
            if($data['bank_details'][0]->corp_id !="" || $data['bank_details'][0]->user_id !="" || $data['bank_details'][0]->urn_no !=""){
               $IciciApi=$this->Adminmaster_model->IciciApi($data);
               $data['msg']= $IciciApi;
            }else{
            	//
            }
              
		}
		
		echo json_encode($data);
		exit();		
	}

	public function icici_wallet_transaction(){
	  $data = $this->input->post();


		 $bus_info_xebra=$this->Adminmaster_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));
       
		$reg_id = $bus_info_xebra[0]->reg_id;
		$bus_id =$bus_info_xebra[0]->bus_id;
		$data['reg_id']=$reg_id;
		$data['bus_id']=$bus_id;

		

		   $IciciApi=$this->Adminmaster_model->IciciApi_transaction($data);
		   exit;

	}

		public function save_bank(){
	 $bus_info_xebra=$this->Adminmaster_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));
       
		$reg_id = $bus_info_xebra[0]->reg_id;
		$bus_id =$bus_info_xebra[0]->bus_id;
		$gst_xebra=$this->Adminmaster_model->selectData("gst_number","*", array('bus_id' =>$bus_id,'type'=>'business'),'gst_id','DESC');
		$gst_id = $gst_xebra[0]->gst_id;
		$bank_info = array();

		parse_str(($_POST['frm']),$bank_info);	

        
        $bank_acc_no=$this->Adminmaster_model->selectData('company_bank','*',array("bus_id"=>$bus_id,'cbank_account_no'=>$bank_info['ac_number']));
		$insert_bank = array(
			'bus_id'	=>	$bus_id,
			'cbank_account_no' 	=>  $bank_info['ac_number'],
			'cbank_name'		=>	$bank_info['bank_name'],
			'cbank_branch_name'		=>	$bank_info['bank_branch_name'],
			'cbank_account_type'	=>	$bank_info['acc_type'],
			'cbank_ifsc_code'		=>	@$bank_info['ifsc_code'],
			'urn_no'		=>	@$bank_info['urn_no'],
			'cbank_country_code'		=>	@$bank_info['bus_bank_country'],
			'cbank_currency_code'		=>	$bank_info['bank_cur_check'],
			'user_id'		=>	$bank_info['user_id'],
            'corp_id'		=>	$bank_info['corp_id'],
            'api_integration'		=>	"No",
		);
		if(count($bank_acc_no)>0){
         $flag=$this->Adminmaster_model->updateData('company_bank',$insert_bank,array("bus_id"=>$bus_id,'cbank_account_no'=>$bank_info['ac_number']));
		}else{
		$flag=$this->Adminmaster_model->insertData('company_bank',$insert_bank);
		if($flag != false)
		{   
			 $acc = $this->Adminmaster_model->selectData('account','*',array("bus_id"=>$bus_id),'acc_id','DESC'); 
		
		if(count($acc)>0){
			$last_cs_no=$acc[0]->acc_no; 
			 $whatIWant = substr($last_cs_no, strpos($last_cs_no, "/") + 1); 
              $iemNo=str_replace('/'.$whatIWant,"",$last_cs_no);
              $temp=str_replace("ACC","",$iemNo);
			//if($inv_no[0]->document_type=='2'){
				$exp_ref='1'; 
				$temp=intval($temp) + 1;
				//$temp = substr($last_inv_no, 3, -7);
				//$temp= str_replace('INV', '', $last_inv_no); 
				$exp_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
			//}
		} 
		else
		{
				$exp_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
		}
		
		$acc_no='ACC'.$exp_ref;

		  $bankData= array(
           'bus_id'=>$bus_id,
           'reg_id'=>$reg_id,
           'gst_id'=>$gst_id,
           'acc_no'=>$acc_no,
           'date'=>date('d/m/Y'),
           'acc_name'=> $insert_bank['cbank_name'],
           'open_bal'=>0,
           'acc_type'=> 'Bank',
           'acc_cat'=> 'B',
           'status'=> 'Active',
           'createdat'=> date('Y-m-d H:i:s'),
           'updatedat'=> date('Y-m-d H:i:s'),

		  );

            $bankforjv = $this->Adminmaster_model->insertData('account',$bankData);


           
		
		$acc_no1='ACC'.($exp_ref+1);

		  $bankData1= array(
           'bus_id'=>$bus_id,
           'reg_id'=>$reg_id,
          'gst_id'=>$gst_id,
           'acc_no'=>$acc_no1,
           'date'=>date('d/m/Y'),
           'acc_name'=> $insert_bank['cbank_name'],
            'open_bal'=> 0,
           'acc_type'=> 'Bank',
           'acc_cat'=> 'C',
           'status'=> 'Active',
           'createdat'=> date('Y-m-d H:i:s'),
           'updatedat'=> date('Y-m-d H:i:s'),

		  );

            $bankforjv = $this->Adminmaster_model->insertData('account',$bankData1);




           

            

	}
}


 ///////////////////////////////////////////////////////////////////////////////
  $IciciApi=$this->Adminmaster_model->IciciApi_registration($bank_info);

  if($IciciApi=="Success"){

  	 $flag2=$this->Adminmaster_model->updateData('company_bank',array("api_integration"=>"Yes"),array("bus_id"=>$bus_id,'api'=>$bank_info['ac_number']));
  }

 

 ///////////////////////////////////////////////////////////////////////////////
echo true;exit;

}


	public function create_referral_payment(){


      $post=$this->input->post();
		$referaldetails= $this->Adminmaster_model->selectData('refer_earn', '*',array('refer_id'=>$post['id']));

		          $from="Xebra";
            $data['subject']="Referal fee transfer to Your Account";
            $data['name'] =  @$referaldetails[0]->refer_name;
            $data['amount'] =$post['amount'];
           
          //  $content=$this->load->view('email_template/addition_from_wallet',$data,true);

          $content="Referal fee amount" .$post['amount']." Rs Transfer to your Account.";
         

          
        
          sendEmail($from, @$referaldetails[0]->refer_email,$data['subject'],$content);

          
        $bus_info_xebra=$this->Adminmaster_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));

        $xebra_bank=$this->Adminmaster_model->selectData('company_bank', '*',array('bus_id'=>$bus_info_xebra[0]->bus_id,'cbank_ifsc_code like'=>substr("ICIC", 0, 4)."%"),'cbank_id','ASC');
       
		$reg_id = $bus_info_xebra[0]->reg_id;
		$bus_id =$bus_info_xebra[0]->bus_id;

         $gst_xebra=$this->Adminmaster_model->selectData("gst_number","*", array('bus_id' =>$bus_id,'type'=>'business'),'gst_id','DESC');
		$gst_id = $gst_xebra[0]->gst_id;
		$acc1 = $this->Adminmaster_model->selectData('bank_statement','*',array("bus_id"=>$bus_id),'bank_stat_id','DESC'); 

						if(count($acc1)>0){
						$last_cs_no1=$acc1[0]->bank_stat_id; 

						$whatIWant = substr($last_cs_no1, strpos($last_cs_no1, "/") + 1); 
						$iemNo=str_replace('/'.$whatIWant,"",$last_cs_no1);
						$temp1=str_replace("BSI","",$iemNo);

						//if($inv_no[0]->document_type=='2'){
						$exp_ref='1'; 
						$temp1=intval($temp1);

						//$temp = substr($last_inv_no, 3, -7);
						//$temp= str_replace('INV', '', $last_inv_no); 
						$exp_ref=str_pad($temp1, 0, 0, STR_PAD_LEFT);
						//}
						} 
						else
						{
						$exp_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
						}

          $bank_cash_data['bus_id'] = $bus_id;
						$bank_cash_data['reg_id'] = $reg_id;
						$bank_cash_data['gst_id'] = $gst_id;
						$bank_cash_data['bank_statement_date'] = date('Y-m-d');
						$bank_cash_data['bank_id'] =$xebra_bank[0]->cbank_id;
						$bank_cash_data['bank_id_to'] =0;
						$bank_cash_data['bank_note'] = "Amount transferred to referral Account";

						$bank_cash_data['bank_amount'] = $post['amount'];
						$bank_cash_data['bank_state_code'] = 'BSI'.$exp_ref;
						$bank_cash_data['status'] = "Active";

						$bank_cash=$this->Adminmaster_model->insertData('bank_statement',$bank_cash_data);
						/////////////////////////////////////////////////////////////////////////////////


					


						$pay_amt=$post['amount'];


						$ledgerEntry=array(
						'bus_id'=>$bus_id,
						'reg_id'=>$reg_id,
						'gst_id'=>$gst_id,            
						'common_id'=>$bank_cash,
						'resource_id'=>"BSI".$bank_cash,
						'ledger_name'=>"bank_transfer",
						'ledger_type'=>"bank",
						'acc_ledg_debit'=>$pay_amt,
						'particulars'=>"Bank Transfer for ref BSI".$bank_cash  ,
						'bank_debit'=>$pay_amt,
						'ledg_date'=>date('Y-m-d'),
						//'bank_debit'=>$pay_amt,
						'bank_id'=>$xebra_bank[0]->cbank_id,
						'bank_id_debit'=>$xebra_bank[0]->cbank_id,

						'status'=>'Active',




						);

						

						$this->Adminmaster_model->insertData('ledger',$ledgerEntry);



						

						$balanceEntry=array(
						'bus_id'=>$bus_id,
						'reg_id'=>$reg_id,
						'gst_id'=>$gst_id,
						'ledger_name'=>"bank_transfer",
						'ledger_type'=>"bank",
						'common_id'=>$bank_cash,
						'resource_id'=>"BSI".$bank_cash,            
						'date'=>date('Y-m-d'),

						'bank'=>$pay_amt,
						'acc_pay'=>$pay_amt,


						'status'=>'Active',


						);//print_r($balanceEntry);
						//exit();


						$this->Adminmaster_model->insertData('balance_sheet',$balanceEntry);	


				


						$acc = $this->Adminmaster_model->selectData('account_jv','*',array("bus_id"=>$bus_id),'jvacc_id','DESC'); 
						if(count($acc)>0){
						$last_cs_no=$acc[0]->jv_no; 
						$whatIWant = substr($last_cs_no, strpos($last_cs_no, "/") + 1); 
						$iemNo=str_replace('/'.$whatIWant,"",$last_cs_no);
						$temp=str_replace("JV ","",$iemNo);
						//if($inv_no[0]->document_type=='2'){
						$exp_ref='1'; 
						$temp=intval($temp) + 1;
						//$temp = substr($last_inv_no, 3, -7);
						//$temp= str_replace('INV', '', $last_inv_no); 
						$exp_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
						//}
						} 
						else
						{
						$exp_ref= str_pad(1, 0, 0, STR_PAD_LEFT);
						}

						$jv_no='JV '.$exp_ref;

						$jvEntry=array(
						'jv_no'=> $jv_no,
						'common_id'=>$bank_cash,
						'resource_id'=>"BSI".$bank_cash,  
						'ledger_name'=>"bank_transfer",

						
						'reg_id'=> $reg_id,
						'gst_id'=> $gst_id,
						'bus_id'=> $bus_id,
						'date'=>date('Y-m-d'),
						'account_name'=>"bank",
						'dacc_name'=>$xebra_bank[0]->cbank_id,
						'debit'=>$pay_amt,
						'cacc_name'=>"Payment for wallet",
						'credit'=>$pay_amt,


						'narration'=>"bank",

						'status'=>'Active',
						);
						$this->Adminmaster_model->insertData('account_jv',$jvEntry);


					



						$p_lEntry=array(
						'bus_id'=>$bus_id,
						'reg_id'=>$reg_id,
						'gst_id'=>$gst_id,
						'ledger_name'=>"bank_transfer",
						'ledger_type'=>"bank",
						'common_id'=>$bank_cash,
						'resource_id'=>"BSI".$bank_cash,               
						'date'=>date('Y-m-d'),                  


						'gen_and_admin'=>$pay_amt,

						'status'=>'Active',



						);

						$this->Adminmaster_model->insertData('profit_and_loss',$p_lEntry);



						$data["response"]="true";

                   echo json_encode($data);exit;
						

       
	}

	
	public function ck_upload(){
		
		$this->load->view('admin_dashboard/blog_post/ck_upload'); 
		
	}
	
	public function blog_digest_list(){
		
		$this->load->view('admin_dashboard/blog_post/blog_digest_list'); 
		
	}
	
	public function get_digest_details(){ 
		
		  $post = $this->input->post();
      
        
        $field_pos=array("id"=>'0');  

        $sort_field=array_search($post['order'][0]['column'],$field_pos);

        if($post['order'][0]['dir']=='asc')
        {
            $orderBy="ASC";
        }
        else
        {
            $orderBy="DESC";
        }   

        $TotalRecord=$this->Admin_dashboard_model->fetchBlogsDigest($post,$sort_field,$orderBy,0);   
        $userData = $this->Admin_dashboard_model->fetchBlogsDigest($post,$sort_field,$orderBy,1);
        
        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
        
        foreach ($userData as $key => $value) {
    
     $check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple post_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
            $action = '';$status='';$cls='';$cls1='';
            $action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['id']."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value['id']."' class='dropdown-content'>
                <a href=".base_url()."services/edit-service/".$value['id'].'>
            ';

            $action  .= $status;
            
              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';
			$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_admin_blog/".$value['id']."'>Edit</a></br><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/delete_admin_blog/".$value['id']."'>Delete</a></div>";
         
            $records["data"][] = array(
            	$check,
				
				$value['name'],
				$value['email'],
				date("d-m-Y",strtotime($value['created_at'])) ,
				//$action_links,
                
            );
            
            }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);               
        exit;
	}
	
	public function print_multiple_digest(){
		//$array=$this->input->get('ids');
		//$array=explode(',', $array);

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->digestDataDownload($array);
		
		$this->load->view('admin_dashboard/blog_post/blog_digest_print');
	}
	
	public function print_multiple_post(){
		//$array=$this->input->get('ids');
		//$array=explode(',', $array);

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->blogDataDownload($array);
		
		$this->load->view('admin_dashboard/blog_post/blog_post_print',$data);
	}
	
	public function survey_list(){
		
		//$this->load->view('admin_dashboard/blog_post/survey_list',$data); 
		$this->load->view('admin_dashboard/blog_post/survey_list'); 
		
	}
	
	public function get_survey_details(){ 
		
		  $post = $this->input->post();
      
        
        $field_pos=array("id"=>'0');  

        $sort_field=array_search($post['order'][0]['column'],$field_pos);

        if($post['order'][0]['dir']=='asc')
        {
            $orderBy="ASC";
        }
        else
        {
            $orderBy="DESC";
        }   

        $TotalRecord=$this->Admin_dashboard_model->fetchSurvey($post,$sort_field,$orderBy,0);   
        $userData = $this->Admin_dashboard_model->fetchSurvey($post,$sort_field,$orderBy,1);
        
        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
		
        foreach ($userData as $key => $value) {
    
			$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple survey_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
            $action = '';$status='';$cls='';$cls1='';
            $action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['id']."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value['id']."' class='dropdown-content'>
                <a href=".base_url()."admin_dashboard/edit-survey/".$value['id'].'>
            ';

            $action  .= $status;
            
              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';
			//$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_admin_blog/".$value['id']."'>Edit</a></br><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/delete_admin_blog/".$value['id']."'>Delete</a></div>";
         
            $records["data"][] = array(
            	$check,
				date('d-m-Y',strtotime($value['created_at'])),
				$value['email'],
				$value['reason'],
				//$action_links,
            );
            
            }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);               
        exit;
	}
	
	public function print_multiple_survey(){
		//$array=$this->input->get('ids');
		//$array=explode(',', $array);

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->surveyDataDownload($array);
		
		$this->load->view('admin_dashboard/blog_post/survey_list_print',$data);
	}
	
	public function newsletter_list(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$this->load->view('admin_dashboard/newsletter_list',$data);

	}
	
	public function newsletter_add(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$this->load->view('admin_dashboard/add-newsletter',$data);

	}
	
	public function edit_newsletter($id){
		
		         $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
           //$post['reg_id']=$session['admin_id'];
		}

            $get_newsletter_id = $this->Admin_dashboard_model->updateData('newsletters',$post,array("id"=>$id));

            
                $image_info = array();
                $image_info = $_FILES;
                   if($get_newsletter_id){
                if($image_info['featured_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('featured_image','newsletter_image/'.$id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('newsletters',array('filename'=>$image_info['featured_image']['name'],'image'=>base_url("/public/upload/newsletter_image/".$id)),array('id'=>$id ));
                       
                     
                    }
                }

                 if($image_info['newsletter_file']['name'] != '')
                {
                    $user_file=$this->Common_model->upload_org_filename('newsletter_file','newsletter_file/'.$id ,$allowd="*",array());
                    if($user_file!=false){
                        
                        $get_file_id = $this->Admin_dashboard_model->updateData('newsletters',array('file'=>base_url("/public/upload/newsletter_file/".$id.'/'.$image_info['newsletter_file']['name'])),array('id'=>$id ));
                       
                     
                    }
                }

                   
                }
               redirect('admin_dashboard/newsletter_list');
        }
		$data['newsletter'] = $this->Admin_dashboard_model->selectData("newsletters","*",array('id'=>$id));
		$this->load->view('admin_dashboard/edit-newsletter',$data);
	}

	public function delete_newsletter($id){
		 $get_newsletter_id = $this->Admin_dashboard_model->deleteData('newsletters',array('id'=>$id));
            redirect('admin_dashboard/newsletter_list');
	}
	



	public function submit_newsletter(){ 

		 $session=$this->session->userdata('admin_session');
		//pr($session,1);
		
		
        $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
          // $post['reg_id']=$session['admin_id'];
		}

            $get_newsletter_id = $this->Admin_dashboard_model->insertData('newsletters',$post);
               
                $image_info = array();
                $image_info = $_FILES;
                   if($get_newsletter_id){
                if($image_info['featured_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('featured_image','newsletter_image/'.$get_newsletter_id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('newsletters',array('filename'=>$image_info['featured_image']['name'],'image'=>base_url("/public/upload/newsletter_image/".$get_newsletter_id)),array('id'=>$get_newsletter_id ));
                       
                     
                    }
                }


                  if($image_info['newsletter_file']['name'] != '')
                {
                    $user_file=$this->Common_model->upload_org_filename('newsletter_file','newsletter_file/'.$get_newsletter_id ,$allowd="*",array());
                    if($user_file!=false){
                        
                        $get_file_id = $this->Admin_dashboard_model->updateData('newsletters',array('file'=>base_url("/public/upload/newsletter_file/".$get_newsletter_id.'/'.$image_info['newsletter_file']['name'])),array('id'=>$get_newsletter_id ));
                       
                     
                    }
                }

			
                }
               redirect('admin_dashboard/newsletter_list');
        }
	}
	
	public function get_newsletter_details(){ 
	 $post = $this->input->post();
      
        
        $field_pos=array("id"=>'0');  

        $sort_field=array_search($post['order'][0]['column'],$field_pos);

        if($post['order'][0]['dir']=='asc')
        {
            $orderBy="ASC";
        }
        else
        {
            $orderBy="DESC";
        }   

        $TotalRecord=$this->Admin_dashboard_model->fetchNewsletters($post,$sort_field,$orderBy,0);   
        $userData = $this->Admin_dashboard_model->fetchNewsletters($post,$sort_field,$orderBy,1);
        
        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
        
        foreach ($userData as $key => $value) {
    
     $check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple post_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
            $action = '';$status='';$cls='';$cls1='';
            $action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['id']."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value['id']."' class='dropdown-content'>
                <a href=".base_url()."services/edit-service/".$value['id'].'>
            ';

            $action  .= $status;
            
              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';
			$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_newsletter/".$value['id']."'>Edit</a></br><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/delete_newsletter/".$value['id']."'>Delete</a></div>";
         
            $records["data"][] = array(
            	$check,
				date("d-m-Y",strtotime($value['createdat'])) ,
				$value['newsletter_name'],
				$value['newsletter_year'],
				$action_links,
                
            );
            
            }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);               
        exit;
	}
	
	public function print_multiple_newsletter(){
		//$array=$this->input->get('ids');
		//$array=explode(',', $array);

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->newsletterDownload($array);
		
		$this->load->view('admin_dashboard/newsletter_list_print',$data);
	}
	
	public function admin_challenge(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		//$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$this->load->view('admin_dashboard/challange_list',$data);

	}

	public function submit_challange(){ 

		 $session=$this->session->userdata('admin_session');
		//pr($session,1);
		
		
        $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
          // $post['reg_id']=$session['admin_id'];
		}
            
          $post['challange_date']= date("Y-m-d", strtotime(str_replace('/', '-',$post['challange_date'])));
          $post['last_day_entry'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['last_day_entry'])));
            $get_challange_id = $this->Admin_dashboard_model->insertData('challanges',$post);
               
                $image_info = array();
                $image_info = $_FILES;
                   if($get_challange_id){
                if($image_info['challange_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('challange_image','challange_image/'.$get_challange_id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('challanges',array('challange_image'=>$image_info['challange_image']['name']),array('id'=>$get_challange_id ));
                       
                     
                    }
                }


                ////////////////////////////////////Bvcohorts///////////////////////
					// API URL
                 $post['file']=new CurlFile($_FILES['challange_image']['tmp_name'], $_FILES['challange_image']['type'], $_FILES['challange_image']['name']);
$url = 'https://xebra.in/index/insert_challange';

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = $post;//array(
//'username' => 'codexworld',
//'password' => '123456'
//);
$payload = json_encode($data);

//print_r($payload);

// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);
////////////////////////////////////Bvcohorts///////////////////////
////////////////////////////////////Onecohorts///////////////////////

$url = 'https://onecohort.in//index/insert_challange';

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = $post;//array(
//'username' => 'codexworld',
//'password' => '123456'
//);
$payload = json_encode($data);



// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);





                //////////////////////////////////////////////////////////////////////////


                 
			
                }
               redirect('admin_dashboard/admin_challenge');
        }
	}
	
	public function add_challenge(){ 

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		//$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
		$challanges = $this->Admin_dashboard_model->selectData('challanges','*','','id','DESC'); 
		
		if(count($challanges)>0){
			$last_cs_no=$challanges[0]->challange_code; 
			 $whatIWant = substr($last_cs_no, strpos($last_cs_no, "/") + 1); 
              $iemNo=str_replace('/'.$whatIWant,"",$last_cs_no);
              $temp=str_replace("CHA","",$iemNo);
			//if($inv_no[0]->document_type=='2'){
				$cus_ref='1'; 
				$temp=intval($temp) + 1;
				//$temp = substr($last_inv_no, 3, -7);
				//$temp= str_replace('INV', '', $last_inv_no); 
				$cha_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
			//}
		} 
		else
		{
				$cha_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
		}
		
		$data['cha_code']='CHA'.$cha_ref;
		$this->load->view('admin_dashboard/add_challange',$data);

	}
	
	public function edit_challenge($id){
		
		         $post=$this->input->post();

        if($post){
        	if (isset($session['admin_id'])) {
           //$post['reg_id']=$session['admin_id'];
		}
           
               $post['challange_date']= date("Y-m-d", strtotime(str_replace('/', '-',$post['challange_date'])));
             $post['last_day_entry'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['last_day_entry'])));
            $get_challange_id = $this->Admin_dashboard_model->updateData('challanges',$post,array("id"=>$id));

            
                $image_info = array();
                $image_info = $_FILES;
                   if($get_challange_id){
                if($image_info['challange_image']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('challange_image','challange_image/'.$id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Admin_dashboard_model->updateData('challanges',array('challange_image'=>$image_info['challange_image']['name']),array('id'=>$id ));
                       
                     
                    }
                }
                   
                }
               redirect('admin_dashboard/admin_challenge');
        }
		$data['challanges'] = $this->Admin_dashboard_model->selectData("challanges","*",array('id'=>$id));
		$this->load->view('admin_dashboard/edit_challange',$data);
	}

	//public function delete_challange($id){
		 //$get_newsletter_id = $this->Admin_dashboard_model->deleteData('challanges',array('id'=>$id));
         //redirect('admin_dashboard/admin_challenge');
	//}
	
	public function delete_challange() {

        $chal_id=$this->input->post('id');
        $status='Inactive';
		$get_newsletter_id = $this->Admin_dashboard_model->deleteData('challanges',array('id'=>$chal_id));
		echo true;exit;
    }
		
	public function get_challange_details(){ 
	 $post = $this->input->post();
      
        
        $field_pos=array("id"=>'0');  

        $sort_field=array_search($post['order'][0]['column'],$field_pos);

        if($post['order'][0]['dir']=='asc')
        {
            $orderBy="ASC";
        }
        else
        {
            $orderBy="DESC";
        }   

        $TotalRecord=$this->Admin_dashboard_model->fetchChallanges($post,$sort_field,$orderBy,0);   
        $userData = $this->Admin_dashboard_model->fetchChallanges($post,$sort_field,$orderBy,1);
        
        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
        
        foreach ($userData as $key => $value) {
    
     $check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple challange_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
            $action = '';$status='';$cls='';$cls1='';
            $action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['id']."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value['id']."' class='dropdown-content'>
                <a href=".base_url()."services/edit-service/".$value['id'].'>
            ';

            $action  .= $status;
            
              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';
			 //href='".base_url()."admin_dashboard/delete_challange/".$value['id']."' 
			$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_challenge/".$value['id']."'>Edit</a></br><a style='font-size:12px;' href='javascript:void(0);' class='delete_challenge_btn action_link' data-chal_id=".$value['id'].">Delete</a></div>";
			
            $records["data"][] = array(
            	$check,
				$value['challange_code']."</br>".date("d-m-Y",strtotime($value['challange_date'])) ,
				$value['challange_name'],
				wordwrap($value['challange_desc'],50,"<br>"),
				$value['challange_nature'],
				date("d-m-Y",strtotime($value['last_day_entry'])) ,
				$value['location'],
				$action_links,
                
            );
            
            }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);               
        exit;
	}
	
	public function download_multiple_challenge(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->challange_data_download($array);

		$this->load->view('admin_dashboard/export_challenge',$data);

	}
	
	
	
	public function users(){ 

		$res_country_ids	=	$this->Admin_dashboard_model->Get_unique_country();
		for($i=0;$i<count($res_country_ids);$i++)
		{
			$arrc[$i] = $res_country_ids[$i]['bus_billing_country'];
		}
		//$stringc = implode(",",$arrc);

		//$res['country'] = $this->Admin_dashboard_model->Get_country($stringc);


		$res_city_ids	=	$this->Admin_dashboard_model->Get_unique_cities();
		for($i=0;$i<count($res_city_ids);$i++)
		{
			$arr[$i] = $res_city_ids[$i]['bus_billing_city']; 
		}
		//$string = implode(",",$arr);

		///$res['cities'] = $this->Admin_dashboard_model->Get_cities($string);
		$this->load->view('admin_dashboard/user_list',$res);

	}
	
	public function get_users_details()
	{
		is_login_admin_panel();
		$post = $this->input->post();

		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"reg_username"=>'2',"reg_mobile"=>'3',"reg_istrial"=>'4',"status"=>'5',"reg_designation"=>'6',"amount"=>'7');	

		$sort_field=array_search(@$post['order'][0]['column'],$field_pos);

		if(@$post['order'][0]['dir']=='asc')
		{
	 		$orderBy="DESC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	


		$TotalRecord=$this->Admin_dashboard_model->fetchUsers($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Admin_dashboard_model->fetchUsers($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	// echo '<pre>';
	 	// print_r($userData);
	 	// echo '</pre>';
	 	// exit;
	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['reg_id'].'" name="filled-in-box'.$value['reg_id'].'" class="filled-in purple users_bulk_action" value="'.$value['reg_id'].'"/><label for="filled-in-box'.$value['reg_id'].'"></label>';

	  		$created_at =  date("d-m-Y", strtotime($value['createdat']));

	  		if($value['reg_istrial']==0)
	  		{
	  			$reg_istrial = 'NO';
	  			

	  		}else{
	  			$reg_istrial = 'YES';
	  			$sub_status = 'NO';
	  		}



	  		if($value['promocode']!="" && $value['referalcode']!="")
	  		{
	  			$value['promocode'] = 'Promo &&</br> Referal';
	  			

	  		}else if($value['promocode']!="" && $value['referalcode']=="")
	  		{
	  			$value['promocode'] = 'Promo ';
	  			

	  		}else if($value['promocode']=="" && $value['referalcode']!="")
	  		{
	  			$value['promocode'] = 'Referal ';
	  			

	  		}else{
	  			$value['promocode'] = 'No any </br>code used ';
	  		}

	  		$sub_status = $value['sub_status'];
	  		if($sub_status==""){
	  			$sub_status = "Not </br>Subscribed";
	  		}

	  		if($value['amount']!=""){
	  			$value['amount']=$value['amount'];
	  		}else{
	  			$value['amount']=0;
	  		}
			
		$date1=date_create(date("Y-m-d", strtotime($value['createdat'])));
		$date2=date_create(date("Y-m-d"));
		$diff=date_diff($date1,$date2);
		$days = $diff->format("%a");


		$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$cls='deactive_record';
				$cls1='deactivate';

				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>	'";

				$status='<li class="activation"><a href="#" class="active_users_profile" id="active_users_profile"  data-profileid='.$value["reg_id"].'><i class="dropdwon-icon icon activate"></i>Re-Activate</a></li>';
				
			}else{

				$cls='active_record';
				$cls1='activate';
				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>'";
				$status='<li class="deactivation"><a href="#" class="deactive_users_profile" id="deactive_users_profile" data-profileid="'.$value["reg_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				
			}
			
				
			
			$action  .= $status;
			
			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

			

			 	$records["data"][] = array(
					$check,
					'REG'.$value['reg_id'].'</br>'.$created_at,
		  			$value['reg_username'],
		  			$value['reg_email'],
		  			$value['reg_mobile'],
		  			$value['name'].'</br>'.wordwrap($value['country_name'],10,"<br>\n"),
		  			
		  			$action,
		  			);

			 
							
	  		

	  	}

	  	 //  print_r($value['amount']);
			  // exit();

		
		//$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($records["data"]);
		$records["recordsFiltered"] = count($records["data"]);	
		echo json_encode($records);				  
		exit;		
	}



	public function download_multiple_users(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result'] = $this->Admin_dashboard_model->Subscriber_Data_download($array);
		// echo '<pre>';
		// print_r($data['subscriber']);
		// echo '</pre>';
		// exit;
		$this->load->view('admin_dashboard/export_users',$data);

	}

	public function deactivate_user()
	{	
		$reg_ids = $this->input->post('reg_ids');	
		$reg_ids_array = explode(",",$reg_ids);
		foreach($reg_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Inactive"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('registration',$deactive,array('reg_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}


	public function activate_user()
	{	
		$reg_ids = $this->input->post('reg_ids');	
		$reg_ids_array = explode(",",$reg_ids);
		foreach($reg_ids_array as $id)
		{
			$deactive = array(
						'status'	=> "Active"
						);

			$get_deactive_id = $this->Admin_dashboard_model->updateData('registration',$deactive,array('reg_id'=>$id));
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
	}
	
	public function interview_list(){
		//$post = $this->input->post();

		//$field_pos=array("createdat"=>'0',"promo_id"=>'1',"coupon_code"=>'2',"nature_of_code"=>'3',"description"=>'4',"status"=>'5',"validity"=>'6');	
		//$data['TotalRecord']=$this->Admin_dashboard_model->couponFilter($post,$field_pos,"",1);
		$this->load->view('admin_dashboard/interview/interview-list-view');
		
	}
	
	public function get_interview_details(){
		is_login_admin_panel();
		$post = $this->input->post();

		$field_pos=array("createdat"=>'0',"reg_id"=>'1',"reg_username"=>'2',"reg_mobile"=>'3',"reg_istrial"=>'4',"status"=>'5',"reg_designation"=>'6',"amount"=>'7');	

		//$sort_field=array_search(@$post['order'][0]['column'],$field_pos);

		/*if(@$post['order'][0]['dir']=='asc') {
	 		$orderBy="DESC";
		}else{
	 		$orderBy="DESC";
		}	*/


		//$TotalRecord=$this->Admin_dashboard_model->fetchUsers($post,$sort_field,$orderBy,0);	
	 	//$userData = $this->Admin_dashboard_model->fetchUsers($post,$sort_field,$orderBy,1);

	 	//$iTotalRecords = $TotalRecord['NumRecords'];

	 	//$records = array();
	  	//$records["data"] = array();
		
	  	//foreach ($userData as $key => $value) {
			
	  		/*$check='<input type="checkbox" id="filled-in-box'.$value.'" name="filled-in-box'.$value.'" class="filled-in purple users_bulk_action" value="'.$value.'"/><label for="filled-in-box'.$value.'"></label>';

	  		$created_at =  date("d-m-Y", strtotime($value['createdat']));

	  		if($value['reg_istrial']==0)
	  		{
	  			$reg_istrial = 'NO';
	  			

	  		}else{
	  			$reg_istrial = 'YES';
	  			$sub_status = 'NO';
	  		}*/


			$value="01";
			$status = "Active";
			$action = '';$status='';$cls='';$cls1='';$disable='';
 			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value."'><i class='action-dot'></i></div>

            <ul id='dropdown".$value."' class='dropdown-content'>";
			$li = "<li><a href=".base_url()."employee/edit_employee_master/".$value.'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>
			<li><a href="javascript:void(0);" onclick="email_employee_payment('.$value.');" ><i class="material-icons">email</i>Email</a></li>
			<li><a href='.base_url().'employee/download_multiple_employee/?ids='.$value.'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 21px;height: 20px;"><p class="ex">Export</p></a></li>
			<li class='.$disable.'><a href='.base_url().'employee/print_employee_master/'.$value.' target="_blank"><i class="material-icons dp48">print</i>Print</a></li>';
			
			if($status=="Active"){
				$action  .= $li;
			}
			$action  .= $status;

              $action  .= '</ul>

              <div class="status-action '.$cls1.'">

                <i class="action-status-icon"></i>

              </div>';
			
			$action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/edit_interview/".$value."'>EDIT</a></br><a style='font-size:12px;' class='action_link' href='".base_url()."admin_dashboard/delete_interview/".$value."'>DELETE</a></div>";

			$records["data"][] = array(
				//$check,
				"First Interview",
				"Nitesh",
				"Nitesh",
				"interview.mp4",
				$action_links,
			);

	  	//}

		///$records["recordsTotal"] = count($records["data"]);
		//$records["recordsFiltered"] = count($records["data"]);	
		echo json_encode($records);				  
		exit;		
	}

	public function add_interview(){

		$this->load->view('admin_dashboard/interview/add-interview');
	}


	public function edit_interview()
	{
		
		
		$post=$this->input->post();		
		if($post){
			$id = $post['id'];
			$post['code_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_date'])));
			$post['start_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_start_date'])));
			$post['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$post['coupon_end_date'])));
			$post['createdat'] = date("Y-m-d H:i:s");
			$post['updatedat'] = date("Y-m-d H:i:s");
			$post['discount']=$post['coupon_discount'];
			unset($post['coupon_discount']);
			
			$post['target_group']=$post['coupon_tg'];
			unset($post['id']);
			unset($post['coupon_tg']);
			unset($post['coupon_date']);
			unset($post['coupon_start_date']);
			unset($post['coupon_end_date']);
			$post['validity']=$post['coupon_validity'];
			unset($post['coupon_validity']);
			$post['description']=$post['coupon_desc'];
			unset($post['coupon_desc']);
			$post['referrel']=$post['referal_name'];
			unset($post['referal_name']);			
			$post['nature_of_code']=$post['coupon_nature'];
			unset($post['coupon_nature']);
			
			
			
			$copn_id=$this->Admin_dashboard_model->updateData('coupon_promo',$post,array("promo_id"=>$id));

			
		
			$this->session->set_flashdata('success',"Your Coupon code has been saved successfully");
			redirect('admin_dashboard/coupon_generator');
		}

		redirect('admin_dashboard/coupon_generator');
	}


	
	function reCaptcha($recaptcha){
		$secret = "6Ld_6L8ZAAAAAHFU42COviOd1xTky3IIPsdsMKvl";
		$ip = $_SERVER['REMOTE_ADDR'];

		$postvars = array("secret"=>$secret, "response"=>$recaptcha, "remoteip"=>$ip);
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$data = curl_exec($ch);
		curl_close($ch);

		return json_decode($data, true);
	}

}