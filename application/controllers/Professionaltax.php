<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class professionaltaxs extends Index_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Profile_model');
		$this->load->model('Account_model');

		is_login();

        is_accessAvail(1);
		is_trialExpire();
		 is_BusReg();


		$this->user_session = $this->session->userdata('user_session');
	}

	public function index()
	{
		 $this->load->view('receipts_payments/professional_tax/blank');

	}

  public function createprofessionaltax()
  {
     $this->load->view('receipts_payments/professional_tax/createprofessionaltax');

  }
	public function editprofessionaltax()
  {
     $this->load->view('receipts_payments/professional_tax/editprofessionaltax');

  }
	public function printpt()
  {
     print_r($_POST);
		 exit;

  }
	public function printptedit()
  {
     print_r($_POST);
		 exit;

  }
	public function proftaxlist()
  {
     $this->load->view('receipts_payments/professional_tax/proftaxlist');

  }
	public function proftaxview()
  {
     $this->load->view('receipts_payments/professional_tax/proftaxview');

  }
	public function get_pt_details(){
		//print_r("Hello");
		$check='<input type="checkbox" id="filled-in-box1" name="filled-in-box1" class="filled-in purple proxtax_bulk_action" value="01"/> <label for="filled-in-box1"></label>';
		$check1='<input type="checkbox" id="filled-in-box2" name="filled-in-box2" class="filled-in purple proxtax_bulk_action" value="02"/> <label for="filled-in-box2"></label>';

		$records["data"][0] = array(
			$check,
			"PAY001",
	  		"June",
	  		"ACT",
	  		"10,000",
	  		"CH-001",
	  		"PAID",
			"...",
	  	);
		$records["data"][1] = array(
			$check1,
			"PAY002",
	  		"July",
	  		"ACT",
	  		"5,000",
	  		"CH-002",
	  		"PAID",
			"...",
	  	);

		$records["recordsTotal"] = 2;
		$records["recordsFiltered"] = 10;
		echo json_encode($records);
		exit;
	}
}?>
