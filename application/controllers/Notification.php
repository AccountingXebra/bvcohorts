<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Notifications extends Index_Controller 
{
	function __construct(){
		//is_trialExpire();
		parent::__construct();
		$this->load->model('Sales_model');
		$this->load->model('User_model');

	}
	
public function index(){

}

public function first_video() {

				$chekUser= $this->Adminmaster_model->selectData('registration', '*', array('status'=>'Active'));

     foreach ($chekUser as $key => $value) {
 
         
            $createdDate=date('Y-m-d',strtotime($value->createdat."+365 days"));
            $videos=$this->Adminmaster_model->selectData('video', '*', array('reg_id'=>$value->reg_id));

            if(count($videos)==1){

            	 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = "Bharat Vaani";
			$subject = 'Congratulations on uploading your first ever video';
			$message=$this->load->view('email_template/first_video_upload',$data,true);
			sendEmail($from,$to,$subject,$message);


            }


        }
     	
     
	}

	public function weekly_email() {

				$chekUser= $this->Adminmaster_model->selectData('registration', '*', array('status'=>'Active'));

					$data['video'] = $this->Editorspick_model->getVideos();
      	$data['videoinsp'] = $this->Inspiringpeoples_model->getVideos();
      		$data['videolatest'] = $this->Inspiringpeoples_model->getLatestVideos();

      		
      			$data['videotrend'] = $this->Inspiringpeoples_model->getTrendVideos();

     foreach ($chekUser as $key => $value) {
 
         
            $createdDate=date('Y-m-d',strtotime($value->createdat."+365 days"));
            $videos=$this->Adminmaster_model->selectData('video', '*', array('reg_id'=>$value->reg_id));

           

            	 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = "Bharat Vaani";
			$subject = 'Alert! '.$data['cust_name'].' Check out these handpicked videos just for you!';
			$message=$this->load->view('email_template/top_pick_for_you_email',$data,true);
			sendEmail($from,$to,$subject,$message);


            


        }
     	
     
	}



	public function login_reminder() {

				$chekUser= $this->Adminmaster_model->selectData('registration', '*', array('status'=>'Active'));

        $data['videolatest'] = $this->Inspiringpeoples_model->getLatestVideos();

     foreach ($chekUser as $key => $value) {

     	    $login_record = $this->Adminmaster_model->selectData('login_record','*',array('reg_id' =>$value->reg_id),'login_id','DESC');	

           	 $createdDate=date('Y-m-d',strtotime($login_record[0]->created_at));
     	
     	$period_end_14=date('Y-m-d',strtotime($createdDate."+14days"));
     	$period_end_30=date('Y-m-d',strtotime($createdDate."+30days"));
     	$period_end_60=date('Y-m-d',strtotime($createdDate."+60days"));
 
         
           
           

            	 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = "Bharat Vaani";
			$subject = $data['cust_name'].' We miss you';
			$message=$this->load->view('email_template/miss_you_email',$data,true);

			if(date("Y-m-d")==$period_end_14 || date("Y-m-d")==$period_end_30 || date("Y-m-d")==$period_end_60){
				sendEmail($from,$to,$subject,$message);
			}
			


            


        }
     	
     
	}


  public function milestone_email() {

        $chekUser= $this->Adminmaster_model->selectData('registration', '*', array('status'=>'Active'));


     foreach ($chekUser as $key => $value) {

          $views = $this->Adminmaster_model->selectData('views','*',array('parent_id' =>$value->reg_id),'id','DESC');  
          
           $views_count=count($views);

               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = "Bharat Vaani";
    

      if($views_count==100 ){
          $subject = 'You Hit 100!';
      $message=$this->load->view('email_template/views_hundred_email',$data,true);
      sendEmail($from,$to,$subject,$message);
      }else if($views_count==10000){
           $subject = 'You Hit 10000!';
         $message=$this->load->view('email_template/views_10k_email',$data,true);
         sendEmail($from,$to,$subject,$message);
      }else if($views_count==50000){
           $subject = "You Hit 50000! Let's Celebrate";
         $message=$this->load->view('email_template/views_50k_email',$data,true);
         sendEmail($from,$to,$subject,$message);
      }else{

        //
      }
      

      
            


        }
      
     
  }

	
}