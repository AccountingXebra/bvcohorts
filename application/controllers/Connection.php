<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require ("common/Index_Controller.php");
class Connection extends Index_Controller {

	function __construct(){

		parent::__construct();
		//$this->load->model('Assets_model');

		$this->load->model('User_model');
		$this->load->model('Profile_model');
		$this->load->library('session');
		
		is_login();

        is_accessAvail(1);
		is_trialExpire();
		 is_BusReg();

		$this->user_session = $this->session->userdata('user_session');

	}
	
	public function welcome(){
		$this->load->view('connection/welcome_connection');
	}
	
	public function no_connection(){

		$reg_id=$this->user_session['reg_id'];
        $bus_id=$this->user_session['bus_id'];
        $gst_id=$this->user_session['ei_gst'];
         
		$data['conn'] =  $this->User_model->businessData('');
		
        //print_r($data['conn']);
        //exit();
		


		
		 /*$tracking_info['module_name'] = 'Company Expense';
		 $tracking_info['action_taken'] = 'Exported';
		 $tracking_info['reference'] = $id;
		 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);*/

		$this->load->view('connection/no-connection',$data);
	
		
	}
		
	

	public function sent_receive(){
		print_r('reached');
		exit();
		$this->load->view('connection/sent-receive-connection');
	}
	
	public function with_connection(){
		$this->load->view('connection/with-connection');
	}
	
	public function view_gift(){
		$this->load->view('connection/activate-gift');
	}

}