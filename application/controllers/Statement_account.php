<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Statements_account extends Index_Controller {

	function __construct(){

		parent::__construct();

		$this->load->model('Sales_model');
		$this->load->model('Statement_model');
		$this->load->model('RecPay_model');
		$this->load->model('User_model');
		$this->load->model('Profile_model');
		setlocale(LC_MONETARY, 'en_US.UTF-8'); 
		is_login();
		is_trialExpire();
		 is_BusReg();
		$this->user_session = $this->session->userdata('user_session');

	}

	public function index()
	{
		
		redirect('statement_account/statement_of_account');
		
	}

	public function statement_of_account()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$gst_id=$this->user_session['ei_gst'];
		
		$data['invoice']=$this->Sales_model->selectData('sales_invoices', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'cust_id'=>'57','inv_document_type'=>'Sales Invoice','status'=>'Active'),'inv_id','ASC'); 
		$data['srec_amount']=0;$data['inv_amount']=0;$data['cd_total']=0;
		$count=0;
		$count_cd=0;
		$bad_debts=0;
			$sreceipts=0;$inv_amount=0;$cd_total=0;
		if(count($data['invoice'])>0){
		$count=count($data['invoice']);
	

		foreach($data['invoice'] as $key => $value) {
			$inv_amount=$inv_amount+$value->inv_grant_total;
			$srec=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value->inv_id,'status'=>'Active'));

			for($i=0;$i<count($srec);$i++)
			{
				$sr=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$srec[$i]->srec_id,'status'=>'Active'));

				if($sr)
				{
					$count++;
					$sreceipts=$sreceipts+$sr[0]->srec_amount;

					if($sr[0]->srec_tds_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_tds_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_baddebts_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$bad_debts=$bad_debts+$sr[0]->srec_baddebts_amt;
			  				$count++;
			  			}
						
						if($sr[0]->srec_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count--;
			  			}
				}	

			}

			$cd_note=$this->Sales_model->selectData('sales_credit_debit', 'cd_grant_total', array('cd_invoice_no'=>$value->inv_invoice_no,'cust_id'=>$value->cust_id,'status'=>'Active'));
			for($i=0;$i<count($cd_note);$i++)
			{
				$count_cd++;
				$cd_total=$cd_total+$cd_note[$i]->cd_grant_total;
			}
			
		}
		} 

		$data['inv_amount']=$inv_amount;
		$data['sreceipts']=$sreceipts;
		$data['cd_total']=$cd_total;
		$data['bad_debts']=$bad_debts;

         //print_r($bad_debts);
         //exit();

  /////////////////////////////////////////////////////////////////

		$data['expense']=$this->Sales_model->selectData('company_expense', '*', array('bus_id'=>$bus_id),'exp_id','ASC'); 
		$data['pay_amount']=0;$data['exp_amount']=0;$data['db_total']=0;
		$count_1=0;
		$count_cd_1=0;
		if(count($data['expense'])>0){
		$count_1=count($data['expense']);
		$pay_amount=0;$exp_amount=0;$db_total=0;

		foreach($data['expense'] as $key => $value) {
			$exp_amount=$exp_amount+$value->ce_grandtotal;
			$pay=$this->Sales_model->selectData('expense_payment_list', '*', array('epayl_exp_no'=>$value->ce_number,'status'=>'Active'));

			for($i=0;$i<count($pay);$i++)
			{
				$sr=$this->Sales_model->selectData('expense_payments', '*', array('epay_id'=>$pay[$i]->epay_id,'status'=>'Active'));

				if($sr)
				{
					$count_1++;
					$pay_amount=$pay_amount+$sr[0]->epay_amount;

					if($sr[0]->epay_tds_amt > 0)
			  			{
			  				$pay_amount=$pay_amount+$sr[0]->epay_tds_amt;
			  				$count_1++;
			  			}

			  			
						
						if($sr[0]->epay_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count_1--;
			  			}
				}	

			}

			$db_note=$this->Sales_model->selectData('expense_debit_note', 'debit_grant_total', array('debit_invoice_no'=>$value->ce_number,'vendor_id'=>$value->ce_vendorname,'status'=>'Active'));
			for($i=0;$i<count($db_note);$i++)
			{
				$count_cd_1++;
				$db_total=$db_total+$db_note[$i]->debit_grant_total;
			}
			
		}
		} 

		$data['exp_amount']=$exp_amount;
		$data['pay_amount']=$pay_amount;
		$data['db_total']=$db_total;

///////////////////////////////////////////////////////////////////		


		$data['count']=$count+$count_cd+$count_1+$count_cd_1;
		$this->load->view('soa/statement-of-account',$data);
		
	
	}
	public function get_soa(){
		$post = $this->input->post();

		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		//$post['cust_id']=$this->session->userdata['client_session']['cust_id'];
		
		$field_pos=array("inv_id"=>'0',"inv_invoice_date"=>'1',"inv_invoice_no"=>'2',"inv_grant_total"=>'4');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Statement_model->MyinvoicesFilterSOA($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Statement_model->MyinvoicesFilterSOA($post,$sort_field,$orderBy,1);
	 	// echo '<pre>';
	 	// print_r($userData);
	 	// echo '</pre>';
	 	// exit;
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		$count=0;$balance_amt=0;
	  	foreach ($userData as $key => $value) {
	  		$count++;
	  		$check='';
	  			$cust_name=$value['cust_name'];
	  			
				$inv_invoice_no='';
				if($value['inv_invoice_no']!='')
				{
					
					$inv_invoice_no =$value['inv_invoice_no'];
					
				}
				$inv_date='';
				if($value['inv_invoice_date']!='')
				{
					
					$dateTime = date("d-m-Y",  strtotime($value['inv_invoice_date']));
					if($value['inv_invoice_date']=='0000-00-00'){
						$dateTime='';
					}
					$inv_date = str_replace("-","/",$dateTime);
				}
			

		
			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="active_sales_invoice" data-inv_id='.$value["inv_id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{
				
				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_sales_invoice" data-inv_id="'.$value["inv_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				$cls='active_record';
				$cls1='activate';
				
			}
			
			//$str="edit-";
			//$str.=strtolower(str_replace(" ","-",$value['inv_document_type']));
			//$str+=strtolower(str_replace("-"," ",$value['inv_document_type']));
			
			/*$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["inv_id"]."'><i class='action-dot'></i></div>

			 	<ul id='dropdown".$value["inv_id"]."' class='dropdown-content'>
					".'
					<li><a href="javascript:void(0);" onclick="email_cinvoice('.$value["inv_id"].');"><i class="material-icons">email</i>Email</a></li>
                    <li><a href='.base_url().'statement_account/download-cinvoice/'.$value["inv_id"].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;height: 20px;">Export</a></li>
					<li><a href='.base_url().'statement_account/print-cinvoice/'.$value["inv_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>
				</ul>';*/
			$balance_amt=$balance_amt+$value['inv_grant_total'];
		
	  		$records["data"][] = array(
				//$check,
	  			$inv_date,
	  			$inv_invoice_no,
	  			'',
	  			round($value['inv_grant_total']),
	  			'NA',
	  			'NA',
	  			round($balance_amt),
	  			
	  			);

	  		
	  		// Creadit Note

	  		$cd_note = $this->Sales_model->selectData('sales_credit_debit', '*', array('cd_invoice_no'=>$value['inv_invoice_no'],'status'=>'Active','cust_id'=>$value['cust_id']));	

	  		for($i = 0; $i < count($cd_note); $i++)
	  		{
	  			if(count($cd_note)>0){
	  			
		  			// echo '<pre>';
		  			// print_r($cd_note);
		  			// echo '</pre>';
		  			// exit();
		  			
		  			$scheck='<input type="checkbox" id="filled-in-box'.$cd_note[$i]->cd_id.'" name="filled-in-box'.$cd_note[$i]->cd_id.'" data-type="cd_note" class="filled-in purple soa_bulk_action" value="'.$cd_note[$i]->cd_id.'"/> <label for="filled-in-box'.$cd_note[$i]->cd_id.'"></label>';

		  			$cd_note_date='';

		  			if($cd_note[$i]->cd_note_date !='')
					{
		
						$dateTime = date("d-m-Y",  strtotime($cd_note[$i]->cd_note_date));
						if($cd_note[$i]->cd_note_date=='0000-00-00'){
							$dateTime='';
						}
						$cd_note_date = str_replace("-","/",$dateTime);
			
					}
			
					$balance_amt=$balance_amt-$cd_note[$i]->cd_grant_total;
		  			$records["data"][] = array(
					//$scheck,
		  			$cd_note_date,
		  			$cd_note[$i]->cd_note_no,
		  			'CREDIT NOTE FOR '.$value['inv_invoice_no'],
		  			round($value['inv_grant_total']),
		  			round($cd_note[$i]->cd_grant_total),
		  			'',
		  			number_format((float)$balance_amt, 0, '.', ''),
		  				
		  			);
	  	
	  			}
	  		}

	  		
            // Debit Note

	  		$db_note = $this->Sales_model->selectData('expense_debit_note', '*', array('debit_invoice_no'=>$value['inv_invoice_no'],'status'=>'Active','vendor_id'=>$value['cust_id']));	

	  		for($i = 0; $i < count($db_note); $i++)
	  		{
	  			if(count($db_note)>0){
	  			
		  			// echo '<pre>';
		  			// print_r($cd_note);
		  			// echo '</pre>';
		  			// exit();
		  			
		  			$scheck='<input type="checkbox" id="filled-in-box'.$db_note[$i]->debit_id.'" name="filled-in-box'.$db_note[$i]->debit_id.'" data-type="cd_note" class="filled-in purple soa_bulk_action" value="'.$db_note[$i]->debit_id.'"/> <label for="filled-in-box'.$db_note[$i]->debit_id.'"></label>';

		  			$debit_note_date='';

		  			if($db_note[$i]->debit_note_date !='')
					{
		
						$dateTime = date("d-m-Y",  strtotime($db_note[$i]->debit_note_date));
						if($db_note[$i]->debit_note_date=='0000-00-00'){
							$dateTime='';
						}
						$debit_note_date = str_replace("-","/",$dateTime);
			
					}
			
					$balance_amt=$balance_amt-$db_note[$i]->debit_grant_total;
		  			$records["data"][] = array(
					//$scheck,
		  			$debit_note_date,
		  			$db_note[$i]->debit_note_no,
		  			'DEBIT NOTE FOR '.$value['inv_invoice_no'],
		  			round($value['inv_grant_total']),
		  			round($db_note[$i]->debit_grant_total),
		  			'',
		  			number_format((float)$balance_amt, 0, '.', ''),
		  				
		  			);
	  	
	  			}
	  		}
	  	
	  		// Sales Recipts
	  		//if($count%2!=0){
	  			$sreceipts=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value['inv_id'],'status'=>'Active'));
	  			
	  			for($i=0; $i< count($sreceipts); $i++)
	  			{
	  				if(count($sreceipts)>0)
			  		{
			  			
			  			$srec=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$sreceipts[$i]->srec_id,'status'=>'Active'));	
			  			
			  			if($srec)
			  			{
			  			
			  			$scheck='<input type="checkbox" id="filled-in-box'.$sreceipts[$i]->srecl_id.'" name="filled-in-box'.$sreceipts[$i]->srecl_id.'" data-type="srec" class="filled-in purple soa_bulk_action" value="'.$sreceipts[$i]->srecl_id.'"/> <label for="filled-in-box'.$sreceipts[$i]->srecl_id.'"></label>';
			  			$srec_date='';

			  			if(@$srec[0]->srec_date )

			  			if(@$srec[0]->srec_date !='')
						{
			
							$dateTime = date("d-m-Y",  strtotime($srec[0]->srec_date));
							if($srec[0]->srec_date=='0000-00-00'){
								$dateTime='';
							}
							$srec_date = str_replace("-","/",$dateTime);
				
						}

						if($srec[0]->srec_tds_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_tds_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'TDS FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_tds_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  			if($srec[0]->srec_baddebts_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_baddebts_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'BADDEBTS FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_baddebts_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  			if($srec[0]->srec_amount>0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_amount;
				  			$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'SALES RECEIPT FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_amount),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  		
			  			}	

			  		}
	  			}


              // Expense Payment
	  		//if($count%2!=0){
	  			$exppay=$this->Sales_model->selectData('expense_payment_list', '*', array('epayl_exp_no'=>$value['inv_invoice_no'],'status'=>'Active'));
	  			
	  			for($i=0; $i< count($exppay); $i++)
	  			{
	  				if(count($exppay)>0)
			  		{
			  			
			  			$srec=$this->Sales_model->selectData('expense_payments', '*', array('epay_id'=>$exppay[$i]->epay_id,'status'=>'Active'));	
			  			
			  			if($srec)
			  			{
			  			
			  			$scheck='<input type="checkbox" id="filled-in-box'.$exppay[$i]->epayl_id.'" name="filled-in-box'.$exppay[$i]->epayl_id.'" data-type="srec" class="filled-in purple soa_bulk_action" value="'.$exppay[$i]->epayl_id.'"/> <label for="filled-in-box'.$exppay[$i]->epayl_id.'"></label>';
			  			$srec_date='';

			  			if(@$srec[0]->epayl_exp_date )

			  			if(@$srec[0]->epayl_exp_date !='')
						{
			
							$dateTime = date("d-m-Y",  strtotime($srec[0]->epay_date));
							if($srec[0]->epayl_exp_date=='0000-00-00'){
								$dateTime='';
							}
							$srec_date = str_replace("-","/",$dateTime);
				
						}

						if($srec[0]->epay_tds_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->epay_tds_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->epay_code,
				  			'TDS FOR '.$value['inv_invoice_no'],
				  			round($exppay[$i]->epayl_exp_amt),
				  			'',
				  			round($srec[0]->epay_tds_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  			/*if($srec[0]->srec_baddebts_amt > 0)
			  			{
			  				$balance_amt=$balance_amt-$srec[0]->srec_baddebts_amt;
			  				$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->srec_code,
				  			'BADDEBTS FOR '.$value['inv_invoice_no'],
				  			round($sreceipts[$i]->srecl_inv_amt),
				  			'',
				  			round($srec[0]->srec_baddebts_amt),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}*/

			  			if($srec[0]->epay_amount>0)
			  			{   

			  				$balance_amt=$balance_amt-$srec[0]->epay_amount;
				  			$records["data"][] = array(
							//$scheck,
				  			$srec_date,
				  			$srec[0]->epay_code,
				  			'PAYMENT FOR '.$value['inv_invoice_no'],
				  			round($exppay[$i]->epayl_exp_amt),
				  			'',
				  			round($srec[0]->epay_amount),
				  			number_format((float)$balance_amt, 0, '.', ''),
				  				
				  			);
			  			}

			  		
			  			}	

			  		}
	  			}





	  	
	  		//}
	  	}


	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;	

		//----------------------------------------------------------------------------

		$res = $records["data"];
		function date_compare($a, $b)
		{
		    $t1 = strtotime(str_replace("/","-",$a[0]));
		    $t2 = strtotime(str_replace("/","-",$b[0]));
		    return $t1 - $t2;
		}    
		usort($res, 'date_compare');

		$bal = 0;
		for($i=0;$i<count($res);$i++)
		{
			
			if($res[$i][5] == 'NA' && $res[$i][4] == 'NA')
			{
				$res[$i][6] = $bal + $res[$i][3];
				$bal = $res[$i][6];
				$res[$i][5] = '';
				$res[$i][4] = '';

			}elseif ($res[$i][5] == '' && $res[$i][4] != '') {
				
				$res[$i][6] = $bal - $res[$i][4];
				$bal = $res[$i][6];

			}elseif ($res[$i][5] != '' && $res[$i][4] == '') {
				
				$res[$i][6] = $bal - $res[$i][5];
				$bal = $res[$i][6];
			}
		}

		// echo strtotime(str_replace("/","-",$res[0][0]));
		// echo '<br>';
		// echo strtotime(str_replace("/","-",@$post['inv_start_date']));
		// exit;

		// echo '<pre>';
		// print_r($res);	
		// echo '</pre>';
		// exit;
		// print_r($post['inv_start_date']);
		// echo '<br>';
		// print_r($post['inv_end_date']); exit;

		$newres = array();
		if(@$post['inv_start_date'] != '' && @$post['inv_end_date'] != '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) >= strtotime(str_replace("/","-",$post['inv_start_date'])) && strtotime(str_replace("/","-",$res[$i][0])) <= strtotime(str_replace("/","-",$post['inv_end_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}elseif(@$post['inv_start_date'] != '' && @$post['inv_end_date'] == '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) >= strtotime(str_replace("/","-",$post['inv_start_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}elseif(@$post['inv_start_date'] == '' && @$post['inv_end_date'] != '')
		{
			$j=0;
			for($i=0;$i<count($res);$i++)
			{
				if(strtotime(str_replace("/","-",$res[$i][0])) <= strtotime(str_replace("/","-",$post['inv_end_date'])))
				{
					$newres[$j][0] = $res[$i][0];
					$newres[$j][1] = $res[$i][1];
					$newres[$j][2] = $res[$i][2];
					$newres[$j][3] = $res[$i][3];
					$newres[$j][4] = $res[$i][4];
					$newres[$j][5] = $res[$i][5];
					$newres[$j][6] = $res[$i][6];
					$j++;
				}

			}
		}
		


		//----------------------------------------------------------------------------
		if(@$post['inv_start_date'] != '' || @$post['inv_end_date'] != '')
		{
			$records["data"] = $newres;
		}else{
			$records["data"] = $res;
		}
		
		// echo '<pre>';
		// print_r($records["data"]);	
		// echo '</pre>';
		// exit;
		echo json_encode($records);				  
		exit;
	}
	
	/*---START CUSTOMER INVOICE---*/
	public function my_invoices()
	{

		//echo "<pre>";print_r($this->session->userdata());exit;
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		//$post['cust_id']=$this->session->userdata['client_session']['cust_id'];
		$cust_id = $this->session->userdata['client_session']['cust_id'];
		
		$data['invoice']=$this->Sales_model->selectData('sales_invoices', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'cust_id'=>$cust_id),'','','','','','rowcount'); 
		//if($data['invoice']>0){
		$data['customers']     = $this->Sales_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		
		$this->load->view('statement_account/invoices-list',$data);
		//} 
	
	}
	public function check_client_exist()
	{
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$result=$this->Sales_model->selectData('sales_customers', '*', array(	'cust_name'=>trim($this->input->get('cust_name'))));
		if(count($result)>0){
			echo json_encode('true');
			
		}else{
			echo json_encode('Client Name Not Exist..Please Enter Another..!');
			
		}
	}
	public function get_customer_invoices(){
		$post = $this->input->post();
		$post['reg_id']=$this->session->userdata['client_session']['reg_id'];
		$post['bus_id']=$this->session->userdata['client_session']['bus_id'];
		$post['cust_id']=$this->session->userdata['client_session']['cust_id'];
		
		$field_pos=array("inv_id"=>'0',"inv_invoice_no"=>'1',"inv_invoice_date"=>'2',"cust_name"=>'3',"state_name"=>'4',"country_name"=>'5',"inv_grant_total"=>'6',"inv_status"=>'7',"inv_attachment"=>'8');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Client_model->MyinvoicesFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Client_model->MyinvoicesFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['inv_id'].'" name="filled-in-box'.$value['inv_id'].'" class="filled-in purple cinvoice_bulk_action" value="'.$value['inv_id'].'"/> <label for="filled-in-box'.$value['inv_id'].'"></label>';
	  			$cust_name=$value['cust_name'];
				if($value['country_name'] != '' &&  $value['state_name'] != ''){
					$cust_name .= '<div class="row"><div class="col s12 m12 l12"><small class="text-gray">'.$value['state_name'].', '.$value['country_name'].'</small></div></div>';
					}	
	  			
				$inv_invoice_no='';
				if($value['inv_invoice_no']!='')
				{
					
					$inv_invoice_no =$value['inv_invoice_no'];
					$inv_invoice_no.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray">'.$value['inv_document_type'].'</small></div></div>';
				}
				$inv_invoice_no='<a href="'.base_url().'statement_account/view-cinvoice/'.$value['inv_id'].'" target="_blank">'.$inv_invoice_no.'</a>';
				$inv_date='';
				if($value['inv_invoice_date']!='')
				{
					
					$dateTime = date("d-m-Y",  strtotime($value['inv_invoice_date']));
					if($value['inv_invoice_date']=='0000-00-00'){
						$dateTime='';
					}
					$inv_date = str_replace("-",".",$dateTime);
				}
				$attachment="";
				if($value['inv_document_type'] =="Sales Invoice"){
				$attachment='<a href="javascript:void(0);"><img class="deletes" src="'.base_url().'asset/css/img/icons/attach.png"></a><span class="green-c">'.$value['inv_attachment'].'</span>';
				}
			
			$status_inv='<span class="red-c">'.$value['inv_status'].'</span>';
			$grant_total=$value['currencycode'].' '.$value['inv_grant_total'];
			if($value['inv_status']=="Full Received" || $value['inv_status']=="Paid"){
				$status_inv='<span class="green-c">'.$value['inv_status'].'</span>';
				$grant_total=$value['currencycode'].' '.$value['inv_grant_total'];
			}
			if($value['inv_status']=="Part Received"){
				$status_inv='<a href="'.base_url().'receipts-payments/add-sales-receipt" target="_blank">'.$status_inv.'</a>';
			}
			//$grant_total=$value['currencycode'].' '.$value['inv_grant_total'];
			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="active_sales_invoice" data-inv_id='.$value["inv_id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{
				
				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_sales_invoice" data-inv_id="'.$value["inv_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				$cls='active_record';
				$cls1='activate';
				
			}
			$str="edit-";
			$str.=strtolower(str_replace(" ","-",$value['inv_document_type']));
			//$str+=strtolower(str_replace("-"," ",$value['inv_document_type']));
			$conv_to_inv='';
			if($value['inv_document_type']=="Estimate Invoice"){
			$conv_to_inv='<li><a href="javascript:voice(0);"><img class="icon-img" src="'.base_url().'public/icons/convert_invoice.png">Convert to Invoice</a></li>';
			}
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["inv_id"]."'><i class='action-dot'></i></div>

			 	<ul id='dropdown".$value["inv_id"]."' class='dropdown-content'>
					".'
					<li><a href="javascript:void(0);" onclick="email_cinvoice('.$value["inv_id"].');"><i class="material-icons">email</i>Email</a></li>
                    <li><a href='.base_url().'statement_account/download-cinvoice/'.$value["inv_id"].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;height: 20px;">Export</a></li>
					<li><a href='.base_url().'statement_account/print-cinvoice/'.$value["inv_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>
				</ul>';
		
	  		$records["data"][] = array(
				$check,
	  			$inv_invoice_no,
	  			$inv_date,
	  			$cust_name,
	  			$grant_total,
	  			$status_inv,
	  			$attachment,
				/*$action_ed,*/
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}
	public function download_multiple_cinvoices(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['invall'] = $this->Client_model->export_customer_invoice($array,$reg_id,$bus_id,$cust_id);
		$this->load->view('sales/invoice/export_all_invoice',$data);
	}
	public function print_multiple_cinvoices(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		
		$data['invall'] = $this->Client_model->export_customer_invoice($array,$reg_id,$bus_id,$cust_id);
		$this->load->view('sales/invoice/print_all_invoice',$data);
	}
	public function print_cinvoice($id){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$sales_inv = $this->Sales_model->selectData('sales_invoices','inv_document_type',array("inv_id"=>$id));
		$invoice_type="sales_invoices_list";
		$invoice_type="sales_invoices_list";
		if($sales_inv[0]->inv_document_type=="Estimate Invoice"){
			$invoice_type="sales_estimate_list";
		}else if($sales_inv[0]->inv_document_type=="Proforma Invoice"){
			$invoice_type="sales_proforma_invoices_list";
		}
		$data['billing_doc'] = $this->Sales_model->salesInvoiceDetails($invoice_type,$id,$reg_id,$bus_id,0,$cust_id);
		$data['company'] = $this->Sales_model->selectData('businesslist','bus_id,bus_company_logo,bus_cin_no,bus_pancard,gst_id,bus_company_name',array("bus_id"=>$bus_id));
		$data['my_services']   = $this->Sales_model->selectData('services', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id),'service_id','ASC');
		
		$data['user'] = $this->Sales_model->selectData('registration','*',array("reg_id"=>$reg_id));
		$data['gstno_cli']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->inv_gst_id,'status'=>'Active'));
		$data['gstno']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->gst_id,'status'=>'Active'));
		$data['cust_pan']= $this->Sales_model->selectData('sales_customers','*',array("cust_id"=>$data['billing_doc'][0]->cust_id));
		if($data['billing_doc'][0]->inv_billing_country==101){
		$data['placeSupply']= $this->Sales_model->selectData('states','state_name,state_gst_code',array("state_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
		$data['place']=$data['placeSupply'][0]->state_name."-".$data['placeSupply'][0]->state_gst_code;
		}else{
			$data['placeSupply']= $this->Sales_model->selectData('countries','country_name',array("country_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
				$data['place']=$data['placeSupply'][0]->country_name;

		}
		$data['custom_inv'] = $this->Sales_model->selectData('customise_invoices','*',array("inv_id"=>$id));
		
		 $currency_info	   = $this->Sales_model->selectData('currency', '*','','currency_id','ASC');
         $data['currency']=$currency_info;
         $data['currencycode'] = $this->Sales_model->selectData('currency','*',array("currency_id"=>$data['billing_doc'][0]->inv_export_currency));
		$this->load->view('sales/invoice/pinvoice',$data);
	
	}
	
	public function download_cinvoice($id){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];

		$sales_inv = $this->Sales_model->selectData('sales_invoices','inv_document_type',array("inv_id"=>$id));
		$invoice_type="sales_invoices_list";
		$invoice_type="sales_invoices_list";
		if($sales_inv[0]->inv_document_type=="Estimate Invoice"){
			$invoice_type="sales_estimate_list";
		}else if($sales_inv[0]->inv_document_type=="Proforma Invoice"){
			$invoice_type="sales_proforma_invoices_list";
		}
		$data['billing_doc'] = $this->Sales_model->salesInvoiceDetails($invoice_type,$id,$reg_id,$bus_id,'0',$cust_id);
		$data['company'] = $this->Sales_model->selectData('businesslist','bus_id,bus_company_logo,bus_cin_no,bus_pancard,gst_id,bus_company_name',array("bus_id"=>$bus_id));
		$data['my_services']   = $this->Sales_model->selectData('services', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id),'service_id','ASC');
		$data['gstno']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->gst_id));
		$data['gstno_cli']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->inv_gst_id));

		$data['cust_pan']= $this->Sales_model->selectData('sales_customers','*',array("cust_id"=>$data['billing_doc'][0]->cust_id));
		if($data['billing_doc'][0]->inv_billing_country==101){
		$data['placeSupply']= $this->Sales_model->selectData('states','state_name,state_gst_code',array("state_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
		$data['place']=$data['placeSupply'][0]->state_name."-".$data['placeSupply'][0]->state_gst_code;
		}else{
			$data['placeSupply']= $this->Sales_model->selectData('countries','country_name',array("country_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
				$data['place']=$data['placeSupply'][0]->country_name;

		}
		$data['custom_inv'] = $this->Sales_model->selectData('customise_invoices','*',array("inv_id"=>$id));
		
		 $currency_info	   = $this->Sales_model->selectData('currency', '*','','currency_id','ASC');
         $data['currency']=$currency_info;
         $data['currencycode'] = $this->Sales_model->selectData('currency','*',array("currency_id"=>$data['billing_doc'][0]->inv_export_currency));
		$html_data=$this->load->view('sales/invoice/download_inv',$data,true);
		$inv_name=str_replace('/','-',$data['billing_doc'][0]->inv_invoice_no);
		$this->pdf->create($html_data,$inv_name,'download');
	
	}
	public function email_cinvoice(){

		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];

		$post_data = $_POST;
		$ids = explode(',',$post_data['invoice_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			$sales_inv = $this->Sales_model->selectData('sales_invoices','inv_document_type',array("inv_id"=>$ids[$i]));
			$invoice_type="sales_invoices_list";
			$invoice_type="sales_invoices_list";
			if($sales_inv[0]->inv_document_type=="Estimate Invoice"){
				$invoice_type="sales_estimate_list";
			}else if($sales_inv[0]->inv_document_type=="Proforma Invoice"){
				$invoice_type="sales_proforma_invoices_list";
			}
			$data['billing_doc'] = $this->Sales_model->salesInvoiceDetails($invoice_type,$ids[$i],$reg_id,$bus_id,'0',$cust_id);
			$data['company'] = $this->Sales_model->selectData('businesslist','bus_id,bus_company_logo,bus_cin_no,bus_pancard,gst_id,bus_company_name',array("bus_id"=>$bus_id));
			$data['my_services']   = $this->Sales_model->selectData('services', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id),'service_id','ASC');
			$data['gstno']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->gst_id));
			$data['gstno_cli']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->inv_gst_id));

			$data['cust_pan']= $this->Sales_model->selectData('sales_customers','*',array("cust_id"=>$data['billing_doc'][0]->cust_id));
			if($data['billing_doc'][0]->inv_billing_country==101){
			$data['placeSupply']= $this->Sales_model->selectData('states','state_name,state_gst_code',array("state_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
			$data['place']=$data['placeSupply'][0]->state_name."-".$data['placeSupply'][0]->state_gst_code;
			}else{
				$data['placeSupply']= $this->Sales_model->selectData('countries','country_name',array("country_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
					$data['place']=$data['placeSupply'][0]->country_name;

			}
			$data['custom_inv'] = $this->Sales_model->selectData('customise_invoices','*',array("inv_id"=>$ids[$i]));
			
			 $currency_info	   = $this->Sales_model->selectData('currency', '*','','currency_id','ASC');
	         $data['currency']=$currency_info;
	         $data['currencycode'] = $this->Sales_model->selectData('currency','*',array("currency_id"=>$data['billing_doc'][0]->inv_export_currency));


			//$invoname= 	$data['billing_doc'][0]->inv_invoice_no;	

			$html_data=$this->load->view('sales/invoice/download_inv',$data,true);

			//$attach[$i]=$this->pdf->create($html_data,$invoname.'.pdf','email');
			$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
			$invoname=str_replace('/','-',$data['billing_doc'][0]->inv_invoice_no)."_".time();
			$downld=$this->pdf->create($html_data,$invoname,'download_multi');
			$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
			$attach[$i]=$path;

		}


		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);


		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];
		$from = $fromemail[0]->bus_company_name;
		//$to =  $data['email_to'];
		$subject = $post_data['email_subject'];
		//$message=$this->load->view('email_template/subscription_detail',$data,true);

		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$attach,$invoname,$cc);
		for($i=0; $i < count($attach); $i++)
		{
			unlink($attach[$i]);
		}

		if($status)
		{
			echo 1;

		}else{
			
			echo 0;
		}
		
		
	
	}

	public function email_multiple_cinvoices(){
		$array=$this->input->post('sales_values');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		
		$data['invall'] = $this->Client_model->export_customer_invoice($array,$reg_id,$bus_id,$cust_id);
		$html_data=$this->load->view('sales/invoice/print_all_invoice',$data,true);
		$inv_name='Customise Invoices';
		$output = $this->pdf->create($html_data,$inv_name,'email');
		$msg ="Hey ".ucwords($this->session->userdata['client_session']['client_username']).", Congratulations! You have successfully create Customise Invoices.";
		sendInvoiceEmail($this->session->userdata['client_session']['client_email'],'Your Customise Invoices has been created',$msg,$output,$inv_name);
	}
	public function view_cinvoice($id){
		
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		
	
		$sales_inv = $this->Sales_model->selectData('sales_invoices','inv_document_type',array("inv_id"=>$id,"cust_id"=>$cust_id,'bus_id'=>$bus_id,'reg_id'=>$reg_id));
		if($sales_inv==false){
			redirect('statement_account/my-invoices');
		}
		$invoice_type="sales_invoices_list";
		if($sales_inv[0]->inv_document_type=="Estimate Invoice"){
			$invoice_type="sales_estimate_list";
		}else if($sales_inv[0]->inv_document_type=="proforma Invoice"){
			$invoice_type="sales_proforma_invoices_list";
		}
		$data['billing_doc'] = $this->Sales_model->salesInvoiceDetails($invoice_type,$id,$reg_id,$bus_id,0,$cust_id);
		
		$data['company'] = $this->Sales_model->selectData('businesslist','bus_id,bus_company_logo,bus_cin_no,bus_pancard,gst_id,bus_company_name',array("bus_id"=>$bus_id));
		$data['my_services']   = $this->Sales_model->selectData('services', '*',array('bus_id'=>$bus_id),'service_id','ASC');		
		$data['gstno']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->gst_id));
		$data['gstno_cli']= $this->Sales_model->selectData('gst_number','*',array("gst_id"=>$data['billing_doc'][0]->inv_gst_id));
		$data['cust_pan']= $this->Sales_model->selectData('sales_customers','*',array("cust_id"=>$data['billing_doc'][0]->cust_id));
		if($data['billing_doc'][0]->inv_billing_country==101){
		$data['placeSupply']= $this->Sales_model->selectData('states','state_name,state_gst_code',array("state_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
		$data['place']=$data['placeSupply'][0]->state_name."-".$data['placeSupply'][0]->state_gst_code;
		}else{
			$data['placeSupply']= $this->Sales_model->selectData('countries','country_name',array("country_id"=>$data['billing_doc'][0]->inv_place_of_supply));	
				$data['place']=$data['placeSupply'][0]->country_name;

		}
		
		$data['user'] = $this->Sales_model->selectData('registration','*',array("reg_id"=>$reg_id));

		$data['custom_inv'] = $this->Sales_model->selectData('customise_invoices','*',array("inv_id"=>$id));

		 $currency_info	   = $this->Sales_model->selectData('currency', '*','','currency_id','ASC');
         $data['currency']=$currency_info;
         $data['currencycode'] = $this->Sales_model->selectData('currency','*',array("currency_id"=>$data['billing_doc'][0]->inv_export_currency));

		$this->load->view('statement_account/view-cinvoice',$data);
		
	}
	/*---END CUSTOMER INVOICE---*/
	/*---START CUSTOMER PAYMENT MADE---*/
	public function receipts()
	{
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['preceipts']=$this->Sales_model->selectData('sales_receipts', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'cust_id'=>$cust_id),'','','','','','rowcount');
		
		$data['customers']=$this->Sales_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$this->load->view('statement_account/payment-made',$data);
	
	}
	public function get_payment_made(){
		$post = $this->input->post();
		$post['reg_id']=$this->session->userdata['client_session']['reg_id'];
		$post['bus_id']=$this->session->userdata['client_session']['bus_id'];
		$post['cust_id']=$this->session->userdata['client_session']['cust_id'];
		
		$field_pos=array("srec_id"=>'0',"cust_name"=>'1',"state_name"=>'2',"country_name"=>'3',"inv_invoice_no"=>'4',"srec_amount"=>'5',"srec_tds_amt"=>'6',"srec_status"=>'8');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Client_model->paymentFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Client_model->paymentFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
	  	foreach ($userData as $key => $value) {
            $receipt_lists=$this->RecPay_model->selectData('sales_receipts_list', '*', array('srec_id'=>$value['srec_id']));
	  		$check='<input type="checkbox" id="filled-in-box'.$value['srec_id'].'" name="filled-in-box'.$value['srec_id'].'" class="filled-in purple payment_bulk_action" value="'.$value['srec_id'].'"/> <label for="filled-in-box'.$value['srec_id'].'"></label>';
  			 $srec_code='';
           $srec_date='';
           $invNo="";
           $custName="";
           $invAmt="";
	  		foreach($receipt_lists as $receipt_list){
             // print_r($receipt_list->srec_id);

             
				if($value['srec_date']!='')
				{
					
					$dateTime = date("d M, Y",  strtotime($value['srec_date']));
					if($value['srec_date']=='0000-00-00'){
						$dateTime='';
					}
					$srec_date .= $dateTime;
				}
				
				if($value['srec_code']!='')
				{
					
					$srecc_code =$value['srec_code'];
					$srecc_code.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$dateTime.'</b></small></div></div>';
					$srec_code.='<a href="javascript:void(0)" >'.$srecc_code.'</a></br>';
				}

				

                 $receipts_lists=$this->RecPay_model->selectData('sales_invoices', '*', array('inv_id'=>$receipt_list->srecl_inv_no));

				$invNo.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$receipts_lists[0]->inv_invoice_no.'</b></small></div></div></br>';

				$custName.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$value['cust_name'].'</b></small></div></div></br>';
				$invAmt.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$receipt_list->srecl_inv_amt.'</b></small></div></div></br>';
				
				
	  		}
	  			

			$srec_status='<span class="red-c">'.ucwords(str_replace('_', ' ', $value['srec_status'])).'</span>';
			
			if($value['srec_status']=="part_received"){
				$srec_status='<span class="red-c">Part Received</span>';
				
			}else if($value['srec_status']=="full_received"){
				$srec_status='<span class="green-c">Full Received</span>';
			}
			
			$action = '';$cls='';
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["srec_id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["srec_id"]."' class='dropdown-content'>
					".'<li><a href="javascript:void(0);" onclick="email_payment('.$value["srec_id"].');"><i class="material-icons">email</i>Email</a></li>
                    <li><a href='.base_url().'statement_account/download-payment-made/'.$value["srec_id"].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;height: 20px;">Export</a></li>
					<li><a href='.base_url().'statement_account/print-payment-made/'.$value["srec_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>
					</ul>';
				
			if($value['srec_status']=="bad_debts"){	
				
				$recipt_amount = $value['srec_baddebts_amt'];
			}else{
				$recipt_amount = $value['srec_amount'];
			}
			
	  		$records["data"][] = array(
				$check,
				$srec_code,
	  			$invNo,
	  			$custName,
	  			/*$value['currencycode'].' '.*/$invAmt,
	  			/*$value['currencycode'].' '.*/$recipt_amount,
	  			/*$value['currencycode'].' '.*/$value['srec_tds_amt'],
	  			/*$value['currencycode'].' '.*/$value['srec_pending_amt'],
	  			$srec_status,
	  			
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}
	public function download_payment_made($id){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id='';//$this->session->userdata['client_session']['gst_id'];
		$data['result'] = $this->RecPay_model->download_multiple_sreceipts($id,$reg_id,$bus_id,$gst_id);
		$this->load->view('receipts_payments/sales_receipts/download_all_receipt',$data);
		
	}
	public function print_payment_made($id){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		echo true;
	
	}
	public function email_payment_made(){
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];


		$gst_id=''; //$this->session->userdata['client_session']['gst_id'];

		$data = $_POST;
		$ids = explode(',',$data['recipt_id']);
		//print_r($ids);exit;
		// for($i = 0; $i < count($ids); $i++)
		// {

			
		// }

		$data_rec['result'] = $this->RecPay_model->download_multiple_sreceipts($ids,$reg_id,$bus_id,$gst_id);
		$html_data=$this->load->view('receipts_payments/sales_receipts/print_all_receipt',$data_rec,true);
		//$invoname="Sales Receipt";
		//$Attach[0] = $this->pdf->create($html_data,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Sales Receipt')."_".time();
		$downld=$this->pdf->create($html_data,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$subject = $data['email_subject'];
		$all_emails =  $data['email_to'];
		$output = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $data['email_message'];


		for($i=0; $i < count($output);$i++)
		{
			$to = $output[$i];
			//$status = sendEmail($to,$subject,$message);
			$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);
		}

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}
		
	}

	public function download_multiple_payment(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$gst_id='';//$this->session->userdata['client_session']['gst_id'];
		$data['result'] = $this->RecPay_model->download_multiple_sreceipts($array,$reg_id,$bus_id,$gst_id);
		$this->load->view('receipts_payments/sales_receipts/download_all_receipt',$data);
	}
	public function print_multiple_payment(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id='';//$this->session->userdata['client_session']['gst_id'];
		$data['result'] = $this->RecPay_model->download_multiple_sreceipts($array,$reg_id,$bus_id,$gst_id);
		$this->load->view('receipts_payments/sales_receipts/print_all_receipt',$data);
	}
	public function email_multiple_payment(){
		$array=$this->input->post('sales_values');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id='';//$this->session->userdata['client_session']['gst_id'];
		$data['result'] = $this->RecPay_model->download_multiple_sreceipts($array,$reg_id,$bus_id,$gst_id);
		$html_data=$this->load->view('receipts_payments/sales_receipts/print_all_receipt',$data,true);
		$inv_name='Sales Receipts';
		$output = $this->pdf->create($html_data,$inv_name,'email');
		$msg ="Hey ".ucwords($this->session->userdata['client_session']['client_username']).", Congratulations! You have successfully create Sales Receipts.";
		sendInvoiceEmail($this->session->userdata['client_session']['client_email'],'Your Sales Receipts has been created',$msg,$output,$inv_name);

		
	}
	public function view_payment_made($srec_id){
		if($srec_id==''){
			redirect('statement_account/payment-made');
		}
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$this->load->model('RecPay_model');
		$data['sales_receipts'] = $this->RecPay_model->salesReceiptDetails($srec_id,$reg_id,$bus_id,'',$cust_id);

		if(count($data['sales_receipts'])<=0){
			redirect('statement_account/payment-made');
		}
		$data['customers']     = $this->Sales_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$data['padvice_documents']     = $this->Sales_model->selectData('srf_document', '*', array('parent_id'=>$srec_id,'srf_type'=>'sales_receipt'));
		$data['bank_details'] = $this->Sales_model->selectData('company_bank', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$this->load->view('statement_account/view-payment-made',$data);
	}
	/*---START DOCUMETATION SECTION---*/
	public function documentation()
	{
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['legal'] = $this->Sales_model->selectData('legal_document', '*',array('master_id'=>$bus_id,'bus_id'=>$cust_id,'legal_type'=>'customer'));
		$data['other'] = $this->Sales_model->selectData('other_document', '*',array('master_id'=>$bus_id,'bus_id'=>$cust_id,'other_type'=>'customer'));
		
		$this->load->view('statement_account/documents-list',$data);
	}
	public function get_documents_locker(){
		$post = $this->input->post();
		$post['reg_id']=$this->session->userdata['client_session']['reg_id'];
		$post['bus_id']=$this->session->userdata['client_session']['bus_id'];
		$post['cust_id']=$this->session->userdata['client_session']['cust_id'];
		
		$field_pos=array("legal.bus_id"=>'0',"legal_document"=>'1',"legal_po_date"=>'3',"legal_start_date"=>'4',"legal_end_date"=>'5',"legal_reminder"=>'6');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	
		$this->load->model('User_model');
		$TotalRecord=$this->User_model->documentLockerFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->User_model->documentLockerFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
		
	  	foreach ($userData as $key => $value) {

	  		$path="";

			if($value['doc_type']=="legal" and $value['type']=="customer"){
             $path=base_url().'public/upload/sales/customer/customer_legal_documents/'.$value['bus_id'].'/'.$value['dc_name'];
             $doctype=1;
			}

			if($value['doc_type']=="legal" and $value['type']=="company"){
				$path=base_url().'public/upload/company_legal_documents/'.$value['dc_name'];
             	$doctype=1;
			}

			if($value['doc_type']=="purchase_order" and $value['type']=="customer"){
              $path=base_url().'public/upload/sales/customer/customer_po_documents/'.$value['bus_id'].'/'.$value['dc_name'];
              $doctype=2;
			}
			if($value['doc_type']=="legal" and $value['type']=="company"){
			  $path=base_url().'public/upload/company_po_documents/'.$value['dc_name'];
              $doctype=2;
			}

			if($value['doc_type']=="other" and $value['type']=="customer"){
				$path=base_url().'public/upload/sales/customer/customer_other_documents/'.$value['bus_id'].'/'.$value['dc_name'];
				$doctype=3;
			}
			if($value['doc_type']=="legal" and $value['type']=="company"){
				$path=base_url().'public/upload/company_other_documents/'.$value['dc_name'];
				$doctype=3;
			}

			if($value['doc_type']=="Memorandum"){
				$path=base_url().'public/upload/company_moa_documents/'.$value['dc_name'];
				$doctype=4;
			}
	  	
	  		$check='<input type="checkbox" id="filled-in-box'.$value['dc_id'].'" name="filled-in-box'.$value['dc_id'].'" class="filled-in purple docl_bulk_action" value="'.$value['dc_id'].','.$doctype.'"/> <label for="filled-in-box'.$value['dc_id'].'"></label>';
	  		
	  		$d_name=preg_replace('/\\.[^.\\s]{3,4}$/', '', $value['dc_name']);
	  		
	  		// truncate string
	  		$dc_name=$d_name;
	  		if (strlen($d_name) > 25) {
   		    $stringCut = substr($d_name, 0, 25);
            $endPoint = strrpos($stringCut, ' ');
            $dc_name = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
   			$dc_name .= '...';
   			}
	  		if($value['dc_name']!=''){
	  		$dc_name='<a href="'.base_url().'public/upload/sales/customer/customer_legal_documents/'.$value['bus_id'].'/'.$value['dc_name'].'" target="_blank">'.$dc_name.'</a>';
	  		}
			$po_date='';$s_date='';$e_date='';$rem_type='';

			//if($value['doc_type']=="legal" && $value['doc_type']=="purchase_order"){
			if($value['po_date']!='')
			{
				
				$dateTime = date("d-m-Y",  strtotime($value['po_date']));
				if($value['po_date']=='0000-00-00'){
					$dateTime='';
				}
				$po_date = str_replace("-",".",$dateTime);
			}
			if($value['start_date']!='')
			{
				
				$dateTime = date("d-m-Y",  strtotime($value['start_date']));
				if($value['start_date']=='0000-00-00'){
					$dateTime='';
				}
				$s_date = str_replace("-",".",$dateTime);
			}
			if($value['end_date']!='')
			{
				
				$dateTime = date("d-m-Y",  strtotime($value['end_date']));
				if($value['end_date']=='0000-00-00'){
					$dateTime='';
				}
				$e_date = str_replace("-",".",$dateTime);
			}
		//}
			if($value['reminder']=="1"){
				$rem_type="One week before";
			}else if($value['reminder']=="2"){
				$rem_type="Fortnight";
			}else if($value['reminder']=="3"){
				$rem_type="One month before";
			}else if($value['reminder']=="4"){
				$rem_type="Two months before";
			}
			

			 $path="";
			// if($value['doc_type']=="legal"){
   //           $path=base_url().'public/upload/company_legal_documents/'.$value['dc_name'];
   //           $doctype=1;
			// }
			// if($value['doc_type']=="purchase_order"){
   //            $path=base_url().'public/upload/company_po_documents/'.$value['dc_name'];
   //            $doctype=2;
			// }

			// if($value['doc_type']=="other"){
			// 	$path=base_url().'public/upload/company_other_documents/'.$value['dc_name'];
			// 	$doctype=3;
			// }

			if($value['doc_type']=="legal" and $value['type']=="customer"){
             $path=base_url().'public/upload/sales/customer/customer_legal_documents/'.$value['bus_id'].'/'.$value['dc_name'];
             $doctype=1;
			}

			if($value['doc_type']=="legal" and $value['type']=="company"){
				$path=base_url().'public/upload/company_legal_documents/'.$value['dc_name'];
             	$doctype=1;
			}

			if($value['doc_type']=="purchase_order" and $value['type']=="customer"){
              $path=base_url().'public/upload/sales/customer/customer_po_documents/'.$value['bus_id'].'/'.$value['dc_name'];
              $doctype=2;
			}
			if($value['doc_type']=="legal" and $value['type']=="company"){
			  $path=base_url().'public/upload/company_po_documents/'.$value['dc_name'];
              $doctype=2;
			}

			if($value['doc_type']=="other" and $value['type']=="customer"){
				$path=base_url().'public/upload/sales/customer/customer_other_documents/'.$value['bus_id'].'/'.$value['dc_name'];
				$doctype=3;
			}
			if($value['doc_type']=="legal" and $value['type']=="company"){
				$path=base_url().'public/upload/company_other_documents/'.$value['dc_name'];
				$doctype=3;
			}

			if($value['doc_type']=="Memorandum"){
				$path=base_url().'public/upload/company_moa_documents/'.$value['dc_name'];
				$doctype=4;
			}
			
			$action = '';$status='';$cls='';$cls1='';$disable='';
			
			
			
			
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['dc_id']."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value['dc_id']."' class='dropdown-content'>
					".' <li><a href="javascript:void(0);" onclick="email_document('.$value['dc_id'].','.$doctype.');"><i class="material-icons">email</i>Email</a></li>
                              <li ><a href='.$path.' download><i class="dropdwon-icon icon download"></i>Download</a></li>
                            <!--<li><a href='.base_url().'statement_account/print-document/'.$value['dc_id'].' target="_blank"><i class="material-icons dp48">print</i>Print</a></li>--></ul>';

		
	  		$records["data"][] = array(
				$check,
				$dc_name,
				$po_date,
	  			$s_date,
	  			$e_date,
	  			$rem_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}
	public function download_document($array){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array,'1');
		$this->load->view('dashboard/document_locker/download_document_locker',$data);
	}
	public function print_document($array){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array,'1');
		$this->load->view('dashboard/document_locker/print_all_document_locker',$data);
	}
	public function email_document(){
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];

		$post_data = $_POST;

		$ids = explode(',',$post_data['doc_id']);
		//print_r($post_data);
		for($i = 0; $i < count($ids); $i++)
		{

			 $path="";
			 $type = $ids[$i+1];

			if($type==1){
				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
				
				if($doc_name[0]->legal_type == 'company')
				{
            	 	$path=DOC_ROOT.'/public/upload/company_legal_documents/'.$doc_name[0]->legal_document;
				}
				if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}
			}

			if($type==2){

				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
			  	if($doc_name[0]->legal_type == 'company')
				{
              		$path=DOC_ROOT.'/public/upload/company_po_documents/'.$doc_name[0]->legal_document;
              	}
              	if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

			}

			if($type==3){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));

				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_other_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
			}


			if($type==4){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_moa_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
				
			}

			$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
			$invoname ="Document Locker";
			$Attach[$i] = $path;

			$i++;

		}

		

		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];
		
		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$subject = $post_data['email_subject'];

		//print_r($Attach);exit;

		$status = sendInvoiceEmailDoclocker($from,$to,$subject,$message,$Attach,$invoname,$cc);

		if($status)
		{
			echo 1;
		}else{

			echo 0;
		}
		
	}

	public function download_multiple_documents(){
		
		$array=$this->input->get('ids');
	
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];

		$ids = explode(',', $array);
		//print_r($post_data);
		for($i = 0; $i < count($ids); $i++)
		{

			 $path="";
			 $type = $ids[$i+1];

			if($type==1){
				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
				
				if($doc_name[0]->legal_type == 'company')
				{
            	 	$path=DOC_ROOT.'/public/upload/company_legal_documents/'.$doc_name[0]->legal_document;
				}
				if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}
			}

			if($type==2){

				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
			  	if($doc_name[0]->legal_type == 'company')
				{
              		$path=DOC_ROOT.'/public/upload/company_po_documents/'.$doc_name[0]->legal_document;
              	}
              	if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

			}

			if($type==3){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));

				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_other_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
			}


			if($type==4){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_moa_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
				
			}


			//$invoname ="Document Locker";
			//$Attach[$i] = $path;
			$this->zip->read_file($path); 
			$i++;

		}

		$doc_zip = "Documents_".time().".zip";
		$this->zip->download($doc_zip);

		//$data['result'] = $this->User_model->download_multiple_documents_locker($array);
		//$this->load->view('dashboard/document_locker/download_document_locker',$data);
	}
	
	public function print_multiple_documents(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array);
		$this->load->view('dashboard/document_locker/print_all_document_locker',$data);
	}
	public function email_multiple_documents(){
		$array=$this->input->post('sales_values');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array);
		$htmldata = $this->load->view('dashboard/document_locker/print_all_document_locker',$data,true);
		$invoname ='Documents';
		$msg ="Hey ".ucwords($this->session->userdata['client_session']['client_username']).", Congratulations! You have successfully create Documents.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->session->userdata['client_session']['client_email'],'Your Documents has been created',$msg,$output,$invoname); 
	}

	/*---END DOCUMETATION SECTION---*/
	/*---START TDS SECTION---*/
	public function tds()
	{
		
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$data['tds']=$this->Sales_model->selectData('sales_receipts', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'cust_id'=>$cust_id,'srec_tds_amt >'=>'1'),'','','','','','rowcount');
			$data['customers']=$this->Sales_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
			$this->load->view('statement_account/tds-list',$data);
	}
	public function get_tds_list(){
		$post = $this->input->post();
		$post['reg_id']=$this->session->userdata['client_session']['reg_id'];
		$post['bus_id']=$this->session->userdata['client_session']['bus_id'];
		$post['gst_id']=0;
		$post['srec_customer']=$this->session->userdata['client_session']['cust_id'];
		
		$this->load->model('RecPay_model');
		$field_pos=array("srec_id"=>'0',"inv_invoice_no"=>'1',"srec_date"=>'2',"srec_amount"=>'3',"srec_status"=>'4',"srec_tds_amt"=>'5',"srec_cstatus"=>'6');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->RecPay_model->ClientPortalsalesReceiptFilter($post,$sort_field,$orderBy,0,1);	
	 	$userData = $this->RecPay_model->ClientPortalsalesReceiptFilter($post,$sort_field,$orderBy,1,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
	  	foreach ($userData as $key => $value) {
                 $receipt_lists=$this->RecPay_model->selectData('sales_receipts_list', '*', array('srec_id'=>$value['srec_id']));
	  		$check='<input type="checkbox" id="filled-in-box'.$value['srec_id'].'" name="filled-in-box'.$value['srec_id'].'" class="filled-in purple tds_bulk_action" value="'.$value['srec_id'].'"/> <label for="filled-in-box'.$value['srec_id'].'"></label>';
	  				
	  			/*$srec_date='';
				if($value['srec_date']!='')
				{
					
					$dateTime = date("d M, Y",  strtotime($value['srec_date']));
					if($value['srec_date']=='0000-00-00'){
						$dateTime='';
					}
					$srec_date = $dateTime;
				}*/

                 $srec_code='';
           $srec_date='';
           $invNo="";
           $custName="";
           $invAmt="";

           foreach($receipt_lists as $receipt_list){
             // print_r($receipt_list->srec_id);

             
				if($value['srec_date']!='')
				{
					
					$dateTime = date("d M, Y",  strtotime($value['srec_date']));
					if($value['srec_date']=='0000-00-00'){
						$dateTime='';
					}
					$srec_date .= $dateTime;
				}
				
				if($value['srec_code']!='')
				{
					
					$srecc_code =$value['srec_code'];
					$srecc_code.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$dateTime.'</b></small></div></div>';
					$srec_code.='<a href="javascript:void(0)" >'.$srecc_code.'</a></br>';
				}

				

                 $receipts_lists=$this->RecPay_model->selectData('sales_invoices', '*', array('inv_id'=>$receipt_list->srecl_inv_no));

				$invNo.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$receipts_lists[0]->inv_invoice_no.'</br><span>'.$dateTime.'</span></b></small></div></div></br>';

				$custName.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.$value['cust_name'].'</b></small></div></div></br>';
				$invAmt.='<div class="row"><div class="col s12 m12 l12"><small class="text-gray"><b>'.round($receipt_list->srecl_inv_amt).'</b></small></div></div></br>';
				
				
	  		}

			$srec_cstatus='';	$checked='';
				
			if($value['srec_cstatus'] == 1){
	  			 $value['srec_cstatus']=0;
				$checked='checked="checked"';
            }else{
            	 $value['srec_cstatus']=1;
            }

            $srec_cstatus='<div class="switch none-after">
              <label>
              <input type="checkbox" id="tds_status'.$value['srec_id'].'" name="tds_status'.$value['srec_id'].'"  class="cust_status center_cls" data-tds_id="'.$value['srec_id'].'" value="'.$value['srec_cstatus'].'" '.$checked.'  >
                <span class="lever tabl"></span>
                </label>
            </div>';
			$srec_status='<span class="red-c">'.ucwords(str_replace('_', ' ', $value['srec_status'])).'</span>';
			if($value['srec_status']=="full_received" || $value['srec_status']=="Paid"){	
			$srec_status='<span class="green-c">Full Received</span>';
			}
			if($value['srec_status']=="part_received"){
				$srec_status='<span class="red-c">Part Received</span>';
				
			}
			
			$action = '';$cls='';
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["srec_id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["srec_id"]."' class='dropdown-content'>
					".'<li><a href="javascript:void(0);" onclick="email_tds('.$value["srec_id"].');"><i class="material-icons">email</i>Email</a></li>
                    <li><a href='.base_url().'statement_account/download-tds/'.$value["srec_id"].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;height: 20px;">Export</a></li>
					<li><a href='.base_url().'statement_account/print-tds/'.$value["srec_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>
					</ul>';
				$tds_per=number_format((float)$value['srec_tds_amt']/$value['srec_amount']*100,0,'.','');
		 
	  		$records["data"][] = array(
				$check,
				$invNo,
				$custName,
				$invAmt,
	  			//$value['inv_invoice_no'],
	  			//$srec_date,
	  			/*$value['cust_name'],*/
	  			/*$value['currencycode'].' '.*///$value['srec_amount'],
	  			$srec_status,
	  			/*$value['currencycode'].' '.*/round($value['srec_tds_amt']),
	  			$tds_per.'%',
	  			$srec_cstatus,
	  			$action,
	  			);
	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}
	public function download_tds($array){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;
		$data['result']=$this->RecPay_model->print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id,'1');
		$this->load->view('tax_summary/download_all_tds',$data);
	}
	public function print_tds($array){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;
		$data['result']=$this->RecPay_model->print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id,'1');
		$this->load->view('tax_summary/print_all_tds',$data);
	}
	public function email_tds(){

		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;

		$data = $_POST;
		$ids = explode(',',$data['tds_id']);

		$data_tds['result']=$this->RecPay_model->print_multiple_clienttds($ids,$reg_id,$bus_id,$gst_id);
		$htmldata = $this->load->view('tax_summary/print_all_tds',$data_tds,true);
		//$invoname ="TDS";
		//$Attach[0]=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','TDS')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$subject = $data['email_subject'];
		$all_emails =  $data['email_to'];
		$output = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $data['email_message'];


		for($i=0; $i < count($output);$i++)
		{
			$to = $output[$i];
			//$status = sendEmail($to,$subject,$message);
			$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);
		}

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}
		
	}

	public function download_multiple_tds(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;
		$data['result']=$this->RecPay_model->print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id);
		$this->load->view('tax_summary/download_all_tds',$data);
		
	}
	public function print_multiple_tds(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;
		$data['result']=$this->RecPay_model->print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id);
		$this->load->view('tax_summary/print_all_tds',$data);
	}
	public function email_multiple_tds($id){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];
		$gst_id=0;
		$data['result']=$this->RecPay_model->print_multiple_clienttds($array,$reg_id,$bus_id,$gst_id);
		$htmldata = $this->load->view('tax_summary/print_all_tds',$data,true);
		$invoname ="TDS";
		$msg ="Hey ".ucwords($this->session->userdata['client_session']['client_username']).", Congratulations! You have successfully create TDS.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->session->userdata['client_session']['client_email'],'Your TDS has been created',$msg,$output,$invoname); 
		
	}
	public function clients_tds_status()
	{
		$ctds_id = $this->input->post('ctds_id');
		
		$filter = array(
			'srec_cstatus'	=>	$this->input->post('status'),

		);
		$flag=$this->Sales_model->updateData('sales_receipts',$filter,array('srec_id'=>$ctds_id));

		if($flag != false)
		{
			echo true;
		}
		else
		{
			echo false;
		}
	}

	public function companies(){
		$reg_id=$this->session->userdata['client_session']['reg_id'];
		$bus_id=$this->session->userdata['client_session']['bus_id'];
		$cust_id=$this->session->userdata['client_session']['cust_id'];

		$data['company_data'] = $this->Sales_model->selectData('businesslist','*',array("bus_id"=>$bus_id));

		$this->load->view('statement_account/company-list',$data);

	}
	
	public function download_multiple_soa(){

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$cust_id=0;//$this->user_session['cust_id'];
       
		$data['invoice']=$this->Sales_model->selectData('sales_invoices', '*', array('bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice','status'=>'Active'),'inv_id','ASC'); 

		$data['srec_amount']=0;$data['inv_amount']=0;$data['cd_total']=0;
		$count=0;
		$count_cd=0;
		if(count($data['invoice'])>0){
		$count=count($data['invoice']);

		$sreceipts=0;$inv_amount=0;$cd_total=0;
		foreach($data['invoice'] as $key => $value) {
			$inv_amount=$inv_amount+$value->inv_grant_total;
			$srec=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value->inv_id,'status'=>'Active'));
			for($i=0;$i<count($srec);$i++)
			{
				$sr=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$srec[$i]->srec_id,'status'=>'Active'));

				if($sr)
				{
					$count++;
					$sreceipts=$sreceipts+$sr[0]->srec_amount;

					if($sr[0]->srec_tds_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_tds_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_baddebts_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count++;
			  			}
			  			if($sr[0]->srec_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count--;
			  			}
				
				}	

			}
			$cd_note=$this->Sales_model->selectData('sales_credit_debit', 'cd_grant_total', array('cd_invoice_no'=>$value->inv_invoice_no,'status'=>'Active','cust_id'=>$value->cust_id));
			
			for($i=0;$i<count($cd_note);$i++)
			{
				$count_cd++;
				$cd_total=$cd_total+$cd_note[$i]->cd_grant_total;
			}
		}
		} 

			$data['expense']=$this->Sales_model->selectData('company_expense', '*', array('bus_id'=>$bus_id),'exp_id','ASC'); 
		$data['pay_amount']=0;$data['exp_amount']=0;$data['db_total']=0;
		$count_1=0;
		$count_cd_1=0;
		if(count($data['expense'])>0){
		$count_1=count($data['expense']);
		$pay_amount=0;$exp_amount=0;$db_total=0;

		foreach($data['expense'] as $key => $value) {
			$exp_amount=$exp_amount+$value->ce_grandtotal;
			$pay=$this->Sales_model->selectData('expense_payment_list', '*', array('epayl_exp_no'=>$value->ce_number,'status'=>'Active'));

			for($i=0;$i<count($pay);$i++)
			{
				$sr=$this->Sales_model->selectData('expense_payments', '*', array('epay_id'=>$pay[$i]->epay_id,'status'=>'Active'));

				if($sr)
				{
					$count_1++;
					$pay_amount=$pay_amount+$sr[0]->epay_amount;

					if($sr[0]->epay_tds_amt > 0)
			  			{
			  				$pay_amount=$pay_amount+$sr[0]->epay_tds_amt;
			  				$count_1++;
			  			}

			  			
						
						if($sr[0]->epay_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count_1--;
			  			}
				}	

			}

			$db_note=$this->Sales_model->selectData('expense_debit_note', 'debit_grant_total', array('debit_invoice_no'=>$value->ce_number,'vendor_id'=>$value->ce_vendorname,'status'=>'Active'));
			for($i=0;$i<count($db_note);$i++)
			{
				$count_cd_1++;
				$db_total=$db_total+$db_note[$i]->debit_grant_total;
			}
			
		}
		} 

		$data['exp_amount']=$exp_amount;
		$data['pay_amount']=$pay_amount;
		$data['db_total']=$db_total;

		$company_name = $this->Sales_model->selectData('businesslist', 'bus_company_name', array('bus_id'=>$bus_id)); 
		$data['company_name'] = $company_name[0]->bus_company_name;
		$data['inv_amount']=$inv_amount;
		$data['sreceipts']=$sreceipts;
		$data['cd_total']=$cd_total;
		$data['count']=$count;

		$data['statement_data'] = $this->Statement_model->printsoa($reg_id,$bus_id,$cust_id,$_GET['start'],$_GET['end']);
		$html_data = $this->load->view('soa/download-soa-doc',$data,true);
		// echo '<pre>';
		 
		

		  
		// echo '</pre>';
		// exit;
		
		$this->pdf->create($html_data,"Statement of account",'download');

		echo true;
	}
	public function print_multiple_soa(){

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$cust_id=0;//$this->session->userdata['client_session']['cust_id'];
		$vendor_id=0;//$this->session->userdata['client_session']['cust_id'];

		$data['invoice']=$this->Sales_model->selectData('sales_invoices', '*', array('bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice'),'inv_id','ASC'); 
		$data['srec_amount']=0;$data['inv_amount']=0;$data['cd_total']=0;
		$count=0;
		$count_cd=0;
		if(count($data['invoice'])>0){
		$count=count($data['invoice']);
		$sreceipts=0;$inv_amount=0;$cd_total=0;
		foreach($data['invoice'] as $key => $value) {
			$inv_amount=$inv_amount+$value->inv_grant_total;
			$srec=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value->inv_id,'status'=>'Active'));
			//print_r($srec);
			for($i=0;$i<count($srec);$i++)
			{
				$sr=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$srec[$i]->srec_id,'status'=>'Active'));

				if($sr)
				{
					$count++;
					$sreceipts=$sreceipts+$sr[0]->srec_amount;

					if($sr[0]->srec_tds_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_tds_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_baddebts_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count--;
			  			}
				
				}	

			}

			$cd_note=$this->Sales_model->selectData('sales_credit_debit', 'cd_grant_total', array('cd_invoice_no'=>$value->inv_invoice_no,'cust_id'=>$value->cust_id,'status'=>'Active'));
			for($i=0;$i<count($cd_note);$i++)
			{
				$count_cd++;
				$cd_total=$cd_total+$cd_note[$i]->cd_grant_total;
			}
		}
		} 


       $data['expense']=$this->Sales_model->selectData('company_expense', '*', array('bus_id'=>$bus_id),'exp_id','ASC'); 
		$data['pay_amount']=0;$data['exp_amount']=0;$data['db_total']=0;
		$pay_amount=0;$exp_amount=0;$db_total=0;
		$count_exp=0;
		$count_exp_db=0;
		if(count($data['expense'])>0){
		$count_exp=count($data['expense']);
		

		foreach($data['expense'] as $key => $value) {
			$exp_amount=$exp_amount+$value->ce_grandtotal;
			$pay=$this->Sales_model->selectData('expense_payment_list', '*', array('epayl_exp_no'=>$value->ce_number,'status'=>'Active'));

			

			for($i=0;$i<count($pay);$i++)
			{
				$sr=$this->Sales_model->selectData('expense_payments', '*', array('epay_id'=>$pay[$i]->epay_id,'status'=>'Active'));

				if($sr)
				{
					$count_exp++;
					$pay_amount=$pay_amount+$sr[0]->epay_amount;

					if($sr[0]->epay_tds_amt > 0)
			  			{
			  				$pay_amount=$pay_amount+$sr[0]->epay_tds_amt;
			  				$count_exp++;
			  			}

			  			
						
						if($sr[0]->epay_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count_exp--;
			  			}
				}	

			}

			$db_note=$this->Sales_model->selectData('expense_debit_note', 'debit_grant_total', array('debit_invoice_no'=>$value->ce_number,'vendor_id'=>$value->ce_vendorname,'status'=>'Active'));
			for($i=0;$i<count($db_note);$i++)
			{
				$count_exp_db++;
				$db_total=$db_total+$db_note[$i]->debit_grant_total;
			}
			
		}
		} 

		$data['exp_amount']=$exp_amount;

		$data['pay_amount']=$pay_amount;
		$data['db_total']=$db_total;
		$data['count_exp']=$count_exp+$count_exp_db;




		$company_name = $this->Sales_model->selectData('businesslist', 'bus_company_name', array('bus_id'=>$bus_id)); 
		$data['company_name'] = $company_name[0]->bus_company_name;
		$data['inv_amount']=$inv_amount;
		$data['sreceipts']=$sreceipts;
		$data['cd_total']=$cd_total;
		$data['count']=$count+$count_cd;;

		$data['statement_data'] = $this->Statement_model->printsoa($reg_id,$bus_id,$cust_id,$_GET['start'],$_GET['end']);
		
		$this->load->view('soa/print-soa-doc',$data);
		//echo true;
		
	}

	public function email_multiple_soa(){
		
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$cust_id=0;//$this->session->userdata['client_session']['cust_id'];

		$post_data = $_POST;
		
		$data['invoice']=$this->Sales_model->selectData('sales_invoices', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice'),'inv_id','ASC'); 
		$data['srec_amount']=0;$data['inv_amount']=0;$data['cd_total']=0;
		$count=0;
		$count_cd=0;
		if(count($data['invoice'])>0){
		$count=count($data['invoice']);
		$sreceipts=0;$inv_amount=0;$cd_total=0;
		foreach($data['invoice'] as $key => $value) {
			$inv_amount=$inv_amount+$value->inv_grant_total;
			$srec=$this->Sales_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value->inv_id,'status'=>'Active'));
			//print_r($srec);
			for($i=0;$i<count($srec);$i++)
			{
				$sr=$this->Sales_model->selectData('sales_receipts', '*', array('srec_id'=>$srec[$i]->srec_id,'status'=>'Active'));

				if($sr)
				{
					$count++;
					$sreceipts=$sreceipts+$sr[0]->srec_amount;

					if($sr[0]->srec_tds_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_tds_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_baddebts_amt > 0)
			  			{
			  				$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count++;
			  			}

			  			if($sr[0]->srec_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count--;
			  			}
				
				}	

			}

			$cd_note=$this->Sales_model->selectData('sales_credit_debit', 'cd_grant_total', array('cd_invoice_no'=>$value->inv_invoice_no,'cust_id'=>$value->cust_id,'status'=>'Active'));
			for($i=0;$i<count($cd_note);$i++)
			{
				$count_cd++;
				$cd_total=$cd_total+$cd_note[$i]->cd_grant_total;
			}
		}
		} 

		  $data['expense']=$this->Sales_model->selectData('company_expense', '*', array('bus_id'=>$bus_id),'exp_id','ASC'); 
		$data['pay_amount']=0;$data['exp_amount']=0;$data['db_total']=0;
		$pay_amount=0;$exp_amount=0;$db_total=0;
		$count_exp=0;
		$count_exp_db=0;
		if(count($data['expense'])>0){
		$count_exp=count($data['expense']);
		

		foreach($data['expense'] as $key => $value) {
			$exp_amount=$exp_amount+$value->ce_grandtotal;
			$pay=$this->Sales_model->selectData('expense_payment_list', '*', array('epayl_exp_no'=>$value->ce_number,'status'=>'Active'));

			

			for($i=0;$i<count($pay);$i++)
			{
				$sr=$this->Sales_model->selectData('expense_payments', '*', array('epay_id'=>$pay[$i]->epay_id,'status'=>'Active'));

				if($sr)
				{
					$count_exp++;
					$pay_amount=$pay_amount+$sr[0]->epay_amount;

					if($sr[0]->epay_tds_amt > 0)
			  			{
			  				$pay_amount=$pay_amount+$sr[0]->epay_tds_amt;
			  				$count_exp++;
			  			}

			  			
						
						if($sr[0]->epay_amount <= 0)
			  			{
			  				//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
			  				$count_exp--;
			  			}
				}	

			}

			$db_note=$this->Sales_model->selectData('expense_debit_note', 'debit_grant_total', array('debit_invoice_no'=>$value->ce_number,'vendor_id'=>$value->ce_vendorname,'status'=>'Active'));
			for($i=0;$i<count($db_note);$i++)
			{
				$count_exp_db++;
				$db_total=$db_total+$db_note[$i]->debit_grant_total;
			}
			
		}
		} 

		$data['exp_amount']=$exp_amount;

		$data['pay_amount']=$pay_amount;
		$data['db_total']=$db_total;
		$data['count_exp']=$count_exp+$count_exp_db;

		$company_name = $this->Sales_model->selectData('businesslist', 'bus_company_name', array('bus_id'=>$bus_id)); 
		$data['company_name'] = $company_name[0]->bus_company_name;
		$data['inv_amount']=$inv_amount;
		$data['sreceipts']=$sreceipts;
		$data['cd_total']=$cd_total;
		$data['count']=$count;

		$data['statement_data'] = $this->Statement_model->printsoa($reg_id,$bus_id,$cust_id,$post_data['start_date'],$post_data['end_date']);
		// echo '<pre>';
		// print_r($data['statement_data']); 
		// echo '</pre>';
		// exit;
		$html_data = $this->load->view('soa/download-soa-doc',$data,true);
		//$invoname = 'Statement of account';
		//$attach[0]=$this->pdf->create($html_data,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Statement of account')."_".time();
		$downld=$this->pdf->create($html_data,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$attach[0]=$path;

		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;

		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];

		//$to =  $data['email_to'];
		$subject = $post_data['email_subject'];
		//$message=$this->load->view('email_template/subscription_detail',$data,true);

		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$attach,$invoname,$cc);
		
		for($i=0; $i < count($attach); $i++)
		{
			unlink($attach[$i]);
		}

		if($status)
		{
			echo 1;

		}else{
			
			echo 0;
		}
		
		
	}
	public function change_password()
	{
		
		
		$post=$this->input->post();

		if($post){

			$old_password = $post['old_password'];
			$new_password = $post['new_password'];
			
			$cust_id=$this->session->userdata['client_session']['cust_id'];
			$client_email=$this->session->userdata['client_session']['client_email'];
			$client_username=$this->session->userdata['client_session']['client_username'];
			//$id=$this->session->userdata('id');	
			
			$contact=$this->Sales_model->selectData('sales_customer_contacts', '*', array('cust_id'=>$cust_id,'cp_is_admin'=>1,'cp_email'=>$client_email,'cp_name'=>$client_username,'cp_password'=>md5($old_password)));
			
			if(count($contact)>0)
			{
				$update=$this->Sales_model->updateData('sales_customer_contacts',array('cp_password'=>md5($new_password)),array('cust_id'=>$cust_id,'cp_id'=>$contact[0]->cp_id));
				if($update!=false)
				{
					$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
					$this->session->set_flashdata('success','Congratulations! Your password has bee changed successfully');
					$to = $client_email;
					$from = $fromemail[0]->bus_company_name;
					$subject = 'Here’s your new password';
					$data['password']=$new_password;
					$data['name']=ucfirst($client_username);
					$attachment="";
					$message=$this->load->view('email_template/change-password',$data,true);
					sendEmail($from,$to,$subject,$message);
					
				}
				else{
					$this->session->set_flashdata('error','Error while processing!');
					
				}
			}
			else{
				$this->session->set_flashdata('error','<b>Opps!</b> Your Password is Wrong');
			}
			
			redirect('statement_account/change-password');
		}
		$this->load->view('statement_account/change-password');
		
	}
}
