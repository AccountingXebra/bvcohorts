<?php 
	require ("common/Index_Controller.php");
	class Apps_controller extends Index_Controller {
		
		function __construct(){
			parent::__construct();

			$this->load->model('App_model');
			$this->load->model('Adminmaster_model');
			$this->load->model('Business_analytics_model');
			$this->load->model('Sales_model');
			$this->load->library('session');
		}
		
		public function index() {
			print('You Are At the index of the controller');
			exit();
		}
		
		public function testing() {
			echo "testing 1 2 3 !";
			exit();
		}
		
		public function loginAuthentication() {
			
			$data = json_decode( file_get_contents( 'php://input' ), true);
			if($data) {
				$user_email = $data['user_email'];
				$user_password = md5($data['user_password']);
			
				$user_details = $this->App_model->selectData("registration","reg_username,reg_mobile,reg_id,bus_id","reg_email = '".$user_email."' and reg_password = '".$user_password."'");
				
				if($user_details) {
					$unique_code = substr(bin2hex(openssl_random_pseudo_bytes(20)), 0, 20);
					
					$add_token = $this->App_model->updateData("registration",array("auth_token" => $unique_code),"reg_id='".$user_details[0]->reg_id."'");
					
					if($add_token) {
						
						$return_data = [];
						$return_data['reg_id'] = $user_details[0]->reg_id;
						$return_data['bus_id'] = $user_details[0]->bus_id;
						$return_data['reg_username'] = $user_details[0]->reg_username;
						$return_data['reg_mobile'] = $user_details[0]->reg_mobile;
						$return_data['token'] = $unique_code;
						$return_data['success'] = 'true';
						
					} else {
						$return_data['success'] = 'false';
					}
				} else {
					$return_data['success'] = 'false';
					
				}
					echo "[".json_encode($return_data)."]";
				exit();
			}
		}
		
		public function Reset_passwd_email_link() {
			$data=json_decode( file_get_contents( 'php://input' ), true);
			if($data) {
				$emailid = $data['email'];
				$registration = $this->Adminmaster_model->selectData('registration','*',array("reg_email"=>$emailid));
				
				if(count($registration)>0) {
					
					$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$charactersLength = strlen($characters);
					$randomString = '';
					
					for ($i = 0; $i < 12 ; $i++) {
						$randomString .= $characters[rand(0, $charactersLength - 1)];
					}

					//$this->session->set_userdata('forgot_email',$emailid);
				
					$reset_array = array(
											'forget_pwd_token' => $randomString,
										);

					$insertotp = $this->Adminmaster_model->updateData('registration',$reset_array,array('reg_email'=>$emailid));
					
					if($insertotp!=false){
						$msg=base_url().'index/changemypassword?mytoken='. $randomString.'&email='.$emailid;
	          			$to=$emailid;
	          			$data['name']=ucwords($registration[0]->reg_username);
						$data['msg']=$msg;
						$subject="Trouble logging in? Reset your password";
						sendEmail($to,$subject,$message);
					}
					
					echo "success";
				} else {
					echo "failed";
				}
			}
		}
		
		public function Authenticate_user() {
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$passwd = $this->App_model->selectData("registration","reg_password","reg_id='".$data['reg_id']."'");
				
				if($passwd[0]->reg_password == md5($data['passwd'])) {
					echo'success';
					exit();
				} else {
					echo'failed';
					exit();
				}
			}
		}
		
		public function Change_password() {
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if($data['new_password'] == "") {
					echo'failed';
					exit();
				}
				
				$passwd = $this->App_model->selectData("registration","reg_password","reg_id='".$data['reg_id']."'");
				if($passwd[0]->reg_password == md5($data['current_password'])) {
					$reset_data['reg_password'] = md5($data['new_password']);
					$query = $this->App_model->updateData("registration",$reset_data,"reg_id = '".$data['reg_id']."'");
					
					if($query) {
						echo'success';
						exit();
					} else {
						echo'failed';
						exit();
					}
					
				} else {
					echo'failed';
					exit();
				}
				
			}
		}
		
		public function get_user_image() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$user_data = $this->App_model->selectData("registration","reg_profile_image","reg_id = '".$json['reg_id']."'");
				$user_img = $user_data[0]->reg_profile_image;
				if($user_img != null) {
						$encoded_img = base64_encode(file_get_contents(DOC_ROOT.'/public/upload/personal_images/'.$json['reg_id'].'/'.$user_img));
					echo $encoded_img;
				}
			}
		}
		
		public function create_personal_profile() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				unset($json['reg_id']);
				
				if(isset($json['dp_img'])) {
					$dp_img = $json['dp_img'];
					unset($json['dp_img']);
				}
				
				if(isset($json['sign_img'])) {
					$sign_img = $json['sign_img'];
					unset($json['sign_img']);
				}
				
				//Json data validation
				/*foreach($json as $testcase) {
					if($testcase == "") {
						echo "data should not be empty";
						exit();
					}
					if(ctype_space($testcase)) {
						echo "data should not be only white space";
						exit();
					}
				}*/
				
				$json['reg_password'] = md5($json['reg_password']);
				
				//Entering data in database
				$create_user = $this->App_model->insertData("registration",$json);
				
				if($create_user) {
					
					if($json['reg_profile_image'] != "" && $dp_img != "") {
						
						mkdir (DOC_ROOT.'/public/upload/personal_images/'.$create_user, 0777);
						
						$dp_file = fopen(DOC_ROOT.'/public/upload/personal_images/'.$create_user.'/'.$json['reg_profile_image'], "wb");
						//$dp_file = fopen('https://192.168.1.35/xebraintern/public/upload/personal_images/'.$create_user.'/'.$json['reg_profile_image'], "wb");
						$dp_content = base64_decode($dp_img);
						fwrite($dp_file, $dp_content);
						fclose($dp_file);
					}
					
					if($json['reg_degital_signature'] != "" && $sign_img != "") {
						
						mkdir (DOC_ROOT.'/public/upload/degital_signature/'.$create_user, 0777);
						
						$sign_file = fopen(DOC_ROOT.'/public/upload/degital_signature/'.$create_user.'/'.$json['reg_degital_signature'], "wb");
						//$sign_file = fopen('https://192.168.1.35/xebraintern/public/upload/degital_signature/'.$create_user.'/'.$json['reg_degital_signature'], "wb");
						$sign_content = base64_decode($sign_img);
						fwrite($sign_file, $sign_content);
						fclose($sign_file);
					}
					
					echo "success";
					exit();
				} else {
					echo "failed";
					exit();
				}
			}
		}
		
		public function get_personal_profiles() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$bus_id = $json['bus_id'];
				$data = $this->App_model->selectData("registration","reg_id,reg_username,reg_mobile,reg_designation,reg_email,reg_profile_image,reg_degital_signature","bus_id = '".$bus_id."'");
				
				foreach($data as $profile) {
					$user_img = $profile->reg_profile_image;
					$user_sign = $profile->reg_degital_signature;
					$profile->encoded_img = "";
					$profile->encoded_sign = "";
					if($user_img != null) {
						$encoded_img = base64_encode(file_get_contents(DOC_ROOT.'/public/upload/personal_images/'.$profile->reg_id.'/'.$user_img));
						//$encoded_img = base64_encode(file_get_contents(base_url().'public/upload/personal_images/'.$profile->reg_id.'/'.$user_img));
						$profile->encoded_img = $encoded_img;
					}
					if($user_sign != null){
						$encoded_sign = base64_encode(file_get_contents(DOC_ROOT.'/public/upload/degital_signature/'.$profile->reg_id.'/'.$user_sign));
						//$encoded_sign = base64_encode(file_get_contents(base_url().'public/upload/degital_signature/'.$profile->reg_id.'/'.$user_sign));
						$profile->encoded_sign = $encoded_sign;
					}
				}
				
				print_r(json_encode($data));
				exit();
			}
		}

		public function get_personal_profile_data() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$profile_id = $json['profile_id'];
				$profile_data = $this->App_model->selectData("registration","reg_id,reg_username,reg_mobile,reg_designation,reg_email,reg_profile_image,reg_degital_signature","reg_id = '".$profile_id."'");
				
				$user_img = $profile_data[0]->reg_profile_image;
				$user_sign = $profile_data[0]->reg_degital_signature;
				$profile_data[0]->encoded_img = "";
				$profile_data[0]->encoded_sign = "";
				if($user_img != null) {
					$encoded_img = base64_encode(file_get_contents(DOC_ROOT.'/public/upload/personal_images/'.$profile_id.'/'.$user_img));
					//$encoded_img = base64_encode(file_get_contents(base_url().'public/upload/personal_images/'.$profile_id.'/'.$user_img));
					$profile_data[0]->encoded_img = $encoded_img;
				}
				if($user_sign != null){
					$encoded_sign = base64_encode(file_get_contents(DOC_ROOT.'/public/upload/degital_signature/'.$profile_id.'/'.$user_sign));
					//$encoded_sign = base64_encode(file_get_contents(base_url().'public/upload/degital_signature/'.$profile_id.'/'.$user_sign));
					$profile_data[0]->encoded_sign = $encoded_sign;
				}
				
				print_r(json_encode($profile_data));
			}
		}
		
		public function save_edited_profile() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				unset($json['reg_id']);
				
				if($json['reg_profile_image']==""){
					unset($json['reg_profile_image']);
				}
				if($json['reg_degital_signature']==""){
					unset($json['reg_degital_signature']);
				}
				
				if(isset($json['dp_img'])) {
					$dp_img = $json['dp_img'];
					unset($json['dp_img']);
				}
				
				if(isset($json['sign_img'])) {
					$sign_img = $json['sign_img'];
					unset($json['sign_img']);
				}
				
				$profile_id = $json['profile_id'];
				
				unset($json['user_email']);
				unset($json['profile_id']);
				
				$query = $this->App_model->updateData("registration",$json,"reg_id = '".$profile_id."'");
				if($query) {
					
					if(isset($json['reg_profile_image']) && $dp_img != "") {
						
						$dp_folder  = DOC_ROOT.'/public/upload/personal_images/'.$profile_id;
						if(!(realpath($dp_folder) != false AND is_dir(realpath($dp_folder)))){
							mkdir (DOC_ROOT.'/public/upload/personal_images/'.$profile_id, 0777);
						}
						
						$dp_file = fopen(DOC_ROOT.'/public/upload/personal_images/'.$profile_id.'/'.$json['reg_profile_image'], "wb");
						//$dp_file = fopen('https://192.168.1.35/xebraintern/public/upload/personal_images/'.$create_user.'/'.$json['reg_profile_image'], "wb");
						$dp_content = base64_decode($dp_img);
						fwrite($dp_file, $dp_content);
						fclose($dp_file);
					}
					
					if( isset($json['reg_degital_signature']) && $sign_img != "") {
						
						$sign_folder = DOC_ROOT.'/public/upload/degital_signature/'.$profile_id;
						if(!(realpath($sign_folder) != false AND is_dir(realpath($sign_folder)))){
							mkdir (DOC_ROOT.'/public/upload/degital_signature/'.$profile_id, 0777);
						}
						
						$sign_file = fopen(DOC_ROOT.'/public/upload/degital_signature/'.$profile_id.'/'.$json['reg_degital_signature'], "wb");
						//$sign_file = fopen('https://192.168.1.35/xebraintern/public/upload/degital_signature/'.$create_user.'/'.$json['reg_degital_signature'], "wb");
						$sign_content = base64_decode($sign_img);
						fwrite($sign_file, $sign_content);
						fclose($sign_file);
					}
					
					echo 'success';
				} else {
					echo 'failed';
				}
			}
		}

		public function set_alerts() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				$alert_type = $json['alert_type'];
				unset($json['user_email']);
				unset($json['reg_id']);
				
				$json['alert_date'] = date("Y-m-d");
				
				$alerts = $this->App_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>$alert_type),'id','DESC');
				
				switch($alert_type) {
					
					case "customers":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("CA","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='CA'.$alno_ref;
						break;
						
					case "service":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("IA","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='IA'.$alno_ref;
						break;
						
					case "credit_history":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("CHA","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='CHA'.$alno_ref;
						break;
						
					case "vendors":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("VA","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='VA'.$alno_ref;
						break;
						
					case "birthday_anniversary":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("BA","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='BA'.$alno_ref;
						break;
						
					case "expense":
						if(count($alerts)>0){
							$last_alerts_no=$alerts[0]->alert_number; 
							$temp=str_replace("EXP","",$last_alerts_no);
							$temp=intval($temp) + 1;
							$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
						} else {
							$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
						}
						$json['alert_number']='EXP'.$alno_ref;
						break;
						
					default:
						$json['alert_number']= "No Number";
						break;
					}
				
				$create_alert = $this->App_model->insertData("my_alerts",$json);
				
				if($create_alert) {
					echo "success";
					exit();
				} else {
					echo "failed";
					exit();
				}
			}	
		}
		
		public function set_expense_voucher() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				unset($json['user_email']);
				
				$evl_data = $json['evl_items'];
				unset($json['evl_items']);
				
				$expv_id=$this->App_model->insertData('sales_expense_voucher',$json);
				
				foreach($evl_data as $evl) {
					$evl['ev_id'] = $expv_id;
					
					if($evl['evi_exp_image'] != "" AND $evl['evi_image_base64'] != "") {
						$evi_exp_image = $evl['evi_exp_image'];
						$evi_image_base64 = $evl['evi_image_base64'];
						
						unset($evl['evi_exp_image']);
						unset($evl['evi_image_base64']);
					}
					
					$evl_id = $this->App_model->insertData('sales_expense_voucher_list',$evl);
					
					$evi['evi_ev_id'] = $expv_id;
					$evi['evl_id'] = $evl_id;
					$evi['evi_exp_image'] = $evi_exp_image;
					
					$query = $this->App_model->insertData('sales_expense_voucher_image',$evi);
				}
				
				if($expv_id) {
					echo 'success';
				} else {
					echo 'failed';
				}
			}
		}
		
		public function get_alerts() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				$alert_type = $json['alert_type'];
				
				$alerts_list = $this->App_model->selectData("my_alerts","*","bus_id = '".$bus_id."' and alert_type = '".$alert_type."'");
				
				if($alert_type == 'customers' || $alert_type == 'credit_history') {
					foreach ($alerts_list as $alert) {
						$id = $alert->parent_id;
						$company_name = $this->App_model->selectData("sales_customers","cust_name","cust_id = '".$id."'");
						$alert->client_name = $company_name[0]->cust_name; 
					}
				}
				
				if($alert_type == 'services') {
					foreach ($alerts_list as $alert) {
						$id = $alert->parent_id;
						$item_name = $this->App_model->selectData("services","service_name","service_id = '".$id."'");
						$alert->item_name = $item_name[0]->service_name; 
					}
				}
				
				if($alert_type == 'credit_history') {
					
					/*foreach ($alerts_list as $alert) {
						$id = $alert->alert_invoice_id;
						$invoice_no = $this->App_model->selectData("sales_invoices","inv_invoice_no,inv_grant_total","inv_id = '".$id."'");
						if($invoice_no) {
							$alert->inv_invoice_no = $invoice_no[0]->inv_invoice_no; 
							$alert->inv_grant_total = $invoice_no[0]->inv_grant_total; 
						}
					}*/
				}
				
				if($alert_type == 'vendors') {
					foreach ($alerts_list as $alert) {
						$id = $alert->parent_id;
						$vendor_name = $this->App_model->selectData("expense_vendors","vendor_name","vendor_id = '".$id."'");
						$alert->vendor_name = $vendor_name[0]->vendor_name; 
					}
				}
				
				if($alert_type == 'expense') {
					foreach ($alerts_list as $alert) {
						$id = $alert->parent_id;
						$expense_name = $this->App_model->selectData("expense_list","exp_name","exp_id = '".$id."'");
						$alert->exp_name = $expense_name[0]->exp_name; 
					}
				}

				if($alert_type == 'birthday_anniversary') {
					foreach ($alerts_list as $alert) {
						$id = $alert->parent_id;
						if($alert->alert_stakeholder=="Client"){
                           $company_name = $this->App_model->selectData("sales_customers","cust_name","cust_id = '".$id."'");
						$alert->client_name = $company_name[0]->cust_name; 
						}
						
					}
				}
				
				echo json_encode($alerts_list);
			}
		}

		public function get_expense_vouchers() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$exp_data = $this->App_model->selectData("sales_expense_voucher","*","bus_id = '".$bus_id."'");
				if($exp_data) {
					foreach ($exp_data as $exp) {
							$id = $exp->cust_id;
							$company_name = $this->App_model->selectData("sales_customers","cust_name","cust_id = '".$id."'");
							if($company_name) {
								$exp->client_name = $company_name[0]->cust_name;
							}
					}
				}
				echo json_encode($exp_data);
			}
		}

		public function get_invoiceno() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$invoice_list = $this->App_model->selectData("sales_invoices","inv_id,inv_invoice_no,inv_invoice_date,inv_grant_total","bus_id = ".$bus_id);
				echo json_encode($invoice_list);
				exit();
			}
		}

		public function get_item_names() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$services_list = $this->App_model->selectData("services","service_id,service_name","bus_id = ".$bus_id);
				echo json_encode($services_list);
				exit();
			}
		}

		public function statement_of_account() {
			
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id= $json['reg_id'];
				$bus_id= $json['bus_id'];
		          
		          if($json['start_date']!="" && $json['end_date']!=""){
                   $data['invoice']=$this->App_model->selectData('sales_invoices', "inv_id,inv_invoice_no,inv_invoice_date,cust_id,inv_grant_total", array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice','inv_invoice_date >='=>date('Y-m-d',strtotime($json['start_date'])),'inv_invoice_date <='=>date('Y-m-d',strtotime($json['end_date']))),'inv_id','ASC'); 
		          }else{
		          	$data['invoice']=$this->App_model->selectData('sales_invoices', "inv_id,inv_invoice_no,inv_invoice_date,cust_id,inv_grant_total", array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice'),'inv_id','ASC'); 
		          }

		          
				
				$data['srec_amount']=0;
				$data['inv_amount']=0;
				$data['cd_total']=0;
				$count=0;
				$sreceipts=0;
					$inv_amount=0;
					$cd_total=0;
				$count_cd=0;
				$baddebt=0;
				$tds=0;
				$gain=0;
				$loss=0;
				if(count($data['invoice'])>0){
					$count=count($data['invoice']);
					
				
					for($i=0;$i<count($data['invoice']);$i++) {
						$data['invoice'][$i]->cust_name = $this->App_model->selectData('sales_customers',"cust_name",array('cust_id'=>$data['invoice'][$i]->cust_id))[0]->cust_name;
					}

					foreach($data['invoice'] as $key => $value) {
						$inv_amount=$inv_amount+$value->inv_grant_total;
						$srec=$this->App_model->selectData('sales_receipts_list', '*', array('srecl_inv_no'=>$value->inv_id,'status'=>'Active'));

					for($i=0;$i<count($srec);$i++) {
						$sr=$this->App_model->selectData('sales_receipts', '*', array('srec_id'=>$srec[$i]->srec_id,'status'=>'Active'));
	
						if($sr) {
							$count++;
							$sreceipts=$sreceipts+$sr[0]->srec_amount;

							if($sr[0]->srec_tds_amt > 0) {
								$tds=$tds+$sr[0]->srec_tds_amt;
								$count++;
							}
							if($sr[0]->srec_baddebts_amt > 0) {
								$baddebt=$baddebt+$sr[0]->srec_baddebts_amt;
								$count++;
							}
							if($sr[0]->srec_forex_gain > 0) {
								$gain=$gain+$sr[0]->srec_forex_gain;
								$count++;
							}	
							if($sr[0]->srec_forex_loss > 0) {
								$loss=$loss+$sr[0]->srec_forex_loss;
								$count++;
							}							
							if($sr[0]->srec_amount <= 0) {
								//$sreceipts=$sreceipts+$sr[0]->srec_baddebts_amt;
								$count--;
							}
					}	

				}

				$cd_note=$this->App_model->selectData('sales_credit_debit', 'cd_grant_total', array('cd_invoice_no'=>$value->inv_invoice_no,'cust_id'=>$value->cust_id,'status'=>'Active'));
				for($i=0;$i<count($cd_note);$i++)
				{
					$count_cd++;
					$cd_total=$cd_total+$cd_note[$i]->cd_grant_total;
					}	
				}
			} 

			$data['inv_amount']=$inv_amount;
			$data['sreceipts']=$sreceipts;
			$data['tds']=$tds;
			$data['baddebt']=$baddebt;
			$data['gain']=$gain;
			$data['loss']=$loss;
			$data['cd_total']=$cd_total;
			$data['count']=$count+$count_cd;
			
			echo "'".json_encode($data);
			exit(); 
			}
			else{
				echo "failed";
			}
		}

		public function get_expenses() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$expense_list = $this->App_model->selectData("expense_list","exp_id,exp_name,exp_category","bus_id = ".$bus_id);
				echo json_encode($expense_list);
				exit();
			}
		}

		public function get_client_names() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$client_list = $this->App_model->selectData("sales_customers","cust_id,cust_name,cust_credit_period","bus_id = ".$bus_id);
				echo json_encode($client_list);
				exit();
			}
		}

		public function get_employee_codes() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$employee_list = $this->App_model->selectData("employee_master","*","bus_id = ".$bus_id);
				echo json_encode($employee_list);
				exit();
			}
		}

		public function get_employee_details() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				$emp_code = $json['emp_code'];
				
				$employee_list = $this->App_model->selectData("employee_master","*",array("bus_id" =>$bus_id,"employee_id"=>$emp_code));

				echo json_encode($employee_list);
				exit();
			}
		}

		public function get_employee_approver() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				$emp_code = $json['emp_code'];
				
				$employee_list = $this->App_model->selectData("employee_master","*",array("bus_id" =>$bus_id,"emp_id"=>$emp_code));

				echo json_encode($employee_list);
				exit();
			}
		}
		
		public function get_gst_numbers() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$gst_list = $this->App_model->selectData("gst_number","gst_id,gst_no","bus_id = ".$bus_id);
				echo json_encode($gst_list);
				exit();
			}
		}
		
		public function get_expno() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$exp_vo_no=$this->App_model->selectData('sales_expense_voucher', '*', array('bus_id'=>$bus_id),'ev_id','DESC'); 

				if(count($exp_vo_no)>0){
					
					$last_exp_vo_no=$exp_vo_no[0]->ev_exp_vchr_no; 	
					$whatIWant = substr($last_exp_vo_no, strpos($last_exp_vo_no, "/") + 1); 
					$expNo=str_replace('/'.$whatIWant,"",$last_exp_vo_no); 
					$temp=str_replace("EV","",$expNo);
					$expno_ref='1'; 
					$temp=intval($temp) + 1; 
					$expno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
				} else {
					$expno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
				}
				
				echo "EV".$expno_ref;
				exit();
			}
		}
		
		public function get_vendor_names() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$vendor_list = $this->App_model->selectData("expense_vendors","vendor_id,vendor_name","bus_id = ".$bus_id);
				echo json_encode($vendor_list);
				exit();
			}
		}


		public function get_employee_names() {
			$json = json_decode( file_get_contents( 'php://input' ), true);
			if($json) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$json['reg_id']."'");
				if($auth[0]->auth_token != $json['token']) {
					echo'hacker alert';
					exit();
				}
				unset($json['token']);
				
				$reg_id = $json['reg_id'];
				$bus_id = $json['bus_id'];
				
				$emplyee_list = $this->App_model->selectData("employee_master","emp_id,first_name,last_name,employee_id","bus_id = ".$bus_id);
				echo json_encode($emplyee_list);
				exit();
			}
		}
		
		
		//Client & Revenue graphs function
		
		public function get_mtod_graph_data() {
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$result['mtd_start_date']=$this->Business_analytics_model->getMTDStart($data);
	
				$result['mtd_end_date']=$this->Business_analytics_model->getMTDEnd($data);


				if(count( $result['mtd_start_date'])<=0){
					if($data['mtd_start_date']==""){
						$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_start_date']));
					}
					$result['mtd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}
				
				if(count( $result['mtd_end_date'])<=0){
					if($data['mtd_end_date']==""){
						$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_end_date']));
					}
					$result['mtd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}

				$result['mtd_select']=$data['mtd_select'];
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
                 
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
	
		public function get_credit_notes(){
         
			//$data = json_decode( file_get_contents( 'php://input' ), true);
			$data=$this->input->post();
			
			$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
			if($auth[0]->auth_token != $data['token']) {
				echo'hacker alert';
				exit();
			}
			unset($data['token']);
			
			if(isset($data['reg_id'])){
				unset($data['reg_id']);
			}
			
			$date=date('Y-m',strtotime($data['revenueDate']));
			
			$this->db->select("*");
			$this->db->from('sales_invoices as inv');	
			$this->db->where("inv.bus_id",$data['bus_id']);		
			$this->db->where("inv.status","Active");
			$this->db->where("inv.inv_document_type ",'Sales Invoice');
			$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') =",$date);
		
			if($data['cust_id']){
				$this->db->where("inv.cust_id",$data['cust_id']);
			}
		
			$query = $this->db->get();
			$result= $query->result_array();
			$cd_amt=0;

			foreach($result as $key=>$value){
				$this->db->select("*");
				$this->db->from('sales_credit_debit as cd');
				$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'left');
				$this->db->where("cd.bus_id",$data['bus_id']);	
				$this->db->where("cd.cd_invoice_no",$value['inv_invoice_no']);

				if($data['cust_id']){
					$this->db->where("cd.cust_id",$data['cust_id']);
				}
				$querycr = $this->db->get();
				$credit= $querycr->result_array();
				
				foreach($credit as $keyCr=>$valueCr){
					$cd_amt+=($valueCr['cdl_taxable_amt'] * $valueCr['cdn_inr_value']);
				}
			}
		
			$credit['credit_note']=	$cd_amt;
			echo "[".json_encode($credit,JSON_NUMERIC_CHECK )."]";
			exit();	
		}
	
		public function get_bad_debts() {

			//$data = json_decode( file_get_contents( 'php://input' ), true);
			$data=$this->input->post();
			
			$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
			if($auth[0]->auth_token != $data['token']) {
				echo'hacker alert';
				exit();
			}
			unset($data['token']);
			
			if(isset($data['reg_id'])){
				unset($data['reg_id']);
			}
			
			$date=date('Y-m',strtotime($data['revenueDate']));
			
			$this->db->select("*");
			$this->db->from('sales_invoices as inv');	
			$this->db->where("inv.bus_id",$data['bus_id']);	
			$this->db->where("inv.status","Active");
			$this->db->where("inv.inv_document_type ",'Sales Invoice');
			$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') =",$date);

			if($data['cust_id']){
				$this->db->where("inv.cust_id",$data['cust_id']);
			}
			
			$query = $this->db->get();
			$result= $query->result_array();
			$sr_baddebts_amt=0;
			
			foreach($result as $key=>$value) {
				$this->db->select("*");
				$this->db->from('sales_receipts_list as srl');
				$this->db->join('sales_receipts as sr', 'srl.srec_id = sr.srec_id' ,'left');
				$this->db->where("srl.srecl_inv_no",$value['inv_id']);

				if($data['cust_id']) {
					$this->db->where("sr.cust_id",$data['cust_id']);

				}

				$queryBd = $this->db->get();
				$baddebt= $queryBd->result_array();
				
				foreach($baddebt as $keyBd=>$valueBd) {
					$sr_baddebts_amt+=($valueBd['srec_baddebts_amt']* $valueBd['sr_inr_value']);
				}         
			}
			
			$baddebt['bad_debt']=$sr_baddebts_amt;
			echo "[".json_encode($baddebt,JSON_NUMERIC_CHECK )."]";
			exit();			
		}
	
		public function get_ytod_graph_data() {
	
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$result['ytd_start_date']=$this->Business_analytics_model->getYTDStart($data);
				$result['ytd_end_date']=$this->Business_analytics_model->getYTDEnd($data);

				if(count( $result['ytd_start_date'])<=0){
					if($data['ytd_start_date']=="" && $data['ytd_start_date1']==""){
						$date=date('F-Y');
						$dateRev=date('m-Y',strtotime("-2 month -1 year"));
						$dateRevEnd=date('m-Y',strtotime("-1 year"));
						$revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));
					} else {
						$date=date('F-Y',strtotime("01-".$data['ytd_start_date']));
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));
						$dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));
						$dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));
					}
					$result['ytd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
					$result['ytd_start_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_start_date']['revenueSatart']=$dateRev;
					$result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['ytd_start_date']=="" && $data['ytd_start_date1']=="")	{
						$revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));
						$dateRev=date('m-Y',strtotime("-2 month -1 year"));
						$dateRevEnd=date('m-Y',strtotime("-1 year"));
					} else {
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));
						$dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));
						$dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));
					}
					$result['ytd_start_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_start_date']['revenueSatart']=$dateRev;
					$result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;
				}

				if(count( $result['ytd_end_date'])<=0){
					if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){
						$date=date('F-Y');
						$revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');
						$revenueEndStart=date('m-Y',strtotime("-2 month"));
						$revenueEnd=date('m-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['ytd_end_date']));
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));
						$revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));
						$revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));
					}
					$result['ytd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
					$result['ytd_end_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_end_date']['revenueEndStart']=$revenueEndStart;
					$result['ytd_end_date']['revenueEnd']=$revenueEnd;
				} else {
					if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){
						$revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');
						$revenueEndStart=date('m-Y',strtotime("-2 month"));
						$revenueEnd=date('m-Y');
					} else {
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));
						$revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));
						$revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));
					}
					$result['ytd_end_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_end_date']['revenueEndStart']=$revenueEndStart;
					$result['ytd_end_date']['revenueEnd']=$revenueEnd;
				}
				$result['ytd_select']=$data['ytd_select'];
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
	
		public function get_credit_notes_ytd(){

			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(isset($data['reg_id'])){
					unset($data['reg_id']);
				}
				
				$startDt=date('Y-m',strtotime("1-".$data['start_date']));
				$startDtEnd=date('Y-m',strtotime("1-".$data['end_date']));
				$this->db->select("*");
				$this->db->from('sales_invoices as inv');	
				$this->db->where("inv.bus_id",$data['bus_id']);	
				$this->db->where("inv.status","Active");
				$this->db->where("inv.inv_document_type ",'Sales Invoice');
				$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') >=",$startDt);
				$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') <=",$startDtEnd);
				
				if($data['cust_id']){
					$this->db->where("inv.cust_id",$data['cust_id']);
				}
				
				$query = $this->db->get();
				$result= $query->result_array();
				$cd_amt=0;
	
				foreach($result as $key=>$value){
					$this->db->select("*");
					$this->db->from('sales_credit_debit as cd');
					$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'left');
					$this->db->where("cd.bus_id",$data['bus_id']);
					$this->db->where("cd.cd_invoice_no",$value['inv_invoice_no']);
		
					if($data['cust_id']){
						$this->db->where("cd.cust_id",$data['cust_id']);
					}
					
					$querycr = $this->db->get();
					$credit= $querycr->result_array();
					
					foreach($credit as $keyCr=>$valueCr){
						$cd_amt+=($valueCr['cdl_taxable_amt'] * $valueCr['cdn_inr_value']);
					}
				}      
				$credit['credit_note']=	$cd_amt;
	
				echo "[".json_encode($credit,JSON_NUMERIC_CHECK )."]";
				exit();	
			}
		}
	
		public function get_bad_debts_ytd(){

			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(isset($data['reg_id'])){
					unset($data['reg_id']);
				}
				
				$startDt=date('Y-m',strtotime("1-".$data['start_date']));
				$startDtEnd=date('Y-m',strtotime("1-".$data['end_date']));
				$this->db->select("*");
				$this->db->from('sales_invoices as inv');	
				$this->db->where("inv.bus_id",$data['bus_id']);	
				$this->db->where("inv.status","Active");
				$this->db->where("inv.inv_document_type ",'Sales Invoice');
				$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') >=",$startDt);
				$this->db->where("DATE_FORMAT(inv.inv_invoice_date, '%Y-%m') <=",$startDtEnd);
		
				if($data['cust_id']){
					$this->db->where("inv.cust_id",$data['cust_id']);
				}
				
				$query = $this->db->get();
				$result= $query->result_array();
				$sr_baddebts_amt=0;

				foreach($result as $key=>$value){
					$this->db->select("*");
					$this->db->from('sales_receipts_list as srl');
					$this->db->join('sales_receipts as sr', 'srl.srec_id = sr.srec_id' ,'left');
					$this->db->where("srl.srecl_inv_no",$value['inv_id']);
					
					if($data['cust_id']) {
						$this->db->where("sr.cust_id",$data['cust_id']);
					}
		
					$queryBd = $this->db->get();
					$baddebt= $queryBd->result_array();

					foreach($baddebt as $keyBd=>$valueBd){
						$sr_baddebts_amt+=($valueBd['srec_baddebts_amt']* $valueBd['sr_inr_value']);
					}         
				}
				
				$baddebt['bad_debt']=$sr_baddebts_amt;

				echo "[".json_encode($baddebt,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
	
		public function get_interval_tables_graph_data() {
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
			
				$result=$this->Business_analytics_model->getIntTables($data);
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
	
				echo "[".json_encode($result,JSON_NUMERIC_CHECK)."]";
				exit();
			}
		}
		
		public function get_item_revenue_contribution_graph_data() {
			
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getItmRevenue($data);
				//print $this->db->last_query();
				if(count( $result['itm_revenue'])<=0) {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']=="") {
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_items_credit_notes(){

         $data=$this->input->post();
		 if($data) {
			
			$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
			if($auth[0]->auth_token != $data['token']) {
				echo'hacker alert';
				exit();
			}
			unset($data['token']);
			
			if(isset($data['reg_id'])){
				unset($data['reg_id']);
			}
			
			if(date('m') > 6) {
				$year = date('Y')."-".(date('Y') +1);
				$curr_start_date=date('Y-04-01');
				$curr_end_date=date('Y-03-31',strtotime('+1 year'));
				$prev_start_date=date('Y-03-31');
				$prev_end_date=date('Y-04-01',strtotime('1 year'));
			} else {
				$year = (date('Y')-1)."-".date('Y');
				$curr_start_date=date('Y-04-01',strtotime('-1 year'));
				$curr_end_date=date('Y-03-31');
				$prev_start_date=date('Y-04-01',strtotime('-2 year'));
				$prev_end_date=date('Y-03-31',strtotime('-1 year'));
			}
			$startDt=date("Y-m-d",strtotime($data['start_date']));
			$startDtEnd=date("Y-m-d",strtotime($data['end_date']));
			
			if($data['start_date']!="" && $data['end_date']!="") {
				$this->db->select("*");
				$this->db->from('sales_credit_debit as cd');
				$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'inner');
				$this->db->where("DATE_FORMAT(cd.cd_note_date, '%Y-%m') >=",$startDt);
				$this->db->where("DATE_FORMAT(cd.cd_note_date, '%Y-%m') <=",$startDtEnd);
				$this->db->where("cdl.service_id",$data['service_id']);
			} else {
				$this->db->select("*");
				$this->db->from('sales_credit_debit as cd');
				$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'inner');
				$this->db->where("DATE_FORMAT(cd.cd_note_date, '%Y-%m') >=",$curr_start_date);
				$this->db->where("DATE_FORMAT(cd.cd_note_date, '%Y-%m') <=",$curr_end_date);
				$this->db->where("cdl.service_id",$data['service_id']);
			}
			$query = $this->db->get();
			$result= $query->result_array();
			$cd_amt=0;

		   foreach($result as $key=>$value){
              $cd_amt+=($value['cdl_taxable_amt'] * $value['cdn_inr_value']);
		   }

			$result['credit_note']=	$cd_amt;
			echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
			exit();	

		
		 }
	}

		public function get_client_revenue_contribution_graph_data() {
			
			$data = $this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getCustRevenue($data);
    
				if(count( $result['cust_revenue'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']=="") {
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
		
		public function get_last_target() {
			
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}

				$result['last_actual_target']=$this->Business_analytics_model->getTarget($data);

				if(count( $result['last_actual_target'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
		
		public function get_last_year_revenue(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(isset($data['reg_id'])){
					unset($data['reg_id']);
				}
				
				$result=$this->Business_analytics_model->get_last_year_revenue($data);
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
	
	
		//Credit History graphs functions
	
		public function get_credit_history(){
			
			$post = $this->input->post();
			if($post) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$post['reg_id']."'");
				if($auth[0]->auth_token != $post['token']) {
					echo'hacker alert';
					exit();
				}
				unset($post['token']);
				
				//$field_pos=array("inv_id"=>'0',"cust_name"=>'1',"inv_grand_total"=>'2',"cust_credit_period"=>'3',"inv_invoice_no"=>4);	
				//$sort_field=array_search($post['order'][0]['column'],$field_pos);
				$post['length']=-1;
				//if($post['order'][0]['dir']=='asc') {
				//	$orderBy="ASC";
				//} else {
				//	$orderBy="DESC";
				//}	
				$TotalRecord=$this->Business_analytics_model->CreditHistoryFilter($post,"","",0);	
				$userData = $this->Business_analytics_model->CreditHistoryFilter($post,"","",1);
				$iTotalRecords = $TotalRecord['NumRecords'];
				$records = array();
				$records["data"] = array();
				
				foreach ($userData as $key => $value) {
	
					$inv_date='';
					if($value['createdat']!='') {
						$dateTime = date("d M, Y",  strtotime($value['createdat']));
						if($value['createdat']=='0000-00-00') {
							$dateTime='';
						}
						$inv_date = $dateTime;
					}

					$invData = $this->Business_analytics_model->CreditHistoryFilter($post,"","",1,0,$value['cust_id']);
					$pending_total = array();
					$invtotal = array();
					$intervalDays = array();
					$remove_inv_count = 0;
				
					for($i=0; $i<count($invData); $i++) {
						$now = time(); // or your date as well
						$your_date = strtotime($invData[$i]['inv_invoice_date']);
						$datediff = $now - $your_date;
						$days = round($datediff / (60 * 60 * 24));
						$intervalDays[$i] = $days - $invData[$i]['cust_credit_period'];
						$invtotal[$i] = $invData[$i]['inv_grant_total'];
					
						if($intervalDays[$i] < 1) {
							$pending_total[$i] = 0;
							$invtotal[$i] = 0;
							$intervalDays[$i] = 0;
							$remove_inv_count ++;
						} else {
							$sales_receipt=$this->Sales_model->getSalesReceiptAmt($invData[$i]['inv_id']);
							$credit_note=$this->Sales_model->getCreditNoteAmt($invData[$i]['inv_invoice_no'],$value['cust_id']);
							$sr_amt=0;
							$sr_tds_amt=0;
							$sr_baddebts_amt=0;
							$sr_bccharges_amt=0;
							$sr_penalty_amt=0;
							$sr_interest_amt=0;

							foreach($sales_receipt as $key=>$val){
								$sr_amt+=$val['srec_amount']*$val['sr_inr_value'];
								$sr_tds_amt+=($val['srec_tds_amt']*$val['sr_inr_value']);
								$sr_baddebts_amt+=($val['srec_baddebts_amt']*$val['sr_inr_value']);
								$sr_bccharges_amt+=($val['srec_bcharges_amt']*$val['sr_inr_value']);
								$sr_penalty_amt+=($val['srec_penalty_charges']*$val['sr_inr_value']);
								$sr_interest_amt+=($val['srec_interest_charges']*$val['sr_inr_value']);
							}

							$saleRec[$i]=($sr_amt+$sr_tds_amt+$sr_baddebts_amt+$sr_bccharges_amt+$sr_penalty_amt+$sr_interest_amt);
							$pending_total[$i]=round(($invData[$i]['inv_grant_total']*$invData[$i]['inv_inr_value'])-$saleRec[$i]-($credit_note*$invData[$i]['inv_inr_value']));
	
							if($pending_total[$i] <= 0) {
								$pending_total[$i] = 0;
								$invtotal[$i] = 0;
								$intervalDays[$i] = 0;
								$remove_inv_count ++;
							} else {
							//$remove_inv_count ++;
							}
						}
					}
					$total_inv = count($invData) - $remove_inv_count;	
					if($total_inv > 0) {
						if(count($intervalDays)>0) {
							$intervalDays = array_sum($intervalDays)/$total_inv;
						}
						if(count($invtotal)>0) {
							$invtotal = array_sum($invtotal);
						}
						if(count($pending_total)>0) {
							$pending_total = array_sum($pending_total);
						}
						$percent_pending = ($pending_total/$value['amount'])*100;
						$percent = number_format((float)$percent_pending, 2, '.', '');
					
						if($pending_total > 0) {
							$records["data"][] = array(
														$value['cust_name'],
														$value['amount'],
														$pending_total,
														$percent,
														round($intervalDays),
													);
						}		 
					}
				}
				//$records["draw"] = intval($post['draw']);
				$records["recordsTotal"] = count($records["data"]);
				$records["recordsFiltered"] = count($records["data"]);		
	
				echo "[".json_encode($records)."]";	  
				exit();
			}
		}
	
		public function get_credit_history_graph(){
		$post = $this->input->post();
		if($post) {
			
			$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$post['reg_id']."'");
			if($auth[0]->auth_token != $post['token']) {
				echo'hacker alert';
				exit();
			}
			unset($post['token']);
			
			$userData = $this->Business_analytics_model->CreditHistoryInvoiceGraph($post);
			$records = array();
			$recordInt=array();

			$totalPending15=0;
			$totalPending30=0;
			$totalPending45=0;
			$totalPending60=0;
			$totalPending65=0;
			$count=0;

			foreach ($userData as $key => $value) {
					$inv_date='';
					if($value['inv_invoice_date']!='') {
						$dateTime = date("d M, Y",  strtotime($value['inv_invoice_date']));
						if($value['inv_invoice_date']=='0000-00-00') {
							$dateTime='';
						}
						$inv_date = $dateTime;
					}
					$now = time(); // or your date as well
					$your_date = strtotime($value['inv_invoice_date']);
					$datediff = $now - $your_date;
					$days = round($datediff / (60 * 60 * 24));
					$intervalDays = $days - $value['cust_credit_period'];
					$sales_receipt=$this->Sales_model->getSalesReceiptAmt($value['inv_id']);
					$credit_note=$this->Sales_model->getCreditNoteAmt($value['inv_invoice_no'],$value['cust_id']);          
					$sr_amt=0;
					$sr_tds_amt=0;
					$sr_baddebts_amt=0;
					$sr_bccharges_amt=0;
					$sr_penalty_amt=0;
					$sr_interest_amt=0;

					foreach($sales_receipt as $key=>$val){
						$sr_amt+=($val['srec_amount'] * $val['sr_inr_value']);
						$sr_tds_amt+=($val['srec_tds_amt'] * $val['sr_inr_value']);
						$sr_baddebts_amt+=($val['srec_baddebts_amt'] * $val['sr_inr_value']);
						$sr_bccharges_amt+=($val['srec_bcharges_amt']* $val['sr_inr_value']);
						$sr_penalty_amt+=($val['srec_penalty_charges']* $val['sr_inr_value']);
						$sr_interest_amt+=($val['srec_interest_charges']* $val['sr_inr_value']);
					}
					$saleRec=($sr_amt+$sr_tds_amt+$sr_baddebts_amt+$sr_bccharges_amt+$sr_penalty_amt+$sr_interest_amt);
					$pending_total=round(($value['inv_grant_total'] * $value['inv_inr_value'])-$saleRec-$credit_note);
					$percent_pending = ($pending_total/($value['inv_grant_total']*$value['inv_inr_value']))*100;
					$percent = number_format((float)$percent_pending, 2, '.', '');
					$inv_no_date = $value['inv_invoice_no'].'</br>'.$value['inv_invoice_date'];

					$array=array(
									'interval'=>$intervalDays,
									'pending'=>$pending_total,
								);
               
					array_push($recordInt, $array);  
				}	
			
				for ($i=0;$i<count($recordInt);$i++){
					if($recordInt[$i]["interval"]>=0 && $recordInt[$i]["interval"] <= 15 ){
						$interval = '0-15';
						$count +=1;
						$totalPending15 +=$recordInt[$i]["pending"];
						if($totalPending15 > 0){
							$totalPending15=$totalPending15;
						}else{
							$totalPending15=0;
						}
						$records["interval_0_15"]=$interval;
						$records["pending_0_15"]=$totalPending15;
					}
	
					if($recordInt[$i]["interval"]>15 && $recordInt[$i]["interval"] <= 30 ){
						$interval = '16-30';
						$count +=1; 	   
						$totalPending30 +=$recordInt[$i]["pending"];
						if($totalPending30 > 0){
							$totalPending30=$totalPending30;
						} else {
							$totalPending30=0;
						}
						$records["interval_16_30"]=$interval;
						$records["pending_16_30"]=$totalPending30;
					}
					if($recordInt[$i]["interval"]>30 && $recordInt[$i]["interval"] <= 45 ){
						$interval = '31-45';
						$count +=1;
						$totalPending45 +=$recordInt[$i]["pending"];
						if($totalPending45 > 0){
							$totalPending45=$totalPending45;
						} else {
							$totalPending45=0;
						}
						$records["interval_31_45"]=$interval;
						$records["pending_31_45"]=$totalPending45;
					}
					if($recordInt[$i]["interval"]>45 && $recordInt[$i]["interval"] <= 60 ){
						$interval = '46-60';
						$count +=1;
						$totalPending60 +=$recordInt[$i]["pending"];
						if($totalPending60 > 0){
							$totalPending60=$totalPending60;
						} else {
							$totalPending60=0;
						}
						$records["interval_46_60"]=$interval;
						$records["pending_46_60"]=$totalPending60;
					}
					if($recordInt[$i]["interval"] > 60 ){
						$interval = '60+';
						$count +=1;
						$totalPending65 +=$recordInt[$i]["pending"];			
						if($totalPending65 > 0){
							$totalPending65=$totalPending65;
						} else {
							$totalPending65=0;
						}
						$records["interval_60"]=$interval;
						$records["pending_60"]=$totalPending65;
					}
				}
					$records['count']=$count;
			
					echo "[".json_encode($records)."]";  
					exit;
				}
			}
		
		public function get_credit_history_invoice(){
		$post = $this->input->post();
		if($post) {
			
			$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$post['reg_id']."'");
			if($auth[0]->auth_token != $post['token']) {
				echo'hacker alert';
				exit();
			}
			unset($post['token']);
			
			//$field_pos=array("inv_id"=>'0',"cust_name"=>'1',"inv_grand_total"=>'2',"cust_credit_period"=>'3',"inv_invoice_no"=>4);	
			//$sort_field=array_search($post['order'][0]['column'],$field_pos);
			
			//if($post['order'][0]['dir']=='asc')	{
			//	$orderBy="ASC";
			//} else {
			//	$orderBy="DESC";
			//}	
			$post['length'] = -1;
			$TotalRecord=$this->Business_analytics_model->CreditHistoryInvoiceFilter($post,"","",0);	
			$userData = $this->Business_analytics_model->CreditHistoryInvoiceFilter($post,"","",1);
			
			$iTotalRecords = $TotalRecord['NumRecords'];
			$records = array();
			$records["data"] = array();

			foreach ($userData as $key => $value) {
				$inv_date='';
				if($value['createdat']!='') {
					$dateTime = date("d M, Y",  strtotime($value['inv_invoice_date']));
					if($value['createdat']=='0000-00-00'){
						$dateTime='';
					}
					$inv_date = $dateTime;
				}
				$now = time(); // or your date as well
				$your_date = strtotime($value['inv_invoice_date']);
				$datediff = $now - $your_date;
				$days = round($datediff / (60 * 60 * 24));
				$intervalDays = $days - $value['cust_credit_period'];
				$sales_receipt=$this->Sales_model->getSalesReceiptAmt($value['inv_id']);
				$credit_note=$this->Sales_model->getCreditNoteAmt($value['inv_invoice_no'],$value['cust_id']);
				$sr_amt=0;
				$sr_tds_amt=0;
				$sr_baddebts_amt=0;
				$sr_bccharges_amt=0;
				$sr_penalty_amt=0;
				$sr_interest_amt=0;

				foreach($sales_receipt as $key=>$val){
					$sr_amt+=$val['srec_amount'];
					$sr_tds_amt+=$val['srec_tds_amt'];
					$sr_baddebts_amt+=$val['srec_baddebts_amt'];
					$sr_bccharges_amt+=$val['srec_bcharges_amt'];
					$sr_penalty_amt+=$val['srec_penalty_charges'];
					$sr_interest_amt+=$val['srec_interest_charges'];
				}
				$saleRec=($sr_amt+$sr_tds_amt+$sr_baddebts_amt+$sr_bccharges_amt+$sr_penalty_amt+$sr_interest_amt);
				$pending_total=round(($value['inv_grant_total'])-$saleRec-$credit_note);
				$percent_pending = ($pending_total/$value['inv_grant_total'])*100;
				$percent = number_format((float)$percent_pending, 2, '.', '');
				$inv_no_date = $value['inv_invoice_no'];
				$pending_with_zero_amt = 0;
				
				if($pending_total > 0) {	
					if($intervalDays <= 15)	{
						$interval = '0-15';
						$sortkey = 1;
					} else if($intervalDays <= 30) {
						$interval = '16-30';
						$sortkey = 2;
					} else if($intervalDays <= 45) {
						$interval = '31-45';
						$sortkey = 3;
					} else if($intervalDays <= 60) {
						$interval = '46-60';
						$sortkey = 4;
					} else if($intervalDays > 60) {
						$interval = '60 +';
						$sortkey = 5;
					}
					if($post['cp_interval'] != '') {
						if($post['cp_interval'] == 1) {
							if($intervalDays >= 0 && $intervalDays <=15) {
								$records["data"][] = array(
															
															$interval,
															//$value['cust_name'],
															//$inv_no_date,
															$pending_total,
															//$percent,
															$intervalDays,
															$value['cust_name'],
												$inv_no_date,
															
														  );
							}
						}
						if($post['cp_interval'] == 2) {
							if($intervalDays >= 15 && $intervalDays <= 30) {
								$records["data"][] = array(
															
															$interval,
															//$value['cust_name'],
															//$inv_no_date,
															$pending_total,
															//$percent,
															$intervalDays,
															$value['cust_name'],
												$inv_no_date,
															
														  );
							}
						}
						if($post['cp_interval'] == 3) {
							if($intervalDays >= 30 && $intervalDays <=45) {
								$records["data"][] = array(
													
													$interval,
													//$value['cust_name'],
													//$inv_no_date,
													$pending_total,
													//$percent,
													$intervalDays,
													$value['cust_name'],
												$inv_no_date,
													
												  );
							}
						}
						if($post['cp_interval'] == 4) {
							if($intervalDays >= 45 && $intervalDays <= 60) {
								$records["data"][] = array(
													
													$interval,
													//$value['cust_name'],
													//$inv_no_date,
													$pending_total,
													//$percent,
													$intervalDays,
													$value['cust_name'],
												$inv_no_date,
													
												  );
							}
						}
						if($post['cp_interval'] == 5) {
							if($intervalDays >= 60) {
								$records["data"][] = array(
													
													$interval,
													//$value['cust_name'],
													//$inv_no_date,
													$pending_total,
													//$percent,
													$intervalDays,
													$value['cust_name'],
												$inv_no_date,
													
												  );
							}
						}
				}
				if($post['cp_interval'] == "") {
					if($intervalDays >= 0) {
						$records["data"][] = array(
												
												$interval,
												
												$pending_total,
												//$percent,
												$intervalDays,

												$value['cust_name'],
												$inv_no_date,
												
												//$sortkey,
											   );
					}
				}
			} else {
				$pending_with_zero_amt ++;
			}
		}	
		
		/*if($post['cp_interval'] == "") {
			$res = $this->array_sort($records['data'], '8', SORT_DESC);
			$cot=0;
			$result=[];
			foreach ($res as $value) {
				array_pop($value);
				$result[$cot] = $value;
				$cot++;
			}
			$records['data'] = $result;
		}*/
		
	  	//$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($records['data']);
		$records["recordsFiltered"] = count($records['data']);		
		
		echo "[".json_encode($records)."]";				  

		exit;
		}
	}
		
		public function array_sort($array, $on, $order=SORT_DESC){

			$new_array = array();
			$sortable_array = array();
	
			if (count($array) > 0) {
				foreach ($array as $k => $v) {
					if (is_array($v)) {
						foreach ($v as $k2 => $v2) {
							if ($k2 == $on) {
								$sortable_array[$k] = $v2;
							}
						}
					} else {
						$sortable_array[$k] = $v;
					}
				}
	
				switch ($order) {
					case SORT_ASC:
						asort($sortable_array);
						break;
					case SORT_DESC:
						arsort($sortable_array);
						break;
				}
	
				foreach ($sortable_array as $k => $v) {
					$new_array[$k] = $array[$k];
				}
			}	
			return $new_array;
		}
	
	
		//Vendor & Expense graph functions
	
		public function get_mtd_vendor(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$result['mtd_start_date']=$this->Business_analytics_model->getMTDStart_vendor($data);
				$result['mtd_end_date']=$this->Business_analytics_model->getMTDEnd_vendor($data);

				if(count( $result['mtd_start_date'])<=0){
					if($data['mtd_start_date']==""){
					$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_start_date']));
					}
					$result['mtd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}
				if(count( $result['mtd_end_date'])<=0){
					if($data['mtd_end_date']==""){
						$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_end_date']));
					}	
					$result['mtd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}
				$result['mtd_select']=$data['mtd_select'];
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
			
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
	
		public function get_ytd_vendor(){
			$data=$this->input->post();
			if($data) {
     
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
	 
				$result['ytd_start_date']=$this->Business_analytics_model->getYTDStart_vendor($data);
				$result['ytd_end_date']=$this->Business_analytics_model->getYTDEnd_vendor($data);

				if(count( $result['ytd_start_date'])<=0){
					if($data['ytd_start_date']=="" && $data['ytd_start_date1']==""){
						$date=date('F-Y');
						$dateRev=date('m-Y',strtotime("-2 month -1 year"));
						$dateRevEnd=date('m-Y',strtotime("-1 year"));
						$revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));
					} else {
						$date=date('F-Y',strtotime("01-".$data['ytd_start_date']));
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));
						$dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));
						$dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));
					}
					$result['ytd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
					$result['ytd_start_date']['revenuePeriod']=$revenuePeriod;	
					$result['ytd_start_date']['revenueSatart']=$dateRev;
					$result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['ytd_start_date']=="" && $data['ytd_start_date1']==""){
						$revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));
						$dateRev=date('m-Y',strtotime("-2 month -1 year"));
						$dateRevEnd=date('m-Y',strtotime("-1 year"));
					} else {
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));
						$dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));
						$dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));
					}
					$result['ytd_start_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_start_date']['revenueSatart']=$dateRev;	
					$result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;
				}
			
				if(count( $result['ytd_end_date'])<=0) {
					if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){
						$date=date('F-Y');
						$revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');
						$revenueEndStart=date('m-Y',strtotime("-2 month"));
						$revenueEnd=date('m-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['ytd_end_date']));
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));
						$revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));
						$revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));
					}
					$result['ytd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
					$result['ytd_end_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_end_date']['revenueEndStart']=$revenueEndStart;
					$result['ytd_end_date']['revenueEnd']=$revenueEnd;	
				} else {
					if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){
						$revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');
						$revenueEndStart=date('m-Y',strtotime("-2 month"));	
						$revenueEnd=date('m-Y');
					} else {
						$revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));
						$revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));
						$revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));
					}
					$result['ytd_end_date']['revenuePeriod']=$revenuePeriod;
					$result['ytd_end_date']['revenueEndStart']=$revenueEndStart;
					$result['ytd_end_date']['revenueEnd']=$revenueEnd;
				}
				$result['ytd_select']=$data['ytd_select'];
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}
		
		public function get_debit_notes(){
			
			$data=$this->input->post();
			if($data){
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(isset($data['reg_id'])){
					unset($data['reg_id']);
				}
				
				$date=date('Y-m',strtotime($data['revenueDate']));
				$this->db->select("*");
				$this->db->from('company_expense as exp'); 
				$this->db->where("exp.bus_id",$data['bus_id']);  
				$this->db->where("exp.status","Active");
				$this->db->where("DATE_FORMAT(exp.ce_date, '%Y-%m') =",$date);

				if($data['cust_id']){
					$this->db->where("exp.ce_vendorname",$data['cust_id']);
				}
				$query = $this->db->get();
				$result= $query->result_array();
				$cd_amt=0;
		
				foreach($result as $key=>$value){
					$this->db->select("*");
					$this->db->from('expense_debit_note as cd');
					$this->db->join('expense_debit_note_list as cdl', 'cd.debit_id = cdl.debit_id' ,'left');
					$this->db->where("cd.bus_id",$data['bus_id']);
					$this->db->where("cd.debit_invoice_no",$value['debit_invoice_no']);
					
					if($data['cust_id']){
						$this->db->where("cd.cust_id",$data['cust_id']);
					}
					$querycr = $this->db->get();
					$credit= $querycr->result_array();
					
					foreach($credit as $keyCr=>$valueCr){
						$cd_amt+=($valueCr['cdl_taxable_amt'] * $valueCr['cdn_inr_value']);
					}
				}
				$credit['debit_note']= $cd_amt;

				echo "[".json_encode($credit,JSON_NUMERIC_CHECK)."]";
				exit(); 
			}
		}

		public function get_debit_notes_ytd(){
			
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if(isset($data['reg_id'])){
					unset($data['reg_id']);
				}
				
				$startDt=date('Y-m',strtotime("1-".$data['start_date']));
				$startDtEnd=date('Y-m',strtotime("1-".$data['end_date']));		
				 $this->db->select("*");

    $this->db->from('company_expense as exp'); 

    $this->db->where("exp.bus_id",$data['bus_id']); 
        //$this->db->where("inv.bus_id",$data['bus_id']); 

    $this->db->where("exp.status","Active");
   

    $this->db->where("DATE_FORMAT(exp.ce_date, '%Y-%m') >=",$startDt);

    $this->db->where("DATE_FORMAT(exp.ce_date, '%Y-%m') <=",$startDtEnd);

    if($data['cust_id']){

    $this->db->where("exp.ce_vendorname",$data['cust_id']);

    }



    $query = $this->db->get();

  

    $result= $query->result_array();

    

    $cd_amt=0;

    foreach($result as $key=>$value){





         $this->db->select("*");

     $this->db->from('expense_debit_note as cd');

    $this->db->join('expense_debit_note_list as cdl', 'cd.debit_id = cdl.debit_id' ,'left');

    $this->db->where("cd.bus_id",$data['bus_id']);
       // $this->db->where("inv.bus_id",$data['bus_id']); 

    
    $this->db->where("cd.debit_invoice_no",$value['ce_number']);

    if($data['cust_id']){

    $this->db->where("cd.vendor_id",$data['cust_id']);

    }



    $querycr = $this->db->get();

    

    $credit= $querycr->result_array();

    

       foreach($credit as $keyCr=>$valueCr){

              $cd_amt+=($valueCr['dbl_taxable_amt'] * $valueCr['debit_inr_value']);

       }



    }      



    

    $credit['debit_note']= $cd_amt;
			
				echo "[".json_encode($credit,JSON_NUMERIC_CHECK)."]";
				exit();
			}
		}

		public function get_int_tables_vendor() {
		
			$data=$this->input->post();
			if($data) {
			
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
			
				$result=$this->Business_analytics_model->getIntTables_vendor($data);
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_expense_revenue(){
			
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getExpRevenue($data);
				if(count( $result['exp_revenue'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_expense_debit_notes(){

			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$startDt=date("Y-m-d",strtotime($data['start_date']));
				$startDtEnd=date("Y-m-d",strtotime($data['end_date']));
				
				 if($data['start_date']!="" && $data['end_date']!=""){

         $this->db->select("*");

    

     $this->db->from('expense_debit_note as cd');

    $this->db->join('expense_debit_note_list as cdl', 'cd.debit_id = cdl.debit_id' ,'inner');


    //$this->db->where("cd.cd_invoice_no",$data['inv_no']);

    $this->db->where("DATE_FORMAT(cd.debit_note_date, '%Y-%m') >=",$startDt);

    $this->db->where("DATE_FORMAT(cd.debit_note_date, '%Y-%m') <=",$startDtEnd);

    //$this->db->where("cd.cust_id",$data['cust_id']);

    $this->db->where("cdl.dbl_service_type",$data['service_id']);

  }else{

     $this->db->select("*");

     $this->db->from('expense_debit_note as cd');

    $this->db->join('expense_debit_note_list as cdl', 'cd.debit_id = cdl.debit_id' ,'inner');

    

    //$this->db->where("cd.cd_invoice_no",$data['inv_no']);

    $this->db->where("DATE_FORMAT(cd.debit_note_date, '%Y-%m') >=",$curr_start_date);

    $this->db->where("DATE_FORMAT(cd.debit_note_date, '%Y-%m') <=",$curr_end_date);

    //$this->db->where("cd.cust_id",$data['cust_id']);

    $this->db->where("cdl.dbl_service_type",$data['service_id']);



  }
				$query = $this->db->get();
				//print $this->db->last_query();exit;
				$result= $query->result_array();
				$cd_amt=0;

				foreach($result as $key=>$value){
					$cd_amt+=($value['dbl_taxable_amt'] * $value['debit_inr_value']);
				}
				//print $cd_amt;exit;
				$result['debit_note']= $cd_amt;

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_vendor_revenue(){

			$data=$this->input->post();
			if($data) {
		
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);

				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getVendRevenue($data);

				if(count( $result['ven_revenue'])<=0){	
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_last_target_vendor(){

			$data=$this->input->post();
			if($data) {
 
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
 
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result['last_actual_target']=$this->Business_analytics_model->getTarget_vendor($data);
				
				if(count( $result['last_actual_target'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_last_year_revenue_vendor(){

			$data=$this->input->post();
			if($data) {

				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);

				$result=$this->Business_analytics_model->get_last_year_revenue_vendor($data);

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}


		//Employee graphs functions

		public function get_mtd_employee(){
			$data=$this->input->post();
			
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$result['mtd_start_date']=$this->Business_analytics_model->getMTDStart_employee($data);
				$result['mtd_end_date']=$this->Business_analytics_model->getMTDEnd_employee($data);
				
				if(count( $result['mtd_start_date'])<=0){
					if($data['mtd_start_date']==""){
						$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_start_date']));
					}
					$result['mtd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}
				if(count( $result['mtd_end_date'])<=0){
					if($data['mtd_end_date']==""){
						$date=date('F-Y');
					} else {
						$date=date('F-Y',strtotime("01-".$data['mtd_end_date']));
					}
					$result['mtd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );
				}
				$result['mtd_select']=$data['mtd_select'];
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_ytd_employee(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				 $result['ytd_start_date']=$this->Business_analytics_model->getYTDStart_employee($data);

     $result['ytd_end_date']=$this->Business_analytics_model->getYTDEnd_employee($data);



    if(count( $result['ytd_start_date'])<=0){

            if($data['ytd_start_date']=="" && $data['ytd_start_date1']==""){

              $date=date('F-Y');

              $dateRev=date('m-Y',strtotime("-2 month -1 year"));

              $dateRevEnd=date('m-Y',strtotime("-1 year"));



              $revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));

             }else{

              $date=date('F-Y',strtotime("01-".$data['ytd_start_date']));

              $revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));

              $dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));

              $dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));

             }



            

       $result['ytd_start_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );

       $result['ytd_start_date']['revenuePeriod']=$revenuePeriod;

        $result['ytd_start_date']['revenueSatart']=$dateRev;

         $result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;

    }else{



             if($data['ytd_start_date']=="" && $data['ytd_start_date1']==""){

              

              $revenuePeriod=date('M-y',strtotime("-2 month -1 year"))."-".date('M-y',strtotime("-1 year"));

              $dateRev=date('m-Y',strtotime("-2 month -1 year"));

              $dateRevEnd=date('m-Y',strtotime("-1 year"));

             }else{

              

              $revenuePeriod=date('M-y',strtotime("01-".$data['ytd_start_date']))."-".date('M-y',strtotime("01-".$data['ytd_start_date1']));

              $dateRev=date('m-Y',strtotime("01-".$data['ytd_start_date']));

              $dateRevEnd=date('m-Y',strtotime("01-".$data['ytd_start_date1']));

             }



            

      

       $result['ytd_start_date']['revenuePeriod']=$revenuePeriod;

         $result['ytd_start_date']['revenueSatart']=$dateRev;

         $result['ytd_start_date']['revenueStartEnd']=$dateRevEnd;

    }



     if(count( $result['ytd_end_date'])<=0){

             if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){

              $date=date('F-Y');



              $revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');

              $revenueEndStart=date('m-Y',strtotime("-2 month"));

              $revenueEnd=date('m-Y');

             }else{

              $date=date('F-Y',strtotime("01-".$data['ytd_end_date']));

              $revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));

              $revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));

              $revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));

             }



            

       $result['ytd_end_date'][0]= array('actual' =>0 ,'revenueDate'=>$date );

       $result['ytd_end_date']['revenuePeriod']=$revenuePeriod;

       $result['ytd_end_date']['revenueEndStart']=$revenueEndStart;

         $result['ytd_end_date']['revenueEnd']=$revenueEnd;

    }else{



             if($data['ytd_end_date']=="" && $data['ytd_end_date1']==""){

              

              $revenuePeriod=date('M-y',strtotime("-2 month"))."-".date('M-y');

              $revenueEndStart=date('m-Y',strtotime("-2 month"));

              $revenueEnd=date('m-Y');

             }else{

              

              $revenuePeriod=date('M-y',strtotime("01-".$data['ytd_end_date']))."-".date('M-y',strtotime("01-".$data['ytd_end_date1']));

              $revenueEndStart=date('m-Y',strtotime("01-".$data['ytd_end_date']));

              $revenueEnd=date('m-Y',strtotime("01-".$data['ytd_end_date1']));

             }



            

      

       $result['ytd_end_date']['revenuePeriod']=$revenuePeriod;

       $result['ytd_end_date']['revenueEndStart']=$revenueEndStart;

         $result['ytd_end_date']['revenueEnd']=$revenueEnd;

    }



     $result['ytd_select']=$data['ytd_select'];

      $result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_int_tables_employee(){
			$data=$this->input->post();
			if($data){
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				$result=$this->Business_analytics_model->getIntTables_employee($data);

				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_emp_revenue(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getEmpRevenue($data);
				if(count( $result['emp_revenue'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {	
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
			
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}		

		public function get_exp_revenue_employee(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				 $result=$this->Business_analytics_model->getExpRevenueEmp($data);

      if(count( $result['exp_revenue'])<=0){

            if($data['start_date']=="" && $data['end_date']==""){

              

              $dateRev=date('m-Y',strtotime($curr_start_date));

              $dateRevEnd=date('m-Y',strtotime($curr_end_date));



              

             }else{

              

              $dateRev=date('m-Y',strtotime($data['start_date']));

              $dateRevEnd=date('m-Y',strtotime($data['end_date']));

             }



            

      

        $result['revenueSatart']=$dateRev;

         $result['revenueStartEnd']=$dateRevEnd;

    }else{



              if($data['start_date']=="" && $data['end_date']==""){

              

              $dateRev=date('m-Y',strtotime($curr_start_date));

              $dateRevEnd=date('m-Y',strtotime($curr_end_date));



              

             }else{

              

              $dateRev=date('m-Y',strtotime($data['start_date']));

              $dateRevEnd=date('m-Y',strtotime($data['end_date']));

             }



            

      

        $result['revenueSatart']=$dateRev;

         $result['revenueStartEnd']=$dateRevEnd;

    }

   

$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));

				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}

		public function get_exp_vou_revenue_employee(){
			$data=$this->input->post();
			if($data) {
				
				$auth = $this->App_model->selectData("registration","auth_token","reg_id='".$data['reg_id']."'");
				if($auth[0]->auth_token != $data['token']) {
					echo'hacker alert';
					exit();
				}
				unset($data['token']);
				
				if (date('m') > 6) {
					$year = date('Y')."-".(date('Y') +1);
					$curr_start_date=date('Y-04-01');
					$curr_end_date=date('Y-03-31',strtotime('+1 year'));
					$prev_start_date=date('Y-03-31');
					$prev_end_date=date('Y-04-01',strtotime('1 year'));
				} else {
					$year = (date('Y')-1)."-".date('Y');
					$curr_start_date=date('Y-04-01',strtotime('-1 year'));
					$curr_end_date=date('Y-03-31');
					$prev_start_date=date('Y-04-01',strtotime('-2 year'));
					$prev_end_date=date('Y-03-31',strtotime('-1 year'));
				}
				$result=$this->Business_analytics_model->getExpVouRevenueEmp($data);
				if(count( $result['exp_revenue'])<=0){
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
						$dateRevEnd=date('m-Y',strtotime($curr_end_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));	
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				} else {
					if($data['start_date']=="" && $data['end_date']==""){
						$dateRev=date('m-Y',strtotime($curr_start_date));
					} else {
						$dateRev=date('m-Y',strtotime($data['start_date']));
						$dateRevEnd=date('m-Y',strtotime($data['end_date']));
					}
					$result['revenueSatart']=$dateRev;
					$result['revenueStartEnd']=$dateRevEnd;
				}
				$result['business']=$this->Business_analytics_model->selectData('businesslist', '*', array('bus_id'=>$data['bus_id']));
				
				echo "[".json_encode($result,JSON_NUMERIC_CHECK )."]";
				exit();
			}
		}



		public function insert_blog(){
			$data=$this->input->post();


				echo json_encode($data);exit;
		}

	}
	
	

?>