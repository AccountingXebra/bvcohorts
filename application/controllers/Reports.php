<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require ("common/Index_Controller.php");

class Reports extends Index_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('Business_analytics_model');

		setlocale(LC_MONETARY, 'en_US.UTF-8'); 

		is_login();

		$this->user_session = $this->session->userdata('user_session');

		is_trialExpire();

		is_accessAvail(1);
		 is_BusReg();

	}

	public function report_page(){
		$this->load->view('business_analytics/report');
	}
	
}?>