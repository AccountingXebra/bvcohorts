<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Settings extends Index_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Settings_model');
		setlocale(LC_MONETARY, 'en_US.UTF-8');
		is_login();
		is_trialExpire();
		// is_BusReg();
		$this->user_session = $this->session->userdata('user_session');
	}

	public function index()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		//$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id' => $bus_id, 'alert_type' => "customers", 'status' => 'Active'),'','','','','','rowcount');

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', " `bus_id` = ".$bus_id. " and `alert_type` = 'customers' or `alert_type` = 'vendors' and `status` = 'active'") ;
		//echo '<pre>'; print_r($data['alerts']); echo '<pre>'; exit();
		//echo $this->db->last_query(); exit();
		//if($data['alerts']>0){
		//$data['customers'] = $this->Settings_model->selectData('sales_customers', '', array('status'=>'Active','bus_id'=>$bus_id));

		$data['customers'] = $this->Settings_model->unioncusven($bus_id);
		//echo $this->db->last_query(); exit();
		//$data['customers'] = $this->Settings_model->selectData('my_alerts', '*', " `bus_id` = ".$bus_id. " and `alert_type` = 'customers' or `alert_type` = 'vendors' and `status` = 'active'") ;
		//echo '<pre>';print_r($data['customers']); echo '<pre>';exit();

		$this->load->view('settings/alerts/alerts-list',$data);
		// }else{

		// 	redirect('settings/add-client-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');

		// }

	}

	/*-------Start Customer Alert Section -------*/
	public function get_my_alerts(){

		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']=array("customers","vendors");

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Clients Alerts";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End


		$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"cust_name"=>'3',"alert_condition"=>'4',"alert_target"=>'5',"alert_reminder"=>'6');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->myAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->myAlertsFilter($post,$sort_field,$orderBy,1);

	 	//echo '<pre>'; print_r($userData); echo '<pre>'; exit();
		//echo $this->db->last_query(); exit();

	 	// print_r($userData);
	 	// exit;
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$name="";
	  		if($value['alert_type']=='customers'){
	  			$c_name = $this->Settings_model->selectData('sales_customers', '*', array('cust_id'=>$value["parent_id"]));
	  			$name=$c_name[0]->cust_name;
	  		}

	  		if($value['alert_type']=='vendors'){
	  			$c_name = $this->Settings_model->selectData('expense_vendors', '*', array('vendor_id'=>$value["parent_id"]));
	  			$name=$c_name[0]->vendor_name;
	  		}
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple calerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$cust_name = '<a href="'.base_url().'settings/view-client-alert/'.$value['id'].'" >'.$name.'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{

				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
          	$rem_date='';
			if($value['alert_reminder']!='')
			{

				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}

			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_calerts" data-calert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_calerts" data-calert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</a></li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-client-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_alerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-alert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;"><p class="ex">Export</p></a></li>

					<li><a href='.base_url().'settings/print-alert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$cust_name,
				$value['alert_condition'],
				$value['alert_target'],
				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}

	public function add_client_alert()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['parent_name']=="client"){
				$alertpost['alert_type']="customers";
			}else{
				$alertpost['alert_type']="vendors";
			}

			$alertpost['bus_id']=$bus_id;
			//echo "<pre>"; print_r($alertpost);exit();

		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
		//$aflag = $this->Settings_model->insertData('sales_customers',$alertpost);
		if($aflag != false){

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$aflag));
			$tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			$tracking_info['action_taken'] = 'Add';
			$tracking_info['reference'] = $rec_info[0]->alert_number;
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

			$this->session->set_flashdata('success',"Your Client Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}

		redirect('settings');
		}

		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"customers"),'id','DESC');
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('bus_id'=>$bus_id,'status'=>'Active'));

		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("CA","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
		}
		// Activity Tracker start

			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Add';
			 $tracking_info['reference'] = "Client Alert";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
		$data['alertNo']='CA'.$alno_ref;
		$this->load->view('settings/alerts/customer-alerts',$data);
	}

	public function get_mobile_email()
    {
        $data['emp']=$this->Employee_model->selectData('registration', '*',array('reg_id' => $_POST['reg_id']));
        //$data['location']=$this->Employee_model->selectData('states', '*',array('state_id'=>$data['emp'][0]->state));
        echo json_encode($data);
	}

    public function get_client_vendor(){

    	$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();
       if($alertpost['name']=='client'){
       	$data['client']=$this->Settings_model->selectData('sales_customers', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
       	$result['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"customers"),'id','DESC');


		if(count($result['alerts'])>0){
			$last_alerts_no=$result['alerts'][0]->alert_number;
              $temp=str_replace("CA","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		$data['alertNo']='CA'.$alno_ref;
		$data['type']='client';
       }else if($alertpost['name']=='vendor'){
       	$data['client']=$this->Settings_model->selectData('expense_vendors', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
       	$result['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"vendors"),'id','DESC');


		if(count($result['alerts'])>0){
			$last_alerts_no=$result['alerts'][0]->alert_number;
              $temp=str_replace("VA","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		$data['alertNo']='VA'.$alno_ref;
		$data['type']='vendor';
       }else if($alertpost['name']=='item'){

       $data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"services"),'id','DESC');
		$data['services'] = $this->Settings_model->selectData('services','*',array("bus_id"=>$bus_id));
		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("IA","",$last_alerts_no);
				$alno_ref='1';
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);

		}
		// Activity Tracker start

				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = "Add New Item Alert";
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		$data['alertNo']='IA'.$alno_ref;
		$data['type']='item';

       }else if($alertpost['name']=='expense'){

          $data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"expenses"),'id','DESC');
		$data['expenses'] = $this->Settings_model->selectData('expense_list','*',array("bus_id"=>$bus_id));
		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("EA","",$last_alerts_no);
				$alno_ref='1';
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);

		}
		// Activity Tracker start

				 $tracking_info['module_name'] = 'Settings / Set Alert / Expense';
				 $tracking_info['action_taken'] = 'Add';
				 $tracking_info['reference'] = "Add New Expense Alert";
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		$data['alertNo']='EA'.$alno_ref;
		$data['type']='expense';

       }else{
       	//
       }

       echo json_encode($data);

    }


	public function deactive_alerts()
	{
		$id = $this->input->post('calert_id');
		$alert_type = $this->input->post('alert_type');
		$bus_id=$this->user_session['bus_id'];
		$filter = array(
			'status'	=>	$this->input->post('status'),
		);

		$flag=$this->Settings_model->updateData('my_alerts',$filter,array('id'=>$id));
		if($this->input->post('status')=="Inactive" && $alert_type=="services"){
			$alerts = $this->Settings_model->selectData('my_alerts','*',array('id'=>$id));
			$ialerts = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"parent_id"=>$alerts[0]->parent_id,"alert_type"=>"services","status"=>"Active"),'','','','','','rowcount');

			$test=$this->Settings_model->updateData('services',array('service_total_alert'=>$ialerts),array('service_id'=>$alerts[0]->parent_id));
		}
		if($flag != false)
		{
			// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
				 $tracking_info['module_name'] = 'Settings / Set Alert';
				 $tracking_info['action_taken'] = 'Deactivate';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			echo true;
		}
		else
		{
			echo false;
		}
	}

	function deactive_multiple_alerts()
	{
		$alerts_values = $this->input->post('alerts_values');
		$alert_type = $this->input->post('alert_type');
		$alerts_values_array = explode(",",$alerts_values);
		foreach($alerts_values_array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert';
			 $tracking_info['action_taken'] = 'Deactivate';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

			$filter = array(
				'status'	=>	$this->input->post('status'),
			);
			$flag=$this->Settings_model->updateData('my_alerts',$filter,array('id'=>$id));
			if($this->input->post('status')=="Inactive" && $alert_type=="services"){
			$alerts = $this->Settings_model->selectData('my_alerts','*',array('id'=>$id));
			$ialerts = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"parent_id"=>$alerts[0]->parent_id,"alert_type"=>"services","status"=>"Active"),'','','','','','rowcount');

			$test=$this->Settings_model->updateData('services',array('service_total_alert'=>$ialerts),array('service_id'=>$alerts[0]->parent_id));
		}
		}

		echo true;
	}

	function view_client_alert($alert_id){

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['calerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'customers','sales_customers','cust_id');
		if(count($data['calerts'])==0){
			redirect('settings');
		}

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
		//$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$this->load->view('settings/alerts/view-customer-alert',$data);
	}

	function edit_client_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			$alert_id=$alertpost['alert_id'];
			unset($alertpost['alert_id']);
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_date'] ='';
			}


			if($alertpost['parent_name']=="client"){
				$alertpost['alert_type']="customers";
			}else{
				$alertpost['alert_type']="vendors";
			}



		$aflag = $this->Settings_model->updateData('my_alerts',$alertpost,array('id'=>$alert_id));
		if($aflag != false){

			// Activity Tracker start
				 $rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$aflag));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->session->set_flashdata('success',"Your Client Alert has been updated successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings');

		}
		$data['calerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'customers','sales_customers','cust_id');
		if(count($data['calerts'])==0){
			redirect('settings');
		}

		// Activity Tracker start
				 $rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
				 $tracking_info['action_taken'] = 'Edit';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		//$data['calerts'] = $this->Settings_model->selectData('my_alerts','*',array("id"=>$alert_id,"bus_id"=>$bus_id,"alert_type"=>"customers"));
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$this->load->view('settings/alerts/edit-customer-alert',$data);
	}
	/*-------End Customer Alert Section -------*/
	/*-------Start Item Alert Section -------*/
	public function item_alerts()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"services",'status'=>'Active'),'','','','','','rowcount');
		//if($data['alerts']>0){
		$data['services'] = $this->Settings_model->selectData('services','*',array("bus_id"=>$bus_id,"reg_id"=>$reg_id));
		$this->load->view('settings/alerts/item-alerts-list',$data);

		// }else{

		// 	redirect('settings/add-item-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');
		// }
	}

	public function get_item_alerts(){

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Item Alerts List";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']=array("services","expense");

		$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"service_name"=>'3',"alert_condition"=>'4',"alert_target"=>'5',"alert_reminder"=>'6');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->itemAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->itemAlertsFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {

	  		$name="";
	  		if($value['alert_type']=='services'){
	  			$c_name = $this->Settings_model->selectData('services', '*', array('service_id'=>$value["parent_id"]));
	  			$name=@$c_name[0]->service_name;
	  		}

	  		if($value['alert_type']=='expense'){
	  			$c_name = $this->Settings_model->selectData('expense_list', '*', array('exp_id'=>$value["parent_id"]));
	  			$name=@$c_name[0]->exp_name;
	  		}
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple ialerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$item_name =$name;// '<a href="'.base_url().'settings/view-item-alert/'.$value['id'].'" >'.$value['service_name'].'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
          	$rem_date='';
			if($value['alert_reminder']!='')
			{

				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}

			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_ialerts" data-ialert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_ialerts" data-ialert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-item-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_ialerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-ialert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;"><p class="ex">Export</p></a></li>

					<li><a href='.base_url().'settings/print-ialert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$item_name,
				$value['alert_condition'],
				$value['alert_target'],
				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}
	public function add_item_alert()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}

			if($alertpost['parent_name']=="item"){
				$alertpost['alert_type']="services";
			}else{
				$alertpost['alert_type']="expense";
			}

			$alertpost['bus_id']=$bus_id;
			//echo "<pre>"; print_r($alertpost);exit();

		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
		if($aflag != false){

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$aflag));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$ialerts = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"services","status"=>"Active"),'','','','','','rowcount');
			$this->Settings_model->updateData('services',array('service_total_alert'=>$ialerts),array('service_id'=>$alertpost['parent_id'],'status'=>'Active'));

			$this->session->set_flashdata('success',"Your Item Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/item-alerts');

		}
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"services"),'id','DESC');
		$data['services'] = $this->Settings_model->selectData('services','*',array("bus_id"=>$bus_id,'status'=>'Active'));

		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("IA","",$last_alerts_no);
				$alno_ref='001';
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);

		}
		// Activity Tracker start

				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Add';
				 $tracking_info['reference'] = "Add New Item Alert";
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		$data['alertNo']='IA'.$alno_ref;
		$this->load->view('settings/alerts/add-item-alert',$data);
	}

	function edit_item_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			$alert_id=$alertpost['alert_id'];
			unset($alertpost['alert_id']);
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_date'] ='';
			}

			if($alertpost['parent_name']=="item"){
				$alertpost['alert_type']="services";
			}else{
				$alertpost['alert_type']="expense";
			}

		$aflag = $this->Settings_model->updateData('my_alerts',$alertpost,array('id'=>$alert_id));
		if($aflag != false){

			// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->session->set_flashdata('success',"Your Item Alert has been updated successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/item-alerts');

		}
		$data['ialerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'services','services','service_id');
		if(count($data['ialerts'])==0){
			redirect('settings/item-alerts');
		}
			// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Edit';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['services'] = $this->Settings_model->selectData('services','*',array("bus_id"=>$bus_id,"reg_id"=>$reg_id,'status'=>'Active'));
		$this->load->view('settings/alerts/edit-item-alert',$data);
	}

	function view_item_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['ialerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'services','services','service_id');
		if(count($data['ialerts'])==0){
			redirect('settings/item-alerts');
		}

		// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Item';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		//$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$this->load->view('settings/alerts/view-item-alert',$data);
	}
	/*-------End Item Alert Section -------*/
	/*-------Start Journal Voucher Alert Section -------*/
	public function jv_alerts()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"journal_voucher"),'','','','','','rowcount');
		if($data['alerts']>0){
		$this->load->view('settings/alerts/jv-alerts-list',$data);
		}else{
			$this->load->view('settings/alerts/alert-blank');
		}
	}
	public function get_jv_alerts(){
		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']="journal_voucher";


		$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"parent_id"=>'3',"alert_reminder"=>'4');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->jvAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->jvAlertsFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple jalerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$item_name = '<a href="'.base_url().'settings/view-jv-alert/'.$value['id'].'" >Account '.$value['parent_id'].'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
			$rem_date='';
			if($value['alert_reminder']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}


			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-jv-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_alerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-alert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;">Export</a></li>

					<li><a href='.base_url().'settings/print-alert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$item_name,
				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}
	public function add_jv_alert()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_reminder'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			$alertpost['alert_type']="journal_voucher";
			$alertpost['bus_id']=$bus_id;
			//echo "<pre>"; print_r($alertpost);exit();

		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
		if($aflag != false){

			$this->session->set_flashdata('success',"Your Journal Voucher Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/jv-alerts');

		}
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"journal_voucher"),'id','DESC');
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));

		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("JA","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}

		$data['alertNo']='JA'.$alno_ref;
		$this->load->view('settings/alerts/add-jv-alerts',$data);
	}
	function edit_jv_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			$alert_id=$alertpost['alert_id'];
			unset($alertpost['alert_id']);
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_reminder'] ='';
			}


		$aflag = $this->Settings_model->updateData('my_alerts',$alertpost,array('id'=>$alert_id));
		if($aflag != false){

			$this->session->set_flashdata('success',"Your Journal Voucher Alert has been updated successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/jv-alerts');

		}
		$data['jalerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'journal_voucher','','');
		if(count($data['jalerts'])==0){
			redirect('settings/jv-alerts');
		}
		//$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
		$this->load->view('settings/alerts/edit-jv-alert',$data);
	}
	function view_jv_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['jalerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'journal_voucher','','');
		if(count($data['jalerts'])==0){
			redirect('settings/jv-alerts');
		}

		$this->load->view('settings/alerts/view-jv-alert',$data);
	}
	/*-------End Journal Voucher Alert Section -------*/
	/*-------Start Credit Period Alert Section -------*/
	public function credit_period_alerts()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"credit_history",'status'=>'Active'),'','','','','','rowcount');
		//if($data['alerts']>0){
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('status'=>'Active','bus_id'=>$bus_id));

		$this->load->view('settings/alerts/ch-alerts-list',$data);
		// }else{
		// 	redirect('settings/add-credit-period-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');
		// }
	}

	public function get_ch_alerts(){

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert';
		$tracking_info['action_taken'] = 'Viewed';
		$tracking_info['reference'] = "Credit Period Alerts List";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']="credit_history";

		$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"cust_name"=>'3',"alert_reminder"=>'4');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->chAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->chAlertsFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple halerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';

	  		$cust_name = '<a href="'.base_url().'settings/view-credit-period-alert/'.$value['id'].'" >'.$value['cust_name'].'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{

				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
			$rem_date='';
			if($value['alert_reminder']!='')
			{

				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}


			$action = '';$status='';$cls='';$cls1='';
			/*
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="delete_ch_alerts" data-halert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="delete_ch_alerts" data-halert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</a></li>';
				$cls='active_record';
				$cls1='activate';

			}
			*/

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li><a href=".base_url()."settings/edit-credit-period-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_cpalerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-cpalert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;"><p class="ex">Export</p></a></li>

					<li><a href='.base_url().'settings/print-cpalert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>

					<li><a href="javascript:void(0);" class="delete_ch_alerts" data-halert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</a></li>';

					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';

					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}

					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}

					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}

					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';

					$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';


	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$cust_name,
				$value['alert_interval'],
	  			$action_type,
	  			$action,
	  		);
	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
		exit;
	}

	public function delete_ch_alerts() {

		$id = $this->input->post('halert_id');

		$flag = $this->Settings_model->deleteData("my_alerts", array('id' => $id));
		//print $this->db->last_query(); exit();
		if($flag != 0)
		{
			echo true;
			exit();
		}
	}

	public function add_credit_period_alert()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();
		if($alertpost){
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_interval'] = $alertpost['alert_reminder'];
			}else {
				$alertpost['alert_reminder'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			$alertpost['alert_type']="credit_history";
			$alertpost['bus_id']=$bus_id;
			//echo "<pre>"; print_r($alertpost);exit();
		   unset($alertpost['alert_credit_period']);
		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
		if($aflag != false){

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$aflag));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Added';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
			$this->session->set_flashdata('success',"Your Credit History Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/credit-period-alerts');

		}
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"credit_history"),'id','DESC');
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('bus_id'=>$bus_id,'status'=>'Active'));

		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("CHA","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Add';
			 $tracking_info['reference'] = "Add New Alert";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
		$data['alertNo']='CHA'.$alno_ref;
		$this->load->view('settings/alerts/add-ch-alert',$data);
	}

	function edit_credit_period_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			$alert_id=$alertpost['alert_id'];
			unset($alertpost['alert_id']);
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_interval'] = $alertpost['alert_reminder'];
			}else {
				$alertpost['alert_reminder'] ='';
			}


		$aflag = $this->Settings_model->updateData('my_alerts',$alertpost,array('id'=>$alert_id));
		if($aflag != false){

			// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Client Period';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->session->set_flashdata('success',"Your Credit History Alert has been updated successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/credit-period-alerts');

		}
		$data['halerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'credit_history','sales_customers','cust_id');
		if(count($data['halerts'])==0){
			redirect('settings/credit-period-alerts');
		}

		// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Client Period';
				 $tracking_info['action_taken'] = 'Edit';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		//$data['calerts'] = $this->Settings_model->selectData('my_alerts','*',array("id"=>$alert_id,"bus_id"=>$bus_id,"alert_type"=>"customers"));
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('status'=>'Active','bus_id'=>$bus_id));
		$this->load->view('settings/alerts/edit-ch-alert',$data);
	}


	function get_credit_period(){
		$post=$this->input->post();
		$rec_info = $this->Settings_model->selectData('sales_customers', 'cust_credit_period',array('cust_id'=>$post['id']));
		if($rec_info[0]->cust_credit_period > 0)
		{
			$cp = $rec_info[0]->cust_credit_period;

			$html = '<div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 padd-n">
                                <div class="row">
                                  <div class="input-field label-active">
                                      <label for="alert_credit_period" class="full-bg-label active">Credit Period (In Days)</label>
                                      <input id="alert_credit_period" name="alert_credit_period" class="full-bg adjust-width border-top-none border-right" type="text" value="'.$cp.'">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 border-field">
                                  <div class="row">
                                  <div class="input-field">

                                  </div>
                                </div>
                              </div>

                            </div>';
		}
		else
		{
			$cp = 0;

			$html = '<div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 padd-n">
                                <div class="row">
                                  <div class="input-field">
                                      <label for="alert_credit_period" class="full-bg-label">Credit Period (In Days)</label>
                                      <input id="alert_credit_period" name="alert_credit_period" class="full-bg adjust-width border-top-none border-right" type="text" value="'.$cp.'">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 border-field">
                                  <div class="row">
                                  <div class="input-field">

                                  </div>
                                </div>
                              </div>

                            </div>';
		}

		echo $html;

	}


	function view_credit_period_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['halerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'credit_history','sales_customers','cust_id');
		if(count($data['halerts'])==0){
			redirect('settings/credit-period-alerts');
		}

		// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Client Period';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$this->load->view('settings/alerts/view-ch-alert',$data);
	}
	/*-------End Credit Period Alert Section -------*/
	/*-------Start Birthday Anniversary Alert Section -------*/

	public function birthday_anniversary_alerts()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"birthday_anniversary",'status'=>'Active'),'','','','','','rowcount');
		//if($data['alerts']>0){
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('status'=>'Active','bus_id'=>$bus_id));
		$this->load->view('settings/alerts/ba-alerts-list',$data);
		// }else{
		// 	redirect('settings/add-birthday-anniversary-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');
		// }
	}

	public function add_birthday_anniversary_alert()
	{

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			//echo "<pre>";print_r($alertpost);exit;
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_reminder'] ='';
			}
			if($alertpost['occasion_date'] != ''){
				$alertpost['occasion_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['occasion_date'])));
			}else {
				$alertpost['occasion_date'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			$alertpost['alert_type']="birthday_anniversary";
			$alertpost['bus_id']=$bus_id;

			if(isset($alertpost['emp_code']) && $alertpost['emp_code']!=""){
				$alertpost['subparent_id']=$alertpost['emp_code'];
				unset($alertpost['contact_per']);
				unset($alertpost['emp_code']);
			}

			if(isset($alertpost['parent_id1']) && $alertpost['parent_id1']!=""){
				$alertpost['parent_id']=$alertpost['parent_id1'];
				//unset($alertpost['emp_code']);
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}

			if(isset($alertpost['parent_id']) && $alertpost['parent_id']!=""){
				$alertpost['parent_id']=$alertpost['parent_id'];
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}

			if(isset($alertpost['parent_id2']) && $alertpost['parent_id2']!=""){
				$alertpost['parent_id']=$alertpost['parent_id2'];
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}



			if(isset($alertpost['contact_per']) && $alertpost['contact_per']!=""){
				$alertpost['subparent_id']=$alertpost['contact_per'];
				unset($alertpost['contact_per']);
				unset($alertpost['emp_code']);
			}


			//echo "<pre>"; print_r($alertpost);exit();

		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
		if($aflag != false){

			// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$aflag));
				 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $rec_info[0]->alert_number;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->session->set_flashdata('success',"Your ".$alertpost['alert_occasion']." Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/birthday-anniversary-alerts');

		}
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"birthday_anniversary"),'id','DESC');
		$data['vendors']=$this->Settings_model->selectData('expense_vendors', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$data['employee']=$this->Settings_model->selectData('employee_master', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$data['bus_data'] = $this->Settings_model->selectData('businesslist', 'bus_id,bus_company_name', array('bus_id'=>$bus_id));
		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
            $temp=str_replace("BA","",$last_alerts_no);
			$temp=intval($temp) + 1;
			$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
			$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}

			// Activity Tracker start

				 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
				 $tracking_info['action_taken'] = 'Add';
				 $tracking_info['reference'] = "New Birthday & Anniversary";
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['alertNo']='BA'.$alno_ref;
		$this->load->view('settings/alerts/add-ba-alert',$data);
	}

	public function get_ba_alerts(){

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Alert List";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']="birthday_anniversary";


		$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"alert_name"=>'3',"alert_occasion"=>'4',"alert_reminder"=>'5');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->baAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->baAlertsFilter($post,$sort_field,$orderBy,1);
		//print $this->db->last_query(); exit();
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple balerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		//$alert_name = '<a href="'.base_url().'settings/view-birthday-anniversary-alert/'.$value['id'].'" >'.$value['alert_name'].'</a>';
              $name="";
		   if($value['alert_stakeholder']=='client'){
             $client =$this->Settings_model->selectData('sales_customers', '*',array('cust_id'=>$value['parent_id'],'bus_id'=>$value['bus_id']));
             $contact_per=$this->Settings_model->selectData('sales_customer_contacts', '*',array('cust_id'=>$value['parent_id'],'cp_id'=>$value['subparent_id']));
             $name=@$contact_per[0]->cp_name;
		   }

		   if($value['alert_stakeholder']=='vendor'){
             $vendor =$this->Settings_model->selectData('expense_vendors', '*',array('vendor_id'=>$value['parent_id'],'bus_id'=>$value['bus_id']));
             $contact_per=$this->Settings_model->selectData('expense_vendor_contacts', '*',array('vendor_id'=>$value['parent_id'],'cp_id'=>$value['subparent_id']));
             $name=@$contact_per[0]->cp_name;
		   }

		   if($value['alert_stakeholder']=='employee'){
             $employee =$this->Settings_model->selectData('employee_master', '*',array('emp_id'=>$value['subparent_id'],'bus_id'=>$value['bus_id']));

             $name=@$employee[0]->first_name." ".@$employee[0]->last_name;
		   }
			$alt_date='';
			if($value['alert_date']!='')
			{

				$dateTime = date("d-M",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-"," ",$dateTime);
			}
			$rem_date='';
			if($value['alert_reminder']!='')
			{

				$dateTime = date("d-M",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-"," ",$dateTime);
			}


			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_balerts" data-balert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_balerts" data-balert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</a></li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-birthday-anniversary-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_baalerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-baalert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;"><p class="ex">Export</p></a></li>

					<li><a href='.base_url().'settings/print-baalert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				ucfirst($name),
				ucfirst($value['alert_stakeholder']),
				//$alert_name,
				ucfirst($value['alert_occasion']),
				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}

	function edit_birthday_anniversary_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			$alert_id=$alertpost['alert_id'];
			unset($alertpost['alert_id']);
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_reminder'] ='';
			}

			if($alertpost['occasion_date'] != ''){
				$alertpost['occasion_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['occasion_date'])));
			}else {
				$alertpost['occasion_date'] ='';
			}

			if(isset($alertpost['emp_code']) && $alertpost['emp_code']!=""){
				$alertpost['subparent_id']=$alertpost['emp_code'];
				unset($alertpost['contact_per']);
				unset($alertpost['emp_code']);
			}

			if(isset($alertpost['parent_id1']) && $alertpost['parent_id1']!=""){
				$alertpost['parent_id']=$alertpost['parent_id1'];
				//unset($alertpost['emp_code']);
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}

			if(isset($alertpost['parent_id']) && $alertpost['parent_id']!=""){
				$alertpost['parent_id']=$alertpost['parent_id'];
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}

			if(isset($alertpost['parent_id2']) && $alertpost['parent_id2']!=""){
				$alertpost['parent_id']=$alertpost['parent_id2'];
				unset($alertpost['parent_id1']);
				unset($alertpost['parent_id2']);
			}



			if(isset($alertpost['contact_per']) && $alertpost['contact_per']!=""){
				$alertpost['subparent_id']=$alertpost['contact_per'];
				unset($alertpost['contact_per']);
				unset($alertpost['emp_code']);
			}



		$aflag = $this->Settings_model->updateData('my_alerts',$alertpost,array('id'=>$alert_id));
		if($aflag != false){

			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Edited';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

			$this->session->set_flashdata('success',"Your ".$alertpost['alert_occasion']." Alert has been updated successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/birthday-anniversary-alerts');
		}
		$data['balerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'birthday_anniversary','sales_customers','cust_id');
		if(count($data['balerts'])==0){
			redirect('settings/birthday-anniversary-alerts');
		}

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Edit';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		//$data['calerts'] = $this->Settings_model->selectData('my_alerts','*',array("id"=>$alert_id,"bus_id"=>$bus_id,"alert_type"=>"customers"));
		$data['vendors']=$this->Settings_model->selectData('expense_vendors', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$data['customers']=$this->Settings_model->selectData('sales_customers', '*', array('bus_id'=>$bus_id,'status'=>'Active'));
		$data['employee']=$this->Settings_model->selectData('employee_master', '*', array('bus_id'=>$bus_id,'status'=>'Active'));

		$data['bus_data'] = $this->Settings_model->selectData('businesslist', 'bus_id,bus_company_name', array('bus_id'=>$bus_id));
		$data['countries']	=	$this->Settings_model->selectData('countries', '*','','country_id','ASC');
		//print_r($data);exit();
		$this->load->view('settings/alerts/edit-ba-alert',$data);
	}

	function view_birthday_anniversary_alert($alert_id){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['balerts'] = $this->Settings_model->join_Alrts($alert_id,$bus_id,'birthday_anniversary','sales_customers','cust_id');

		if(count($data['balerts'])==0){
			redirect('settings/birthday-anniversary-alerts');
		}

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$alert_id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('settings/alerts/view-ba-alert',$data);
	}

	/*~~~~~~~~~~CLIENT ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/
	public function download_multiple_alerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);

		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'customers','sales_customers','cust_id');
		$this->load->view('settings/alerts/download_all_alerts',$data);
	}
	public function print_multiple_alerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);

		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'customers','sales_customers','cust_id');
		$this->load->view('settings/alerts/print_all_alerts',$data);
	}
	public function download_alert($array) {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'customers','sales_customers','cust_id','1');
		$this->load->view('settings/alerts/download_all_alerts',$data);
	}
	public function print_alert($array){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End


		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'customers','sales_customers','cust_id','1');
		//print $this->db->last_query(); exit();
		//print_r($data);exit();
		$this->load->view('settings/alerts/print_all_alerts',$data);
	}
	public function email_multiple_alerts(){
		$array=$this->input->post('alerts_values');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'customers','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_alerts',$data,true);
		$invoname ="Client Alerts";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Client Alerts.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Client Alerts has been created',$msg,$output,$invoname);
	}
	public function email_alert() {

		$post_data = $_POST;
		$id = $post_data['alert_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['alert_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$ids[$i]));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Clients';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		}

		$data['result'] = $this->Settings_model->print_multiple_alerts($ids,$bus_id,'customers','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_alerts',$data,true);
		//$invoname ="Client Alerts";
		//$Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Settings_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Client Alerts')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];

		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$subject = $post_data['email_subject'];


		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}

	}

	/*~~~~~~~~~~END CLIENT ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	/*~~~~~~~~~~ITEM ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	public function download_multiple_ialerts(){

		$array = $this->input->get('ids');
		$array = explode(',', $array);

		foreach($array as $id) {
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			$tracking_info['module_name'] = 'Settings / Set Alert / Items';
			$tracking_info['action_taken'] = 'Downloaded';
			$tracking_info['reference'] = $rec_info[0]->alert_number;
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$data['result'] = $this->Settings_model->print_multiple_alert($array,$bus_id,'services','services','service_id');

		$this->load->view('settings/alerts/download_all_ialerts',$data);
	}

	public function print_multiple_ialerts(){

		$array = $this->input->get('ids');
		$array = explode(',', $array);

		foreach($array as $id) {
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			$tracking_info['module_name'] = 'Settings / Set Alert / Items';
			$tracking_info['action_taken'] = 'Printed';
			$tracking_info['reference'] = $rec_info[0]->alert_number;
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$data['result'] = $this->Settings_model->print_multiple_alert($array,$bus_id,'services','services','service_id');

		$this->load->view('settings/alerts/print_all_ialerts',$data);
	}

	public function download_ialert($array) {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Items';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alert($array,$bus_id,'services','services','service_id','1');
		$this->load->view('settings/alerts/download_all_ialerts',$data);
	}
	public function print_ialert($array){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Items';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alert($array,$bus_id,'services','services','service_id','1');
		//print $this->db->last_query(); exit();
		$this->load->view('settings/alerts/print_all_ialerts',$data);
	}
	public function email_multiple_ialerts(){
		$array=$this->input->post('alerts_values');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Items';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alert($array,$bus_id,'services','services','service_id');
		$htmldata = $this->load->view('settings/alerts/print_all_ialerts',$data,true);
		$invoname ="Items Alerts";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Items Alerts.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Items Alerts has been created',$msg,$output,$invoname);
	}
	public function email_ialert() {

		$post_data = $_POST;
		$id = $post_data['alert_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['alert_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$ids[$i]));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Items';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		}

		$data['result'] = $this->Settings_model->print_multiple_alert($ids,$bus_id,'services','services','service_id');
		$htmldata = $this->load->view('settings/alerts/print_all_ialerts',$data,true);
		//$invoname ="Items Alerts";

		//$content = $this->load->view('settings/alerts/email_template_item',$data,true);
		//$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Items Alerts.";
		//$Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Items Alerts')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;


		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);

		$message = $post_data['email_message'];

		$subject = $post_data['email_subject'];


		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}


		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}


		//sendInvoiceEmail($this->user_session['email'],'Your Items Alerts has been created',$msg,$output,$invoname);

	}

	/*~~~~~~~~~~END ITEM ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	/*~~~~~~~~~~Journal Vouchar ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

/*	public function download_multiple_jvalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'journal_voucher','','');
		print_r($data);
		exit();
		$this->load->view('settings/alerts/download_all_jvalerts',$data);
	}
	public function print_multiple_jalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'journal_voucher','','');
		$this->load->view('settings/alerts/print_all_jvalerts',$data);
	}
	public function download_jvalert($array) {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'journal_voucher','','','1');
		$this->load->view('settings/alerts/download_all_jvalerts',$data);
	}
	public function print_jvalert($array){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'journal_voucher','','','1');
		$this->load->view('settings/alerts/print_all_jvalerts',$data);
	}*/

	/*~~~~~~~~~~END Journal Vouchar ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	/*~~~~~~~~~~Credit Period ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	public function download_multiple_cpalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'credit_history','sales_customers','cust_id');
		$this->load->view('settings/alerts/download_all_cpalerts',$data);
	}
	public function print_multiple_cpalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'credit_history','sales_customers','cust_id');
		$this->load->view('settings/alerts/print_all_cpalerts',$data);
	}

	public function download_cpalert($array) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
		$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
		$tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
		$tracking_info['action_taken'] = 'Downloaded';
		$tracking_info['reference'] = $rec_info[0]->alert_number;
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'credit_history','sales_customers','cust_id','1');

		$this->load->view('settings/alerts/download_all_cpalerts',$data);
	}

	public function print_cpalert($array) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		//Activity Tracker start
		$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
		$tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
		$tracking_info['action_taken'] = 'Printed';
		$tracking_info['reference'] = $rec_info[0]->alert_number;
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		//Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alerts($array, $bus_id, 'credit_history', 'sales_customers', 'cust_id', '1');
		//echo "<pre>"; print_r($data['result']); echo "</pre>"; exit();
		$this->load->view('settings/alerts/print_all_cpalerts',$data);
	}

	public function email_multiple_cpalerts(){
		$array=$this->input->post('alerts_values');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			 $rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'credit_history','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_cpalerts',$data,true);
		$invoname ="Credit Period Alerts";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Credit Period Alerts.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Credit Period Alerts has been created',$msg,$output,$invoname);
	}
	public function email_cpalert() {

		$post_data = $_POST;
		$id = $post_data['alert_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['alert_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			// Activity Tracker start
			 $rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$ids[$i]));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Credit Period';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$data['result'] = $this->Settings_model->print_multiple_alerts($ids,$bus_id,'credit_history','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_cpalerts',$data,true);
		//$invoname ="Credit Period Alerts";
		//$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Credit Period Alerts.";
		//$content = $this->load->view('settings/alerts/email_template_chalert',$data,true);
		//$Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Credit Period Alerts')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;

		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];
		$subject = $post_data['email_subject'];


		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}


		//sendInvoiceEmail($this->user_session['email'],'Your Credit Period Alerts has been created',$msg,$output,$invoname);

	}

	/*~~~~~~~~~~END Credit Period ALERTS DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	/*~~~~~~~~~~Birthday/Anniversary DOWNLOAD AND PRINT~~~~~~~~~~~~~~~~~~~*/

	public function download_multiple_baalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'birthday_anniversary','sales_customers','cust_id');
		$this->load->view('settings/alerts/download_all_baalerts',$data);
	}
	public function print_multiple_baalerts(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'birthday_anniversary','sales_customers','cust_id');
		$this->load->view('settings/alerts/print_all_baalerts',$data);
	}
	public function download_baalert($array) {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'birthday_anniversary','sales_customers','cust_id','1');
		$this->load->view('settings/alerts/download_all_baalerts',$data);
	}
	public function print_baalert($array){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'birthday_anniversary','sales_customers','cust_id','1');
		$this->load->view('settings/alerts/print_all_baalerts',$data);
	}
	public function email_multiple_baalerts(){
		$array=$this->input->post('alerts_values');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result'] = $this->Settings_model->print_multiple_alerts($array,$bus_id,'birthday_anniversary','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_baalerts',$data,true);
		$invoname ="Birthday Anniversary Alerts";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Birthday & Anniversary Alerts.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Birthday & Anniversary Alerts has been created',$msg,$output,$invoname);
	}
	public function email_baalert() {

		$post_data = $_POST;
		$id = $post_data['alert_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['alert_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			// Activity Tracker start
			 $rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$ids[$i]));
			 $tracking_info['module_name'] = 'Settings / Set Alert / Birthday & Anniversary';
			 $tracking_info['action_taken'] = 'Emailed';
			 $tracking_info['reference'] = $rec_info[0]->alert_number;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$data['result'] = $this->Settings_model->print_multiple_alerts($ids,$bus_id,'birthday_anniversary','sales_customers','cust_id');
		$htmldata = $this->load->view('settings/alerts/print_all_baalerts',$data,true);
		//$invoname ="Birthday Anniversary Alerts";
		//$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Birthday & Anniversary Alerts.";
		//$content = $this->load->view('settings/alerts/email_template_ba',$data,true);
		//$Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Settings_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Birthday Anniversary Alerts')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;


		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];
		$subject = $post_data['email_subject'];


		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}

	}


	public function add_jv_alert_test() {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alertno'] = $this->Settings_model->selectData('my_alerts','*');
		//if($data['alerts']>0){


		//$this->load->view('settings/alerts/ba-alerts-list',$data);
		// }else{
		// 	redirect('settings/add-birthday-anniversary-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');
		// }

		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"JV"),'id','DESC');


		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("JV","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}


		$data['alertNo']='JV'.$alno_ref;

		$this->load->view('settings/alerts/add-jv-alerts',$data);
	}
	public function vendor_alert_create() {
$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$alertpost=$this->input->post();

		if($alertpost){
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] ='';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_reminder'] ='';
			}
			if($alertpost['alert_msg']==''){
				$alertpost['alert_mobile']='';
			}
			if($alertpost['alert_mail']==''){
				$alertpost['alert_email']='';
			}
			/*if($alertpost['alert_condition']==''){
				$alertpost['alert_condition']='';
			}*/
			$alertpost['alert_type']="Vendor";
			$alertpost['bus_id']=$bus_id;
			//echo "<pre>"; print_r($alertpost);exit();

		  $alertpost['parent_id']= $alertpost['vendor_id'];
		  unset($alertpost['vendor_id']);
		  //$aflag = $this->Settings_model->insertData('expense_vendors',$alertpost);

		$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);
				if($aflag != false){

			$this->session->set_flashdata('success',"Your vendor Alert has been created successfully");
		} else {
			$this->session->set_flashdata('error',"Error while processing...!!");
		}
		redirect('settings/vendor_alert_list');

		}
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"V"),'id','DESC');
	    $data['vendor'] = $this->Settings_model->selectData('expense_vendors','*');
	    $alerttable=$data['alerts'] ;
	   		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("V","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}


		$data['alertNo']='V'.$alno_ref;
		//redirect('settings/vendor-alerts-list');

		$this->load->view('settings/alerts/vendor-alert-create',$data);

	/*	$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alertno'] = $this->Settings_model->selectData('my_alerts','*');
		//if($data['alerts']>0){


		//$this->load->view('settings/alerts/ba-alerts-list',$data);
		// }else{
		// 	redirect('settings/add-birthday-anniversary-alert');
		// 	//$this->load->view('settings/alerts/alert-blank');
		// }

		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"V"),'id','DESC');
	    $data['vendor'] = $this->Settings_model->selectData('expense_vendors','*',array("bus_id"=>$bus_id));;
	    $alerttable=$data['alerts'] ;
	   		if(count($data['alerts'])>0){
			$last_alerts_no=$data['alerts'][0]->alert_number;
              $temp=str_replace("V","",$last_alerts_no);
				$temp=intval($temp) + 1;
				$alno_ref=str_pad($temp, 3, 0, STR_PAD_LEFT);
		}
		else
		{
				$alno_ref=str_pad(1, 3, 0, STR_PAD_LEFT);
		}


		$data['alertNo']='V'.$alno_ref;
		redirect('settings/jv-alert-list');

		$this->load->view('settings/alerts/vendor-alert-create',$data);*/

	}

	public function get_v_alerts(){
		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']="vendor";


	$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"cust_name"=>'3',"alert_condition"=>'4',"alert_target"=>'5',"alert_reminder"=>'6');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->myvAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->myvAlertsFilter($post,$sort_field,$orderBy,1);
        //print_r($userdata);
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple jalerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$item_name = '<a href="'.base_url().'settings/vendor-alert-list/'.$value['id'].'" >Account '.$value['parent_id'].'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
			$rem_date='';
			if($value['alert_reminder']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}


			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-jv-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_alerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-alert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;">Export</a></li>

					<li><a href='.base_url().'settings/print-alert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$item_name,
				"",
				"",

				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}
		public function vendor_alert_list() {


/*reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"V"),'','','','','','rowcount');
		$data['vendor'] = $this->Settings_model->selectData('expense_vendors','*',array("bus_id"=>$bus_id));
		if($data['alerts']>0){
		$this->load->view('settings/alerts/vendor-alert-list',$data);
		}else{
		$this->load->view('settings/alerts/alert-blank');*/
$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
	$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"V"),'id','DESC');
	    $data['vendor'] = $this->Settings_model->selectData('expense_vendors','*',array("bus_id"=>$bus_id));;
		if($data['alerts']>0){
		$this->load->view('settings/alerts/vendor-alert-list',$data);
		}else{
			$this->load->view('settings/alerts/alert-blank');
		}




}

public function exp_alert() {


/*reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array('bus_id'=>$bus_id,'alert_type'=>"V"),'','','','','','rowcount');
		$data['vendor'] = $this->Settings_model->selectData('expense_vendors','*',array("bus_id"=>$bus_id));
		if($data['alerts']>0){
		$this->load->view('settings/alerts/vendor-alert-list',$data);
		}else{
		$this->load->view('settings/alerts/alert-blank');*/
$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
	$data['alerts'] = $this->Settings_model->selectData('my_alerts','*',array("bus_id"=>$bus_id,"alert_type"=>"exp"),'id','DESC');
	    $data['expense'] = $this->Settings_model->selectData('expense_list','*',array("bus_id"=>$bus_id));
		if($data['alerts']>0){
		$this->load->view('settings/alerts/exp-alert-list',$data);
		}else{
			$this->load->view('settings/alerts/alert-blank');
		}




}

public function get_contact_persons(){
     $post = $this->input->post();
	$bus_id = $this->user_session['bus_id'];
	if($post['stakeholder']=="client"){
	 $data = $this->Settings_model->selectData('sales_customer_contacts','*',array("cust_id"=>$post['id'],'status'=>'Active'));
	}else{
		$data = $this->Settings_model->selectData('expense_vendor_contacts','*',array("vendor_id"=>$post['id'],'status'=>'Active'));
	}
	 echo json_encode($data);
		exit;
}

public function get_exp_alerts(){
		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		$post['alert_type']="expense";


	$field_pos=array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"cust_name"=>'3',"alert_condition"=>'4',"alert_target"=>'5',"alert_reminder"=>'6');

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}

		$TotalRecord=$this->Settings_model->myexpAlertsFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->Settings_model->myexpAlertsFilter($post,$sort_field,$orderBy,1);
        //print_r($userdata);
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$check='<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple jalerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$item_name = '<a href="'.base_url().'settings/exp-alert-list/'.$value['id'].'" >Account '.$value['parent_id'].'</a>';


			$alt_date='';
			if($value['alert_date']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}
			$rem_date='';
			if($value['alert_reminder']!='')
			{
				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}


			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_jalerts" data-jalert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-jv-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

					<li><a href="javascript:void(0);" onclick="email_alerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'settings/download-alert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;">Export</a></li>

					<li><a href='.base_url().'settings/print-alert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';


					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				$check,
				$value['alert_number'],
				$alt_date,
				$item_name,
				"",
				"",
				$rem_date,
	  			$action_type,
	  			$action,
	  			);
	  	}
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}




	//Activity Alerts

	public function activity_alerts() {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', " `bus_id` = ".$bus_id. " and `alert_type` = 'activity' and `status` = 'active'");

		$this->load->view('settings/alerts/alerts-generic-list', $data);
	}

	public function get_generic_alerts() {

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Viewed';
		$tracking_info['reference'] = "Item Alerts List";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();

		$post['reg_id'] = $this->user_session['reg_id'];
		$post['bus_id'] = $this->user_session['bus_id'];
		$bus_id = $post['bus_id'];
		$post['alert_type'] = array("activity");

		$field_pos = array("id"=>'0',"alert_number"=>'1',"alert_date"=>'2',"alert_type"=>'3',"alert_name"=>'4',"alert_date_time"=>'5',"alert_reminder"=>'6');

		$sort_field = array_search($post['order'][0]['column'], $field_pos);

		if($post['order'][0]['dir'] =='asc') {
			$orderBy = "ASC";
		} else {
			$orderBy = "DESC";
		}

		$TotalRecord = $this->Settings_model->activityAlertsFilter($post, $sort_field, $orderBy, 0);
	 	$userData = $this->Settings_model->activityAlertsFilter($post, $sort_field, $orderBy, 1);
	 	//echo "<pre>"; print_r($userData); echo "</pre>"; exit();
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {

	  		$time = "";
	  		if($value['alert_type'] == 'activity') {
	  			$time = $value['alert_time'];
	  		}

	  		$name = "";
	  		if($value['alert_type'] == 'activity'){
	  			$name = $value['alert_name'];
	  		}
	  		$check = '<input type="checkbox" id="filled-in-box'.$value['id'].'" name="filled-in-box'.$value['id'].'" class="filled-in purple ialerts_bulk_action" value="'.$value['id'].'"/><label for="filled-in-box'.$value['id'].'"></label>';
	  		$item_name = $name;

	  		$alt_date='';
	  		if($value['alert_date']!='') {
				$dateTime = date("d-m-Y",  strtotime($value['alert_date']));
				if($value['alert_date']=='0000-00-00'){
					$dateTime='';
				}
				$alt_date = str_replace("-",".",$dateTime);
			}

			$rem_date='';
			if($value['alert_reminder']!='') {
				$dateTime = date("d-m-Y",  strtotime($value['alert_reminder']));
				if($value['alert_reminder']=='0000-00-00'){
					$dateTime='';
				}
				$rem_date = str_replace("-",".",$dateTime);
			}
			$date = $this->Settings_model->sss($bus_id, $value['id']);
			//print_r($this->db->last_query());exit();
			$date='';
			if($value['alert_date_time']!='') {
				$dateTime = date("d-m-Y",  strtotime($value['alert_date_time']));
				if($value['alert_date_time']=='0000-00-00'){
					$dateTime='';
				}
				$date = str_replace("-",".",$dateTime);
			}

			$action = '';$status='';$cls='';$cls1='';

			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="deactive_aalerts" data-aalert_id='.$value["id"].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{

				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_aalerts" data-aalert_id="'.$value["id"].'"><i class="material-icons">delete</i>Delete</li>';
				$cls='active_record';
				$cls1='activate';

			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["id"]."' class='dropdown-content'>
				 	<li>
                   	 <a href=".base_url()."settings/edit-activity-alert/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>

                   	 <!--li><a href="javascript:void(0);" onclick="email_aalerts('.$value["id"].');"><i class="material-icons">email</i>Email</a></li-->

                    <li><a href='.base_url().'settings/download-aalert/'.$value["id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 15px;"><p class="ex">Export</p></a></li>

					<li><a href='.base_url().'settings/print-aalert/'.$value["id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';

					$bell='bell-icon';$msg='msg-gicon';$mail='mail-icon';
					if($value["alert_notification"]==0){
						$bell='bell-grey';
					}
					if($value["alert_msg"]==0){
						$msg='msg-grey';
					}
					if($value["alert_mail"]==0){
						$mail='mail-grey';
					}
					$action_type ='<img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$bell.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$msg.'.png"><img class="icon-img-noti" src="'.base_url().'asset/css/img/icons/'.$mail.'.png">';
			$action  .= $status;

			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';

	  		$records["data"][] = array(
				//$check,
				$value['alert_number']."</br>".$alt_date,
				$name,
				$date,
				$time,
				$rem_date,
	  			$action_type,
	  			$action,
	  			);

	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit;
	}

	public function print_aalert($array){

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
		$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Printed';
		$tracking_info['reference'] = $rec_info[0]->alert_number;
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_activityalert($array,$bus_id,'services','services','service_id','1');
		//print $this->db->last_query(); exit();
		$this->load->view('settings/alerts/print_all_aalerts',$data);
	}

	public function download_aalert($array) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		// Activity Tracker start
		$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$array));
		$tracking_info['module_name'] = 'Settings / Set Alert / Items';
		$tracking_info['action_taken'] = 'Downloaded';
		$tracking_info['reference'] = $rec_info[0]->alert_number;
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

		$data['result'] = $this->Settings_model->print_multiple_activityalert($array,$bus_id,'services','services','service_id','1');
		$this->load->view('settings/alerts/download_all_aalerts',$data);
	}

	public function deactive_aalerts() {

		$id = $this->input->post('aalert_id');
		//print_r($id); exit;
		$alert_type = $this->input->post('alert_type');
		$bus_id = $this->user_session['bus_id'];

		$filter = array(
			'status' => $this->input->post('status'),
		);

		$flag = $this->Settings_model->updateData('my_alerts', $filter, array('id'=>$id));
		//print $this->db->last_query(); exit();
		if($flag != false) {

			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
			$tracking_info['module_name'] = 'Settings / Set Alert';
			$tracking_info['action_taken'] = 'Deactivate';
			$tracking_info['reference'] = 'Delete Alert';
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			echo true;
		}
		else
		{
			echo false;
		}
	}

	public function email_aalert() {

		$post_data = $_POST;
		$id = $post_data['alert_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['alert_id']);

		for($i = 0; $i < count($ids); $i++) {
			// Activity Tracker start
			$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$ids[$i]));
			$tracking_info['module_name'] = 'Settings / Set Alert / Items';
			$tracking_info['action_taken'] = 'Emailed';
			$tracking_info['reference'] = $rec_info[0]->alert_number;
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$data['result'] = $this->Settings_model->print_multiple_activityalert($ids,$bus_id,'services','services','service_id');
		$htmldata = $this->load->view('settings/alerts/print_all_aalerts',$data,true);
		//$invoname ="Items Alerts";

		//$content = $this->load->view('settings/alerts/email_template_item',$data,true);
		//$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Items Alerts.";
		//$Attach=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Activity Alerts')."_".time();
		$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$Attach[0]=$path;


		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);

		$message = $post_data['email_message'];

		$subject = $post_data['email_subject'];


		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}


		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}

	}

	public function add_activity_alert() {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$alertpost = $this->input->post();

		if($alertpost) {

			if($alertpost['alert_date'] != '') {
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_reminder'] != '') {
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_date_time'] != '') {
				$alertpost['alert_date_time'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date_time'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_msg'] == '') {
				$alertpost['alert_mobile'] = '';
			}

			if($alertpost['alert_mail'] == '') {
				$alertpost['alert_email'] = '';
			}

			$alertpost['bus_id'] = $bus_id;
			$alertpost['alert_type'] = 'activity';

			$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);

			if($aflag != false) {

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id' => $aflag));
				$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
				$tracking_info['action_taken'] = 'Add';
				$tracking_info['reference'] = $rec_info[0]->alert_number;
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Activity Alert has been created successfully");

			} else {
				$this->session->set_flashdata('error',"Error while processing...!!");
			}
			redirect('settings/activity-alerts');
		}

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', array("bus_id" => $bus_id, "alert_type" => "activity"), 'id', 'DESC');

		//$data['customers'] = $this->Settings_model->selectData('sales_customers', '*', array('bus_id' => $bus_id,'status' => 'Active'));

		if(count($data['alerts']) > 0) {
			$last_alerts_no = $data['alerts'][0]->alert_number;
			$temp = str_replace("AA", "", $last_alerts_no);
			$temp = intval($temp) + 1;
			$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
		} else {
			$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Add';
		$tracking_info['reference'] = "Activity Alert";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['alertNo']='AA'.$alno_ref;
		//print_r($data['alertNo']); exit();

		$this->load->view('settings/alerts/add-generic-alert', $data);
	}

	public function add_offer_activity_alert($id) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$data['offers'] = $this->Settings_model->selectData('offers', '*', ['id'=>$id]);

		$alertpost = $this->input->post();

		if($alertpost) {

			if($alertpost['alert_date'] != '') {
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_reminder'] != '') {
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_date_time'] != '') {
				$alertpost['alert_date_time'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date_time'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_msg'] == '') {
				$alertpost['alert_mobile'] = '';
			}

			if($alertpost['alert_mail'] == '') {
				$alertpost['alert_email'] = '';
			}

			$alertpost['bus_id'] = $bus_id;
			$alertpost['alert_type'] = 'activity';

			$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);

			if($aflag != false) {

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id' => $aflag));
				$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
				$tracking_info['action_taken'] = 'Add';
				$tracking_info['reference'] = $rec_info[0]->alert_number;
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Activity Alert has been created successfully");

			} else {
				$this->session->set_flashdata('error',"Error while processing...!!");
			}
			redirect('settings/activity-alerts');
		}

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', array("bus_id" => $bus_id, "alert_type" => "activity"), 'id', 'DESC');

		//$data['customers'] = $this->Settings_model->selectData('sales_customers', '*', array('bus_id' => $bus_id,'status' => 'Active'));

		if(count($data['alerts']) > 0) {
			$last_alerts_no = $data['alerts'][0]->alert_number;
			$temp = str_replace("AA", "", $last_alerts_no);
			$temp = intval($temp) + 1;
			$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
		} else {
			$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Add';
		$tracking_info['reference'] = "Activity Alert";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['alertNo']='AA'.$alno_ref;
		//print_r($data['alertNo']); exit();

		$this->load->view('settings/alerts/add-activity-alert-offers', $data);
	}

	public function add_event_activity_alert($id) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$data['events'] = $this->Settings_model->selectData('events', '*', ['id'=>$id]);
		
		$alertpost = $this->input->post();

		if($alertpost) {

			if($alertpost['alert_date'] != '') {
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_reminder'] != '') {
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_date_time'] != '') {
				$alertpost['alert_date_time'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date_time'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_msg'] == '') {
				$alertpost['alert_mobile'] = '';
			}

			if($alertpost['alert_mail'] == '') {
				$alertpost['alert_email'] = '';
			}

			$alertpost['bus_id'] = $bus_id;
			$alertpost['alert_type'] = 'activity';

			$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);

			if($aflag != false) {

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id' => $aflag));
				$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
				$tracking_info['action_taken'] = 'Add';
				$tracking_info['reference'] = $rec_info[0]->alert_number;
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Activity Alert has been created successfully");

			} else {
				$this->session->set_flashdata('error',"Error while processing...!!");
			}
			redirect('settings/activity-alerts');
		}

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', array("bus_id" => $bus_id, "alert_type" => "activity"), 'id', 'DESC');

		//$data['customers'] = $this->Settings_model->selectData('sales_customers', '*', array('bus_id' => $bus_id,'status' => 'Active'));

		if(count($data['alerts']) > 0) {
			$last_alerts_no = $data['alerts'][0]->alert_number;
			$temp = str_replace("AA", "", $last_alerts_no);
			$temp = intval($temp) + 1;
			$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
		} else {
			$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Add';
		$tracking_info['reference'] = "Activity Alert";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['alertNo']='AA'.$alno_ref;
		//print_r($data['alertNo']); exit();

		$this->load->view('settings/alerts/add-activity-events-offers', $data);
	}


	public function add_deal_activity_alert($id) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

	
		$data['offers'] = $this->Settings_model->selectData('offers', '*', ['id'=>$id]);

		$alertpost = $this->input->post();

		if($alertpost) {

			if($alertpost['alert_date'] != '') {
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_reminder'] != '') {
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_date_time'] != '') {
				$alertpost['alert_date_time'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date_time'])));
			} else {
				$alertpost['alert_date'] = '';
			}

			if($alertpost['alert_msg'] == '') {
				$alertpost['alert_mobile'] = '';
			}

			if($alertpost['alert_mail'] == '') {
				$alertpost['alert_email'] = '';
			}

			$alertpost['bus_id'] = $bus_id;
			$alertpost['alert_type'] = 'activity';

			$aflag = $this->Settings_model->insertData('my_alerts',$alertpost);

			if($aflag != false) {

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id' => $aflag));
				$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
				$tracking_info['action_taken'] = 'Add';
				$tracking_info['reference'] = $rec_info[0]->alert_number;
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Activity Alert has been created successfully");

			} else {
				$this->session->set_flashdata('error',"Error while processing...!!");
			}
			redirect('settings/activity-alerts');
		}

		$data['alerts'] = $this->Settings_model->selectData('my_alerts', '*', array("bus_id" => $bus_id, "alert_type" => "activity"), 'id', 'DESC');

		//$data['customers'] = $this->Settings_model->selectData('sales_customers', '*', array('bus_id' => $bus_id,'status' => 'Active'));

		if(count($data['alerts']) > 0) {
			$last_alerts_no = $data['alerts'][0]->alert_number;
			$temp = str_replace("AA", "", $last_alerts_no);
			$temp = intval($temp) + 1;
			$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
		} else {
			$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Add';
		$tracking_info['reference'] = "Activity Alert";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['alertNo']='AA'.$alno_ref;
		//print_r($data['alertNo']); exit();

		$this->load->view('settings/alerts/add-activity-alert-offers', $data);
	}

	public function edit_activity_alert($id) {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$alertpost = $this->input->post();

		if($alertpost) {

			$alert_id = $alertpost['id'];
			unset($alertpost['id']);

			if($alertpost['alert_msg'] == ''){
				$alertpost['alert_mobile'] = '';
			}
			if($alertpost['alert_mail'] == ''){
				$alertpost['alert_email']='';
			}
			if($alertpost['alert_date'] != ''){
				$alertpost['alert_date'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_date'])));
			}else {
				$alertpost['alert_date'] = '';
			}
			if($alertpost['alert_reminder'] != ''){
				$alertpost['alert_reminder'] = date("Y-m-d", strtotime(str_replace('/', '-',$alertpost['alert_reminder'])));
			}else {
				$alertpost['alert_date'] ='';
			}

			$aflag = $this->Settings_model->updateData('my_alerts', $alertpost, array('id'=>$id));

			if($aflag != false) {

				// Activity Tracker start
				$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
				$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
				$tracking_info['action_taken'] = 'Edit';
				$tracking_info['reference'] = 'Activity Alert';
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Activity Alert has been updated successfully");
			} else {
				$this->session->set_flashdata('error',"Error while processing...!!");
			}
			redirect('settings/activity-alerts');
		}

		// Activity Tracker start
		$rec_info = $this->Settings_model->selectData('my_alerts', '*',array('id'=>$id));
		$tracking_info['module_name'] = 'Settings / Set Alert / Activity';
		$tracking_info['action_taken'] = 'Edit';
		$tracking_info['reference'] = 'Activity Alert';
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['results'] = $this->Settings_model->selectData('my_alerts', '*', " `bus_id` = ".$bus_id. " and `alert_type` = 'activity' and `status` = 'active'and  `id` = ".$id."") ;
		//echo "<pre>"; print_r($data['results']); echo "</pre>"; exit();
		$this->load->view('settings/alerts/edit-generic-alert', $data);
	}


}
