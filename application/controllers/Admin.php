<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {

        parent::__construct();

        // Load form validation library & user model 
        $this->load->library('form_validation');
        $this->load->model('Admin_model');

        // User login status 
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');

        // Load Models
        $this->load->model('Editorspick_model');
        $this->load->model('Inspiringpeoples_model');
        $this->load->model('Uploadvideos_model');
        //$this->load->model('Submitstories_model');
        //$this->load->model('Contactus_model');
        //$this->load->model('Partnerwithus_model');
        //$this->load->model('Jobapplication_model');
        //$this->load->model('User_model');
        $this->load->model('Common_model');
        $this->load->model('Adminmaster_model');
        $this->load->model('Admin_dashboard_model');


        // Load form helper
        $this->load->helper('form');

        // Load pagination library
        $this->load->library('pagination');

        // Per page limit
        $this->perPage = 10;
    }

    public function index() {

        if ($this->isUserLoggedIn) {
            redirect('admin/dashboard');
        } else {
            redirect('admin/login');
        }
    }

    public function login() {

        $data = array();

        // Get messages from the session 
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        // If login request submitted 
        if ($this->input->post('loginSubmit')) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'required');

            if ($this->form_validation->run() == true) {
                $con = array(
                    'returnType' => 'single',
                    'conditions' => array(
                        'email' => $this->input->post('email'),
                        'password' => md5($this->input->post('password')),
                        'status' => 1
                    )
                );
                $checkLogin = $this->Admin_model->getRows($con);
                if ($checkLogin) {
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                    $this->session->set_userdata('userId', $checkLogin['id']);
                    redirect('admin/dashboard');
                } else {
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            } else {
                $data['error_msg'] = 'Please fill all the mandatory fields.';
            }
        }

        $data['title'] = "Admin Login";

        $this->load->view('partials/admin-header', $data);
        $this->load->view('admin/adminlogin');
        $this->load->view('partials/admin-footer');
    }

    public function register_new_user() {
        $data = $userData = array();

        // If registration request is submitted 
        if ($this->input->post('signupSubmit')) {
            $this->form_validation->set_rules('first_name', 'First Name', 'required|alpha_numeric_spaces');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|alpha_numeric_spaces');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[password]');

            $userData = array(
                'first_name' => strip_tags($this->input->post('first_name')),
                'last_name' => strip_tags($this->input->post('last_name')),
                'email' => strip_tags($this->input->post('email')),
                'password' => md5($this->input->post('password')),
                'phone' => strip_tags($this->input->post('phone'))
            );

            if ($this->form_validation->run() == true) {
                $insert = $this->Admin_model->insert($userData);
                if ($insert) {
                    $this->session->set_flashdata('msg', 'New user registered successfully.');
                    redirect('admin/dashboard');
                } else {
                    $this->session->set_flashdata('msg', 'Some error occured, Please try again.');
                    redirect('admin/register-new-user');
                }
            } else {
                $data['error_msg'] = 'Please fill all the mandatory fields.';
            }
        }

        // Posted data 
        $data['user'] = $userData;

        // Load view
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Register New User';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/registration', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function change_password() {

        $data = $userData = array();

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            if ($this->input->post('Submit')) {
                $this->form_validation->set_rules('current_password', 'Current Password', 'callback_password_check[' . $con['id'] . ']');
                $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');

                $userData = array(
                    'password' => md5($this->input->post('new_password')),
                );

                if ($this->form_validation->run() == true) {
                    $update = $this->Admin_model->update($userData, $con);
                    if ($update) {
                        $this->session->set_userdata('success_msg', 'Some problems occured, please try again.');
                        redirect('admin/change-password');
                    } else {
                        $data['error_msg'] = 'Your password has been updated successfully.';
                    }
                } else {
                    $data['error_msg'] = '';
                }
            }

            $data['user'] = $userData;

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Change Password';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/change-password', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function password_check($current_password, $con) {

        $id = $con;
        $user = $this->Admin_model->getAdmindata($id);

        if ($user[0]->password !== md5($current_password)) {
            $this->form_validation->set_message('password_check', 'The {field} does not match');
            return false;
        }
        return true;
    }

    public function logout() {

        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->sess_destroy();

        redirect('admin/login/');
    }

    // Existing email check during validation register
    public function email_check($str) {
        $con = array(
            'returnType' => 'count',
            'conditions' => array(
                'email' => $str
            )
        );
        //$checkEmail = $this->User_model->getRows($con); 
        //if($checkEmail > 0){ 
        // $this->form_validation->set_message('email_check', 'The given email already exists.'); 
        // return FALSE; 
        //}else{ 
        return TRUE;
        //} 
    }

    public function reset_password() {

        $data['title'] = 'Reset Password';

        $this->load->view('partials/admin-header', $data);
        $this->load->view('admin/reset-password', $data);
        $this->load->view('partials/admin-footer');
    }

    public function send_link() {

        $email = $this->input->post('email');
        $email_check = $this->Admin_model->email_exist($email);

        if ($email_check) {

            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.gmail.com',
                'smtp_port' => '587',
                'smtp_user' => 'mail.bharatvaani.in',
                'smtp_pass' => 'bharatvaani@2019',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );

            $this->load->library('email', $config);
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            //Email content
            $htmlContent = '<h1>Password Reset Link</h1> <br><br>';
            $htmlContent .= '<p>Click <a href="http://localhxost/bv-test/admin/update-password">here</a> to reset your password.</p>';

            $this->email->to($email);
            $this->email->from('mail.bharatvaani.in', 'Link');
            $this->email->subject('Password Reset Link');
            $this->email->message($htmlContent);

            //Send email
            $this->email->send();

            if ($this->email->send()) {
                $this->session->set_flashdata('msg', 'A link to reset your password has been successfully sent to your email.');
                redirect('admin/reset-password');
            } else {
                //$this->session->set_flashdata('msg', 'Error! Please try again.');
                //redirect('admin/reset-password');
                show_error($this->email->print_debugger());
            }
            redirect('admin/reset-password');
        } else {

            $this->session->set_flashdata('msg', 'Email not found! Please enter your registered email.');
            redirect('admin/reset-password');
        }
    }

    public function update_password() {

        $data['title'] = 'Update Password';

        $this->load->view('partials/admin-header', $data);
        $this->load->view('admin/update-password', $data);
        $this->load->view('partials/admin-footer');
    }

    public function update_password_save() {

        $data = $userData = array();


        $email = $this->input->post('email');
        $email_check = $this->Admin_model->email_exist($email);

        if ($email_check) {


            if ($this->input->post('Submit')) {
                $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
                $this->form_validation->set_rules('password_conf', 'Confirm Password', 'trim|required|matches[new_password]');

                $userData = array(
                    'password' => md5($this->input->post('new_password')),
                );

                if ($this->form_validation->run() == true) {
                    $email = $this->input->post('email');
                    $update = $this->Admin_model->updatePassword($email, $userData);
                    if ($update) {
                        $this->session->set_flashdata('msg', 'Some problems occured, please try again.');
                        redirect('admin/update-password');
                    } else {
                        $this->session->set_flashdata('msg', 'Your password has been reset successfully, Now you can login to your account!');
                        redirect('admin/login');
                    }
                } else {
                    $this->session->set_flashdata('msg', 'New Password and Confirm Password are not matching.');
                    redirect('admin/update-password');
                }
            }
        } else {

            $this->session->set_flashdata('msg', 'Email not found! Please enter your registered email.');
            redirect('admin/update-password');
        }
    }

    public function edit_profile() {

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['title'] = 'Edit Profile';

            //print_r($data); exit;

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/edit-profile', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function update_profile() {

        $data = $this->input->post();

        if ($data) {
            $id = $data['id'];
            unset($data['id']);
            unset($data['Submit']);

            $this->load->model('Admin_model');

            if ($this->Admin_model->updatePost($data, $id)) {
                $this->session->set_flashdata('msg', 'Profile Updated Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Update Profile!');
            }
            return redirect('admin/edit-profile');
        } else {
            $this->load->view('admin/dashboard');
        }
    }

    public function clients() {

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $this->load->model('Admin_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['title'] = "Details";

            $data['count_users'] = $this->Admin_model->getUsers();
            $data['count_clients'] = $this->Admin_model->getClients();

            $data['count_editorspick'] = $this->Admin_model->getEditorspick();
            $data['count_editorstotal'] = $this->Admin_model->getEditorspicktotal();
            $data['count_editorspickassamese'] = $this->Admin_model->getEditorspickassamese();
            $data['count_editorspickbangla'] = $this->Admin_model->getEditorspickbangla();
            $data['count_editorspickhindi'] = $this->Admin_model->getEditorspickhindi();
            $data['count_editorspickgujarati'] = $this->Admin_model->getEditorspickgujarati();
            $data['count_editorspickmarathi'] = $this->Admin_model->getEditorspickmarathi();
            $data['count_editorspicktamil'] = $this->Admin_model->getEditorspicktamil();
            $data['count_editorspicktelugu'] = $this->Admin_model->getEditorspicktelugu();

            $data['count_inspiringpeople'] = $this->Admin_model->getInspiringpeople();
            $data['count_inspiringpeopletotal'] = $this->Admin_model->getInspiringpeopletotal();
            $data['count_inspiringpeopleassamese'] = $this->Admin_model->getInspiringpeopleassamese();
            $data['count_inspiringpeoplebangla'] = $this->Admin_model->getInspiringpeoplebangla();
            $data['count_inspiringpeoplehindi'] = $this->Admin_model->getInspiringpeoplehindi();
            $data['count_inspiringpeoplegujarati'] = $this->Admin_model->getInspiringpeoplegujarati();
            $data['count_inspiringpeoplemarathi'] = $this->Admin_model->getInspiringpeoplemarathi();
            $data['count_inspiringpeopletamil'] = $this->Admin_model->getInspiringpeopletamil();
            $data['count_inspiringpeopletelugu'] = $this->Admin_model->getInspiringpeopletelugu();

            $data['count_categories'] = $this->Admin_model->getCategories();
            $data['count_categoriestotal'] = $this->Admin_model->getCategoriestotal();
            $data['count_categoriesassamese'] = $this->Admin_model->getCategoriesassamese();
            $data['count_categoriesbangla'] = $this->Admin_model->getCategoriesbangla();
            $data['count_categorieshindi'] = $this->Admin_model->getCategorieshindi();
            $data['count_categoriesgujarati'] = $this->Admin_model->getCategoriesgujarati();
            $data['count_categoriesmarathi'] = $this->Admin_model->getCategoriesmarathi();
            $data['count_categoriestamil'] = $this->Admin_model->getCategoriestamil();
            $data['count_categoriestelugu'] = $this->Admin_model->getCategoriestelugu();

            $data['posts'] = $this->Admin_model->getClientsdata();

            //echo "<pre>";
            //print_r($data); 
            //echo "</pre>";
            //exit;
            // Pass the user data and load view 
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/clients', $data);
            $this->load->view('partials/admin-footer');
        } else {
            redirect('admin/login');
        }
    }

    public function submit_stories() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Submitstories_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/submit-stories/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Submitstories_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Submit Stories';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/submitted_stories', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function stories_delete($id) {

        $this->load->model('Submitstories_model');

        if ($this->Submitstories_model->deleteStories($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/submit-stories');
    }

    public function stories_details($id) {

        $this->load->model('Submitstories_model');
        $post['post'] = $this->Submitstories_model->getSingleStories($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Submit Stories';


            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/story_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function contact_us() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        // If search request submitted
        $searchKeyword = "";
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Contactus_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/contact-us/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Contactus_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Contact Us';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/contact', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function contacts_delete($id) {

        $this->load->model('Contactus_model');

        if ($this->Contactus_model->deleteContact($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/contact-us');
    }

    public function contact_us_details($id) {

        $this->load->model('Contactus_model');
        $post['post'] = $this->Contactus_model->getSingleContact($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Contact Details';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/contact_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function partner_with_us() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
            $searchKeyword = "";
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Partnerwithus_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/submit-stories/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Partnerwithus_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Partner With Us';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/partner', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function partners_delete($id) {

        $this->load->model('Partnerwithus_model');

        if ($this->Partnerwithus_model->deletePartner($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/partner-with-us');
    }

    public function partner_with_us_details($id) {

        $this->load->model('Partnerwithus_model');
        $post['post'] = $this->Partnerwithus_model->getSinglePartner($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Partner With Us Details';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/partner_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function job_application() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Jobapplication_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/job-application/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Jobapplication_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Job Applications';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/job_application', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function jobs_delete($id) {

        $this->load->model('Jobapplication_model');

        $result = $this->db->get_where('job_application', ['id' => $id]);
        $rows = $result->result();

        foreach ($rows as $row) {
            unlink('assets/resume/' . $row->filename);
        }

        if ($this->Jobapplication_model->deleteJob($id)) {
            $this->session->set_flashdata('msg', 'Application Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Application cannot be Deleted!');
        }
        return redirect('admin/job-application');
    }

    public function job_application_details($id) {

        $this->load->model('Jobapplication_model');
        $post['post'] = $this->Jobapplication_model->getSingleJob($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = 'Job Applications';

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/job_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function dashboard() {

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $this->load->model('Admin_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['title'] = "Admin Dashboard";

            $data['count_users'] = $this->Admin_model->getUsers();
            $data['count_clients'] = $this->Admin_model->getClients();

            $data['count_editorspick'] = $this->Admin_model->getEditorspick();
            $data['count_editorstotal'] = $this->Admin_model->getEditorspicktotal();
            $data['count_editorspickassamese'] = $this->Admin_model->getEditorspickassamese();
            $data['count_editorspickbangla'] = $this->Admin_model->getEditorspickbangla();
            $data['count_editorspickhindi'] = $this->Admin_model->getEditorspickhindi();
            $data['count_editorspickgujarati'] = $this->Admin_model->getEditorspickgujarati();
            $data['count_editorspickmarathi'] = $this->Admin_model->getEditorspickmarathi();
            $data['count_editorspicktamil'] = $this->Admin_model->getEditorspicktamil();
            $data['count_editorspicktelugu'] = $this->Admin_model->getEditorspicktelugu();

            $data['count_inspiringpeople'] = $this->Admin_model->getInspiringpeople();
            $data['count_inspiringpeopletotal'] = $this->Admin_model->getInspiringpeopletotal();
            $data['count_inspiringpeopleassamese'] = $this->Admin_model->getInspiringpeopleassamese();
            $data['count_inspiringpeoplebangla'] = $this->Admin_model->getInspiringpeoplebangla();
            $data['count_inspiringpeoplehindi'] = $this->Admin_model->getInspiringpeoplehindi();
            $data['count_inspiringpeoplegujarati'] = $this->Admin_model->getInspiringpeoplegujarati();
            $data['count_inspiringpeoplemarathi'] = $this->Admin_model->getInspiringpeoplemarathi();
            $data['count_inspiringpeopletamil'] = $this->Admin_model->getInspiringpeopletamil();
            $data['count_inspiringpeopletelugu'] = $this->Admin_model->getInspiringpeopletelugu();

            $data['count_categories'] = $this->Admin_model->getCategories();
            $data['count_categoriestotal'] = $this->Admin_model->getCategoriestotal();
            $data['count_categoriesassamese'] = $this->Admin_model->getCategoriesassamese();
            $data['count_categoriesbangla'] = $this->Admin_model->getCategoriesbangla();
            $data['count_categorieshindi'] = $this->Admin_model->getCategorieshindi();
            $data['count_categoriesgujarati'] = $this->Admin_model->getCategoriesgujarati();
            $data['count_categoriesmarathi'] = $this->Admin_model->getCategoriesmarathi();
            $data['count_categoriestamil'] = $this->Admin_model->getCategoriestamil();
            $data['count_categoriestelugu'] = $this->Admin_model->getCategoriestelugu();

            $data['posts'] = $this->Admin_model->getUsersdata();

            //echo "<pre>";
            //print_r($data); 
            //echo "</pre>";
            //exit;
            // Pass the user data and load view 
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/dashboard', $data);
            $this->load->view('partials/admin-footer');
        } else {
            redirect('admin/login');
        }
    }

    public function user_delete($id) {

        $this->load->model('Admin_model');

        $result = $this->db->get_where('users', ['id' => $id]);
        $rows = $result->result();

        // foreach($rows as $row) {
        //    unlink('assets/images/users/avatar/'.$row->filename);
        // }

        if ($this->Admin_model->deleteUser($id)) {
            $this->session->set_flashdata('msg', 'User Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/dashboard');
    }

    public function client_delete($id) {

        $this->load->model('Admin_model');

        $result = $this->db->get_where('clients', ['id' => $id]);
        $rows = $result->result();

        // foreach($rows as $row) {
        //    unlink('assets/images/users/avatar/'.$row->filename);
        // }

        if ($this->Admin_model->deleteClient($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/clients');
    }

    public function inspiring_people() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }

        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                //$this->session->set_userdata('searchKeyword',$searchKeyword);
            } else {
                //$this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            //$this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword;

        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/inspiring_people', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                // $this->session->set_userdata('searchKeyword',$searchKeyword);
            } else {
                //  $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            //$this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getAssameserows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getAssameserows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                // $this->session->set_userdata('searchKeyword',$searchKeyword);
            } else {
                // $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            //$this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getBanglarows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getBanglarows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getHindirows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getHindirows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getMarathirows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getMarathirows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getGujaratirows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getGujaratirows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getTamilrows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getTamilrows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiring_people_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Inspiringpeoples_model->getTelugurows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/inspiring-people-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Inspiringpeoples_model->getTelugurows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Inspiring People";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/inspiring-people-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiringpeople_create() {

        $this->load->model('Editorspick_model');
        $getLanguage = $this->Editorspick_model->getLanguage();
        $apply['getLanguage'] = $getLanguage;

        $this->load->view('admin/inspiring_create', $apply);
    }

    public function inspiringpeople_save() {

        if (empty($_FILES['video']['name']) && $_POST['video_link'] == "") {
            $this->form_validation->set_rules('video', 'Video', 'required');
        }

        $this->form_validation->set_rules('title', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[1000]');
        $this->form_validation->set_rules('language', 'Language', 'required');

        if ($this->form_validation->run()) {

            $data = $this->input->post();

            $today = date('Y-m-d');
            $data['date_created'] = $today;

            if (!empty($_FILES['video']['name'])) {
                $data['video'] = base_url("/assets/videos/inspiring_videos/" . $_FILES['video']['name']);
                $data['type'] = "File";
                $data['filename'] = $_FILES['video']['name'];
            } else {
                $data['video'] = $data['video_link'];
                $data['type'] = "Link";
            }
            $data['uploaded_user'] = "Admin";
            $data['reg_id'] = $this->session->userdata('userId');
            unset($data['submit']);
            unset($data['video_link']);

            $this->load->model('Inspiringpeoples_model');

            if ($this->Inspiringpeoples_model->addPost($data)) {
                if (!empty($_FILES['video']['name'])) {



                    $config['upload_path'] = './assets/videos/inspiring_videos/';
                    $config['allowed_types'] = 'mp4|mov|flv|3gp|ogg|wmv|avi';
                    $config['max_size'] = 1024;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('video')) {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);
                        $this->load->view('admin/inspiring_people', $error);
                    } else {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('admin/inspiring_people', $data);
                    }
                } else {
                    
                }
                $this->session->set_flashdata('msg', 'Video Uploaded Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Upload Video!');
            }
            return redirect('admin/inspiring-people');
        } else {
            $this->load->model('Uploadvideos_model');
            $getLanguage = $this->Uploadvideos_model->getLanguage();
            $apply['getLanguage'] = $getLanguage;
            $this->load->view('admin/inspiringpeople_create', $apply);
        }
    }

    public function inspiringpeople_details($id) {

        $this->load->model('Inspiringpeoples_model');
        $post['post'] = $this->Inspiringpeoples_model->getSinglePosts($id);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Post Details";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/inspiring_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiringpeople_update($id) {

        $this->load->model('Inspiringpeoples_model');
        $post = $this->Inspiringpeoples_model->getSinglePosts($id);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Admin Dashboard";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/inspiring_update', ['post' => $post]);
            $this->load->view('partials/admin-footer');
        }
    }

    public function inspiringpeople_change($id) {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run()) {
            $data = $this->input->post();

            unset($data['submit']);

            $this->load->model('Inspiringpeoples_model');

            if ($this->Inspiringpeoples_model->updatePost($data, $id)) {
                $this->session->set_flashdata('msg', 'Post Updated Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Update Post!');
            }
            return redirect('admin/inspiring-people');
        } else {
            $this->load->view('admin/inspiring-create');
        }
    }

    public function inspiringpeople_delete($id) {

        $this->load->model('Inspiringpeoples_model');

        $result = $this->db->get_where('insp_people', ['id' => $id]);
        $rows = $result->result();

        foreach ($rows as $row) {
            unlink('assets/videos/inspiring_videos/' . $row->filename);
        }

        if ($this->Inspiringpeoples_model->deletePosts($id)) {
            $this->session->set_flashdata('msg', 'Post Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Post cannot be Deleted!');
        }
        return redirect('admin/inspiring-people');
    }

    public function editors_pick() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/editor_pick', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getAssameseRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getAssameseRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getBanglaRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getBanglaRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getHindiRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getHindiRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getGujaratiRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getGujaratiRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $searchKeyword; // $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getMarathiRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getMarathiRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $searchKeyword; //$data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getTamilRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getTamilRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editors_pick_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Editorspick_model->getTeluguRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/editors-pick-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Editorspick_model->getTeluguRows($conditions);

        // Page
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Editor's Pick";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/editors-pick-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editorspick_create() {

        $this->load->model('Editorspick_model');
        $getLanguage = $this->Editorspick_model->getLanguage();
        $apply['getLanguage'] = $getLanguage;

        $this->load->view('admin/editor_create', $apply);
    }

    public function editorspick_details($id) {

        $this->load->model('Editorspick_model');
        $post['post'] = $this->Editorspick_model->getSinglePosts($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Post Details";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/editor_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editorspick_save() {

        if (empty($_FILES['video']['name']) && $_POST['video_link'] == "") {
            $this->form_validation->set_rules('video', 'Video', 'required');
        }

        $this->form_validation->set_rules('title', 'Title', 'required|max_length[100]');
        $this->form_validation->set_rules('description', 'Description', 'required|max_length[1000]');
        $this->form_validation->set_rules('language', 'Language', 'required');

        if ($this->form_validation->run()) {

            $data = $this->input->post();

            if (!empty($_FILES['video']['name'])) {
                $data['video'] = base_url("/assets/videos/editors_pick/" . $_FILES['video']['name']);
                $data['type'] = "File";
                $data['filename'] = $_FILES['video']['name'];
            } else {
                $data['video'] = $data['video_link'];
                $data['type'] = "Link";
            }

            $today = date('Y-m-d');
            $data['date_created'] = $today;

            unset($data['submit']);
            unset($data['video_link']);

            $this->load->model('Editorspick_model');

            if ($this->Editorspick_model->addPost($data)) {
                if (!empty($_FILES['video']['name'])) {
                    $config['upload_path'] = './assets/videos/editors_pick/';
                    $config['allowed_types'] = 'mp4|mov|flv|3gp|ogg|wmv|avi';
                    $config['max_size'] = 1024;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('video')) {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);
                        $this->load->view('admin/editors_pick', $error);
                    } else {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('admin/editors_pick', $data);
                    }
                } else {
                    
                }
                $this->session->set_flashdata('msg', 'Video Uploaded Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Upload Video!');
            }
            return redirect('admin/editor-pick');
        } else {
            $this->load->model('Uploadvideos_model');
            $getLanguage = $this->Uploadvideos_model->getLanguage();
            $apply['getLocation'] = $getLanguage;
            $this->editorspick_create();
        }
    }

    public function editorspick_delete($id) {

        $this->load->model('Editorspick_model');

        $result = $this->db->get_where('editors_pick', ['id' => $id]);
        $rows = $result->result();

        foreach ($rows as $row) {
            unlink('assets/videos/editors_pick/' . $row->filename);
        }

        if ($this->Editorspick_model->deletePosts($id)) {
            $this->session->set_flashdata('msg', 'Post Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Post cannot be Deleted!');
        }
        return redirect('admin/editors-pick');
    }

    public function editorspick_update($id) {

        $this->load->model('Editorspick_model');
        $post = $this->Editorspick_model->getSinglePosts($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Video Details";
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/editor_update', ['post' => $post]);
            $this->load->view('partials/admin-footer');
        }
    }

    public function editorspick_change($id) {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run()) {
            $data = $this->input->post();

            unset($data['submit']);

            $this->load->model('Editorspick_model');

            if ($this->Editorspick_model->updatePost($data, $id)) {
                $this->session->set_flashdata('msg', 'Post Updated Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Update Post!');
            }
            return redirect('admin/editors-pick');
        } else {
            $this->load->view('admin/editorspick-create');
        }
    }

    public function edit_details($id) {

        $this->load->model('Editorspick_model');
        $post = $this->Editorspick_model->getSinglePosts($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Video Details";
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/edit-details', ['post' => $post]);
            $this->load->view('partials/admin-footer');
        }
    }

    public function details_change($id) {

        $reg_id = $this->session->userdata('userId');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('category', 'Category');
        $this->form_validation->set_rules('subcategory', 'Sub-Category');
        $this->form_validation->set_rules('category2', 'Category2');
        $this->form_validation->set_rules('cust_url', 'cust_url');
        $this->form_validation->set_rules('subcategory2', 'Sub-Category2');

        //  $this->form_validation->set_rules('category3', 'Category3');
        //  $this->form_validation->set_rules('subcategory3', 'Sub-Category3');
        //  $this->form_validation->set_rules('category4', 'Category4');
        //  $this->form_validation->set_rules('subcategory4', 'Sub-Category4');
        //  $this->form_validation->set_rules('category5', 'Category5');
        //  $this->form_validation->set_rules('subcategory5', 'Sub-Category5');

        if ($this->form_validation->run()) {
            $data = $this->input->post();
            $data['url'] = $data['cust_url'];
            unset($data['cust_url']);
            unset($data['submit']);
            unset($data['image_info']);
            $this->load->model('Editorspick_model');

            if ($this->Editorspick_model->updatePost($data, $id)) {


                if (isset($_FILES['thumbnail']['name']) && $_FILES['thumbnail']['name'] != '') {
                    $_FILES['thumbnail']['name'] = str_replace(" ", "_", $_FILES['thumbnail']['name']);
                    $user_video = $this->Common_model->upload_org_filename('thumbnail', 'admin_video/' . $reg_id . '/' . $id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                    if ($user_video != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('video', array('thumbnail' => base_url("/public/upload/admin_video/" . $reg_id . '/' . $id . "/thumbs/thumbs/" . $_FILES['thumbnail']['name'])), array('reg_id' => $reg_id, 'id' => $id));
                    }
                }
                $this->session->set_flashdata('msg', 'Post Updated Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Update Post!');
            }
            return redirect('admin/view-uploaded-video');
        } else {
            $this->load->view('admin/view-uploaded-video');
        }
    }

    public function upload_video() {

        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/upload_video', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function upload() {
        //$reg_id=$this->session->userdata['user_session']['reg_id'];

        $post = $this->input->post();



        /* if(empty($_FILES['video']['name']) &&  isset($_POST['video_link']) && $_POST['video_link'] ==""){
          $this->form_validation->set_rules('video', 'Video File','required');
          }
          $reg_id=$this->session->userdata('userId') ;
          $this->form_validation->set_rules('particular', 'Particular');
          //$this->form_validation->set_rules('language', 'Language', 'required');
          $this->form_validation->set_rules('title', 'Title', 'required|max_length[100]');
          $this->form_validation->set_rules('description', 'Description', 'required|max_length[1000]');
          $this->form_validation->set_rules('category', 'Category');
          $this->form_validation->set_rules('subcategory', 'Sub-Category');
          $this->form_validation->set_rules('category2', 'Category2');
          $this->form_validation->set_rules('subcategory2', 'Sub-Category2');
          // $this->form_validation->set_rules('category3', 'Category3');
          // $this->form_validation->set_rules('subcategory3', 'Sub-Category3');
          // $this->form_validation->set_rules('category4', 'Category4');
          //  $this->form_validation->set_rules('subcategory4', 'Sub-Category4');
          //  $this->form_validation->set_rules('category5', 'Category5');
          // $this->form_validation->set_rules('subcategory5', 'Sub-Category5');
          $this->form_validation->set_rules('uploaded_by', 'Uploaded By');

          if($this->form_validation->run()) {

          $data = $this->input->post();

          $today = date('Y-m-d');
          $data['date_created'] = $today;

          if(!empty($_FILES['video']['name'])){
          //  $data['video']=base_url("/public/upload/admin_video/".$reg_id.'/'.$get_video_id);
          $data['type']="File";
          $data['filename']=$_FILES['video']['name'];
          }
          else {
          $data['video']=$data['video_link'];
          $data['type']="Link";
          }
          $data['uploaded_user']="Admin";
          $data['language']="English";
          $data['reg_id']=$this->session->userdata('userId');
          $data['status']="Active";
          unset($data['submit']);
          unset($data['video_link']);
          unset($data['image_info']);

          $this->load->model('Uploadvideos_model');
          $get_video_id=$this->Uploadvideos_model->addData($data);
          $video_id=$this->db->insert_id();
          if($get_video_id) {
          if(!empty($_FILES['video']['name'])) {
          $user_video=$this->Common_model->upload_org_filename('video','admin_video/'.$reg_id.'/'.$video_id ,$allowd="mp4|mkv|mov",array('width'=>200,'height'=>300));

          $get_update_id = $this->Adminmaster_model->updateData('video',array('filename'=>$_FILES['video']['name'],'video'=>base_url("/public/upload/admin_video/".$reg_id.'/'.$video_id)),array('reg_id'=>$reg_id,'id'=>$video_id ));

          if($_FILES['thumbnail']['name'] != '')
          {
          $user_video=$this->Common_model->upload_org_filename('thumbnail','admin_video/'.$reg_id.'/'.$video_id.'/thumbs' ,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
          if($user_video!=false){

          $get_update_id = $this->Adminmaster_model->updateData('video',array('thumbnail'=>base_url("/public/upload/admin_video/".$reg_id.'/'.$video_id."/thumbs/thumbs/".$_FILES['thumbnail']['name'])),array('reg_id'=>$reg_id,'id'=>$video_id ));


          }
          }
          /* $config['upload_path'] = './assets/videos/uploaded';
          $config['allowed_types'] = 'mp4|mov|flv|3gp|ogg|wmv|avi';
          $config['max_size'] = 1024;

          $this->load->library('upload', $config); */

        /* if( !  $user_video) {
          $error = array('error' => "Video not uploaded.");

          $this->load->view('admin/upload_video',$error);
          }
          else {
          $data = array('upload_data' => $_FILES['video']);

          $this->load->view('admin/upload_video', $data);
          }
          }
          else {

          }
          $this->session->set_flashdata('msg', 'Video Uploaded Successfully!');
          }
          else {
          $this->session->set_flashdata('msg', 'Failed to Upload Video!');
          }
          redirect('admin/upload');
          }
          else {
          $this->load->model('Uploadvideos_model');
          $getLanguage = $this->Uploadvideos_model->getLanguage();
          $apply['getLocation']=$getLanguage;
          $this->upload_video();
          } */



        $post = $this->input->post();
        //print_r($post); exit();
        $reg_id = $this->session->userdata('userId');

        $today = date('Y-m-d');

        if (!empty($_FILES['file']['name'])) {
            //  $data['video']=base_url("/public/upload/admin_video/".$reg_id.'/'.$get_video_id);
            $type = "File";
            $video = "";
        } else {
            $video = $post['link'];
            $type = "Link";
        }

        $video_info = array();
        $video_info = $_FILES;
        $insertData = array(
            'reg_id' => $reg_id,
            'video' => $video,
            'title' => $post['title'],
            'description' => $post['desc'],
            'tags' => $post['tags'],
            'url' => $post['url'],
            //   'category'=>$post['category'],
            'status' => 'Active',
            'uploaded_user' => "Admin",
            'uploaded_by' => $post['username'],
            'date_created' => $today,
            'type' => $type,
            'particular' => 'Categories',
            'language' => 'English',
            'category' => $post['category'],
            'subcategory' => $post['subcategory'],
            'category2' => $post['category1'],
            'subcategory2' => $post['subcategory1'],
        );

        /* if(isset($post['category'])){

          //$category=explode(",",$post['category']);

          foreach($post['category'] as $key=>$val){
          if($key==0){
          $insertData['category']=$val;
          }
          if($key==1){
          $insertData['category2']=$val;
          }
          if($key==2){
          $insertData['category3']=$val;
          }
          if($key==3){
          $insertData['category4']=$val;
          }
          if($key==4){
          $insertData['category5']=$val;
          }
          }

          }

          if(isset($post['subcategory'])){

          //$category=explode(",",$post['category']);

          foreach($post['subcategory'] as $keySub=>$valSub){
          if($keySub==0){
          $insertData['subcategory']=$valSub;
          }
          if($keySub==1){
          $insertData['subcategory2']=$valSub;
          }
          if($keySub==2){
          $insertData['subcategory3']=$valSub;
          }
          if($keySub==3){
          $insertData['subcategory4']=$valSub;
          }
          if($keySub==4){
          $insertData['subcategory5']=$valSub;
          }
          }

          } */

        $get_video_id = $this->Adminmaster_model->insertData('video', $insertData);
        //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);

        if ($get_video_id) {
            if ($video_info['file']['name'] != '') {
                $video_info['file']['name'] = str_replace(" ", "_", $video_info['file']['name']);
                $user_video = $this->Common_model->upload_org_filename('file', 'admin_video/' . $reg_id . '/' . $get_video_id, $allowd = "mp4|mkv|mov", array('width' => 200, 'height' => 300));
                if ($user_video != false) {

                    $get_update_id = $this->Adminmaster_model->updateData('video', array('filename' => $video_info['file']['name'], 'video' => base_url("/public/upload/admin_video/" . $reg_id . '/' . $get_video_id)), array('reg_id' => $reg_id, 'id' => $get_video_id));
                }
            }

            if ($video_info['thumbnail']['name'] != '') {
                $video_info['thumbnail']['name'] = str_replace(" ", "_", $video_info['thumbnail']['name']);
                $user_video = $this->Common_model->upload_org_filename('thumbnail', 'admin_video/' . $reg_id . '/' . $get_video_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                if ($user_video != false) {

                    $get_update_id = $this->Adminmaster_model->updateData('video', array('thumbnail' => base_url("/public/upload/admin_video/" . $reg_id . '/' . $get_video_id . "/thumbs/" . $video_info['thumbnail']['name'])), array('reg_id' => $reg_id, 'id' => $get_video_id));
                }
            }

            ////////////////////////////////////Bvcohorts///////////////////////
            // SERVER A - UPLOAD FILE VIA CURL POST
            // (A) SETTINGS
            $urls = array();
            //array of cURL handles
            //set the urls
            //$urls[] = 'https://xebra.in/index/insert_video';
            //$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            //$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            $urls[] = 'https://onecohort.in/index/insert_video';
            //$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            $file = DOC_ROOT . "/public/upload/admin_video/" . $reg_id . "/" . $get_video_id . "/" . $_FILES['file']['name']; // File to upload
            $upname = $_FILES['file']['name']; // File name to be uploaded as

            $cover = DOC_ROOT . "/public/upload/admin_video/" . $reg_id . "/" . $get_video_id . "/thumbs/" . $_FILES['thumbnail']['name']; // File to upload
            $covername = $_FILES['thumbnail']['name']; // File name to be uploaded as
            // (B) NEW CURL FILE
            $cf = new CURLFile($file, mime_content_type($file), $upname);
            $cfile = new CURLFile($cover, mime_content_type($cover), $covername);
            $data = $insertData;

            $data['upload'] = $cf;
            $data['thumbnail'] = $cfile;
            // (C) CURL INIT
            foreach ($urls as $key => $url) {
                //print $url;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // (D) CURL RUN
                // (D1) GO!
                $result = curl_exec($ch);

                // (D2) CURL ERROR
                if (curl_errno($ch)) {
                    //  echo "CURL ERROR - " . curl_error($ch);
                }

                // (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                else {
                    // $info = curl_getinfo($ch);
                    // print_r($info);
                    // echo $result;
                }

                // (D4) DONE
                curl_close($ch);
            }

            ////////////////////////////////////Bvcohorts///////////////////////
            echo true;
        } else {
            echo false;
        }
    }

    public function view_uploaded_video() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getRows($conditions);
        // print $this->db->last_query();exit;
        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/view_video', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getAssameseRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getAssameseRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getBanglaRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getBanglaRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getHindiRows($conditions);



        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getHindiRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getGujaratiRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getGujaratiRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getMarathiRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getMarathiRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $searchKeyword; //$data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getTamilRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getTamilRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function view_uploaded_video_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->getTeluguRows($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/view-uploaded-video-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->getTeluguRows($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/parts/view-uploaded-video-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function videos_update($id) {

        $this->load->model('Viewvideos_model');
        $post = $this->Viewvideos_model->getSinglePosts($id);

        $this->load->view('admin/video_update', ['post' => $post]);
    }

    public function video_details($id) {

        $this->load->model('Viewvideos_model');
        $post['post'] = $this->Viewvideos_model->getSinglePosts($id);

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['title'] = "Video Details";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/video_view', $post);
            $this->load->view('partials/admin-footer');
        }
    }

    public function videos_change($id) {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run()) {
            $data = $this->input->post();

            $today = date('Y-m-d');
            $data['date_created'] = $today;

            unset($data['submit']);

            $this->load->model('Viewvideos_model');

            if ($this->Viewvideos_model->updatePost($data, $id)) {
                $this->session->set_flashdata('msg', 'Post Updated Successfully!');
            } else {
                $this->session->set_flashdata('msg', 'Failed to Update Post!');
            }
            return redirect('admin/editor-pick');
        } else {
            $this->load->view('admin/editor-create');
        }
    }

    public function videos_delete($id) {

        $this->load->model('Viewvideos_model');

        $result = $this->db->get_where('video_upload', ['id' => $id]);
        $rows = $result->result();

        foreach ($rows as $row) {
            unlink('assets/videos/uploaded/' . $row->filename);
        }

        if ($this->Viewvideos_model->deletePosts($id)) {
            $this->session->set_flashdata('msg', 'Post Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Post cannot be Deleted!');
        }
        return redirect('admin/view-uploaded-video');
    }

    public function category_art_culture_and_artist() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artists($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artists($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artistassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artistassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artistbangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artistbangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artistgujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artistgujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $searchKeyword; //$data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artisthindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artisthindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artistmarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artistmarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artisttamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artisttamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_art_culture_and_artist_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->artisttelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-art-culture-and-artist-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->artisttelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/art-culture-and-artists-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->science($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->science($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->scienceassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->scienceassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencebangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencebangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencegujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencegujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencehindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencehindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencemarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencemarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencetamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencetamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_science_and_tech_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->sciencetelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-science-and-tech-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->sciencetelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/science-and-tech-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->child($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->child($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->child($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childbangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childbangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childgujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childgujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childhindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childhindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childmarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childmarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childtamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childtamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_child_and_elderly_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->childtelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-child-and-elderly-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->childtelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/child-and-elderly-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->education($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->education($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationbangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationbangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $searchKeyword; //$data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationgujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationgujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationhindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationhindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationmarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationmarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationtamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationtamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_education_and_development_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->educationtelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-education-and-development-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->educationtelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/education-and-development-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->health($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->health($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthbangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthbangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthgujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthgujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthhindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthhindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthmarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthmarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthtamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthtamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_health_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->healthtelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-health-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->healthtelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/health-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestyle($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestyle($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestyleassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestyleassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestylebangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestylebangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestylegujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestylegujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestylehindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestylehindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestylemarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestylemarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestyletamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestyletamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_lifestyle_and_outdoors_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->lifestyletelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-lifestyle-and-outdoors-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->lifestyletelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/lifestyle-and-outdoors-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->people($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->people($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peopleassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peopleassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peoplebangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peoplebangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peoplegujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peoplegujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peoplehindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peoplehindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peoplemarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peoplemarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peopletamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peopletamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_people_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; // $this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->peopletelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-people-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->peopletelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/people-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->pets($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->pets($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_assamese() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petsassamese($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-assamese/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petsassamese($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-assamese', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_bangla() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petsbangla($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-bangla/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petsbangla($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-bangla', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_gujarati() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petsgujarati($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-gujarati/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petsgujarati($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-gujarati', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_hindi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petshindi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-hindi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petshindi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-hindi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_marathi() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petsmarathi($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-marathi/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petsmarathi($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-marathi', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_tamil() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petstamil($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-tamil/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petstamil($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-tamil', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function category_pets_and_environment_telugu() {

        $data = array();

        // Get messages from the session
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        $searchKeyword = "";
        // If search request submitted
        if ($this->input->post('submitSearch')) {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if (!empty($searchKeyword)) {
                $this->session->set_userdata('searchKeyword', $searchKeyword);
            } else {
                $this->session->unset_userdata('searchKeyword');
            }
        } elseif ($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
        }
        $data['searchKeyword'] = $searchKeyword; //$this->session->userdata('searchKeyword');
        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['returnType'] = 'count';
        $rowsCount = $this->Uploadvideos_model->petstelugu($conditions);

        // Pagination config
        $config['base_url'] = base_url() . 'admin/category-pets-and-environment-telugu/';
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rowsCount;
        $config['per_page'] = $this->perPage;

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '&nbsp&nbsp&nbsp<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['last_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
        $config['prev_tag_open'] = '<li>&nbsp&nbsp&nbsp';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '&nbsp&nbsp&nbsp<li>';
        $config['next_tag_close'] = '</li>';

        // Initialize pagination library
        $this->pagination->initialize($config);

        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page ? 0 : $page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $data['posts'] = $this->Uploadvideos_model->petstelugu($conditions);

        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);

            $data['title'] = "Uploaded Videos";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/category/pets-and-environment-telugu', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function change_status() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "English");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "English");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People. Please unselect a video to put a new video for Inspiring People.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_hindi() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Hindi");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Hindi");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_assamese() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Assamese");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Assamese");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_bangla() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Bangla");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Bangla");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_gujarati() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Gujarati");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Gujarati");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_marathi() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Marathi");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Marathi");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_tamil() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Tamil");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Tamil");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function change_status_telugu() {
        $post = $this->input->post();

        if ($post['type'] == 'edt') {

            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Telugu");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_edt", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 5) {
                    $msg = "You have already selected 5 videos for Editor's Picks. Please unselect a video to put a new video for Editor's Picks.";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_edt' => $post['status'],
            );
        }

        if ($post['type'] == 'insp') {
            if ($post['status'] == 1) {
                $this->db->select("ep.*");
                $this->db->from("video as ep");
                $this->db->where("language", "Telugu");
                //$this->db->where("particular", "Editors_pick");
                $this->db->where("show_insp", 1);
                $this->db->order_by("ep.id", "DESC");

                $query = $this->db->get();

                $result = $query->result();
                if (count($result) >= 9) {
                    $msg = "You have already selected 9 videos for Inspiring People";
                    echo json_encode($msg);
                    exit;
                }
            }
            $data = array(
                'show_insp' => $post['status'],
            );
        }

        $flag = $this->db->where('id', $post['vid_id'])
                ->update('video', $data);

        echo json_encode($flag);
        exit;
    }

    public function signup_details() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $data['cities'] = $this->Admin_model->selectData('registration', "distinct(user_city)");

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/signup_details_list');
            $this->load->view('partials/admin-footer');
        }
    }

    public function deactivate_user($id) {

        $this->Admin_dashboard_model->updateData('registration', array('status' => 'Inactive'), array('reg_id' => $id));

        $this->session->set_flashdata('success', " User deactivated Successfully.");

        redirect('admin/signup_details');
    }

    public function activate_user($id) {

        $this->Admin_dashboard_model->updateData('registration', array('status' => 'Active'), array('reg_id' => $id));

        $this->session->set_flashdata('success', " User activated Successfully.");

        redirect('admin/signup_details');
    }

    public function print_signup() {

        $array = $this->input->get('ids');
        $array = explode(',', $array);
        //$data['result'] = $this->Admin_dashboard_model->Subscriber_Data_download($array);
        $data['result'] = $this->Admin_model->download_multiple_users( $array);
      
        $this->load->view('admin/export_signup_users', $data);
    }

    public function user_video_upload() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/user_video_upload');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_admin_deatils() {

        $post = $this->input->post();


        $field_pos = array("reg_id" => '0', "createdat" => '1');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchUser($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchUser($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['reg_id'] . '" name="filled-in-box' . $value['reg_id'] . '" class="filled-in purple signup_bulk_action" value="' . $value['reg_id'] . '"/><label for="filled-in-box' . $value['reg_id'] . '"></label>';




            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['reg_id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['reg_id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['reg_id'] . '>
                    
                    ';



            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            if ($value['status'] == 'Active') {
                $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Export</a></div><div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href=" . base_url() . "admin/deactivate_user/" . $value['reg_id'] . ">Deactivate User</a></div>";
            } else {
                $action_links = "<!--div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Export</a></div--><div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href=" . base_url() . "admin/activate_user/" . $value['reg_id'] . ">Activate User</a></div>";
            }
            //$video_info = "<div class='row'><div class='col-lg-2'><img alt='' title='' src='".base_url()."assets/images/team/sapana.png' width='40' height='40'></div><div class='col-lg-10'><p style='text-align:left;'>New Age Water Purifire</p></div></div>";   
            $country = $this->Admin_model->selectData("countries", "*", array('country_id' => $value['user_country']));
            $city = $this->Admin_model->selectData("cities", "*", array('city_id' => $value['user_city']));

            $date1 = date_create(date("Y-m-d", strtotime($value['createdat'])));
            $date2 = date_create(date("Y-m-d"));
            $diff = date_diff($date1, $date2);
            $days = $diff->format("%a");
            $records["data"][] = array(
                $check,
                "BV" . $value['reg_id'] . "</br>" . date('d-m-Y', strtotime($value['createdat'])),
                $value['reg_username'],
                //$value['reg_uniqname'],
                $value['reg_email'],
                base_url() . "profile/" . $value['reg_uniqname'],
                @$country[0]->country_name,
                @$city[0]->name,
                $days,
                $value['status'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function get_uservideo_deatils() {


        $post = $this->input->post();
        // $post['reg_id']=$this->session->userdata['user_session']['reg_id'];

        $field_pos = array("id" => '0', "created_at" => '1');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchUserVideo($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchUserVideo($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';




            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
                    
                    ';



            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Edit</a></div><div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Delete</a></div>";
            $unblock_user = "<div style='padding: 5px 10%;'><a class='action_link' href='#'>Unblock User</a></div>";
            //$video_info = "<div class='row'><div class='col-lg-2'><img alt='' title='' src='".base_url()."assets/images/team/sapana.png' width='40' height='40'></div><div class='col-lg-10'><p style='text-align:left;'>New Age Water Purifire</p></div></div>";     
            $records["data"][] = array(
                $check,
                $value['reg_username'],
                date('d-m-Y', strtotime($value['created_at'])),
                $value['title'],
                $value['description'],
                $value['category'],
                $value['category2'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function top_view_video() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/top_viewed_video');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_topvideo_deatils() {



        $post = $this->input->post();
        //  $post['reg_id']=$this->session->userdata['user_session']['reg_id'];

        $field_pos = array("id" => '0', "created_at" => '1');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchUserTopVideo($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchUserTopVideo($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {
//print_r($value);exit;

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';




            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
                    
                    ';



            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Edit</a></div><div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Delete</a></div>";
            $unblock_user = "<div style='padding: 5px 10%;'><a class='action_link' href='#'>Unblock User</a></div>";
            //$video_info = "<div class='row'><div class='col-lg-2'><img alt='' title='' src='".base_url()."assets/images/team/sapana.png' width='40' height='40'></div><div class='col-lg-10'><p style='text-align:left;'>New Age Water Purifire</p></div></div>";
            //$views=$this->Admin_model->selectData("views","*",array('video_id'=>$value['id']));     

            $records["data"][] = array(
                date('d-m-Y', strtotime($value['created_at'])),
                $value['title'],
                $value['description'],
                $value['category'],
                $value['reg_username'],
                $value['views'],
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function top_engage_video() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/top_engaged_video');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_topengaged_deatils() {



        $post = $this->input->post();
        //$post['reg_id']=$this->session->userdata['user_session']['reg_id'];

        $field_pos = array("id" => '0', "created_at" => '1');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchUserVideo($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchUserVideo($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';




            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
                    
                    ';



            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Edit</a></div><div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='#'>Delete</a></div>";
            $unblock_user = "<div style='padding: 5px 10%;'><a class='action_link' href='#'>Unblock User</a></div>";
            //$video_info = "<div class='row'><div class='col-lg-2'><img alt='' title='' src='".base_url()."assets/images/team/sapana.png' width='40' height='40'></div><div class='col-lg-10'><p style='text-align:left;'>New Age Water Purifire</p></div></div>";     
            $records["data"][] = array(
                date('d-m-Y', strtotime($value['created_at'])),
                $value['title'],
                $value['description'],
                $value['category'],
                "00",
                "00",
                "00",
                "00",
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function active_users_list() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/active_users');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_activeuser_deatils() {

        $post = $this->input->post();


        $field_pos = array("reg_id" => '0', "createdat" => '1');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchActiveUser($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchActiveUser($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {


            $check = '<input type="checkbox" id="filled-in-box' . $value['reg_id'] . '" name="filled-in-box' . $value['reg_id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['reg_id'] . '"/><label for="filled-in-box' . $value['reg_id'] . '"></label>';



            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['reg_id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['reg_id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['reg_id'] . '>
                    
                    ';



            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';

            $records["data"][] = array(
                $value['reg_username'],
                date('d-m-Y', strtotime($value['createdat'])),
                "00",
                "00",
                "00",
                "00",
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function top_categories_list() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/top_categories');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_categories_deatils() {

        $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchCategory($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchCategory($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {


            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';



            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';

            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

              <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                    
                     <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
                    
                    ';

            if ($value['name'] == "Education & Development") {
                $category = "Education";
            }

            if ($value['name'] == "Lifestyle & Outdoors") {
                $category = "Lifestyle";
            }


            if ($value['name'] == "Health") {
                $category = "Health";
            }


            if ($value['name'] == "People") {
                $category = "People";
            }

            if ($value['name'] == "Pets & Environment") {
                $category = "Pets";
            }


            if ($value['name'] == "Science & Tech") {
                $category = "Science";
            }

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';

            $records["data"][] = array(
                $value['name'],
                "00",
                "00",
                "00",
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function video_summary() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Video";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/video_summary');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_videos_summary() {
        $data = $this->input->post();
        $result = $this->Admin_model->get_videos_summary($data);
        echo json_encode($result, JSON_NUMERIC_CHECK);
        exit;
    }

    public function admin_interviews() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Interview";

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/admin_interviews');
            $this->load->view('partials/admin-footer');
        }
    }

    public function get_interview_deatils() {


        $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchInterview($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchInterview($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';
            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';
            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
            ';

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/interview_edit/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/delete_interviews/" . $value['id'] . "'>Delete</a></div>";

            $records["data"][] = array(
                $value['title'],
                $value['name'],
                $value['conduct'],
                $value['filename'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function delete_interviews($id) {

        $get_interview_id = $this->Adminmaster_model->deleteData('interviews', array('id' => $id));
        redirect('admin/admin_interviews');
    }

    public function interview_add() {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Interview";


            $formdata = $this->input->post();
            if ($formdata) {



                $user = explode("-", $formdata['name']);

                //$formdata['profile_id']=$user[0];
                //$formdata['name']=$user[1];
                unset($formdata['submit']);
                $get_interview_id = $this->Adminmaster_model->insertData('interviews', $formdata);
                //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
                $video_info = array();
                $video_info = $_FILES;
                if ($get_interview_id) {
                    if ($video_info['file']['name'] != '') {
                        $video_info['file']['name'] = str_replace(" ", "_", $video_info['file']['name']);
                        $user_video = $this->Common_model->upload_org_filename('file', 'interview_video/' . $get_interview_id, $allowd = "mp4|mkv|mov", array('width' => 200, 'height' => 300));
                        if ($user_video != false) {

                            $get_update_id = $this->Adminmaster_model->updateData('interviews', array('filename' => $video_info['file']['name'], 'video' => base_url("/public/upload/interview_video/" . $get_interview_id)), array('id' => $get_interview_id));

                            // echo true;
                        }
                    }

                    if (isset($video_info['cover']) && $video_info['cover']['name'] != '') {
                        $video_info['cover']['name'] = str_replace(" ", "_", $video_info['cover']['name']);
                        $user_cover = $this->Common_model->upload_org_filename('cover', 'interview_video/' . $get_interview_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                        if ($user_cover != false) {

                            $get_updated_id = $this->Adminmaster_model->updateData('interviews', array('cover' => base_url("/public/upload/interview_video/" . $get_interview_id . "/thumbs/" . $video_info['cover']['name'])), array('id' => $get_interview_id));
                        }
                    }

                    ////////////////////////////////////Bvcohorts///////////////////////
                    // SERVER A - UPLOAD FILE VIA CURL POST
                    // (A) SETTINGS
                    $urls = array();
                    //array of cURL handles
                    //set the urls
                    //$urls[] = 'https://xebra.in/index/insert_interview';
                    //$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                    //$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                    $urls[] = 'https://onecohort.in/index/insert_interview';
                    //$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                    $file = DOC_ROOT . "/public/upload/interview_video/" . $get_interview_id . "/" . $_FILES['file']['name']; // File to upload
                    $upname = $_FILES['file']['name']; // File name to be uploaded as

                    $cover = DOC_ROOT . "/public/upload/interview_video/" . $get_interview_id . "/thumbs/" . $_FILES['cover']['name']; // File to upload
                    $covername = $_FILES['cover']['name']; // File name to be uploaded as
                    // (B) NEW CURL FILE
                    $cf = new CURLFile($file, mime_content_type($file), $upname);
                    $cfile = new CURLFile($cover, mime_content_type($cover), $covername);
                    $data = $formdata;

                    $data['upload'] = $cf;
                    $data['cover'] = $cfile;
                    // (C) CURL INIT
                    foreach ($urls as $key => $url) {
                        //print $url;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        // (D) CURL RUN
                        // (D1) GO!
                        $result = curl_exec($ch);

                        // (D2) CURL ERROR
                        if (curl_errno($ch)) {
                            //   echo "CURL ERROR - " . curl_error($ch);
                        }

                        // (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                        else {
                            // $info = curl_getinfo($ch);
                            // print_r($info);
                            // echo $result;
                        }

                        // (D4) DONE
                        curl_close($ch);
                    }

                    ////////////////////////////////////Bvcohorts///////////////////////
                    echo true;
                    exit;
                    //redirect('admin/admin_interviews');
                }
            }
            $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));

            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/add_interview', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function interview_edit($id) {
        $this->load->model('Uploadvideos_model');

        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Interview";


            $formdata = $this->input->post();
            if ($formdata) {



                $user = explode("-", $formdata['name']);

                $formdata['profile_id'] = $user[0];
                //$formdata['name'] = $user[1];
                unset($formdata['submit']);
                if (isset($formdata['file'])) {
                    unset($formdata['file']);
                }

                $get_interview_idd = $this->Adminmaster_model->updateData('interviews', $formdata, array('id' => $formdata['id']));
                $get_interview_id = $formdata['id'];
                //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
                $video_info = array();
                $video_info = $_FILES;
                if ($get_interview_idd) {
                    if (isset($video_info['file']['name']) && $video_info['file']['name'] != '') {
                        $video_info['file']['name'] = str_replace(" ", "_", $video_info['filr']['name']);
                        $user_video = $this->Common_model->upload_org_filename('file', 'interview_video/' . $get_interview_id, $allowd = "mp4|mkv|mov", array('width' => 200, 'height' => 300));
                        if ($user_video != false) {

                            $get_update_id = $this->Adminmaster_model->updateData('interviews', array('filename' => $video_info['file']['name'], 'video' => base_url("/public/upload/interview_video/" . $get_interview_id)), array('id' => $get_interview_id));

                            echo true;
                        }
                    }

                    if (isset($video_info['cover']) && $video_info['cover']['name'] != '') {
                        $video_info['cover']['name'] = str_replace(" ", "_", $video_info['cover']['name']);
                        $user_cover = $this->Common_model->upload_org_filename('cover', 'interview_video/' . $get_interview_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                        if ($user_cover != false) {

                            $get_updated_id = $this->Adminmaster_model->updateData('interviews', array('cover' => base_url("/public/upload/interview_video/" . $get_interview_id . "/thumbs/thumbs/" . $video_info['cover']['name'])), array('id' => $get_interview_id));
                        }
                    }
                    echo true;
                    exit;
                    // redirect('admin/admin_interviews');
                }
            }

            $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
            $data['interviews'] = $this->Adminmaster_model->selectData('interviews', "*", array('id' => $id));
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/edit_interview', $data);
            $this->load->view('partials/admin-footer');
        }
    }

    public function admin_blogs() {
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $reg_id = $this->session->userdata('userId');
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Uplaod Blog";
        }

        $post = $this->input->post();

        if ($post) {
            $post['reg_id'] = $reg_id;
            $get_blog_id = $this->Adminmaster_model->insertData('blogs', $post);
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $image_info = array();
            $image_info = $_FILES;
            if ($get_blog_id) {
                if ($image_info['featured_image']['name'] != '') {
                    $image_info['featured_image']['name'] = str_replace(" ", "_", $image_info['featured_image']['name']);
                    $user_image = $this->Common_model->upload_org_filename('featured_image', 'blog_image/' . $get_blog_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
                    if ($user_image != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('blogs', array('filename' => $image_info['featured_image']['name'], 'image' => base_url("/public/upload/blog_image/" . $get_blog_id)), array('id' => $get_blog_id));
                    }
                }
            }

            ////////////////////////////////////Bvcohorts///////////////////////
            // SERVER A - UPLOAD FILE VIA CURL POST
            // (A) SETTINGS
            $urls = array();
            //array of cURL handles
            //set the urls
            //$urls[] = 'https://xebra.in/index/insert_blog';
            //$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            //$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            $urls[] = 'https://onecohort.in/index/insert_blog';
            //$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
            $file = DOC_ROOT . "/public/upload/blog_image/" . $get_blog_id . "/" . $_FILES['featured_image']['name']; // File to upload
            $upname = $_FILES['featured_image']['name']; // File name to be uploaded as
            // (B) NEW CURL FILE
            $cf = new CURLFile($file, mime_content_type($file), $upname);

            $data = $post;

            $data['upload'] = $cf;

            // (C) CURL INIT
            foreach ($urls as $key => $url) {
                //print $url;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // (D) CURL RUN
                // (D1) GO!
                $result = curl_exec($ch);

                // (D2) CURL ERROR
                if (curl_errno($ch)) {
                    echo "CURL ERROR - " . curl_error($ch);
                }

                // (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                else {
                    // $info = curl_getinfo($ch);
                    // print_r($info);
                    echo $result;
                }

                // (D4) DONE
                curl_close($ch);
            }

            ////////////////////////////////////Bvcohorts///////////////////////
            $this->session->set_flashdata('msg', "Your blog has been added successfully");
            redirect('admin/admin_blogs_list');
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/add_blogs');
        $this->load->view('partials/admin-footer');
    }

    public function edit_admin_blog($id) {
        $this->load->model('Uploadvideos_model');
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $reg_id = $this->session->userdata('userId');
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Edit Blog";
        }

        $post = $this->input->post();
        //print_r($post); exit();

        if ($post) {
            $post['reg_id'] = $reg_id;
            $get_blog_id = $this->Adminmaster_model->updateData('blogs', $post, array('id' => $id));
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $image_info = array();
            $image_info = $_FILES;
            if ($get_blog_id) {
                if ($image_info['featured_image']['name'] != '') {
                    $image_info['featured_image']['name'] = str_replace(" ", "_", $image_info['featured_image']['name']);
                    $user_image = $this->Common_model->upload_org_filename('featured_image', 'blog_image/' . $id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
                    if ($user_image != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('blogs', array('filename' => $image_info['featured_image']['name'], 'image' => base_url("/public/upload/blog_image/" . $id)), array('id' => $id));
                    }
                }
            }
            $this->session->set_flashdata('msg', "Your blog has been updated successfully");
            redirect('admin/admin_blogs_list');
        }
        $data['blog'] = $this->Adminmaster_model->selectData('blogs', "*", array('id' => $id));
        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/edit_blogs', $data);
        $this->load->view('partials/admin-footer');
    }

    public function admin_blogs_list() {
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Blog";
        }
        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/blog_list');
        $this->load->view('partials/admin-footer');
    }

    public function get_post_deatils() {

        $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchBlogs($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchBlogs($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';
            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';
            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
            ';

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_admin_blog/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/delete_admin_blog/" . $value['id'] . "'>Delete</a></div>";

            $records["data"][] = array(
                date("d-m-Y", strtotime($value['created_at'])),
                $value['post_name'],
                $value['category'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function delete_admin_blog($id) {

        $get_podcast_id = $this->Adminmaster_model->deleteData('blogs', array('id' => $id));
        redirect('admin/admin_blogs_list');
    }

    public function admin_podcast() {
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Podcast";
        }

        $formdata = $this->input->post();
        if ($formdata) {



            $user = explode("-", $formdata['name']);

            $formdata['profile_id'] = $user[0];
            $formdata['name'] = $user[1];
            unset($formdata['submit']);
            $get_podcast_id = $this->Adminmaster_model->insertData('podcast', $formdata);
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $podcast_info = array();
            $podcast_info = $_FILES;

            if ($get_podcast_id) {
                if ($podcast_info['podcast']['name'] != '') {
                    $podcast_info['podcast']['name'] = str_replace(" ", "_", $podcast_info['podcast']['name']);
                    $user_podcast = $this->Common_model->upload_org_filename('podcast', 'podcast/' . $get_podcast_id, $allowd = "mp3|mp4", array('width' => 200, 'height' => 300));
                    if ($user_podcast != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('podcast', array('filename' => $podcast_info['podcast']['name'], 'podcast' => base_url("/public/upload/podcast/" . $get_podcast_id)), array('id' => $get_podcast_id));
                    }
                }


                if ($podcast_info['cover']['name'] != '') {
                    $podcast_info['cover']['name'] = str_replace(" ", "_", $podcast_info['cover']['name']);
                    $user_cover = $this->Common_model->upload_org_filename('cover', 'podcast/' . $get_podcast_id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                    if ($user_cover != false) {

                        //$get_updated_id = $this->Adminmaster_model->updateData('podcast',array('cover'=>base_url("/public/upload/podcast/".$get_podcast_id."/thumbs/thumbs/".$podcast_info['cover']['name'])),array('id'=>$get_podcast_id ));
                        $get_updated_id = $this->Adminmaster_model->updateData('podcast', array('cover' => base_url("/public/upload/podcast/" . $get_podcast_id . "/thumbs/" . $podcast_info['cover']['name'])), array('id' => $get_podcast_id));
                    }
                }

                echo true;
                exit;
                // redirect('admin/admin_interviews');
            }
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/add_podcast');
        $this->load->view('partials/admin-footer');
    }

    public function podcast_list() {
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Upload Podcast";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/podcast_list');
        $this->load->view('partials/admin-footer');
    }

    public function podcast_add() {
        
    }

    public function get_podcast_deatils() {
        $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchPodcast($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchPodcast($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple uservideo_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';
            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';
            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
            ';

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_admin_podcast/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/delete_admin_podcast/" . $value['id'] . "'>Delete</a></div>";

            $records["data"][] = array(
                $value['title'],
                $value['name'],
                $value['conduct'],
                $value['filename'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function delete_admin_podcast($id) {

        $get_podcast_id = $this->Adminmaster_model->deleteData('podcast', array('id' => $id));
        redirect('admin/podcast_list');
    }

    public function edit_admin_podcast($id = null) {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );

            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Podcast";
        }

        $formdata = $this->input->post();
        if ($formdata) {



            $user = explode("-", $formdata['name']);

            $formdata['profile_id'] = $user[0];
            $formdata['name'] = $user[1];
            unset($formdata['cover']);
            unset($formdata['podcast']);
            unset($formdata['submit']);
            $get_podcast_id = $this->Adminmaster_model->updateData('podcast', $formdata, array('id' => $id));
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $podcast_info = array();
            $podcast_info = $_FILES;


            if ($get_podcast_id) {
                if (!empty($podcast_info)) {

                    if (isset($podcast_info['podcast']) && $podcast_info['podcast']['name'] != '') {
                        $podcast_info['podcast']['name'] = str_replace(" ", "_", $podcast_info['podcast']['name']);

                        $user_podcast = $this->Common_model->upload_org_filename('podcast', 'podcast/' . $get_podcast_id, $allowd = "mp3|mp4", array('width' => 200, 'height' => 300));


                        if ($user_podcast != false) {

                            $get_update_id = $this->Adminmaster_model->updateData('podcast', array('filename' => $podcast_info['podcast']['name'], 'podcast' => base_url("/public/upload/podcast/" . $id)), array('id' => $id));
                        }
                    }


                    if (isset($podcast_info['cover']) && $podcast_info['cover']['name'] != '') {
                        $podcast_info['cover']['name'] = str_replace(" ", "_", $podcast_info['cover']['name']);
                        $user_cover = $this->Common_model->upload_org_filename('cover', 'podcast/' . $id . '/thumbs', $allowd = "jpg|jpeg|png", array('width' => 200, 'height' => 300));
                        if ($user_cover != false) {

                            //$get_updated_id = $this->Adminmaster_model->updateData('podcast',array('cover'=>base_url("/public/upload/podcast/".$id."/thumbs/thumbs/".$podcast_info['cover']['name'])),array('id'=>$id ));
                            $get_updated_id = $this->Adminmaster_model->updateData('podcast', array('cover' => base_url("/public/upload/podcast/" . $id . "/thumbs/" . $podcast_info['cover']['name'])), array('id' => $id));
                        }
                    }
                }

                echo true;
                exit;
                // redirect('admin/admin_interviews');
            }
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));

        $data['podcast'] = $this->Adminmaster_model->selectData('podcast', "*", array('id' => $id));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/edit_podcast');
        $this->load->view('partials/admin-footer');
    }

    public function admin_quiz_list() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Quiz";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/admin_quiz_list');
        $this->load->view('partials/admin-footer');
    }

    public function admin_addquiz() {


        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Quiz";
        }

        $post = $this->input->post();
        if ($post) {

            $post['quiz_start_date'] = date('Y-m-d', strtotime($post['sub_start_date']));
            $post['quiz_end_date'] = date('Y-m-d', strtotime($post['sub_end_date']));
            $post['question'] = json_encode($post['question']);
            $post['option1'] = json_encode($post['option1']);
            $post['option2'] = json_encode($post['option2']);
            $post['option3'] = json_encode($post['option3']);
            $post['option4'] = json_encode($post['option4']);
            $post['answer'] = json_encode($post['answer']);
            $post['status'] = "Active";

            unset($post['sub_start_date']);
            unset($post['sub_end_date']);

            // print_r($post);exit;
            $get_quiz_id = $this->Adminmaster_model->insertData('quizs', $post);
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $quiz_info = array();
            $quiz_info = $_FILES;

            if ($get_quiz_id) {
                if ($quiz_info['cover_image']['name'] != '') {
                    $quiz_info['cover_image']['name'] = str_replace(" ", "_", $quiz_info['cover_image']['name']);
                    $user_podcast = $this->Common_model->upload_org_filename('cover_image', 'quizs/' . $get_quiz_id, $allowd = "jpeg|png|jpg", array('width' => 200, 'height' => 300));
                    if ($user_podcast != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('quizs', array('image' => base_url("/public/upload/quizs/" . $get_quiz_id . "/" . $quiz_info['cover_image']['name'])), array('id' => $get_quiz_id));
                    }
                }
                redirect('admin/admin_quiz_list');
            }
        } else {
            $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/admin_addquiz');
            $this->load->view('partials/admin-footer');
        }
    }

    public function admin_editquiz($id) {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            $data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Quiz";
        }

        $post = $this->input->post();
        if ($post) {

            $post['quiz_start_date'] = date('Y-m-d', strtotime($post['sub_start_date']));
            $post['quiz_end_date'] = date('Y-m-d', strtotime($post['sub_end_date']));
            $post['question'] = json_encode($post['question']);
            $post['option1'] = json_encode($post['option1']);
            $post['option2'] = json_encode($post['option2']);
            $post['option3'] = json_encode($post['option3']);
            $post['option4'] = json_encode($post['option4']);
            $post['answer'] = json_encode($post['answer']);
            $post['status'] = "Active";

            unset($post['sub_start_date']);
            unset($post['sub_end_date']);

            // print_r($post);exit;
            $get_quiz_id = $this->Adminmaster_model->updateData('quizs', $post, array('id' => $id));
            //$get_video_id = $this->Adminmaster_model->insertData('user_videos',$insertData);
            $quiz_info = array();
            $quiz_info = $_FILES;

            if ($get_quiz_id) {
                if ($quiz_info['cover_image']['name'] != '') {
                    $quiz_info['cover_image']['name'] = str_replace(" ", "_", $quiz_info['cover_image']['name']);
                    $user_podcast = $this->Common_model->upload_org_filename('cover_image', 'quizs/' . $id, $allowd = "jpeg|png|jpg", array('width' => 200, 'height' => 300));
                    if ($user_podcast != false) {

                        $get_update_id = $this->Adminmaster_model->updateData('quizs', array('image' => base_url("/public/upload/quizs/" . $id . "/" . $quiz_info['cover_image']['name'])), array('id' => $id));
                    }
                }
                redirect('admin/admin_quiz_list');
            }
        } else {

            $data['quizs'] = $this->Adminmaster_model->selectData('quizs', "*", array('id' => $id));
            $this->load->view('partials/admin-header-dashboard', $data);
            $this->load->view('admin/admin_editquiz');
            $this->load->view('partials/admin-footer');
        }
    }

    public function delete_admin_quiz($id) {

        $this->load->model('Submitstories_model');
        $delete = $this->Adminmaster_model->deleteData('quizs', array('id' => $id));

        if ($delete) {
            $this->session->set_flashdata('msg', 'Deleted Successfully!');
        } else {
            $this->session->set_flashdata('msg', 'Cannot be Deleted!');
        }
        return redirect('admin/admin_quiz_list');
    }

    public function get_quiz_deatils() {
        $post = $this->input->post();
        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_model->fetchQuizs($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_model->fetchQuizs($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        //	$value = 1;

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple quiz_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';
            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';
            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
            ';

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/admin_editquiz/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/delete_admin_quiz/" . $value['id'] . "'>Delete</a></div>";

            $records["data"][] = array(
                date('d-m-Y', strtotime($value['quiz_start_date'])),
                $value['quiz_name'],
                date('d-m-Y', strtotime($value['quiz_start_date'])),
                date('d-m-Y', strtotime($value['quiz_end_date'])),
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function admin_challenge() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/challenge/admin_challenge_list');
        $this->load->view('partials/admin-footer');
    }

    public function get_challange_details() {
        $this->load->model('Admin_dashboard_model');
        $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_dashboard_model->fetchChallanges($post, $sort_field, $orderBy, 0);
        $userData = $this->Admin_dashboard_model->fetchChallanges($post, $sort_field, $orderBy, 1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();

        foreach ($userData as $key => $value) {

            $check = '<input type="checkbox" id="filled-in-box' . $value['id'] . '" name="filled-in-box' . $value['id'] . '" class="filled-in purple challange_bulk_action" value="' . $value['id'] . '"/><label for="filled-in-box' . $value['id'] . '"></label>';
            $action = '';
            $status = '';
            $cls = '';
            $cls1 = '';
            $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value['id'] . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value['id'] . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value['id'] . '>
            ';

            $action .= $status;

            $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
            //href='".base_url()."admin_dashboard/delete_challange/".$value['id']."' -- delete_challenge_btn 
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_challenge/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' href='" . base_url() . "admin/delete_challange/" . $value['id'] . "' class='action_link' data-chal_id=" . $value['id'] . ">Delete</a></div>";

            $records["data"][] = array(
                //  $check,
                $value['challange_code'] . "</br>" . date("d-m-Y", strtotime($value['challange_date'])),
                $value['challange_name'],
                wordwrap($value['challange_desc'], 50, "<br>"),
                $value['challange_nature'],
                date("d-m-Y", strtotime($value['last_day_entry'])),
                //date("d-m-Y",strtotime($value['end_day_entry'])) ,
                $value['location'],
                $action_links,
            );
        }
        $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function delete_challange($id) {
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $get_newsletter_id = $this->Adminmaster_model->deleteData('challanges', array('id' => $id));
        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));

        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/challenge/admin_challenge_list');
        $this->load->view('partials/admin-footer');
    }

    public function download_multiple_challenge() {
        $this->load->model('Admin_dashboard_model');

        $array = $this->input->get('ids');
        $array = explode(',', $array);
        $data['result'] = $this->Admin_dashboard_model->challange_data_download($array);

        $this->load->view('admin_dashboard/export_challenge', $data);
    }

    public function submit_challange() {

        $session = $this->session->userdata('admin_session');
        //pr($session,1);
        $this->load->model('Admin_dashboard_model');


        $post = $this->input->post();

        if ($post) {
            if (isset($session['admin_id'])) {
                // $post['reg_id']=$session['admin_id'];
            }

            $post['challange_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $post['challange_date'])));
            $post['last_day_entry'] = date("Y-m-d", strtotime(str_replace('/', '-', $post['last_day_entry'])));
            $post['end_day_entry'] = date("Y-m-d", strtotime(str_replace('/', '-', $post['end_day_entry'])));
            $get_challange_id = $this->Admin_dashboard_model->insertData('challanges', $post);

            $image_info = array();
            $image_info = $_FILES;
            if ($get_challange_id) {


                if ($image_info['challange_image']['name'] != '') {
                    $image_info['challange_image']['name'] = str_replace(" ", "_", $image_info['challange_image']['name']);
                    $user_image = $this->Common_model->upload_org_filename('challange_image', 'challange_image/' . $get_challange_id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
                    if ($user_image != false) {

                        $get_update_id = $this->Admin_dashboard_model->updateData('challanges', array('challange_image' => $image_info['challange_image']['name']), array('id' => $get_challange_id));
                    }
                }
                ////////////////////////////////////Bvcohorts///////////////////////
                // SERVER A - UPLOAD FILE VIA CURL POST
                // (A) SETTINGS
                $urls = array();
                //array of cURL handles
                //set the urls
                //$urls[] = 'https://xebra.in/index/insert_challange';
                //$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                //$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $urls[] = 'https://onecohort.in/index/insert_challange';
                //$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $file = DOC_ROOT . "/public/upload/challange_image/" . $get_challange_id . "/" . $_FILES['challange_image']['name']; // File to upload
                $upname = $_FILES['challange_image']['name']; // File name to be uploaded as
                // (B) NEW CURL FILE
                $cf = new CURLFile($file, mime_content_type($file), $upname);

                $data = $post;

                $data['upload'] = $cf;

                // (C) CURL INIT
                foreach ($urls as $key => $url) {
                    //print $url;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    // (D) CURL RUN
                    // (D1) GO!
                    $result = curl_exec($ch);

                    // (D2) CURL ERROR
                    if (curl_errno($ch)) {
                        echo "CURL ERROR - " . curl_error($ch);
                    }

                    // (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                    else {
                        // $info = curl_getinfo($ch);
                        // print_r($info);
                        echo $result;
                    }

                    // (D4) DONE
                    curl_close($ch);
                }

                ////////////////////////////////////Bvcohorts///////////////////////
            }
            redirect('admin/admin_challenge');
        }
    }

    public function add_challenge() {

        $this->load->model('Uploadvideos_model');
        $this->load->model('Admin_dashboard_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $array = $this->input->get('ids');
        $array = explode(',', $array ?? '');
        //$data['result'] = $this->Admin_dashboard_model->caDataDownload($array);
        $challanges = $this->Admin_dashboard_model->selectData('challanges', '*', '', 'id', 'DESC');

        if (count($challanges) > 0) {
            $last_cs_no = $challanges[0]->challange_code;
            $whatIWant = substr($last_cs_no, strpos($last_cs_no, "/") + 1);
            $iemNo = str_replace('/' . $whatIWant, "", $last_cs_no);
            $temp = str_replace("CHA", "", $iemNo);
            //if($inv_no[0]->document_type=='2'){
            $cus_ref = '1';
            $temp = intval($temp) + 1;
            //$temp = substr($last_inv_no, 3, -7);
            //$temp= str_replace('INV', '', $last_inv_no); 
            $cha_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
            //}
        } else {
            $cha_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
        }

        $data['cha_code'] = 'CHA' . $cha_ref;

        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/challenge/add_challenge', $data);
        $this->load->view('partials/admin-footer');
    }

    public function edit_challenge($id) {
        $this->load->model('Uploadvideos_model');
        $this->load->model('Admin_dashboard_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $post = $this->input->post();

        if ($post) {
            if (isset($session['admin_id'])) {
                //$post['reg_id']=$session['admin_id'];
            }

            $post['challange_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $post['challange_date'])));
            $post['last_day_entry'] = date("Y-m-d", strtotime(str_replace('/', '-', $post['last_day_entry'])));
            $get_challange_id = $this->Admin_dashboard_model->updateData('challanges', $post, array("id" => $id));


            $image_info = array();
            $image_info = $_FILES;
            if ($get_challange_id) {
                if ($image_info['challange_image']['name'] != '') {
                    $image_info['challange_image']['name'] = str_replace(" ", "_", $image_info['challange_image']['name']);
                    $user_image = $this->Common_model->upload_org_filename('challange_image', 'challange_image/' . $id, $allowd = "jpeg|jpg|png", array('width' => 200, 'height' => 300));
                    if ($user_image != false) {

                        $get_update_id = $this->Admin_dashboard_model->updateData('challanges', array('challange_image' => $image_info['challange_image']['name']), array('id' => $id));
                    }
                }
            }
            redirect('admin/admin_challenge');
        }
        $data['challanges'] = $this->Admin_dashboard_model->selectData("challanges", "*", array('id' => $id));
        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/challenge/edit_challenge', $data);
        $this->load->view('partials/admin-footer');
    }

    public function admin_incubator() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/incubator/incubator_list');
        $this->load->view('partials/admin-footer');
    }

    public function add_incubator() {

        $post = $this->input->post();
        if ($post) {

            if (isset($post['incubator_name'])) {

                $post['name'] = $post['incubator_name'];
                unset($post['incubator_name']);
            }

            if (isset($post['incubator_address'])) {

                $post['address'] = $post['incubator_address'];
                unset($post['incubator_address']);
            }

            if (isset($post['incubator_country'])) {

                $post['country'] = $post['incubator_country'];
                unset($post['incubator_country']);
            }

            if (isset($post['incubator_state'])) {

                $post['state'] = $post['incubator_state'];
                unset($post['incubator_state']);
            }

            if (isset($post['incubator_city'])) {

                $post['city'] = $post['incubator_city'];
                unset($post['incubator_city']);
            }

            if (isset($post['incubator_emailid'])) {

                $post['email_id'] = $post['incubator_emailid'];
                unset($post['incubator_emailid']);
            }

            if (isset($post['incubator_contact'])) {

                $post['contact_no'] = $post['incubator_contact'];
                unset($post['incubator_contact']);
            }

            if (isset($post['submit'])) {


                unset($post['submit']);
            }
            unset($post['image_info']);
            if (isset($_FILES['incubator_image']) && $_FILES['incubator_image']['name'] != "") {

                if ($_FILES['incubator_image']['name'] != NULL) {
                    $post['image'] = $_FILES['incubator_image']['name'];
                }
            }

            $insert_incubator = $this->Admin_dashboard_model->insertData("incubation", $post);


            if ($insert_incubator != false) {

                $image_info = array();
                $image_info = $_FILES;

                if (isset($image_info['incubator_image']) && $image_info['incubator_image']['name'] != "") {

                    if ($_FILES['incubator_image']['name'] != NULL) {

                        $files = $_FILES['incubator_image'];

                        $_FILES['incubator_image']['name'] = $files['name'];
                        $_FILES['incubator_image']['type'] = $files['type'];
                        $_FILES['incubator_image']['tmp_name'] = $files['tmp_name'];
                        $_FILES['incubator_image']['error'] = $files['error'];
                        $_FILES['incubator_image']['size'] = $files['size'];

                        $_FILES['incubator_image']['name'] = str_replace(" ", "_", $_FILES['incubator_image']['name']);

                        $docs = $this->Common_model->upload_org_filename('incubator_image', 'incubator_image/' . $insert_incubator, $allowd = "jpg|jpeg|png", array('width' => 100, 'height' => 100));
                    }
                }



                ////////////////////////////////////Bvcohorts///////////////////////
                // SERVER A - UPLOAD FILE VIA CURL POST
// (A) SETTINGS
                $urls = array();
//array of cURL handles
//set the urls
                //$urls[] = 'https://xebra.in/index/insert_incubators';
//$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
//$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $urls[] = 'https://onecohort.in/index/insert_incubators';
//$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $file = DOC_ROOT . "/public/upload/incubator_image/" . $insert_incubator . "/" . $_FILES['incubator_image']['name']; // File to upload
                $upname = $_FILES['incubator_image']['name']; // File name to be uploaded as
// (B) NEW CURL FILE
                $cf = new CURLFile($file, mime_content_type($file), $upname);
                $data = $post;

                $data['upload'] = $cf;
// (C) CURL INIT
                foreach ($urls as $key => $url) {
                    //print $url;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// (D) CURL RUN
// (D1) GO!
                    $result = curl_exec($ch);

// (D2) CURL ERROR
                    if (curl_errno($ch)) {
                        echo "CURL ERROR - " . curl_error($ch);
                    }

// (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                    else {
                        // $info = curl_getinfo($ch);
                        // print_r($info);
                        echo $result;
                    }

// (D4) DONE
                    curl_close($ch);
                }




                $this->session->set_flashdata('success', "Your Data has been added successfully");

                redirect('admin/admin_incubator');
            }
        }
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }


        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $data['countries'] = $this->Adminmaster_model->selectData('countries', '*', '', 'country_id', 'ASC');
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/incubator/add_incubator', $data);
        $this->load->view('partials/admin-footer');
    }

    public function edit_incubator($id) {

        $post = $this->input->post();
        if ($post) {

            if (isset($post['incubator_name'])) {

                $post['name'] = $post['incubator_name'];
                unset($post['incubator_name']);
            }

            if (isset($post['incubator_address'])) {

                $post['address'] = $post['incubator_address'];
                unset($post['incubator_address']);
            }

            if (isset($post['incubator_country'])) {

                $post['country'] = $post['incubator_country'];
                unset($post['incubator_country']);
            }

            if (isset($post['incubator_state'])) {

                $post['state'] = $post['incubator_state'];
                unset($post['incubator_state']);
            }

            if (isset($post['incubator_city'])) {

                $post['city'] = $post['incubator_city'];
                unset($post['incubator_city']);
            }

            if (isset($post['incubator_emailid'])) {

                $post['email_id'] = $post['incubator_emailid'];
                unset($post['incubator_emailid']);
            }

            if (isset($post['incubator_contact'])) {

                $post['contact_no'] = $post['incubator_contact'];
                unset($post['incubator_contact']);
            }

            if (isset($post['submit'])) {


                unset($post['submit']);
            }
            unset($post['image_info']);
            if (isset($_FILES['incubator_image']) && $_FILES['incubator_image']['name'] != "") {

                if ($_FILES['incubator_image']['name'] != NULL) {
                    $post['image'] = $_FILES['incubator_image']['name'];
                }
            }

            $insert_incubator = $this->Admin_dashboard_model->updateData("incubation", $post, array('id' => $id));


            if ($insert_incubator != false) {

                $image_info = array();
                $image_info = $_FILES;

                if (isset($image_info['incubator_image']) && $image_info['incubator_image']['name'] != "") {

                    if ($_FILES['incubator_image']['name'] != NULL) {

                        $files = $_FILES['incubator_image'];

                        $_FILES['incubator_image']['name'] = $files['name'];
                        $_FILES['incubator_image']['type'] = $files['type'];
                        $_FILES['incubator_image']['tmp_name'] = $files['tmp_name'];
                        $_FILES['incubator_image']['error'] = $files['error'];
                        $_FILES['incubator_image']['size'] = $files['size'];

                        $_FILES['incubator_image']['name'] = str_replace(" ", "_", $_FILES['incubator_image']['name']);

                        $docs = $this->Common_model->upload_org_filename('incubator_image', 'incubator_image/' . $id, $allowd = "jpg|jpeg|png", array('width' => 100, 'height' => 100));
                    }
                }


                $this->session->set_flashdata('success', "Your Data has been edited successfully");

                redirect('admin/admin_incubator');
            }
        }
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $data['countries'] = $this->Adminmaster_model->selectData('countries', '*', '', 'country_id', 'ASC');
        $data['incubation'] = $this->Admin_dashboard_model->selectData("incubation", "*", array('id' => $id));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/incubator/edit_incubator', $data);
        $this->load->view('partials/admin-footer');
    }

    public function delete_incubation() {

        $id = $this->input->post('incube_id');


        $filter = array(
            'status' => $this->input->post('status'),
        );

        $flag = $this->Adminmaster_model->deleteData('incubation', array('id' => $id));
        //print $this->db->last_query(); exit();
        if ($flag != false) {

            echo true;
        } else {
            echo false;
        }
    }

    public function get_incubator_details() {
       $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_dashboard_model->getIncubation($post,0);
        $incube = $this->Admin_dashboard_model->getIncubation($post,1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
       // $incube = $this->Admin_dashboard_model->getIncubation($post);
        foreach ($incube as $key => $value) {
            if ($value['image'] != '') {
                $image = '<img src="' . base_url() . 'public/upload/incubator_image/' . $value['id'] . '/' . $value['image'] . '" height="130px" class="logo_style_2" width="130px" style="object-fit:contain;">';
            } else {
                $image = '<p style="text-align:center;">No Image</p>';
            }


            $country = $this->Admin_dashboard_model->selectData('countries', "*", array('country_id' => $value['country']));

            $cities = $this->Admin_dashboard_model->selectData('cities', "*", array('city_id' => $value['city']));

            if (count($country) > 0) {
                $ctry = $country[0]->country_name;
            } else {
                $ctry = "";
            }


            if (count($cities) > 0) {
                $city = $cities[0]->name;
            } else {
                $city = "";
            }


            $middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#bc343a !important;"><strong>' . $value['name'] . '</strong></h5>
						</div>
						</div>
						<div class="row">
						<div class="col-lg-12">
						<div class="col-lg-6" style="margin-left:-12px; float:left;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: ' . wordwrap($value['address'], 60, "<br>\n") . ' </strong></p>
						</div>
						<div class="col-lg-3" style="margin-left: -10px; float:right;">
							<p class="offer_p"><!--i class="fas fa-globe"></i--> <strong>COUNTRY: ' . $ctry . ' </strong></p>
						</div>
						<div class="col-lg-3" style="margin-left: -10px; float:right;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: ' . $city . ' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist' . $value["id"] . '" style="margin:20px 0 0 0;">
							<div class="col-lg-12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col-lg-6" style="margin-left:-12px; float:left;">
									<p class="offer_p eve-email"><a style="color:#000;" href="javascript:void(0);" onclick="email_event(01);"><!--i class="fas fa-envelope"></i--> <strong>EMAIL: ' . $value['email_id'] . ' </strong></a></p>
								</div>
								<div class="col-lg-6" style="margin-left:-1.6%; float:left;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: ' . $value['contact_no'] . ' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;" hidden>
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist' . $value["id"] . '" class="more showmore active" title="Show More" onclick="showmorelist(' . $value["id"] . ')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less' . $value["id"] . '" class="less showmore" title="Show More" onclick="showmorelist(' . $value["id"] . ')" hidden>SHOW LESS</a>
							</div>
						</div>';
            $vid = $value['id'];
            $action = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown$vid' class='dropdown-content'>
				<li><a href=" . base_url() . "admin_dashboard/edit_incubator/" . $value['id'] . '><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_incub" data-cd_id="' . $value['id'] . '"><i class="material-icons">delete</i>DELETE</a></li>';

            $action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';
            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_incubator/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' href='javascript:void(0);' class='action_link deactive_incub' data-cd_id=" . $value['id'] . ">Delete</a></div>";

            $records["data"][] = array(
                $image,
                $middle_part,
                $action_links,
            );
        }
                $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function admin_office() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/office/office_list');
        $this->load->view('partials/admin-footer');
    }

    public function add_office() {


        $post = $this->input->post();
        if ($post) {

            if (isset($post['co_name'])) {

                $post['name'] = $post['co_name'];
                unset($post['co_name']);
            }

            if (isset($post['co_address'])) {

                $post['address'] = $post['co_address'];
                unset($post['co_address']);
            }

            if (isset($post['co_country'])) {

                $post['country'] = $post['co_country'];
                unset($post['co_country']);
            }

            if (isset($post['co_state'])) {

                $post['state'] = $post['co_state'];
                unset($post['co_state']);
            }

            if (isset($post['co_city'])) {

                $post['city'] = $post['co_city'];
                unset($post['co_city']);
            }

            if (isset($post['co_emailid'])) {

                $post['email_id'] = $post['co_emailid'];
                unset($post['co_emailid']);
            }

            if (isset($post['co_contact'])) {

                $post['contact_no'] = $post['co_contact'];
                unset($post['co_contact']);
            }

            if (isset($post['submit'])) {


                unset($post['submit']);
            }
            unset($post['image_info']);
            if (isset($_FILES['co_working_image']) && $_FILES['co_working_image']['name'] != "") {

                if ($_FILES['co_working_image']['name'] != NULL) {
                    $post['image'] = $_FILES['co_working_image']['name'];
                }
            }

            $insert_working_space = $this->Admin_dashboard_model->insertData("coworking_space", $post);


            if ($insert_working_space != false) {

                $image_info = array();
                $image_info = $_FILES;

                if (isset($image_info['co_working_image']) && $image_info['co_working_image']['name'] != "") {

                    if ($_FILES['co_working_image']['name'] != NULL) {

                        $files = $_FILES['co_working_image'];

                        $_FILES['co_working_image']['name'] = $files['name'];
                        $_FILES['co_working_image']['type'] = $files['type'];
                        $_FILES['co_working_image']['tmp_name'] = $files['tmp_name'];
                        $_FILES['co_working_image']['error'] = $files['error'];
                        $_FILES['co_working_image']['size'] = $files['size'];

                        $_FILES['co_working_image']['name'] = str_replace(" ", "_", $_FILES['co_working_image']['name']);

                        $docs = $this->Common_model->upload_org_filename('co_working_image', 'co_working_image/' . $insert_working_space, $allowd = "jpg|jpeg|png", array('width' => 100, 'height' => 100));
                    }
                }


                ////////////////////////////////////Bvcohorts///////////////////////
                ////////////////////////////////////Bvcohorts///////////////////////
                // SERVER A - UPLOAD FILE VIA CURL POST
// (A) SETTINGS
                $urls = array();
//array of cURL handles
//set the urls
                //$urls[] = 'https://xebra.in/index/insert_coworking';
//$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
//$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $urls[] = 'https://onecohort.in/index/insert_coworking';
//$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
                $file = DOC_ROOT . "/public/upload/co_working_image/" . $insert_working_space . "/" . $_FILES['co_working_image']['name']; // File to upload
                $upname = $_FILES['co_working_image']['name']; // File name to be uploaded as
// (B) NEW CURL FILE
                $cf = new CURLFile($file, mime_content_type($file), $upname);
                $data = $post;

                $data['upload'] = $cf;
// (C) CURL INIT
                foreach ($urls as $key => $url) {
                    //print $url;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// (D) CURL RUN
// (D1) GO!
                    $result = curl_exec($ch);

// (D2) CURL ERROR
                    if (curl_errno($ch)) {
                        echo "CURL ERROR - " . curl_error($ch);
                    }

// (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
                    else {
                        // $info = curl_getinfo($ch);
                        // print_r($info);
                        echo $result;
                    }

// (D4) DONE
                    curl_close($ch);
                }





                $this->session->set_flashdata('success', "Your Data has been added successfully");

                redirect('admin/admin_office');
            }
        }
        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $data['countries'] = $this->Adminmaster_model->selectData('countries', '*', '', 'country_id', 'ASC');
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/office/add_office', $data);
        $this->load->view('partials/admin-footer');
    }

    public function edit_office($id) {


        $post = $this->input->post();
        if ($post) {

            if (isset($post['co_name'])) {

                $post['name'] = $post['co_name'];
                unset($post['co_name']);
            }

            if (isset($post['co_address'])) {

                $post['address'] = $post['co_address'];
                unset($post['co_address']);
            }

            if (isset($post['co_country'])) {

                $post['country'] = $post['co_country'];
                unset($post['co_country']);
            }

            if (isset($post['co_state'])) {

                $post['state'] = $post['co_state'];
                unset($post['co_state']);
            }

            if (isset($post['co_city'])) {

                $post['city'] = $post['co_city'];
                unset($post['co_city']);
            }

            if (isset($post['co_emailid'])) {

                $post['email_id'] = $post['co_emailid'];
                unset($post['co_emailid']);
            }

            if (isset($post['co_contact'])) {

                $post['contact_no'] = $post['co_contact'];
                unset($post['co_contact']);
            }

            if (isset($post['submit'])) {


                unset($post['submit']);
            }
            unset($post['image_info']);
            if (isset($_FILES['co_working_image']) && $_FILES['co_working_image']['name'] != "") {

                if ($_FILES['co_working_image']['name'] != NULL) {
                    $post['image'] = $_FILES['co_working_image']['name'];
                }
            }

            $insert_working_space = $this->Admin_dashboard_model->updateData("coworking_space", $post, array('id' => $id));


            if ($insert_working_space != false) {

                $image_info = array();
                $image_info = $_FILES;

                if (isset($image_info['co_working_image']) && $image_info['co_working_image']['name'] != "") {

                    if ($_FILES['co_working_image']['name'] != NULL) {

                        $files = $_FILES['co_working_image'];

                        $_FILES['co_working_image']['name'] = $files['name'];
                        $_FILES['co_working_image']['type'] = $files['type'];
                        $_FILES['co_working_image']['tmp_name'] = $files['tmp_name'];
                        $_FILES['co_working_image']['error'] = $files['error'];
                        $_FILES['co_working_image']['size'] = $files['size'];

                        $_FILES['co_working_image']['name'] = str_replace(" ", "_", $_FILES['co_working_image']['name']);

                        $docs = $this->Common_model->upload_org_filename('co_working_image', 'co_working_image/' . $id, $allowd = "jpg|jpeg|png", array('width' => 100, 'height' => 100));
                    }
                }


                $this->session->set_flashdata('success', "Your Data has been edited successfully");
                redirect('admin/admin_office');
            }
        }

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Incubator";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $data['countries'] = $this->Adminmaster_model->selectData('countries', '*', '', 'country_id', 'ASC');
        $data['working_space'] = $this->Adminmaster_model->selectData("coworking_space", "*", array('id' => $id));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/office/edit_office', $data);
        $this->load->view('partials/admin-footer');
    }

    public function delete_workspace() {

        $id = $this->input->post('workspace_id');


        $filter = array(
            'status' => $this->input->post('status'),
        );

        $flag = $this->Adminmaster_model->deleteData('coworking_space', array('id' => $id));
        //print $this->db->last_query(); exit();
        if ($flag != false) {

            echo true;
        } else {
            echo false;
        }
    }

    public function get_coworking_details() {
       $post = $this->input->post();


        $field_pos = array("id" => '0');

        $sort_field = array_search($post['order'][0]['column'], $field_pos);

        if ($post['order'][0]['dir'] == 'asc') {
            $orderBy = "ASC";
        } else {
            $orderBy = "DESC";
        }

        $TotalRecord = $this->Admin_dashboard_model->getWorkspace($post,0);
        $working_space = $this->Admin_dashboard_model->getWorkspace($post,1);

        $iTotalRecords = $TotalRecord['NumRecords'];

        $records = array();
        $records["data"] = array();
        //$working_space = $this->Admin_dashboard_model->getWorkspace($post);
        foreach ($working_space as $key => $value) {
            if ($value['image'] != '') {
                $image = '<img src="' . base_url() . 'public/upload/co_working_image/' . $value['id'] . '/' . $value['image'] . '" height="130px" class="logo_style_2" width="130px" style="object-fit:contain;">';
            } else {
                $image = '<p style="text-align:center;">No Image</p>';
            }


            $country = $this->Admin_dashboard_model->selectData('countries', "*", array('country_id' => $value['country']));

            $cities = $this->Admin_dashboard_model->selectData('cities', "*", array('city_id' => $value['city']));

            if (count($country) > 0) {
                $ctry = $country[0]->country_name;
            } else {
                $ctry = "";
            }


            if (count($cities) > 0) {
                $city = $cities[0]->name;
            } else {
                $city = "";
            }


            $middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#bc343a !important;"><strong>' . $value['name'] . '</strong></h5>
						</div>
						</div>
						<div class="row">
						<div class="col-lg-12">
						<div class="col-lg-6" style="margin-left:-12px; float:left;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: ' . wordwrap($value['address'], 60, "<br>\n") . ' </strong></p>
						</div>
						<div class="col-lg-3" style="margin-left: -10px; float:right;">
							<p class="offer_p"><!--i class="fas fa-globe"></i--> <strong>COUNTRY: ' . $ctry . ' </strong></p>
						</div>
						<div class="col-lg-3" style="margin-left: -10px; float:right;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: ' . $city . ' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist' . $value["id"] . '" style="margin:20px 0 0 0;">
							<div class="col-lg-12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col-lg-6" style="margin-left:-12px; float:left;">
									<p class="offer_p eve-email"><a style="color:#000;" href="javascript:void(0);" onclick="email_event(01);"><!--i class="fas fa-envelope"></i--> <strong>EMAIL: ' . $value['email_id'] . ' </strong></a></p>
								</div>
								<div class="col-lg-6" style="margin-left:-1.6%; float:left;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: ' . $value['contact_no'] . ' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;" hidden>
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist' . $value["id"] . '" class="more showmore active" title="Show More" onclick="showmorelist(' . $value["id"] . ')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less' . $value["id"] . '" class="less showmore" title="Show More" onclick="showmorelist(' . $value["id"] . ')" hidden>SHOW LESS</a>
							</div>
						</div>';
            $vid = $value['id'];
            $action = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown$vid' class='dropdown-content'>
				<li><a href=" . base_url() . "admin_dashboard/edit_office/" . $value['id'] . '><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_space" data-cd_id="' . $value['id'] . '"><i class="material-icons">delete</i>DELETE</a></li>';

            $action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';

            $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_office/" . $value['id'] . "'>Edit</a></br><a style='font-size:12px;' href='javascript:void(0);' class='action_link deactive_space' data-cd_id=" . $value['id'] . ">Delete</a></div>";

            $records["data"][] = array(
                $image,
                $middle_part,
                $action_links,
            );
        }
      $records["draw"] = intval($post['draw']);
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    public function news() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/news/admin_news_list');
        $this->load->view('partials/admin-footer');
    }

    public function add_news() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/news/add_news');
        $this->load->view('partials/admin-footer');
    }

    public function edit_news() {

        $this->load->model('Uploadvideos_model');
        $data = array();
        if ($this->isUserLoggedIn) {
            $con = array(
                'id' => $this->session->userdata('userId')
            );
            $data['user'] = $this->Admin_model->getRows($con);
            //$data['getLanguage'] = $this->Uploadvideos_model->getLanguage();
            $data['title'] = "Challenges";
        }

        $data['registeredUser'] = $this->Adminmaster_model->selectData('registration', "*", array('status' => "Active"));
        $this->load->view('partials/admin-header-dashboard', $data);
        $this->load->view('admin/news/edit_news');
        $this->load->view('partials/admin-footer');
    }

    public function get_news_details() {
        //$this->load->model('Admin_dashboard_model'); 
        //$post = $this->input->post();

        /* $field_pos=array("id"=>'0');  

          $sort_field=array_search($post['order'][0]['column'],$field_pos);

          if($post['order'][0]['dir']=='asc'){
          $orderBy="ASC";
          }else{
          $orderBy="DESC";
          } */

        //$TotalRecord=$this->Admin_dashboard_model->fetchChallanges($post,$sort_field,$orderBy,0);   
        //$userData = $this->Admin_dashboard_model->fetchChallanges($post,$sort_field,$orderBy,1);
        //$iTotalRecords = $TotalRecord['NumRecords'];
        //$records = array();
        //$records["data"] = array();
        //foreach ($userData as $key => $value) {
        $value = "1";
        $check = '<input type="checkbox" id="filled-in-box' . $value . '" name="filled-in-box' . $value . '" class="filled-in purple news_bulk_action" value="' . $value . '"/><label for="filled-in-box' . $value . '"></label>';
        $action = '';
        $status = '';
        $cls = '';
        $cls1 = '';
        $action = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background " . $cls . "' href='#' data-activates='dropdown" . $value . "'><i class='action-dot'></i></div>

            <ul id='dropdown" . $value . "' class='dropdown-content'>
                <a href=" . base_url() . "services/edit-service/" . $value . '>
            ';

        $action .= $status;

        $action .= '</ul>

              <div class="status-action ' . $cls1 . '">

                <i class="action-status-icon"></i>

              </div>';
        //href='".base_url()."admin_dashboard/delete_challange/".$value['id']."' -- delete_challenge_btn
        $action_links = "<div style='padding: 5px 10%;'><a style='font-size:12px;' class='action_link' href='" . base_url() . "admin/edit_news/" . $value . "'>Edit</a></br><a style='font-size:12px;' href='" . base_url() . "admin/delete_news/" . $value . "' class='action_link' data-news_id=" . $value . ">Delete</a></div>";

        $records["data"][] = array(
            "",
            "",
            "",
            "",
            "",
            "",
            $action_links,
        );

        //}
        //$records["draw"] = intval($post['draw']);
        //$records["recordsTotal"] = $iTotalRecords;
        //$records["recordsFiltered"] = $iTotalRecords;       
        echo json_encode($records);
        exit;
    }

    public function ck_upload() {

        $this->load->view('admin/ck_upload');
    }

}
