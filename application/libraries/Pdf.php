<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter PDF Library
 *
 * @package			CodeIgniter
 * @subpackage		Libraries
 * @category		Libraries
 * @author			Muhanz
 * @license			MIT License
 * @link			https://github.com/hanzzame/ci3-pdf-generator-library
 *
 */

require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

class Pdf 
{
	public function create($html,$filename,$type)
    {
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    $dompdf->setPaper('A4');
	    
	    $dompdf->render();
	    if($type=='email'){
	    $output = $dompdf->output();	
	    } else if($type=='download_multi') {
	    //$dompdf->stream($filename.'.pdf');
	    	$output = $dompdf->output();

	    	file_put_contents(DOC_ROOT.'/public/pdfmail/'.$filename.'.pdf',$output);

		}else{
			$output =$dompdf->stream($filename.'.pdf');
		}
	    //$output = $dompdf->output();
	    
	    return $output;
  }
}