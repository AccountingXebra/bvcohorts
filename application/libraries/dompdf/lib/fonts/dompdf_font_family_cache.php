<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'material icons' => array(
    'normal' => $fontDir . '\783da70f2601f84d40b050c13f601e1a',
  ),
  'fira sans' => array(
    'normal' => $fontDir . '\8e56f1963ada0fde4fdd937f04f51708',
  ),
  'roboto' => array(
    'normal' => $fontDir . '\720228ebe98581cf3dcb9c52d14cb500',
    'italic' => $fontDir . '\57b4f9f22b6b458397f660428af4302d',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\43ed29441a44386a5b3b93c46e9b6448',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '\4cdc02245bf2a72f269c298e3d17973c',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '\bc3a9e190c270f6e1000bd646e999401',
  ),
) ?>