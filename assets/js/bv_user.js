$(document).ready(function(){
	console.log('BV USER JS');	
	$(".upvidclose").click(function(){
		$("#bvuploaddetails").hide();
		$('.modal-backdrop').css('display', 'none');
		location.reload();
	});
	
	/* Mouse Out function for category modal */
	$("#catdropdown").mouseleave(function() {
		$("#catdropdown").hide();
		$('.modal-backdrop').css('display', 'none');
		//location.reload();
		$('html, body').removeClass('modal-open'); 
	});
	
	$(".awardsmodal").click(function() {
		$('#award_frm').find("input[type=text], textarea").val("");
	});
	
	/* Adding Tag Box for upload video */
	$(function(){ 
		$("#select_tags input").on({
				focusout : function() {
				var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
				if(txt) $("<span/>", {text:txt.toLowerCase(), insertBefore:this});
				this.value = "";
			},
			keyup : function(ev) {
				// if: comma|enter (delimit more keyCodes with | pipe)
				if(/(188|13)/.test(ev.which)) $(this).focusout(); 
			}
		});
		$('#select_tags').on('click', 'span', function() {
			if(confirm("Remove "+ $(this).text() +"?")) $(this).remove(); 
		});
	});
	
	$(document).on("change",".file-video",function(){
		var file_id=$(this).data("val");
		var filesize = (this.files[0].size);
		var filesize_mb =  filesize / 1048576;
		var filename = (this.files[0].name);
		if(filename !=""){
			var file = this.files[0];
			var fileType = file["type"];
			var validImageTypes = ["video/mp4", "video/mov", "video/wmv", "video/flv", "video/avi"];
			if ($.inArray(fileType, validImageTypes) < 0) {
				$("#toast").toast({
					type: 'error_1',
					message: 'Video Format is incorrect.'
				});
			}else if(filesize_mb > 100) {
				$("#toast").toast({
					type: 'error_1',
					message: 'Upload video size should not be more than 100mb'
				});
				$("#upload_first")[0].reset();
			}else{	
				$("#upload_second")[0].reset();
				$('#bvuploaddetails').show();
				$('#bvuploadvideo').hide();
			}	
		}
		
	});
	
	$(".side-menu li a").click(function() {
		$(this).parent().addClass('active').siblings().removeClass('active');
	});
	
	$("#mailing_form").validate({
		rules:{
			email:{
				required:true,
			},
		},
		messages:{
			email:{
				required:"Email is required",
			},
		},
		 errorPlacement: function(error, element) {
          if(element.prop('tagName')  == 'SELECT') {
			error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
          }else{
			error.insertAfter(element);
          }
        } ,
		submitHandler: function(form) {
			form.submit();
		},
	});
	
	$("#personal_profile").validate({
		rules:{
			user_name:{
				required:true,
			},
		},
		messages:{
			user_name:{
				required:"Name is required",
			},
		},
		 errorPlacement: function(error, element) {
         
          if(element.prop('tagName')  == 'SELECT') {
          
          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
          }else{
           error.insertAfter(element);
          }
        } ,
		submitHandler: function(form) {
		form.submit();
		},
	});

    
	$("#user_country").on("change",function(){
		var country_id = $(this).val();
		if(country_id!=''){
			

			$.ajax({
			url:base_url+'home/get_states',
			type:"POST",
			data:{'country_id':country_id},
			success:function(res){
				$("#user_state").html(res);
				$("#user_state").parents('.input-field').addClass('label-active');
				//$('#sstate').material_select();
			},
			});
        }else{
			// Materialize.toast('Please select the country ', 7000,'red rounded');
        }
	});

	$("#user_state").on("change",function(){
		var state_id = $(this).val();
        //$('#select2-shipping_state-container').html($('#select2-shipping_state-container').html().replace(/(\*)/g, '<span style="color: transparent;">$1</span>'));
        if(state_id!=''){
			
			$.ajax({
				url:base_url+'home/get_cities',
				type:"POST",
				data:{'state_id':state_id},
				success:function(res){
					$("#user_city").html(res);
					$("#user_city").parents('.input-field').addClass('label-active');
					//$('#city').material_select();
					//$('#shipcitysec').addClass('bodrpinkright');
				},
			});
		}else{
			//Materialize.toast('Please select the state', 7000,'red rounded');
        }
	});



	


/* Publish video */
	$('#publish').on('click',function(){
					
                    var title=$('#video_title').val();
                    var desc=$('#video_desc').val();
                    var category=$('#select_category').val();
                    var category1=$('#select_category2').val();
                    var subcategory=$('#categorySelect').val();
                    var subcategory1=$('#categorySelect2').val();
                    var tags=$('#select_tags').val();
                     var url=$('#video_url').val();
					var formData = new FormData();
					formData.append('title', title);
					formData.append('desc', desc);
					formData.append('url', url);
					formData.append('tags', tags);
					formData.append('category',category);
					formData.append('category1',category1);
					formData.append('subcategory',subcategory);
					formData.append('subcategory1',subcategory1);
					
					
formData.append('file', $('#video_up')[0].files[0]);
formData.append('thumbnail', $('#thumbnail')[0].files[0]);

$(".loader").show();
  $.ajax({
   url:base_url+'home/upload_video',
   method:"POST",
   data:formData,
   contentType:false,
   cache:false,
   processData:false,
   success:function(data){
    $('#video_up').val('');
    if(data==true){
		//alert("video upload successfully");
		$("#bvuploaddetails").hide();
		$('.close').click();
		$('.modal-backdrop').css('display', 'none');
		window.setTimeout(function(){
			$(".loader").hide();
			$("#toast").toast({
				type: 'success',
				message: 'Your video has been successfully uploaded'
			});
		window.setTimeout(function(){
			location.reload();
		}, 200);
		}, 300);
    }else{
    	//alert("Something went wrong.Try again...");
		$("#toast").toast({
			type: 'error',
			message: 'Something went wrong.Try again...'
		});
    }
    
   }
  })
	});    
});	

	document.addEventListener("DOMContentLoaded", function() {
	var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

	if ("IntersectionObserver" in window) {
		let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
			entries.forEach(function(entry) {
				if (entry.isIntersecting) {
					entry.target.classList.add("visible");
					lazyBackgroundObserver.unobserve(entry.target);
				}
			});
		});

		lazyBackgrounds.forEach(function(lazyBackground) {
			lazyBackgroundObserver.observe(lazyBackground);
		});
	}
	});