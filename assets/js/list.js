$.validator.setDefaults({ ignore: '' });

$(document).ready(function(){
	console.log("List JS");
	mycommenttable(base_path()+'home/get_comments/','comment_table');
	
	myvideotable(base_path()+'home/get_videos/','video_table');
	
	mytopEngagedtable(base_path()+'home/get_mytopengaged_videos/','my_topengage_table');
	
	mytopViewedtable(base_path()+'home/get_mytopview_videos/','my_topview_table');
	
	myincubatortable(base_path()+'home/get_incubators/','incubators_table');



	$("#incu_name").change(function(){
		myincubatortable(base_path()+'home/get_incubators/','incubators_table');
	})
	$("#incu_country").change(function(){
		myincubatortable(base_path()+'home/get_incubators/','incubators_table');
	})
	$("#incu_city").change(function(){
		myincubatortable(base_path()+'home/get_incubators/','incubators_table');
	})
	
});	

function mycommenttable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
		});*/
		},
	}); 
}

function myvideotable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, ] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
		});*/
		},
	}); 
}

function mytopEngagedtable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
		});*/
		},
	}); 
}

function mytopViewedtable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
		});*/
		},
	}); 
}

function myincubatortable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, ] }],
		"aoColumns": [ 
			{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'name': $("#incu_name").val(), 'country':$("#incu_country").val(),'city':$("#incu_city").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			
		},
		"fnDrawCallback": function () {
			
		},
	}); 
}