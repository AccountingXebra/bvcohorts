$(document).ready(function(){
	//var base_url = window.location.origin;
	
	console.log('BV Main JS');	
	$('.inactive').click(function () {
		$("#toast").toast({
			type: 'normal',
			message: 'Sign-up/Login is required to share and comment on videos'
		});
	});
	
	$('.list_awards').hide();
	
	$("#category").on("mousemove", function() {
		$("#category")[0].click();
		$("#category").unbind('mouseenter mouseleave');		
	});
	
	/*$("#category").one("mouseenter", function(e){
		$("#category")[0].click();
		$("#category").unbind('mouseenter mouseleave');		
	}).one("mouseleave", function(e){
		$("#category").unbind("click");
		$("#category").unbind('mouseenter mouseleave');
	});*/

	
	$(document).on("change",".hide-file-profile",function(){
		var filesize = (this.files[0].size);
		var filename = (this.files[0].name);
		/*$.ajax({
			url:base_url+'profile/check_for_allowedsize_profile',
			type:"POST",
			datatype: 'json',
			data:{"filesize":filesize,
				"filename":filename,
				"doctype":"image",
			},
			success:function(res){
		  
				var res1 = JSON.parse(res);
		
			},
		});*/
		
		if(filename !=""){
			var file = this.files[0];
			var fileType = file["type"];
		}
		
		readURL(this);
   });
   
   $(document).on("change",".hide-file-thumbnail",function(){
		var filesize = (this.files[0].size);
		var filename = (this.files[0].name);
		/*$.ajax({
			url:base_url+'profile/check_for_allowedsize_profile',
			type:"POST",
			datatype: 'json',
			data:{"filesize":filesize,
				"filename":filename,
				"doctype":"image",
			},
			success:function(res){
		  
				var res1 = JSON.parse(res);
		
			},
		});*/
		readthumbURL(this);
   });
	

	


		$("#deactivate").click(function(){
							
				$.ajax({
					url:base_url+'home/deactivated',
					type:"POST",
					data:{},
					success:function(res){
						if(res == true){	
							$("#toast").toast({
								type: 'success',
								message: 'Your account deactivated successfully'
							});
							window.setTimeout(function(){
								$('.close').click();
							}, 500);
							window.setTimeout(function(){
								// Move to a new location or you can do something else
								window.location.href = base_url+'logout';
							}, 3000);
						}else{
							$("#toast").toast({
								type: 'error',
								message: 'Something went wrong.Please try again'
							});
						}
					}
				});			
			
		});

		$("#feedback_frm").submit(function(e){
							e.preventDefault();
							}).validate({
							rules:{

								'feedback':{
									required:true,

								},

							},
							messages:{

								'feedback':{
									required:"Please give us a feedback",

								},

							},
							errorPlacement: function(error, element) {
		                       if (element.attr("name") == "feedback") {
		                           $("#feedback_error").html(error)
		                        }

		                    },
							submitHandler:function(form){

									var frm=$(form).serialize();
											$.ajax({
											url:base_url+'home/feedback',
											type:"POST",
											data:{"frm":frm,},
											success:function(res){
												var data = JSON.parse(res);

												if(data!= false)
												{
													//Materialize.toast('Your feedback has been received', 2000,'green rounded');
													//alert('Your feedback has been received');
													window.setTimeout(function(){
														$('.close').click();
													window.setTimeout(function(){
														$("#toast").toast({
															type: 'success',
															message: 'Thank You. We have received your feedback.'
														});
													}, 100);
													}, 200);
												}
												else
												{
													//Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													$("#toast").toast({
														type: 'error',
														message: 'Error while processing. Please try again'
													});
												}
												$("#feedback_frm").find("input[type=text],textarea").val("");
												$("#feedback_modal").modal('close');
											},
										});
									},
								});



		$("#award_frm").validate({
			rules: {
				name_awards:{
					required:true,
				},
				year_awards: {
					required:true,
					
				},
				desc_awards: {
					required:true,
					
				},
			},
			messages: {
				name_awards:{
					required:"Award name is required",
				},
				year_awards: {
					required:"Award year is required",
					
				},
				desc_awards: {
					required:"Award description is required.",
					
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serialize();
				//$(form).ajaxSubmit();								
				$.ajax({
					url:base_url+'home/award',
					type:"POST",
					data:{"frm":frm_signup,},
					success:function(res){
						//$('#verifify_modal').modal('show');
						//var data=JSON.parse(res);
						var yr_awards = $('#year_awards').val();
						var name_awards = $('#name_awards').val();
						var desc_awards = $('#desc_awards').val();
						var temp = '<div class="row" style="margin:0px -20px 10px -20px;"><div class="col-lg-6"><input id="award1" name="award1"  value="'+ yr_awards +'" class="full-bg adjust-width" type="text" disabled></div><div class="col-lg-6"><input id="name_award1" name="name_award1"  value="'+ name_awards +'" class="full-bg adjust-width" type="text" disabled></div></div>';
						$('.list_awards').show();
						$('.list_awards').append(temp);
						window.setTimeout(function(){
							$('.award-cancel').click();
							window.setTimeout(function(){
								//$("#toast").toast({
									//type: 'success',
									//message: 'Your feedback has been received'
								//});
							}, 100);
						}, 200);
						
					}
				});			
			}
		});
	
	$("#chngepassfrm").validate({
			rules: {
				old_password:{
					required:true,
				},
				new_password: {
					required:true,
					checklowercase: true,
					checkuppercase: true,
					checknumber: true,
					checkspecialchar: true,
					minlength:8,
				},
				new_confirm_password: {
					required:true,
					//checklowercase: true,
					//checkuppercase: true,
					//checknumber: true,
					//checkspecialchar: true,
					//minlength:8,
					equalTo: '#new_password',
				},
			},
			messages: {
				old_password:{
					required:"Old Password is required",
				},
				new_password: {
					required:"New Password is required",
					checklowercase:"At least 1 Lower case",
					checkuppercase:"At least 1 Upper case",
					checknumber:"At least 1 Number",
					checkspecialchar:"At least 1 Special Character",
					minlength:"Password must contain at least 8 characters.",
				},
				new_confirm_password: {
					required:"Confirm Password is required.",
					//minlength:"Password must contain at least 8 characters.",
					equalTo:"Password does not match!",
				},
			},
		submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								  
								$.ajax({
								url:base_url+'home/change_password',
								type:"POST",
								data:{"frm":frm,},
								success:function(res){
									//console.log(res);
									var data = JSON.parse(res);
										if(data == true)
										{
											 //$('#change_pass_success').modal('open');

											// $('#password-modal').modal('close');
											 // OTP Validation before Password change
											 // $('#password-otp-modal').modal('open');
											 // $('#password-otp-modal #mobile_no_pwd').prop("checked",false);
											 // $('#password-otp-modal #email_address_pwd').prop("checked",false);
											/* $('#pass-succ').modal({
												dismissible: false, // Modal can be dismissed by clicking outside of the modal
											  });

											 $('#pass-succ').modal('open');*/
											window.setTimeout(function(){
												$('.close').click();
												window.setTimeout(function(){
													$("#toast").toast({
														type: 'success',
														message: 'You have successfully changed the password'
													});
												}, 100);
											}, 200);
											//alert('Your password has been changed successfully.');
											window.setTimeout(function(){
												// Move to a new location or you can do something else
											    window.location.href = base_url+'logout';
											}, 5000);
										}
										else
										{  
											$("#toast").toast({
												type: 'error',
												message: 'Something went wrong.Please try again'
											});
										}
										$("#chngepassfrm #old_password").val('');
										$("#chngepassfrm #new_password").val('');
										$("#chngepassfrm #confirm_password").val('');
									},
					});
				},
		});
	

	$("#send_email_frm1").submit(function(e){

		e.preventDefault();

	}).validate({

				 rules: {

					get_email: {

						required:true,

						email:true,

					},

				 },

				  messages: {

					get_email: {

						required:"An Email id is required.",

						email:"Please Enter valid Email ID"

					},

				  },

				
				submitHandler: function(form) {
					//Materialize.toast('An email has been resent', 4000,'green rounded');
					$(".modal-loader").fadeIn("slow");
					var frm=$(form).serialize();
					var email_modal = $('#get_email').val();
					$.ajax({

					    url:base_url+"home/forgot_password",

                        type: "post",

                        data: {"frm":frm,},

						dataType: 'json',

                        success: function(res) {

							//alert(res);
							$(".modal-loader").fadeOut("slow");
                            if(res == true)

							{	
										
										$('#forgot_email_modal').modal('hide');
										$('#forgot_email_modal #get_email').val('');
										$("#toast_sign").toast({
											type: 'success',
											message: 'Reset link sent on registered Email Id'
										});
										$('#resend_email').modal('show');
										$('#resend_email #email_cust').text(email_modal);
										$('#resend_email #get_email').val(email_modal);
										/*window.setTimeout(function() {
										window.location.href = base_url;
										}, 5000);*/
							}

							else

							{

								$('#forgot_email_modal #get_email').val('');
								alert('This email ID is not registered with us');

								$("#send_email_frm1 #error_popup").html("<div class='error_login'><span>Error:</span>This email ID is not registered with us</div>");

							}

						}

					});

			}
		});


	   	$("#send_email_frm").submit(function(e){

	e.preventDefault();

	}).validate({

				 rules: {

					get_email: {

						required:true,

						email:true,

					},

				 },

				  messages: {

					get_email: {

						required:"An Email id is required.",

						email:"Please Enter valid Email ID"

					},

				  },

				
				submitHandler: function(form) {
					//Materialize.toast('An email has been resent', 4000,'green rounded');
					$(".modal-loader").fadeIn("slow");
					var frm=$(form).serialize();
					var email_modal = $('#get_email').val();
					$.ajax({

					    url:base_url+"home/forgot_password",

                        type: "post",

                        data: {"frm":frm,},

						dataType: 'json',

                        success: function(res) {

							//alert(res);
							$(".modal-loader").fadeOut("slow");
                            if(res == true)

							{	
										
										$('#forgot_email_modal').modal('hide');
										$('#forgot_email_modal #get_email').val('');
										$("#toast_sign").toast({
											type: 'success',
											message: 'Reset link sent on registered Email Id'
										});
										//$('#resend_email').modal('show');
										$('#resend_email #email_cust').text(email_modal);
										$('#resend_email #get_email').val(email_modal);
										/*window.setTimeout(function() {
										window.location.href = base_url;
										}, 5000);*/
							}

							else

							{

								$('#forgot_email_modal #get_email').val('');
								alert('This email ID is not registered with us');

								$("#send_email_frm #error_popup").html("<div class='error_login'><span>Error:</span>This email ID is not registered with us</div>");

							}

						}

					});

			}
		});
	

	$("#signin_frm").submit(function(e){
		e.preventDefault();
		
	}).validate({

				 rules:{

					name:{

						required:true,

						email:true,

					},

					password_in:{

						required:true,

						minlength:8,

					},

					"g-recaptcha-response":{required:true,}



				},

				messages:{

					name:{

						required:"Please enter valid email-id",

						email:"Please Enter valid Email ID"

					},

					password_in:{

						required:"Please enter valid password",

						minlength:"Password must contain atleast 8 characters."

					},

					"g-recaptcha-response":{required:"Varify Recaptcha",}
				},
				highlight: function (element) {
					$('.pass-wrng').addClass('high');
				},
				unhighlight: function (element) {
					$('.pass-wrng').removeClass('high');
				},
				  
				submitHandler: function(form) {
					var frm_login=$(form).serialize();
					$.ajax({
					    url:base_url+'home/login_user',
                        type: "post",
                        data: {"frm_login":frm_login,},
						//dataType: 'json',
                        success: function(res) {
                        	
                        	str1=res.split('/')

							if(str1[0]=='success')
							{    

								
  
                                  if(page_url==""){
                                  	window.location.href=base_url;
                                  }else{
                                  	window.location.href=base_url;
                                  }
								//Accountant
								//$("#login_modal").modal('hide');
								//$("#login_modal #referral_login_frm").find("input[type=text], textarea").val("");
								//$("#login_modal #referral_login_frm").find("input[type=password], textarea").val("");
								//$("#login_otp_modal").modal('show');							
								
							}
							
							else if(str1[0]=='error_password'){
								 $("#error_popup").html("<div class='error_login'>Oops, the password that you entered is incorrect</div>");
							}
							else if(str1[0]=='InvalidEmail'){
								$('#error_popup').html("<div class='error_login'>No such username exists in the system!</div>");
								//$("#error_popup").html("<div class='error_login'>Your Account is Block By Admin. Contact To Admin. </div>");
							}
							else if(str1[0]=='captcha_show'){
								//$("#recaptcha").show();
							}
							else if(str1[0]=='attemt_exceed'){
								//$("#recaptcha").show();
								//$('#error_popup').html("<div class='error_login'>Someone have already logged in your account.</div>");
								//$("#error_popup").html("<div class='error_login'>Your Account is Block By Admin. Contact To Admin. </div>");
							}
							else if(str1[0]=='error_account'){
								$("#error_popup").html("<div class='error_login'>Your account is currently deactivated. </br>Email <a style='color:rgb(77, 68, 167); text-decoration:underline;' href='mailto:support@bharatvaani.in'>support@bharatvaani.in</a> to reactivate it</div>");
								//$('#reactive_bus').modal('show');
							}
							else
							{   
							
                                    $('#error_popup').html("<div class='error_login'>Account is not verified .</div>");
							}

	

                        }

					});

				},

	});
	
	$("#signin_frm_mb").submit(function(e){
		e.preventDefault();
	}).validate({

				 rules:{

					name:{

						required:true,

						email:true,

					},

					password_in:{

						required:true,

						minlength:8,

					},

					"g-recaptcha-response":{required:true,}



				},

				messages:{

					name:{

						required:"Please enter valid email-id",

						email:"Please Enter valid Email ID"

					},

					password_in:{

						required:"Please enter valid password",

						minlength:"Password must contain atleast 8 characters."

					},

					"g-recaptcha-response":{required:"Varify Recaptcha",}
				},
				highlight: function (element) {
					$('.pass-wrng').addClass('high');
				},
				unhighlight: function (element) {
					$('.pass-wrng').removeClass('high');
				},
				  
				submitHandler: function(form) {
					var frm_login=$(form).serialize();
					$.ajax({
					    url:base_url+'home/login_user',
                        type: "post",
                        data: {"frm_login":frm_login,},
						//dataType: 'json',
                        success: function(res) {
                        	
                        	str1=res.split('/')

							if(str1[0]=='success'){    
								if(page_url==""){
									window.location.href=base_url;
                                }else{
									window.location.href=base_url;
                                }
								//Accountant
								//$("#login_modal").modal('hide');
								//$("#login_modal #referral_login_frm").find("input[type=text], textarea").val("");
								//$("#login_modal #referral_login_frm").find("input[type=password], textarea").val("");
								//$("#login_otp_modal").modal('show');							
								
							}
							
							else if(str1[0]=='error_password'){
								 $("#error_popup_mb").html("<div class='error_login'>Oops, the password that you entered is incorrect</div>");
							}
							else if(str1[0]=='InvalidEmail'){
								$('#error_popup_mb').html("<div class='error_login'>No such username exists in the system!</div>");
								//$("#error_popup").html("<div class='error_login'>Your Account is Block By Admin. Contact To Admin. </div>");
							}
							else if(str1[0]=='captcha_show'){
								//$("#recaptcha").show();
							}
							else if(str1[0]=='attemt_exceed'){
								//$("#recaptcha").show();
								//$('#error_popup').html("<div class='error_login'>Someone have already logged in your account.</div>");
								//$("#error_popup").html("<div class='error_login'>Your Account is Block By Admin. Contact To Admin. </div>");
							}
							else if(str1[0]=='error_account'){
								$("#error_popup_mb").html("<div class='error_login'>Your account is currently deactivated. </br>Email <a style='color:rgb(77, 68, 167); text-decoration:underline;' href='mailto:support@bharatvaani.in'>support@bharatvaani.in</a> to reactivate it</div>");
								//$('#reactive_bus').modal('show');
							}
							else
							{   
							
                                    $('#error_popup_mb').html("<div class='error_login'>Account is not verified .</div>");
							}

	

                        }

					});

				},

			});
	
	
	$("#signup_frm").validate({
		rules: {
			name: {
				required:true,
			},
			email: {
				required:true,
				email:true,
			},
			password_2: {
				required:true,
				checklowercase: true,
				checkuppercase: true,
				checknumber: true,
				checkspecialchar: true,
				minlength:8,
			},
			confirm_password: {
				required:true,
				//minlength:8,
				equalTo: '#password_2',
			},
			contact: {
				required:true,
				number:true,
				maxlength:10,
				minlength:10,
			},
		},
		messages: {
			name: {
				required:"Name is required.",						
			},
			email: {
				required:"An Email id is required.",
				email:"Please Enter valid Email ID"
			},
			password_2: {
				required:"Password is required",
				checklowercase:"At least 1 Lower case",
				checkuppercase:"At least 1 Upper case",
				checknumber:"At least 1 Number",
				checkspecialchar:"At least 1 Special Character",
				minlength:"Password must contain at least 8 characters.",
			},
			confirm_password: {
				required:"Confirm Password is required.",
				//minlength:"Password must contain at least 8 characters.",
				equalTo:"Password does not match!",
			},
			contact: {
				required:"Contact Number is required.",
				number: "Please enter valid 10-digit mobile number.",
				maxlength:"Please enter valid 10-digit mobile number.",
				minlength:"Please enter valid 10-digit mobile number.",
			},
		},
		highlight: function (element) {
			$('.pass-wrng').addClass('high');
		},
		unhighlight: function (element) {
			$('.pass-wrng').removeClass('high');
		},
		submitHandler: function(form) {
					var frm_signup=$(form).serialize();
								 //$(form).ajaxSubmit();								
								$.ajax({
								url:base_url+'home/home_register',
								type:"POST",
								data:{"frm_signup":frm_signup,},
								success:function(res){

										if(res == false)
										{
											$("#signup_frm #error_popup").html("<div class='error_login'><span>Error:</span>Email Not Send ! Please try again!</div>");
										}	
										
										else if(res == 'notExits')
										{
											$("#signup_frm #error_popup").html("<div class='error_login'>This email ID is already registered with us !!</div>");
										}
										else {   
												$("#signup_frm #error_popup").html("<div class='error_login'><span> A verification email has been sent to your registered id</span></div>");
												$("#signup_frm #invalid_password_alpha").css('display','none');
												$("#signup_frm").find("input[type=text], textarea").val("");
											//	$('#signup_modal').modal('hide');
												var data=JSON.parse(res);

												
												$("#reg_id_otp").val(data);
												//$("#reg_id_otp").val(data[0]['reg_id']);

													$.ajax({
								url:base_url+'home/send_verification',
								type: "post",
								data: {"reg_id":data,},
								 success: function(res) {
								 	//$("#referral_login_frm #error_popup").html("<div class='error_login'><span>Error:</span> Verification link send on your register email id ! Please verify and login!</div>");
                               //  window.location.href= base_url+"refer";
								 }
							});
												

										}
											
									},
							});			
		}
	});
	
	$("#signup_frm_mb").validate({
		rules: {
			name: {
				required:true,
			},
			email: {
				required:true,
				email:true,
			},
			password_2: {
				required:true,
				checklowercase: true,
				checkuppercase: true,
				checknumber: true,
				checkspecialchar: true,
				minlength:8,
			},
			confirm_password: {
				required:true,
				//minlength:8,
				equalTo: '#password_mb2',
			},
			contact: {
				required:true,
				number:true,
				maxlength:10,
				minlength:10,
			},
		},
		messages: {
			name: {
				required:"Name is required.",						
			},
			email: {
				required:"An Email id is required.",
				email:"Please Enter valid Email ID"
			},
			password_2: {
				required:"Password is required",
				checklowercase:"At least 1 Lower case",
				checkuppercase:"At least 1 Upper case",
				checknumber:"At least 1 Number",
				checkspecialchar:"At least 1 Special Character",
				minlength:"Password must contain at least 8 characters.",
			},
			confirm_password: {
				required:"Confirm Password is required.",
				//minlength:"Password must contain at least 8 characters.",
				equalTo:"Password does not match!",
			},
			contact: {
				required:"Contact Number is required.",
				number: "Please enter valid 10-digit mobile number.",
				maxlength:"Please enter valid 10-digit mobile number.",
				minlength:"Please enter valid 10-digit mobile number.",
			},
		},
		highlight: function (element) {
			$('.pass-wrng').addClass('high');
		},
		unhighlight: function (element) {
			$('.pass-wrng').removeClass('high');
		},
		submitHandler: function(form) {
					var frm_signup=$(form).serialize();
								 //$(form).ajaxSubmit();								
								$.ajax({
								url:base_url+'home/home_register',
								type:"POST",
								data:{"frm_signup":frm_signup,},
								success:function(res){

										if(res == false)
										{
											$("#signup_frm_mb #error_popup").html("<div class='error_login'><span>Error:</span>Email Not Send ! Please try again!</div>");
										}	
										
										else if(res == 'notExits')
										{
											$("#signup_frm_mb #error_popup").html("<div class='error_login'>This email ID is already registered with us !!</div>");
										}
										else {   
												$("#signup_frm_mb #error_popup").html("<div class='error_login'><span> A verification email has been sent to your registered id</span></div>");
												$("#signup_frm_mb #invalid_password_alpha").css('display','none');
												$("#signup_frm_mb").find("input[type=text], textarea").val("");
											//	$('#signup_modal').modal('hide');
												var data=JSON.parse(res);

												
												$("#reg_id_otp").val(data);
												//$("#reg_id_otp").val(data[0]['reg_id']);

													$.ajax({
								url:base_url+'home/send_verification',
								type: "post",
								data: {"reg_id":data,},
								 success: function(res) {
								 	//$("#referral_login_frm #error_popup").html("<div class='error_login'><span>Error:</span> Verification link send on your register email id ! Please verify and login!</div>");
                               //  window.location.href= base_url+"refer";
								 }
							});
												

										}
											
									},
							});			
		}
	});
	
	$("#advertisting_form").validate({
			rules: {
				name:{
					required:true,
				},
				company: {
					required:true,
				},
				email: {
					required:true,
				},
				subject: {
					required:true,
				},
				message: {
					required:true,
				},
			},
			messages: {
				name:{
					required:"Name is required",
				},
				company: {
					required:"Company Name is required",
				},
				email: {
					required:"Email is required",
				},
				subject: {
					required:"Subject is required",
				},
				message: {
					required:"Message is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				console.log(frm_signup);
				frm_signup.submit();
			}
		});
	
	$.validator.addMethod("checklowercase",
		function(value, element) {
		return /^(?=(.*[a-z]){1,}).{1,}$/.test(value);
	});
	$.validator.addMethod("checkuppercase",
		function(value, element) {
		return /^(?=.*?[A-Z]).{1,}$/.test(value);
	});
	$.validator.addMethod("checknumber",
		function(value, element) {
		return /^(?=(.*[\d]){1,}).{1,}$/.test(value);
	});
	$.validator.addMethod("checkspecialchar",
		function(value, element) {
		return /^(?=(.[\W]){1,})(?!.\s).{1,}$/.test(value);
	});
	
/* Notification Code */
(function($) {
	$.fn.toast = function(options)  {
   
		var settings = $.extend({
			type: 'normal',
			message:  null
		}, options);
    
		var item = $('<div class="notification ' + settings.type + '"><span>' + settings.message + '</span></div>');
		this.append($(item));
		$(item).animate({ "top": "12px" }, "fast");
		setInterval(function() {
			$(item).animate({ "top": "-400px" }, function() {
				$(item).remove();
			});
		}, 3000);
	}
  
	$(document).on('click', '.notification', function() {
		$(this).fadeOut(400, function(){
			$(this).remove();
		});
	});  
}(jQuery));

/*$("#toast").toast({
  type: 'success',
  message: 'Success message'
});

$("#toast").toast({
  type: 'error',
  message: 'Error message'
});*/

/* Notification Code End */

});

function open_forgotpassword(){

	$('#forgot_email_modal').modal('show');

	//$('#resend_email').modal('show');
}

function open_login(){
	$('#forgot_email_modal').modal('hide');
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.uploader-placeholder').css('background-image', 'url('+e.target.result+')');
		};
		reader.readAsDataURL(input.files[0]);
	}
}

function readthumbURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.thumbnail_box').css('background-image', 'url('+e.target.result+')');
		};
		
		reader.readAsDataURL(input.files[0]);
	}
}