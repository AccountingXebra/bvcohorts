//$.validator.setDefaults({ ignore: '' });

$(document).ready(function(){
	console.log("Admin List JS");
	
		$(document).on("change","#video_up",function(){
			var file_id=$(this).data("val");
			var filesize = (this.files[0].size);
			var filesize_mb =  filesize / 1048576;
			var filename = (this.files[0].name);
			if(filename !=""){
				var file = this.files[0];
				var fileType = file["type"];
				var validImageTypes = ["video/mp4", "video/mov", "video/wmv", "video/flv", "video/avi"];
				if ($.inArray(fileType, validImageTypes) < 0) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Video Format is incorrect.'
					});
					$("#video_up").val('');
				}else if(filesize_mb > 100) {
					$("#toast").toast({
						type: 'error_1',
						message: 'Upload video size should not be more than 100mb'
					});
					$("#video_up").val('');
				}	
			}
		});
		
	$("#news_form").submit(function(e){
		e.preventDefault();
		}).validate({
			rules:{
				news_code:{
					required:true,
				},
				news_date:{
					required:true,
				},
				news_name:{
					required:true,
				},
				news_desc:{
					//required:true,
				},
				start_day_entry:{
					//required:true,
				},
				end_day_entry:{
					//required:true,
				},
				location:{
					required:true,
				},
				url:{
					required:true,
				},
			},

			messages:{
				news_code:{
					required:"Code is required",
				},
				news_date:{
					required:"Date is required",
				},
				news_name:{
					required:"News Title is required",
				},
				news_desc:{
					required:"Description is required",
				},
				start_day_entry:{
					required:"Start Date is required",
				},
				end_day_entry:{
					required:"End Date is required",
				},
				location:{
					required:"Location is required",
				},
				url:{
					required:"Website Url is required",
				},
			},
			submitHandler: function(form) {
				form.submit();
		    }
		});		
		
	$("#challange_form").validate({
			rules: {
				challange_code:{
					required:true,
				},
				challange_date:{
					required:true,
				},
				challange_name: {
					required:true,
				},
				challange_image: {
					required:true,
				},
				challange_desc:{
					required:true,
				},
				challange_nature:{
					required:true,
				},
				last_day_entry:{
					required:true,
				},
				location:{
					required:true,
				},
				url:{
					required:true,
				},
			},
			messages: {
				challange_code:{
					required:"Code is required",
				},
				challange_date:{
					required:"Date is required",
				},
				challange_name: {
					required:"Name is required",
				},
				challange_image: {
					required:"Image is required",
				},
				challange_desc:{
					required:"Description is required",
				},
				challange_nature:{
					required:"Nature of Business is required",
				},
				last_day_entry:{
					required:"Last Date is required",
				},
				location:{
					required:"Location is required",
				},
				url:{
					required:"Website url is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				//console.log(frm_signup);
				frm_signup.submit();
				
				$(".loader").show();
				
			}
		});			
		
	$("#quiz_form").validate({
			rules: {
				quiz_name:{
					required:true,
				},
				quiz_url:{
					required:true,
				},
				cover_image: {
					required:true,
				},
				sub_start_date: {
					required:true,
				},
				sub_end_date:{
					required:true,
				},
				duration:{
					required:true,
				},
			},
			messages: {
				quiz_name:{
					required:"Name is required",
				},
				quiz_url:{
					required:"Customize url is required",
				},
				cover_image: {
					required:"Image is required",
				},
				sub_start_date: {
					required:"Start Date is required",
				},
				sub_end_date:{
					required:"End Date is required",
				},
				duration:{
					required:"Duration is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				//console.log(frm_signup);
				frm_signup.submit();
				
				$(".loader").show();
				
			}
		});		


	$("#edit_quiz_form").validate({
			rules: {
				quiz_name:{
					required:true,
				},
				quiz_url:{
					required:true,
				},
				
				sub_start_date: {
					required:true,
				},
				sub_end_date:{
					required:true,
				},
				duration:{
					required:true,
				},
			},
			messages: {
				quiz_name:{
					required:"Name is required",
				},
				quiz_url:{
					required:"Customize url is required",
				},
				
				sub_start_date: {
					required:"Start Date is required",
				},
				sub_end_date:{
					required:"End Date is required",
				},
				duration:{
					required:"Duration is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				//console.log(frm_signup);
				frm_signup.submit();
				
				$(".loader").show();
				
			}
		});		
		
		
	$("#podcast_form").validate({
			rules: {
				podcast_name:{
					required:true,
				},
				podcast_url:{
					required:true,
				},
				cover_image: {
					required:true,
				},
				name: {
					required:true,
				},
				podcast_conduct:{
					required:true,
				},
				podcast_audio:{
					required:true,
				},
				alt_name:{
					required:true,
				},
				pod_description:{
					required:true,
				}
			},
			messages: {
				podcast_name:{
					required:"Title of the podcast is required",
				},
				podcast_url:{
					required:"Customize Url is required",
				},
				cover_image: {
					required:"Cover image is required",
				},
				name: {
					required:"Name is required",
				},
				podcast_conduct:{
					required:"Conducted by is required",
				},
				podcast_audio:{
					required:"Audio is required",
				},
				alt_name:{
					required:"This field is required",
				},
				pod_description:{
					required:"Description is required",
				}
			},
			submitHandler: function(form) {
				//var frm_signup=$(form).serializeArray();
				//console.log(frm_signup);
				//frm_signup.submit();
				var title=$('#podcast_name').val();
				var name=$('#name').val();
				var conduct=$('#podcast_conduct').val();
				var url=$('#podcast_url').val();
				var pod_description=$('#pod_description').val();
				var alt_name=$('#alt_name').val();
				var formData = new FormData();
				formData.append('title', title);
				formData.append('name', name);
				formData.append('conduct', conduct);
				formData.append('url', url);
				formData.append('pod_description', pod_description);
				formData.append('alt_name', alt_name);
					
				formData.append('cover', $('#cover_image')[0].files[0]);
				formData.append('podcast', $('#podcast_audio')[0].files[0]);
				//formData.append('thumbnail', $('#thumbnail')[0].files[0]);


				$(".loader").show();
				$.ajax({
				url:base_url+'admin/admin_podcast',
					method:"POST",
					data:formData,
					contentType:false,
					cache:false,
					processData:false,
					success:function(data){
						//$('#video_up').val('');
						//$('#video_up').val('');
						if(data==true){
							//alert("video upload successfully");
							window.setTimeout(function(){
							$(".loader").hide();
							window.setTimeout(function(){
								$("#toast").toast({
									type: 'success',
									message: 'Podcast Uploaded Successfully!'
								});
							window.setTimeout(function(){	
							window.location.href=base_url+'admin/podcast_list';
							}, 200);
							}, 300);
							}, 500);
						}else{
							//alert("Something went wrong.Try again...");
							$("#toast").toast({
								type: 'error',
								message: 'Something went wrong.Try again...'
							});
						}
					}
				});
			}
		});	

	$("#edit_podcast_form").validate({
			rules: {
				podcast_name:{
					required:true,
				},
				
				name: {
					required:true,
				},
				podcast_conduct:{
					required:true,
				},

				podcast_url:{
					required:true,
				},
				alt_name:{
					required:true,
				},
			},
			messages: {
				podcast_name:{
					required:"Title of the podcast is required",
				},

				podcast_url:{
					required:"Customize Url is required",
				},
				
				name: {
					required:"Name is required",
				},
				podcast_conduct:{
					required:"Conducted by is required",
				},
				alt_name:{
					required:"This field is required",
				},
				
			},
			submitHandler: function(form) {
				//var frm_signup=$(form).serializeArray();
				//console.log(frm_signup);
				//frm_signup.submit();
				var id=$('#podcast_id').val();
				var title=$('#podcast_name').val();
				var name=$('#name').val();
				var conduct=$('#podcast_conduct').val();
				var url=$('#podcast_url').val();
				var pod_description=$('#pod_description').val();
				var alt_name=$('#alt_name').val();
				
				var formData = new FormData();
				formData.append('title', title);
				formData.append('name', name);
				formData.append('conduct', conduct);
				formData.append('url', url);
				formData.append('pod_description', pod_description);
				formData.append('alt_name', alt_name);
					 formData.append('cover', $('#cover_image')[0].files[0]);	
					
					formData.append('podcast', $('#podcast_audio')[0].files[0]);
					
				
				
				
				//formData.append('thumbnail', $('#thumbnail')[0].files[0]);


				//$(".loader").show();
				$.ajax({
				url:base_url+'admin/edit_admin_podcast/'+id,
					method:"POST",
					data:formData,
					contentType:false,
					cache:false,
					processData:false,
					success:function(data){
						//$('#video_up').val('');
						//$('#video_up').val('');
						if(data==true){
							//alert("video upload successfully");
							window.setTimeout(function(){
								//$(".loader").hide();
							window.setTimeout(function(){
								$("#toast").toast({
									type: 'success',
									message: 'Podcast Uploaded Successfully!'
								});
							window.setTimeout(function(){	
							window.location.href=base_url+'admin/podcast_list';
							}, 200);
							}, 300);
							}, 500);
						}else{
							//alert("Something went wrong.Try again...");
							$("#toast").toast({
								type: 'error',
								message: 'Something went wrong.Try again...'
							});
						}
					}
				});
			}
		});	
		

	$("#blog_form_vid").validate({

			rules: {
				category:{
					required:true,
				},
				post_name:{
					required:true,
				},
				blog_content:{
					//required:true,
				},
				alt_name:{
					required:true,
				},
				featured_image:{
					required:true,
				},
				excerpt:{
					required:true,
				},
			},
			messages: {
				category:{
					required:"Category is required",
				},
				post_name:{
					required:"Post name is required",
				},
				blog_content:{
					//required:"Please enter content",
				},
				alt_name:{
					required:"This field is required",
				},
				featured_image:{
					required:"Featured Image is required",
				},
				excerpt:{
					required:"Excerpt is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				console.log(frm_signup);
				frm_signup.submit();
				var category=$('#category').val();
				var post_name=$('#post_name').val();
				var content=$('#blog_content').val();
				var excerpt=$('#excerpt').val();
				var url=$('#url').val();
				var alt_name=$('#alt_name').val();
				var formData = new FormData();
				formData.append('category', category);
				formData.append('post_name', post_name);
				formData.append('url', url);
				formData.append('content', content);
				formData.append('excerpt', excerpt);
				formData.append('alt_name', alt_name);
				formData.append('file', $('#featured_image')[0].files[0]);

				
				//$(".loader").show();
				/*$.ajax({
				url:base_url+'admin/interview_add',
					method:"POST",
					data:formData,
					contentType:false,
					cache:false,
					processData:false,
					success:function(data){
						$('#video_up').val('');
						if(data==true){
							//alert("video upload successfully");
							window.setTimeout(function(){
								$(".loader").hide();
							window.setTimeout(function(){
								$("#toast").toast({
									type: 'success',
									message: 'Interview Uploaded Successfully!'
								});
							window.setTimeout(function(){	
							window.location.href=base_url+'admin/admin_interviews';
							}, 200);
							}, 300);
							}, 500);
						}else{
							//alert("Something went wrong.Try again...");
							$("#toast").toast({
								type: 'error',
								message: 'Something went wrong.Try again...'
							});
						}
					}
				});*/
		}
	});	
	
	$("#interview_form_vid").validate({
			rules: {
				name:{
					required:true,
				},
				url:{
					required:true,
				},
				name:{
					required:true,
				},
				conduct: {
					required:true,
				},
				video: {
					required:true,
				},
			},
			messages: {
				name:{
					required:"Name is required",
				},
				url:{
					required:"Url is required",
				},
				name:{
					required:"Name is required"
				},
				conduct: {
					required:"Name is required",
				},
				video: {
					required:"Video is required",
				},
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				console.log(frm_signup);
				//frm_signup.submit();
				var title=$('#title').val();
				var name=$('#name').val();
				var conduct=$('#conduct').val();
				var url=$('#url').val();
				var formData = new FormData();
				formData.append('title', title);
				formData.append('name', name);
				formData.append('conduct', conduct);
				formData.append('url', url);
					
				formData.append('file', $('#video_up')[0].files[0]);
				 formData.append('cover', $('#cover_image')[0].files[0]);	
				//formData.append('thumbnail', $('#thumbnail')[0].files[0]);
				
				$(".loader").show();
				$.ajax({
				url:base_url+'admin/interview_add',
					method:"POST",
					data:formData,
					contentType:false,
					cache:false,
					processData:false,
					success:function(data){
						$('#video_up').val('');
						if(data==1){
							//alert("video upload successfully");
							window.setTimeout(function(){
								$(".loader").hide();
							window.setTimeout(function(){
								$("#toast").toast({
									type: 'success',
									message: 'Interview Uploaded Successfully!'
								});
							window.setTimeout(function(){	
							window.location.href=base_url+'admin/admin_interviews';
							}, 200);
							}, 300);
							}, 500);
						}else{
							//alert("Something went wrong.Try again...");
							$(".loader").hide();
							$("#toast").toast({
								type: 'error_1',
								message: 'Something went wrong. Try again...'
							});
						}
					}
				});
			}
		});

		$("#interview_form_vid_edit").validate({
			rules: {
				name:{
					required:true,
				},
				url:{
					required:true,
				},
				conduct: {
					required:true,
				},
				
			},
			messages: {
				name:{
					required:"Name is required",
				},
				url:{
					required:"Url is required",
				},
				conduct: {
					required:"Name is required",
				},
			
			},
			submitHandler: function(form) {
				var frm_signup=$(form).serializeArray();
				console.log(frm_signup);
				//frm_signup.submit();
				var title=$('#title').val();
				var name=$('#name').val();
				var conduct=$('#conduct').val();
				var url=$('#url').val();
				var id=$('#id').val();
				var formData = new FormData();
				formData.append('title', title);
				formData.append('name', name);
				formData.append('conduct', conduct);
				formData.append('url', url);
				formData.append('id', id);
					
				formData.append('file', $('#video_up')[0].files[0]);
				 formData.append('cover', $('#cover_image')[0].files[0]);	
				//formData.append('thumbnail', $('#thumbnail')[0].files[0]);
				
				$(".loader").show();
				$.ajax({
				url:base_url+'admin/interview_edit/'+id,
					method:"POST",
					data:formData,
					contentType:false,
					cache:false,
					processData:false,
					success:function(data){
						$('#video_up').val('');
						if(data==true){
							//alert("video upload successfully");
							window.setTimeout(function(){
								$(".loader").hide();
							window.setTimeout(function(){
								$("#toast").toast({
									type: 'success',
									message: 'Interview Uploaded Successfully!'
								});
							window.setTimeout(function(){	
							window.location.href=base_url+'admin/admin_interviews';
							}, 200);
							}, 300);
							}, 500);
						}else{
							//alert("Something went wrong.Try again...");
							$("#toast").toast({
								type: 'error',
								message: 'Something went wrong.Try again...'
							});
						}
					}
				});
			}
		});
	
	/* Signup Details Table */
	mysignupDetailstable(base_path()+'admin/get_admin_deatils/','signup_details_table');
	$("#status").change(function(){
		mysignupDetailstable(base_path()+'admin/get_admin_deatils/','signup_details_table');
	})
	$("#city").change(function(){
		mysignupDetailstable(base_path()+'admin/get_admin_deatils/','signup_details_table');
	})
	
	//mysignupDetailstable(base_path()+'admin/get_admin_deatils/','signup_details_table');
	
	var bulk_signup = [];
	$("body").on("click","input[id='sign_bulk']",function(){
		if($(this).is(':checked')) {
			$(".signup_bulk_action").prop('checked', true);
			bulk_signup = [];
			$(".signup_bulk_action:checked").each(function() {
				bulk_signup.push($(this).val());
			});
			bulk_signup = bulk_signup.join(",");
			$('#signup_details_print').attr('data-multi_signup',bulk_signup);
		}
		else {
    		$('#signup_details_table tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
   			});
			bulk_signup = [];
			$('#signup_details_print').attr('data-multi_signup',0);
		}
	});
	
	$("body").on('click','#signup_details_print',function(e) {
		var dnwl_ids = $('#signup_details_print').attr('data-multi_signup');
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			alert('Please select two or more row for bulk action');
			Materialize.toast('Please select two or more row for bulk action', 2000,'red rounded');
			 var toastHTML = '<span>I am toast content</span><button class="btn-flat toast-action">Undo</button>';
			M.toast({html: toastHTML});
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#signup_details_print').attr('data-multi_signup',0);
				$('#signup_details_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				$("#sign_bulk").prop('checked', false);
				window.location=base_url+'admin/print_signup?ids='+dnwl_ids;
				mysignupDetailstable(base_path()+'admin/get_admin_deatils/','signup_details_table');
				Materialize.toast('Signup details has been successfully print', 2000,'green rounded');
			}
		}
	});
	
	/* User Upload details Table */
	myuservideoDetailstable(base_path()+'admin/get_uservideo_deatils/','uservideo_details_table');
	
	var bulk_uservideo = [];
	$("body").on("click","input[id='uservideo_bulk']",function(){
		if($(this).is(':checked')) {
			$(".uservideo_bulk_action").prop('checked', true);
			bulk_uservideo = [];
			$(".uservideo_bulk_action:checked").each(function() {
				bulk_uservideo.push($(this).val());
			});
			bulk_uservideo = bulk_uservideo.join(",");
			$('#user_video_print').attr('data-multi_uservideo',bulk_uservideo);
		}
		else {
    		$('#uservideo_details_table tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
   			});
			bulk_uservideo = [];
			$('#user_video_print').attr('data-multi_uservideo',0);
		}
	});
	
	$("body").on('click','#user_video_print',function(e) {
		var dnwl_ids = $('#user_video_print').attr('data-multi_uservideo');
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			alert('Please select two or more row for bulk action');
			Materialize.toast('Please select two or more row for bulk action', 2000,'red rounded');
			var toastHTML = '<span>I am toast content</span><button class="btn-flat toast-action">Undo</button>';
			M.toast({html: toastHTML});
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#user_video_print').attr('data-multi_uservideo',0);
				$('#uservideo_details_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				$("#sign_bulk").prop('checked', false);
				window.location=base_url+'admin/print_signup?ids='+dnwl_ids;
				mysignupDetailstable(base_path()+'admin/get_admin_deatils/','uservideo_details_table');
				Materialize.toast('User Video details has been successfully print', 2000,'green rounded');
			}
		}
	});
	
	/* Top view video details Table */
	mytopvideoDetailstable(base_path()+'admin/get_topvideo_deatils/','top_viewvideo_table');
	
	/* Top engaged video details Table */
	mytopEngagedtable(base_path()+'admin/get_topengaged_deatils/','top_engagedvideo_table');
	
	/* Active Users details Table */
	myactiveUserstable(base_path()+'admin/get_activeuser_deatils/','active_users_table');
	
	/* Top Categories details Table */
	mytopCattable(base_path()+'admin/get_categories_deatils/','top_category_table');
	
	/* Interviews details Table */
	myinterviewtable(base_path()+'admin/get_interview_deatils/','interview_table');
	
	/* Post list Table */
	mypoststable(base_path()+'admin/get_post_deatils/','post_table');
	
	/* Podcast list Table */
	myPodcasttable(base_path()+'admin/get_podcast_deatils/','podcast_table');
	
	/* Quiz list Table */
	myQuizstable(base_path()+'admin/get_quiz_deatils/','quiz_table');
	
	/* Challenges Table */
	myChallengestable(base_path()+'admin/get_challange_details/','challenge_table');
	
	/* Incubator Table */
	incubatorDatatable(base_path()+'admin/get_incubator_details','incubator_table');
	
	/* Office Table */
	coworkingDatatable(base_path()+'admin/get_coworking_details','cowork_table');

	/* News Table */
	myNewstable(base_path()+'admin/get_news_details/','news_table');

});	

function myNewstable(url,id){
	
    $('#'+id).DataTable().destroy();
  
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
	{ "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{'name':$('#cowork_name').val(), 'state':$('#cowork_country').val(), 'city':$('#cowork_city').val()},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      /*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });*/

        	$('.deactive_space').on('click',function(){

				var workspace_id = $(this).data('cd_id');

				 $.ajax({

					url:base_url+'admin/delete_workspace',

					type:"POST",

					data:{"workspace_id":workspace_id,},

					success:function(res){
							if(res == 1)
							{
								//window.location.href=base_url+'Community/events';
								//Materialize.toast('Your Workspace has been successfully deleted', 2000,'green rounded');
								$("#toast").toast({
									type: 'success',
									message: 'Your Workspace has been successfully deleted'
								});
								coworkingDatatable(base_path()+'admin/get_coworking_details','cowork_table');
								//$('select').material_select();
							}
							else
							{
								$("#toast").toast({
									type: 'error_1',
									message: 'Error while processing.'
								});
								//window.location.href=base_url+'Community/deals';
								//Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});

			});

			
		  
			  
            
    }
});
}

function coworkingDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
  
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{'name':$('#cowork_name').val(), 'state':$('#cowork_country').val(), 'city':$('#cowork_city').val()},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      /*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });*/

        	$('.deactive_space').on('click',function(){

				var workspace_id = $(this).data('cd_id');

				 $.ajax({

					url:base_url+'admin/delete_workspace',

					type:"POST",

					data:{"workspace_id":workspace_id,},

					success:function(res){
							if(res == true)
							{
								//window.location.href=base_url+'Community/events';
								//Materialize.toast('Your Workspace has been successfully deleted', 2000,'green rounded');
								$("#toast").toast({
									type: 'success',
									message: 'Your Workspace has been successfully deleted'
								});
								coworkingDatatable(base_path()+'admin/get_coworking_details','cowork_table');
								//$('select').material_select();
							}
							else
							{
								$("#toast").toast({
									type: 'error_1',
									message: 'Error while processing.'
								});
								//window.location.href=base_url+'Community/deals';
								//Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});

			});

			
		  
			  
            
    }
});
}

function incubatorDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
  
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{ 'name':$('#incub_name').val(), 'country':$('#incub_country').val(), 'city':$('#incub_city').val()},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      /*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });*/

        	$('.deactive_incub').on('click',function(){

				var incub_id = $(this).data('cd_id');

				 $.ajax({

					url:base_url+'admin/delete_incubation',

					type:"POST",

					data:{"incube_id":incub_id,},

					success:function(res){
							if(res == true)
							{
								//window.location.href=base_url+'Community/events';
								$("#toast").toast({
									type: 'success',
									message: 'Your Incubator has been successfully deleted'
								});
								 incubatorDatatable(base_path()+'admin/get_incubator_details','incubator_table');
								$('select').material_select();
							}
							else
							{
								$("#toast").toast({
									type: 'error_1',
									message: 'Error while processing'
								});
							}
						},
					});

			});


			
		  
		  
			  
            
    }
});
}

function mysignupDetailstable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'city': $("#city").val(), 'status':$("#status").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var signup = [];
        var signup_values = "";
        $(".signup_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".signup_bulk_action:checked").length;
				signup=[];
				$(".signup_bulk_action:checked").each(function() {
				signup.push($(this).val());
            });
          }
          else
          {
            signup=$('#signup_details_print').attr('data-multi_signup').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            signup = jQuery.grep(signup, function(value) {
              return value != remove_id;
            });
          }
            signup_values = signup.join(",");
            $('#signup_details_print').attr('data-multi_signup',signup_values);

          });
		},
	}); 
}

function myuservideoDetailstable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7, ] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function mytopvideoDetailstable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function mytopEngagedtable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function myactiveUserstable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function mytopCattable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3,] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function myinterviewtable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		var uservid = [];
        var uservide_values = "";
        $(".uservideo_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
				var total_checked = $(".uservideo_bulk_action:checked").length;
				uservid=[];
				$(".uservideo_bulk_action:checked").each(function() {
				uservid.push($(this).val());
            });
          }
          else
          {
            uservid=$('#user_video_print').attr('data-multi_uservideo').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            uservid = jQuery.grep(uservid, function(value) {
              return value != remove_id;
            });
          }
            uservide_values = uservid.join(",");
            $('#user_video_print').attr('data-multi_uservideo',uservide_values);

          });
		},
	}); 
}

function mypoststable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		
		},
	}); 
}

function myPodcasttable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		
		},
	}); 
}

function myQuizstable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_sms").val(), 'sms_start_date':$("#sms_start_date").val(),'sms_end_date':$("#sms_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		
		},
	}); 
}

function myChallengestable(url,id){
	$('#'+id).DataTable().destroy();
	table_alerts = $('#'+id).dataTable({ 
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6] }],
		"aoColumns": [ 
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"order": [0,'DESC'],
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		oLanguage:{
			sLengthMenu: "_MENU_",
		},
		"sDom": 'Rfrtlip',
		"iDisplayLength": 10,
		lengthMenu: [
			[ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
		"aaSorting": [[1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search': $("#search_challange").val(),'challange_nature': $("#challange_nature").val(), 'challange_start_date':$("#challange_start_date").val(),'challange_end_date':$("#challange_end_date").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		},
		"headerCallback": function(row, data, start, end, display) {
			var api = this.api(),
            data;
			// Remove the formatting to get integer data for summation
            var intVal = function(i) {
				return typeof i === 'string' ?
					i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
                };
			// Total over all pages
				totalKb = api
                    .column(1)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb).toFixed(2);

                //$('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");
		},
		"fnDrawCallback": function () {
			/*$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
			});*/
			
		
		},
	}); 
}
/* Notification Code */
(function($) {
	$.fn.toast = function(options)  {
   
		var settings = $.extend({
			type: 'normal',
			message:  null
		}, options);
    
		var item = $('<div class="notification ' + settings.type + '"><span>' + settings.message + '</span></div>');
		this.append($(item));
		$(item).animate({ "top": "12px" }, "fast");
		setInterval(function() {
			$(item).animate({ "top": "-400px" }, function() {
				$(item).remove();
			});
		}, 3000);
	}
  
	$(document).on('click', '.notification', function() {
		$(this).fadeOut(400, function(){
			$(this).remove();
		});
	});  
}(jQuery));