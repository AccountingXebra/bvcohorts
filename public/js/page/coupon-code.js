$(document).ready(function() {
	
	console.log("Coupon code pge");
	$("#add_admin_user_profile","#edit_admin_user_profile").validate({

				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					reg_mobile:{

						required:true,

						number:true,

						minlength:10,

						maxlength:10,

					},

					reg_password: {

						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},
				},

				messages:{

					reg_username:{

						required:"Contact Name is required",

					},

					reg_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					reg_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						minlength:"Please Enter Mobile No like (e.g 0123456789)",

						maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					reg_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},

					reg_admin_type:{

						required:"Please Allow any access to New User",

					},

				},
				errorPlacement: function(error, element) {
         
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
			        form.submit();
			      }   
				});

	
	
	
	
	$(".referal_name_div").hide();
	$("#coupon_nature").change(function(){  
		var nature_code = $("#coupon_nature").val();
		if(nature_code == 2){
			$(".referal_name_div").show();
		}else{
			$(".referal_name_div").hide();
		}
	});
	
		var bulk_coupon = [];
		$("input[id='coupon_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".coupon_bulk_action").prop('checked', true);
				$(".coupon_bulk_action:checked").each(function() {
					bulk_coupon.push($(this).val());
				});
				bulk_coupon = bulk_coupon.join(",");
				$('#deactive_multiple_profiles').attr('data-multi_profiles',bulk_coupon);
				$('#download_multiple_coupon').attr('data-multi_sub',bulk_coupon);
			}
			else {
				$(".coupon_bulk_action").prop('checked',false);
					bulk_coupon = [];
					$('#download_multiple_coupon').attr('data-multi_sub',0);
			}
		});
	
	 $("#create_coupon_code, #edit_coupon_code").validate({
				rules:{

					coupon_code:{

						required:true,

					},

					coupon_date:{

						required:true,

					},

					coupon_nature:{

						required:true,

					},

					referal_name:{
						
						required:function(){
							return $("#coupon_nature").val() == 2;
						},
					},

					coupon_desc: {

						required:true,
					},

					coupon_tg:{

						required:true,

					},
					
					coupon_validity:{

						required:true,

					},
					
					coupon_start_date:{

						required:true,

					},
					
					coupon_end_date:{

						required:true,

					},
					
					coupon_discount:{

						required:true,

					},
					
					
				},

				messages:{

					coupon_code:{

						required:"Code is required",

					},

					coupon_date:{

						required:"Date is required",

					},

					coupon_nature:{

						required:"Nature of code is required",

					},

					referal_name: {

						required:"Referal name required",

					},

					coupon_desc: {

						required:"Description is required.",

					},

					coupon_tg:{

						required:"Target group is required.",

					},
					
					coupon_validity:{

						required:"Validity is required.",

					},
					
					coupon_start_date:{

						required:"Start date is required.",

					},
					
					coupon_end_date:{

						required:"End date is required.",

					},
					
					coupon_discount:{

						required:"Discount is required.",

					},

				},
				errorPlacement: function(error, element) {
         
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
			        form.submit();
			    }   
			});
		
		/*Coupon code DataTable*/
		couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
		$('select').material_select();


		$('.coupon-search').on('change','#coupon_start_date,#coupon_end_date',function() { if($(this).val()!=''){ couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
 		$('select').material_select();} } );

		$('.coupon-search').on('change','#search_coupon',function() { couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
 		$('select').material_select(); } );

 		$('.coupon-search').on('change','#search_by_naturec',function() { couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
 		$('select').material_select(); } );
		
		$("body").on('click','#email_coupon_data',function(e) {

			var coupon_values =$('#download_multiple_coupon').attr('data-multi_sub');
			if(coupon_values == '' || coupon_values == '0')
			{
				Materialize.toast('Please select a record first', 2000,'red rounded');
			}
			else
			{

				var words = coupon_values.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
			    }else{
					// $.ajax({
					// 	url:base_url+'receipts_payments/email_multiple_sreceipts',
					// 	type:"POST",
					// 	data:{"sreceipt_values":sreceipt_values},
					// 	beforeSend: function(){
					//       Materialize.toast('Sending Email.......', 2000,'green rounded');
					//     },
					// 	success:function(res){
									
					// 				salesReceiptsDatatable(base_path()+'receipts_payments/get_sales_receipts/','srec_tbl');
					// 				$('select').material_select();
					// 				$('#deactive_multiple_sreceipts').attr('data-multi_srec',0);
									
					// 				$('#srec_tbl th').find('input[type="checkbox"]').each(function() {
					// 				    $(this).prop('checked', false);
					// 				  });
					// 				Materialize.toast('Sales Receipts has been emailed', 2000,'green rounded');
					// 		},
					// });	
					//email_coupon_deatils(coupon_values);
					email_coupon_deatils(coupon_values);
				}	
			}
		});

		$("body").on('click','#deactive_multiple_coupon',function(e) {
			var dnwl_ids = $('#download_multiple_coupon').attr('data-multi_sub');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select a record first', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
			    }else{

					$('#download_multiple_coupon').attr('data-multi_sub',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#coupon_code_table th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });

					// adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 			// 	$('select').material_select();	

					// window.location=base_url+'admin-dashboard/download-multiple-subscriber?ids='+dnwl_ids;
							
					// Materialize.toast('Subscriber Details has been downloaded', 2000,'green rounded');


					$.ajax({
		
							url:base_url+'admin_dashboard/deactive_coupon/',
		
							type:"POST",
		
							data:{"coupon_ids":dnwl_ids,},
		
							success:function(res){
									if(res == true)
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
	 									$('select').material_select();	
										Materialize.toast('Coupon has been successfully deactivated', 2000,'green rounded');
									}
									else
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
	 									$('select').material_select();	
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
								},
		
							});	

				}
			}
		});

		
		$("body").on('click','#active_coupon',function(e) {
		
			var coupon_ids = $(this).data('profileid');

			$.ajax({

				url:base_url+'admin_dashboard/active_coupon/' + coupon_ids,

				type:"POST",

				data:{"coupon_ids":coupon_ids,},

				success:function(res){
						if(res == true)
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
								$('select').material_select();	
							Materialize.toast('Coupon has been successfully Activated', 2000,'green rounded');
						}
						else
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
								$('select').material_select();	
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}

					},

			});										
		});


		$("body").on('click','#deactive_coupon',function(e) {
			var coupon_ids = $(this).data('profileid');

			$.ajax({

				url:base_url+'admin_dashboard/deactive_coupon/' + coupon_ids,

				type:"POST",

				data:{"coupon_ids":coupon_ids,},

				success:function(res){
						if(res == true)
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
								$('select').material_select();	
							Materialize.toast('Coupon has been successfully Deactivated', 2000,'green rounded');
						}
						else
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
								$('select').material_select();	
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}

					},

			});										
		});

		$("body").on('click','#download_multiple_coupon',function(e) {
		
			var dnwl_ids = $('#download_multiple_coupon').attr('data-multi_sub');
			//alert(dnwl_ids);
			if(dnwl_ids == '' || dnwl_ids == '0')
			{
	
				Materialize.toast('Please select a record first', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
			    }else{
					$('#download_multiple_coupon').attr('data-multi_sub',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#coupon_code_table th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });

					couponCodeDatatable(base_path()+'admin-dashboard/get-coupon-details/','coupon_code_table');
	 				$('select').material_select();	

					window.location=base_url+'admin-dashboard/download-multiple-coupon?ids='+dnwl_ids;
							
					Materialize.toast('Coupon Details has been exported', 2000,'green rounded');
				}
			}
		});



});


function couponCodeDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#download_multiple_coupon').attr('data-multi_sub',0);
	
	table_coupon_code = $('#'+id).dataTable({	
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6,7,8,9,10,11] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },

	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10,20, 30, 50, -1 ],
            [ 'Show 10','Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search_by_naturec':$('#search_by_naturec').val(),'search_coupon':$('#search_coupon').val(),'coupon_start_date':$('#coupon_start_date').val(),'coupon_end_date':$('#coupon_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[10]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
			$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			
			
			var coupon = [];
				var coupon_values = "";
				$(".coupon_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".coupon_bulk_action:checked").length;
						 
						  coupon=[];
						  $(".coupon_bulk_action:checked").each(function() {
			
							coupon.push($(this).val());
						}); 
					}
					else
					{
						coupon=$('#download_multiple_coupon').attr('data-multi_sub').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						coupon = jQuery.grep(coupon, function(value) {
						  return value != remove_id;
						});
					}
						coupon_values = coupon.join(",");
						$('#download_multiple_coupon').attr('data-multi_sub',coupon_values);
					});
					
	 },
  }); 
}

function email_coupon_deatils(id){
  //  $.ajax({
  //   type: "POST",
  //    url:base_url+'receipts_payments/email_sales_receipt/'+$id,
  //   //data:{'cmp_id':$id},
  //   beforeSend: function(){
  //     Materialize.toast('Sending Email.......', 2000,'green rounded');
  //   },
  //   success: function(data){
  //     Materialize.toast('Mail has been sent!', 2000,'green rounded');
  //   },
  // });

	//var gst_id = $id;
	$('#send_email_coupon_modal').modal('open');
	$('#send_email_coupon_modal #coupon_id').val(id);

}