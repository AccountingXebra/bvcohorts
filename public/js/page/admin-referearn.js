	$(document).ready(function() {
		console.log("Referral Code List");
		refearnDatatable(base_path()+'admin-dashboard/get_refearn_details/','refearn_table');
		$('select').material_select();
		
		var bulk_activity = [];
		$("input[id='refearn_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".refearn_bulk_action").prop('checked', true);
				$(".refearn_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_refearn').attr('data-multi_ref',bulk_activity);
			}
			else {
				$(".refearn_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_refearn').attr('data-multi_ref',0);
			}
		});
	});
	
	function refearnDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#download_multiple_refearn').attr('data-multi_ref',0);
	
	table_refearn_code = $('#'+id).dataTable({	
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10,20, 30, 50, -1 ],
            [ 'Show 10','Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			"data":'',
			"data": {'search_refer':$('#search_refer').val(),'refer_start_date':$('#refer_start_date').val(),'refer_end_date':$('#refer_end_date').val()},
		},
		"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[10]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		},
		"fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_refearn_code.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
			var refearn = [];
				var refearn_values = "";
				$(".refearn_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".refearn_bulk_action:checked").length;
						 
						  refearn=[];
						  $(".refearn_bulk_action:checked").each(function() {
			
							refearn.push($(this).val());
						}); 
					}
					else
					{
						refearn=$('#download_multiple_refearn').attr('data-multi_ref').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						refearn = jQuery.grep(refearn, function(value) {
						  return value != remove_id;
						});
					}
						refearn_values = refearn.join(",");
						$('#download_multiple_refearn').attr('data-multi_ref',refearn_values);
					});
			},
		}); 
	}