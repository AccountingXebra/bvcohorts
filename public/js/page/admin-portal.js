$(document).ready(function() {
	//alert('Enter in JS....!');
	

	$("#create_challange_form").submit(function(e){
		e.preventDefault();
		}).validate({
			rules:{
				challange_date:{
					required:true,
				},
				challange_name:{
					required:true,
				},
				challange_image:{
					required:true,
				},
				challange_desc:{
					required:true,
				},
				last_day_entry:{
					required:true,
				},
				location:{
					required:true,
				},
				url:{
					required:true,
				}
			},

			messages:{
				challange_date:{
					required:"Date is required",
				},
				challange_name:{
					required:"Name is required",
				},
				challange_image:{
					required:"Image id required",
				},
				challange_desc:{
					required:"Description is required",
				},
				last_day_entry:{
					required:"Last Day of entry is required",
				},
				location:{
					required:"Location is required",
				},
				url:{
					required:"Website URL is required",
				}
			},
			submitHandler: function(form) {
				form.submit();
		    }
		});

	$("#edit_challange_form").submit(function(e){
		e.preventDefault();
		}).validate({
			rules:{
				challange_date:{
					required:true,
				},
				challange_name:{
					required:true,
				},
				
				challange_desc:{
					required:true,
				},
				last_day_entry:{
					required:true,
				},
				location:{
					required:true,
				},
				url:{
					required:true,
				}
			},

			messages:{
				challange_date:{
					required:"Date is required",
				},
				challange_name:{
					required:"Name is required",
				},
				
				challange_desc:{
					required:"Description is required",
				},
				last_day_entry:{
					required:"Last Day of entry is required",
				},
				location:{
					required:"Location is required",
				},
				url:{
					required:"Website URL is required",
				}
			},
			submitHandler: function(form) {
				form.submit();
		    }
		});
	
	$("#create_newsletter").submit(function(e){
			e.preventDefault();

		}).validate({
			rules:{
				newsletter_name:{
					required:true,
				},
				newsletter_url:{
					//required:true,
				},
				newsletter_year:{
					required:true,
				},
				featured_image:{
					required:true,
				},
				newsletter_file:{
					required:true,
				},
				newsletter_contain:{
					required:true,
				}
			},

			messages:{
				newsletter_name:{
					required:"Name is required",
				},
				newsletter_url:{
					required:"Url is required",
				},
				newsletter_year:{
					required:"Year is required",
				},
				newsletter_file:{
					required:"File is required",
				},
				featured_image:{
					required:"Image is required",
				},
				newsletter_contain:{
					required:"Contain url is required",
				}
			},
			submitHandler: function(form) {
				form.submit();
		    }
		});

	$("#edit_newsletter").submit(function(e){
			e.preventDefault();
		}).validate({
			rules:{
				newsletter_name:{
					required:true,
				},
				newsletter_url:{
					required:true,
				},
				newsletter_year:{
					required:true,
				},
				
				newsletter_contain:{
					required:true,
				}
			},

			messages:{
				newsletter_name:{
					required:"Name is required",
				},
				newsletter_url:{
					required:"Url is required",
				},
				newsletter_year:{
					required:"Year is required",
				},
				featured_image:{
					required:"Image is required",
				},
				newsletter_contain:{
					required:"Contain url is required",
				}
			},
			submitHandler: function(form) {
				form.submit();
		    }
		});
	
	$("#create_post").submit(function(e){
			e.preventDefault();
		}).validate({
			rules:{
				post_category:{
					required:true,
				},
				post_title:{
					required:true,
				},
				post_url:{
					required:true,
				},
				post_content:{
					//required:true,
				},
				featured_img:{
					required:true,
				},
				post_excerpt:{
					required:true,
				},
			},

			messages:{
				post_category:{
					required:"Category is required",
				},
				post_title:{
					required:"Post Title is required",
				},
				post_url:{
					required:"Post url is required",
				},
				post_content:{
					//required:"Post Content is required",
				},
				featured_img:{
					required:"Featured image is required",
				},
				post_excerpt:{
					required:"Excerpt is required",
				},
			},
			submitHandler: function(form) {
				form.submit();
		    }

		});
	
	$("#edit_post").submit(function(e){
			e.preventDefault();
		}).validate({
			rules:{
				post_category:{
					required:true,
				},
				post_title:{
					required:true,
				},
				post_content:{
					//required:true,
				},
				featured_img:{
					required:true,
				},
				post_excerpt:{
					required:true,
				},
			},

			messages:{
				post_category:{
					required:"Category is required",
				},
				post_title:{
					required:"Post Title is required",
				},
				post_content:{
					//required:"Post Content is required",
				},
				featured_img:{
					required:"Featured image is required",
				},
				post_excerpt:{
					required:"Excerpt is required",
				},
			},
			submitHandler: function(form) {
				form.submit();
		    }

		});
	
	$(function() {
	  $("#super_admin_login").attr('autocomplete', 'off');
	  $("#super_admin_login").validate({
		  rules: {
			  ad_email:{
				required:true,
				email:true,
			  },
			  password:{ 
				required:true,
				minlength:8,
			  },
		  },
		  messages:{
			  ad_email:{

				required:"User Name is required",

			  },
			  password:{
				required:"Password is required",
				//minlength:"Please Enter Minimum 8 characters long password",
			  },
			},
		
		  submitHandler: function(form) {
			form.submit();
		  }
		});
		
		
		$("#admin_change_password").submit(function(e){
			e.preventDefault();
		}).validate({

			rules:{

				old_password:{
					required:true,
					minlength:8,
				},

				new_password:{
					required:true,
					minlength:8,
				},
				confirm_password:{
					required:true,
					equalTo: '#new_password',
				},
			},

			messages:{

				old_password:{
					required:"Old Password is required",
					minlength:"Please Enter Minimum 8 characters long password",
				},

				new_password:{
					required:"New Password is required",
					minlength:"Please Enter Minimum 8 characters long password",
				},

				confirm_password:{
					required:"Confirm your new password",
					equalTo:"Your password doesn't match",
				},
			},
			submitHandler: function(form) {
    
		        form.submit();
		       
		    }

		});
		
		$("#create_coworking_space").submit(function(e){
			e.preventDefault();
		}).validate({

			rules:{
				co_name:{
					required:true,
				},
				co_address:{
					required:true,
				},
				co_country:{
					required:true,
				},
				co_state:{
					required:true,
				},
				co_city:{
					required:true,
				},
				co_emailid:{
					required:true,
				},
				co_contact:{
					required:true,
				},
			},

			messages:{
				co_name:{
					required:"Co-working space name is required",
				},
				co_address:{
					required:"Address is required",
				},
				co_country:{
					required:"Country is required",
				},
				co_state:{
					required:"State is required",
				},
				co_city:{
					required:"City is required",
				},
				co_emailid:{
					required:"Email id is required",
				},
				co_contact:{
					required:"Contact number is required",
				},
			},
			submitHandler: function(form) {
    
		        form.submit();
		       
		    }

		});

	  });


	adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select();
	
	coworkingDatatable(base_path()+'admin-dashboard/get_coworking_details','cowork_table');
	$('select').material_select();
	
	capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
 	$('select').material_select();
	
	blogPostDatatable(base_path()+'admin-dashboard/get-post-details/','post_table');
	$('select').material_select();
	
	blogDigestDatatable(base_path()+'admin-dashboard/get-digest-details/','digest_table');
	$('select').material_select();
	
	surveyDatatable(base_path()+'admin-dashboard/get-survey-details/','survey_table');
	$('select').material_select();
	
	newsletterDatatable(base_path()+'admin-dashboard/get-newsletter-details/','newsletter_table');
	$('select').material_select();
	
	challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
	$('select').material_select();
	
	adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
 	$('select').material_select();
	
	interviewDatatable(base_path()+'admin-dashboard/get-interview-details/','interview_table');
 	$('select').material_select();

	$("#search_challange").on('keyup',function() { if($(this).val()!=''){challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
	$('select').material_select();} } );
	$("#challange_nature").on('change',function() { if($(this).val()!=''){challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
	$('select').material_select();} } );
   
	$("#challange_start_date").on('change',function() { if($(this).val()!=''){challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
	$('select').material_select();} } );

	$("#challange_end_date").on('change',function() { if($(this).val()!=''){challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
	$('select').material_select();} } );

    $("#search_newsletter").on('keyup',function() { if($(this).val()!=''){newsletterDatatable(base_path()+'admin-dashboard/get-newsletter-details/','newsletter_table');
	$('select').material_select();} } );
   
	$("#news_start_date").on('change',function() { if($(this).val()!=''){ newsletterDatatable(base_path()+'admin-dashboard/get-newsletter-details/','newsletter_table');
	$('select').material_select();} } );

	$("#news_end_date").on('change',function() { if($(this).val()!=''){newsletterDatatable(base_path()+'admin-dashboard/get-newsletter-details/','newsletter_table');
	$('select').material_select();} } );




	$("#search_feedback").on('keyup',function() { if($(this).val()!=''){ blogPostDatatable(base_path()+'admin-dashboard/get-post-details/','post_table');
	$('select').material_select();} } );
   
	$("#feedback_start_date").on('change',function() { if($(this).val()!=''){ blogPostDatatable(base_path()+'admin-dashboard/get-post-details/','post_table');
	$('select').material_select();} } );

	$("#feedback_end_date").on('change',function() { if($(this).val()!=''){ blogPostDatatable(base_path()+'admin-dashboard/get-post-details/','post_table');
	$('select').material_select();} } );

	$("#search_survey").on('keyup',function() { if($(this).val()!=''){ surveyDatatable(base_path()+'admin-dashboard/get-survey-details/','survey_table');
	$('select').material_select();} } );
   
	$("#survey_start_date").on('change',function() { if($(this).val()!=''){ surveyDatatable(base_path()+'admin-dashboard/get-survey-details/','survey_table');
	$('select').material_select();} } );

	$("#survey_end_date").on('change',function() { if($(this).val()!=''){ surveyDatatable(base_path()+'admin-dashboard/get-survey-details/','survey_table');
	$('select').material_select();} } );
 	
 	$("#search_sub").on('keyup',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
 		$('select').material_select();} } );


 	$("#search_bycompany").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );

	$("#search_bybuzz").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );

	$("#search_by_city").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );

	$("#search_by_reg_date").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );
   
   $("#sub_start_date").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );

	$("#sub_end_date").on('change',function() { if($(this).val()!=''){ capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
	$('select').material_select();} } );

	$("#cowork_name").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'admin-dashboard/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#cowork_country").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'admin-dashboard/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#cowork_city").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'admin-dashboard/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#incub_name").on('change',function() { if($(this).val()!=''){ incubatorDatatable(base_path()+'admin-dashboard/get_incubator_details','incubator_table');
	$('select').material_select();
	} } );

	$("#incub_country").on('change',function() { if($(this).val()!=''){ incubatorDatatable(base_path()+'admin-dashboard/get_incubator_details','incubator_table');
	$('select').material_select();
	} } );

	$("#incub_city").on('change',function() { if($(this).val()!=''){ cincubatorDatatable(base_path()+'admin-dashboard/get_incubator_details','incubator_table');
	$('select').material_select();
	} } );
	
	incubatorDatatable(base_path()+'admin-dashboard/get_incubator_details','incubator_table');
	$('select').material_select();

 	$('.sub-search').on('change','#sub_start_date,#sub_end_date',function() { if($(this).val()!=''){ adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select();} } );

 	$('.sub-search').on('change','#search_bycountry_sub',function() { adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select(); } );
 	$('.sub-search').on('change','#search_by_city_sub',function() { adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select(); } );
 	$('.sub-search').on('change','#search_sub',function() { adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select(); } );

 	$('.sub-search').on('change','#search_by_reg_date',function() { adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
 	$('select').material_select(); } );



 	$('.user-search').on('change','#users_start_date,#users_end_date',function() { if($(this).val()!=''){  adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
 	$('select').material_select();} } );

 	$('.user-search').on('change','#search_bycountry_users',function() {   adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
 	$('select').material_select(); } );
 	$('.user-search').on('change','#search_by_city_users',function() {   adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
 	$('select').material_select(); } );
 	$('.user-search').on('change','#search_users',function() {   adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
 	$('select').material_select(); } );

		var bulk_user = [];
			$("body").on("click","input[id='users_bulk']",function(){
			if($(this).is(':checked')) {
				$(".users_bulk_action").prop('checked', true);
				bulk_user = [];
				$(".users_bulk_action:checked").each(function() {
						bulk_user.push($(this).val());
					});
					bulk_user = bulk_user.join(",");
					$('#download_multiple_users').attr('data-multi_user',bulk_user);
				}
				else {
					$('#my-users-admin tr').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
					});
					bulk_user = [];
					$('#download_multiple_users').attr('data-multi_user',0);
				}
		});
		
		$("body").on('click','#download_multiple_users',function(e) {

			var dnwl_ids = $('#download_multiple_users').attr('data-multi_user');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			    }else{
					$('#download_multiple_users').attr('data-multi_user',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#my-users-admin th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });
					adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
					$('select').material_select();
					$("#users_bulk").prop('checked', false);
					window.location=base_url+'admin-dashboard/download-multiple-users?ids='+dnwl_ids;
					Materialize.toast('Users Details has been exported', 2000,'green rounded');
				}
			}
		});
		
		var bulk_chal = [];
			$("body").on("click","input[id='challange_bulk']",function(){
			if($(this).is(':checked')) {
				$(".challange_bulk_action").prop('checked', true);
				bulk_chal = [];
				$(".challange_bulk_action:checked").each(function() {
						bulk_chal.push($(this).val());
					});
					bulk_chal = bulk_chal.join(",");
					$('#download_multiple_challange').attr('data-multi_part',bulk_chal);
				}
				else {
					$('#challange_table tr').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
					});
					bulk_chal = [];
					$('#download_multiple_challange').attr('data-multi_part',0);
				}
		});
		
		$("body").on('click','#download_multiple_challange',function(e) {

			var dnwl_ids = $('#download_multiple_challange').attr('data-multi_part');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			    }else{
					$('#download_multiple_challange').attr('data-multi_sub',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#challange_table th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });
					challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
					$('select').material_select();
					$("#challange_bulk").prop('checked', false);
					window.location=base_url+'admin-dashboard/download-multiple-challenge?ids='+dnwl_ids;
					Materialize.toast('Challenge Details has been exported', 2000,'green rounded');
				}
			}
		});

	 	var bulk_sub = [];
			$("body").on("click","input[id='subscriber_bulk']",function(){
			if($(this).is(':checked')) {
				$(".subscriber_bulk_action").prop('checked', true);
				bulk_sub = [];
				$(".subscriber_bulk_action:checked").each(function() {
						bulk_sub.push($(this).val());
					});
					bulk_sub = bulk_sub.join(",");
					$('#download_multiple_subscriber').attr('data-multi_sub',bulk_sub);
				}
				else {
					$('#my-revenue-admin tr').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
					});
					bulk_sub = [];
					$('#download_multiple_subscriber').attr('data-multi_sub',0);
				}
		});
		
		$("body").on('click','#download_multiple_subscriber',function(e) {

			var dnwl_ids = $('#download_multiple_subscriber').attr('data-multi_sub');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			    }else{
					$('#download_multiple_subscriber').attr('data-multi_sub',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#my-subscriber-admin th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });
					adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 				$('select').material_select();	

					window.location=base_url+'admin-dashboard/download-multiple-subscriber?ids='+dnwl_ids;
							
					Materialize.toast('Subscriber Details has been exported', 2000,'green rounded');
				}
			}
		});

		$("body").on('click','#deactive_multiple_subscriber',function(e) {

			var dnwl_ids = $('#download_multiple_subscriber').attr('data-multi_sub');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			    }else{

					$('#download_multiple_subscriber').attr('data-multi_sub',0);
					//$('#company-profile').find('input:checkbox').attr('checked',false);
					$('#my-subscriber-admin th').find('input[type="checkbox"]').each(function() {
					    $(this).prop('checked', false);
					  });

					// adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 			// 	$('select').material_select();	

					// window.location=base_url+'admin-dashboard/download-multiple-subscriber?ids='+dnwl_ids;
							
					// Materialize.toast('Subscriber Details has been downloaded', 2000,'green rounded');


					$.ajax({
		
							url:base_url+'admin_dashboard/deactive_reg_user/',
		
							type:"POST",
		
							data:{"reg_ids":dnwl_ids,},
		
							success:function(res){
									if(res == true)
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 									$('select').material_select();	
										Materialize.toast('Subscriber has been successfully deactivated', 2000,'green rounded');
									}
									else
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 									$('select').material_select();	
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
								},
		
							});	

				}
			}
		});
		
		$("body").on('click','#active_subscriber_profile',function(e) {
		
			var sub_id = $(this).data('profileid');

			$.ajax({

				url:base_url+'admin_dashboard/active_reg_user/' + sub_id,

				type:"POST",

				data:{"reg_ids":sub_id,},

				success:function(res){
						if(res == true)
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
								$('select').material_select();	
							Materialize.toast('Subscriber has been successfully Activated', 2000,'green rounded');
						}
						else
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
								$('select').material_select();	
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}

					},

			});										
		}); 

		$("body").on('click','#deactive_subscriber_profile',function(e) {
		
			var sub_id = $(this).data('profileid');

			$.ajax({

				url:base_url+'admin_dashboard/deactive_reg_user/' + sub_id,

				type:"POST",

				data:{"reg_ids":sub_id,},

				success:function(res){
						if(res == true)
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
								$('select').material_select();	
							Materialize.toast('Subscriber has been successfully Deactivated', 2000,'green rounded');
						}
						else
						{
							//window.location.href=base_url+'profile/manage_personal_profile';
							adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
								$('select').material_select();	
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}

					},

			});										
		});

		$("#send_email_sub_modal").on('click', function(e) {

			var dnwl_ids = $('#download_multiple_subscriber').attr('data-multi_sub');
			if(dnwl_ids == '' || dnwl_ids == '0')
			{

				Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			}
			else
			{
				var words = dnwl_ids.split(",");
			    if(words.length < 2){
			     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
			    }else{
					// $('#download_multiple_subscriber').attr('data-multi_sub',0);
					// //$('#company-profile').find('input:checkbox').attr('checked',false);
					// $('#my-subscriber-admin th').find('input[type="checkbox"]').each(function() {
					//     $(this).prop('checked', false);
					//   });
					// adminSubDatatable(base_path()+'admin-dashboard/get-subscriber-details/','my-subscriber-admin');
	 			// 	$('select').material_select();	

					// window.location=base_url+'admin-dashboard/download-multiple-subscriber?ids='+dnwl_ids;
							
					// Materialize.toast('Subscriber Details has been downloaded', 2000,'green rounded');
					email_subscriber(dnwl_ids);
				}
			}
			
		});
		
		var bulk_di = [];
			$("body").on("click","input[id='digest_bulk']",function(){
			if($(this).is(':checked')) {
				$(".digest_bulk_action").prop('checked', true);
				bulk_di = [];
				$(".digest_bulk_action:checked").each(function() {
						bulk_di.push($(this).val());
					});
					bulk_di = bulk_di.join(",");
					$('#print_multiple_digest').attr('data-multi_di',bulk_di);
				}
				else {
					$('#digest_table tr').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
					});
					bulk_di = [];
					$('#print_multiple_digest').attr('data-multi_di',0);
				}
		});
		
			var bulk_post = [];
			$("body").on("click","input[id='post_bulk']",function(){
			if($(this).is(':checked')) {
				$(".post_bulk_action").prop('checked', true);
				bulk_post = [];
				$(".post_bulk_action:checked").each(function() {
						bulk_post.push($(this).val());
					});
					bulk_post = bulk_post.join(",");
					$('#print_multiple_post').attr('data-multi_po',bulk_post);
				}
				else {
					$('#post_table tr').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
					});
					bulk_post = [];
					$('#print_multiple_post').attr('data-multi_po',0);
				}
			});
		
		$("body").on('click','#print_multiple_digest',function(e) {

		var dnwl_ids = $('#print_multiple_digest').attr('data-multi_di');
		if(dnwl_ids == '' || dnwl_ids == '0'){
			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#print_multiple_digest').attr('data-multi_di',0);
				$('#digest_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				blogDigestDatatable(base_path()+'admin-dashboard/get-digest-details/','digest_table');
				$('select').material_select();
				$("#digest_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/print-multiple-digest?ids='+dnwl_ids;
				Materialize.toast('Blog digest subscribers details has been print', 2000,'green rounded');
			}
		}
		});
		
		$("body").on('click','#print_multiple_post',function(e) {

		var dnwl_ids = $('#print_multiple_post').attr('data-multi_po');
		if(dnwl_ids == '' || dnwl_ids == '0'){
			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#print_multiple_post').attr('data-multi_po',0);
				$('#post_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				blogPostDatatable(base_path()+'admin-dashboard/get-post-details/','post_table');
				$('select').material_select();
				$("#post_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/print-multiple-post?ids='+dnwl_ids;
				Materialize.toast('Blog post details has been print', 2000,'green rounded');
			}
		}
		});

		 function email_subscriber($id){

		  var subscriber_id = $id;
		  $('#send_email_subscriber_modal').modal('open');
		  $('#send_email_subscriber_modal #subscriber_id').val(subscriber_id);

		}
		
	adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select();

 	$('.rev-search').on('change','#search_by_package',function() { adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select(); } );
 	$('.rev-search').on('change','#search_by_subsc',function() { adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select(); } );
 	$('.rev-search').on('change','#rev_start_date,#rev_end_date',function() { if($(this).val()!=''){ adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select();} } );
	
 	$('.rev-search').on('change','#search_bycountry',function() { adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select(); } );
 	$('.rev-search').on('change','#search_by_city',function() { adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select(); } );
 	$('.rev-search').on('change','#search_by_coupon',function() { adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 	$('select').material_select(); } );

 	$('.ctds-search').on('keyup','#search_ctds',function(){ 
		ctdsDatatable(base_path()+'tax_summary/get_ctds_list/','my-customer-tds');
		$('select').material_select();
	});



	var bulk_rev = [];
		$("body").on("click","input[id='revenue_bulk']",function(){
		if($(this).is(':checked')) {
			$(".revenue_bulk_action").prop('checked', true);
			bulk_rev = [];
			$(".revenue_bulk_action:checked").each(function() {
					bulk_rev.push($(this).val());
				});
				bulk_rev = bulk_rev.join(",");
				$('#download_multiple_rev').attr('data-multi_rev',bulk_rev);
			}
			else {
				$('#my-revenue-admin tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_rev = [];
				$('#download_multiple_rev').attr('data-multi_rev',0);
			}
	});
	
	$("body").on('click','#download_multiple_rev',function(e) {

		var dnwl_ids = $('#download_multiple_rev').attr('data-multi_rev');
		//alert(dnwl_ids);
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#download_multiple_rev').attr('data-multi_rev',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#my-revenue-admin th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 				$('select').material_select();	
				$("#revenue_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/download-multiple-rev?ids='+dnwl_ids;
						
				Materialize.toast('Revenue Details has been downloaded', 2000,'green rounded');
			}
		}
	});


	$("body").on('click','#download_multiple_rev_invoice',function(e) {

		var dnwl_ids = $('#download_multiple_rev').attr('data-multi_rev');
		//alert(dnwl_ids);
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 1){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#download_multiple_rev').attr('data-multi_rev',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#my-revenue-admin th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				adminRevDatatable(base_path()+'admin-dashboard/get-rev-details/','my-revenue-admin');
 				$('select').material_select();	

				window.location=base_url+'admin-dashboard/download_multiple_rev_invoice?ids='+dnwl_ids;
						
				Materialize.toast('Revenue Details has been downloaded', 2000,'green rounded');
			}
		}
	});

	var bulk_partner = [];
		$("body").on("click","input[id='revenue_bulk']",function(){
		if($(this).is(':checked')) {
			$(".capartner_bulk_action").prop('checked', true);
			bulk_partner = [];
			$(".capartner_bulk_action:checked").each(function() {
					bulk_partner.push($(this).val());
				});
				bulk_partner = bulk_partner.join(",");
				$('#download_multiple_partner').attr('data-multi_part',bulk_partner);
			}
			else {
				$('#ca_partner_table th').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_partner = [];
				$('#download_multiple_partner').attr('data-multi_part',0);
			}
	});
	
	$("body").on('click','#download_multiple_partner',function(e) {

		var dnwl_ids = $('#download_multiple_partner').attr('data-multi_part');
		if(dnwl_ids == '' || dnwl_ids == '0'){
			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#download_multiple_partner').attr('data-multi_part',0);
				$('#ca_partner_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				capartnerDatatable(base_path()+'admin-dashboard/get-capartner-details/','ca_partner_table');
				$('select').material_select();
				$("#partner_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/download-multiple-capartner?ids='+dnwl_ids;
				Materialize.toast('CA Partner details has been exported', 2000,'green rounded');
			}
		}
	});
	
	$("body").on('click','#deactive_ca_partner',function(e) {
		console.log('Hello');
	});

	var bulk_rev = [];
	$("body").on("click","input[id='survey_bulk']",function(){
		if($(this).is(':checked')) {
			$(".survey_bulk_action").prop('checked', true);
			bulk_rev = [];
			$(".survey_bulk_action:checked").each(function() {
				bulk_rev.push($(this).val());
			});
			bulk_rev = bulk_rev.join(",");
			$('#print_multiple_survey').attr('data-multi_di',bulk_rev);
		}else {
			$('#survey_table tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
			});
			bulk_rev = [];
			$('#print_multiple_survey').attr('data-multi_di',0);
		}
	});
	
	$("body").on('click','#print_multiple_survey',function(e) {

		var dnwl_ids = $('#print_multiple_survey').attr('data-multi_di');
		if(dnwl_ids == '' || dnwl_ids == '0'){
			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#print_multiple_survey').attr('data-multi_di',0);
				$('#survey_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				surveyDatatable(base_path()+'admin-dashboard/get-survey-details/','survey_table');
				$('select').material_select();
				$("#survey_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/print-multiple-survey?ids='+dnwl_ids;
				Materialize.toast('Survey details has been print', 2000,'green rounded');
			}
		}
	});
	
	var bulk_news = [];
	$("body").on("click","input[id='news_bulk']",function(){
		if($(this).is(':checked')) {
			$(".newletter_bulk_action").prop('checked', true);
			bulk_news = [];
			$(".newletter_bulk_action:checked").each(function() {
				bulk_news.push($(this).val());
			});
			bulk_news = bulk_news.join(",");
			$('#print_multiple_newsletter').attr('data-multi_news',bulk_news);
		}else {
			$('#newsletter_table tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
			});
			bulk_news = [];
			$('#print_multiple_newsletter').attr('data-multi_news',0);
		}
	});
	
	$("body").on('click','#print_multiple_newsletter',function(e) {

		var dnwl_ids = $('#print_multiple_newsletter').attr('data-multi_news');
		if(dnwl_ids == '' || dnwl_ids == '0'){
			Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		}
		else{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Please select two or more rows for bulk action', 2000,'red rounded');
		    }else{
				$('#print_multiple_newsletter').attr('data-multi_news',0);
				$('#newsletter_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				newsletterDatatable(base_path()+'admin-dashboard/get-newsletter-details/','newsletter_table');
				$('select').material_select();
				$("#news_bulk").prop('checked', false);
				window.location=base_url+'admin-dashboard/print-multiple-newsletter?ids='+dnwl_ids;
				Materialize.toast('Newsletter details has been print', 2000,'green rounded');
			}
		}
	});	

	});



function adminRevDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#download_multiple_ctds').attr('data-multi_ctds',0);
	
	table_sales_rev = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6, 7, 8, 9, 10, 11] }],
	"aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search_bycountry':$('#search_bycountry').val(),'search_by_city':$('#search_by_city').val(),'search_by_coupon':$('#search_by_coupon').val(),'search_by_package':$('#search_by_package').val(),'search_by_subsc':$('#search_by_subsc').val(),'rev_start_date':$('#rev_start_date').val(),'rev_end_date':$('#rev_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                totalAmt = api
                    .column(9)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                     totalAmt=parseFloat(totalAmt).toFixed(2);
                $('tr:eq(1) th:eq(8)', api.table().header()).html("₹" +add_commas(totalAmt));	
				$('tr:eq(1) th:eq(9)', api.table().header()).html("₹");
				$('tr:eq(1) th:eq(10)', api.table().header()).html("₹");
				$('tr:eq(1) th:eq(11)', api.table().header()).html("₹");
                
            },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_rev.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
			var rev = [];
				var rev_values = "";
				$(".revenue_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".revenue_bulk_action:checked").length;
						 
						  rev=[];
						  $(".revenue_bulk_action:checked").each(function() {
			
							rev.push($(this).val());
						}); 
					}
					else
					{
						rev=$('#download_multiple_rev').attr('data-multi_rev').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						rev = jQuery.grep(rev, function(value) {
						  return value != remove_id;
						});
					}
						rev_values = rev.join(",");
						$('#download_multiple_rev').attr('data-multi_rev',rev_values);
					});
					
	 },
  }); 
}

function incubatorDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
  
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{ 'name':$('#incub_name').val(), 'country':$('#incub_country').val(), 'city':$('#incub_city').val()},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      	//  if($(aData[6]+' div').hasClass('deactive_record')){
      	//  $('td', nRow).parent('tr').addClass('deactivate-row');
      	// } 
      	$('td:first-child', nRow).addClass('bulk');
		$('td:last-child', nRow).addClass('action-tab');
		$(nRow).addClass("label-warning");	
		$("#fixedHeader").removeClass('sticky');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

        	$('.deactive_incub').on('click',function(){

				var incub_id = $(this).data('cd_id');

				 $.ajax({

					url:base_url+'admin_dashboard/delete_incubation',

					type:"POST",

					data:{"incube_id":incub_id,},

					success:function(res){
							if(res == true)
							{
								//window.location.href=base_url+'Community/events';
								Materialize.toast('Your Incubator has been successfully deleted', 2000,'green rounded');
								 incubatorDatatable(base_path()+'admin-dashboard/get_incubator_details','incubator_table');
	$('select').material_select();
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});

			});


			
		  
		  
			  
            
    }
});
}

function coworkingDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
  
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{'name':$('#cowork_name').val(), 'state':$('#cowork_country').val(), 'city':$('#cowork_city').val()},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

        	$('.deactive_space').on('click',function(){

				var workspace_id = $(this).data('cd_id');

				 $.ajax({

					url:base_url+'admin_dashboard/delete_workspace',

					type:"POST",

					data:{"workspace_id":workspace_id,},

					success:function(res){
							if(res == true)
							{
								//window.location.href=base_url+'Community/events';
								Materialize.toast('Your Workspace has been successfully deleted', 2000,'green rounded');
								coworkingDatatable(base_path()+'admin-dashboard/get_coworking_details','cowork_table');
	$('select').material_select();
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});

			});

			
		  
			  
            
    }
});
}


/*...............................................*/
/*...............................................*/
/*...............................................*/
/*...............................................*/
/*...............................................*/
/*...............................................*/

function capartnerDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3, 4, 5, 6, 7, 8, 9,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search':$('#search_sub').val(),'search_bycompany':$('#search_bycompany').val(),'search_bybuzz':$('#search_bybuzz').val(),'search_by_city':$('#search_by_city').val(),'search_by_reg_date':$('#search_by_reg_date').val(),'sub_start_date':$('#sub_start_date').val(),'sub_end_date':$('#sub_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[9]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     /*"headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                     totalAmt=parseFloat(totalAmt).toFixed(2);
					//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));


              
               
                
            },*/
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = this.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
			var sub = [];
				var sub_values = "";
				$(".capartner_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".capartner_bulk_action:checked").length;
						 
						  sub=[];
						  $(".capartner_bulk_action:checked").each(function() {
			
							sub.push($(this).val());
						}); 
					}
					else
					{
						sub = $('#download_multiple_partner').attr('data-multi_part').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sub = jQuery.grep(sub, function(value) {
						  return value != remove_id;
						});
					}
						sub_values = sub.join(",");
						$('#download_multiple_partner').attr('data-multi_part',sub_values);
					});
					
	 },
  }); 
}

function adminSubDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search':$('#search_sub').val(),'sub_start_date':$('#sub_start_date').val(),'sub_end_date':$('#sub_end_date').val(),'search_by_city_sub':$('#search_by_city_sub').val(),'search_bycountry_sub':$('#search_bycountry_sub').val(),'search_by_days':$('#search_by_reg_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                     totalAmt=parseFloat(totalAmt).toFixed(2);
                $('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));


              
               
                
            },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
			var sub = [];
				var sub_values = "";
				$(".subscriber_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".subscriber_bulk_action:checked").length;
						 
						  sub=[];
						  $(".subscriber_bulk_action:checked").each(function() {
			
							sub.push($(this).val());
						}); 
					}
					else
					{
						sub = $('#download_multiple_subscriber').attr('data-multi_sub').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sub = jQuery.grep(sub, function(value) {
						  return value != remove_id;
						});
					}
						sub_values = sub.join(",");
						$('#download_multiple_subscriber').attr('data-multi_sub',sub_values);
					});
					
	 },
  }); 
}

function blogPostDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3, 4,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search': $("#search_feedback").val(), 'blog_start_date':$("#feedback_start_date").val(),'blog_end_date':$("#feedback_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			//var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	//$('#rev_count').html(num);
			
				var sub = [];
				var sub_values = "";
				$(".post_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".post_bulk_action:checked").length;
						 
						  sub=[];
						  $(".post_bulk_action:checked").each(function() {
			
							sub.push($(this).val());
						}); 
					}
					else
					{
						sub = $('#print_multiple_post').attr('data-multi_po').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sub = jQuery.grep(sub, function(value) {
						  return value != remove_id;
						});
					}
						sub_values = sub.join(",");
						$('#print_multiple_post').attr('data-multi_po',sub_values);
					});
					
	 },
  }); 
}

function blogDigestDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search': $("#search_feedback").val(), 'blog_start_date':$("#feedback_start_date").val(),'blog_end_date':$("#feedback_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			//var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	//$('#rev_count').html(num);
			
				var sub = [];
				var sub_values = "";
				$(".digest_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".digest_bulk_action:checked").length;
						 
						  sub=[];
						  $(".digest_bulk_action:checked").each(function() {
			
							sub.push($(this).val());
						}); 
					}
					else
					{
						sub = $('#print_multiple_digest').attr('data-multi_di').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sub = jQuery.grep(sub, function(value) {
						  return value != remove_id;
						});
					}
						sub_values = sub.join(",");
						$('#print_multiple_digest').attr('data-multi_di',sub_values);
					});
					
	 },
  }); 
}

function surveyDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search': $("#search_survey").val(), 'survey_start_date':$("#survey_start_date").val(),'survey_end_date':$("#survey_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
				var sub = [];
				var sub_values = "";
				$(".survey_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".survey_bulk_action:checked").length;
						 
						  sub=[];
						  $(".survey_bulk_action:checked").each(function() {
			
							sub.push($(this).val());
						}); 
					}
					else
					{
						sub = $('#print_multiple_survey').attr('data-multi_di').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sub = jQuery.grep(sub, function(value) {
						  return value != remove_id;
						});
					}
						sub_values = sub.join(",");
						$('#print_multiple_survey').attr('data-multi_di',sub_values);
					});
					
	 },
  }); 
}

function newsletterDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3, 4,] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search': $("#search_newsletter").val(), 'newsletter_start_date':$("#news_start_date").val(),'newsletter_end_date':$("#news_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			//var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	//$('#rev_count').html(num);
			
				var news = [];
				var news_values = "";
				$(".newletter_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".newletter_bulk_action:checked").length;
						 
						  news=[];
						  $(".newletter_bulk_action:checked").each(function() {
			
							news.push($(this).val());
						}); 
					}
					else
					{
						news = $('#print_multiple_newsletter').attr('data-multi_news').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						news = jQuery.grep(news, function(value) {
						  return value != remove_id;
						});
					}
						news_values = news.join(",");
						$('#print_multiple_newsletter').attr('data-multi_news',news_values);
					});
					
	 },
  }); 
}

function challangeDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search': $("#search_challange").val(),'challange_nature': $("#challange_nature").val(), 'challange_start_date':$("#challange_start_date").val(),'challange_end_date':$("#challange_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

			//var num = table_sales_sub.fnSettings().fnRecordsTotal();
            //$("#rev_count").html(num);

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
				$('.delete_challenge_btn').off().on('click', function(){
					var chal_id = $(this).data('chal_id');
					$('#delete_challenge').modal('open');
					$('#delete_challenge #remove_challenge_id').val(chal_id);
				});
				
				$('.deactive_challenge_btn').off().on('click', function(){
					var chal_id = $('#delete_challenge #remove_challenge_id').val();	
					$.ajax({
						//url:base_url+'My_payroll/deactivate_salary_expense/',
						url:base_url+'admin_dashboard/delete_challange',
						type:"POST",
						data:{"id":chal_id},
						success:function(res){
						if(res == true){
							challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
							$('select').material_select();	
							Materialize.toast('Challenge has been deleted', 2000,'green rounded');
						}else{
							challangeDatatable(base_path()+'admin-dashboard/get-challange-details/','challange_table');
							$('select').material_select();	
							Materialize.toast('Error while processing!', 2000,'red rounded');
						}
						},
					});				
				});
			
				var chal = [];
				var chal_values = "";
				$(".challange_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".challange_bulk_action:checked").length;
						 
						  chal=[];
						  $(".challange_bulk_action:checked").each(function() {
			
							chal.push($(this).val());
						}); 
					}
					else
					{
						chal = $('#download_multiple_challange').attr('data-multi_part').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						chal = jQuery.grep(chal, function(value) {
						  return value != remove_id;
						});
					}
						chal_values = chal.join(",");
						$('#download_multiple_challange').attr('data-multi_part',chal_values);
					});
					
	 },
  }); 
}

function adminuserDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6 ] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search':$('#search_users').val(),'users_start_date':$('#users_start_date').val(),'users_end_date':$('#users_end_date').val(),'search_by_city_users':$('#search_by_city_users').val(),'search_bycountry_users':$('#search_bycountry_users').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
				$('.deactive_users_profile').off().on('click', function(){
					var user_id = $(this).data('profileid');
					$('#deactive_challenge').modal('open');
					$('#deactive_challenge #remove_user_id').val(user_id);
				});
				
				$('.deactive_user_btn').off().on('click', function(){
					var chal_id = $('deactive_challenge #remove_user_id').val();	
					$.ajax({
						//url:base_url+'My_payroll/deactivate_salary_expense/',
						url:base_url+'admin_dashboard/deactivate_user',
						type:"POST",
						data:{"id":user_id},
						success:function(res){
						if(res == true){
							adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
							$('select').material_select();
							Materialize.toast('User has been deactivate', 2000,'green rounded');
						}else{
							adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
							$('select').material_select();	
							Materialize.toast('Error while processing!', 2000,'red rounded');
						}
						},
					});				
				});
			
				var user = [];
				var user_values = "";
				$(".users_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".users_bulk_action:checked").length;
						 
						  user=[];
						  $(".users_bulk_action:checked").each(function() {
			
							user.push($(this).val());
						}); 
					}
					else
					{
						user = $('#download_multiple_users').attr('data-multi_user').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						user = jQuery.grep(user, function(value) {
						  return value != remove_id;
						});
					}
						user_values = user.join(",");
						$('#download_multiple_users').attr('data-multi_user',user_values);
					});
					
	 },
  }); 
}

function interviewDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_sales_sub = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, ] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'search':$('#search_users').val(),'users_start_date':$('#users_start_date').val(),'users_end_date':$('#users_end_date').val(),'search_by_city_users':$('#search_by_city_users').val(),'search_bycountry_users':$('#search_bycountry_users').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[13]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     
     },
     "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                /*totalAmt = api
                    .column(10)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                totalAmt=parseFloat(totalAmt).toFixed(2);*/
				//$('tr:eq(1) th:eq(10)', api.table().header()).html("₹" +add_commas(totalAmt));
			},
			"fnDrawCallback": function () {
			$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_sales_sub.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
				$('.deactive_users_profile').off().on('click', function(){
					var user_id = $(this).data('profileid');
					$('#deactive_challenge').modal('open');
					$('#deactive_challenge #remove_user_id').val(user_id);
				});
				
				$('.deactive_user_btn').off().on('click', function(){
					var chal_id = $('deactive_challenge #remove_user_id').val();	
					$.ajax({
						//url:base_url+'My_payroll/deactivate_salary_expense/',
						url:base_url+'admin_dashboard/deactivate_user',
						type:"POST",
						data:{"id":user_id},
						success:function(res){
						if(res == true){
							adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
							$('select').material_select();
							Materialize.toast('User has been deactivate', 2000,'green rounded');
						}else{
							adminuserDatatable(base_path()+'admin-dashboard/get-users-details/','my-users-admin');
							$('select').material_select();	
							Materialize.toast('Error while processing!', 2000,'red rounded');
						}
						},
					});				
				});
			
				var user = [];
				var user_values = "";
				$(".users_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".users_bulk_action:checked").length;
						 
						  user=[];
						  $(".users_bulk_action:checked").each(function() {
			
							user.push($(this).val());
						}); 
					}
					else
					{
						user = $('#download_multiple_users').attr('data-multi_user').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						user = jQuery.grep(user, function(value) {
						  return value != remove_id;
						});
					}
						user_values = user.join(",");
						$('#download_multiple_users').attr('data-multi_user',user_values);
					});
					
	 },
  }); 
}