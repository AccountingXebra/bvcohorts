$(document).ready(function() {
		/*profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
		$('select').material_select();
		$('.profile-search').on('keyup','#search_personal_profile',function(){ 
		  profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
		  $('select').material_select();
				
		});*/
		
		//$('#search').on('keyup', function() { get_table() } );
		
		var bulk_profiles = [];
		$("input[id='personal_profile_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".personal_profile_bulk_action").prop('checked', true);
				$(".personal_profile_bulk_action:checked").each(function() {
					bulk_profiles.push($(this).val());
				});
				bulk_profiles = bulk_profiles.join(",");
				$('#deactive_multiple_profiles').attr('data-multi_profiles',bulk_profiles);
				$('#download_multiple_profiles').attr('data-multi_download',bulk_profiles);
			}
			else {
				$(".personal_profile_bulk_action").prop('checked',false);
					bulk_profiles = [];
					$('#deactive_multiple_profiles').attr('data-multi_profiles',0);
					$('#download_multiple_profiles').attr('data-multi_download',0);
			}
		});

 $("#reg_admin_type").on("change",function(){
  var reg_admin_type = $(this).val();
  if(reg_admin_type!=''){
    $("#select2-reg_admin_type-container").css("font-size","14px");
    $("#select2-reg_admin_type-container").css("color","#000");
    $("#select2-reg_admin_type-container").css("font-weight","500");
  }
});

 });

 $("#edit_personal_profile").validate({

				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					reg_mobile:{

						required:true,

						number:true,

						minlength:10,

						maxlength:10,

					},

					reg_password: {

						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},

					reg_designation:{

						//required:true,

					},

				},

				messages:{

					reg_username:{

						required:"Contact Name is required",

					},

					reg_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					reg_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						minlength:"Please Enter Mobile No like (e.g 0123456789)",

						maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					reg_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},

					reg_admin_type:{

						required:"Please Allow any access to New User",

					},

					reg_designation:{

						required:"Please Enter Designation",

					},

				},
				errorPlacement: function(error, element) {
         
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
			        form.submit();
			      }   
				});
 


$.validator.addMethod("checklowercase",
    function(value, element) {
        return /^(?=(.*[a-z]){1,}).{1,}$/.test(value);
});
$.validator.addMethod("checkuppercase",
    function(value, element) {
        return /^(?=.*?[A-Z]).{1,}$/.test(value);
});
$.validator.addMethod("checknumber",
    function(value, element) {
        return /^(?=(.*[\d]){1,}).{1,}$/.test(value);
});
$.validator.addMethod("checkspecialchar",
    function(value, element) {
        return /^(?=(.*[\W]){1,})(?!.*\s).{1,}$/.test(value);
});
