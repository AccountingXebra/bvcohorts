$(document).ready(function() {
invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');
$('select').material_select();
paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');
$('select').material_select();
docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');
$('select').material_select();
tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');
$('select').material_select();
soaDatatable(base_path()+'statement_account/get_soa/','statement-of-account');
$('select').material_select();
$('.cinvoices-search').on('change','#search_by_invstatus',function() { invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');$('select').material_select(); } );
$('.cinvoices-search').on('change','#inv_start_date,#inv_end_date',function() { if($(this).val()!=''){ invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');$('select').material_select(); } } );
$('.payment-search').on('change','#search_by_pmstatus',function() { paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');$('select').material_select(); } );
$('.documents-search').on('change','#docl_start_date,#docl_end_date',function() { if($(this).val()!=''){ docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');$('select').material_select(); } } );
$('.tds-search').on('change','#ser_start_date,#ser_end_date',function() { if($(this).val()!=''){ tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');$('select').material_select(); } } );
$('.soa-search').on('change','#inv_start_date,#inv_end_date',function() { if($(this).val()!=''){ soaDatatable(base_path()+'statement_account/get_soa/','statement-of-account');$('select').material_select(); } } );
	
 	var bulk_invoices = [];
	$("body").on("click","input[id='cinvoices_bulk']",function(){
		if($(this).is(':checked')) {
			$(".cinvoice_bulk_action").prop('checked', true);
			bulk_invoices = [];
			$(".cinvoice_bulk_action:checked").each(function() {
					bulk_invoices.push($(this).val());
				});
				bulk_invoices = bulk_invoices.join(",");
				$('#download_multiple_invoices').attr('data-multi_invdownload',bulk_invoices);
			}
			else {
				$('#customer-invoice tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_invoices = [];
				$('#download_multiple_invoices').attr('data-multi_invdownload',0);
			}
	});
	
	
	$("body").on('click','#download_multiple_invoices',function(e) {

		var dnwl_ids = $('#download_multiple_invoices').attr('data-multi_invdownload');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_invoices').attr('data-multi_invdownload',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#customer-invoice th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				window.location=base_url+'statement_account/download-multiple-cinvoices?ids='+dnwl_ids;
				invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');
				$('select').material_select();
				Materialize.toast('Customer Invoices has been downloaded', 2000,'green rounded');
			}
		}
	});

	$("body").on('click','#email_multiple_invoices',function(e) {

		var invoice_values =$('#download_multiple_invoices').attr('data-multi_invdownload');
		if(invoice_values == '' || invoice_values == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = invoice_values.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				// $.ajax({
				// 	url:base_url+'statement_account/email_multiple_cinvoices',
				// 	type:"POST",
				// 	data:{"sales_values":invoice_values},
				// 	beforeSend: function(){
				//       Materialize.toast('Sending Email.......', 2000,'green rounded');
				//     },
				// 	success:function(res){
								
				// 				invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');
				// 				$('#download_multiple_invoices').attr('data-multi_invdownload',0);
				// 				$('#customer-invoice th').find('input[type="checkbox"]').each(function() {
				// 				    $(this).prop('checked', false);
				// 				  });
				// 				Materialize.toast('Customer Invoices has been emailed', 2000,'green rounded');
				// 		},
				// });	

				email_cinvoice(invoice_values);
			}	
		}
	});
	$("body").on('click','#print_multiple_invoices',function(e) {

		var dnwl_ids = $('#download_multiple_invoices').attr('data-multi_invdownload');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_invoices').attr('data-multi_invdownload',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#customer-invoice th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				});
				invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');
				$('select').material_select();
				window.location=base_url+'client-statement_account/print-multiple-cinvoices?ids='+dnwl_ids;
				Materialize.toast('Customer Invoices has been printed', 2000,'green rounded');
			}
		}
	});

	var bulk_payment = [];
	$("body").on("click","input[id='payment_bulk']",function(){
		if($(this).is(':checked')) {
			$(".payment_bulk_action").prop('checked', true);
			bulk_payment = [];
			$(".payment_bulk_action:checked").each(function() {
					bulk_payment.push($(this).val());
				});
				bulk_payment = bulk_payment.join(",");
				$('#download_multiple_payment').attr('data-multi_pmdownload',bulk_payment);
			}
			else {
				$('#payment-made tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_payment = [];
				$('#download_multiple_payment').attr('data-multi_pmdownload',0);
			}
	});
	$("body").on('click','#download_multiple_payment',function(e) {

		var dnwl_ids = $('#download_multiple_payment').attr('data-multi_pmdownload');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 1){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_payment').attr('data-multi_pmdownload',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#payment-made th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				window.location=base_url+'statement_account/download-multiple-payment?ids='+dnwl_ids;
				paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');
				$('select').material_select();			
				Materialize.toast('Sales Receipts has been downloaded', 2000,'green rounded');
			}
		}
	});
	$("body").on('click','#email_multiple_payment',function(e) {

		var dnwl_ids =$('#download_multiple_payment').attr('data-multi_pmdownload');
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 1){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				// $.ajax({
				// 	url:base_url+'statement_account/email_multiple_payment',
				// 	type:"POST",
				// 	data:{"sales_values":dnwl_ids},
				// 	beforeSend: function(){
				//       Materialize.toast('Sending Email.......', 2000,'green rounded');
				//     },
				// 	success:function(res){
								
				// 				paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');
				// 				$('#download_multiple_payment').attr('data-multi_pmdownload',0);
				// 				$('#payment-made th').find('input[type="checkbox"]').each(function() {
				// 				    $(this).prop('checked', false);
				// 				  });
				// 				Materialize.toast('Sales Receipts has been emailed', 2000,'green rounded');
				// 		},
				// });	
				email_payment(dnwl_ids);
			}	
		}
	});
	$("body").on('click','#print_multiple_payment',function(e) {

		var dnwl_ids = $('#download_multiple_payment').attr('data-multi_pmdownload');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 1){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_payment').attr('data-multi_pmdownload',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#payment-made th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				});
				paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');
				$('select').material_select();				
				window.location=base_url+'statement_account/print-multiple-payment?ids='+dnwl_ids;
				Materialize.toast('Sales Receipts has been printed', 2000,'green rounded');
			}
		}
	});
	
	var bulk_doc = [];
	//$("body").on("click","input[id='documents_bulk']",function(){
	$("input[id='documents_bulk']").on("click",function(){
		if($(this).is(':checked')) {
			$(".docl_bulk_action").prop('checked', true);
			bulk_doc = [];
			$(".docl_bulk_action:checked").each(function() {
					bulk_doc.push($(this).val());
				});
				bulk_doc = bulk_doc.join(",");
				$('#download_multiple_documents').attr('data-multi_doc',bulk_doc);
			}
			else {
				$('#documentation_tbl tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_doc = [];
				$('#download_multiple_documents').attr('data-multi_doc',0);
			}
	});

	 var docl = [];
        var docl_values = "";
        //$(".docl_bulk_action").on('click', function() {
        $("body").on("click",".docl_bulk_action",function(){
	          if($(this).is(':checked')){
	            var total_checked = $(".docl_bulk_action:checked").length;
	              docl=[];
	              $(".docl_bulk_action:checked").each(function() {
	                docl.push($(this).val());
	              }); 
	          }
	          else
	          {
	            docl=$('#download_multiple_documents').attr('data-multi_doc').split(',');
	            $(this).prop('checked',false);
	            var remove_id = $(this).val();  
	            docl = jQuery.grep(docl, function(value) {
	              return value != remove_id;
	            });
	          }
            docl_values = docl.join(",");
            
            $('#download_multiple_documents').attr('data-multi_doc',docl_values);
          });
	
	
	$("body").on('click','#download_multiple_documents',function(e) {
		
		var dnwl_ids = $('#download_multiple_documents').attr('data-multi_doc');
	
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");

			// var sorted_word = '';
			// for(i=0; i < words.length; i++)
			// {
			// 	sorted_word += words[i] + ','; 
			// 	i++;
			// }

			// sorted_word =  sorted_word.replace(/,\s*$/, "");

		    if(words.length < 4){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_documents').attr('data-multi_doc',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#documentation_tbl th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				window.location=base_url+'statement_account/download-multiple-documents?ids='+dnwl_ids;
				docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');
				$('select').material_select();				
				Materialize.toast('Customer Document has been downloaded', 2000,'green rounded');
			}
		}
	});
	
	$("body").on('click','#email_multiple_documents',function(e) {

		var dnwl_ids =$('#download_multiple_documents').attr('data-multi_doc');
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$.ajax({
					url:base_url+'statement_account/email_multiple_documents',
					type:"POST",
					data:{"sales_values":dnwl_ids},
					success:function(res){
								
								// docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');
								// $('select').material_select();
								// $('#download_multiple_documents').attr('data-multi_doc',0);
								// $('#documentation_tbl th').find('input[type="checkbox"]').each(function() {
								//     $(this).prop('checked', false);
								//   });
								// Materialize.toast('Customer Document has been emailed', 2000,'green rounded');
								//alert(dnwl_ids);
								email_document(dnwl_ids);
						},
				});	
			}	
		}
	});
	$("body").on('click','#print_multiple_documents',function(e) {

		var dnwl_ids = $('#download_multiple_documents').attr('data-multi_doc');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");

			var sorted_word = '';
			for(i=0; i < words.length; i++)
			{
				sorted_word += words[i] + ','; 
				i++;
			}

			sorted_word =  sorted_word.replace(/,\s*$/, "");


		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_documents').attr('data-multi_doc',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#documentation_tbl th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				});

				docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');
				$('select').material_select();				
				window.location=base_url+'statement_account/print-multiple-documents?ids='+sorted_word;
				Materialize.toast('Customer Document has been printed', 2000,'green rounded');
			}
		}
	});
	/*-----Start Bulk Action-----*/
	var bulk_tds = [];
		$("body").on("click","input[id='tds_bulk']",function(){
		if($(this).is(':checked')) {
			$(".tds_bulk_action").prop('checked', true);
			bulk_tds = [];
			$(".tds_bulk_action:checked").each(function() {
					bulk_tds.push($(this).val());
				});
				bulk_tds = bulk_tds.join(",");
				$('#download_multiple_tds').attr('data-multi_tds',bulk_tds);
			}
			else {
				$('#my-tds tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_tds = [];
				$('#download_multiple_tds').attr('data-multi_tds',0);
			}
	});
	
	$("body").on('click','#download_multiple_tds',function(e) {

		var dnwl_ids = $('#download_multiple_tds').attr('data-multi_tds');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 4){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_tds').attr('data-multi_tds',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#my-tds th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');		
				window.location=base_url+'statement_account/download-multiple-tds?ids='+dnwl_ids;
				$('select').material_select();		
				Materialize.toast('Clients TDS has been downloaded', 2000,'green rounded');
			}
		}
	});
	$("body").on('click','#email_multiple_tds',function(e) {

		var sreceipt_values =$('#download_multiple_tds').attr('data-multi_tds');
		if(sreceipt_values == '' || sreceipt_values == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = sreceipt_values.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				// $.ajax({
				// 	url:base_url+'statement_account/email_multiple_tds',
				// 	type:"POST",
				// 	data:{"sreceipt_values":sreceipt_values},
				// 	beforeSend: function(){
				//       Materialize.toast('Sending Email.......', 2000,'green rounded');
				//     },
				// 	success:function(res){
								
				// 				tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');
				// 				$('#download_multiple_tds').attr('data-multi_tds',0);
								
				// 				$('#my-tds th').find('input[type="checkbox"]').each(function() {
				// 				    $(this).prop('checked', false);
				// 				  });
				// 				Materialize.toast('Clients TDS has been emailed', 2000,'green rounded');
				// 		},
				// });	
				email_tds(sreceipt_values);
			}	
		}
	});
	$("body").on('click','#print_multiple_tds',function(e) {

		var dnwl_ids = $('#download_multiple_tds').attr('data-multi_tds');
		
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_tds').attr('data-multi_tds',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#my-tds th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				});
				tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');
				$('select').material_select();
				window.location=base_url+'statement_account/print-multiple-tds?ids='+dnwl_ids;
				Materialize.toast('Clients TDS has been printed', 2000,'green rounded');
			}
		}
	});
	var bulk_soa = [];
	var bulk_type = [];
	$("body").on("click","input[id='soa_bulk']",function(){
		if($(this).is(':checked')) {
			$(".soa_bulk_action").prop('checked', true);
			bulk_soa = [];
			$(".soa_bulk_action:checked").each(function() {
					bulk_soa.push($(this).val());
					bulk_type.push($(this).attr('data-type'));
				});
				bulk_soa = bulk_soa.join(",");
				bulk_type = bulk_type.join(",");
				$('#download_multiple_soa').attr('data-multi_soa',bulk_soa);
				$('#download_multiple_soa').attr('data-multi_type',bulk_type);
			}
			else {
				$('#statement-of-account tr').find('input[type="checkbox"]').each(function() {
				$(this).prop('checked', false);
				});
				bulk_soa = [];bulk_type = [];
				$('#download_multiple_soa').attr('data-multi_soa',0);
				$('#download_multiple_soa').attr('data-multi_type','');
			}
	});
	
	
	$("body").on('click','#download_multiple_soa',function(e) {

		var start_date = $('#inv_start_date').val();
		var end_date = $('#inv_end_date').val()
		window.location.href = base_url+'statement_account/download_multiple_soa?start='+start_date+'&end='+end_date;
		return false;
	});

	$("body").on('click','#email_multiple_soa',function(e) {
		var start_date = $('#inv_start_date').val();
		var end_date = $('#inv_end_date').val()
		email_soa(start_date,end_date);

	});
	$("body").on('click','#print_multiple_soa',function() {

		// $('#download_multiple_soa').attr('data-multi_soa',0);
		// $('#download_multiple_soa').attr('data-multi_type','');
		// //$('#company-profile').find('input:checkbox').attr('checked',false);
		// $('#statement-of-account tr').find('input[type="checkbox"]').each(function() {
		// $(this).prop('checked', false);
		// });
		//soaDatatable(base_path()+'statement_account/get_soa/','statement-of-account');
		var start_date = $('#inv_start_date').val();
		var end_date = $('#inv_end_date').val()
		// alert(start_date);
		// alert(end_date);
		//alert(base_url+'statement_account/print_multiple_soa/');
		window.location.href = base_url+'statement_account/print_multiple_soa?start='+start_date+'&end='+end_date;
		 return false;
		//Materialize.toast('Statement Of Accounts has been printed', 2000,'green rounded');


	});

	/*-----End Bulk Action-----*/
	$('#my-tds').on('click','.cust_status',function(){
		var id = $(this).data('tds_id');
		var tds_cstatus = $(this).attr('value');
		 $('#customer_tds_status').modal('open');
		 $('#customer_tds_status #ctds_id').val(id);	
		 $('#customer_tds_status #ctds_status').val(tds_cstatus);
		 if(tds_cstatus!="0"){
		 	tds_cstatus="Paid";
		 }else{
		 	tds_cstatus="Unpaid";
		 }
		 $('#customer_tds_status .tstatus_type').text(tds_cstatus);
		 
	});
	$('body').on('click','.tds_cstatus_yes',function(){

		var ctds_id = $('#customer_tds_status #ctds_id').val();
		var ctds_status =  $('#customer_tds_status #ctds_status').val();
		$.ajax({

			url:base_url+'statement_account/clients_tds_status',

			type:"POST",

			data:{'ctds_id':ctds_id,'status':ctds_status},

			success:function(res){
					if(res == true)
					{
						if(ctds_status=="0"){ctds_status="Unpaid";}else{ctds_status="Paid"; }
						tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');
						$('select').material_select();
						Materialize.toast('Client TDS status change to '+ctds_status, 2000,'green rounded');
					}
					else
					{
						Materialize.toast('Error while processing!', 2000,'red rounded');
					}
					
				},
			});										
	});
	$('body').on('click','.no_tdsstatus_change',function(){
		var ctds_id = $('#customer_tds_status #ctds_id').val();
		 if($('#tds_status'+ctds_id).prop('checked')==false){
          $('#tds_status'+ctds_id).prop('checked',true);
        }else{
          $('#tds_status'+ctds_id).prop('checked',false);
        }
	});
});
$(function() {

  $("#client_login").attr('autocomplete', 'off');
  $("#client_login").validate({
      rules: {
         cust_name:{
            required:true,
            remote:base_path()+'statement_account/check_client_exist',
          },
          cp_name:{
            required:true,
          },
          cp_email:{
            required:true,
            email:true,
          
          },

          password:{
          	required:true,
			minlength:8,
			
          },


      },
      messages:{

          cust_name:{

            required:"Comapany Name is required",

          }, 
          cp_name:{

            required:"Client Name is required",

          },
          cp_email:{

            required:"User Name is required",

          },
          password:{
            required:"Password is required",
            minlength:"Please Enter Minimum 8 characters long password",
          },

     
        
        },
    
       
         
      submitHandler: function(form) {
        form.submit();
      }
    });
  	$("#client_change_password").submit(function(e){
			e.preventDefault();
		}).validate({

			rules:{

				old_password:{
					required:true,
					minlength:8,
				},

				new_password:{
					required:true,
					minlength:8,
				},
				confirm_password:{
					required:true,
					equalTo: '#new_password',
				},
			},

			messages:{

				old_password:{
					required:"Old Password is required",
					minlength:"Please Enter Minimum 8 characters long password",
				},

				new_password:{
					required:"New Password is required",
					minlength:"Please Enter Minimum 8 characters long password",
				},

				confirm_password:{
					required:"Confirm your new password",
					equalTo:"Your password doesn't match",
				},
			},
			submitHandler: function(form) {
    
		        form.submit();
		       
		    }

		});
  });
function invoiceDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	//$('#deactive_multiple_invoices').attr('data-multi_invoices',0);
	//$('#download_multiple_invoices').attr('data-multi_invdownload',0);
	table_sales_inv = $('#'+id).dataTable({	
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6,7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable":  false},
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search':'','cust_id':$("cust_id").val(),'inv_start_date':$("#inv_start_date").val(),'inv_end_date':$("#inv_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		
		if($(aData[5]+'span').hasClass('green-c')){
					
			$('td:nth-child(6)', nRow).addClass('green-c');
		}else{
			$('td:nth-child(6)', nRow).addClass('red-c');
		}
		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(7)', nRow).addClass('green-c');
		//$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
		
			var invoices = [];
				var invoices_values = "";
				$(".cinvoice_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".cinvoice_bulk_action:checked").length;
						  invoices=[];
						  $(".cinvoice_bulk_action:checked").each(function() {
			
							invoices.push($(this).val());
						}); 
					}
					else
					{
						invoices=$('#download_multiple_invoices').attr('data-multi_invdownload').split(',');
						//console.log(invoices);
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						invoices = jQuery.grep(invoices, function(value) {
						  return value != remove_id;
						});
					}
						invoices_values = invoices.join(",");
						
						$('#download_multiple_invoices').attr('data-multi_invdownload',invoices_values);
					});
					
			
	 },
  }); 
}
function paymentDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#deactive_multiple_preceipts').attr('data-multi_pmade',0);
	
	table_sales_rec = $('#'+id).dataTable({	
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable":  false},
		{ "bSortable": true },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search_by_status':$('#search_by_pmstatus').val(),'srec_start_date':$('#srec_start_date').val(),'srec_end_date':$('#srec_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[5]+' span').hasClass('red-c')){
					
			$('td:nth-child(6)', nRow).addClass('red-c');
		}else{
			$('td:nth-child(6)', nRow).addClass('green-c');
		}
		$('td:first-child', nRow).addClass('bulk');
		
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
							
			
			var sreceipts = [];
				var sreceipts_values = "";
				$(".payment_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".payment_bulk_action:checked").length;
						 /* if (jQuery.inArray(invoices, $(this).val())== '-1') {
							invoices.push($(this).val());
						  }*/
						  sreceipts=[];
						  $(".payment_bulk_action:checked").each(function() {
			
							sreceipts.push($(this).val());
						}); 
					}
					else
					{
						sreceipts=$('#download_multiple_payment').attr('data-multi_pmdownload').split(',');
						//console.log(invoices);
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						sreceipts = jQuery.grep(sreceipts, function(value) {
						  return value != remove_id;
						});
					}
						sreceipts_values = sreceipts.join(",");
						$('#download_multiple_payment').attr('data-multi_pmdownload',sreceipts_values);
					});
					
	 },
  }); 
}

function docDatatable(url,id){
  
  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_alerts = $('#'+id).dataTable({ 
  "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
   "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
   "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": true },
    { "bSortable": false },
    { "bSortable":  false},
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    
  ],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
	"sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],	
 //"aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'search': '','docl_type':'customer','docl_start_date':$("#docl_start_date").val(),'docl_end_date':$("#docl_end_date").val()},
   
    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

  
    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });
      
      var docum= [];
        var document_values = "";
        $(".document_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".document_bulk_action:checked").length;
              docum=[];
              $(".document_bulk_action:checked").each(function() {
                docum.push($(this).val());
              }); 
          }
          else
          {
            docum=$('#download_multiple_documents').attr('data-multi_doc').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();  
            docum = jQuery.grep(docum, function(value) {
              return value != remove_id;
            });
          }
            document_values = docum.join(",");
            $('#download_multiple_documents').attr('data-multi_doc',document_values);
          });
   },
  }); 
}
function tdsDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#download_multiple_tds').attr('data-multi_tds',0);
	
	table_sales_rec = $('#'+id).dataTable({	
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6,7,8] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		/*{ "bSortable": false },*/
		/*{ "bSortable":  true},*/
		{ "bSortable": true },
		{ "bSortable":  true},
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search':'','srec_customer':'','srec_status':$('#tds_status').val(),'srec_start_date':$('#ser_start_date').val(),'srec_end_date':$('#ser_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[4]+' span').hasClass('red-c')){
					
			$('td:nth-child(5)', nRow).addClass('red-c');
		}else{
			$('td:nth-child(5)', nRow).addClass('green-c');
		}
		$('td:first-child', nRow).addClass('bulk');
		
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			
			
			var tds = [];
				var tds_values = "";
				$(".tds_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".tds_bulk_action:checked").length;
						 
						  tds=[];
						  $(".tds_bulk_action:checked").each(function() {
			
							tds.push($(this).val());
						}); 
					}
					else
					{
						tds=$('#download_multiple_tds').attr('data-multi_tds').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						tds = jQuery.grep(tds, function(value) {
						  return value != remove_id;
						});
					}
						tds_values = tds.join(",");
						$('#download_multiple_tds').attr('data-multi_tds',tds_values);
					});
					
	 },
  }); 
}
function soaDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	table_soa = $('#'+id).dataTable({
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable":  false},
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'search':'','search_by_customer':'','search_by_status':'','search_by_document_type':'','inv_start_date':$('#inv_start_date').val(),'inv_end_date':$('#inv_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		$('td:first-child', nRow).addClass('bulk');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
		
				var soa = [];var type = [];
				var soa_values = "";var type_values = "";
				$(".soa_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".soa_bulk_action:checked").length;
						  soa=[]; type=[];
						  $(".soa_bulk_action:checked").each(function() {
			
							soa.push($(this).val());
							type.push($(this).attr('data-type'));
						}); 
					}
					else
					{
						soa=$('#download_multiple_soa').attr('data-multi_soa').split(',');
						type=$('#download_multiple_soa').attr('data-multi_type').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();
						var indexArr = soa.indexOf(remove_id);	
						soa.splice(indexArr,1);
						type.splice(indexArr,1);
					
					}
						soa_values = soa.join(",");
						type_values = type.join(",");
						
						$('#download_multiple_soa').attr('data-multi_soa',soa_values);
						$('#download_multiple_soa').attr('data-multi_type',type_values);
					});
					
			
	 },
  }); 
}
function email_cinvoice($id){
  //  $.ajax({
  //   type: "POST",
  //    url:base_url+'statement_account/email_cinvoice/'+$id,
  //   //data:{'cmp_id':$id},
  //   beforeSend: function(){
  //     Materialize.toast('Sending Email.......', 2000,'green rounded');
  //   },
  //   success: function(data){
  //     Materialize.toast('Mail has been sent!', 2000,'green rounded');
  //   },
  // });



  var invoice_id = $id;
  $('#send_email_portal_invoice').modal('open');
  $('#send_email_portal_invoice #invoice_id').val(invoice_id);


}
function email_payment($id){
  //  $.ajax({
  //   type: "POST",
  //    url:base_url+'statement_account/email_payment_made/'+$id,
  //   //data:{'cmp_id':$id},
  //   beforeSend: function(){
  //     Materialize.toast('Sending Email.......', 2000,'green rounded');
  //   },
  //   success: function(data){
  //     Materialize.toast('Mail has been sent!', 2000,'green rounded');
  //   },
  // });

  var recipt_id = $id;
  $('#send_email_portal_recipt').modal('open');
  $('#send_email_portal_recipt #recipt_id').val(recipt_id);

}

function email_document($id,$type=""){
  //  $.ajax({
  //   type: "POST",
  //    url:base_url+'statement_account/email_document/'+$id,
  //   //data:{'cmp_id':$id},
  //   beforeSend: function(){
  //     Materialize.toast('Sending Email.......', 2000,'green rounded');
  //   },
  //   success: function(data){
  //     Materialize.toast('Mail has been sent!', 2000,'green rounded');
  //   },
  // });
  if($type)
  { 
	  var doc_id = $id+','+$type;

  }else{

	    var doc_id = $id;
  }

  var type =$type;
  $('#send_email_portal_doc').modal('open');
  $('#send_email_portal_doc #doc_id').val(doc_id);
}

function email_tds($id){
  //  $.ajax({
  //   type: "POST",
  //    url:base_url+'statement_account/email_tds/'+$id,
  //   //data:{'cmp_id':$id},
  //   beforeSend: function(){
  //     Materialize.toast('Sending Email.......', 2000,'green rounded');
  //   },
  //   success: function(data){
  //     Materialize.toast('Mail has been sent!', 7000,'green rounded');
  //   },
  // });
  var tds_id = $id;
  $('#send_email_portal_tds').modal('open');
  $('#send_email_portal_tds #tds_id').val(tds_id);
}

function reset_cinvfilter(){
  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
  $('.btn-date,.search-hide-show').val('');
  $('.js-example-basic-single').trigger('change.select2');
  //$('.search-hide-show').toggle();
 /* var select = $('select');
  $('select').prop('selectedIndex',0);*/
  $('select').material_select();
  $('#customer-invoice th').find('input[type="checkbox"]').each(function() {
    $(this).prop('checked', false);
  });
  invoiceDatatable(base_path()+'statement_account/get_customer_invoices/','customer-invoice');
  $('select').material_select();
}

function reset_cpayment(){
  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
  $('.btn-date,.search-hide-show').val('');
  $('.js-example-basic-single').trigger('change.select2');
  //$('.search-hide-show').toggle();
 /* var select = $('select');
  $('select').prop('selectedIndex',0);*/
  $('select').material_select();
  $('#payment-made th').find('input[type="checkbox"]').each(function() {
    $(this).prop('checked', false);
  });
  paymentDatatable(base_path()+'statement_account/get_payment_made/','payment-made');
  $('select').material_select();
  }
  function reset_cspayment(){
  $('.btn-date,.search-hide-show').val('');
  $('#documentation_tbl th').find('input[type="checkbox"]').each(function() {
    $(this).prop('checked', false);
  });
  docDatatable(base_path()+'statement_account/get_documents_locker/','documentation_tbl');
  $('select').material_select();
  }
  function reset_tdsfilter($id){
  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
  $('.js-example-basic-single').trigger('change.select2');
  $('.btn-date,.search-hide-show').val('');
 
  $('select').material_select();
  $('#my-tds tr').find('input[type="checkbox"]').each(function() {
	$(this).prop('checked', false);
	});
  tdsDatatable(base_path()+'statement_account/get_tds_list/','my-tds');
  $('select').material_select();
}

function reset_soafilter($id){
   $('.btn-date,.search-hide-show').val('');
 
  
  $('#statement-of-account tr').find('input[type="checkbox"]').each(function() {
	$(this).prop('checked', false);
	});
  soaDatatable(base_path()+'statement_account/get_soa/','statement-of-account');
  $('select').material_select();
}


function email_soa(start_date,end_date){
  var soa_id = 1;
  $('#send_email_soa_modal').modal('open');
  $('#send_email_soa_modal #soa_id').val(soa_id);
  $('#send_email_soa_modal #start_date').val(start_date);
  $('#send_email_soa_modal #end_date').val(end_date);
}