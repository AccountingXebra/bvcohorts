 $.validator.setDefaults({ ignore: '' });

// ----------Documents Locker for My Profile -----    //
$(document).ready( function () {
 doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
 $('select').material_select();
 $('#docl_type,#docl_start_date,#docl_end_date').change( function() { 
    if($(this).attr('id')=="docl_type" ){ doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker'); $('select').material_select();
    }else{ if($(this).val()!=''){ doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker'); $('select').material_select(); } } 
});	
$('#search_docl').keyup(function(){ 
  doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
  $('select').material_select();
});
var bulk_docl = [];
	$("input[id='docl_bulk']").on("click",function(){
	if($(this).is(':checked')) {
		$(".docl_bulk_action").prop('checked', true);
		bulk_docl = [];
		$(".docl_bulk_action:checked").each(function() {
				bulk_docl.push($(this).val());
			});
			bulk_docl = bulk_docl.join(",");
			$('#deactive_multiple_docl').attr('data-multi_docl',bulk_docl);
			
		}
		else {
			$('#documents-locker tr').find('input[type="checkbox"]').each(function() {
			$(this).prop('checked', false);
			});
			bulk_docl = [];
			$('#deactive_multiple_docl').attr('data-multi_docl',0);
		}
});
	
});
 function doclDatatable(url,id){
  
  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_alerts = $('#'+id).dataTable({ 
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
	"aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },/*
    { "bSortable": false },*/
    
  ],

    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
    oLanguage:{
       sLengthMenu: "_MENU_",
    },
    "sDom": 'Rfrtlip',
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
  "aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'search': $("#search_docl").val(),'docl_type':$("#docl_type").val(),'docl_start_date':$("#docl_start_date").val(),'docl_end_date':$("#docl_end_date").val()},
   
    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

  
    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
      "headerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;
                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                 totalKb = api
                    .column(5)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    totKb=parseFloat(totalKb/1024).toFixed(2);

                $('tr:eq(1) th:eq(5)', api.table().header()).html(totKb+" MB");

               
                
            },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

      var num = table_alerts.fnSettings().fnRecordsTotal();
      $('#doc_tot').html(num);
                  
      $('.deactive_dcl').off().on('click', function(){
          var dcl_id = $(this).data('dcl_id');  
         
         $('#deactivate_dcl').modal('open');

         $('#deactivate_dcl #remove_dcl_id').val(dcl_id);
        });
      $('.delete_dcl').off().on('click', function(){
        var dcl_id = $('#deactivate_dcl #remove_dcl_id').val();
        $.ajax({

          url:base_url+'dashboard/deactive_document_locker',

          type:"POST",

          data:{"dcl_id":dcl_id,"status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
                $('select').material_select();
                Materialize.toast('Document has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });                   
      });
      
      
      var docl = [];
        var docl_values = "";
        $(".docl_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".docl_bulk_action:checked").length;
              docl=[];
              $(".docl_bulk_action:checked").each(function() {
                docl.push($(this).val());
              }); 
          }
          else
          {
            docl=$('#deactive_multiple_docl').attr('data-multi_docl').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();  
            docl = jQuery.grep(docl, function(value) {
              return value != remove_id;
            });
          }
            docl_values = docl.join(",");
            $('#deactive_multiple_docl').attr('data-multi_docl',docl_values);
          });
   },
  }); 
  $('select').material_select();
}
$("#deactive_multiple_docl").on('click', function() {
    var docl_values = $(this).attr('data-multi_docl');
    if(docl_values == '' || docl_values == '0')
    {
      Materialize.toast('Please select a record first', 2000,'red rounded');
    }
    else
    {
      var words = docl_values.split(",");
        if(words.length < 2){
          Materialize.toast('Select two or more records first..!', 2000,'red rounded');
         }else{


        var sorted_word = '';
        for(i=0; i < words.length; i++)
        {
          sorted_word += words[i] + ','; 
          i++;
        }

      sorted_word =  sorted_word.replace(/,\s*$/, "");

        $.ajax({
          url:base_url+'dashboard/deactive_multiple_docl',
          type:"POST",
          data:{"docl_values":sorted_word,"status":"Inactive"},
          success:function(res){
                
                 doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
                 $('select').material_select();
                 $('#deactive_multiple_docl').attr('data-multi_docl',0);
                  $('#documents-locker th').find('input[type="checkbox"]').each(function() {
                      $(this).prop('checked', false);
                    });
					$("#docl_bulk").prop('checked', false);
                Materialize.toast('Documents Locker has been deactivated', 2000,'green rounded');
            },
        }); 
      } 
    }
  });
$("body").on('click','#download_multiple_docl',function(e) {

      var dnwl_ids = $('#deactive_multiple_docl').attr('data-multi_docl');
      
      
      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {

         var words = dnwl_ids.split(",");

      //   var sorted_word = '';
      //   for(i=0; i < words.length; i++)
      //   {
      //     sorted_word += words[i] + ','; 
      //     i++;
      //   }

      // sorted_word =  sorted_word.replace(/,\s*$/, "");

        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_docl').attr('data-multi_docl',0);
          $('#documents-locker th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$("#docl_bulk").prop('checked', false);
          window.location=base_url+'dashboard/download-multiple-documents-locker?ids='+dnwl_ids;
          doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
          $('select').material_select();
          Materialize.toast('Documents Locker has been successfully downloaded', 2000,'green rounded');
        }
      }
});

$("body").on('click','#print_multiple_docl',function(e) {
      var dnwl_ids = $('#deactive_multiple_docl').attr('data-multi_docl');
      
      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");

        var sorted_word = '';
        for(i=0; i < words.length; i++)
        {
          sorted_word += words[i] + ','; 
          i++;
        }

      sorted_word =  sorted_word.replace(/,\s*$/, "");

        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_docl').attr('data-multi_docl',0);
          $('#documents-locker th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
          window.location=base_url+'dashboard/print-multiple-documents-locker?ids='+sorted_word;
          doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
          $('select').material_select();
          Materialize.toast('Documents Locker has been successfully print', 2000,'green rounded');
        }
      }
});
$("#email_multiple_docl").on('click', function() {
        var docl_values = $('#deactive_multiple_docl').attr('data-multi_docl');
        //alert(docl_values);
        if(docl_values == '' || docl_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = docl_values.split(",");
          if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'dashboard/email_multiple_locker',
            //   type:"POST",
            //   data:{"alerts_values":docl_values},
            //   success:function(res){
            //         doclDatatable(base_url+'dashboard/get_documents_locker/','documents-locker');
            //         $('select').material_select();
            //         $('#deactive_multiple_docl').attr('data-multi_docl',0);
            //         $('#documents-locker th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Documents Locker has been successfully email', 2000,'green rounded');
            //     },
            // }); 
            email_document_locker(docl_values)
          }  
        }
      });
function email_document_locker($id,$type=""){
//    $.ajax({
//     type: "POST",
//      url:base_url+'dashboard/email_document_locker/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });
if($type)
{ 
  var doc_id = $id+'-'+$type;

}else{

    var doc_id = $id;
}

var type =$type;
$('#email_popup_doc_modal').modal('open');
$('#email_popup_doc_modal #doc_id').val(doc_id);
$('#email_popup_doc_modal #doc_type').val(type);

}