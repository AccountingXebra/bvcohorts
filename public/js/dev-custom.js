(function ($) {

})(jQuery); // end of jQuery name space


$('#my-sales-retun').dataTable( {
  "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2 , 3, 4, 6, 7, 8] }],
  "order": [],
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bInfo": false,
  "searching":false
});

$('.sales_delete').on('click',function(){
  var id = $(this).data('id');
  $('#remove_sales_return').val(id);
  $('#delete_sales').modal('open');
});
function deleteSalesReturn(){
  var id=$('#remove_sales_return').val();
  var base=$('#base_url').val();
  var url=base+'My_sales_return/delete/'+id;
  window.location=url;
}