ALTER TABLE `notifications` ADD `clear_noti` INT NOT NULL DEFAULT '0' AFTER `noti_read_status`;
ALTER TABLE `salary_expense` ADD `sal_dollar_number` DECIMAL(10,2) NOT NULL AFTER `net_earning`, ADD `sal_inr_val` DECIMAL(10,2) NOT NULL AFTER `sal_dollar_number`;
ALTER TABLE `employee_payment` ADD `esic_amt` DECIMAL(10,2) NOT NULL AFTER `emp_epf`;




-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2020 at 06:57 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets_bank_details`
--

CREATE TABLE `assets_bank_details` (
  `ab_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `bank_acc_no` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `acc_type` varchar(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `swift_code` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` datetime NOT NULL,
  `updatedat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets_bank_details`
--
ALTER TABLE `assets_bank_details`
  ADD PRIMARY KEY (`ab_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets_bank_details`
--
ALTER TABLE `assets_bank_details`
  MODIFY `ab_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `account_jv` CHANGE `date` `date` DATE NOT NULL;

ALTER TABLE `account_jv` CHANGE `date` `date` VARCHAR(255) NOT NULL;









-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2020 at 09:56 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `connections`
--

CREATE TABLE `connections` (
  `conn_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `conn_date` date NOT NULL,
  `client` enum('0','1') NOT NULL DEFAULT '0',
  `vendor` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `connections`
--
ALTER TABLE `connections`
  ADD PRIMARY KEY (`conn_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `connections`
--
ALTER TABLE `connections`
  MODIFY `conn_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `connections` ADD `message` VARCHAR(255) NOT NULL AFTER `vendor`;
ALTER TABLE `connections` ADD `bus_id_connected_to` INT NOT NULL AFTER `gst_id`;


ALTER TABLE `connections` ADD `accepted` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `status`;



ALTER TABLE `my_alerts` CHANGE `alert_reminder` `alert_reminder` DATE NOT NULL;
ALTER TABLE `my_alerts` ADD `alert_date_time` DATETIME NOT NULL AFTER `alert_occasion`;




ALTER TABLE `my_alerts` CHANGE `alert_date_time` `alert_date_time` DATE NOT NULL;
ALTER TABLE `my_alerts` ADD `alert_time` VARCHAR(255) NOT NULL AFTER `alert_date_time`;








/*28Feb*/
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 11:14 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `offer_number` int(11) NOT NULL,
  `offer_date` date NOT NULL,
  `offer_title` varchar(255) NOT NULL,
  `offer_image` varchar(255) NOT NULL,
  `offer_description` varchar(255) NOT NULL,
  `offer_category` varchar(255) NOT NULL,
  `promo_code` varchar(255) NOT NULL,
  `website_link` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `offers` ADD `bus_id` INT NOT NULL AFTER `id`, ADD `reg_id` INT NOT NULL AFTER `bus_id`;

ALTER TABLE `offers` ADD `start_time` VARCHAR(255) NOT NULL AFTER `end_date`, ADD `end_time` VARCHAR(255) NOT NULL AFTER `start_time`;

ALTER TABLE `offers` ADD `email` VARCHAR(255) NOT NULL AFTER `contact_name`;
ALTER TABLE `offers` ADD `offer_image_filename` VARCHAR(255) NOT NULL AFTER `offer_image`;
ALTER TABLE `connections` CHANGE `bus_id_connected_to` `bus_id_connected_to` VARCHAR(255) NOT NULL;




-- 11th march
-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2020 at 05:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `event_number` varchar(255) NOT NULL,
  `event_date` date NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_image` varchar(255) NOT NULL,
  `event_address` varchar(255) NOT NULL,
  `event_city` varchar(255) NOT NULL,
  `event_nature` varchar(255) NOT NULL,
  `event_description` varchar(255) NOT NULL,
  `website_link` varchar(255) NOT NULL,
  `entry_fee` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `bus_id`, `reg_id`, `event_number`, `event_date`, `event_name`, `event_image`, `event_address`, `event_city`, `event_nature`, `event_description`, `website_link`, `entry_fee`, `start_date`, `end_date`, `contact_name`, `company_name`, `email`, `mobile_no`, `status`, `date_created`, `date_updated`) VALUES
(2, 2, 3, 'EV1', '2020-03-11', 'asd', 'C:\\fakepath\\Mind-and-Sleep.png', '402, 4th Floor, Capri Tower, Kherwadi, Bandra East', 'Mumbai', 'Accounting & Taxation', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s De Finibus Bonorum e', 'www.jll.com', '43', '2020-03-12', '2020-03-28', 'Nimesh Admin', 'JLL Enterprises', 'nimesh@xebra.in', '9867209679', '1', '2020-03-11 18:37:06', '2020-03-11 18:37:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
