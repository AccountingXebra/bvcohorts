-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2019 at 01:49 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eazyinvoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_master`
--

CREATE TABLE `employee_master` (
  `emp_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `permanent_address` varchar(250) NOT NULL,
  `city` varchar(25) NOT NULL,
  `pincode` varchar(25) NOT NULL,
  `local_address` varchar(250) NOT NULL,
  `birthday` varchar(25) NOT NULL,
  `birthday_reminder` varchar(25) NOT NULL,
  `anniversary` varchar(25) NOT NULL,
  `anniversary_reminder` varchar(25) NOT NULL,
  `employee_id` varchar(25) NOT NULL,
  `employee_email_id` varchar(250) NOT NULL,
  `employee_mobile_no` varchar(25) NOT NULL,
  `blood_group` varchar(25) NOT NULL,
  `allergies` varchar(25) NOT NULL,
  `medical_concern` varchar(25) NOT NULL,
  `emergency_contact_name` varchar(25) NOT NULL,
  `emergency_contact_number` varchar(25) NOT NULL,
  `designation` varchar(25) NOT NULL,
  `department` varchar(25) NOT NULL,
  `location` varchar(25) NOT NULL,
  `reporting_to` varchar(25) NOT NULL,
  `date_of_joining` varchar(25) NOT NULL,
  `date_of_confirmation` varchar(25) NOT NULL,
  `confirmation_reminder` varchar(25) NOT NULL,
  `date_of_approval` varchar(25) NOT NULL,
  `approval_reminder` varchar(25) NOT NULL,
  `pan` varchar(25) NOT NULL,
  `pf_no` varchar(25) NOT NULL,
  `bank_acc_no` varchar(25) NOT NULL,
  `bank_name` varchar(25) NOT NULL,
  `bank_ifsc_code` varchar(25) NOT NULL,
  `branch_name` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  `date_of_exit` varchar(25) NOT NULL,
  `emp_profile_image` varchar(250) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `country_local` varchar(25) NOT NULL,
  `state_local` varchar(25) NOT NULL,
  `city_local` varchar(25) NOT NULL,
  `pincode_local` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_master`
--

INSERT INTO `employee_master` (`emp_id`, `reg_id`, `bus_id`, `status`, `createdat`, `updatedat`, `title`, `first_name`, `last_name`, `permanent_address`, `city`, `pincode`, `local_address`, `birthday`, `birthday_reminder`, `anniversary`, `anniversary_reminder`, `employee_id`, `employee_email_id`, `employee_mobile_no`, `blood_group`, `allergies`, `medical_concern`, `emergency_contact_name`, `emergency_contact_number`, `designation`, `department`, `location`, `reporting_to`, `date_of_joining`, `date_of_confirmation`, `confirmation_reminder`, `date_of_approval`, `approval_reminder`, `pan`, `pf_no`, `bank_acc_no`, `bank_name`, `bank_ifsc_code`, `branch_name`, `state`, `country`, `date_of_exit`, `emp_profile_image`, `gst_id`, `currency`, `country_local`, `state_local`, `city_local`, `pincode_local`) VALUES
(1, 73, 152, 'Active', '2019-06-17 07:36:09', '2019-06-25 10:50:26', 'Mr', 'manoj', 'rte', 'MD/s-72,Sector-G,hcsjabjhcbsajbcsahcbahcvsahjcvsavcsavcbsavcbjasvhhbjvcsjavsjvasvjcsa', '5909', '226012', 'MD/s-72,Sector-G', '', '', '', '', '16BCE0955', 'vishvam.vyas2016@vitstude', 'w', 'AB+', 'none', 'none', 'ubuub', '9532998084', 'Student', 'web', 'Maharashtra', 'aaiu', '18/06/2019', '19/06/2019', 'One Week Before', '19/06/2019', 'On That Day', 'w', '1516516', 'chgtcty', 'tydytd', 'TYDYT', 'YEYT', '42', '1', '18/06/2019', '4W858xv1.jpg', 92, 'Dollars $', '1', '42', '5909', '226012'),
(15, 73, 152, 'Active', '2019-06-21 09:19:58', '2019-06-21 09:39:49', 'Mr', 'Vishvam', 'Vyas', 'MD/s-72,Sector-G', '5912', '226012', 'MD/s-72,Sector-G', '', '', '', '', 'g', 'vishvam.vyas2016@vitstude', 'jk', 'AB+', ' j', 'jkjnjk', 'kjnkj', 'jjlk', 'jkl', 'jkk', 'kj', 'kjkj', 'jk', 'kjkj', 'One Week Before', 'kj', 'One Week Before', 'jkkj', 'jjbjl', 'jhk', 'bjhkb', 'kjb', 'hjjhbkjh', '42', '1', '29/06/2019', '73f051252649542ab16ecf894f5c9b18.jpg', 92, 'Dollars $', '1', '42', '5912', '226012'),
(16, 73, 152, 'Active', '2019-06-21 09:23:57', '2019-06-21 10:25:14', 'Mr', 'hjbhuy', 'yugy', 'gyu', '20421', 'vh', 'gyu', '', '', '', '', 'vhjv', 'mvhjk', 'hvh', 'O-', 'vjhvkhv', 'ghj', 'gh', 'chcgg', 'jggchcv', 'kghgvg', 'vg', 'aaiu', '12/06/2019', '13/06/2019', 'One Week Before', '27/06/2019', 'On That Day', ' ghg', 'gvgv', 'gvg', 'ghvgh', 'ghvh', 'mgvgg', '1409', '83', '26/06/2019', '3e9b58be447605795ba12e31714a5e16.jpg', 92, '', '83', '1409', '20421', 'vh'),
(17, 73, 152, 'Inactive', '2019-06-25 07:52:56', '2019-06-25 07:57:47', 'Mr', 'Vishvam', 'w', 'w', '2', '226012', 'w', '', '', '', '', 'q', 'qq', 'q', '', 'q', 'qq', 'q', 'qq', 'q', 'qq', 'q', 'qq', 'q', 'q', '', 'q', '', 'q', 'qq', 'q', 'q', 'q', 'qq', '1', '101', '', '3e9b58be447605795ba12e31714a5e16.jpg', 92, '47', '101', '1', '2', '226012'),
(18, 73, 152, 'Inactive', '2019-06-25 07:54:43', '2019-06-25 07:57:47', 'Mr', 'q', 'q', 'qq', '5909', 'q', 'qq', '', '', '', '', 'q', 'q', 'qq', '', 'qq', 'q', '', '', 'q', 'web', 'q', 'aaiu', '12/06/2019', '13/06/2019', 'One Week Before', '03/06/2019', 'One Week Before', 'q', 'q', 'q', 'qq', 'q', 'qqq', '74', '2', '25/06/2019', '132f2927a531a57359adc8fe589e1278--one-piece-nami-straws.jpg', 92, '', '2', '74', '5909', 'q'),
(19, 73, 152, 'Active', '2019-06-25 08:01:19', '2019-06-25 08:01:19', 'Mr', 'q', 'qq', 'q', '31691', '226012', 'q', '', '', '', '', '16BCE0955', 'r4', '8765387241', 'AB+', 'kjv', 'bhbhhbhjbh', 'ihe', '9532998084', 'Student', 'web', 'Maharashtra', 'aaiu', '20/06/2019', '18/06/2019', 'One Month Before', '27/06/2019', 'On That Day', 'ca', '1516516', 'eee', 'eee', 'eee', 'eeee', '', '101', '24/06/2019', '73f051252649542ab16ecf894f5c9b18.jpg', 92, '', '101', '', '31691', '226012'),
(20, 73, 152, 'Active', '2019-06-25 08:04:33', '2019-06-25 08:04:33', 'Mr', 'r', 'r', 'r', '13561', 'rt', 'r', '', '', '', '', 'tft', 'jjijoi', 'hjhjjh', 'B-', 'gyygyjh', 'ygyugy', 'yyuyugyu', 'hjjhv', 'hbhj', 'hhjbjhb', 'Maharashtra', 'aaiu', '24/06/2019', '18/06/2019', 'On That Day', '03/06/2019', 'One Week Before', 'vhjv', 'hvhg', 'vhgv', 'ghvgh', 'vghvgh', 'vghvghvghhvb', '819', '49', '27/06/2019', '50490efc83e588ed95bbe14936c58368b41985fd_hq.jpg', 92, '4', '49', '819', '13561', 'rt');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payment`
--

CREATE TABLE `employee_payment` (
  `empay_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `payment_code` varchar(100) DEFAULT NULL,
  `emp_pay_mnth` varchar(25) NOT NULL,
  `payment_date` varchar(25) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `pay_mode` varchar(100) DEFAULT NULL,
  `pay_bank_id` int(11) NOT NULL,
  `payment_amount` decimal(10,2) NOT NULL,
  `payment_tds_amount` decimal(10,2) NOT NULL,
  `prof_tax_amount` decimal(10,2) NOT NULL,
  `paymentc_notes` text NOT NULL,
  `status` enum('Pending','Paid') NOT NULL DEFAULT 'Paid',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `emp_id` int(11) NOT NULL,
  `employee_name` varchar(25) NOT NULL,
  `payment_bank_charges` varchar(25) NOT NULL,
  `payment_forex_loss` varchar(25) NOT NULL,
  `payment_forex_gain` varchar(25) NOT NULL,
  `sal_exp_id` int(11) NOT NULL,
  `exp_pay_date` varchar(25) NOT NULL,
  `salary_exp_no` varchar(25) NOT NULL,
  `net_earning` varchar(25) NOT NULL,
  `salary_exp_date` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_payment`
--

INSERT INTO `employee_payment` (`empay_id`, `bus_id`, `reg_id`, `gst_id`, `payment_code`, `emp_pay_mnth`, `payment_date`, `payment_status`, `pay_mode`, `pay_bank_id`, `payment_amount`, `payment_tds_amount`, `prof_tax_amount`, `paymentc_notes`, `status`, `createdat`, `updatedat`, `emp_id`, `employee_name`, `payment_bank_charges`, `payment_forex_loss`, `payment_forex_gain`, `sal_exp_id`, `exp_pay_date`, `salary_exp_no`, `net_earning`, `salary_exp_date`) VALUES
(3, 152, 73, 92, 'MPMT000002', 'October', '10/06/2019', 'full_received', 'Cheque', 49, '231.00', '32.00', '122.00', 'vsvd', 'Paid', '2019-06-20 21:17:13', '2019-06-20 21:17:13', 0, 'sdsaas', '2234', '', '', 0, '21/06/2019', '', '', ''),
(4, 152, 73, 92, 'MPMT000003', 'February', '08/06/2019', 'full_received', 'Cash', 49, '231.00', '78.00', '122.00', 'vjhcvwhcjw', 'Paid', '2019-06-21 06:00:19', '2019-06-21 06:00:19', 0, 'sdsaas', '2234', '', '', 0, '21/06/2019', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `employee_payment_list`
--

CREATE TABLE `employee_payment_list` (
  `emppayl_id` int(11) NOT NULL,
  `emppay_id` int(11) NOT NULL,
  `exp_payment_no` varchar(100) NOT NULL,
  `exp_amount` varchar(11) NOT NULL,
  `exp_conv_amt` varchar(11) NOT NULL,
  `emp_pay_date` varchar(25) NOT NULL,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `exp_cat` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_payment_list`
--

INSERT INTO `employee_payment_list` (`emppayl_id`, `emppay_id`, `exp_payment_no`, `exp_amount`, `exp_conv_amt`, `emp_pay_date`, `status`, `createdat`, `updatedat`, `exp_cat`) VALUES
(2, 2, '000001', '47.00', '', '24/05/6', 'Active', '2019-06-20 20:29:10', '2019-06-20 20:29:10', '  exp002 exp003 fdvdfvd'),
(3, 3, 'EV000002', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(4, 3, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(5, 3, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(6, 3, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(7, 3, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(8, 3, 'EV000004', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(9, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(10, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(11, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(12, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(13, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(14, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(15, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(16, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(17, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(18, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(19, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(20, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(21, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(22, 3, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(23, 3, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(24, 3, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(25, 3, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(26, 3, 'EV000007', '68.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(27, 3, 'EV000009', '92.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(28, 3, 'EV000010', '92.00', '', '0000-00-00', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(29, 3, 'EV000011', '92.00', '', '2019-11-03', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(30, 3, 'EV000012', '46.00', '', '2019-12-03', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(31, 3, 'EV000013', '46.00', '', '2019-12-03', 'Active', '2019-06-20 21:17:14', '2019-06-20 21:17:14', '  fdvdfvd'),
(32, 3, 'EV000013', '46.00', '', '2019-12-03', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(33, 3, 'EV000014', '46.00', '', '2019-12-03', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(34, 3, 'EV000014', '46.00', '', '2019-12-03', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(35, 3, 'EV000015', '527.00', '', '2019-04-11', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(36, 3, 'EV000016', '92.00', '', '2019-04-11', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(37, 3, 'EV000017', '92.00', '', '2019-04-11', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(38, 3, 'EV000018', '46.00', '', '2019-04-11', 'Active', '2019-06-20 21:17:15', '2019-06-20 21:17:15', '  fdvdfvd'),
(39, 4, 'EV000002', '0.00', '', '30/11/0', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(40, 4, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(41, 4, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(42, 4, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(43, 4, 'EV000003', '46.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(44, 4, 'EV000004', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(45, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(46, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(47, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(48, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(49, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(50, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:19', '2019-06-21 06:00:19', '  fdvdfvd'),
(51, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(52, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(53, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(54, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(55, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(56, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(57, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(58, 4, 'EV000005', '0.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(59, 4, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(60, 4, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(61, 4, 'EV000006', '68.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(62, 4, 'EV000007', '68.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(63, 4, 'EV000009', '92.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(64, 4, 'EV000010', '92.00', '', '0000-00-00', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(65, 4, 'EV000011', '92.00', '', '2019-11-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(66, 4, 'EV000012', '46.00', '', '2019-12-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(67, 4, 'EV000013', '46.00', '', '2019-12-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(68, 4, 'EV000013', '46.00', '', '2019-12-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(69, 4, 'EV000014', '46.00', '', '2019-12-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(70, 4, 'EV000014', '46.00', '', '2019-12-03', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(71, 4, 'EV000015', '527.00', '', '2019-04-11', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(72, 4, 'EV000016', '92.00', '', '2019-04-11', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(73, 4, 'EV000017', '92.00', '', '2019-04-11', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd'),
(74, 4, 'EV000018', '46.00', '', '2019-04-11', 'Active', '2019-06-21 06:00:20', '2019-06-21 06:00:20', '  fdvdfvd');

-- --------------------------------------------------------

--
-- Table structure for table `prof_tax`
--

CREATE TABLE `prof_tax` (
  `pt_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pt_pay_no` varchar(250) NOT NULL,
  `type_pt` varchar(25) NOT NULL,
  `prof_tax_form_id` varchar(25) NOT NULL,
  `loc` varchar(25) NOT NULL,
  `prox_tax_yr` varchar(25) NOT NULL,
  `tds_pay_amt` varchar(25) NOT NULL,
  `pt_pay_receiptchdate` varchar(25) NOT NULL,
  `pay_date` varchar(25) NOT NULL,
  `pay_status1` varchar(250) NOT NULL,
  `pay_mode` varchar(250) NOT NULL,
  `pay_bank_id` varchar(250) NOT NULL,
  `pay_amount` varchar(25) NOT NULL,
  `int_charge` varchar(25) NOT NULL,
  `pen_amount` varchar(25) NOT NULL,
  `com_money` varchar(25) NOT NULL,
  `fine` varchar(25) NOT NULL,
  `fees` varchar(25) NOT NULL,
  `amt_fortified` varchar(25) NOT NULL,
  `deposit` varchar(25) NOT NULL,
  `prof_notes` varchar(25) NOT NULL,
  `prox_tax_mnt` varchar(25) NOT NULL,
  `adv_pay` varchar(25) NOT NULL,
  `challanno` varchar(25) NOT NULL,
  `tds_pay_receiptchdate` varchar(25) NOT NULL,
  `total_amount_prof` varchar(234) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prof_tax`
--

INSERT INTO `prof_tax` (`pt_id`, `reg_id`, `bus_id`, `gst_id`, `status`, `createdat`, `updatedat`, `pt_pay_no`, `type_pt`, `prof_tax_form_id`, `loc`, `prox_tax_yr`, `tds_pay_amt`, `pt_pay_receiptchdate`, `pay_date`, `pay_status1`, `pay_mode`, `pay_bank_id`, `pay_amount`, `int_charge`, `pen_amount`, `com_money`, `fine`, `fees`, `amt_fortified`, `deposit`, `prof_notes`, `prox_tax_mnt`, `adv_pay`, `challanno`, `tds_pay_receiptchdate`, `total_amount_prof`) VALUES
(15, 73, 152, 92, 'Active', '2019-06-22 18:03:20', '2019-06-22 18:03:20', 'PTPMT000003', 'PTEC ATC', 'FORM_VIII', 'ALIBAUGH', '2019', '123', '22/06/2019', '11/06/2019', 'Full Payment', 'cheque', '49', '12', '12', '12', '121', '1', '21', '2', '121221', 'jkjngdhed', 'October', '212', 'bhjkjb', '22/06/2019', ''),
(16, 73, 152, 92, 'Active', '2019-06-23 06:11:22', '2019-06-23 06:11:22', 'PTPMT000003', 'PTRC ATC', 'FORM_IIIB', 'AKOLA', '2019', '123', '23/06/2019', '23/06/2019', 'Full Payment', 'cheque', '49', '1', '1', '11', '1', '1', '1', '11', '1', '1gvgh', 'October', '1', '1234skmvml', '23/06/2019', '29');

-- --------------------------------------------------------

--
-- Table structure for table `prof_tax_images`
--

CREATE TABLE `prof_tax_images` (
  `pt_id` int(11) NOT NULL,
  `challan_pic` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prof_tax_images`
--

INSERT INTO `prof_tax_images` (`pt_id`, `challan_pic`) VALUES
(12, '1d0411068b2e054f9b1f67fc9'),
(12, '3e9b58be447605795ba12e317'),
(12, '4W858xv.jpg'),
(12, '7d96db35f23f02a77e9b6cb5d'),
(12, '73f051252649542ab16ecf894'),
(12, '132f2927a531a57359adc8fe5'),
(12, '264b21641384db56788591137'),
(12, '1541-joker-batman-and-two'),
(12, '50490efc83e588ed95bbe1493'),
(12, '293753eb2b9e7fb8175d0db68'),
(12, '308030.jpg'),
(12, '424433-bigthumbnail.jpg'),
(13, '1d0411068b2e054f9b1f67fc9'),
(13, '3e9b58be447605795ba12e317'),
(13, '4W858xv.jpg'),
(13, '7d96db35f23f02a77e9b6cb5d'),
(13, '73f051252649542ab16ecf894'),
(13, '132f2927a531a57359adc8fe5'),
(13, '264b21641384db56788591137'),
(13, '1541-joker-batman-and-two'),
(13, '50490efc83e588ed95bbe1493'),
(13, '293753eb2b9e7fb8175d0db68'),
(13, '308030.jpg'),
(13, '424433-bigthumbnail.jpg'),
(16, 'tAG_12516.jpg'),
(16, 'This-World-Is-Rotten-l.jp'),
(16, 'thumb-1920-316877.jpg'),
(16, 'timer.png'),
(16, 'xkfZJM.jpg'),
(16, 'ZzlC6G.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `prof_tax_location`
--

CREATE TABLE `prof_tax_location` (
  `Locations` varchar(250) NOT NULL,
  `pt_loc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `prof_tax_location`
--

INSERT INTO `prof_tax_location` (`Locations`, `pt_loc_id`) VALUES
('AHMEDNAGAR', 4),
('AKOLA', 5),
('AMRAVATI', 6),
('AURANGABAD', 7),
('ALIBAUGH', 8),
('BARSHI', 9),
('BEED', 10),
('BHANDARA', 11),
('BHAYENDAR', 12),
('CHANDRAPUR', 13),
('DHULE', 14),
('GADCHIROLI', 15),
('GONDIA', 16),
('HINGOLI', 17),
('JALGAON', 18),
('JALNA', 19),
('KALYAN', 20),
('KHAMGAON', 21),
('LATUR', 22),
('MALEGAON', 23),
('MAZGAON', 24),
('NANDUBAR', 25),
('NAGPUR', 26),
('NASHIK', 27),
('OROS', 28),
('OSMANBAD', 29),
('PALGHAR', 30),
('PARBHANI', 31),
('PUNE', 32),
('RAIGAD', 33),
('RATNAGIRI', 34),
('SANGLI', 35),
('SATARA', 36),
('MAZGAON', 37),
('SOLAPUR', 38),
('THANE', 39),
('WARDHA', 40),
('WASHIM', 41),
('YAVATMAL', 42);

-- --------------------------------------------------------

--
-- Table structure for table `salary_expense`
--

CREATE TABLE `salary_expense` (
  `salary_exp_no` varchar(25) NOT NULL,
  `salary_exp_date` varchar(25) NOT NULL,
  `salary_month` varchar(25) NOT NULL,
  `salary_year` varchar(25) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `employee_id` varchar(25) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `sal_exp_id` int(11) NOT NULL,
  `employee_na` varchar(25) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `designation` varchar(25) NOT NULL,
  `location` varchar(25) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `Status` varchar(25) NOT NULL,
  `net_earning` varchar(25) NOT NULL,
  `working_days` varchar(25) NOT NULL,
  `month_days` varchar(25) NOT NULL,
  `sstatus` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_expense`
--

INSERT INTO `salary_expense` (`salary_exp_no`, `salary_exp_date`, `salary_month`, `salary_year`, `bus_id`, `reg_id`, `employee_id`, `gst_id`, `sal_exp_id`, `employee_na`, `currency`, `designation`, `location`, `emp_id`, `Status`, `net_earning`, `working_days`, `month_days`, `sstatus`) VALUES
('SXP000001', '19/06/2019', 'October', '2019', 152, 73, 'g', 92, 2, 'manoj', 'Afghanis ?', 'Student', 'Maharashtra', 5, 'Paid', '122', '25', '24', 'Active'),
('SXP000002', '20/06/2019', 'October', '2019', 152, 73, '16BCE0955', 92, 3, 'Mr manoj ', 'bbj', 'Student', 'Maharashtra', 1, 'Pending', '13', '24', '24', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `salary_expense_add`
--

CREATE TABLE `salary_expense_add` (
  `particulars_add` varchar(25) NOT NULL,
  `amount_add` varchar(11) NOT NULL,
  `recurring_month_add` varchar(123) NOT NULL,
  `sal_exp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_expense_add`
--

INSERT INTO `salary_expense_add` (`particulars_add`, `amount_add`, `recurring_month_add`, `sal_exp_id`) VALUES
('1st', '123', 'Yes', 2),
('2nd', '233', 'Yes', 2),
('2nd', '123', 'Yes', 3),
('2nd2', '123', 'No', 3);

-- --------------------------------------------------------

--
-- Table structure for table `salary_expense_ded`
--

CREATE TABLE `salary_expense_ded` (
  `particulars_ded` varchar(25) NOT NULL,
  `amount_ded` varchar(25) NOT NULL,
  `recurring_month_ded` varchar(25) NOT NULL,
  `sal_exp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_expense_ded`
--

INSERT INTO `salary_expense_ded` (`particulars_ded`, `amount_ded`, `recurring_month_ded`, `sal_exp_id`) VALUES
('1st', '234', 'Yes', 2),
('1st', '233', 'Yes', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tds_employee`
--

CREATE TABLE `tds_employee` (
  `tdsemp_id` int(11) NOT NULL,
  `tds_emp_no` varchar(256) NOT NULL,
  `tds_emp_date` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `tds_expense_head_emp` varchar(250) NOT NULL,
  `type_of_tds_emp` varchar(100) NOT NULL,
  `tds_amount_emp` varchar(20) NOT NULL,
  `tds_emp_challanno` varchar(30) NOT NULL,
  `tds_emp_challandate` varchar(10) NOT NULL,
  `payment_emp_date` varchar(10) NOT NULL,
  `pay_emp_status` varchar(20) NOT NULL,
  `mode_of_payment` varchar(15) NOT NULL,
  `bank_name` varchar(30) NOT NULL,
  `pay_amount` varchar(30) NOT NULL,
  `int_charge` varchar(30) NOT NULL,
  `pen_charge` varchar(30) NOT NULL,
  `pay_other` varchar(30) NOT NULL,
  `tds_emp_notes` varchar(250) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gst_id` int(11) NOT NULL,
  `tax_applicable` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tds_employee`
--

INSERT INTO `tds_employee` (`tdsemp_id`, `tds_emp_no`, `tds_emp_date`, `reg_id`, `bus_id`, `tds_expense_head_emp`, `type_of_tds_emp`, `tds_amount_emp`, `tds_emp_challanno`, `tds_emp_challandate`, `payment_emp_date`, `pay_emp_status`, `mode_of_payment`, `bank_name`, `pay_amount`, `int_charge`, `pen_charge`, `pay_other`, `tds_emp_notes`, `status`, `createdat`, `updatedat`, `gst_id`, `tax_applicable`) VALUES
(8, 'TMPMT000001', '25/06/2019', 73, 152, '92B', '', '122', 'babcjhak2', '18/06/2019', '19/06/2019', 'Full Payment', 'cash', '', '1', '1', '312', '123', ',ckljcqa', 'Active', '2019-06-25 08:34:14', '2019-06-25 08:34:14', 92, '(0020) Company Deductees');

-- --------------------------------------------------------

--
-- Table structure for table `tds_employee_images`
--

CREATE TABLE `tds_employee_images` (
  `tdsemp_id` int(11) NOT NULL,
  `challan_pic` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tds_employee_images`
--

INSERT INTO `tds_employee_images` (`tdsemp_id`, `challan_pic`) VALUES
(1, '1d0411068b2e054f9b1f67fc9'),
(1, '3e9b58be447605795ba12e317'),
(1, '4W858xv.jpg'),
(1, '7d96db35f23f02a77e9b6cb5d'),
(1, '73f051252649542ab16ecf894'),
(1, '132f2927a531a57359adc8fe5'),
(2, '73f051252649542ab16ecf894'),
(8, '3e9b58be447605795ba12e317');

-- --------------------------------------------------------

--
-- Table structure for table `tds_employee_list`
--

CREATE TABLE `tds_employee_list` (
  `tdsemp_id` int(11) NOT NULL,
  `tds_expense_head_emp_l` varchar(234) NOT NULL,
  `type_of_tds_emp_l` varchar(234) NOT NULL,
  `tds_amount_l` varchar(234) NOT NULL,
  `tdsemp_id_l` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tds_employee_list`
--

INSERT INTO `tds_employee_list` (`tdsemp_id`, `tds_expense_head_emp_l`, `type_of_tds_emp_l`, `tds_amount_l`, `tdsemp_id_l`) VALUES
(8, '92A', '', '8988', 10),
(8, '92A', '', '122', 11),
(8, '92B', '', '1234', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee_master`
--
ALTER TABLE `employee_master`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `employee_payment`
--
ALTER TABLE `employee_payment`
  ADD PRIMARY KEY (`empay_id`);

--
-- Indexes for table `employee_payment_list`
--
ALTER TABLE `employee_payment_list`
  ADD PRIMARY KEY (`emppayl_id`);

--
-- Indexes for table `prof_tax`
--
ALTER TABLE `prof_tax`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `prof_tax_location`
--
ALTER TABLE `prof_tax_location`
  ADD PRIMARY KEY (`pt_loc_id`);

--
-- Indexes for table `salary_expense`
--
ALTER TABLE `salary_expense`
  ADD PRIMARY KEY (`sal_exp_id`);

--
-- Indexes for table `tds_employee`
--
ALTER TABLE `tds_employee`
  ADD PRIMARY KEY (`tdsemp_id`);

--
-- Indexes for table `tds_employee_list`
--
ALTER TABLE `tds_employee_list`
  ADD PRIMARY KEY (`tdsemp_id_l`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee_master`
--
ALTER TABLE `employee_master`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `employee_payment`
--
ALTER TABLE `employee_payment`
  MODIFY `empay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee_payment_list`
--
ALTER TABLE `employee_payment_list`
  MODIFY `emppayl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `prof_tax`
--
ALTER TABLE `prof_tax`
  MODIFY `pt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `prof_tax_location`
--
ALTER TABLE `prof_tax_location`
  MODIFY `pt_loc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `salary_expense`
--
ALTER TABLE `salary_expense`
  MODIFY `sal_exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tds_employee`
--
ALTER TABLE `tds_employee`
  MODIFY `tdsemp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tds_employee_list`
--
ALTER TABLE `tds_employee_list`
  MODIFY `tdsemp_id_l` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
