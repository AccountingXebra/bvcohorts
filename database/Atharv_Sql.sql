-- --------------------------------------------------------

--
-- Table structure for table `expense_debit_note`
--

CREATE TABLE `expense_debit_note` (
  `debit_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `debit_note_type` varchar(50) NOT NULL,
  `debit_note_no` varchar(100) NOT NULL,
  `debit_note_date` date NOT NULL,
  `debit_po_no` varchar(100) NOT NULL,
  `debit_po_date` date NOT NULL,
  `debit_note_ref_no` varchar(100) NOT NULL,
  `debit_note_ref_date` date NOT NULL,
  `debit_invoice_no` varchar(100) NOT NULL,
  `debit_invoice_date` date NOT NULL,
  `debit_reason` varchar(100) NOT NULL,
  `debit_gst_id` int(11) NOT NULL,
  `debit_place` varchar(250) NOT NULL,
  `debit_export_currency` int(11) NOT NULL,
  `debit_export_type` int(11) NOT NULL,
  `debit_dollar_number` int(11) NOT NULL,
  `debit_inr_value` decimal(10,2) NOT NULL DEFAULT '1.00',
  `debit_billing_address` text NOT NULL,
  `debit_billing_country` int(11) NOT NULL,
  `debit_billing_state` int(11) NOT NULL,
  `debit_billing_city` int(11) NOT NULL,
  `debit_billing_zipcode` varchar(100) NOT NULL,
  `debit_shipping_address` text NOT NULL,
  `debit_shipping_country` int(11) NOT NULL,
  `debit_shipping_state` int(11) NOT NULL,
  `debit_shipping_city` int(11) NOT NULL,
  `debit_shipping_zipcode` varchar(100) NOT NULL,
  `debit_signature` varchar(250) DEFAULT NULL,
  `debit_sign_name` varchar(100) NOT NULL,
  `debit_sign_designation` varchar(100) NOT NULL,
  `debit_notes` text NOT NULL,
  `due_date` date NOT NULL,
  `debit_terms` text NOT NULL,
  `debit_discount_amt` decimal(10,2) NOT NULL,
  `debit_taxable_amt` decimal(10,2) NOT NULL,
  `debit_cgst_total` decimal(10,2) NOT NULL,
  `debit_sgst_total` decimal(10,2) NOT NULL,
  `debit_igst_total` decimal(10,2) NOT NULL,
  `debit_cess_total` decimal(10,2) NOT NULL,
  `debit_other_total` decimal(10,2) NOT NULL,
  `debit_grant_total` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_debit_note`
--

INSERT INTO `expense_debit_note` (`debit_id`, `cust_id`, `bus_id`, `reg_id`, `gst_id`, `debit_note_type`, `debit_note_no`, `debit_note_date`, `debit_po_no`, `debit_po_date`, `debit_note_ref_no`, `debit_note_ref_date`, `debit_invoice_no`, `debit_invoice_date`, `debit_reason`, `debit_gst_id`, `debit_place`, `debit_export_currency`, `debit_export_type`, `debit_dollar_number`, `debit_inr_value`, `debit_billing_address`, `debit_billing_country`, `debit_billing_state`, `debit_billing_city`, `debit_billing_zipcode`, `debit_shipping_address`, `debit_shipping_country`, `debit_shipping_state`, `debit_shipping_city`, `debit_shipping_zipcode`, `debit_signature`, `debit_sign_name`, `debit_sign_designation`, `debit_notes`, `due_date`, `debit_terms`, `debit_discount_amt`, `debit_taxable_amt`, `debit_cgst_total`, `debit_sgst_total`, `debit_igst_total`, `debit_cess_total`, `debit_other_total`, `debit_grant_total`, `status`, `createdat`, `updatedat`) VALUES
(4, 61, 154, 86, 97, 'CN', 'DBN1', '2019-05-13', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', 'Order Revised', 98, '22', 47, 0, 1, '1.00', 'Nowhere', 101, 22, 2707, '400053', '', 0, 0, 0, '', NULL, '', '', 'gkgjhghjg', '0000-00-00', 'kjhlhlih;oj;', '789.00', '338481.00', '3968689.73', '3968689.73', '0.00', '15455042.46', '30463.29', '23761366.21', 'Active', '2019-05-13 07:02:10', '2019-05-13 07:02:10');

-- --------------------------------------------------------

--
-- Table structure for table `expense_debit_note_list`
--

CREATE TABLE `expense_debit_note_list` (
  `dbl_id` int(11) NOT NULL,
  `debit_id` int(11) NOT NULL,
  `dbl_invoice_no` varchar(100) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `dbl_particulars` varchar(255) DEFAULT NULL,
  `dbl_hsn_sac_no` varchar(100) DEFAULT NULL,
  `dbl_quantity` int(11) NOT NULL,
  `dbl_uom` int(11) NOT NULL,
  `dbl_rate` decimal(10,2) NOT NULL,
  `dbl_discount` decimal(10,2) NOT NULL,
  `dbl_taxable_amt` decimal(10,2) NOT NULL,
  `dbl_igst` int(11) NOT NULL,
  `dbl_igst_amt` decimal(10,2) NOT NULL,
  `dbl_cgst` int(11) NOT NULL,
  `dbl_cgst_amt` decimal(10,2) NOT NULL,
  `dbl_sgst` int(11) NOT NULL,
  `dbl_sgst_amt` decimal(10,2) NOT NULL,
  `dbl_cess` int(11) NOT NULL,
  `dbl_cess_amt` decimal(10,2) NOT NULL,
  `dbl_other` int(11) NOT NULL,
  `dbl_other_amt` decimal(10,2) NOT NULL,
  `dbl_amount` decimal(10,2) NOT NULL,
  `dbl_grand_total` decimal(10,2) NOT NULL,
  `dbl_invoice_date` date NOT NULL,
  `dbl_service_type` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_debit_note_list`
--

INSERT INTO `expense_debit_note_list` (`dbl_id`, `debit_id`, `dbl_invoice_no`, `service_id`, `dbl_particulars`, `dbl_hsn_sac_no`, `dbl_quantity`, `dbl_uom`, `dbl_rate`, `dbl_discount`, `dbl_taxable_amt`, `dbl_igst`, `dbl_igst_amt`, `dbl_cgst`, `dbl_cgst_amt`, `dbl_sgst`, `dbl_sgst_amt`, `dbl_cess`, `dbl_cess_amt`, `dbl_other`, `dbl_other_amt`, `dbl_amount`, `dbl_grand_total`, `dbl_invoice_date`, `dbl_service_type`, `status`, `createdat`, `updatedat`) VALUES
(3, 4, NULL, 41, 'gkgllhlihlh', '2345', 5, 0, '67854.00', '789.00', '338481.00', 0, '0.00', 1173, '3968689.73', 1173, '3968689.73', 4566, '15455042.46', 9, '30463.29', '23761366.21', '0.00', '0000-00-00', 1, 'Active', '2019-05-13 07:02:10', '2019-05-13 07:02:10');



-- --------------------------------------------------------

--
-- Table structure for table `expense_purchase_order`
--

CREATE TABLE `expense_purchase_order` (
  `po_id` int(11) NOT NULL,
  `po_type` varchar(5) DEFAULT NULL,
  `cust_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `po_no` varchar(100) NOT NULL,
  `po_date` date NOT NULL,
  `ref_doc_no` varchar(100) NOT NULL,
  `po_start_date` date NOT NULL,
  `po_end_date` varchar(100) NOT NULL,
  `po_gst_id` int(11) NOT NULL,
  `po_place` varchar(250) NOT NULL,
  `po_export_currency` int(11) NOT NULL,
  `po_export_type` int(11) NOT NULL,
  `po_dollar_number` int(11) NOT NULL,
  `po_inr_value` decimal(10,2) NOT NULL DEFAULT '1.00',
  `po_billing_address` text NOT NULL,
  `po_billing_country` int(11) NOT NULL,
  `po_billing_state` int(11) NOT NULL,
  `po_billing_city` int(11) NOT NULL,
  `po_billing_zipcode` varchar(100) NOT NULL,
  `po_shipping_address` text NOT NULL,
  `po_shipping_country` int(11) NOT NULL,
  `po_shipping_state` int(11) NOT NULL,
  `po_shipping_city` int(11) NOT NULL,
  `po_shipping_zipcode` varchar(100) NOT NULL,
  `po_signature` varchar(250) DEFAULT NULL,
  `po_sign_name` varchar(100) NOT NULL,
  `po_sign_designation` varchar(100) NOT NULL,
  `purchase_orders` text NOT NULL,
  `due_date` date NOT NULL,
  `po_terms` text NOT NULL,
  `po_notes` varchar(255) DEFAULT NULL,
  `po_discount_amt` decimal(10,2) NOT NULL,
  `po_taxable_amt` decimal(10,2) NOT NULL,
  `po_cgst_total` decimal(10,2) NOT NULL,
  `po_sgst_total` decimal(10,2) NOT NULL,
  `po_igst_total` decimal(10,2) NOT NULL,
  `po_cess_total` decimal(10,2) NOT NULL,
  `po_other_total` decimal(10,2) NOT NULL,
  `po_grant_total` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_purchase_order`
--

INSERT INTO `expense_purchase_order` (`po_id`, `po_type`, `cust_id`, `bus_id`, `reg_id`, `gst_id`, `po_no`, `po_date`, `ref_doc_no`, `po_start_date`, `po_end_date`, `po_gst_id`, `po_place`, `po_export_currency`, `po_export_type`, `po_dollar_number`, `po_inr_value`, `po_billing_address`, `po_billing_country`, `po_billing_state`, `po_billing_city`, `po_billing_zipcode`, `po_shipping_address`, `po_shipping_country`, `po_shipping_state`, `po_shipping_city`, `po_shipping_zipcode`, `po_signature`, `po_sign_name`, `po_sign_designation`, `purchase_orders`, `due_date`, `po_terms`, `po_notes`, `po_discount_amt`, `po_taxable_amt`, `po_cgst_total`, `po_sgst_total`, `po_igst_total`, `po_cess_total`, `po_other_total`, `po_grant_total`, `status`, `createdat`, `updatedat`) VALUES
(5, 'PO', 61, 154, 86, 97, 'PON1', '2019-05-13', '1970-01-01', '2019-05-02', '2019-05-30', 98, '22', 47, 0, 1, '1.00', 'Nowhere', 101, 22, 2707, '400053', '', 0, 0, 0, '', NULL, '', '', '', '0000-00-00', 'vsjhgejh', 'kjgvjge', '789.00', '338481.00', '3968689.73', '3968689.73', '0.00', '15455042.46', '30463.29', '23761366.21', 'Active', '2019-05-13 14:15:47', '2019-05-13 14:15:47');

-- --------------------------------------------------------

--
-- Table structure for table `expense_purchase_order_list`
--

CREATE TABLE `expense_purchase_order_list` (
  `pol_id` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `pol_invoice_no` varchar(100) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `pol_particulars` varchar(255) DEFAULT NULL,
  `pol_hsn_sac_no` varchar(100) DEFAULT NULL,
  `pol_quantity` int(11) NOT NULL,
  `pol_uom` int(11) NOT NULL,
  `pol_rate` decimal(10,2) NOT NULL,
  `pol_discount` decimal(10,2) NOT NULL,
  `pol_taxable_amt` decimal(10,2) NOT NULL,
  `pol_igst` int(11) NOT NULL,
  `pol_igst_amt` decimal(10,2) NOT NULL,
  `pol_cgst` int(11) NOT NULL,
  `pol_cgst_amt` decimal(10,2) NOT NULL,
  `pol_sgst` int(11) NOT NULL,
  `pol_sgst_amt` decimal(10,2) NOT NULL,
  `pol_cess` int(11) NOT NULL,
  `pol_cess_amt` decimal(10,2) NOT NULL,
  `pol_other` int(11) NOT NULL,
  `pol_other_amt` decimal(10,2) NOT NULL,
  `pol_amount` decimal(10,2) NOT NULL,
  `pol_grand_total` decimal(10,2) NOT NULL,
  `pol_invoice_date` date NOT NULL,
  `pol_service_type` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_purchase_order_list`
--

INSERT INTO `expense_purchase_order_list` (`pol_id`, `po_id`, `pol_invoice_no`, `service_id`, `pol_particulars`, `pol_hsn_sac_no`, `pol_quantity`, `pol_uom`, `pol_rate`, `pol_discount`, `pol_taxable_amt`, `pol_igst`, `pol_igst_amt`, `pol_cgst`, `pol_cgst_amt`, `pol_sgst`, `pol_sgst_amt`, `pol_cess`, `pol_cess_amt`, `pol_other`, `pol_other_amt`, `pol_amount`, `pol_grand_total`, `pol_invoice_date`, `pol_service_type`, `status`, `createdat`, `updatedat`) VALUES
(1, 4, NULL, 41, 'ewkjhdwkjbfwkjb', '2345', 5, 0, '67854.00', '897.00', '338373.00', 0, '0.00', 1173, '3967423.42', 1173, '3967423.42', 4566, '15450111.18', 9, '30453.57', '23753784.59', '0.00', '0000-00-00', 1, 'Active', '2019-05-13 07:41:01', '2019-05-13 07:41:01'),
(2, 5, NULL, 41, 'jwbefkjegkj', '2345', 5, 0, '67854.00', '789.00', '338481.00', 0, '0.00', 1173, '3968689.73', 1173, '3968689.73', 4566, '15455042.46', 9, '30463.29', '23761366.21', '0.00', '0000-00-00', 1, 'Active', '2019-05-13 14:15:48', '2019-05-13 14:15:48');

-- --------------------------------------------------------

--
-- Table structure for table `expense_vendors`
--

CREATE TABLE `expense_vendors` (
  `vendor_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `vendor_code` varchar(100) NOT NULL,
  `vendor_unique_code` varchar(250) NOT NULL,
  `vendor_date` date NOT NULL,
  `vendor_name` varchar(100) NOT NULL,
  `vendor_expense_target` varchar(20) DEFAULT NULL,
  `vendor_billing_address` text NOT NULL,
  `vendor_billing_country` int(11) NOT NULL,
  `vendor_billing_state` int(11) NOT NULL,
  `vendor_billing_city` int(11) NOT NULL,
  `vendor_billing_pincode` varchar(50) NOT NULL,
  `vendor_shipping_address` text NOT NULL,
  `vendor_shipping_country` int(11) NOT NULL,
  `vendor_shipping_state` int(11) NOT NULL,
  `vendor_shipping_city` int(11) NOT NULL,
  `vendor_shipping_pincode` varchar(50) NOT NULL,
  `vendor_pan_no` varchar(50) NOT NULL,
  `vendor_tan_no` varchar(100) NOT NULL,
  `vendor_credit_period` int(11) NOT NULL,
  `vendor_credit_reminder` varchar(50) NOT NULL,
  `vendor_currency` int(11) NOT NULL,
  `vendor_revenue_target` decimal(10,2) NOT NULL,
  `vendor_last_revenue` decimal(10,2) NOT NULL,
  `vendor_revenue_start_date` date NOT NULL,
  `vendor_revenue_end_date` date NOT NULL,
  `vendor_due_date` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vendor_portal_access` int(11) NOT NULL DEFAULT '0',
  `is_invited` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_vendors`
--

INSERT INTO `expense_vendors` (`vendor_id`, `reg_id`, `bus_id`, `vendor_code`, `vendor_unique_code`, `vendor_date`, `vendor_name`, `vendor_expense_target`, `vendor_billing_address`, `vendor_billing_country`, `vendor_billing_state`, `vendor_billing_city`, `vendor_billing_pincode`, `vendor_shipping_address`, `vendor_shipping_country`, `vendor_shipping_state`, `vendor_shipping_city`, `vendor_shipping_pincode`, `vendor_pan_no`, `vendor_tan_no`, `vendor_credit_period`, `vendor_credit_reminder`, `vendor_currency`, `vendor_revenue_target`, `vendor_last_revenue`, `vendor_revenue_start_date`, `vendor_revenue_end_date`, `vendor_due_date`, `status`, `createdat`, `updatedat`, `vendor_portal_access`, `is_invited`) VALUES
(3, 86, 154, 'VEN000001', 'VEN371TM', '1970-01-01', 'SomeVendor', '5678', 'Nowhere', 101, 22, 2707, '400054', '', 0, 0, 0, '', 'ABCDE1234F', 'PDES03028F', 5, '', 47, '0.00', '0.00', '0000-00-00', '0000-00-00', '0000-00-00', 'Active', '2019-05-14 10:31:55', '2019-05-14 10:31:55', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `expense_vendor_contacts`
--

CREATE TABLE `expense_vendor_contacts` (
  `cp_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `cp_vendor_code` varchar(250) NOT NULL,
  `cp_name` varchar(250) NOT NULL,
  `cp_mobile` varchar(100) NOT NULL,
  `cp_email` varchar(100) NOT NULL,
  `cp_password` varchar(250) NOT NULL,
  `cp_birth_date` date DEFAULT NULL,
  `cp_birth_reminder` int(11) DEFAULT NULL,
  `cp_anniversary_date` date DEFAULT NULL,
  `cp_anniversary_reminder` int(11) DEFAULT NULL,
  `cp_is_admin` int(11) NOT NULL,
  `cp_otp` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_vendor_contacts`
--

INSERT INTO `expense_vendor_contacts` (`cp_id`, `vendor_id`, `cp_cust_code`, `cp_name`, `cp_mobile`, `cp_email`, `cp_password`, `cp_birth_date`, `cp_birth_reminder`, `cp_anniversary_date`, `cp_anniversary_reminder`, `cp_is_admin`, `cp_otp`, `type`, `status`, `createdat`, `updatedat`) VALUES
(2, 3, 'VEN371TM', 'NoOne', '9845467331', 'NoOne@gmail.com', '', '2019-05-08', NULL, '2019-05-22', NULL, 0, '', 'vendor', 'Active', '2019-05-14 10:31:55', '2019-05-14 10:31:55');