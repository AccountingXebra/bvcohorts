-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2019 at 03:17 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eazyinvoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `expense_payments`
--

CREATE TABLE `expense_payments` (
  `epay_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `epay_code` varchar(100) DEFAULT NULL,
  `epay_expense_id` int(11) NOT NULL,
  `epay_date` date NOT NULL COMMENT 'expense_date',
  `vendor_id` int(11) NOT NULL,
  `epay_date1` date NOT NULL COMMENT 'payment_date',
  `epay_status` varchar(100) NOT NULL,
  `epay_cstatus` int(11) NOT NULL DEFAULT '0',
  `epay_mode` varchar(100) DEFAULT NULL,
  `epay_bank_id` int(11) NOT NULL,
  `epay_amount` decimal(10,2) NOT NULL,
  `epay_tds_amt` decimal(10,2) NOT NULL,
  `epay_non_payable_amt` decimal(10,2) NOT NULL,
  `epay_bcharges_amt` decimal(10,2) NOT NULL,
  `epay_penalty_charges` decimal(10,2) NOT NULL,
  `epay_interest_charges` decimal(10,2) NOT NULL,
  `epay_forex_gain` decimal(10,2) NOT NULL,
  `epay_forex_loss` decimal(10,2) NOT NULL,
  `ep_dollar_number` int(11) NOT NULL,
  `ep_inr_value` decimal(10,2) NOT NULL,
  `epay_pending_amt` decimal(10,2) NOT NULL,
  `epay_padvice` varchar(250) DEFAULT NULL,
  `epay_notes` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expense_payments`
--
ALTER TABLE `expense_payments`
  ADD PRIMARY KEY (`epay_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expense_payments`
--
ALTER TABLE `expense_payments`
  MODIFY `epay_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `refer_earn`
--

CREATE TABLE `refer_earn` (
  `refer_id` int(11) NOT NULL,
  `reg_no` int(11) NOT NULL,
  `refer_code` varchar(100) NOT NULL,
  `refer_name` varchar(200) NOT NULL,
  `refer_email` varchar(200) NOT NULL,
  `refer_cell_no` varchar(20) NOT NULL,
  `refer_billing_address` varchar(200) NOT NULL,
  `refer_pan_no` varchar(50) NOT NULL,
  `refer_bank_no` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `refer_earn`
--

INSERT INTO `refer_earn` (`refer_id`, `reg_no`, `refer_code`, `refer_name`, `refer_email`, `refer_cell_no`, `refer_billing_address`, `refer_pan_no`, `refer_bank_no`, `status`, `createdat`, `updatedat`) VALUES
(1, 73, 'manojxebra000001', 'manoj', 'manu@yopmail.com', '9225146080', 'IIITG - IT Park Street, Bongora, Guwahati, Assam 781015', 'AAAPL1234C', '', 'Active', '2019-05-30 10:05:20', '2019-05-30 10:05:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `refer_earn`
--
ALTER TABLE `refer_earn`
  ADD PRIMARY KEY (`refer_id`),
  ADD KEY `sales_advance_receipts_ibfk_2` (`refer_code`),
  ADD KEY `sales_advance_receipts_ibfk_3` (`reg_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `refer_earn`
--
ALTER TABLE `refer_earn`
  MODIFY `refer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `refer_earn_list`
--

CREATE TABLE `refer_earn_list` (
  `refer_list_id` int(11) NOT NULL,
  `reg_no` int(11) NOT NULL,
  `refer_id` int(11) NOT NULL,
  `refer_list_email` varchar(100) NOT NULL,
  `refer_list_mobile` int(11) NOT NULL,
  `last_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refer_earn_list`
--

INSERT INTO `refer_earn_list` (`refer_list_id`, `reg_no`, `refer_id`, `refer_list_email`, `refer_list_mobile`, `last_updated`) VALUES
(1, 85, 1, 'aoe0x1@gmail.com', 0, '2019-05-23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `refer_earn_list`
--
ALTER TABLE `refer_earn_list`
  ADD PRIMARY KEY (`refer_list_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `refer_earn_list`
--
ALTER TABLE `refer_earn_list`
  MODIFY `refer_list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_purchase`
--

CREATE TABLE `asset_purchase` (
  `apur_id` int(11) NOT NULL,
  `asset_type_id` int(11) DEFAULT NULL,
  `cust_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asset_code` varchar(100) NOT NULL,
  `asset_nature` varchar(100) NOT NULL,
  `apur_date` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `apur_amt` decimal(10,0) NOT NULL,
  `apur_bill` varchar(250) DEFAULT NULL,
  `ref_inv_no` varchar(100) NOT NULL,
  `ref_po_no` varchar(100) DEFAULT NULL,
  `apur_preowned` varchar(10) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL,
  `apur_gst_id` int(11) NOT NULL,
  `apur_supply_place` varchar(250) NOT NULL,
  `apur_dep_method` varchar(100) NOT NULL,
  `apur_dep_sdate` varchar(100) NOT NULL,
  `apur_dep_percentage` decimal(10,0) NOT NULL,
  `apur_eff_life_year` int(11) NOT NULL,
  `apur_eff_life_month` int(11) NOT NULL,
  `apur_residual_val` int(11) NOT NULL,
  `apur_purchase_date` varchar(100) NOT NULL,
  `apur_terms` text NOT NULL,
  `apur_notes` varchar(255) DEFAULT NULL,
  `apur_taxable_amt` decimal(10,2) NOT NULL,
  `apur_cgst_total` decimal(10,2) NOT NULL,
  `apur_sgst_total` decimal(10,2) NOT NULL,
  `apur_grant_total` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_purchase`
--

INSERT INTO `asset_purchase` (`apur_id`, `asset_type_id`, `cust_id`, `bus_id`, `reg_id`, `gst_id`, `asset_code`, `asset_nature`, `apur_date`, `asset_name`, `apur_amt`, `apur_bill`, `ref_inv_no`, `ref_po_no`, `apur_preowned`, `vendor_id`, `apur_gst_id`, `apur_supply_place`, `apur_dep_method`, `apur_dep_sdate`, `apur_dep_percentage`, `apur_eff_life_year`, `apur_eff_life_month`, `apur_residual_val`, `apur_purchase_date`, `apur_terms`, `apur_notes`, `apur_taxable_amt`, `apur_cgst_total`, `apur_sgst_total`, `apur_grant_total`, `status`, `createdat`, `updatedat`) VALUES
(1, 1, 0, 152, 73, 92, 'AP000001', 'Tangible', '2019-06-05', 'Laptop', '90000', NULL, 'INV/50', 'PO/50', NULL, 0, 0, '101', 'Straight Line', '0000-00-00', '12', 1, 1, 50, '0000-00-00', '', 'notes', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-05 13:18:52', '2019-06-05 15:48:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_purchase`
--
ALTER TABLE `asset_purchase`
  ADD PRIMARY KEY (`apur_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_purchase`
--
ALTER TABLE `asset_purchase`
  MODIFY `apur_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- --------------------------------------------------------

--
-- Table structure for table `asset_type_list`
--

CREATE TABLE `asset_type_list` (
  `atl_id` int(11) NOT NULL,
  `asset_type_name` varchar(200) NOT NULL,
  `sac_no` varchar(100) NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_type_list`
--

INSERT INTO `asset_type_list` (`atl_id`, `asset_type_name`, `sac_no`, `notes`) VALUES
(1, 'Computer', 'sacno12345', 'Computer'),
(2, 'Furniture', 'sacno12346', 'Something like that');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_type_list`
--
ALTER TABLE `asset_type_list`
  ADD PRIMARY KEY (`atl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_type_list`
--
ALTER TABLE `asset_type_list`
  MODIFY `atl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_purchase_list`
--

CREATE TABLE `asset_purchase_list` (
  `apl_id` int(11) NOT NULL,
  `apur_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `particulars` varchar(200) NOT NULL,
  `hsn` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `taxable_amt` decimal(10,0) NOT NULL,
  `cgst_amt` decimal(10,0) NOT NULL,
  `sgst_amt` decimal(10,0) NOT NULL,
  `amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_purchase_list`
--
ALTER TABLE `asset_purchase_list`
  ADD PRIMARY KEY (`apl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_purchase_list`
--
ALTER TABLE `asset_purchase_list`
  MODIFY `apl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_sale`
--

CREATE TABLE `asset_sale` (
  `asal_id` int(11) NOT NULL,
  `asset_apur_id` int(11) DEFAULT NULL,
  `cust_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asal_date` varchar(100) NOT NULL,
  `asal_amt` decimal(10,0) NOT NULL,
  `asal_profit_loss` varchar(10) NOT NULL,
  `purchaser_name` varchar(100) NOT NULL,
  `purchase_pan_no` varchar(50) DEFAULT NULL,
  `purchase_gst_no` varchar(100) NOT NULL,
  `asal_billing_address` varchar(250) NOT NULL,
  `asal_shipping_address` varchar(250) NOT NULL,
  `asal_supply_place` varchar(250) NOT NULL,
  `asal_terms` text NOT NULL,
  `asal_notes` text,
  `asal_taxable_amt` decimal(10,2) NOT NULL,
  `asal_cgst_total` decimal(10,2) NOT NULL,
  `asal_sgst_total` decimal(10,2) NOT NULL,
  `asal_grant_total` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_sale`
--

INSERT INTO `asset_sale` (`asal_id`, `asset_apur_id`, `cust_id`, `bus_id`, `reg_id`, `gst_id`, `asal_date`, `asal_amt`, `asal_profit_loss`, `purchaser_name`, `purchase_pan_no`, `purchase_gst_no`, `asal_billing_address`, `asal_shipping_address`, `asal_supply_place`, `asal_terms`, `asal_notes`, `asal_taxable_amt`, `asal_cgst_total`, `asal_sgst_total`, `asal_grant_total`, `status`, `createdat`, `updatedat`) VALUES
(1, 1, 0, 152, 73, 92, '0000-00-00', '10000', 'Profit', 'Shikhar Yadav', 'something', 'trysomething', 'IIITG - IT Park Street, Bongora, Guwahati, Assam 781015', 'IIITG - IT Park Street, Bongora, Guwahati, Assam 781015', 'India', '', 'notes', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-03 11:26:24', '2019-06-03 11:28:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_sale`
--
ALTER TABLE `asset_sale`
  ADD PRIMARY KEY (`asal_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_sale`
--
ALTER TABLE `asset_sale`
  MODIFY `asal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_sale_list`
--

CREATE TABLE `asset_sale_list` (
  `asl_id` int(11) NOT NULL,
  `asal_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `particulars` varchar(200) NOT NULL,
  `hsn` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `taxable_amt` decimal(10,0) NOT NULL,
  `cgst_amt` decimal(10,0) NOT NULL,
  `sgst_amt` decimal(10,0) NOT NULL,
  `amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_sale_list`
--
--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_sale_list`
--
ALTER TABLE `asset_sale_list`
  ADD PRIMARY KEY (`asl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_sale_list`
--
ALTER TABLE `asset_sale_list`
  MODIFY `asl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_register`
--

CREATE TABLE `asset_register` (
  `areg_id` int(11) NOT NULL,
  `apur_id` int(11) DEFAULT NULL,
  `cust_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asset_description` varchar(200) NOT NULL,
  `areg_serial_no` varchar(100) NOT NULL,
  `areg_warranty_expiry` varchar(250) NOT NULL,
  `areg_remainder_email` varchar(100) NOT NULL,
  `areg_status` varchar(250) NOT NULL,
  `areg_employee_code` varchar(250) DEFAULT NULL,
  `areg_location` varchar(250) NOT NULL,
  `areg_vendor_id` int(11) DEFAULT NULL,
  `areg_vendor_gst_id` int(11) NOT NULL,
  `areg_vendor_contact_name` varchar(250) NOT NULL,
  `areg_vendor_contact_number` varchar(100) NOT NULL,
  `areg_vendor_email` varchar(250) NOT NULL,
  `areg_amc_provider_name` varchar(250) NOT NULL,
  `areg_amc_contact_name` varchar(250) NOT NULL,
  `areg_amc_contact_number` varchar(250) NOT NULL,
  `areg_amc_email` varchar(250) NOT NULL,
  `areg_amc_service_type` varchar(250) NOT NULL,
  `areg_amc_service_frequency` varchar(250) NOT NULL,
  `areg_amc_remainder_date` varchar(255) DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table `asset_register`
--
ALTER TABLE `asset_register`
  ADD PRIMARY KEY (`areg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_register`
--
ALTER TABLE `asset_register`
  MODIFY `areg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_sales_receipt`
--

CREATE TABLE `asset_sales_receipt` (
  `asrec_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asrec_code` varchar(100) DEFAULT NULL,
  `asrec_date` varchar(100) NOT NULL,
  `asrec_receipt_date` varchar(200) NOT NULL,
  `asrec_status` varchar(200) NOT NULL,
  `asrec_mode` varchar(200) NOT NULL,
  `asrec_bank_id` int(11) NOT NULL,
  `asrec_receipt_amt` decimal(10,2) NOT NULL,
  `asrec_tds_amt` decimal(10,2) NOT NULL,
  `asrec_baddebts_amt` decimal(10,2) NOT NULL,
  `asrec_bcharges_amt` decimal(10,2) NOT NULL,
  `asrec_penalty_charges` decimal(10,2) NOT NULL,
  `asrec_interest_charges` decimal(10,2) NOT NULL,
  `asrec_forex_gain` decimal(10,2) NOT NULL,
  `asrec_forex_loss` decimal(10,2) NOT NULL,
  `asrec_notes` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for dumped tables
--

--
-- Indexes for table `asset_sales_receipt`
--
ALTER TABLE `asset_sales_receipt`
  ADD PRIMARY KEY (`asrec_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_sales_receipt`
--
ALTER TABLE `asset_sales_receipt`
  MODIFY `asrec_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `asset_payments`
--

CREATE TABLE `asset_payments` (
  `apay_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `apay_code` varchar(100) DEFAULT NULL,
  `apay_date` varchar(100) NOT NULL COMMENT 'expense_date',
  `vendor_id` int(11) NOT NULL,
  `apay_date1` varchar(100) NOT NULL COMMENT 'payment_date',
  `apay_status` varchar(100) NOT NULL,
  `apay_mode` varchar(100) DEFAULT NULL,
  `apay_bank_id` int(11) NOT NULL,
  `apay_amount` decimal(10,2) NOT NULL,
  `apay_tds_amt` decimal(10,2) NOT NULL,
  `apay_baddebts_amt` decimal(10,2) NOT NULL,
  `apay_bcharges_amt` decimal(10,2) NOT NULL,
  `apay_penalty_charges` decimal(10,2) NOT NULL,
  `apay_interest_charges` decimal(10,2) NOT NULL,
  `apay_forex_gain` decimal(10,2) NOT NULL,
  `apay_forex_loss` decimal(10,2) NOT NULL,
  `ap_dollar_number` int(11) NOT NULL,
  `ap_inr_value` decimal(10,2) NOT NULL,
  `apay_pending_amt` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_payments`
--
ALTER TABLE `asset_payments`
  ADD PRIMARY KEY (`apay_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_payments`
--
ALTER TABLE `asset_payments`
  MODIFY `apay_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
